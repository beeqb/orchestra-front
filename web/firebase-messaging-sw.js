// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
importScripts("https://www.gstatic.com/firebasejs/8.0.2/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.0.2/firebase-messaging.js");
var firebaseConfig = {
  apiKey: "AIzaSyBDgnLQXkPxAWbj2xjoGhkZNtqathz73zA",
  authDomain: "decisive-studio-274111.firebaseapp.com",
  databaseURL: "https://decisive-studio-274111.firebaseio.com",
  projectId: "decisive-studio-274111",
  storageBucket: "decisive-studio-274111.appspot.com",
  messagingSenderId: "256902664452",
  appId: "1:256902664452:web:50582d29debd725e294051",
  measurementId: "G-5RFZLTDY5H",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
var messaging = firebase.messaging();

console.log("messaging init: ", messaging);

messaging.onBackgroundMessage(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  
  // Customize notification here
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
    icon: 'web/icon_96x96.png'
/*     https://arc.beeqb.com/api/statics/logo.png
 */  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});
