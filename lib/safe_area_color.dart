import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class SafeAreaColor with ChangeNotifier {
  Color backgroundColor = Colors.white;
  Color statusBarColor = Colors.white;

  ThemeData _data = ThemeData.dark();

  void setThemeData(ThemeData data) {
    _data = data;
    log(data.toString());
    backgroundColor = data.primaryColor;
    statusBarColor = data.primaryColor;
  }

  void setColorLoginPage() {
    backgroundColor = const Color(0xFF2d3447);
    statusBarColor = backgroundColor;
    notifyListeners();
  }

  void setStyle(
    bool isPrimary,
  ) {
    statusBarColor = _data.primaryColor;
    log(isPrimary.toString());
    final brightness = SchedulerBinding.instance.window.platformBrightness;
    if (isPrimary) {
      backgroundColor = _data.primaryColor;
    } else {
      backgroundColor = backgroundColor =
          brightness == Brightness.light ? Colors.white : _data.backgroundColor;
    }

    notifyListeners();
  }
}
