import '../models/user.dart';

class UserViewModel {
  final UserModel currentUser;

  UserViewModel(this.currentUser);

  get profit => (currentUser.stats != null) ? currentUser.stats!.profit : 0;
  get purchases =>
      (currentUser.stats != null) ? currentUser.stats!.purchases : 0;
  get costs => (currentUser.stats != null) ? currentUser.stats!.costs : 0;
  get incomeAmount => (profit +
          (currentUser.received != null ? currentUser.received! : 0) +
          (currentUser.deposited != null ? currentUser.deposited! : 0))
      .toStringAsFixed(2);
  get spendAmount => (purchases +
          costs +
          (currentUser.sended != null ? currentUser.sended! : 0))
      .toStringAsFixed(2);
}
