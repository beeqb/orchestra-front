import 'package:orchestra/models/enums/enums.dart';

import '../config.dart';
import '../core/const.dart';
import '../models/product/product.dart';
import '../util/text_util.dart';

class ProductViewModel {
  static String priceToStr(Product product) {
    String _price = formatCurrency.format(product.pricingAndBusiness.price);
    return product.pricingAndBusiness.model ==
            BusinessModelType.monthlySubscription
        ? '$_price/mo'
        : product.pricingAndBusiness.model == BusinessModelType.perRequest
            ? '$_price/req'
            : _price;
  }

  static String rankStringDecimal(String? rank) {
    if (rank != null) {
      if (rank.length >= 3) {
        return rank.substring(0, 3);
      } else {
        return '${rank.substring(0, 1)}.0';
      }
    } else {
      return '0.0';
    }
  }

  static double? rankAsDouble(String? rank) {
    if (rank != null) {
      return double.tryParse(rank);
    } else {
      return 0.0;
    }
  }

  String getWidgetAvatarUrl(String id) => '$kWidgetAvatarUrl/$id.png';

  String getProductTypeTitle(String name) =>
      ProductTypeAliases.kProductType[name]!;

  static String getTestFly(List<String> testEndsAfter) {
    var result = 'None';
    for (var e in testEndsAfter) {
      var days = e.endsWith('d');
      if (days) {
        result = '${e.split('d').first} days';
      } else {
        result = '${e.split('l').first} launches';
      }
    }
    return result;
  }
}
