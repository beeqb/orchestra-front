// ignore_for_file: unnecessary_string_escapes

import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:orchestra/core/const.dart';
import 'package:orchestra/models/product/product.dart';

import '../config.dart';
import '../models/enums/photo_type.dart';

extension CapExtension on String {
  String get inCaps => '${this[0].toUpperCase()}${substring(1)}';
  String get allInCaps => toUpperCase();
}

bool isValidEmail(String input) {
  final regex = RegExp(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
  return regex.hasMatch(input);
}

final formatCurrency = NumberFormat.simpleCurrency();
final formatCurrency5Digits = NumberFormat.simpleCurrency(decimalDigits: 5);
final formatCurrency2Digits = NumberFormat.simpleCurrency(decimalDigits: 2);

final dateFormat = DateFormat('yyyy-MM-dd');
final dateFormatWithTime = DateFormat('yyyy-MM-dd HH:mm');
final dateFormatWithTimeSec = DateFormat('yyyy-MM-dd HH:mm:ss');

String getDayNumber(int dayNum) {
  return DateTime.now().subtract(Duration(days: dayNum)).day.toString();
}

String getWeekDay(int dayNum) {
  return DateFormat('EE')
      .format(DateTime.now().subtract(Duration(days: dayNum)));
}

String getLabelText({
  required bool freeForTest,
  required List<String> testEndsAfter,
}) {
  if (freeForTest) {
    if (testEndsAfter.isEmpty) {
      return 'Forever free';
    }
    if (testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '20 launches in 10 days';
    }
    if (testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '15 launches in 10 days';
    }
    if (testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '5 launches in 10 days';
    }
    if (testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '10 days';
    }
    if (!testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '7 days';
    }
    if (testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '3 days';
    }
    if (!testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return 'Forever free';
    }
    if (!testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '15 launches';
    }
    if (!testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '20 launches';
    }
    if (!testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '5 launches';
    }
    if (testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '5 launches in 3 days';
    }
    if (testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '15 launches in 3 days';
    }
    if (testEndsAfter.contains('3d') &&
        !testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '20 launches in 3 days';
    }
    if (!testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        !testEndsAfter.contains('15l')) {
      return '5 launches in 7 days';
    }
    if (!testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        !testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '15 launches in 7 days';
    }
    if (!testEndsAfter.contains('3d') &&
        testEndsAfter.contains('7d') &&
        testEndsAfter.contains('5l') &&
        testEndsAfter.contains('15l')) {
      return '20 launches in 7 days';
    }
  }
  return 'No Test fly';
}

String getImageUrl({
  required Product? editProduct,
  required PhotoUrlType fieldName,
}) {
  const fileNotFound = 'File not found';
  const error = 'error';
  if (editProduct != null) {
    if (editProduct.photoDescriptionUrls != null &&
        editProduct.photoDescriptionUrls!.isNotEmpty) {
      final length = editProduct.photoDescriptionUrls!.length;
      if (fieldName == PhotoUrlType.photoDescUrl_1 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![0].substring(
              1,
            );
      }
      if (fieldName == PhotoUrlType.photoDescUrl_2 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![1];
      }
      if (fieldName == PhotoUrlType.photoDescUrl_3 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![2];
      }
      if (fieldName == PhotoUrlType.photoDescUrl_4 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![3];
      }
      if (fieldName == PhotoUrlType.photoDescUrl_5 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![4];
      }
      if (fieldName == PhotoUrlType.photoDescUrl_6 &&
          fieldName.index < length) {
        return kWidgetPhotoDescriptionUrl +
            editProduct.photoDescriptionUrls![5];
      }
    }
    if (fieldName == PhotoUrlType.logoUrl) {
      return editProduct.productLogoURL.isNotEmpty
          ? kWidgetAvatarUrl + editProduct.productLogoURL
          : error;
    }
    if (fieldName == PhotoUrlType.promoUrl) {
      return editProduct.productPromoPictureURL != null
          ? kWidgetAvatarUrl + editProduct.productPromoPictureURL!
          : error;
    }

    if (fieldName == PhotoUrlType.miniPromoUrl) {
      return editProduct.productMiniPromoPictureURL != null
          ? kWidgetAvatarUrl + editProduct.productMiniPromoPictureURL!
          : error;
    }
    if (fieldName == PhotoUrlType.launcherFileUrl) {
      return editProduct.productLauncher.productLauncherFileUrl ?? fileNotFound;
    }
  }
  return error;
}

String corsRefactoring(String result) {
  if (result.contains('img') && kIsWeb) {
    final url = result.split('<');

    final string = url[1].split('=');
    log('1212 ${string[1]}');

    final handled = [
      '<img src=',
      kCors,
      string[1],
    ].join().replaceAll('\"', '');
    url.removeAt(1);
    url.insert(1, handled);
    final str = url.join('<');
    var rezStr = str.replaceFirst(RegExp('<'), '');
    return rezStr.substring(0, rezStr.length - 1);
  } else {
    return result;
  }
}
