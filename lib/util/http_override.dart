import 'dart:io';

// resolve error HandshakeException (HandshakeException: Handshake error in
//client (OS Error:  	CERTIFICATE_VERIFY_FAILED: unable to get local issuer
//certificate(handshake.cc:354)))

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (cert, host, port) => true;
  }
}
