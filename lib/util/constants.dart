library constants;

const String kAppTitle = 'Orchestra';
const String kUsersAuthKey = 'user-auth-key';
const String kDefaultAppIcon =
    'https://upload-icon.s3.us-east-2.amazonaws.com/uploads/icons/png/8122551761583400157-512.png';
const kDefaultHeaders = {'Content-Type': 'application/json'};
const String kImageUrlPlaceholder =
    'https://upload-icon.s3.us-east-2.amazonaws.com/uploads/icons/png/8122551761583400157-512.png';
const String kSocketPoint = 'wss://app.beeqb.com:443';
const String kFcmVapidKey =
    // ignore: lines_longer_than_80_chars
    'BMZ5Q6wXu3SG-BaeDQKqz0DmbbWduhmvLCKfKecfo5K7cdrbvSvJUGPNgSEHhBdxwpvgyPNuHXyfdiaHlgjv6RE';

// Deposit constants
const String kPublishableKeyStripe = 'pk_test_3WlEREAR9fv19nvFIPzb2UGx';
const String kDepositSuccessPageUrl = 'https://app.beeqb.com/#/depositSuccess';
const String kDepositFailedPageUrl = 'https://app.beeqb.com/#/depositFailed';

// Share product link
const String kShareProductLink = 'https://app.beeqb.com/#/marketplace/';

// Open telegram bor
const String kTelegramBotLink = 'https://www.telegram.me/OrchestrasBot';
