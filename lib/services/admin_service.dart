import 'package:orchestra/models/billing_models/admin_transact_model.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/running_app.dart';
import 'package:orchestra/repository/billing_repository.dart';
import 'package:orchestra/repository/product_repository.dart';

class AdminService {
  final BillingRepository _billingRepository;
  final ProductRepository _productRepository;

  AdminService({
    required BillingRepository billingRepository,
    required ProductRepository productRepository,
  })  : _billingRepository = billingRepository,
        _productRepository = productRepository,
        super();

  Future<List<AdminTransactModel>> getAllTransactions() async {
    return await _billingRepository.getAllTransactions();
  }

  Future<List<Product>> getAllProducts() async {
    return await _productRepository.getAllProducts();
  }

  Future<void> moderate(String productId, ModerateType moderateType) async {
    return await _productRepository.moderate(productId, moderateType);
  }

  Future<List<Product>> searchProduct(String query) async {
    return await _productRepository.searchProduct(query);
  }

  Future<String> getProductSource(String productId) async {
    return await _productRepository.getProductSource(productId);
  }

  Future<String> getProductManifest(String productId) async {
    return await _productRepository.getProductManifest(productId);
  }

  Future<String> startTestProduct(
    String productId,
    Map<String, dynamic> settings,
  ) async {
    return await _productRepository.startTestProduct(
      productId,
      settings,
    );
  }

  Future<List<RunningAppModel>> getRunningsProduct() async {
    return await _productRepository.getRunningProducts();
  }
}
