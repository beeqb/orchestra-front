import 'package:dartz/dartz.dart';
import 'package:file_picker/file_picker.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/product_models/post_product.dart';

import '../core/exceptions.dart';
import '../core/failures.dart';
import '../models/answer_item.dart';
import '../models/app.dart';
import '../models/app_category.dart';
import '../models/purchase_model.dart';
import '../models/rating_item_model.dart';
import '../models/server_message.dart';
import '../repository/product_repository.dart';

class ProductService {
  final ProductRepository _productRepository;

  const ProductService({required ProductRepository productRepository})
      : _productRepository = productRepository,
        super();

  Future<Either<Failure, List<AppCategory>>>
      getProductsNotCreatedByUser() async {
    try {
      return Right(await _productRepository.getProductsNotCreatedByUser());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<List<PurchaseModel>> getPurchasedProducts() async {
    return await _productRepository.getPurchasedProducts();
  }

  Future<Product> buyProduct(String productId) async {
    return await _productRepository.buyProduct(productId);
  }

  Future<List<Product>> getProductsByUserId() async {
    return await _productRepository.getProductsByUserId();
  }

  Future<Either<Failure, List<AppCategory>>>
      getTopProductsByCategories() async {
    try {
      return Right(await _productRepository.getTopProductsByCategories());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, List<Product>>> getTopProducts() async {
    try {
      return Right(await _productRepository.getTopProducts());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<ServerMessage> saveWidgetForApp(
      Product product, String appId, String userId) async {
    return await _productRepository.saveWidgetForApp(product, appId, userId);
  }

  Future<AppModel> deleteWidgetFromApp(
      String userId, String appId, String productId) async {
    return await _productRepository.deleteWidgetFromApp(
        userId, appId, productId);
  }

  Future<String> addRatingAndComment(
      String productId, String userId, int rating, String comment) async {
    return await _productRepository.addRatingAndComment(
        productId, userId, rating, comment);
  }

  Future<dynamic> addRatingAnswer({
    required AnswerItem answerItem,
    required String productId,
  }) async {
    return await _productRepository.addRatingAnswer(
      answerItem,
      productId,
    );
  }

  Future<List<RatingItem>> getProductRatings(String productId) async {
    return await _productRepository.getProductRatings(productId);
  }

  Future<Product> addProduct({required PostProduct newProduct}) async {
    return await _productRepository.addProduct(newProduct);
  }

  Future<Product> editProductById({required Product newProduct}) async {
    return await _productRepository.editProductById(newProduct);
  }

  Future<void> changeProductStatusToDeleted({required String productId}) async {
    return await _productRepository.changeProductStatusToDeleted(productId);
  }

  Future<List<Product>> getProductsByQuery(String query) async {
    return await _productRepository.getProductsByQuery(query);
  }

  Future<Product> getProductById(String productId) async {
    return await _productRepository.getProductById(productId);
  }

  Future<void> uploadFile(
      PlatformFile file, String field, String userId) async {
    await _productRepository.uploadFile(file, field, userId);
  }
}
