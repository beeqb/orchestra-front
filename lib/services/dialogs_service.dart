import 'dart:async';

import 'package:orchestra/models/product/product.dart';

import '../models/dialog_model.dart';
import '../models/enums/dialog_type.dart';
import '../models/rating_item_model.dart';

class DialogsService {
  late Function(DialogRequest) _showDialogListener;
  late Completer<DialogResponse> _dialogCompleter;

  // ignore: use_setters_to_change_properties
  /// Registers a callback function. Typically to show the dialog
  void registerDialogListener(Function(DialogRequest) showDialogListener) {
    _showDialogListener = showDialogListener;
  }

  /// Calls the dialog listener and returns a Future that will wait for
  /// dialogComplete.
  Future<DialogResponse> showDialog({
    required DialogType dialogType,
    String? title,
    String? description,
    RatingItem? ratingItem,
    Product? product,
  }) {
    _dialogCompleter = Completer();
    _showDialogListener(DialogRequest(
      dialogType: dialogType,
      title: title ?? '',
      description: description ?? '',
      ratingItem: ratingItem,
      product: product,
    ));
    return _dialogCompleter.future;
  }

  /// Completes the _dialogCompleter to resume the Future's execution call
  void dialogComplete(DialogResponse response) {
    _dialogCompleter.complete(response);
    // _dialogCompleter = null;
  }
}
