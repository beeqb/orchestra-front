import 'dart:async';

import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as html;

import '../../blocs/cubits/checkout_cubit.dart';
import '../../injection_container.dart';

Future<void> redirectToCoinCheckout(BuildContext _, {String? sessionId}) async {
  StreamSubscription? subscription;
  final cubit = sl<CheckoutCubit>();
  subscription = cubit.stream.listen((state) {
    if (state is CheckoutLoaded) {
      html.window.location.assign(state.session);
      subscription!.cancel();
    }
  });
}
