import 'package:flutter/material.dart';

import 'coin_checkout_stub.dart'
    if (dart.library.io) 'package:orchestra/pages/coin_deposit.dart'
    if (dart.library.js) 'package:orchestra/services/coinbase/coin_checkout_web.dart'
    as impl;

void redirectToCheckout(BuildContext context) =>
    impl.redirectToCoinCheckout(context);
