import 'dart:async';

import 'package:rxdart/rxdart.dart';
import '../injection_container.dart';
import 'app_service.dart';

class TimerService {
  Timer? _timer;
  final _controller = BehaviorSubject<String>();
  Stream<String> get stream => _controller.stream;
  String? currentTime;

  void close() {
    _controller.close();
  }

  void startTimer(int runningTimeInSec) {
    _timer = _timer ??
        Timer.periodic(const Duration(seconds: 1), (timer) {
          runningTimeInSec++;
          final timerValue = sl<AppService>().printDuration(
            Duration(seconds: runningTimeInSec),
          );
          //debugPrint('timer $timerValue');
          _controller.add(timerValue);
          currentTime = timerValue;
        });
  }

  void resetTimer() {
    //debugPrint('timer STOPPED');
    currentTime = null;
    _timer?.cancel();
    _timer = null;
  }
}
