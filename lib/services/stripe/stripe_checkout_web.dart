@JS()
library stripe;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:js/js.dart';
import '../../blocs/cubits/checkout_cubit.dart';
import '../../injection_container.dart';
import '../../util/constants.dart' as constants;

void redirectToCheckout(BuildContext _, {required String sessionId}) async {
  StreamSubscription? subscription;
  final cubit = sl<CheckoutCubit>();
  subscription = cubit.stream.listen((state) {
    if (state is CheckoutLoaded) {
      final stripe = Stripe(constants.kPublishableKeyStripe);
      stripe.redirectToCheckout(
        CheckoutOptions(
          sessionId: state.session,
        ),
      );
      subscription!.cancel();
    }
  });
}

@JS()
class Stripe {
  external Stripe(String key);
  external void redirectToCheckout(Object object);
}

@JS()
@anonymous
class CheckoutOptions {
  external String get sessionId;
  external factory CheckoutOptions({
    String sessionId,
  });
}
