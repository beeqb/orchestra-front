import 'package:flutter/material.dart';

import 'stripe_checkout_stub.dart'
    if (dart.library.io) 'package:orchestra/pages/stripe_deposit.dart'
    if (dart.library.js) 'package:orchestra/services/stripe/stripe_checkout_web.dart'
    as impl;

void redirectToCheckout(BuildContext context) =>
    impl.redirectToCheckout(context);
