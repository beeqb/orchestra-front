import 'package:flutter/foundation.dart';
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';
import 'package:orchestra/blocs/bloc/results_bloc.dart';
// ignore: library_prefixes
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../injection_container.dart';
import '../models/enums/enums.dart';
import '../util/constants.dart';
import 'dialogs_service.dart';
import 'user_service.dart';

/// Создает соединение с web socket для веб-приложения. В мобильных версиях
/// не используется, так как не получается наладить соединение с сокетом
class SocketService {
  IO.Socket? socket;
  final ResultsBloc _resultsCubit;
  final UserService _userService;
  final DialogsService _dialogsService;

  SocketService({
    required ResultsBloc resultsCubit,
    required UserService userService,
    required DialogsService dialogsService,
  })  : _resultsCubit = resultsCubit,
        _userService = userService,
        _dialogsService = dialogsService,
        super();

  void close() {}

  /// Создает соединение с сокетом и обрабатывает события
  void createSocketConnection() async {
    final _user = await _userService.checkLocalStorage();
    final userId = _user.id;

    socket = IO.io(kSocketPoint, <String, dynamic>{
      'transports': ['websocket'],
      'query': {'userId': userId},
      'autoConnect': false,
    });

    socket!.connect();

    socket!
      ..on('error', (error) => debugPrint('SOCKET_SERVICE error $error'))
      ..on('connect', (_) => debugPrint('connect: ${socket!.id}'))
      ..on('disconnect', (_) => debugPrint('disconnect'))
      ..on('new_data', _eventHandler);
  }

  void _eventHandler(data) {
    final _data = Map<String, dynamic>.from(data);

    //debugPrint('data $_data');

    _dialogsService.showDialog(
      dialogType: DialogType.appResultDialog,
      title: _data['appName'],
      description: _data['data'].first,
    );
    // добавляю ответ сокета в лог для админа
    sl<LogScreenBloc>().add(AddStringLogScreenEvent(
        newString:
            'Socket response:\n${_data['appName']}\n${_data['data'].first}'));
    // обновляю результаты в нижней панели дашборда
    _resultsCubit.add(ResetResultsEvent());
    _resultsCubit.add(FetchResultsEvent());
  }
}
