import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/core/const.dart';

import '../core/exceptions.dart';
import '../core/failures.dart';
import '../models/customer_activity.dart';
import '../models/daily_sale.dart';
import '../models/launch_stats.dart';
import '../models/user.dart';
import '../repository/auth_repository.dart';
import '../repository/user_repository.dart';

class UserService {
  final AuthRepository _authRepository;
  final UserRepository _userRepository;
  UserService({
    required AuthRepository authRepository,
    required UserRepository userRepository,
  })  : _authRepository = authRepository,
        _userRepository = userRepository,
        super();

  Future<Either<Failure, UserModel>> signInWithGoogle() async {
    final user = await _authRepository.signInWithGoogle();
    if (user.id == defaultUserId) {
      return Left(CacheFailure());
    }
    //debugPrint('user from google $user');
    try {
      final savedUser = await _userRepository.saveUserAsGoogleUser(user);
      await _authRepository.saveLocalUser(savedUser);
      return Right(savedUser);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, UserModel>> signInWithFacebook() async {
    print('StartFacebookAuthEvent --- 3');

    final user = await _authRepository.signInWithFacebook();

    print('StartFacebookAuthEvent --- 4');

    if (user.id == defaultUserId) {
      return Left(CacheFailure());
    }
    try {
      final savedUser = await _userRepository.saveUserAsFacebookUser(user);
      print(
          'final savedUser = await _userRepository.saveUserAsFacebookUser(user);');
      print(user.id);
      await _authRepository.saveLocalUser(savedUser);
      print('await _authRepository.saveLocalUser(savedUser);');
      print(savedUser.id);
      return Right(savedUser);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<UserModel> checkLocalStorage() async {
    final user = await _authRepository.getLocalUser();
    if (user.id != defaultUserId) {
      return await getUser();
    }
    return UserModel.empty;
  }

  Future<void> signOut(LoginType type) async {
    debugPrint('sighOut');
    await _authRepository.signOut(type);
  }

  Future<Either<Failure, UserModel>> updateUser(UserModel user) async {
    try {
      final result = await _userRepository.updateUser(user);
      return Right(result);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<UserModel> getUser() async {
    return await _userRepository.getUser();
  }

  Future<String> addPushToken(String token) async {
    return await _userRepository.addPushToken(token);
  }

  Future<Map<String, String>> becomeContributor() async {
    return await _userRepository.becomeContributor();
  }

  Future<List<UserModel>> fetchAllUsers() async {
    return await _userRepository.fetchAllUsers();
  }

  Future<UserModel> adminUserUpdate(UserModel user) async {
    return await _userRepository.adminUserUpdate(user);
  }

  Future<Map<String, String>> addReferral() async {
    return await _userRepository.addReferral();
  }

  Future<Either<Failure, List<UserModel>>> searchUsers(String query) async {
    try {
      return Right(await _userRepository.searchUsers(query: query));
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, List<DailySales>>> getDailySales() async {
    try {
      return Right(await _userRepository.getDailySales());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, LaunchStats>> getLaunchStats() async {
    try {
      return Right(await _userRepository.getLaunchStats());
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  Future<Either<Failure, CustomerActivity>> getCustomerActivity() async {
    try {
      return Right(await _userRepository.getCustomerActivity());
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
