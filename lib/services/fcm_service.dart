import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:orchestra/blocs/bloc/results_bloc.dart';
import 'package:orchestra/services/notif_handler.dart';
import 'package:orchestra/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as html;

import '../injection_container.dart';
import '../main.dart';
import '../models/enums/enums.dart';
import 'dialogs_service.dart';

/// Сервис для работы с Firebase Cloud Messaging
class FcmService {
  FcmService({
    required DialogsService dialogsService,
    required ResultsBloc resultsCubit,
    required UserService userService,
  })  : _dialogsService = dialogsService,
        _resultsCubit = resultsCubit,
        super();

  final DialogsService _dialogsService;
  final ResultsBloc _resultsCubit;

  void init() async {
    var _recieved = false;

    if (!kIsWeb) {
      // Set the background messaging handler early on, as a named top-level function
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);

      /// Create a [AndroidNotificationChannel] for heads up notifications
      const channel = AndroidNotificationChannel(
        'high_importance_channel', // id
        'New app result', // title
        importance: Importance.high,
      );

      /// Initialize the [FlutterLocalNotificationsPlugin] package.
      final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      final settings = await FirebaseMessaging.instance.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );

      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        debugPrint('User granted permission');
      } else if (settings.authorizationStatus ==
          AuthorizationStatus.provisional) {
        debugPrint('User granted provisional permission');
      } else {
        debugPrint('User declined or has not accepted permission');
      }

      /// Update the iOS foreground notification presentation options to allow
      /// heads up notifications.
      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );

      NotificationHandler.initNotifications(wiredashKey);

      final messs = FirebaseMessaging.instance.isAutoInitEnabled;
/* 
      var initializationSettingsAndroid =
          AndroidInitializationSettings('app_icon'); */

      debugPrint('FCM enabled $messs');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        debugPrint('Got a message whilst in the foreground!');
        debugPrint('Message data: ${message.data}');

        if (message.notification != null) {
          debugPrint(
              'Message also contained a notification: ${message.notification}');
        }

        showNotification(message.data['title'], message.data['body']);

        // If `onMessage` is triggered with a notification, construct our own
        // local notification to show to users using the created channel.
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        debugPrint('Message onMessageOpenedApp data: ${message.data}');
      });
    } else {
      // проверить совместное оповещение

      html.document.on['messageFcm'].listen((event) {
        debugPrint('recieved event $event');
        //  debugPrint((event as CustomEvent).detail);
        final fcmMessage = Map.from(
            (event as html.CustomEvent).detail as Map<dynamic, dynamic>);
        debugPrint(' FCM message from js $fcmMessage');
        if (!_recieved) {
          _updateAppInfo();
          _recieved = true;
        }
      });
    }
  }

  void fcmMessageDialog(Map<String, dynamic> message) async {
    await _dialogsService.showDialog(
      dialogType: DialogType.appResultDialog,
      title: message['data']['title'] ?? 'empty title',
      description: message['data']['body'],
    );
  }

  void _updateAppInfo() {
    _resultsCubit.add(FetchResultsEvent());
  }

  static void showNotification(title, body) async {
    final androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'high_importance_channel',
      'New app result',
      visibility: NotificationVisibility.public,
      importance: Importance.max,
      priority: Priority.high,
      icon: 'notif_icon',
    );

    const iosPlatformSpecifics = IOSNotificationDetails();

    final platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, iOS: iosPlatformSpecifics);

    int currentCount = sl<SharedPreferences>().getInt('countBages') ?? 0;

    FlutterAppBadger.updateBadgeCount(currentCount + 1);

    await NotificationHandler.show(0, title, body, platformChannelSpecifics,
        payload: 'payload');
  }
}

/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print('background');
  int currentCount = sl<SharedPreferences>().getInt('countBages') ?? 0;

  FlutterAppBadger.updateBadgeCount(currentCount + 1);
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  debugPrint('Handling a background message ${message.messageId}');
}
