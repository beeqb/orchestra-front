import 'package:orchestra/models/product/product.dart';
import '../models/app.dart';
import '../models/results_model.dart';
import '../models/settings_model.dart';
import '../repository/app_repository.dart';

class AppService {
  final AppRepository _appRepository;
  const AppService({required AppRepository appRepository})
      : _appRepository = appRepository,
        super();

  Future<List<AppModel>> getAppsByUserId() async =>
      _appRepository.getAppsByUserId();

  Future<AppModel> saveApp(AppModel app) async => _appRepository.saveApp(app);

  Future<AppModel> updateApp(AppModel app) async =>
      _appRepository.updateApp(app);

  Future<AppModel> saveAppSettings(AppModel app) async =>
      _appRepository.saveAppSettings(app);

  Future<AppModel> deleteApp(AppModel app) async =>
      _appRepository.deleteApp(app);

  Future<dynamic> startApp(String appId) async =>
      _appRepository.startApp(appId);

  Future<dynamic> pauseApp(String appId) async =>
      _appRepository.pauseApp(appId);

  String getRunningTime(num startTime) {
    String time;
    var _duration = Duration(microseconds: int.parse(startTime.toString()));
    time = printDuration(_duration);
    return time;
  }

  String printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    var twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    var twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return '${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds';
  }

  Future<List<Product>> getWidgetsByAppId(
    String appId,
  ) async {
    return await _appRepository.getWidgetsByAppId(appId);
  }

  Future<List<SettingsModel>> getWidgetSettings(
    String productId,
  ) async {
    return await _appRepository.getWidgetSettings(productId);
  }

  Future<String> getAppWidgetSettings(String appId) async {
    return await _appRepository.getAppWidgetSettings(appId);
  }

  Future<String> saveAppWidgetSettings(
    String appId,
    String productId,
    Map<String, dynamic> settings,
  ) async {
    return await _appRepository.saveAppWidgetSettings(
      appId,
      productId,
      settings,
    );
  }

  Future<List<ResultsModel>> getAppResults(int page) async {
    return await _appRepository.getAppResults(page);
  }

  Future<List<ResultsModel>> getUpdates(String timestamp) async {
    return await _appRepository.getUpdates(timestamp);
  }

  Future<AppModel> getAppById(String appId) async {
    return await _appRepository.getAppById(appId);
  }
}
