import 'package:orchestra/models/billing_info.dart';
import '../models/billing_models/transaction_model.dart';
import '../models/billing_models/transfer_model.dart';
import '../models/billing_models/transfer_user.dart';
import '../repository/billing_repository.dart';

abstract class BillingService {
  Future<TransferModel> send(String userId, double amount);
  Future<Map<String, dynamic>> deposit(
      {required String gateway, required double amount});
  Future<void> withdraw(String data);
  Future<Map<String, String>> chargeForProduct(String productId);
  Future<List<TransactionModel>> getTransactionHistory(int page);
  Future<TransferUser?> getTransferUser(String query);
  Future<BillingInfoModel> getBillingInfo();
}

class BillingServiceImpl extends BillingService {
  final BillingRepository _billingRepository;
  BillingServiceImpl({required BillingRepository billingRepository})
      : _billingRepository = billingRepository,
        super();

  @override
  Future<Map<String, String>> chargeForProduct(String productId) async {
    return await _billingRepository.chargeForProduct(productId);
  }

  @override
  Future<Map<String, dynamic>> deposit(
      {required String gateway, required double amount}) async {
    return await _billingRepository.deposit(gateway, amount);
  }

  @override
  Future<List<TransactionModel>> getTransactionHistory(int page) async {
    return await _billingRepository.getTransactionHistory(page);
  }

  @override
  Future<void> withdraw(String data) async {
    // TODO: implement withdraw
  }

  @override
  Future<TransferUser?> getTransferUser(String query) async {
    return await _billingRepository.getTransferUser(query);
  }

  @override
  Future<TransferModel> send(String userId, double amount) async {
    return await _billingRepository.send(userId, amount);
  }

  @override
  Future<BillingInfoModel> getBillingInfo() async {
    return await _billingRepository.getBillingInfo();
  }
}
