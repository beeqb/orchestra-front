import '../injection_container.dart';
import '../models/app.dart';
import 'timer_service.dart';

class TimersInjections {
  void addTimersToGetIt({required List<AppModel> apps}) {
    // создаю инстанс таймера
    for (var app in apps) {
      if (!sl.isRegistered<TimerService>(instanceName: app.id)) {
        sl.registerLazySingleton<TimerService>(
          () => TimerService(),
          instanceName: app.id,
        );
      }
    }
  }
}
