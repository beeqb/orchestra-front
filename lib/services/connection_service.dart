import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import '../config.dart';
import '../models/enums/enums.dart';
import 'dialogs_service.dart';

/// запуск таймера с проверкой статуса интернет-соединения
class ConnectionService {
  final DialogsService _dialogsService;
  Future? _connectStatusDialog;
  Timer? _timer;

  ConnectionService({required DialogsService dialogsService})
      : _dialogsService = dialogsService,
        super();
  void init() {
    _timer = Timer.periodic(const Duration(seconds: 10), (timer) {
      if (!kDebugMode) {
        _updateConnectionStatus();
      }
    });
  }

  void close() {
    _timer?.cancel();
  }

  Future<void> _updateConnectionStatus() async {
//    debugPrint('проверка соединения с интернетом....');
    final response = await checkConnection();

    if (response == 200) {
    } else {
      _checkAndShowDialog();
    }
  }

  void _checkAndShowDialog() async {
    if (_connectStatusDialog == null) {
      _connectStatusDialog = showNoInternetDialog();
      await _connectStatusDialog;
      _connectStatusDialog = null;
    } else {
      //do nothing
    }
  }

  Future<int> checkConnection() async {
    http.Response response;

    try {
      final _uri = Uri.parse('https://rest.ensembl.org/info/ping');
      response = await http.get(_uri);
      debugPrint(response.statusCode.toString());
      return response.statusCode;
    } on Exception catch (e) {
      debugPrint('connection check error $e');
      rethrow;
    }
  }

  Future showNoInternetDialog() async {
    return _dialogsService.showDialog(
      dialogType: DialogType.noInternetDialog,
      title: 'No server connection',
      description: 'Fix it and move forward',
    );
  }
}
