import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

// PublishSubject<RemoteMessage> newMessagesStream =
//     PublishSubject<RemoteMessage>();

final FlutterLocalNotificationsPlugin localNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void showLocalNotification(String? title, String? body, String? url) async {
  const androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'notify_channel',
    'Информация для сотрудника',
    category: 'Запросы',
    autoCancel: true,
    ongoing: false,
    visibility: NotificationVisibility.public,
    importance: Importance.max,
    priority: Priority.high,
  );

  const iosPlatformSpecifics = IOSNotificationDetails();

  final platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics, iOS: iosPlatformSpecifics);

  await localNotificationsPlugin.show(0, title, body, platformChannelSpecifics,
      payload: url);
}

class FirebaseMessagingWidget extends StatefulWidget {
  final Widget child;
  final GlobalKey globalKey;

  const FirebaseMessagingWidget({
    Key? key,
    required this.child,
    required this.globalKey,
  }) : super(key: key);

  @override
  _FirebaseMessagingWidgetState createState() =>
      _FirebaseMessagingWidgetState();
}

class _FirebaseMessagingWidgetState extends State<FirebaseMessagingWidget> {
  _FirebaseMessagingWidgetState();

  late FirebaseMessaging _messaging;

  @override
  void initState() {
    super.initState();
    Firebase.initializeApp().then((value) {
      _messaging = FirebaseMessaging.instance;

      FirebaseMessaging.instance.getInitialMessage().then((initialMessage) {
        log('message 456 1 $initialMessage');

        // If the message also contains a data property with a "type" of "chat",
        // navigate to a chat screen
        if (initialMessage != null && initialMessage.data['url'] != null) {
          debugPrint(initialMessage.toString());
        }

        //NotificationHandler.initNotifications(_globalKey);
        firebaseCloudMessagingListener();

        //Локальные уведомления
        const androidInitSettings =
            AndroidInitializationSettings('@mipmap/launcher_icon');
        final iosInitSettings = IOSInitializationSettings(
            onDidReceiveLocalNotification: _onDidReceiveLocalNotification);

        final initSettings = InitializationSettings(
            android: androidInitSettings, iOS: iosInitSettings);
        localNotificationsPlugin.initialize(initSettings,
            onSelectNotification: _onSelectNotification);
      });
    });
  }

  Future _onSelectNotification(String? payload) async {
    log('message 456 2 ${payload.toString()}');

    if (payload != null) {
      //  await router.navigateTo(context, payload);
    }
  }

  //Получение локального уведомления для IOS
  Future _onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    //showSnackBar(context, body!, FormAlertTypes.info);
  }

  Future<void> firebaseCloudMessagingListener() async {
    await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    //  await _messaging.subscribeToTopic('Pac-ue');

    //Когда приложение открыто
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      log('message 456 3 ${message.notification!.title}');

      if (message.notification != null) {
        showLocalNotification(message.notification!.title,
            message.notification!.body, message.data['link']);
      }
    });

    //Когда нажимаешь на уведомление, которое пришло в фоне
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      log('message 456 4 ${message.toString()}');
      if (message.notification != null) {
        if (message.data['link'] != null) {
          //  router.navigateTo(context, message.data['url']);
        }
        //  newMessagesStream.add(message);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
