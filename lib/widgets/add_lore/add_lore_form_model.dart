import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/business_model_type.dart';
import 'package:orchestra/models/enums/data_collecting_type.dart';
import 'package:orchestra/models/enums/data_processing_type.dart';
import 'package:orchestra/models/enums/incoming_data_type.dart';
import 'package:orchestra/models/enums/outgoing_data_type.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/enums/product_category_type.dart';
import '../../models/product/product.dart';
import '../../models/product_models/adress.dart';
import '../../models/product_models/pricing_and_business.dart';
import '../../models/product_models/product_launcher.dart';
import '../../models/user.dart';

class AddLoreFormModel extends ChangeNotifier {
  final formKey = GlobalKey<FormState>();
  final Product? _product;
  late UserModel _currentUser;
  late String _loreLogoURL;
  late String _loreName;
  String? _lorePromoPictureURL;
  String? _loreType;
  ProductCategoryType? _loreCategory;
  late String _loreVersion;
  late String _loreEmail;
  late String _loreUrl;
  late String _loreDescription;
  late String _loreHowItWorks;
  late String _loreVideoDescription;
  String? _loreLauncherFileUrl;
  IncomingDataType? _incomingDataInfo;
  OutgoingDataType? _outgoingDataInfo;
  DataProcessingType? _dataProcessingType;
  DataCollectingType? _dataCollectingType;
  BusinessModelType? _businessModelType;
  num? _price;
  late List<String> _testEndsAfter;
  late List<String> _lorePhotoDescriptionUrls;
  bool _isFreeForTest = true;
  String contributorName = '';

  AddLoreFormModel({Product? product}) : _product = product {
    _currentUser = sl<AuthBloc>().state.currentUser;
    _loreLogoURL = _product != null ? _product!.productLogoURL : '';
    _loreName = _product != null ? _product!.productName : '';
    _lorePromoPictureURL =
        _product != null ? _product!.productPromoPictureURL : null;
    _loreType = _product != null ? _product!.productType : null;
    _loreCategory = _product != null ? _product!.productCategory : null;
    _loreVersion = _product != null ? _product!.productVersion : '';
    _loreEmail = _product != null ? _product!.productEmail : '';
    _loreUrl = _product != null ? _product!.productURL : '';
    _loreDescription = _product != null ? _product!.productDescription : '';
    _loreHowItWorks = _product != null ? _product!.howItWork : '';
    _loreVideoDescription = _product != null ? _product!.videoDescription : '';
    _price = _product != null ? _product!.pricingAndBusiness.price : null;
    _testEndsAfter = _product != null
        ? _product!.pricingAndBusiness.testEndsAfter?.toList() ?? []
        : [];
    _lorePhotoDescriptionUrls =
        _product != null ? _product!.photoDescriptionUrls?.toList() ?? [] : [];
    _incomingDataInfo = _product != null
        ? _product!.productLauncher.incomingDataInfo
        : IncomingDataType.fixedSource;
    _outgoingDataInfo = _product != null
        ? _product!.productLauncher.outgoingDataInfo
        : OutgoingDataType.fixedChanel;
    _dataProcessingType = _product != null
        ? _product!.productLauncher.dataProcessingType
        : DataProcessingType.onPremise;
    _dataCollectingType = _product != null
        ? _product!.productLauncher.dataCollecting
        : DataCollectingType.noDataCollection;
  }

  Product? get product => _product;
  UserModel get currentUser => _currentUser;

  String get loreName => _loreName;
  set loreName(String loreName) {
    _loreName = loreName;
    notifyListeners();
  }

  String get loreLogoURL => _loreLogoURL;
  set loreLogoUrl(String loreLogoUrl) {
    _loreLogoURL = loreLogoUrl;
    notifyListeners();
  }

  String? get lorePromoPictureURL => _lorePromoPictureURL;
  set lorePromoPictureURL(String? lorePromoPictureURL) {
    _lorePromoPictureURL = lorePromoPictureURL;
    notifyListeners();
  }

  String? get loreType => _loreType;
  set loreType(String? loreType) {
    _loreType = loreType;
    notifyListeners();
  }

  ProductCategoryType? get loreCategory => _loreCategory;
  set loreCategory(ProductCategoryType? loreCategory) {
    _loreCategory = loreCategory;
    notifyListeners();
  }

  String get loreVersion => _loreVersion;
  set loreVersion(String loreVersion) {
    _loreVersion = loreVersion;
    notifyListeners();
  }

  String get loreEmail => _loreEmail;
  set loreEmail(String loreEmail) {
    _loreEmail = loreEmail;
    notifyListeners();
  }

  String get loreUrl => _loreUrl;
  set loreUrl(String loreUrl) {
    _loreUrl = loreUrl;
    notifyListeners();
  }

  String get loreDescription => _loreDescription;
  set loreDescription(String loreDescription) {
    _loreDescription = loreDescription;
    notifyListeners();
  }

  String get loreHowItWorks => _loreHowItWorks;
  set loreHowItWorks(String loreHowItWorks) {
    _loreHowItWorks = loreHowItWorks;
    notifyListeners();
  }

  String get loreVideoDescription => _loreVideoDescription;
  set loreVideoDescription(String loreVideoDescription) {
    _loreVideoDescription = loreVideoDescription;
    notifyListeners();
  }

  String? get loreLauncherFileUrl => _loreLauncherFileUrl;
  set loreLauncherFileUrl(String? loreLauncherFileUrl) {
    _loreLauncherFileUrl = loreLauncherFileUrl;
    notifyListeners();
  }

  IncomingDataType? get incomingDataInfo => _incomingDataInfo;
  set incomingDataInfo(IncomingDataType? incomingDataInfo) {
    _incomingDataInfo = incomingDataInfo;
    notifyListeners();
  }

  OutgoingDataType? get outgoingDataInfo => _outgoingDataInfo;
  set outgoingDataInfo(OutgoingDataType? outgoingDataInfo) {
    _outgoingDataInfo = outgoingDataInfo;
    notifyListeners();
  }

  DataProcessingType? get dataProcessingType => _dataProcessingType;
  set dataProcessingType(DataProcessingType? dataProcessingType) {
    _dataProcessingType = dataProcessingType;
    notifyListeners();
  }

  DataCollectingType? get dataCollectingType => _dataCollectingType;
  set dataCollectingType(DataCollectingType? dataCollectingType) {
    _dataCollectingType = dataCollectingType;
    notifyListeners();
  }

  BusinessModelType? get businessModelType => _businessModelType;
  set businessModelType(BusinessModelType? businessModelType) {
    _businessModelType = businessModelType;
    notifyListeners();
  }

  num? get price => _price;
  set price(num? price) {
    _price = price;
    notifyListeners();
  }

  List<String> get lorePhotoDescriptionUrls => _lorePhotoDescriptionUrls;
  set lorePhotoDescriptionUrls(List<String> lorePhotoDescriptionUrls) {
    _lorePhotoDescriptionUrls = lorePhotoDescriptionUrls;
    notifyListeners();
  }

  set addPhotoDescriptionUrl(String lorePhotoDescriptionUrl) {
    _lorePhotoDescriptionUrls.add(lorePhotoDescriptionUrl);
    notifyListeners();
  }

  set deletePhotoDescriptionUrl(int urlListIndex) {
    _lorePhotoDescriptionUrls[urlListIndex] =
        AddLorePageConstants.photoDescriptionUrlDeletedLabel;
    notifyListeners();
  }

  get photoDescriptionUrlLength => _lorePhotoDescriptionUrls
      .where((element) =>
          element != AddLorePageConstants.photoDescriptionUrlDeletedLabel)
      .length;

  List<String> get testEndsAfter => _testEndsAfter;
  set testEndsAfter(List<String> testEndsAfter) {
    _testEndsAfter = testEndsAfter;
    notifyListeners();
  }

  bool get isFreeForTest => _isFreeForTest;
  set isFreeForTest(bool isFreeForTest) {
    _isFreeForTest = isFreeForTest;
    notifyListeners();
  }

  bool get globalValidator {
    var validate = _loreType != null &&
        (_loreCategory != null) &&
        (_lorePromoPictureURL != null) &&
        (_loreLauncherFileUrl != null) &&
        _loreLogoURL.isNotEmpty;

    return validate;
  }

  Address get address => Address(
        addressLine1: '',
        addressLine2: '',
        country: '',
        city: '',
        state: '',
        createDate: DateTime.now().toString(),
      );

  ProductLauncher get loreLauncher => ProductLauncher(
        createDate: DateTime.now().toString(),
        productLauncherFileUrl: _loreLauncherFileUrl,
        incomingDataInfo: _incomingDataInfo,
        outgoingDataInfo: _outgoingDataInfo,
        dataCollecting: _dataCollectingType,
        dataProcessingType: _dataProcessingType,
      );

  PricingAndBusiness get pricingAndBusiness {
    return PricingAndBusiness(
      testEndsAfter: testEndsAfter,
      model: _businessModelType,
      price: _price,
      marketplaceReward: 30,
      freeForTest: _isFreeForTest,
      createDate: DateTime.now().toString(),
    );
  }
}
