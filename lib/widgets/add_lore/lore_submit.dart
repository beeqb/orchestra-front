import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/added_products_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/enums/dialog_type.dart';
import '../../models/enums/product_status_type.dart';
import '../../models/product_models/post_product.dart';
import '../../services/dialogs_service.dart';
import '../../services/product_service.dart';
import 'add_lore_form_model.dart';
import 'add_lore_form_validate_modal.dart';

class LoreSubmit extends StatelessWidget {
  const LoreSubmit({Key? key}) : super(key: key);

  Future<void> submitForm(BuildContext context) async {
    final addLoreModel = context.read<AddLoreFormModel>();
    if (addLoreModel.formKey.currentState!.validate() &&
        addLoreModel.globalValidator) {
      addLoreModel.formKey.currentState!.save();

      addLoreModel.lorePhotoDescriptionUrls.removeWhere((element) =>
          element == AddLorePageConstants.photoDescriptionUrlDeletedLabel);

      addLoreModel.lorePhotoDescriptionUrls
          .removeWhere((element) => element.isEmpty);

      var addProductFinal = PostProduct(
          productLogoURL: addLoreModel.loreLogoURL,
          productPromoPictureURL: addLoreModel.lorePromoPictureURL,
          photoDescriptionUrls: addLoreModel.lorePhotoDescriptionUrls,
          status: ProductStatusType.waitingForReview,
          userId: addLoreModel.currentUser.id!,
          productDescription: addLoreModel.loreDescription,
          productEmail: addLoreModel.loreEmail,
          productLauncher: addLoreModel.loreLauncher,
          productName: addLoreModel.loreName,
          productType: addLoreModel.loreType!,
          productCategory: addLoreModel.loreCategory!,
          productURL: addLoreModel.loreUrl,
          productVersion: addLoreModel.loreVersion,
          howItWork: addLoreModel.loreHowItWorks,
          videoDescription: addLoreModel.loreVideoDescription,
          address: addLoreModel.address,
          pricingAndBusiness: addLoreModel.pricingAndBusiness,
          productContributor: addLoreModel.contributorName,
          dataCollectingType: addLoreModel.dataCollectingType!,
          incomingDataType: addLoreModel.incomingDataInfo!,
          outgoingDataType: addLoreModel.outgoingDataInfo!,
          dataProcessingType: addLoreModel.dataProcessingType!,
          businessModelType: addLoreModel.businessModelType!);
      try {
        await sl<ProductService>().addProduct(newProduct: addProductFinal);

        /// отправка события в аналитику
        // FirebaseAnalytics.instance.logEvent(
        //   name: 'add_widget',
        //   parameters: {
        //     'productContributor': addLoreModel.contributorName,
        //     'productName': 'productName',
        //     'productType': addLoreModel.loreType,
        //     'productCategory': addLoreModel.loreCategory,
        //   },
        // );
        successSnackbar(
            productName: addProductFinal.productName, context: context);
      } on Exception catch (e) {
        errorSnackbar(
            productName: addProductFinal.productName,
            error: e.toString(),
            context: context);
      }
      sl<AddedProductsBloc>().add(GetProductsByIdEvent());
      sl<NavigationBLoC>().add(const ToAddedProductsEvent());
    } else {
      if (!addLoreModel.globalValidator) {
        showValidateButtonSheet(context);
      } else {
        validateSnackbar(context: context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          width: 140,
          height: 48,
          child: ElevatedButton(
            onPressed: () async {
              await submitForm(context);
            },
            child: Text(
              'Save',
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(color: Theme.of(context).backgroundColor),
            ),
          ),
        ),
      ],
    );
  }

  void successSnackbar(
      {required String productName, required BuildContext context}) {
    sl<DialogsService>().showDialog(
        dialogType: DialogType.successSnackbar,
        title: 'Lore $productName added!');
  }

  void successEditSnackbar(
      {required String productName, required BuildContext context}) {
    sl<DialogsService>().showDialog(
        dialogType: DialogType.successSnackbar,
        title: 'Lore $productName edited!');
  }

  void validateSnackbar({required BuildContext context}) {
    sl<DialogsService>().showDialog(
        dialogType: DialogType.errorSnackbar,
        title: 'Please fill in all the fields');
  }

  void errorSnackbar(
      {required String productName,
      required String error,
      required BuildContext context}) {
    sl<DialogsService>().showDialog(
        dialogType: DialogType.errorSnackbar,
        title: 'Lore $productName error $error');
  }

  void showTestFlyDialog() {
    sl<DialogsService>().showDialog(
      dialogType: DialogType.errorDialog,
      title: 'Set TestFly for this Lore',
      description: 'TestFly period should be filled',
    );
  }
}
