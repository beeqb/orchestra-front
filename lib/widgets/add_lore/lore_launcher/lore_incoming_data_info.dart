import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/enums/incoming_data_type.dart';
import '../add_lore_form_model.dart';

class LoreIncomingDataInfo extends StatelessWidget {
  const LoreIncomingDataInfo({Key? key}) : super(key: key);

  List<DropdownMenuItem<IncomingDataType>> get _dropdownIncomingData =>
      IncomingDataType.values
          .map((e) => DropdownMenuItem<IncomingDataType>(
                value: e,
                child: ListTile(
                  title: Text(e.title),
                  subtitle: Text(e.description),
                ),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Incoming data info',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButtonHideUnderline(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButtonFormField<IncomingDataType>(
                    isDense: false,
                    isExpanded: true,
                    value: context.read<AddLoreFormModel>().incomingDataInfo,
                    hint: _dropdownIncomingData.first,
                    borderRadius: BorderRadius.circular(12.0),
                    style: Theme.of(context).textTheme.headlineMedium,
                    dropdownColor: Theme.of(context).dividerColor,
                    icon: Icon(
                      Icons.expand_more,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    items: _dropdownIncomingData,
                    onChanged: (valueNew) {
                      context.read<AddLoreFormModel>().incomingDataInfo =
                          valueNew;
                    },
                    onSaved: (newValue) => context
                        .read<AddLoreFormModel>()
                        .incomingDataInfo = newValue,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'Users can’t choose or change dara sources. In means there no need provider for the Lore',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
