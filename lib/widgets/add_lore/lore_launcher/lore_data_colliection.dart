import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/enums/data_collecting_type.dart';
import '../add_lore_form_model.dart';

class LoreDataCollection extends StatelessWidget {
  const LoreDataCollection({Key? key}) : super(key: key);

  List<DropdownMenuItem<DataCollectingType>> get _dropdownDataCollecting =>
      DataCollectingType.values
          .map((e) => DropdownMenuItem(
                value: e,
                child: ListTile(
                  title: Text(e.title),
                  subtitle: Text(e.description),
                ),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Data collection',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButtonHideUnderline(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButtonFormField<DataCollectingType>(
                    isDense: false,
                    isExpanded: true,
                    hint: _dropdownDataCollecting.first,
                    value: context.read<AddLoreFormModel>().dataCollectingType,
                    borderRadius: BorderRadius.circular(12.0),
                    style: Theme.of(context).textTheme.headlineMedium,
                    dropdownColor: Theme.of(context).dividerColor,
                    icon: Icon(
                      Icons.expand_more,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    items: _dropdownDataCollecting,
                    onChanged: (valueNew) => context
                        .read<AddLoreFormModel>()
                        .dataCollectingType = valueNew,
                    onSaved: (valueNew) => context
                        .read<AddLoreFormModel>()
                        .dataCollectingType = valueNew,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'The Lore and/or provider doesn’t collect an incoming, processed and outdoing data',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
