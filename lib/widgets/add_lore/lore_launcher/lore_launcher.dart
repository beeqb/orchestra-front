import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/upload_image_bloc.dart';
import 'package:orchestra/models/enums/photo_type.dart';

import '../../add_images/add_image_web.dart';
import '../add_lore_form_model.dart';

class LoreLauncher extends StatelessWidget {
  const LoreLauncher({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Lore\'s launcher',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 180,
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                      width: 1.0,
                      color: Theme.of(context).disabledColor,
                    )),
                child: BlocProvider(
                  create: (context) => UploadImageBloc(
                    editProduct: context.read<AddLoreFormModel>().product,
                    fieldName: PhotoUrlType.launcherFileUrl,
                    fileTypes: ['zip'],
                    contributor: context.read<AddLoreFormModel>().currentUser,
                  )..add(ShowCurrentImage()),
                  child: BlocListener<UploadImageBloc, UploadImageState>(
                    listener: (context, state) {
                      if (state is UploadedLauncherFileState) {
                        context.read<AddLoreFormModel>().loreLauncherFileUrl =
                            state.launcherFileName;
                      }
                    },
                    child: const AddImageWidget(
                      fieldName: PhotoUrlType.launcherFileUrl,
                      fileTypes: ['zip'],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'Upload ZIP which launch your Lore',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
