import 'package:flutter/material.dart';

import 'lore_data_colliection.dart';
import 'lore_data_processing_type.dart';
import 'lore_incoming_data_info.dart';
import 'lore_launcher.dart';
import 'lore_outgoing_data_info.dart';

class LoreLauncherSection extends StatelessWidget {
  const LoreLauncherSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Lore’s launcher',
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        const SizedBox(
          height: 32,
        ),
        const LoreLauncher(),
        const SizedBox(
          height: 32,
        ),
        const LoreDataCollection(),
        const SizedBox(
          height: 32,
        ),
        const LoreIncomingDataInfo(),
        const SizedBox(
          height: 32,
        ),
        const LoreOutgoingDataInfo(),
        const SizedBox(
          height: 32,
        ),
        const LoreDataProcessingType(),
        const SizedBox(
          height: 64,
        ),
      ],
    );
  }
}
