import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/enums/data_processing_type.dart';
import '../add_lore_form_model.dart';

class LoreDataProcessingType extends StatelessWidget {
  const LoreDataProcessingType({Key? key}) : super(key: key);

  List<DropdownMenuItem<DataProcessingType>> get _dropdownDataProcessingType =>
      DataProcessingType.values
          .map((e) => DropdownMenuItem(
                value: e,
                child: ListTile(
                  title: Text(e.title),
                  subtitle: Text(e.description),
                ),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Data processing type',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButtonHideUnderline(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButtonFormField<DataProcessingType>(
                    isDense: false,
                    isExpanded: true,
                    hint: _dropdownDataProcessingType.first,
                    value: context.read<AddLoreFormModel>().dataProcessingType,
                    borderRadius: BorderRadius.circular(12.0),
                    style: Theme.of(context).textTheme.headlineMedium,
                    dropdownColor: Theme.of(context).dividerColor,
                    icon: Icon(
                      Icons.expand_more,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    items: _dropdownDataProcessingType,
                    onChanged: (valueNew) => context
                        .read<AddLoreFormModel>()
                        .dataProcessingType = valueNew,
                    onSaved: (valueNew) => context
                        .read<AddLoreFormModel>()
                        .dataProcessingType = valueNew,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'All data os processed locally - on-premise. User data is not transferred to a third part server',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
