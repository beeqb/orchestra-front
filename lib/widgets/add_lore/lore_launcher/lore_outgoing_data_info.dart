import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/enums/outgoing_data_type.dart';
import '../add_lore_form_model.dart';

class LoreOutgoingDataInfo extends StatelessWidget {
  const LoreOutgoingDataInfo({Key? key}) : super(key: key);

  List<DropdownMenuItem<OutgoingDataType>> get _dropdownOutgoingData =>
      OutgoingDataType.values
          .map((e) => DropdownMenuItem(
                value: e,
                child: ListTile(
                  title: Text(e.title),
                  subtitle: Text(e.description),
                ),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Outgoing data info',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DropdownButtonHideUnderline(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButtonFormField<OutgoingDataType>(
                    isDense: false,
                    isExpanded: true,
                    hint: _dropdownOutgoingData.first,
                    value: context.read<AddLoreFormModel>().outgoingDataInfo,
                    borderRadius: BorderRadius.circular(12.0),
                    style: Theme.of(context).textTheme.headlineMedium,
                    dropdownColor: Theme.of(context).dividerColor,
                    icon: Icon(
                      Icons.expand_more,
                      color: Theme.of(context).iconTheme.color,
                    ),
                    items: _dropdownOutgoingData,
                    onChanged: (valueNew) => context
                        .read<AddLoreFormModel>()
                        .outgoingDataInfo = valueNew,
                    onSaved: (newValue) => context
                        .read<AddLoreFormModel>()
                        .outgoingDataInfo = newValue,
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'Users can’t choose or change result streaming service / type. In means there no need result streamer for the Lore',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
