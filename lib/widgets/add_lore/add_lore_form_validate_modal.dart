import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'add_lore_form_model.dart';

void showValidateButtonSheet(BuildContext context) {
  var text = 'Fill in the field';
  final addLoreModel = context.read<AddLoreFormModel>();
  showModalBottomSheet<dynamic>(
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20),
        topRight: Radius.circular(20),
      ),
    ),
    isScrollControlled: true,
    context: context,
    builder: (context) => Wrap(children: [
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (addLoreModel.loreLogoURL.isEmpty)
              const Text(
                'Please add your solution logo (av. formats:'
                ' jpg, png, gif)',
              ),
            if (addLoreModel.loreType == null) Text('$text : Lore Type'),
            if (addLoreModel.loreCategory == null)
              Text('$text : Lore Category'),

            if (addLoreModel.lorePromoPictureURL == null)
              const Text('Add  image to the promo picture'),
            //  if (photoDescriptionUrls.isEmpty)
            //    Text('Add  image to the photo description'),
            if (addLoreModel.loreLauncherFileUrl == null)
              const Text('Upload ZIP which launch your Lore'),
            if (addLoreModel.price == null) const Text('Set lore price'),
            if (addLoreModel.pricingAndBusiness.testEndsAfter == null)
              const SizedBox(
                height: 50,
              ),
          ],
        ),
      ),
    ]),
  );
}
