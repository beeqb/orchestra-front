
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/test_ends_after_bloc.dart';
import 'package:orchestra/core/const.dart';



class LoreTestEndsAfterWidget extends StatelessWidget {
  final Function(List<String> testEndsAfter, BuildContext context) testEndsAfter;
  final bool? freeForTest;
  const LoreTestEndsAfterWidget({
    Key? key,
    required this.testEndsAfter,
    this.freeForTest,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<TestEndsAfterBloc, TestEndsAfterState>(
      listener: (context, state) {
        if (state is TestEndsAfterSet) {
          testEndsAfter(state.testEndsAfter, context);
        }
      },
      builder: (context, state) {
        if (state is TestEndsAfterSet) {
          return Column(
            children: [
              Wrap(
                children: List.generate(
                  kTestEndsAfter.length,
                      (index) => CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    title: Text(
                      kTestEndsAfter.entries.elementAt(index).value,
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    value: state.testEndsAfter.contains(
                        kTestEndsAfter.entries.elementAt(index).key)
                        ? true
                        : false,
                    onChanged: (value) {
                      debugPrint('onChange->$value');
                      BlocProvider.of<TestEndsAfterBloc>(context)
                          .add(SetTestEndsAfter(value: value!, index: index));
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: TextFormField(
                  enabled: false,
                  decoration: InputDecoration(
                    labelStyle: Theme.of(context).textTheme.headlineMedium,
                    labelText: state.description,
                  ),
                ),
              ),
            ],
          );
        }
        return Container();
      },
    );
  }
}
