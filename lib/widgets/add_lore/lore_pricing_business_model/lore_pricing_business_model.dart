import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/bloc/test_ends_after_bloc.dart';
import '../../../models/enums/business_model_type.dart';
import '../add_lore_form_model.dart';
import 'lore_test_ends_after.dart';

class LorePricingBusinessModel extends StatelessWidget {
  const LorePricingBusinessModel({Key? key}) : super(key: key);

  List<DropdownMenuItem<BusinessModelType>> get _dropdownBusiness =>
      BusinessModelType.values
          .map((e) => DropdownMenuItem<BusinessModelType>(
                value: e,
                child: ListTile(
                  title: Text(e.title),
                  subtitle: Text(e.description),
                ),
              ))
          .toList();

  // ignore: use_setters_to_change_properties
  void _setTestEndsAfter(List<String> testEndsAfter, BuildContext context) {
    debugPrint('set test and after');
    debugPrint(testEndsAfter.toString());
    context.read<AddLoreFormModel>().testEndsAfter = testEndsAfter;
//    debugPrint('setTestEndsAfter $_testEndsAfter');
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Business model',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DropdownButtonHideUnderline(
                    child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButtonFormField<BusinessModelType>(
                        isDense: false,
                        isExpanded: true,
                        hint: _dropdownBusiness.first,
                        value:
                            context.read<AddLoreFormModel>().businessModelType,
                        borderRadius: BorderRadius.circular(12.0),
                        style: Theme.of(context).textTheme.headlineMedium,
                        dropdownColor: Theme.of(context).dividerColor,
                        icon: Icon(
                          Icons.expand_more,
                          color: Theme.of(context).iconTheme.color,
                        ),
                        items: _dropdownBusiness,
                        onChanged: (valueNew) {
                          context.read<AddLoreFormModel>().businessModelType =
                              valueNew;
                        },
                        onSaved: (newValue) => context
                            .read<AddLoreFormModel>()
                            .businessModelType = newValue,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Text(
                    'Customer but Lore with no additional costs',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                ],
              ),
            )
          ],
        ),
        const SizedBox(
          height: 32,
        ),
        Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'Price',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextFormField(
                        initialValue:
                            context.read<AddLoreFormModel>().price?.toString(),
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            hintText: '0.00',
                            prefixIconConstraints:
                                const BoxConstraints(minWidth: 0, minHeight: 0),
                            prefixIcon: Padding(
                              padding: const EdgeInsetsDirectional.only(
                                start: 24,
                                end: 12.0,
                              ),
                              child: Text('\$',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headlineMedium
                                      ?.copyWith(
                                        color: Theme.of(context).disabledColor,
                                      )),
                            )),
                        validator: (val) =>
                            val!.isEmpty ? 'Price is required' : null,
                        onSaved: (newValue) {
                          context.read<AddLoreFormModel>().price =
                              num.parse(newValue!);
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              children: [
                const Expanded(flex: 1, child: SizedBox()),
                Expanded(
                  flex: 3,
                  child: Text(
                    'Enter “0.00” if you provide free lore',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 32,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Marketplace reward',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
            Expanded(
              flex: 3,
              child: Text(
                '30% fixed reward. An change no results',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            )
          ],
        ),
        const SizedBox(
          height: 32,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  'Test ends after',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Theme.of(context).disabledColor),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BlocProvider(
                    create: (context) => TestEndsAfterBloc(
                        testEndsAfter:
                            context.read<AddLoreFormModel>().testEndsAfter)
                      ..add(TestEndsAfterInit()),
                    child: LoreTestEndsAfterWidget(
                      freeForTest:
                          context.read<AddLoreFormModel>().isFreeForTest,
                      testEndsAfter: _setTestEndsAfter,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                ],
              ),
            )
          ],
        ),
      ],
    );
  }
}
