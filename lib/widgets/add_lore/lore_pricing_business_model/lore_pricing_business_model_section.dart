import 'package:flutter/material.dart';

import 'lore_pricing_business_model.dart';

class LorePricingBusinessModelSection extends StatelessWidget {
  const LorePricingBusinessModelSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Pricing & Business model',
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        const SizedBox(
          height: 32,
        ),
        const LorePricingBusinessModel(),
        const SizedBox(
          height: 44,
        ),
      ],
    );
  }
}
