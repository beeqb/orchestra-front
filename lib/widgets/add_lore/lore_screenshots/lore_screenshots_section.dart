import 'package:flutter/material.dart';

import 'lore_screenshots.dart';

class LoreScreenshotsSection extends StatelessWidget {
  const LoreScreenshotsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Lore’s Screenshots',
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        const SizedBox(
          height: 32,
        ),
        const LoreScreenshots(),
        const SizedBox(
          height: 64,
        ),
      ],
    );
  }
}
