import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import '../../../blocs/bloc/upload_image_bloc.dart';
import '../../../core/const.dart';
import '../../../models/enums/photo_type.dart';
import '../../add_images/add_image_web.dart';
import '../add_lore_form_model.dart';

class LoreScreenshots extends StatefulWidget {
  const LoreScreenshots({Key? key}) : super(key: key);

  @override
  State<LoreScreenshots> createState() => _LoreScreenshotsState();
}

class _LoreScreenshotsState extends State<LoreScreenshots> {
  PhotoUrlType _getFieldNameType(int index) {
    switch (index) {
      case 0:
        return PhotoUrlType.photoDescUrl_1;
      case 1:
        return PhotoUrlType.photoDescUrl_2;
      case 2:
        return PhotoUrlType.photoDescUrl_3;
      case 3:
        return PhotoUrlType.photoDescUrl_4;
      case 4:
        return PhotoUrlType.photoDescUrl_5;
      case 5:
        return PhotoUrlType.photoDescUrl_6;
      default:
        return PhotoUrlType.photoDescUrl_1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AddLoreFormModel>(
      builder: (context, addLoreModel, _) {
        return ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: addLoreModel.lorePhotoDescriptionUrls.length + 1,
            itemBuilder: (context, index) {
              return BlocProvider(
                  create: (context) => UploadImageBloc(
                        editProduct: addLoreModel.product,
                        fieldName: _getFieldNameType(index),
                        fileTypes: ['jpg', 'png', 'gif'],
                        contributor: addLoreModel.currentUser,
                      )..add(ShowCurrentImage()),
                  child: BlocConsumer<UploadImageBloc, UploadImageState>(
                      listener: (context, state) {
                    if (state is AddedImageState) {
                      addLoreModel.addPhotoDescriptionUrl = state.fileName;
                    }
                    if (state is ImageDeletedState) {
                      addLoreModel.deletePhotoDescriptionUrl =
                          state.fieldName.photoIndex;
                    }
                  }, builder: (context, state) {
                    if (state is ImageDeletedState) {
                      return const SizedBox.shrink();
                    }
                    // Don't show add ScreenShot widget
                    // if non-deleted screenshots number
                    // is already more than AddLore.photoDescriptionUrlMaxNumber
                    if (addLoreModel.photoDescriptionUrlLength >=
                            AddLorePageConstants.photoDescriptionUrlMaxNumber &&
                        index >
                            AddLorePageConstants.photoDescriptionUrlMaxNumber -
                                1) {
                      return const SizedBox.shrink();
                    }
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Text(
                            'Lore’s Screenshots',
                            style: Theme.of(context)
                                .textTheme
                                .headlineMedium
                                ?.copyWith(
                                    color: Theme.of(context).disabledColor),
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 180,

                                //width: double.infinity,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    border: Border.all(
                                      width: 1.0,
                                      color: Theme.of(context).disabledColor,
                                    )),
                                child: AddImageWidget(
                                  //  key: const Key('widgetR'),
                                  fieldName: _getFieldNameType(index),
                                  fileTypes: const ['jpg', 'png', 'gif'],
                                ),
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Text(
                                'Please add  Screenshot your Lore (PNG or JPEG)',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium
                                    ?.copyWith(
                                        color: Theme.of(context).disabledColor),
                              ),
                              Text(
                                '1024 px by 768 px',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineMedium
                                    ?.copyWith(
                                        color: Theme.of(context).disabledColor),
                              ),
                            ],
                          ),
                        )
                      ],
                    );
                  }));
            });
      },
    );
  }
}
