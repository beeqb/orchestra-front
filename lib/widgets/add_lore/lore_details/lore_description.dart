import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../add_lore_form_model.dart';

class LoreDescription extends StatelessWidget {
  const LoreDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Lore’s Description',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                initialValue: context.read<AddLoreFormModel>().loreDescription,
                maxLines: 10,
                validator: (val) =>
                    val!.isEmpty ? 'Description is required' : null,
                onSaved: (newValue) {
                  context.read<AddLoreFormModel>().loreDescription =
                      newValue ?? '';
                },
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                'Describe your solution, please. Marketing text as welcome',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        )
      ],
    );
  }
}
