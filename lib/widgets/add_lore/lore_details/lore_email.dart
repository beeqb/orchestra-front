import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../util/text_util.dart';
import '../add_lore_form_model.dart';

class LoreEmail extends StatelessWidget {
  const LoreEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Lore\'s email',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      initialValue: context.read<AddLoreFormModel>().loreEmail,
                      validator: (val) => isValidEmail(val!)
                          ? null
                          : 'Please enter a valid email address',
                      onSaved: (newValue) {
                        context.read<AddLoreFormModel>().loreEmail =
                            newValue ?? '';
                      },
                    ),
                  ],
                ))
          ],
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          children: [
            const Expanded(flex: 1, child: SizedBox()),
            Expanded(
              flex: 3,
              child: Text(
                'We’ll never share your email with wnyone else',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
