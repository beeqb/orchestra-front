import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../models/enums/product_category_type.dart';
import '../add_lore_form_model.dart';

class LoreCategory extends StatelessWidget {
  const LoreCategory({Key? key}) : super(key: key);

  List<DropdownMenuItem<ProductCategoryType>> get _dropdownProductCategory =>
      ProductCategoryType.values
          .map((e) => DropdownMenuItem<ProductCategoryType>(
                value: e,
                child: Text(e.title),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Lore\'s Category',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
          flex: 3,
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButtonFormField<ProductCategoryType>(
                value: context.read<AddLoreFormModel>().loreCategory,
                isExpanded: true,
                hint: Text(
                  'Please select from list',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                style: Theme.of(context).textTheme.headlineMedium,
                dropdownColor: Theme.of(context).dividerColor,
                borderRadius: BorderRadius.circular(12.0),
                icon: Icon(
                  Icons.expand_more,
                  color: Theme.of(context).iconTheme.color,
                ),
                items: _dropdownProductCategory,
                onChanged: (valueNew) {
                  context.read<AddLoreFormModel>().loreCategory = valueNew;
                },
              ),
            ),
          ),
        )
      ],
    );
  }
}
