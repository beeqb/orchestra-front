import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/upload_image_bloc.dart';
import 'package:orchestra/models/enums/photo_type.dart';

import '../../add_images/add_image_web.dart';
import '../add_lore_form_model.dart';

class LorePromoPicture extends StatelessWidget {
  const LorePromoPicture({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Lore\'s promo picture',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 180,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          width: 1.0,
                          color: Theme.of(context).disabledColor,
                        )),
                    child: BlocProvider(
                      create: (context) => UploadImageBloc(
                        editProduct: context.read<AddLoreFormModel>().product,
                        fieldName: PhotoUrlType.promoUrl,
                        fileTypes: ['jpg', 'png', 'gif'],
                        contributor:
                            context.read<AddLoreFormModel>().currentUser,
                      )..add(ShowCurrentImage()),
                      child: BlocListener<UploadImageBloc, UploadImageState>(
                        listener: (context, state) {
                          if (state is AddedImageState) {
                            context
                                .read<AddLoreFormModel>()
                                .lorePromoPictureURL = state.fileName;
                          }
                        },
                        child: const AddImageWidget(
                          fieldName: PhotoUrlType.promoUrl,
                          fileTypes: ['jpg', 'png', 'gif'],
                        ),
                      ),
                    )),
                const SizedBox(
                  height: 15,
                ),
                Text(
                  'Please add your promo picture (PNG or JPEG)',
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Theme.of(context).disabledColor),
                ),
                Text(
                  '824 px by 400 px',
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall
                      ?.copyWith(color: Theme.of(context).disabledColor),
                ),
              ],
            ))
      ],
    );
  }
}
