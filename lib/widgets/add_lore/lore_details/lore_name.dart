import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../add_lore_form_model.dart';

class LoreName extends StatelessWidget {
  const LoreName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Lore\'s name',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      initialValue: context.read<AddLoreFormModel>().loreName,
                      validator: (val) =>
                          val!.isEmpty ? 'Name is required' : null,
                      onSaved: (newValue) {
                        context.read<AddLoreFormModel>().loreName =
                            newValue ?? '';
                      },
                    ),
                  ],
                ))
          ],
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          children: [
            const Expanded(flex: 1, child: SizedBox()),
            Expanded(
              flex: 3,
              child: Text(
                'Please enter name of your :pre, ex. “Next Birthday Predictor”',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
