import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/const.dart';
import '../add_lore_form_model.dart';

class LoreType extends StatelessWidget {
  const LoreType({Key? key}) : super(key: key);

  List<DropdownMenuItem<String>> get _dropdownProductTypes =>
      ProductTypeAliases.kProductType.entries
          .map((e) => DropdownMenuItem<String>(
                value: e.key,
                child: Text(e.value),
              ))
          .toList();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          flex: 1,
          child: Text(
            'Lore\'s Type',
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        ),
        Expanded(
            flex: 3,
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButtonFormField<String>(
                  value: context.read<AddLoreFormModel>().loreType,
                  isExpanded: true,
                  hint: Text(
                    'Please select from list',
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  style: Theme.of(context).textTheme.headlineMedium,
                  dropdownColor: Theme.of(context).dividerColor,
                  borderRadius: BorderRadius.circular(12.0),
                  icon: Icon(
                    Icons.expand_more,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  items: _dropdownProductTypes,
                  onChanged: (valueNew) {
                    context.read<AddLoreFormModel>().loreType = valueNew ?? '';
                  },
                ),
              ),
            ))
      ],
    );
  }
}
