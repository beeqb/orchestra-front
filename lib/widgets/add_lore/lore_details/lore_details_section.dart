import 'package:flutter/material.dart';

import '../loreUrl.dart';
import 'lore_category.dart';
import 'lore_description.dart';
import 'lore_email.dart';
import 'lore_how_it_works.dart';
import 'lore_logo.dart';
import 'lore_name.dart';
import 'lore_promo_picture.dart';
import 'lore_type.dart';
import 'lore_version.dart';
import 'lore_video_description.dart';

class LoreDetailsSection extends StatelessWidget {
  const LoreDetailsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Lore details',
          style: Theme.of(context).textTheme.headlineLarge,
        ),
        const SizedBox(
          height: 40,
        ),
        const LoreLogo(),
        const SizedBox(
          height: 32,
        ),
        const LoreName(),
        const SizedBox(
          height: 32,
        ),
        const LorePromoPicture(),
        const SizedBox(
          height: 32,
        ),
        const LoreType(),
        const SizedBox(
          height: 32,
        ),
        const LoreCategory(),
        const SizedBox(
          height: 32,
        ),
        const LoreVersion(),
        const SizedBox(
          height: 32,
        ),
        const LoreEmail(),
        const SizedBox(
          height: 32,
        ),
        const LoreUrl(),
        const SizedBox(
          height: 32,
        ),
        const LoreDescription(),
        const SizedBox(
          height: 32,
        ),
        const LoreHowItWorks(),
        const SizedBox(
          height: 32,
        ),
        const LoreVideoDescription(),
        const SizedBox(
          height: 64,
        ),
      ],
    );
  }
}
