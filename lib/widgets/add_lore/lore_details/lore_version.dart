import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../add_lore_form_model.dart';

class LoreVersion extends StatelessWidget {
  const LoreVersion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                'Lore\'s Version',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
            Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      initialValue:
                          context.read<AddLoreFormModel>().loreVersion,
                      validator: (val) =>
                          val!.isEmpty ? 'Version is required' : null,
                      onSaved: (newValue) {
                        context.read<AddLoreFormModel>().loreVersion =
                            newValue ?? '';
                      },
                    ),
                  ],
                ))
          ],
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          children: [
            const Expanded(flex: 1, child: SizedBox()),
            Expanded(
              flex: 3,
              child: Text(
                'Please use letters and/or numbers',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
