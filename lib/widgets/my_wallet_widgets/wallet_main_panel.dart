import 'package:flutter/material.dart';
import 'package:orchestra/view_models/user_view_model.dart';

import '../../blocs/cubits/checkout_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/user.dart';
import '../../util/text_util.dart';
import '../bottom_sheets/bottom_deposit_sheet.dart';
import '../bottom_sheets/bottom_transfer_sheet.dart';
import '../bottom_sheets/bottom_withdraw_sheet.dart';

class DesktopWalletMainPanel extends StatelessWidget {
  final UserModel currentUser;

  const DesktopWalletMainPanel({Key? key, required this.currentUser})
      : super(key: key);

  void _showBottomSheet(
      {required bool isScrollControlled,
      required BuildContext context,
      required Widget child}) {
    showModalBottomSheet(
      useRootNavigator: true,
      backgroundColor: Colors.transparent,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      isScrollControlled: isScrollControlled,
      context: context,
      builder: (context) => child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    final userViewModel = UserViewModel(currentUser);
    return Container(
      height: 312,
      padding: const EdgeInsets.all(32),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            formatCurrency.format(currentUser.balance),
            style: Theme.of(context)
                .textTheme
                .displayLarge
                ?.copyWith(fontSize: 40),
          ),
          Text('Total balance',
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                    color: Theme.of(context).iconTheme.color,
                  )),
          const SizedBox(
            height: 24,
          ),
          Row(
            children: [
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 16,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: const Color(0x3324df9b),
                        ),
                        child: const Icon(Icons.arrow_upward_sharp),
                      ),
                      const SizedBox(width: 20),
                      Text(
                        "Income: ${userViewModel.incomeAmount}",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 16,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100),
                          color: const Color(0x33df2323),
                        ),
                        child: const Icon(Icons.arrow_downward_sharp),
                      ),
                      const SizedBox(width: 20),
                      Text(
                        "Spents: ${userViewModel.spendAmount}",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Row(
            children: [
              SizedBox(
                width: 109,
                height: 48,
                child: ElevatedButton(
                    onPressed: () => _showBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        child: isMobile
                            ? BottomDepositSheet(cubit: sl<CheckoutCubit>())
                            : const DepositPopup()),
                    child: Text(
                      'Deposit',
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium
                          ?.copyWith(color: Theme.of(context).backgroundColor),
                    )),
              ),
              const SizedBox(
                width: 8,
              ),
              SizedBox(
                height: 48,
                width: 124,
                child: ElevatedButton(
                  onPressed: () => _showBottomSheet(
                    context: context,
                    child: isMobile
                        ? const BottomWithdrawSheet()
                        : const WithdrawPopup(),
                    isScrollControlled: true,
                  ),
                  style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).cardColor)),
                  child: Text('Withdraw',
                      style: Theme.of(context).textTheme.headlineMedium),
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              SizedBox(
                height: 48,
                width: 124,
                child: ElevatedButton(
                  onPressed: () => isMobile
                      ? _showBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          child: const BottomTransferSheet(),
                        )
                      : showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              const MoneyTransferPopup()),
                  style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).cardColor)),
                  child: Text('Transfer',
                      style: Theme.of(context).textTheme.headlineMedium),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class MobilWalletMainPanel extends StatelessWidget {
  final UserModel currentUser;

  const MobilWalletMainPanel({Key? key, required this.currentUser})
      : super(key: key);

  void _showBottomSheet(
      {required bool isScrollControlled,
      required BuildContext context,
      required Widget child}) {
    showModalBottomSheet(
      useRootNavigator: true,
      backgroundColor: Colors.transparent,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      isScrollControlled: isScrollControlled,
      context: context,
      builder: (context) => child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final userViewModel = UserViewModel(currentUser);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(formatCurrency.format(currentUser.balance),
            style: Theme.of(context).textTheme.displayMedium),
        Text('Total balance',
            style: Theme.of(context).textTheme.labelSmall?.copyWith(
                  color: Theme.of(context).iconTheme.color,
                )),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: 79,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 8,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: const Color(0x3324df9b),
                      ),
                      child: const Icon(Icons.arrow_upward_sharp),
                    ),
                    SizedBox(height: 12),
                    Text(
                      "Income: ${userViewModel.incomeAmount}",
                      style: Theme.of(context).textTheme.labelSmall,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Container(
                height: 79,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Theme.of(context).secondaryHeaderColor,
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 8,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 32,
                      height: 32,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: const Color(0x33df2323),
                      ),
                      child: const Icon(Icons.arrow_downward_sharp),
                    ),
                    SizedBox(height: 12),
                    Text(
                      "Spents:  ${userViewModel.spendAmount}",
                      style: Theme.of(context).textTheme.labelSmall,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: SizedBox(
                height: 40,
                child: ElevatedButton(
                    onPressed: () => _showBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        child: BottomDepositSheet(cubit: sl<CheckoutCubit>())),
                    child: Text(
                      'Deposit',
                      style: Theme.of(context)
                          .textTheme
                          .labelSmall
                          ?.copyWith(color: Theme.of(context).backgroundColor),
                    )),
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Expanded(
              child: SizedBox(
                height: 40,
                child: ElevatedButton(
                  onPressed: () => _showBottomSheet(
                    context: context,
                    child: const BottomWithdrawSheet(),
                    isScrollControlled: true,
                  ),
                  child: Text(
                    'Withdraw',
                    style: Theme.of(context).textTheme.labelSmall,
                    overflow: TextOverflow.ellipsis,
                  ),
                  style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).cardColor)),
                ),
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Expanded(
              child: SizedBox(
                height: 40,
                child: ElevatedButton(
                  onPressed: () => _showBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    child: const BottomTransferSheet(),
                  ),
                  child: Text('Transfer',
                      style: Theme.of(context).textTheme.labelSmall),
                  style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
                      backgroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).cardColor)),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
