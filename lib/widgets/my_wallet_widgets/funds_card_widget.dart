import 'package:flutter/material.dart';

import '../../core/app_colors.dart';
import '../../core/const.dart';
import '../../models/user.dart';
import '../../util/text_util.dart';

class FundsCardWidget extends StatefulWidget {
  const FundsCardWidget({
    Key? key,
    required this.user,
    required this.title,
    required this.amount,
    required this.color,
    required this.setHight,
    required this.width,
  }) : super(key: key);

  final UserModel user;
  final String title;
  final double amount;
  final Color color;
  final VoidCallback setHight;
  final double width;

  @override
  _FundsCardWidgetState createState() => _FundsCardWidgetState();
}

class _FundsCardWidgetState extends State<FundsCardWidget> {
  final double sizeIcon = 30;
  final double _iconPosition = 15;
  double? _height, _width, _inkwRadius;
  bool _isVisibleDetails = false;

  @override
  void initState() {
    _height = 150;
    _width = widget.width;
    _inkwRadius = 20;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final profit = (widget.user.stats != null) ? widget.user.stats!.profit : 0;
    final purchases =
        (widget.user.stats != null) ? widget.user.stats!.purchases : 0;
    final costs = (widget.user.stats != null) ? widget.user.stats!.costs : 0;
    final maxWidth = MediaQuery.of(context).size.width;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: AnimatedContainer(
            onEnd: () {
              if (_height == 200) {
                _isVisibleDetails = true;
              } else {
                _isVisibleDetails = false;
              }
            },
            duration: const Duration(milliseconds: 300),
            decoration: BoxDecoration(
              color: Theme.of(context).dialogBackgroundColor,
              borderRadius: BorderRadius.circular(25),
            ),
            height: _height,
            width: _width,
            child: Stack(
              children: [
                InkWell(
                  borderRadius: BorderRadius.circular(_inkwRadius!),
                  onTap: () => _height == 150 ? updateWidget(maxWidth) : null,
                  child: Container(
                    width: _height! > 150 ? (_width! - 80) : _width,
                    height: _height,
                    decoration: BoxDecoration(
                      color: Theme.of(context).bottomAppBarColor,
                      borderRadius: BorderRadius.circular(_inkwRadius!),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  widget.title == 'Income'
                                      ? const Icon(
                                          Icons.arrow_upward,
                                          size: 30,
                                          color: Colors.green,
                                        )
                                      : const Icon(
                                          Icons.arrow_downward,
                                          size: 30,
                                          color: Colors.red,
                                        ),
                                  Text(
                                    widget.title,
                                    style:
                                        Theme.of(context).textTheme.headline5,
                                  ),
                                  Text(
                                    formatCurrency.format(widget.amount),
                                    style: TextStyle(
                                      color: widget.color,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          _isVisibleDetails
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 3),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        widget.title == 'Income'
                                            ? 'Member profit'
                                            : 'App Purchases',
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      Text(
                                        formatCurrency.format(
                                          widget.title == 'Income'
                                              ? profit
                                              : purchases,
                                        ),
                                        style: const TextStyle(
                                          color: Colors.orange,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : const SizedBox(height: 1),
                          _isVisibleDetails
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 3),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        widget.title == 'Income'
                                            ? 'Deposits'
                                            : 'Running costs',
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      Text(
                                        formatCurrency.format(
                                          widget.title == 'Income'
                                              ? widget.user.deposited
                                              : costs,
                                        ),
                                        style: const TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : const SizedBox(height: 1),
                          _isVisibleDetails
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 3),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        widget.title == 'Income'
                                            ? 'Recieved'
                                            : 'Sended',
                                        style: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      Text(
                                        formatCurrency.format(
                                          widget.title == 'Income'
                                              ? widget.user.received
                                              : widget.user.sended,
                                        ),
                                        style: const TextStyle(
                                          color: AppColors.blueColor,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : const SizedBox(height: 1),
                        ],
                      ),
                    ),
                  ),
                ),
                _isVisibleDetails
                    ? Positioned(
                        right: _iconPosition,
                        top: _height! / 2 - sizeIcon / 2,
                        child: InkWell(
                          onTap: () =>
                              _height == 200 ? updateWidget(maxWidth) : null,
                          child: Icon(
                            Icons.close,
                            size: sizeIcon,
                          ),
                        ))
                    : const SizedBox(
                        height: 1,
                      )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void updateWidget(double maxWidth) {
    setState(() {
      if (_height == 150) {
        _height = 200;
        _width = maxWidth < ApplicationsPageConstants.maxWidth
            ? (maxWidth - _iconPosition * 2)
            : ApplicationsPageConstants.maxWidth;
      } else {
        _height = 150;
        _width = widget.width;
        _isVisibleDetails = false;
      }

      return widget.setHight();
    });
  }
}
