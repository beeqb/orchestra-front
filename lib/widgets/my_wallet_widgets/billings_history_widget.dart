import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/billings_bloc.dart';

import '../../core/const.dart';
import '../../models/billing_models/transaction_model.dart';
import '../../util/text_util.dart';
import '../loading.dart';

class TransactionsHistory extends StatelessWidget {
  const TransactionsHistory({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ScrollController controller;

  int calculateListItemCount(BillingState state) {
    if (state is BillingsLoaded) {
      return state.transactions.length;
    } else {
      // + 1 for the loading indicator
      return state.transactions.length + 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return BlocBuilder<BillingsBloc, BillingState>(
      builder: (context, state) {
        if (state.transactions.isEmpty) {
          return const Center(
            child: Loading(),
          );
        } else {
          return isMobile
              ? ListView.builder(
                  //  controller: widget.controller,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: calculateListItemCount(state),
                  itemBuilder: (context, index) {
                    return index >= state.transactions.length
                        ? SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                              color: Theme.of(context).primaryIconTheme.color,
                            ))
                        : _MobileTransactionItem(
                            transactionModel: state.transactions[index],
                          );
                  },
                )
              : ListView.builder(
                  //  controller: widget.controller,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: calculateListItemCount(state),
                  itemBuilder: (context, index) {
                    final color = index.isEven
                        ? Theme.of(context).backgroundColor
                        : const Color(0xFF1E1E25);
                    if (index == 0) {
                      return const _DesktopTransactionsHeader();
                    }
                    return index >= state.transactions.length
                        ? SizedBox(
                            width: 20,
                            height: 20,
                            child: Center(
                              child: CircularProgressIndicator(
                                strokeWidth: 1,
                                color: Theme.of(context).primaryIconTheme.color,
                              ),
                            ))
                        : _DesktopTransactionItem(
                            transactionModel: state.transactions[index - 1],
                            color: color,
                          );
                  },
                );
        }
      },
    );
  }
}

class _DesktopTransactionItem extends StatelessWidget {
  final TransactionModel transactionModel;
  final Color color;

  const _DesktopTransactionItem(
      {Key? key, required this.transactionModel, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 66,
      color: color,
      padding: const EdgeInsets.only(left: 24),
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Text(
                dateFormatWithTime.format(
                  DateTime.parse(transactionModel.createdAt),
                ),
                style: Theme.of(context).textTheme.headlineMedium,
              )),
          Expanded(
              flex: 2,
              child: Text(
                transactionModel.meta.type,
                style: Theme.of(context).textTheme.headlineMedium,
              )),
          Expanded(
            flex: 2,
            child: Text(
              formatCurrency2Digits.format(
                transactionModel.amount,
              ),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              formatCurrency2Digits.format(transactionModel.meta.total),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
        ],
      ),
    );
  }
}

class _MobileTransactionItem extends StatelessWidget {
  final TransactionModel transactionModel;

  const _MobileTransactionItem({Key? key, required this.transactionModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 36,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                transactionModel.meta.type,
                style: Theme.of(context).textTheme.labelSmall,
              ),
              Text(
                formatCurrency2Digits.format(transactionModel.meta.total),
                style: Theme.of(context).textTheme.labelSmall,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                dateFormatWithTime.format(
                  DateTime.parse(transactionModel.createdAt),
                ),
                style: Theme.of(context)
                    .textTheme
                    .labelSmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
              Text(
                formatCurrency2Digits.format(
                  transactionModel.amount,
                ),
                style: Theme.of(context)
                    .textTheme
                    .labelSmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _DesktopTransactionsHeader extends StatelessWidget {
  const _DesktopTransactionsHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 66,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      padding: const EdgeInsets.only(left: 24),
      child: Row(children: [
        Expanded(
          flex: 2,
          child: Text(
            "Date",
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).iconTheme.color),
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            "Type",
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).iconTheme.color),
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            "Amount",
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).iconTheme.color),
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            "Total balance",
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).iconTheme.color),
          ),
        )
      ]),
    );
  }
}
