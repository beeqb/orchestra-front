import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../core/const.dart';

class CategoryLabel extends StatelessWidget {
  final ProductCategoryType? appCategory;
  final Function(ProductCategoryType? appCategory) onCategoryPressed;
  final bool isSelected;

  const CategoryLabel(
      {Key? key,
      this.appCategory,
      required this.onCategoryPressed,
      required this.isSelected})
      : super(key: key);

  String get _appCategoryTitle =>
      appCategory != null ? appCategory!.title : 'All';

  TextStyle? labelStyle(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    if (isSelected) {
      if (isMobile) {
        return Theme.of(context).textTheme.labelSmall;
      } else {
        return Theme.of(context).textTheme.labelMedium;
      }
    } else {
      if (isMobile) {
        return Theme.of(context)
            .textTheme
            .labelSmall
            ?.copyWith(color: Theme.of(context).disabledColor);
      } else {
        return Theme.of(context)
            .textTheme
            .labelMedium
            ?.copyWith(color: Theme.of(context).disabledColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: isSelected
            ? Theme.of(context).cardColor
            : Theme.of(context).backgroundColor,
      ),
      child: TextButton(
          onPressed: () => onCategoryPressed(appCategory),
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
            ),
            child: Center(
                child: Text(_appCategoryTitle, style: labelStyle(context))),
          )),
    );
  }
}
