import 'dart:math' as math show pi;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:orchestra/blocs/cubits/navigation_menu_cubit.dart';
import 'package:orchestra/core/theme_extensions.dart';
import 'package:wiredash/wiredash.dart';

import '../../../blocs/bloc/auth_bloc.dart';
import '../../../blocs/bloc/navigation_bloc.dart';
import '../../../blocs/bloc/purchased_bloc.dart';
import '../../../blocs/cubits/products_cubit.dart';
import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../../models/enums/user_type.dart';
import '../navigation_items.dart';
import 'collapsible_item.dart';
import 'collapsible_item_selection.dart';
import 'collapsible_item_widget.dart';

class CollapsibleSidebar extends StatefulWidget {
  const CollapsibleSidebar({
    Key? key,
    required this.body,
    required this.isHidden,
    this.isCollapsed = true,
  }) : super(key: key);
  final bool isHidden;
  final Widget body;
  final bool isCollapsed;

  @override
  _CollapsibleSidebarState createState() => _CollapsibleSidebarState();
}

class _CollapsibleSidebarState extends State<CollapsibleSidebar>
    with SingleTickerProviderStateMixin {
  static const double barHeight = double.infinity,
      iconSize = 24,
      verticalPadding = 12,
      iconTitleSpacing = 20;

  late AnimationController _controller;
  late Animation<double> _widthAnimation;
  late CurvedAnimation _curvedAnimation;
  late bool _isCollapsed;
  late double _currWidth;
  late double padding;
  late List<CollapsibleItemWidget> navigationItemWidgets;

  double get _scale =>
      (_currWidth - SidebarConstants.minWidth) /
      (SidebarConstants.maxWidth - SidebarConstants.minWidth);
  double get _currAngle => math.pi * _scale;
  double get _itemTitleOffsetX => _itemTitleMaxOffsetX * _scale;
  double get _itemTitleMaxOffsetX => iconTitleSpacing + iconSize;
  double get _itemSelectionOffsetY => iconSize + verticalPadding * 2;
  double get _itemHorizontalPadding => _isCollapsed ? 12 : 12;
  double get _logoHorizontalPadding => _isCollapsed ? 16 : 34;

  @override
  void initState() {
    super.initState();
    _currWidth = SidebarConstants.minWidth;

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    _curvedAnimation = CurvedAnimation(
      parent: _controller,
      curve: Curves.fastLinearToSlowEaseIn,
    );

    _controller.addListener(() {
      _currWidth = _widthAnimation.value;
      if (_controller.isCompleted) {
        _isCollapsed = _currWidth == SidebarConstants.minWidth;
      }
      setState(() {});
    });
    _listenCollapseChange();
  }

  @override
  void didUpdateWidget(covariant CollapsibleSidebar oldWidget) {
    if (widget.isCollapsed != oldWidget.isCollapsed) {
      _listenCollapseChange();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return widget.isHidden
        ? widget.body
        : Stack(
            alignment: Alignment.topLeft,
            children: [
              AnimatedPadding(
                duration: const Duration(milliseconds: 100),
                curve: Curves.easeInOut,
                padding: EdgeInsets.only(left: _currWidth),
                child: widget.body,
              ),
              GestureDetector(
                child: Container(
                  height: barHeight,
                  width: _currWidth,
                  decoration: BoxDecoration(
                      color: Theme.of(context)
                          .extension<SidebarColors>()
                          ?.backgroundColor,
                      border: Border(
                        right: BorderSide(
                          color: Theme.of(context).dividerColor,
                          width: 1.0,
                        ),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AnimatedPadding(
                        duration: const Duration(milliseconds: 100),
                        padding: EdgeInsets.symmetric(
                            horizontal: _logoHorizontalPadding, vertical: 16),
                        child: _logoWidget,
                      ),
                      const SizedBox(
                        height: 44,
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          physics: const BouncingScrollPhysics(),
                          child: AnimatedPadding(
                            padding: EdgeInsets.symmetric(
                                horizontal: _itemHorizontalPadding),
                            duration: const Duration(milliseconds: 100),
                            child: Stack(
                              children: [
                                CollapsibleItemSelection(
                                  height: _itemSelectionOffsetY,
                                  offsetY: _itemSelectionOffsetY *
                                      sl<NavigationMenuCubit>().state,
                                  color: Theme.of(context)
                                      .extension<SidebarColors>()
                                      ?.selectedBoxColor,
                                ),
                                Column(children: [
                                  ..._navigationItemWidgets,
                                  Divider(
                                    color: Theme.of(context).dividerColor,
                                    indent: 5,
                                    endIndent: 5,
                                    thickness: 1,
                                  ),
                                  _bugReportWidget,
                                ]),
                              ],
                            ),
                          ),
                        ),
                      ),
                      AnimatedPadding(
                        duration: const Duration(milliseconds: 100),
                        padding: EdgeInsets.symmetric(
                            horizontal: _itemHorizontalPadding),
                        child: _toggleButtonWidget,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
  }

  void _listenCollapseChange() {
    _isCollapsed = widget.isCollapsed;
    var endWidth =
        _isCollapsed ? SidebarConstants.minWidth : SidebarConstants.maxWidth;

    _animateTo(endWidth);
  }

  void _animateTo(double endWidth) {
    _widthAnimation = Tween<double>(
      begin: _currWidth,
      end: endWidth,
    ).animate(_curvedAnimation);
    _controller.reset();
    _controller.forward();
  }

  Widget get _logoWidget {
    const double iconTitleSpacing = 16;
    return CollapsibleItemWidget(
      onHoverPointer: SystemMouseCursors.basic,
      titleOffsetX: SidebarConstants.logoSize + iconTitleSpacing,
      scale: _scale,
      leading: SvgPicture.asset(
        'assets/icons/orchestra_logo.svg',
        width: SidebarConstants.logoSize,
        height: SidebarConstants.logoSize,
      ),
      title: 'Orchestra',
      textStyle: Theme.of(context)
          .textTheme
          .headlineMedium
          ?.copyWith(fontFamily: "Gilroy"),
    );
  }

  List<CollapsibleItem> get sideBarNavigationItems {
    final items = <CollapsibleItem>[
      CollapsibleItem(
          text: 'Marketplace',
          asset: 'assets/icons/navigation/marketplace.svg',
          onPressed: () {
            NavigationItems.onSidebarNavigationItemPressed(
              context,
              const ToMarketplaceNavigationEvent(),
              false,
            );
            sl<ProductsCubit>().getProducts();
          },
          isSelected: sl<NavigationMenuCubit>().state == 0),
      CollapsibleItem(
          text: 'My lore',
          asset: 'assets/icons/navigation/my_lores.svg',
          onPressed: () {
            NavigationItems.onSidebarNavigationItemPressed(
              context,
              const ToMyWidgetsNavigationEvent(),
              true,
            );
            sl<PurchasedBloc>().add(LoadPurchasedProductsEvent());
          },
          isSelected: sl<NavigationMenuCubit>().state == 1),
      CollapsibleItem(
          text: 'Applications',
          asset: 'assets/icons/navigation/application.svg',
          onPressed: () => NavigationItems.onSidebarNavigationItemPressed(
                context,
                const ToDashboardNavigationEvent(),
                true,
              ),
          isSelected: sl<NavigationMenuCubit>().state == 2),
      CollapsibleItem(
          text: 'My wallet',
          asset: 'assets/icons/navigation/my_vallet.svg',
          onPressed: () => NavigationItems.onSidebarNavigationItemPressed(
                context,
                const ToBillingsNavigationEvent(),
                true,
              ),
          isSelected: sl<NavigationMenuCubit>().state == 3),
    ];
    if (sl<AuthBloc>().state.currentUser.type == UserType.admin) {
      items.add(adminCollapsibleItem);
    }
    return items;
  }

  CollapsibleItem get adminCollapsibleItem {
    return CollapsibleItem(
        text: 'Admin',
        asset: 'assets/icons/navigation/admin.svg',
        onPressed: () => NavigationItems.onSidebarNavigationItemPressed(
              context,
              const ToAdminNavigationEvent(),
              true,
            ),
        isSelected: sl<NavigationMenuCubit>().state == 4);
  }

  List<CollapsibleItemWidget> get _navigationItemWidgets {
    final items = sideBarNavigationItems;
    return List.generate(items.length, (index) {
      var item = items[index];
      final sidebarColors = Theme.of(context).extension<SidebarColors>();
      var iconColor = sidebarColors?.unselectedIconColor;
      var textColor = sidebarColors?.unselectedTextColor;
      if (item.isSelected) {
        iconColor = sidebarColors?.selectedIconColor;
        textColor = sidebarColors?.selectedTextColor;
      }
      return CollapsibleItemWidget(
        horizontalPadding: 12,
        verticalPadding: 12,
        titleOffsetX: _itemTitleOffsetX,
        height: _itemSelectionOffsetY,
        scale: _scale,
        leading: SvgPicture.asset(
          item.asset,
          color: iconColor,
        ),
        title: item.text,
        textStyle: _itemTitleTextStyle(textColor),
        onTap: () {
          sl<NavigationMenuCubit>().chooseNavItem(index);
          item.onPressed();
        },
      );
    });
  }

  Widget get _bugReportWidget {
    final sidebarColors = Theme.of(context).extension<SidebarColors>();
    final textColor = sidebarColors?.unselectedTextColor;
    final iconColor = sidebarColors?.unselectedIconColor;
    final bugReportItem = CollapsibleItem(
        text: 'Bug report',
        asset: 'assets/icons/navigation/bug_report.svg',
        onPressed: () => Wiredash.of(context).show());
    return CollapsibleItemWidget(
      leading: SvgPicture.asset(
        bugReportItem.asset,
        color: iconColor,
      ),
      title: bugReportItem.text,
      horizontalPadding: 12,
      titleOffsetX: _itemTitleOffsetX,
      height: _itemSelectionOffsetY,
      scale: _scale,
      textStyle: _itemTitleTextStyle(textColor),
      onTap: () => bugReportItem.onPressed(),
    );
  }

  Widget get _toggleButtonWidget {
    final sidebarColors = Theme.of(context).extension<SidebarColors>();
    final textColor = sidebarColors?.unselectedTextColor;
    final iconColor = sidebarColors?.unselectedIconColor;
    return CollapsibleItemWidget(
      horizontalPadding: 12,
      titleOffsetX: _itemTitleOffsetX,
      height: _itemSelectionOffsetY,
      scale: _scale,
      leading: Transform.rotate(
        angle: _currAngle,
        child: SvgPicture.asset(
          'assets/icons/collapse.svg',
          color: iconColor,
        ),
      ),
      title: 'Collapse',
      textStyle: _itemTitleTextStyle(textColor),
      onTap: () {
        _isCollapsed = !_isCollapsed;
        final endWidth = _isCollapsed
            ? SidebarConstants.minWidth
            : SidebarConstants.maxWidth;
        _animateTo(endWidth);
      },
    );
  }

  TextStyle? _itemTitleTextStyle(Color? color) =>
      Theme.of(context).textTheme.headlineMedium?.copyWith(color: color);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
