// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// import '../../blocs/bloc/auth_bloc.dart';
// import '../../blocs/bloc/navigation_bloc.dart';
// import '../../blocs/bloc/sliding_page_bloc.dart';
// import '../../core/const.dart';
// import '../../injection_container.dart';
// import '../../models/user.dart';
// import 'collapsible_item.dart';
//
// class CollapsibleItemList {
//  static void _menuItemPressed(
//     BuildContext context,
//     NavigationEvent event,
//     bool secured,
//   ) {
//     try {
//       BlocProvider.of<SlidingPageBLoC>(context)
//           .add(const CloseSlidingPageEvent());
//     } on Exception catch (e) {
//       debugPrint('error $e');
//     }
//     debugPrint('user3 ${sl<AuthBloc>().state.currentUser}');
//     // если не авторизован - отправляем на логин
//     var securedEvent =
//         secured && sl<AuthBloc>().state.currentUser == UserModel.empty
//             ? const ToLoginNavigationEvent()
//             : event;
//
//     Future.delayed(kSlidingPageAnimDuration)
//         .then((value) => sl<NavigationBLoC>().add(securedEvent));
//   }
//
//   static get items => <CollapsibleItem>[
//     CollapsibleItem(
//         text: 'Marketplace',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/marketplace',
//         ),
//         onPressed: () {
//           _menuItemPressed(
//             context,
//             const ToMarketplaceNavigationEvent(),
//             false,
//           );
//           sl<ProductsCubit>().getProducts();
//         }),
//     CollapsibleItem(
//         text: 'My lores',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/my_lores.svg',
//         ),
//         onPressed: () {
//           _menuItemPressed(
//             context,
//             const ToMyWidgetsNavigationEvent(),
//             true,
//           );
//           sl<PurchasedBloc>().add(LoadPurchasedProductsEvent());
//         }),
//     CollapsibleItem(
//         text: 'Application',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/application.svg',
//         ),
//         onPressed: () => _menuItemPressed(
//               context,
//               const ToDashboardNavigationEvent(),
//               true,
//             )),
//     CollapsibleItem(
//         text: 'My wallet',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/my_vallet.svg',
//         ),
//         onPressed: () => _menuItemPressed(
//               context,
//               const ToDashboardNavigationEvent(),
//               true,
//             )),
//     CollapsibleItem(
//         text: 'Admin',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/admin.svg',
//         ),
//         onPressed: () => _menuItemPressed(
//               context,
//               const ToAdminNavigationEvent(),
//               true,
//             )),
//     CollapsibleItem(
//         text: 'Bug report',
//         icon: SvgPicture.asset(
//           'assets/icons/navigation/bug_report.svg',
//         ),
//         onPressed: () => Wiredash.of(context)!.show())
//   ];
// }
