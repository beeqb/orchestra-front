class CollapsibleItem {
  CollapsibleItem({
    required this.text,
    required this.asset,
    required this.onPressed,
    this.isSelected = false,
  });

  final String text;
  final String asset;
  final Function onPressed;
  bool isSelected;
}
