import 'package:flutter/material.dart';

class CollapsibleItemWidget extends StatelessWidget {
  const CollapsibleItemWidget({
    required this.title,
    required this.leading,
    required this.titleOffsetX,
    required this.scale,
    this.textStyle,
    this.horizontalPadding,
    this.verticalPadding,
    this.onHoverPointer = SystemMouseCursors.click,
    this.height,
    this.onTap,
  });

  final MouseCursor? onHoverPointer;
  final Widget leading;
  final String title;
  final double? height;
  final TextStyle? textStyle;
  final double titleOffsetX, scale;
  final double? horizontalPadding, verticalPadding;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          color: Colors.transparent,
          height: height,
          padding: EdgeInsets.symmetric(
              vertical: verticalPadding ?? 0,
              horizontal: horizontalPadding ?? 0),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              leading,
              SizedBox(
                width: double.infinity,
                child: _title,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget get _title {
    return Opacity(
      opacity: scale,
      child: Transform.translate(
        offset: Offset(titleOffsetX, 0),
        child: Transform.scale(
          scale: scale,
          child: SizedBox(
            width: double.infinity,
            child: Text(
              title,
              style: textStyle,
              softWrap: false,
              overflow: TextOverflow.fade,
            ),
          ),
        ),
      ),
    );
  }
}
