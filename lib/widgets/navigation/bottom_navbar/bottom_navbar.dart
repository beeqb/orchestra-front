import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:orchestra/core/theme_extensions.dart';

import '../../../blocs/cubits/navigation_menu_cubit.dart';
import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../navigation_items.dart';

class BottomNavbar extends StatefulWidget {
  const BottomNavbar({
    Key? key,
  }) : super(key: key);

  @override
  State<BottomNavbar> createState() => _BottomNavbarState();
}

class _BottomNavbarState extends State<BottomNavbar> {
  //final NavigationItems navigationItems;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationMenuCubit, int>(
      builder: (context, state) {
        List<BottomNavigationBarItem> bottomNavbarItems = [
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/icons/navigation/marketplace.svg',
                width: BottomNavbarConstants.iconSize,
                height: BottomNavbarConstants.iconSize,
                color: sl<NavigationMenuCubit>().state == 0
                    ? Theme.of(context)
                        .extension<SidebarColors>()
                        ?.selectedIconColor
                    : Theme.of(context)
                        .extension<SidebarColors>()
                        ?.unselectedTextColor),
            label: 'Marketplace',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/icons/navigation/my_lores.svg',
                width: BottomNavbarConstants.iconSize,
                height: BottomNavbarConstants.iconSize,
                color: sl<NavigationMenuCubit>().state == 1
                    ? Theme.of(context)
                        .extension<SidebarColors>()
                        ?.selectedIconColor
                    : Theme.of(context)
                        .extension<SidebarColors>()
                        ?.unselectedTextColor),
            label: 'My lore',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/icons/navigation/application.svg',
                width: BottomNavbarConstants.iconSize,
                height: BottomNavbarConstants.iconSize,
                color: sl<NavigationMenuCubit>().state == 2
                    ? Theme.of(context)
                        .extension<SidebarColors>()
                        ?.selectedIconColor
                    : Theme.of(context)
                        .extension<SidebarColors>()
                        ?.unselectedTextColor),
            label: 'Applications',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/icons/navigation/my_vallet.svg',
                width: BottomNavbarConstants.iconSize,
                height: BottomNavbarConstants.iconSize,
                color: sl<NavigationMenuCubit>().state == 3
                    ? Theme.of(context)
                        .extension<SidebarColors>()
                        ?.selectedIconColor
                    : Theme.of(context)
                        .extension<SidebarColors>()
                        ?.unselectedTextColor),
            label: 'My wallet',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset('assets/icons/navigation/bug_report.svg',
                width: BottomNavbarConstants.iconSize,
                height: BottomNavbarConstants.iconSize,
                color: Theme.of(context)
                    .extension<SidebarColors>()
                    ?.unselectedTextColor),
            label: 'Report',
          ),
        ];

        return BottomNavigationBar(
          elevation: 0,
          items: bottomNavbarItems,
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          currentIndex: state,
          selectedItemColor: state != bottomNavbarItems.length - 1
              ? Theme.of(context).extension<SidebarColors>()?.selectedTextColor
              : Theme.of(context)
                  .extension<SidebarColors>()
                  ?.unselectedTextColor,
          unselectedItemColor:
              Theme.of(context).extension<SidebarColors>()?.unselectedTextColor,
          selectedLabelStyle: BottomNavbarConstants.selectedLabelStyle,
          unselectedLabelStyle: BottomNavbarConstants.unselectedLabelStyle,
          onTap: (index) {
            NavigationItems.onBottomNavigationBarItemTapped(context, index);
          },
          backgroundColor: Theme.of(context).bottomAppBarColor,
        );
      },
    );
  }
}
