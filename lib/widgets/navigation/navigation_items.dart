import 'package:flutter/material.dart';
import 'package:wiredash/wiredash.dart';

import '../../../blocs/bloc/auth_bloc.dart';
import '../../../blocs/bloc/navigation_bloc.dart';
import '../../../blocs/bloc/purchased_bloc.dart';
import '../../../blocs/cubits/products_cubit.dart';
import '../../../injection_container.dart';
import '../../../models/user.dart';

class NavigationItems {
  static void onBottomNavigationBarItemTapped(BuildContext context, int index) {
    switch (index) {
      case 0:
        {
          onSidebarNavigationItemPressed(
            context,
            const ToMarketplaceNavigationEvent(),
            false,
          );
          sl<ProductsCubit>().getProducts();
          break;
        }
      case 1:
        {
          onSidebarNavigationItemPressed(
              context, const ToMyWidgetsNavigationEvent(), true);
          sl<PurchasedBloc>().add(LoadPurchasedProductsEvent());
          break;
        }

      case 2:
        {
          onSidebarNavigationItemPressed(
            context,
            const ToDashboardNavigationEvent(),
            true,
          );
          break;
        }
      case 3:
        {
          onSidebarNavigationItemPressed(
            context,
            const ToBillingsNavigationEvent(),
            true,
          );
          sl<PurchasedBloc>().add(LoadPurchasedProductsEvent());
          break;
        }
      case 4:
        {
          Wiredash.of(context).show();
          break;
        }
    }
  }

  static void onSidebarNavigationItemPressed(
    BuildContext context,
    NavigationEvent event,
    bool secured,
  ) {
    // если не авторизован - отправляем на логин
    var securedEvent =
        secured && sl<AuthBloc>().state.currentUser == UserModel.empty
            ? const ToLoginNavigationEvent()
            : event;
    sl<NavigationBLoC>().add(securedEvent);
  }
}
