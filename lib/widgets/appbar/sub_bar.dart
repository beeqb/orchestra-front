import 'package:flutter/material.dart';

import '../../core/const.dart';

PreferredSizeWidget SubBar(BuildContext context,
        {required title,
        required bool isMobile,
        List<Widget>? actions,
        double edgeLeft = 0,
        automaticallyImplyLeading = false}) =>
    PreferredSize(
      preferredSize: Size.fromHeight(isMobile
          ? AppBarSizeCostants.mobileSubBarHeight
          : AppBarSizeCostants.desktopSubBarHeight),
      child: AppBar(
        titleSpacing: 0,
        toolbarHeight: isMobile
            ? AppBarSizeCostants.mobileSubBarHeight
            : AppBarSizeCostants.desktopSubBarHeight,
        centerTitle: false,
        title: Padding(
          padding: isMobile
              ? EdgeInsets.only(left: edgeLeft)
              : EdgeInsets.only(left: edgeLeft),
          child: Text(
            title,
            style: isMobile
                ? Theme.of(context).textTheme.titleMedium
                : Theme.of(context).textTheme.titleLarge,
          ),
        ),
        elevation: 0,
        actions: actions,
        automaticallyImplyLeading: automaticallyImplyLeading,
      ),
    );
