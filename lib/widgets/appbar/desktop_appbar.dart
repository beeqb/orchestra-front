import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:orchestra/widgets/appbar/profile_menu_item.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/search_product_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../dart_mode_switch.dart';
import '../marketplace_widgets/modal_bottom_search.dart';
import '../product_image_widget.dart';

class DesktopAppbar extends StatelessWidget {
  const DesktopAppbar({Key? key}) : super(key: key);

  void showSearchDropdown(BuildContext context) {
    sl<SearchProductBloc>().add(const FetchSearchedProductsEvent(query: ''));
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return const ModalBottomSearch();
        });
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        toolbarHeight: 72,
        shape: Border(
            bottom:
                BorderSide(color: Theme.of(context).dividerColor, width: 1)),
        elevation: 0,
        actions: [
          TextButton(
              onPressed: () => showSearchDropdown(context),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 14,
                ),
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: SvgPicture.asset(
                    'assets/icons/search.svg',
                    color: Theme.of(context).appBarTheme.iconTheme?.color,
                  ),
                ),
              )),
          const SizedBox(
            width: 16,
          ),
          BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
            if (state is LoggedAuthState) {
              return const _ProfilePopupMenu();
            } else {
              return const SizedBox.shrink();
            }
          })
        ]);
  }
}

class _ProfilePopupMenu extends StatefulWidget {
  const _ProfilePopupMenu({Key? key}) : super(key: key);

  @override
  State<_ProfilePopupMenu> createState() => _ProfilePopupMenuState();
}

class _ProfilePopupMenuState extends State<_ProfilePopupMenu> {
  @override
  void initState() {
    AdaptiveTheme.of(context).modeChangeNotifier.addListener(() {
      setState(() {});
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(AdaptiveTheme.of(context).mode.isDark);
    return PopupMenuButton(
      padding: EdgeInsets.zero,
      color: Theme.of(context).backgroundColor,
      constraints: const BoxConstraints(
        minWidth: 236,
        maxWidth: 236,
      ),
      position: PopupMenuPosition.under,
      itemBuilder: (context) {
        return profileMenuItems.map(((ProfileMenuItem dropdownItem) {
          final isDarkModeItem =
              dropdownItem.title == ProfileItemConstants.darkModeTitle;
          return PopupMenuItem(
              enabled: !isDarkModeItem,
              onTap: dropdownItem.onTap,
              textStyle: Theme.of(context).popupMenuTheme.textStyle,
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 12,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  isDarkModeItem
                      ? const DarkModeSwitch()
                      : SizedBox(
                          width: 24,
                          height: 24,
                          child: SvgPicture.asset(dropdownItem.icon),
                        ),
                  SizedBox(width: isDarkModeItem ? 12 : 24),
                  Text(
                    dropdownItem.title,
                  ),
                ],
              ));
        })).toList();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(width: 16),
          const _UserAvatar(),
          const SizedBox(
            width: 8,
          ),
          SvgPicture.asset('assets/icons/dropdown_arrow.svg'),
          const SizedBox(
            width: 16,
          ),
        ],
      ),
    );
  }
}

class _UserAvatar extends StatelessWidget {
  const _UserAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        final photoUrl = state.currentUser.photoUrl;
        return ProductImageWidget(
            imageUrl: photoUrl, width: 48, height: 48, borderRadius: 45);
      },
    );
  }
}
