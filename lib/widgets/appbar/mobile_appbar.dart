import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:orchestra/widgets/appbar/profile_menu_item.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../blocs/bloc/search_product_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../dart_mode_switch.dart';
import '../marketplace_widgets/modal_bottom_search.dart';
import '../product_image_widget.dart';

class MobileAppbar extends StatelessWidget {
  const MobileAppbar({Key? key, required this.navigatorKey}) : super(key: key);
  final GlobalKey<NavigatorState> navigatorKey;

  void showSearchDropdown(BuildContext context) {
    sl<SearchProductBloc>().add(const FetchSearchedProductsEvent(query: ''));
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return const ModalBottomSearch();
        });
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
        toolbarHeight: AppBarSizeCostants.mobileAppbarHeight,
        elevation: 0,
        leading: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is LoggedAuthState) {
              return BlocBuilder<NavigationBLoC, NavigationState>(
                builder: (context, state) {
                  return Builder(builder: (context) {
                    return sl<NavigationBLoC>().isRootDirectory
                        ? TextButton(
                            onPressed: () => Scaffold.of(context).openDrawer(),
                            child: const _MobileAvatar(),
                          )
                        : TextButton(
                            onPressed: () => navigatorKey.currentState?.pop(),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Theme.of(context).primaryIconTheme.color,
                            ),
                          );
                  });
                },
              );
            }
            return const SizedBox.shrink();
          },
        ),
        actions: [
          TextButton(
              onPressed: () => showSearchDropdown(context),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 14,
                ),
                child: SizedBox(
                  width: 20,
                  height: 20,
                  child: SvgPicture.asset(
                    'assets/icons/search.svg',
                    color: Theme.of(context).appBarTheme.iconTheme?.color,
                  ),
                ),
              )),
        ]);
  }
}

class _MobileAvatar extends StatelessWidget {
  const _MobileAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        final photoUrl = state.currentUser.photoUrl;
        return ProductImageWidget(
            imageUrl: photoUrl, width: 32, height: 32, borderRadius: 45);
      },
    );
  }
}

class MobileDrawer extends StatelessWidget {
  const MobileDrawer({Key? key, this.closeOnItemTap = true}) : super(key: key);

  final bool closeOnItemTap;

  @override
  Widget build(BuildContext context) {
    final _profileMenuItems = profileMenuItems
        .map((ProfileMenuItem drawerItem) => _DrawerItem(
              profileMenuItem: drawerItem,
              closeOnTap: closeOnItemTap,
            ))
        .toList();
    return SafeArea(
      child: Container(
        width: 240,
        color: Theme.of(context).drawerTheme.backgroundColor,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const Padding(
              padding: EdgeInsets.symmetric(vertical: 14, horizontal: 12),
              child: _MobileAvatar()),
          const _UserNameAndEmail(),
          const SizedBox(
            height: 11,
          ),
          ..._profileMenuItems.sublist(0, _profileMenuItems.length - 1),
          Expanded(
              child: Align(
                  alignment: Alignment.bottomLeft,
                  child: _profileMenuItems.last)),
        ]),
      ),
    );
  }
}

class _UserNameAndEmail extends StatelessWidget {
  const _UserNameAndEmail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = sl<AuthBloc>().state.currentUser;
    final userName = user.name ?? '';
    final userEmail = user.email ?? '';
    return Padding(
      padding: const EdgeInsets.only(left: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: double.infinity,
            child: Text(
              userName,
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
          const SizedBox(height: 4),
          Text(
            userEmail,
            style: TextStyle(
              color: Theme.of(context).disabledColor,
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}

class _DrawerItem extends StatefulWidget {
  const _DrawerItem(
      {Key? key, required this.profileMenuItem, this.closeOnTap = true})
      : super(key: key);

  final ProfileMenuItem profileMenuItem;
  final bool closeOnTap;

  @override
  State<_DrawerItem> createState() => _DrawerItemState();
}

class _DrawerItemState extends State<_DrawerItem> {
  @override
  Widget build(BuildContext context) {
    final isDarkModeItem =
        widget.profileMenuItem.title == ProfileItemConstants.darkModeTitle;
    return SizedBox(
        height: 48,
        child: !isDarkModeItem
            ? TextButton(
                onPressed: () {
                  if (widget.profileMenuItem.onTap != null && !isDarkModeItem) {
                    widget.profileMenuItem.onTap!();
                  }
                  if (widget.closeOnTap) {
                    Navigator.of(context).pop();
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(
                      width: 16,
                    ),
                    Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: SvgPicture.asset(
                        widget.profileMenuItem.icon,
                        color: Theme.of(context).primaryIconTheme.color,
                      ),
                    ),
                    const SizedBox(width: 30),
                    SizedBox(
                      child: Text(
                        widget.profileMenuItem.title,
                        style: Theme.of(context).textTheme.labelMedium,
                      ),
                    ),
                  ],
                ),
              )
            : Row(
                children: [
                  const SizedBox(
                    width: 11,
                  ),
                  const DarkModeSwitch(),
                  const SizedBox(width: 15),
                  SizedBox(
                    child: Text(
                      widget.profileMenuItem.title,
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  )
                ],
              ));
  }
}
