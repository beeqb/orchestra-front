import 'package:orchestra/blocs/cubits/profile_menu_cubit.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../injection_container.dart';

class ProfileMenuItem {
  final String title;
  final String icon;
  final Function()? onTap;
  final bool? isSelected;

  ProfileMenuItem(
      {required this.title, required this.icon, this.onTap, this.isSelected});
}

List<ProfileMenuItem> get profileMenuItems => <ProfileMenuItem>[
      ProfileMenuItem(
          title: 'My Profile',
          icon: 'assets/icons/profile/my_profile.svg',
          onTap: () {
            sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
            sl<ProfileMenuCubit>().chooseMenuItem(0);
          },
          isSelected: sl<ProfileMenuCubit>().state == 0),
      ProfileMenuItem(
          title: 'Address',
          icon: 'assets/icons/profile/address.svg',
          onTap: () {
            sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
            sl<ProfileMenuCubit>().chooseMenuItem(1);
          },
          isSelected: sl<ProfileMenuCubit>().state == 1),
      ProfileMenuItem(
          title: 'Referral program',
          icon: 'assets/icons/profile/referral.svg',
          onTap: () {
            sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
            sl<ProfileMenuCubit>().chooseMenuItem(2);
          },
          isSelected: sl<ProfileMenuCubit>().state == 2),
      ProfileMenuItem(
          title: 'X-API key',
          icon: 'assets/icons/profile/x_api_key.svg',
          onTap: () {
            sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
            sl<ProfileMenuCubit>().chooseMenuItem(3);
          },
          isSelected: sl<ProfileMenuCubit>().state == 3),
      ProfileMenuItem(
          title: 'Contributions',
          icon: 'assets/icons/profile/contributions.svg',
          onTap: () {
            sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
            sl<ProfileMenuCubit>().chooseMenuItem(4);
          },
          isSelected: sl<ProfileMenuCubit>().state == 4),
      ProfileMenuItem(title: 'Dark mode', icon: ''),
      ProfileMenuItem(
          title: 'Add Lore',
          icon: 'assets/icons/profile/contributions.svg',
          onTap: () =>
              sl<NavigationBLoC>().add(const ToAddProductNavigationEvent())),
      ProfileMenuItem(
          title: 'Added Lores',
          icon: 'assets/icons/profile/contributions.svg',
          onTap: () => sl<NavigationBLoC>().add(const ToAddedProductsEvent())),
      ProfileMenuItem(
          title: 'Logout',
          icon: 'assets/icons/profile/logout.svg',
          onTap: () => sl<AuthBloc>().add(StartLogoutAuthEvent())),
    ];
