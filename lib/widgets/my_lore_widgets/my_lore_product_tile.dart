import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../blocs/bloc/root_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/app.dart';
import '../../models/product/product.dart';
import '../../models/purchase_model.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';
import '../../view_models/product_view_model.dart';
import '../applications_widgets/app_creation_settings_form.dart';
import '../marketplace_widgets/star_rating.dart';
import '../product_image_widget.dart';

class DesktopMyLoreProductTile extends StatelessWidget {
  final PurchaseModel purchaseModel;

  const DesktopMyLoreProductTile({Key? key, required this.purchaseModel})
      : super(key: key);

  void onPressed(BuildContext context) {
    if (purchaseModel.isActive) {
      _createApp(purchaseModel, context);
    } else {
      ///To do Buy product event
      showTestFlyGoneDialogMyLore(product: purchaseModel.product);
    }
  }

  void showTestFlyGoneDialogMyLore({
    required Product product,
  }) async {
    final dialogResult = await sl<DialogsService>().showDialog(
      dialogType: DialogType.testFlyGoneDialog,
      title: '',
      product: product,
    );
    if (dialogResult.confirmed!) {
// обработка ответа сервера при покупке виджета
      final result = await sl<BillingService>().chargeForProduct(product.id);
      if (result['status'] == 'error') {
        await sl<DialogsService>().showDialog(
          dialogType: DialogType.warningDialog,
          title: 'Error',
          description: result['message'] ?? '',
        );
      }
    }
  }

  Future<void> _createApp(
      PurchaseModel purchaseModel, BuildContext context) async {
    final authState = sl<AuthBloc>().state;
    if (authState is LoggedAuthState) {
      sl<RootBloc>().add(
        StartCreatingAppFromWidgetEvent(
          AppModel(
            userId: authState.currentUser.id!,
            name: purchaseModel.product.productName,
            failure: purchaseModel.product.failure,
            loop: 'none',
            widgets: [],
            notification: ['modal'],
            status: 'Created',
            icon: purchaseModel.product.productLogoURL,
          ),
          purchaseModel.product,
        ),
      );
      showDialog(
          context: context,
          builder: (context) {
            return DesktopAppCreationSettingsForm(
                product: purchaseModel.product);
          });
    } else {
      sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).backgroundColor),
      child: TextButton(
        onPressed: () => sl<NavigationBLoC>()
            .add(ToProductDetailsNavigationEvent(purchaseModel.product.id)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(children: [
              const SizedBox(
                width: 16,
              ),
              ProductImageWidget(
                imageUrl: purchaseModel.product.productLogoURL,
                width: 90,
                height: 90,
              ),
              const SizedBox(
                width: 16,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    purchaseModel.product.productName,
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    purchaseModel.product.productCategory.title,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                  const SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      StarRating(
                        rating: ProductViewModel.rankAsDouble(
                            purchaseModel.product.rank),
                        iconSize: 12,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        ProductViewModel.rankStringDecimal(
                            purchaseModel.product.rank),
                        style: purchaseModel.product.rank == null
                            ? Theme.of(context).textTheme.bodyMedium?.copyWith(
                                color: Theme.of(context).disabledColor)
                            : Theme.of(context).textTheme.labelSmall,
                      ),
                    ],
                  ),
                  const SizedBox(height: 4),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Theme.of(context).cardColor),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 8,
                          vertical: 4,
                        ),
                        child: Text(
                          ProductViewModel.priceToStr(purchaseModel.product),
                          style: Theme.of(context).textTheme.labelSmall,
                        ),
                      ),
                      const SizedBox(
                        width: 13,
                      ),
                      Text(purchaseModel.subscriptionTimeStatus,
                          style: purchaseModel.isActive
                              ? Theme.of(context).textTheme.bodyMedium
                              : Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Theme.of(context).disabledColor))
                    ],
                  ),
                ],
              ),
            ]),
            const SizedBox(
              height: 16,
            ),
            TextButton(
              onPressed: () => onPressed(context),
              child: purchaseModel.isActive
                  ? Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      Icon(
                        Icons.add,
                        size: 13.5,
                        color: Theme.of(context).primaryIconTheme.color,
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Text(
                        purchaseModel.isActive
                            ? 'Create new application'
                            : 'Buy now',
                        style: Theme.of(context).textTheme.labelMedium,
                      )
                    ])
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Buy now',
                          style: Theme.of(context).textTheme.labelMedium,
                        )
                      ],
                    ),
              style: Theme.of(context).textButtonTheme.style?.copyWith(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      Theme.of(context).cardColor),
                  fixedSize: MaterialStateProperty.all<Size>(
                    const Size(380, 48),
                  )),
            )
          ],
        ),
      ),
    );
  }
}

class MobileMyLoreProductTile extends StatelessWidget {
  final PurchaseModel purchaseModel;

  const MobileMyLoreProductTile({Key? key, required this.purchaseModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TextButton(
        onPressed: () => sl<NavigationBLoC>()
            .add(ToProductDetailsNavigationEvent(purchaseModel.product.id)),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ProductImageWidget(
            imageUrl: purchaseModel.product.productLogoURL,
            width: ProductCardSizeConstants.mobileCardHeight,
            height: ProductCardSizeConstants.mobileCardHeight,
          ),
          const SizedBox(
            width: 12,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                purchaseModel.product.productName,
                style: Theme.of(context).textTheme.labelSmall,
              ),
              const SizedBox(height: 4),
              Text(
                purchaseModel.product.productCategory.title,
                style: Theme.of(context)
                    .textTheme
                    .bodySmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
              const SizedBox(height: 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  StarRating(
                    rating: ProductViewModel.rankAsDouble(
                        purchaseModel.product.rank),
                    iconSize: 12,
                  ),
                  const SizedBox(width: 4),
                  Text(
                    purchaseModel.product.rank == null
                        ? '0.0'
                        : purchaseModel.product.rank.toString(),
                    style: purchaseModel.product.rank == null
                        ? Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: Theme.of(context).disabledColor)
                        : Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
            ],
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Theme.of(context).cardColor),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 6,
                      vertical: 4,
                    ),
                    child: Text(
                      ProductViewModel.priceToStr(purchaseModel.product),
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    purchaseModel.subscriptionTimeStatus,
                    style: purchaseModel.isActive
                        ? Theme.of(context).textTheme.bodySmall
                        : Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
      const SizedBox(
        height: 16,
      ),
    ]);
  }
}
