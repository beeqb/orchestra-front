import 'dart:math';

import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/widgets/my_lore_widgets/my_lore_product_tile.dart';

import '../../core/const.dart';
import '../../core/scroll_behavior.dart';
import '../../models/purchase_model.dart';
import '../category_label.dart';

class MyLoreProducts extends StatefulWidget {
  const MyLoreProducts({
    Key? key,
    required this.purchaseModels,
  }) : super(key: key);

  final List<PurchaseModel> purchaseModels;

  @override
  State<MyLoreProducts> createState() => _MyLoreProductsState();
}

class _MyLoreProductsState extends State<MyLoreProducts> {
  ProductCategoryType? selectedCategory;
  List<PurchaseModel?> purchaseModels = [];
  late List<ProductCategoryType> nonEmptyCategories;

  void onCategoryPressed(ProductCategoryType? appCategory) {
    selectedCategory = appCategory;
    if (appCategory != null) {
      purchaseModels = widget.purchaseModels
          .where((purchaseModel) =>
              purchaseModel.product.productCategory == selectedCategory)
          .toSet()
          .toList();
    } else {
      purchaseModels = widget.purchaseModels;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    nonEmptyCategories = widget.purchaseModels
        .map((purchaseModel) => purchaseModel.product.productCategory)
        .toList()
        .toSet()
        .toList();
    purchaseModels = widget.purchaseModels;
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(
        height: isMobile ? 32 : 36,
        child: ScrollConfiguration(
          behavior: MyCustomScrollBehavior(),
          child: ListView.separated(
            separatorBuilder: (_, __) => const SizedBox(width: 15),
            itemCount: nonEmptyCategories.length + 1,
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              if (index == 0) {
                return CategoryLabel(
                    onCategoryPressed: onCategoryPressed,
                    isSelected: selectedCategory == null);
              }
              return CategoryLabel(
                  appCategory: nonEmptyCategories[index - 1],
                  onCategoryPressed: onCategoryPressed,
                  isSelected:
                      selectedCategory == nonEmptyCategories[index - 1]);
            },
          ),
        ),
      ),
      SizedBox(
        height: isMobile ? 20 : 24,
      ),
      _ProductList(purchaseModels: purchaseModels),
    ]);
  }
}

class _ProductList extends StatelessWidget {
  const _ProductList({Key? key, required this.purchaseModels})
      : super(key: key);

  final List<PurchaseModel?> purchaseModels;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return isMobile
        ? ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: purchaseModels.length,
            separatorBuilder: (_, __) => const SizedBox(width: 45),
            itemBuilder: (context, index) =>
                MobileMyLoreProductTile(purchaseModel: purchaseModels[index]!))
        : LayoutBuilder(builder: (context, constraints) {
            return SizedBox(
              width: (constraints.maxWidth ~/
                      ProductCardSizeConstants.desktopCardWidth) *
                  ProductCardSizeConstants.desktopCardWidth,
              child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: purchaseModels.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: max(
                        constraints.maxWidth ~/
                            ProductCardSizeConstants.desktopCardWidth,
                        1),
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    mainAxisExtent: 182),
                itemBuilder: (BuildContext context, int index) {
                  return DesktopMyLoreProductTile(
                    purchaseModel: purchaseModels[index]!,
                  );
                },
              ),
            );
          });
  }
}
