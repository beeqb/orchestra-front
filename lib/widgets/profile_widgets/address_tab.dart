import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../blocs/cubits/user_cubit.dart';
import '../../injection_container.dart';
import '../../models/product_models/adress.dart';
import '../../models/user.dart';

class DesktopAddressTab extends StatefulWidget {
  final UserModel currentUser;

  const DesktopAddressTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  State<DesktopAddressTab> createState() => _DesktopAddressTabState();
}

class _DesktopAddressTabState extends State<DesktopAddressTab> {
  Address? _address;
  String? _addressLine1, _addressLine2, _country, _city, _state, _postcode;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: const EdgeInsets.all(32),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Address',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 32,
            ),
            Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Address Line 1',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.addressLine1,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _addressLine1 = newValue
                              : widget.currentUser.address?.addressLine1 != null
                                  ? _addressLine1 =
                                      widget.currentUser.address!.addressLine1
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Address Line 2',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.addressLine2,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _addressLine2 = newValue
                              : widget.currentUser.address?.addressLine2 != null
                                  ? _addressLine2 =
                                      widget.currentUser.address!.addressLine2
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 22,
            ),
            Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Country',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.country,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _country = newValue
                              : widget.currentUser.address?.country != null
                                  ? _country =
                                      widget.currentUser.address!.country
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'City',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.city,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _city = newValue
                              : widget.currentUser.address?.city != null
                                  ? _city = widget.currentUser.address!.city
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 22,
            ),
            Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'State',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.state,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _state = newValue
                              : widget.currentUser.address?.state != null
                                  ? _state = widget.currentUser.address!.state
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Postcode',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.address?.postcode,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          newValue != null
                              ? _postcode = newValue
                              : widget.currentUser.address?.postcode != null
                                  ? _postcode =
                                      widget.currentUser.address!.postcode
                                  : null;
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            _buttonsPanel()
          ],
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TextButton(
          style: Theme.of(context).textButtonTheme.style?.copyWith(
              backgroundColor: MaterialStateProperty.all<Color>(
                  Theme.of(context).cardColor)),
          onPressed: () {
            // Navigator.pop(context);
          },
          child: SizedBox(
            width: 103,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () => _submitForm(),
          child: SizedBox(
              width: 103,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ))),
          style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).cardColor),
              ),
        ),
      ],
    );
  }

  void _submitForm() {
    final form = _formKey.currentState;
    print(1);
    if (!form!.validate()) {
      // showMessage(context, 'Form is not valid! Please review and correct.');
    } else {
      form.save();

      final _updAddress = Address().copyWith(
        addressLine1: _addressLine1,
        addressLine2: _addressLine2,
        country: _country,
        city: _city,
        state: _state,
        postcode: _postcode,
      );

      final userUpd = widget.currentUser.copyWith(
        address: _updAddress,
      );

      sl<UserCubit>().updateUserInfo(userUpd);
    }
  }
}

class MobileAddressTab extends StatefulWidget {
  final UserModel currentUser;

  const MobileAddressTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  State<MobileAddressTab> createState() => _MobileAddressTabState();
}

class _MobileAddressTabState extends State<MobileAddressTab> {
  Address? _address;
  String? _addressLine1, _addressLine2, _country, _city, _state, _postcode;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.all(14),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Address',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              const SizedBox(
                height: 25,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Address Line 1',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.addressLine1,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _addressLine1 = newValue
                          : widget.currentUser.address?.addressLine1 != null
                              ? _addressLine1 =
                                  widget.currentUser.address!.addressLine1
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Address Line 2',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.addressLine2,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _addressLine2 = newValue
                          : widget.currentUser.address?.addressLine2 != null
                              ? _addressLine2 =
                                  widget.currentUser.address!.addressLine2
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Country',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.country,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _country = newValue
                          : widget.currentUser.address?.country != null
                              ? _country = widget.currentUser.address!.country
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'City',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.city,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _city = newValue
                          : widget.currentUser.address?.city != null
                              ? _city = widget.currentUser.address!.city
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'State',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.state,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _state = newValue
                          : widget.currentUser.address?.state != null
                              ? _state = widget.currentUser.address!.state
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Postcode',
                    style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.address?.postcode,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      newValue != null
                          ? _postcode = newValue
                          : widget.currentUser.address?.postcode != null
                              ? _postcode = widget.currentUser.address!.postcode
                              : null;
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              _buttonsPanel()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Expanded(
          child: TextButton(
            style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).cardColor)),
            onPressed: () {
              // Navigator.pop(context);
            },
            child: SizedBox(
              height: 40,
              child: Center(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        Expanded(
          child: TextButton(
            onPressed: () => _submitForm(),
            child: SizedBox(
                height: 40,
                child: Center(
                    child: Text(
                  'Save',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ))),
            style: Theme.of(context).textButtonTheme.style?.copyWith(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      Theme.of(context).cardColor),
                ),
          ),
        ),
      ],
    );
  }

  void _submitForm() {
    final form = _formKey.currentState;
    print(1);
    if (!form!.validate()) {
      // showMessage(context, 'Form is not valid! Please review and correct.');
    } else {
      form.save();

      final _updAddress = Address().copyWith(
        addressLine1: _addressLine1,
        addressLine2: _addressLine2,
        country: _country,
        city: _city,
        state: _state,
        postcode: _postcode,
      );

      final userUpd = widget.currentUser.copyWith(
        address: _updAddress,
      );

      sl<UserCubit>().updateUserInfo(userUpd);
    }
  }
}
