import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/cubits/profile_menu_cubit.dart';
import '../../core/const.dart';
import '../appbar/profile_menu_item.dart';

class ProfileTabPanel extends StatefulWidget {
  const ProfileTabPanel({Key? key}) : super(key: key);

  @override
  State<ProfileTabPanel> createState() => _ProfileTabPanelState();
}

class _ProfileTabPanelState extends State<ProfileTabPanel> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 276,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Theme.of(context).backgroundColor,
      ),
      child: BlocBuilder<ProfileMenuCubit, int>(
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 24,
              ),
              const _ProfileAvatar(),
              const SizedBox(height: 20),
              const _NameAndEmail(),
              const SizedBox(
                height: 32,
              ),
              ...profileMenuItems.map(((ProfileMenuItem dropdownItem) {
                final isDarkModeItem =
                    dropdownItem.title == ProfileItemConstants.darkModeTitle;
                return isDarkModeItem
                    ? const SizedBox.shrink()
                    : Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4),
                        child: Column(
                          children: [
                            Container(
                              width: 242,
                              height: 48,
                              decoration: BoxDecoration(
                                color: dropdownItem.isSelected != null &&
                                        dropdownItem.isSelected!
                                    ? Theme.of(context).secondaryHeaderColor
                                    : Theme.of(context).backgroundColor,
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              child: TextButton(
                                onPressed: dropdownItem.onTap,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 16,
                                    ),
                                    SizedBox(
                                      width: 24,
                                      height: 24,
                                      child:
                                          SvgPicture.asset(dropdownItem.icon),
                                    ),
                                    const SizedBox(width: 20),
                                    Text(
                                      dropdownItem.title,
                                      style: dropdownItem.isSelected != null &&
                                              dropdownItem.isSelected!
                                          ? Theme.of(context)
                                              .textTheme
                                              .headlineMedium
                                          : Theme.of(context)
                                              .textTheme
                                              .headlineMedium
                                              ?.copyWith(
                                                  color: Theme.of(context)
                                                      .iconTheme
                                                      .color),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
              })).toList()
            ],
          );
        },
      ),
    );
  }
}

class _ProfileAvatar extends StatelessWidget {
  const _ProfileAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 92,
        height: 92,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(45.0),
          child: BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
            if (state is LoggedAuthState) {
              final photoUrl = state.currentUser.photoUrl;
              if (photoUrl != null) {
                return CachedNetworkImage(imageUrl: photoUrl);
              }
            }
            return Image.asset('assets/images/orchestra_logo_gray.png');
          }),
        ));
  }
}

class _NameAndEmail extends StatelessWidget {
  const _NameAndEmail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      if (state is LoggedAuthState) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              state.currentUser.name ?? '',
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(fontSize: 20),
            ),
            const SizedBox(height: 8),
            Text(state.currentUser.email ?? '',
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    fontSize: 16, color: Theme.of(context).disabledColor)),
          ],
        );
      }
      return const Center(child: CircularProgressIndicator());
    });
  }
}
