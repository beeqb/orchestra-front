import 'package:flutter/material.dart';

import '../../blocs/cubits/user_cubit.dart';
import '../../injection_container.dart';
import '../../models/user.dart';
import '../../pages/profile.dart';

class DesktopMyProfileTab extends StatefulWidget {
  final UserModel currentUser;

  const DesktopMyProfileTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  State<DesktopMyProfileTab> createState() => _DesktopMyProfileTabState();
}

class _DesktopMyProfileTabState extends State<DesktopMyProfileTab> {
  String? _userName;
  String? _companyName;
  String? _occupation;
  String? _phoneNo;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: const EdgeInsets.all(32),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Account information',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 32,
            ),
            Text('Personal information',
                style: Theme.of(context).textTheme.headlineMedium),
            const SizedBox(
              height: 18,
            ),
            Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Full name',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.name,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) {
                          if (newValue != null) {
                            _userName = newValue;
                          } else {
                            _userName = widget.currentUser.name;
                          }
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Company name',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.companyName,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) => newValue != null
                            ? _companyName = newValue
                            : widget.currentUser.companyName != null
                                ? _companyName = widget.currentUser.companyName
                                : null,
                      )
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 22,
            ),
            Row(
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Occupation',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.occupation,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) => newValue != null
                            ? _occupation = newValue
                            : widget.currentUser.occupation != null
                                ? _occupation = widget.currentUser.occupation
                                : null,
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  width: 24,
                ),
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Mobile Number',
                        style:
                            Theme.of(context).textTheme.labelMedium?.copyWith(
                                  color: Theme.of(context).iconTheme.color,
                                ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextFormField(
                        initialValue: widget.currentUser.phoneNo,
                        style: Theme.of(context).textTheme.headlineMedium,
                        onSaved: (newValue) => newValue != null
                            ? _phoneNo = newValue
                            : widget.currentUser.phoneNo != null
                                ? _phoneNo = widget.currentUser.phoneNo
                                : null,
                      )
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 44,
            ),
            _buttonsPanel()
          ],
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TextButton(
          style: Theme.of(context).textButtonTheme.style?.copyWith(
              backgroundColor: MaterialStateProperty.all<Color>(
                  Theme.of(context).cardColor)),
          onPressed: () {
            // Navigator.pop(context);
          },
          child: SizedBox(
            width: 103,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () => _submitForm(),
          child: SizedBox(
              width: 103,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ))),
          style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).cardColor),
              ),
        ),
      ],
    );
  }

  void _submitForm() {
    final form = _formKey.currentState;

    if (!form!.validate()) {
      showMessage(context, 'Form is not valid! Please review and correct.');
    } else {
      form.save();

      final userUpd = widget.currentUser.copyWith(
        phoneNo: _phoneNo,
        name: _userName,
        companyName: _companyName,
        occupation: _occupation,
      );

      sl<UserCubit>().updateUserInfo(userUpd);
    }
  }
}

class MobileMyProfileTab extends StatefulWidget {
  final UserModel currentUser;

  const MobileMyProfileTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  State<MobileMyProfileTab> createState() => _MobileMyProfileTabState();
}

class _MobileMyProfileTabState extends State<MobileMyProfileTab> {
  String? _userName;
  String? _companyName;
  String? _occupation;
  String? _phoneNo;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.all(14),
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Account information',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              const SizedBox(
                height: 25,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Full name',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.name,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) {
                      if (newValue != null) {
                        _userName = newValue;
                      } else {
                        _userName = widget.currentUser.name;
                      }
                    },
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Company name',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.companyName,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) => newValue != null
                        ? _companyName = newValue
                        : widget.currentUser.companyName != null
                            ? _companyName = widget.currentUser.companyName
                            : null,
                  )
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Occupation',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.occupation,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) => newValue != null
                        ? _occupation = newValue
                        : widget.currentUser.occupation != null
                            ? _occupation = widget.currentUser.occupation
                            : null,
                  )
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Mobile Number',
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                        ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  TextFormField(
                    initialValue: widget.currentUser.phoneNo,
                    style: Theme.of(context).textTheme.labelSmall,
                    onSaved: (newValue) => newValue != null
                        ? _phoneNo = newValue
                        : widget.currentUser.phoneNo != null
                            ? _phoneNo = widget.currentUser.phoneNo
                            : null,
                  )
                ],
              ),
              const SizedBox(
                height: 44,
              ),
              _buttonsPanel()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: TextButton(
            style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).cardColor)),
            onPressed: () {
              // Navigator.pop(context);
            },
            child: SizedBox(
              height: 40,
              child: Center(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        Flexible(
          child: TextButton(
            onPressed: () => _submitForm(),
            child: SizedBox(
                height: 40,
                child: Center(
                    child: Text(
                  'Save',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ))),
            style: Theme.of(context).textButtonTheme.style?.copyWith(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      Theme.of(context).cardColor),
                ),
          ),
        ),
      ],
    );
  }

  void _submitForm() {
    final form = _formKey.currentState;

    if (!form!.validate()) {
      showMessage(context, 'Form is not valid! Please review and correct.');
    } else {
      form.save();

      final userUpd = widget.currentUser.copyWith(
        phoneNo: _phoneNo,
        name: _userName,
        companyName: _companyName,
        occupation: _occupation,
      );

      sl<UserCubit>().updateUserInfo(userUpd);
    }
  }
}
