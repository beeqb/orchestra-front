import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:orchestra/repository/user_repository.dart';

import '../../injection_container.dart';
import '../../models/user.dart';
import '../../pages/profile.dart';

class DesktopXApiKeyTab extends StatefulWidget {
  final UserModel currentUser;

  const DesktopXApiKeyTab(
    this.currentUser, {
    Key? key,
  }) : super(key: key);

  @override
  _DesktopXApiKeyTabState createState() => _DesktopXApiKeyTabState();
}

class _DesktopXApiKeyTabState extends State<DesktopXApiKeyTab> {
  late String xApikey;

  @override
  void initState() {
    xApikey = widget.currentUser.apiKey ?? 'x-api-key not found';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'X-API KEY',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const SizedBox(
            height: 32,
          ),
          Text('X-API KEY', style: Theme.of(context).textTheme.headlineMedium),
          const SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(
                        color: Theme.of(context)
                                .inputDecorationTheme
                                .disabledBorder
                                ?.borderSide
                                .color ??
                            Theme.of(context).disabledColor,
                        width: 1.0),
                    // No such attribute
                  ),
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
                  child: Text(
                    xApikey,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              TextButton(
                  onPressed: () {
                    Clipboard.setData(
                      ClipboardData(text: xApikey),
                    );
                    showMessage(
                      context,
                      'x-api-key copied to clipboard',
                      Colors.green,
                      const Duration(seconds: 1),
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 24),
                    child: Text(
                      'Copy',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                  style: Theme.of(context).textButtonTheme.style?.copyWith(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).cardColor),
                      ))
            ],
          ),
          const SizedBox(
            height: 32,
          ),
          TextButton(
              onPressed: () async {
                final newKey = await sl<UserRepository>().generateApiKey();
                setState(() {
                  xApikey = newKey;
                });
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
                child: Text(
                  'Generate new key',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
              style: Theme.of(context).textButtonTheme.style?.copyWith(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).cardColor),
                  ))
        ],
      ),
    );
  }
}

class MobileXApiKeyTab extends StatefulWidget {
  final UserModel currentUser;

  const MobileXApiKeyTab(
    this.currentUser, {
    Key? key,
  }) : super(key: key);

  @override
  _MobileXApiKeyTabState createState() => _MobileXApiKeyTabState();
}

class _MobileXApiKeyTabState extends State<MobileXApiKeyTab> {
  late String xApikey;

  @override
  void initState() {
    xApikey = widget.currentUser.apiKey ?? 'x-api-key not found';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.all(14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'X-API KEY',
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: 25,
          ),
          Row(
            children: [
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    border: Border.all(
                        color: Theme.of(context)
                                .inputDecorationTheme
                                .disabledBorder
                                ?.borderSide
                                .color ??
                            Theme.of(context).disabledColor,
                        width: 1.0),
                    // No such attribute
                  ),
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
                  child: Text(
                    xApikey,
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                ),
              ),
              const SizedBox(
                width: 15,
              ),
              TextButton(
                  onPressed: () {
                    Clipboard.setData(
                      ClipboardData(text: xApikey),
                    );
                    showMessage(
                      context,
                      'x-api-key copied to clipboard',
                      Colors.green,
                      const Duration(seconds: 1),
                    );
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Icon(
                      Icons.copy_rounded,
                      color: Theme.of(context).primaryIconTheme.color,
                      size: 20,
                    ),
                  ),
                  style: Theme.of(context).textButtonTheme.style?.copyWith(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).cardColor),
                      ))
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          TextButton(
              onPressed: () async {
                final newKey = await sl<UserRepository>().generateApiKey();
                setState(() {
                  xApikey = newKey;
                });
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 24),
                child: Text(
                  'Generate new key',
                  style: Theme.of(context).textTheme.labelSmall,
                ),
              ),
              style: Theme.of(context).textButtonTheme.style?.copyWith(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).cardColor),
                  ))
        ],
      ),
    );
  }
}
