import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../core/const.dart';
import '../../core/scroll_behavior.dart';
import '../../models/user.dart';
import '../../pages/profile.dart';

class DesktopReferralProgramTab extends StatelessWidget {
  final UserModel currentUser;

  const DesktopReferralProgramTab(this.currentUser, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dateFormatWithTime = DateFormat('MMM yyyy');
    final date = DateTime.now();
    var _referralUrl = currentUser.referralUrl ??
        '${UrlConstants.kReferralUrl}${currentUser.referralToken}';
    var _debugRefUrl =
        'http://localhost:5000/#/onBoarding?ref=${currentUser.referralToken}';
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: const EdgeInsets.all(32),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Referral program',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            const SizedBox(
              height: 32,
            ),
            SizedBox(
              height: 120,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Flexible(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Theme.of(context).secondaryHeaderColor,
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "\$0.00",
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              Container(
                                  width: 36.35,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                    vertical: 4,
                                  ),
                                  child: Text("0% ",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium)),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          RichText(
                            text: TextSpan(
                                text: "Available for ",
                                style: Theme.of(context)
                                    .textTheme
                                    .labelMedium
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color),
                                children: [
                                  TextSpan(
                                    text: "withdrawal",
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelMedium
                                        ?.copyWith(
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                        ),
                                  )
                                ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Flexible(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Theme.of(context).secondaryHeaderColor,
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${currentUser.referralCount.toString()} Users',
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              Container(
                                  width: 36.35,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                    vertical: 4,
                                  ),
                                  child: Text("0% ",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium)),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text('Visitors',
                              style: Theme.of(context)
                                  .textTheme
                                  .labelMedium
                                  ?.copyWith(
                                      color: Theme.of(context).iconTheme.color))
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Flexible(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Theme.of(context).secondaryHeaderColor,
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '${currentUser.referralCount.toString()} Users',
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                              Container(
                                  width: 36.35,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                    vertical: 4,
                                  ),
                                  child: Text(
                                    "0% ",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  )),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text('Referrals',
                              style: Theme.of(context)
                                  .textTheme
                                  .labelMedium
                                  ?.copyWith(
                                      color: Theme.of(context).iconTheme.color))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 44,
            ),
            Text(
              'Details',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            Column(
              children: [
                SizedBox(
                  height: 40,
                  child: Row(
                    children: [
                      const Expanded(flex: 2, child: SizedBox()),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text(
                          dateFormatWithTime.format(
                              DateTime(date.year, date.month - 3, date.day)),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall
                              ?.copyWith(
                                  color: Theme.of(context).iconTheme.color),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          dateFormatWithTime.format(
                              DateTime(date.year, date.month - 2, date.day)),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall
                              ?.copyWith(
                                  color: Theme.of(context).iconTheme.color),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          dateFormatWithTime.format(
                              DateTime(date.year, date.month - 1, date.day)),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall
                              ?.copyWith(
                                  color: Theme.of(context).iconTheme.color),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          dateFormatWithTime.format(DateTime.now()),
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall
                              ?.copyWith(
                                  color: Theme.of(context).iconTheme.color),
                        ),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                SizedBox(
                  height: 44,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          'Referral payments',
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text('0',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('0',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('0',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('0',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                SizedBox(
                  height: 44,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          'Income',
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                SizedBox(
                  height: 44,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          'Available for withdraw',
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                    ],
                  ),
                ),
                const Divider(),
                SizedBox(
                  height: 44,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          'Withdrawal',
                          style: Theme.of(context).textTheme.labelMedium,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                      Expanded(
                        child: Text('\$0.00',
                            style: Theme.of(context).textTheme.labelSmall),
                      ),
                    ],
                  ),
                ),
                const Divider(),
              ],
            ),
            const SizedBox(
              height: 44,
            ),
            Text(
              'Share your link',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Theme.of(context).disabledColor, width: 1.0),
                      // No such attribute
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 24),
                    child: Text(
                      _referralUrl,
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                TextButton(
                    onPressed: () {
                      Clipboard.setData(
                        ClipboardData(text: _referralUrl),
                      );
                      showMessage(
                        context,
                        'Url copied to clipboard',
                        Colors.green,
                        const Duration(seconds: 1),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12, horizontal: 24),
                      child: Text(
                        'Copy',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ),
                    style: Theme.of(context).textButtonTheme.style?.copyWith(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).cardColor),
                        ))
              ],
            ),
            const SizedBox(
              height: 44,
            ),
            Text(
              'Referral token',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Theme.of(context)
                                  .inputDecorationTheme
                                  .disabledBorder
                                  ?.borderSide
                                  .color ??
                              Theme.of(context).disabledColor,
                          width: 1.0),
                      // No such attribute
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 24),
                    child: Text(
                      currentUser.referralToken!,
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                TextButton(
                    onPressed: () {
                      Clipboard.setData(
                        ClipboardData(text: currentUser.referralToken!),
                      );
                      showMessage(
                        context,
                        'Referral token copied to clipboard',
                        Colors.green,
                        const Duration(seconds: 1),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 12, horizontal: 24),
                      child: Text(
                        'Copy',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ),
                    style: Theme.of(context).textButtonTheme.style?.copyWith(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).cardColor),
                        ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class MobileReferralProgramTab extends StatelessWidget {
  final UserModel currentUser;

  const MobileReferralProgramTab(this.currentUser, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dateFormatWithTime = DateFormat('MMM yyyy');
    final date = DateTime.now();
    var _referralUrl = currentUser.referralUrl ??
        '${UrlConstants.kReferralUrl}${currentUser.referralToken}';
    var _debugRefUrl =
        'http://localhost:5000/#/onBoarding?ref=${currentUser.referralToken}';
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.all(14),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Referral program',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
                height: 75,
                child: ScrollConfiguration(
                  behavior: MyCustomScrollBehavior(),
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      Container(
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "\$0.00",
                                  style:
                                      Theme.of(context).textTheme.labelMedium,
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Theme.of(context).backgroundColor,
                                    ),
                                    padding: const EdgeInsets.all(5),
                                    child: Text(
                                      "0%",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                    )),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            RichText(
                              text: TextSpan(
                                  text: "Available for ",
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelSmall
                                      ?.copyWith(
                                          color: Theme.of(context)
                                              .iconTheme
                                              .color),
                                  children: [
                                    TextSpan(
                                      text: "withdrawal",
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelSmall
                                          ?.copyWith(
                                              color: Theme.of(context)
                                                  .primaryColorDark),
                                    )
                                  ]),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  '${currentUser.referralCount.toString()} Users',
                                  style:
                                      Theme.of(context).textTheme.labelMedium,
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Theme.of(context).backgroundColor,
                                    ),
                                    padding: const EdgeInsets.all(5),
                                    child: Text(
                                      "0%",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                    )),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text('Visitors',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color))
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Theme.of(context).secondaryHeaderColor,
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  '${currentUser.referralCount.toString()} Users',
                                  style:
                                      Theme.of(context).textTheme.labelMedium,
                                ),
                                Container(
                                    width: 29,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Theme.of(context).backgroundColor,
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 4,
                                      vertical: 4,
                                    ),
                                    child: Text("0% ",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium)),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text('Referrals',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color))
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
            const SizedBox(
              height: 44,
            ),
            Text(
              'Details',
              style: Theme.of(context).textTheme.labelMedium,
            ),
            ScrollConfiguration(
              behavior: MyCustomScrollBehavior(),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SizedBox(
                  width: 500,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            const Expanded(flex: 2, child: SizedBox()),
                            Expanded(
                              flex: 1,
                              child: Text(
                                dateFormatWithTime.format(DateTime(
                                    date.year, date.month - 3, date.day)),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text(
                                dateFormatWithTime.format(DateTime(
                                    date.year, date.month - 2, date.day)),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text(
                                dateFormatWithTime.format(DateTime(
                                    date.year, date.month - 1, date.day)),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text(
                                dateFormatWithTime.format(DateTime.now()),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall
                                    ?.copyWith(
                                        color:
                                            Theme.of(context).iconTheme.color),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Text(
                                'Referral payments',
                                style: Theme.of(context).textTheme.labelSmall,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('0',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('0',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('0',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('0',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Text(
                                'Income',
                                style: Theme.of(context).textTheme.labelSmall,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Text(
                                'Available for withdraw',
                                style: Theme.of(context).textTheme.labelSmall,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      SizedBox(
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 2,
                              child: Text(
                                'Withdrawal',
                                style: Theme.of(context).textTheme.labelSmall,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                            Expanded(
                              flex: 1,
                              child: Text('\$0.00',
                                  style:
                                      Theme.of(context).textTheme.labelSmall),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 32,
            ),
            Text(
              'Share your link',
              style: Theme.of(context)
                  .textTheme
                  .labelSmall
                  ?.copyWith(color: Theme.of(context).iconTheme.color),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Theme.of(context)
                                  .inputDecorationTheme
                                  .disabledBorder
                                  ?.borderSide
                                  .color ??
                              Theme.of(context).disabledColor,
                          width: 1.0),
                      // No such attribute
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 24),
                    child: Text(
                      _referralUrl,
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                TextButton(
                    onPressed: () {
                      Clipboard.setData(
                        ClipboardData(text: _referralUrl),
                      );
                      showMessage(
                        context,
                        'Url copied to clipboard',
                        Colors.green,
                        const Duration(seconds: 1),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Icon(
                        Icons.copy_rounded,
                        size: 20,
                        color: Theme.of(context).primaryIconTheme.color,
                      ),
                    ),
                    style: Theme.of(context).textButtonTheme.style?.copyWith(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).cardColor),
                        ))
              ],
            ),
            const SizedBox(
              height: 44,
            ),
            Text(
              'Referral token',
              style: Theme.of(context)
                  .textTheme
                  .labelSmall
                  ?.copyWith(color: Theme.of(context).iconTheme.color),
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              children: [
                Flexible(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      border: Border.all(
                          color: Theme.of(context)
                                  .inputDecorationTheme
                                  .disabledBorder
                                  ?.borderSide
                                  .color ??
                              Theme.of(context).disabledColor,
                          width: 1.0),
                      // No such attribute
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 12, horizontal: 24),
                    child: Text(
                      currentUser.referralToken!,
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                TextButton(
                    onPressed: () {
                      Clipboard.setData(
                        ClipboardData(text: currentUser.referralToken!),
                      );
                      showMessage(
                        context,
                        'Referral token copied to clipboard',
                        Colors.green,
                        const Duration(seconds: 1),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Icon(
                        Icons.copy_rounded,
                        color: Theme.of(context).primaryIconTheme.color,
                        size: 20,
                      ),
                    ),
                    style: Theme.of(context).textButtonTheme.style?.copyWith(
                          backgroundColor: MaterialStateProperty.all<Color>(
                              Theme.of(context).cardColor),
                        ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
