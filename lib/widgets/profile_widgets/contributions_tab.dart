import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/contributor_bloc.dart';
import '../../blocs/cubits/user_cubit.dart';
import '../../injection_container.dart';
import '../../models/enums/confirm_action_type.dart';
import '../../models/user.dart';
import '../../services/dialogs_service.dart';
import '../../services/user_service.dart';
import '../dialogs/contributor_dialog.dart';

class DesktopContributionsTab extends StatefulWidget {
  final UserModel currentUser;

  const DesktopContributionsTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  _DesktopContributionsTabState createState() =>
      _DesktopContributionsTabState();
}

class _DesktopContributionsTabState extends State<DesktopContributionsTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(15.0),
      ),
      padding: const EdgeInsets.all(32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Contributions',
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          const SizedBox(
            height: 32,
          ),
          BlocProvider(
            create: (context) => ContributorBloc(
              userCubit: sl<UserCubit>(),
              userService: sl<UserService>(),
              dialogsService: sl<DialogsService>(),
            )..add(SetContributorEvent(
                isContributor: widget.currentUser.contributor!)),
            child: BlocBuilder<ContributorBloc, ContributorState>(
              builder: (context, stateC) {
                if (stateC is ContributorUpdating) {
                  return const CircularProgressIndicator();
                }
                if (stateC is ContributorUpdated) {
                  if (!stateC.isContributor) {
                    return Column(
                      children: [
                        Text(
                          'If you want to add your own Lore into Orchestra Marketplace, you should be a Contributor. Be careful it\'s payable. Cost \$19.99',
                          style: Theme.of(context).textTheme.displaySmall,
                        ),
                        const SizedBox(
                          height: 44,
                        ),
                        TextButton(
                            onPressed: () async {
                              final confirmAction =
                                  await _showContributorDialog();
                              if (confirmAction == ConfirmAction.accept) {
                                BlocProvider.of<ContributorBloc>(context)
                                    .add(BecomeContributorEvent());
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 24),
                              child: Text(
                                'I want to be a contributor',
                                style:
                                    Theme.of(context).textTheme.headlineMedium,
                              ),
                            ),
                            style: Theme.of(context)
                                .textButtonTheme
                                .style
                                ?.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Theme.of(context).cardColor),
                                )),
                      ],
                    );
                  }
                  return Text(
                    'You are a contributor',
                    style: Theme.of(context).textTheme.headlineMedium,
                  );
                }
                return const SizedBox.shrink();
              },
            ),
          ),
          // const SizedBox(
          //   height: 50,
          // ),
          // TextButton(
          //     onPressed: () {
          //       sl<NavigationBLoC>().add(const ToAddProductNavigationEvent());
          //     },
          //     child: const Text('Add lore')),
          // TextButton(
          //     onPressed: () {
          //       sl<NavigationBLoC>().add(const ToAddedProductsEvent());
          //     },
          //     child: const Text('Added lores')),
          // TextButton(
          //     onPressed: () {
          //       sl<NavigationBLoC>().add(const ToAdminNavigationEvent());
          //     },
          //     child: const Text('Admin page'))
        ],
      ),
    );
  }

  Future<dynamic> _showContributorDialog() async => showDialog(
        context: context,
        builder: (context) => const AlertDialog(
          content: ContributorPaymentDialog(),
        ),
      );
}

class MobileContributionsTab extends StatefulWidget {
  final UserModel currentUser;

  const MobileContributionsTab(this.currentUser, {Key? key}) : super(key: key);

  @override
  _MobileContributionsTabState createState() => _MobileContributionsTabState();
}

class _MobileContributionsTabState extends State<MobileContributionsTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      padding: const EdgeInsets.all(14),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Contributions',
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: 20,
          ),
          BlocProvider(
            create: (context) => ContributorBloc(
              userCubit: sl<UserCubit>(),
              userService: sl<UserService>(),
              dialogsService: sl<DialogsService>(),
            )..add(SetContributorEvent(
                isContributor: widget.currentUser.contributor!)),
            child: BlocBuilder<ContributorBloc, ContributorState>(
              builder: (context, stateC) {
                if (stateC is ContributorUpdating) {
                  return const CircularProgressIndicator();
                }
                if (stateC is ContributorUpdated) {
                  if (!stateC.isContributor) {
                    return Column(
                      children: [
                        Text(
                          'If you want to add your own Lore into Orchestra Marketplace, you should be a Contributor. Be careful it\'s payable. Cost \$19.99',
                          style: Theme.of(context).textTheme.labelSmall,
                        ),
                        const Spacer(),
                        SizedBox(
                          width: double.infinity,
                          child: TextButton(
                              onPressed: () async {
                                final confirmAction =
                                    await _showContributorDialog();
                                if (confirmAction == ConfirmAction.accept) {
                                  BlocProvider.of<ContributorBloc>(context)
                                      .add(BecomeContributorEvent());
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 24),
                                child: Text(
                                  'I want to be a contributor',
                                  style: Theme.of(context).textTheme.labelSmall,
                                ),
                              ),
                              style: Theme.of(context)
                                  .textButtonTheme
                                  .style
                                  ?.copyWith(
                                    backgroundColor:
                                        MaterialStateProperty.all<Color>(
                                            Theme.of(context).cardColor),
                                  )),
                        ),
                      ],
                    );
                  }
                  return Text(
                    'You are a contributor',
                    style: Theme.of(context).textTheme.labelSmall,
                  );
                }
                return const SizedBox.shrink();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<dynamic> _showContributorDialog() async => showDialog(
        context: context,
        builder: (context) => const AlertDialog(
          content: ContributorPaymentDialog(),
        ),
      );
}
