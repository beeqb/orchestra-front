import 'package:flutter/material.dart';

import '../../models/rating_item_model.dart';
import '../../util/text_util.dart';
import '../marketplace_widgets/star_rating.dart';

class CommentInfoDialog extends StatelessWidget {
  final String? title;
  final String buttonLabel;
  final RatingItem ratingItem;

  const CommentInfoDialog({
    Key? key,
    this.title,
    required this.ratingItem,
    this.buttonLabel = 'Ok',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        width: 600,
        alignment: Alignment.center,
        child: Container(
          margin: const EdgeInsets.all(8.0),
          padding: const EdgeInsets.all(20.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Theme.of(context).dialogBackgroundColor,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const SizedBox(height: 10.0),
              Text(
                title!,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const Divider(),
              SingleChildScrollView(
                child: Column(
                  children: [
                    commentHeader(),
                    Text(ratingItem.comment!,
                        style: Theme.of(context).textTheme.bodySmall),
                    const SizedBox(height: 10.0),
                    ratingItem.answer!.isNotEmpty
                        ? SizedBox(
                            width: double.infinity,
                            child: Text('Developer response',
                                style: Theme.of(context).textTheme.labelSmall),
                          )
                        : const SizedBox(width: 1),
                    ratingItem.answer!.isNotEmpty
                        ? Text(
                            ratingItem.answer!,
                            style: Theme.of(context).textTheme.bodySmall,
                          )
                        : const SizedBox.shrink(),
                  ],
                ),
              ),
              const SizedBox(height: 40.0),
              SizedBox(
                width: double.infinity,
                child: TextButton(
                  style: TextButton.styleFrom(
                    padding: const EdgeInsets.all(5.0),
                  ),
                  onPressed: () => Navigator.pop(context, true),
                  child: Text(buttonLabel),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Padding commentHeader() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 6.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          StarRating(
            rating: ratingItem.rank.toDouble(),
            iconSize: 12,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(ratingItem.user!.name),
              Text(
                dateFormat.format(DateTime.parse(ratingItem.createdAt!)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
