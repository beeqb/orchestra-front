import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/sliding_page_bloc.dart';
import 'package:orchestra/injection_container.dart';

import '../../models/enums/enums.dart';

class WelcomeBonusDialog extends StatelessWidget {
  final DialogType type;
  final String title;
  final String content;
  final Widget? icon;
  final String buttonLabel;

  const WelcomeBonusDialog(
      {Key? key,
      this.title = 'Successful',
      required this.content,
      this.icon,
      this.type = DialogType.infoDialog,
      this.buttonLabel = 'Ok'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        width: 600,
        alignment: Alignment.center,
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.all(8.0),
              padding: const EdgeInsets.all(20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Theme.of(context).dialogBackgroundColor,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const SizedBox(height: 10.0),
                  icon ??
                      Icon(
                        type.icon,
                        color: type.color,
                        size: 50,
                      ),
                  const SizedBox(height: 10.0),
                  Text(
                    title,
                    style: Theme.of(context).textTheme.headline5,
                    textAlign: TextAlign.center,
                  ),
                  const Divider(),
                  Text(
                    content,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 40.0),
                  SizedBox(
                    width: double.infinity,
                    child: TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(5.0),
                      ),
                      onPressed: () {
                        Navigator.of(context).maybePop();
                        try {
                          sl<SlidingPageBLoC>()
                              .add(const CloseSlidingPageEvent());
                        } on Exception catch (e) {
                          debugPrint('error $e');
                        }
                        // sl<NavigationBLoC>()
                        //     .add(const ToLoginNavigationEvent());
                      },
                      child: Text(
                        buttonLabel,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
                right: 15.0,
                top: 15.0,
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: const Icon(Icons.close),
                )),
          ],
        ),
      ),
    );
  }
}
