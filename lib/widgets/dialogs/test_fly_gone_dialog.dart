import 'package:flutter/material.dart';
import 'package:orchestra/models/product/product.dart';

import '../../util/text_util.dart';
import '../product_image_widget.dart';

class TestFlyGoneDialog extends StatelessWidget {
  final Product product;
  final Function onPressed;
  final Function onCancel;

  const TestFlyGoneDialog({
    Key? key,
    required this.product,
    required this.onCancel,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var price2 = product.pricingAndBusiness.price;
    return SizedBox(
      width: 600,
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          alignment: Alignment.center,
          child: Container(
            margin: const EdgeInsets.all(8.0),
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Theme.of(context).dialogBackgroundColor,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const SizedBox(height: 10.0),
                ProductImageWidget(
                  imageUrl: product.productLogoURL,
                  height: 50,
                  width: 50,
                ),
                const SizedBox(height: 10.0),
                Text(
                  product.productName,
                  style: Theme.of(context).textTheme.headlineMedium,
                  textAlign: TextAlign.center,
                ),
                const Divider(),
                const Text(
                  'You can not use this widget for free any longer. '
                  'Test-fly is gone, sorry!\n'
                  'But, if you like it, pay for it.',
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 40.0),
                SizedBox(
                  width: double.infinity,
                  child: Row(
                    children: [
                      Expanded(
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            padding: const EdgeInsets.all(5.0),
                          ),
                          onPressed: () => onCancel(),
                          child: Text(
                            'Lore page',
                            style: Theme.of(context).textTheme.labelMedium,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            padding: const EdgeInsets.all(5.0),
                          ),
                          onPressed: () => onPressed(),
                          child: Text('Pay ${formatCurrency.format(price2)}'),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
