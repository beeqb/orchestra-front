import 'package:flutter/material.dart';

import '../../models/enums/enums.dart';

class CustomAlertDialog extends StatelessWidget {
  final String message;
  const CustomAlertDialog({
    Key? key,
    required this.message,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).backgroundColor,
      content: Text(
        message,
        style: Theme.of(context).textTheme.headlineMedium,
      ),
      actions: [
        Row(
          children: [
            Expanded(
              child: OutlinedButton(
                onPressed: () {
                  Navigator.of(context).pop(ConfirmAction.cancel);
                },
                style: OutlinedButton.styleFrom(
                  side: BorderSide(
                    width: 1.0,
                    color: Theme.of(context).errorColor,
                  ),
                ),
                child: SizedBox(
                  // width: 140,
                  height: 40,
                  child: Center(
                    child: Text(
                      'Cancel',
                      style: Theme.of(context)
                          .textTheme
                          .labelSmall
                          ?.copyWith(color: Theme.of(context).errorColor),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop(ConfirmAction.accept);
                },
                child: SizedBox(
                    //  width: 140,
                    height: 40,
                    child: Center(
                        child: Text(
                      'OK',
                      style: Theme.of(context)
                          .textTheme
                          .labelSmall
                          ?.copyWith(color: Theme.of(context).backgroundColor),
                    ))),
              ),
            ),
          ],
        )
      ],
    );
  }
}
