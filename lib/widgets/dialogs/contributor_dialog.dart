import 'package:flutter/material.dart';

import '../../models/enums/enums.dart';

class ContributorPaymentDialog extends StatelessWidget {
  const ContributorPaymentDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400,
      height: 300,
      color: Theme.of(context).dialogBackgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(height: 10.0),
          const Icon(
            Icons.warning_rounded,
            color: Colors.orange,
            size: 50,
          ),
          const SizedBox(height: 10.0),
          Text(
            'Contributor fee',
            style: Theme.of(context).textTheme.titleSmall,
          ),
          const Divider(),
          Text(
              'Saying "Yes", you confirm that you want to become an Orchestra'
              ' contributor. After clicking on the "Save changes" button, a '
              'one-time payment of \$19.99 will be debited from your account'
              ' and you will be able to add your application to the '
              'Marketplace.',
              style: Theme.of(context).textTheme.bodySmall),
          const Spacer(),
          _buttonsPanel(context)
        ],
      ),
    );
  }
}

Widget _buttonsPanel(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Flexible(
        child: OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            height: 40,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
      ),
      const SizedBox(
        width: 12,
      ),
      Flexible(
        child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).pop(ConfirmAction.accept);
          },
          child: SizedBox(
              height: 40,
              child: Center(
                  child: Text(
                'Pay \$19,99',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ),
    ],
  );
}
