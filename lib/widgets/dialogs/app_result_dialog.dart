import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:orchestra/util/text_util.dart';

class AppResultDialog extends StatelessWidget {
  final String title;
  final String result;

  const AppResultDialog({
    Key? key,
    required this.title,
    required this.result,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        width: 600,
        color: Theme.of(context).backgroundColor,
        alignment: Alignment.center,
        child: Container(
            margin: const EdgeInsets.all(8.0),
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Theme.of(context).dialogBackgroundColor,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                dialogHeader(context),
                const Divider(thickness: 0.5),
                const SizedBox(height: 10),
                HtmlWidget(
                  corsRefactoring(result),
                ),
                const SizedBox(height: 20),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    'Ok',
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                ),
              ],
            )),
      ),
    );
  }

  Padding dialogHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
        left: 10,
        right: 10,
      ),
      child: Row(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Icon(
              MdiIcons.checkOutline,
              color: Colors.green,
            ),
          ),
          Expanded(
            child: Text(
              title,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
        ],
      ),
    );
  }
}
