import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String? title;
  final bool? backButton;
  final List<Widget>? actions;
  final PreferredSize? bottom;

  const CustomAppBar({
    Key? key,
    this.title,
    this.backButton,
    this.actions,
    this.bottom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: backButton == null
          ? const Icon(
              Icons.menu,
              color: Colors.transparent,
            )
          : null,
      automaticallyImplyLeading: backButton ?? false,
      title: title != null
          ? Text(
              title!,
              style: Theme.of(context).textTheme.headline6,
            )
          : null,
      centerTitle: false,
      elevation: 0,
      //  backgroundColor: Color(0xFFEFEFEF),
      actions: actions,
      bottom: bottom,
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  @override
  Size get preferredSize =>
      bottom != null ? const Size.fromHeight(100) : const Size.fromHeight(50);
}
