// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import '../../../blocs/bloc/app_timer_bloc.dart';
// import '../../../models/app.dart';
// import '../../../util/text_util.dart';
// import 'timer_panel_widget.dart';
//
// class RunningTimeWidget extends StatefulWidget {
//   final AppModel app;
//   const RunningTimeWidget({
//     Key? key,
//     required this.app,
//   }) : super(key: key);
//
//   @override
//   _RunningTimeWidgetState createState() => _RunningTimeWidgetState();
// }
//
// class _RunningTimeWidgetState extends State<RunningTimeWidget> {
//   bool _isRunningTime = true;
//
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: MediaQuery.of(context).size.width,
//       child: Center(
//         child: Container(
//           color: Colors.transparent,
//           height: 35,
//           width: 170,
//           child: OutlinedButton(
//             style: OutlinedButton.styleFrom(
//               primary: Theme.of(context).textTheme.bodyText1!.color,
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.circular(18.0),
//               ),
//             ),
//             onPressed: () {
//               setState(() {
//                 _isRunningTime = !_isRunningTime;
//               });
//             },
//             child: Stack(
//               children: [
//                 _isRunningTime ? const RunningTimePanel() : const SizedBox(height: 1),
//                 !_isRunningTime
//                     ? runningCostPanel()
//                     : const SizedBox(height: 1),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget runningCostPanel() {
//     return Container(
//       height: double.infinity,
//       decoration: const BoxDecoration(),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.center,
//         mainAxisSize: MainAxisSize.max,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           const Text(
//             'Running costs:',
//             style: TextStyle(
//               fontSize: 10,
//             ),
//           ),
//           const SizedBox(
//             width: 5,
//           ),
//           BlocBuilder<AppTimerBloc, AppTimerState>(
//             builder: (context, state) {
//               return Text(
//                 state is AppTimerComplete
//                     ? formatCurrency.format(state.runningCost)
//                     : 'calculate...',
//                 style: const TextStyle(
//                   fontSize: 10,
//                 ),
//               );
//             },
//           ),
//         ],
//       ),
//     );
//   }
// }
