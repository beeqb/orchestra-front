// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import '../../../blocs/bloc/app_timer_bloc.dart';
//
// class RunningTimePanel extends StatelessWidget {
//   const RunningTimePanel({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.center,
//       mainAxisSize: MainAxisSize.max,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: <Widget>[
//         const Text(
//           'Running time:',
//           style: TextStyle(
//             fontSize: 10,
//           ),
//         ),
//         const SizedBox(
//           width: 5,
//         ),
//         BlocBuilder<AppTimerBloc, AppTimerState>(
//           builder: (context, state) {
//             if (state is AppTimerInitial) {
//               return const Text(
//                 '--:--:--',
//                 style: TextStyle(
//                   fontSize: 10,
//                 ),
//               );
//             } else if (state is AppTimerInProgress) {
//               return Text(
//                 state.runningTime,
//                 style: const TextStyle(
//                   fontSize: 10,
//                 ),
//               );
//             } else if (state is AppTimerComplete) {
//               return Text(
//                 state.runningTime,
//                 style: const TextStyle(
//                   fontSize: 10,
//                 ),
//               );
//             }
//             return Container();
//           },
//         ),
//       ],
//     );
//   }
// }
