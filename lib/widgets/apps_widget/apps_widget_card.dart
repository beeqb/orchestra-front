// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:orchestra/models/product/product.dart';
// import '../../blocs/cubits/app_widgets_cubit.dart';
// import '../../config.dart';
// import '../../models/app.dart';
// import '../loading.dart';
//
// class WidgetCard extends StatelessWidget {
//   final Product product;
//   final AppModel app;
//   const WidgetCard({Key? key, required this.product, required this.app})
//       : super(key: key);
//
//   String get getWidgetAvatarUrl => '$kWidgetAvatarUrl${product.productLogoURL}';
//
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 10.0),
//       child: Row(
//         children: [
//           ClipRRect(
//             borderRadius: BorderRadius.circular(10.0),
//             child: CachedNetworkImage(
//               height: 50,
//               width: 50,
//               imageUrl: getWidgetAvatarUrl,
//               placeholder: (context, url) => const Loading(),
//               errorWidget: (context, url, error) => Text(error.toString()),
//             ),
//           ),
//           Expanded(
//             child: Padding(
//               padding: const EdgeInsets.symmetric(horizontal: 20.0),
//               child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(product.productName),
//                     Text(
//                       product.productDescription,
//                       style: const TextStyle(color: Colors.black54),
//                     ),
//                   ]),
//             ),
//           ),
//           SizedBox(
//             width: 80,
//             child: CupertinoButton(
//               onPressed: () {
//                 Navigator.pop(context);
//                 BlocProvider.of<AppWidgetsCubit>(context)
//                     .addWidgetToApp(product, app.id!, app.userId);
//               },
//               child: const Text('Add'),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
