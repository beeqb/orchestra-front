import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/util/text_util.dart';

import '../../blocs/cubits/stats_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/daily_sale.dart';

class DailySalesWidget extends StatelessWidget {
  const DailySalesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: StatisticsConstants.rowWidth,
      height: StatisticsConstants.firstRowHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).backgroundColor,
      ),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Daily  Sales',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'Check out each column for more details',
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
          // const Spacer(),
          const DailySalesChart(),
        ],
      ),
    );
  }
}

class DailySalesChart extends StatefulWidget {
  const DailySalesChart({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => DailySalesChartState();
}

class DailySalesChartState extends State<DailySalesChart> {
  @override
  void initState() {
    sl<StatsCubit>().getUserDailySales();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StatsCubit, StatsState>(
      builder: (context, state) {
        if (state.salesStatisticsLoadingState.isCompleted) {
          return AspectRatio(
            aspectRatio: 2.5,
            child: Card(
              elevation: 0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4)),
              color: Colors.transparent,
              child: _BarChart(state.dailySales),
            ),
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}

class _BarChart extends StatefulWidget {
  const _BarChart(this.dailySales);

  final List<DailySales>? dailySales;

  @override
  State<_BarChart> createState() => _BarChartState();
}

class _BarChartState extends State<_BarChart> {
  int touchedIndex = -1;
  static const _barColor = Color(0XFF333342);
  static const _barActiveColor = const Color(0XFF24DF9B);

  @override
  Widget build(BuildContext context) {
    return BarChart(
      BarChartData(
        barTouchData: barTouchData,
        titlesData: titlesData,
        borderData: FlBorderData(
          show: false,
        ),
        barGroups: showingGroups,
        gridData: FlGridData(show: false),
        alignment: BarChartAlignment.spaceAround,
        maxY: 20,
      ),
    );
  }

  BarTouchData get barTouchData => BarTouchData(
        enabled: true,
        touchCallback: (FlTouchEvent event, barTouchResponse) {
          setState(() {
            if (!event.isInterestedForInteractions ||
                barTouchResponse == null ||
                barTouchResponse.spot == null) {
              touchedIndex = -1;
              return;
            }
            touchedIndex = barTouchResponse.spot!.touchedBarGroupIndex;
          });
        },
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.blueGrey,
          getTooltipItem: (group, groupIndex, rod, rodIndex) {
            String weekDay;
            switch (group.x) {
              case 0:
                weekDay = 'Monday';
                break;
              case 1:
                weekDay = 'Tuesday';
                break;
              case 2:
                weekDay = 'Wednesday';
                break;
              case 3:
                weekDay = 'Thursday';
                break;
              case 4:
                weekDay = 'Friday';
                break;
              case 5:
                weekDay = 'Saturday';
                break;
              case 6:
                weekDay = 'Sunday';
                break;
              default:
                throw Error();
            }
            return BarTooltipItem(
              '',
              const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              children: <TextSpan>[
                TextSpan(
                  text: (rod.toY - 1).toString(),
                  style: const TextStyle(
                    color: _barActiveColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            );
          },
        ),
      );

  Widget getTitles(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Color(0XFF6C6C6E),
      fontWeight: FontWeight.w500,
      fontSize: 10,
    );
    return SideTitleWidget(
        axisSide: meta.axisSide,
        space: 4,
        child: Column(children: [
          Text(getDayNumber((value.toInt() - 6).abs()), style: style),
          Text(getWeekDay((value.toInt() - 6).abs()), style: style),
        ]));
  }

  FlTitlesData get titlesData => FlTitlesData(
        show: true,
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 30,
            getTitlesWidget: getTitles,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        rightTitles: AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
      );

  BarChartGroupData makeGroupData(
    int x,
    double y, {
    bool isTouched = false,
    Color barColor = _barColor,
    double width = 24,
    List<int> showTooltips = const [],
  }) {
    return BarChartGroupData(
      x: x,
      barRods: [
        BarChartRodData(
          toY: isTouched ? y + 1 : y,
          color: isTouched ? _barActiveColor : barColor,
          width: width,
          borderRadius: BorderRadius.circular(3),
          backDrawRodData: BackgroundBarChartRodData(
            show: true,
            toY: y,
            color: barColor,
          ),
        ),
      ],
      showingTooltipIndicators: showTooltips,
    );
  }

  List<BarChartGroupData> get showingGroups => List.generate(7, (i) {
        switch (i) {
          case 0:
            return makeGroupData(0, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 1:
            return makeGroupData(1, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 2:
            return makeGroupData(2, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 3:
            return makeGroupData(3, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 4:
            return makeGroupData(4, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 5:
            return makeGroupData(5, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          case 6:
            return makeGroupData(6, widget.dailySales![i].sales.toDouble(),
                isTouched: i == touchedIndex);
          default:
            return throw Error();
        }
      });
}
