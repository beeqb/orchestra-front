import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/cubits/stats_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';

class CustomerActivityWidget extends StatefulWidget {
  const CustomerActivityWidget({Key? key}) : super(key: key);

  @override
  State<CustomerActivityWidget> createState() => _CustomerActivityWidgetState();
}

class _CustomerActivityWidgetState extends State<CustomerActivityWidget> {
  void initState() {
    sl<StatsCubit>().getCustomerActivity();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: StatisticsConstants.rowWidth,
      height: StatisticsConstants.firstRowHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).backgroundColor,
      ),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 24),
      child: BlocBuilder<StatsCubit, StatsState>(builder: (context, state) {
        if (state.customerActivityLoadingState.isCompleted) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  'Customer Activity',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                'Conversion rate',
                style: Theme.of(context)
                    .textTheme
                    .labelMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'TestFlighters',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Theme.of(context).cardColor,
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 4,
                    ),
                    child: Text(
                        state.customerActivity?.testFly != null
                            ? '${state.customerActivity?.testFly.toString()}'
                            : '',
                        style: Theme.of(context).textTheme.labelMedium),
                  )
                ],
              ),
              const SizedBox(
                height: 12,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Customers',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Theme.of(context).cardColor,
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 4,
                    ),
                    child: Text(
                        state.customerActivity?.buy != null
                            ? '${state.customerActivity?.buy.toString()}'
                            : '',
                        style: Theme.of(context).textTheme.labelMedium),
                  )
                ],
              ),
              const SizedBox(
                height: 12,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Total CR',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Theme.of(context).cardColor,
                    ),
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 4,
                    ),
                    child: Text(
                        state.customerActivity?.cr != null
                            ? '${state.customerActivity?.cr.toString()}'
                            : 'n/a',
                        style: Theme.of(context).textTheme.labelMedium),
                  )
                ],
              ),
            ],
          );
        } else
          return Center(child: CircularProgressIndicator());
      }),
    );
  }
}
