import 'package:flutter/material.dart';

import '../../blocs/bloc/added_products_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/enums/enums.dart';
import '../../models/product/product.dart';
import '../../util/text_util.dart';
import '../../view_models/product_view_model.dart';
import '../../widgets/dialogs/custom_alert_dialog.dart';
import '../../widgets/marketplace_widgets/star_rating.dart';

class AddedLoreCard extends StatelessWidget {
  final Product product;
  AddedLoreCard({
    Key? key,
    required this.product,
  }) : super(key: key);

  final List<String> value = [
    'Details',
    'Edit',
    'Delete',
  ];

  String get getProfit => formatCurrency.format(
      (product.pricingAndBusiness.price! * product.stats!.sales) *
          (1 - (product.pricingAndBusiness.marketplaceReward! / 100)));
  String get getNetProfit => formatCurrency
      .format(product.pricingAndBusiness.price! * product.stats!.sales);

  String get getConversion => product.stats!.users != 0
      ? ((product.stats!.sales / product.stats!.users) * 100).toStringAsFixed(1)
      : '100';

  @override
  Widget build(BuildContext context) {
    return Container(
      width: StatisticsConstants.rowWidth,
      height: StatisticsConstants.secondRowHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).backgroundColor,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        child: Column(
          children: [
            cardHeader(context, value),
            const Divider(thickness: 0.5),
            const SizedBox(
              height: 16,
            ),
            buildCardItem(
              context: context,
              title: 'Status',
              child: Text(
                product.status.title,
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Type',
              child: Text(
                getTypeTitle(product.productType),
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Category',
              child: Text(
                product.productCategory.title,
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Rating',
              child: StarRating(
                rating: ProductViewModel.rankAsDouble(product.rank),
                iconSize: 12,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Net Profit',
              child: Text(
                getNetProfit,
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Profit',
              child: Text(
                getProfit,
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Price',
              child: Text(
                formatCurrency.format(product.pricingAndBusiness.price),
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Marketplace reward',
              child: Text(
                '${product.pricingAndBusiness.marketplaceReward.toString()}%',
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Users',
              child: Text(
                product.stats!.users.toString(),
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Sales',
              child: Text(
                product.stats != null ? product.stats!.sales.toString() : '0',
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Conversion',
              child: Text(
                '$getConversion%',
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            buildCardItem(
              context: context,
              title: 'Launches',
              child: Text(
                product.stats!.launches.toString(),
                style: Theme.of(context).textTheme.labelMedium,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget cardHeader(BuildContext context, List<String> value) {
    var isFailure = product.failure ?? false;

    final textWidget = isFailure
        ? Row(
            children: [
              Text(
                product.productName,
                style: Theme.of(context).textTheme.titleMedium,
              ),
              IconButton(
                onPressed: () {
                  //_showDialog(context);
                },
                tooltip: failureWidgetMessage,
                icon: const Icon(
                  Icons.circle,
                  color: Colors.red,
                  size: 15,
                ),
              )
            ],
          )
        : Text(
            product.productName,
            style: Theme.of(context).textTheme.titleMedium,
          );

    return SizedBox(
      height: 48,
      child: Row(
        children: [
          Expanded(
            child: textWidget,
          ),
          DropdownButtonHideUnderline(
            child: DropdownButton(
              dropdownColor: Theme.of(context).backgroundColor,
              icon: Icon(Icons.more_horiz_rounded,
                  color: Theme.of(context).iconTheme.color),
              items: value.map<DropdownMenuItem<String>>((value) {
                return DropdownMenuItem(
                    value: value,
                    child: Text(
                      value,
                      style: Theme.of(context).textTheme.labelMedium,
                    ));
              }).toList(),
              onChanged: (valueNew) async {
                if (valueNew == value[2]) {
                  var confirmAction = await _alertDialog(context);
                  if (confirmAction == ConfirmAction.accept) {
                    sl<AddedProductsBloc>().add(
                        ChangeProductStatusToDeletedEvent(
                            productId: product.id));
                  }
                } else if (valueNew == value[1]) {
                  //  if (kIsWeb) {
                  sl<NavigationBLoC>()
                      .add(ToEditProductNavigationEvent(product));
                  // } else {
                  //   await sl<DialogsService>().showDialog(
                  //     dialogType: DialogType.editWidgetDialog,
                  //     title: 'Warning',
                  //     description:
                  //         'Editing the widget is only possible on the web',
                  //   );
                  // }
                } else if (valueNew == value[0]) {
                  sl<NavigationBLoC>()
                      .add(ToProductDetailsNavigationEvent(product.id));
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildCardItem({
    required BuildContext context,
    required String title,
    required Widget child,
  }) {
    return Row(
      children: [
        Expanded(
          child: Text(title,
              style: Theme.of(context)
                  .textTheme
                  .labelMedium
                  ?.copyWith(color: Theme.of(context).iconTheme.color)),
        ),
        child,
      ],
    );
  }

//! TODO переписать все диалоги в менеджера
  Future<dynamic> _alertDialog(BuildContext context) async => showDialog(
        context: context,
        builder: (ctx) =>
            const CustomAlertDialog(message: 'The product will be removed!'),
      );

  String getTypeTitle(String name) => ProductTypeAliases.kProductType[name]!;
}
