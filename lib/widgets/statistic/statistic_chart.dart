import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/models/product/product.dart';

import '../../core/const.dart';

class StatisticChart extends StatefulWidget {
  final List<Product?> products;

  const StatisticChart({Key? key, required this.products}) : super(key: key);

  @override
  StatisticChartState createState() => StatisticChartState();
}

class StatisticChartState extends State<StatisticChart> {
  List<StatisticsData> listData = [];
  ChartDataType currentType = ChartDataType.profit;

  List<DropdownMenuItem<ChartDataType>> get dropdownItems =>
      ChartDataType.values
          .map<DropdownMenuItem<ChartDataType>>(
              (e) => DropdownMenuItem<ChartDataType>(
                    value: e,
                    child: Text(e.name),
                  ))
          .toList();

  @override
  void initState() {
    listData = seriesListProfit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: StatisticsConstants.rowWidth,
      height: StatisticsConstants.secondRowHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).backgroundColor,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Statistic chart',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              height: 20,
            ),
            statisticDropdown(),
            const SizedBox(
              height: 12,
            ),
            chartView(),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget chartView() {
    return listData.isNotEmpty
        ? SingleChildScrollView(
            child: SizedBox(
              height: listData.length * 30,
              width: 300,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(
                          listData.length,
                          (index) => SizedBox(
                                height: 30,
                                child: Center(
                                  child: Text(
                                    listData[index].name,
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelMedium
                                        ?.copyWith(
                                            color: Theme.of(context)
                                                .iconTheme
                                                .color),
                                  ),
                                ),
                              ))),
                  Expanded(
                    child: charts.BarChart(
                      _createData(listData),
                      vertical: false,
                      rtlSpec: const charts.RTLSpec(
                        axisDirection: charts.AxisDirection.reversed,
                      ),
                      layoutConfig: charts.LayoutConfig(
                          leftMarginSpec: charts.MarginSpec.fixedPixel(10),
                          topMarginSpec: charts.MarginSpec.fixedPixel(0),
                          rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                          bottomMarginSpec: charts.MarginSpec.fixedPixel(0)),

                      //barRendererDecorator: BarLabelDecorator<String>(),
                      //BarLabelDecorator(
                      //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
                      //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),

                      defaultRenderer: charts.BarRendererConfig(
                        // cornerStrategy: const charts.ConstCornerStrategy(90),
                        barRendererDecorator: charts.BarLabelDecorator(
                            // labelVerticalPosition: charts.BarLabelVerticalPosition,
                            labelPosition: charts.BarLabelPosition.auto,
                            labelAnchor: charts.BarLabelAnchor.start,
                            outsideLabelStyleSpec: const charts.TextStyleSpec(
                                fontSize: 16,
                                color: charts.MaterialPalette.white),
                            insideLabelStyleSpec: const charts.TextStyleSpec(
                                fontSize: 16,
                                color: charts.MaterialPalette.white)),
                      ),

                      /// Assign a custom style for the measure axis.
                      ///
                      /// The NoneRenderSpec can still draw an axis line with
                      /// showAxisLine=true.
                      primaryMeasureAxis: const charts.NumericAxisSpec(
                          renderSpec: charts.NoneRenderSpec()),
                      domainAxis: const charts.OrdinalAxisSpec(
                          // Make sure that we draw the domain axis line.
                          showAxisLine: false,
                          // But don't draw anything else.
                          renderSpec: charts.NoneRenderSpec()),
                    ),
                  ),
                ],
              ),
            ),
          )
        : const SizedBox.shrink();
  }

  Widget cardHeader(BuildContext context, String chartTitle) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
        left: 10,
        right: 10,
      ),
      child: Row(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Icon(
              MdiIcons.finance,
              color: Colors.purple,
            ),
          ),
          Expanded(
            child: Text(
              chartTitle,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
        ],
      ),
    );
  }

  List<StatisticsData> seriesListProfit() {
    var dataSource = <StatisticsData>[];
    for (var product in widget.products) {
      double profit;
      profit = 0;
      if (product!.stats != null) {
        profit = (product.pricingAndBusiness.price! * product.stats!.sales) *
            (1 - (product.pricingAndBusiness.marketplaceReward! / 100));
      }
      dataSource.add(StatisticsData(product.productName, profit));
    }
    return dataSource;
  }

  List<StatisticsData> seriesListUsers() {
    var dataSource = <StatisticsData>[];
    for (var product in widget.products) {
      double statUsers;
      statUsers = 0;
      if (product!.stats != null) {
        statUsers = product.stats!.users.toDouble();
      }
      dataSource.add(StatisticsData(product.productName, statUsers));
    }

    return dataSource;
  }

  List<StatisticsData> seriesListSales() {
    var dataSource = <StatisticsData>[];
    for (var product in widget.products) {
      double sales;
      sales = 0;
      if (product!.stats != null) {
        sales = product.stats!.sales.toDouble();
      }
      dataSource.add(StatisticsData(product.productName, sales));
    }

    return dataSource;
  }

  List<StatisticsData> seriesListConvers() {
    var dataSource = <StatisticsData>[];
    for (var product in widget.products) {
      num convers = 100;
      if (product!.stats != null) {
        convers = product.stats!.users != 0
            ? num.parse(((product.stats!.sales / product.stats!.users) * 100)
                .toStringAsFixed(1))
            : 100;
      }
      dataSource.add(StatisticsData(product.productName, convers.toDouble()));
    }
    return dataSource;
  }

  List<charts.Series<StatisticsData, String>> _createData(
      List<StatisticsData> data) {
    return [
      charts.Series<StatisticsData, String>(
        id: 'data',
        domainFn: (StatisticsData stat, _) => stat.name,
        measureFn: (StatisticsData stat, _) => stat.value,
        data: data,
        colorFn: (StatisticsData _, i) => barColors[i! % barColors.length],
        labelAccessorFn: (p, _) => mapTypeToLabel(p.name, p.value, currentType),
        // insideLabelStyleAccessorFn: (StatisticsData stat, _) {
        //   return charts.TextStyleSpec(color: charts.MaterialPalette.white);
        // }
      )
    ];
  }

  Widget statisticDropdown() {
    return DropdownButtonHideUnderline(
      child: ButtonTheme(
        alignedDropdown: true,
        child: DropdownButtonFormField(
          hint: Text(ChartDataType.profit.name),
          value: ChartDataType.profit,
          borderRadius: BorderRadius.circular(12.0),
          style: Theme.of(context).textTheme.headlineMedium,
          dropdownColor: Theme.of(context).dividerColor,
          icon: const Icon(
            Icons.expand_more,
          ),
          items: dropdownItems,
          onChanged: (valueNew) {
            if (valueNew == ChartDataType.profit) {
              setState(() {
                listData = seriesListProfit();
                currentType = valueNew as ChartDataType;
              });
            }
            if (valueNew == ChartDataType.users) {
              setState(() {
                listData = seriesListUsers();
                currentType = valueNew as ChartDataType;
              });
            }
            if (valueNew == ChartDataType.sales) {
              setState(() {
                listData = seriesListSales();
                currentType = valueNew as ChartDataType;
              });
            }
            if (valueNew == ChartDataType.conversion) {
              setState(() {
                listData = seriesListConvers();
                currentType = valueNew as ChartDataType;
              });
            }
          },
        ),
      ),
    );
  }
}

final barColors = [
  charts.MaterialPalette.blue.shadeDefault,
  charts.MaterialPalette.cyan.shadeDefault,
  charts.MaterialPalette.deepOrange.shadeDefault,
  charts.MaterialPalette.green.shadeDefault,
  charts.MaterialPalette.indigo.shadeDefault,
  charts.MaterialPalette.lime.shadeDefault,
  charts.MaterialPalette.pink.shadeDefault,
  charts.MaterialPalette.purple.shadeDefault,
  charts.MaterialPalette.red.shadeDefault,
  charts.MaterialPalette.teal.shadeDefault,
];

String mapTypeToLabel(String name, double value, ChartDataType type) {
  switch (type) {
    case ChartDataType.profit:
      return '\$${value.toStringAsFixed(2)}';
    case ChartDataType.users:
      return value.toStringAsFixed(0);
    case ChartDataType.sales:
      return value.toStringAsFixed(2);
    case ChartDataType.conversion:
      return '${value.toStringAsFixed(2)}%';
  }
}

class StatisticsData {
  final String name;
  final double value;

  StatisticsData(this.name, this.value);
}
