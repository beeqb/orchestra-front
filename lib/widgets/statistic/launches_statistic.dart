import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/widgets/statistic/statistic_chart.dart';

import '../../blocs/cubits/stats_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/launch_stats.dart';

class LaunchesStatistic extends StatefulWidget {
  const LaunchesStatistic({Key? key}) : super(key: key);

  @override
  State<LaunchesStatistic> createState() => _LaunchesStatisticState();
}

class _LaunchesStatisticState extends State<LaunchesStatistic> {
  @override
  void initState() {
    sl<StatsCubit>().getLaunchStats();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: StatisticsConstants.rowWidth,
      height: StatisticsConstants.firstRowHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: Theme.of(context).backgroundColor,
      ),
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Launches  Statistic',
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'TestFly vs Buy and return sales',
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
          const SizedBox(
            height: 18,
          ),
          BlocBuilder<StatsCubit, StatsState>(
            builder: (context, state) {
              if (state.launchesStatisticsLoadingState.isCompleted) {
                return LaunchStatsWidget(launchStats: state.launchStats);
              } else {
                return const Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
    );
  }
}

class LaunchStatsWidget extends StatelessWidget {
  final LaunchStats? launchStats;
  LaunchStatsWidget({Key? key, required this.launchStats}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 60,
              width: 220,
              child: Stack(
                children: [
                  Positioned(
                    top: 5,
                    left: 0,
                    width: 220,
                    height: 10,
                    child: Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: const Color(0xFF33333F),
                          borderRadius: BorderRadius.circular(4)),
                    ),
                  ),
                  Positioned(
                    top: 25,
                    left: 0,
                    width: 220,
                    height: 10,
                    child: Container(
                      decoration: BoxDecoration(
                          color: const Color(0xFF33333F),
                          borderRadius: BorderRadius.circular(4)),
                    ),
                  ),
                  Positioned(
                    top: 45,
                    left: 0,
                    width: 220,
                    height: 10,
                    child: Container(
                      // width: double.infinity,
                      // height: 10,
                      decoration: BoxDecoration(
                          color: const Color(0xFF33333F),
                          borderRadius: BorderRadius.circular(4)),
                    ),
                  ),
                  charts.BarChart(
                    // barGroupingType: charts.BarGroupingType.stacked,
                    // // Configures a [PercentInjector] behavior that will calculate measure
                    // // values as the percentage of the total of all data that shares a
                    // // domain value.
                    // behaviors: [
                    //   new charts.PercentInjector<String>(
                    //       totalType: charts.PercentInjectorTotalType.domain)
                    // ],

                    _createData(seriesListLaunchStats(launchStats)),
                    vertical: false,
                    rtlSpec: const charts.RTLSpec(
                      axisDirection: charts.AxisDirection.reversed,
                    ),
                    layoutConfig: charts.LayoutConfig(
                        leftMarginSpec: charts.MarginSpec.fixedPixel(0),
                        topMarginSpec: charts.MarginSpec.fixedPixel(0),
                        rightMarginSpec: charts.MarginSpec.fixedPixel(0),
                        bottomMarginSpec: charts.MarginSpec.fixedPixel(0)),

                    //barRendererDecorator: BarLabelDecorator<String>(),
                    //BarLabelDecorator(
                    //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
                    //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
                    defaultRenderer: charts.BarRendererConfig(
                      maxBarWidthPx: 10,
                      fillPattern: charts.FillPatternType.solid,
                      cornerStrategy: const charts.ConstCornerStrategy(90),
                    ),

                    /// Assign a custom style for the measure axis.
                    ///
                    /// The NoneRenderSpec can still draw an axis line with
                    /// showAxisLine=true.
                    primaryMeasureAxis: const charts.NumericAxisSpec(
                        renderSpec: charts.NoneRenderSpec()),
                    domainAxis: const charts.OrdinalAxisSpec(
                        // Make sure that we draw the domain axis line.
                        showAxisLine: false,
                        // But don't draw anything else.
                        renderSpec: charts.NoneRenderSpec()),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                    launchStats?.testfly != null
                        ? '${launchStats?.testfly.toString()}%'
                        : '',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall
                        ?.copyWith(color: colors[0])),
                Text(
                    launchStats?.buy != null
                        ? '${launchStats?.buy.toString()}%'
                        : '',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall
                        ?.copyWith(color: colors[1])),
                Text(
                    launchStats?.returnSales != null
                        ? '${launchStats?.returnSales.toString()}%'
                        : '',
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall
                        ?.copyWith(color: colors[2])),
              ],
            )
          ],
        ),
        const SizedBox(
          height: 25,
        ),
        Row(
          children: [
            Container(
              height: 14,
              width: 14,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: colors[0],
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              'TestFly',
              style: Theme.of(context)
                  .textTheme
                  .headlineSmall
                  ?.copyWith(color: colors[0]),
            ),
            const SizedBox(
              width: 20,
            ),
            Container(
              height: 14,
              width: 14,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: colors[1],
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Text('Buy',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(color: colors[1])),
            const SizedBox(
              width: 20,
            ),
            Container(
              height: 14,
              width: 14,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: colors[2],
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Text('Return Sales',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall
                    ?.copyWith(color: colors[2]))
          ],
        )
      ],
    );
  }

  List<charts.Series<StatisticsData, String>> _createData(
      List<StatisticsData> data) {
    return [
      charts.Series<StatisticsData, String>(
        id: 'data',
        domainFn: (StatisticsData stat, _) => stat.name,
        measureFn: (StatisticsData stat, _) => stat.value,
        data: data,
        colorFn: (StatisticsData _, i) => barColors[i! % barColors.length],
        labelAccessorFn: (p, _) => p.value.toString(),
        // insideLabelStyleAccessorFn: (StatisticsData stat, _) {
        //   return charts.TextStyleSpec(color: charts.MaterialPalette.white);
        // }
      )
    ];
  }

  List<StatisticsData> seriesListLaunchStats(LaunchStats? launchStats) {
    var dataSource = <StatisticsData>[];

    dataSource
      ..add(StatisticsData('TestFly', launchStats?.testfly ?? 0))
      ..add(StatisticsData('Buy', launchStats?.buy ?? 0))
      ..add(StatisticsData('Return Sales', launchStats?.returnSales ?? 0));
    return dataSource;
  }

  final colors = [
    const Color(0xFFFFAF02),
    const Color(0xFF24DF9B),
    const Color(0xFFFA4F59),
  ];

  final barColors = [
    charts.Color.fromHex(code: '#FFAF02'),
    charts.Color.fromHex(code: '#24DF9B'),
    charts.Color.fromHex(code: '#FA4F59'),
    // charts.MaterialPalette.yellow.shadeDefault,
    // charts.MaterialPalette.green.shadeDefault,
    // charts.MaterialPalette.red.shadeDefault,
  ];
}
