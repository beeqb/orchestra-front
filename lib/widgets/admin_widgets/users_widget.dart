import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/all_users_bloc.dart';
import '../../injection_container.dart';
import '../../models/enums/enums.dart';
import '../../models/user.dart';
import '../../services/user_service.dart';
import '../../util/text_util.dart';

class AdminUsersWidget extends StatefulWidget {
  const AdminUsersWidget({
    Key? key,
    required List<String> dataForColumns,
    required List<UserModel> dataForTableSource,
    required int? sortColumnIndex,
    required bool sortAscending,
  })  : _dataForColumns = dataForColumns,
        _dataForTableSource = dataForTableSource,
        _sortColumnIndex = sortColumnIndex,
        _sortAscending = sortAscending,
        super(key: key);

  final List<String> _dataForColumns;
  final List<UserModel> _dataForTableSource;
  final int? _sortColumnIndex;
  final bool _sortAscending;

  @override
  State<AdminUsersWidget> createState() => _AdminUsersWidgetState();
}

class _AdminUsersWidgetState extends State<AdminUsersWidget> {
  void _sort<T>(
    Comparable<T> Function(UserModel product) getField,
    int colIndex,
    bool asc,
    UserDataTableSource _src,
    List<UserModel> _provider,
    BuildContext context,
  ) {
    debugPrint('user sort $asc');
    debugPrint('user sort ${BlocProvider.of<AllUsersBloc>(context)}');
    if (BlocProvider.of<AllUsersBloc>(context).state is AllUsersLoaded) {
      final state =
          BlocProvider.of<AllUsersBloc>(context).state as AllUsersLoaded;
      print((BlocProvider.of<AllUsersBloc>(context).state as AllUsersLoaded)
          .sortAscending);
      bool nasc =
          !(BlocProvider.of<AllUsersBloc>(context).state as AllUsersLoaded)
              .sortAscending;
      debugPrint('new user sort $nasc');
      BlocProvider.of<AllUsersBloc>(context).add(SortAllUsersEvent(
        getField: getField,
        ascending: nasc,
        sortColumnIndex: colIndex,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
/*     final _dataColumns = _dataForColumns
        .map<DataColumn>((e) => DataColumn(label: Text(e)))
        .toList(); */
    final _dataTableSource =
        UserDataTableSource(userData: widget._dataForTableSource);

    final _dataColumns = [
      DataColumn(
          label: Text(widget._dataForColumns[0]),
          onSort: (collIndex, asc) {
            debugPrint('onSort: (collIndex $collIndex, asc $asc)');
            _sort<String>((user) => user.name!, collIndex, asc,
                _dataTableSource, widget._dataForTableSource, context);
            setState(() {});
          }),
      DataColumn(
          label: Text(widget._dataForColumns[1]),
          onSort: (collIndex, asc) {
            _sort<String>((user) => user.type!.title, collIndex, asc,
                _dataTableSource, widget._dataForTableSource, context);
          }),
      DataColumn(
        label: Text(widget._dataForColumns[2]),
        /* onSort: (collIndex, asc) {
      _sort<bool>((user) => user.contributor, collIndex, asc, _dataTableSource,
                _dataForTableSource, context);
          } */
      ),
      DataColumn(
          label: Text(widget._dataForColumns[3]),
          onSort: (collIndex, asc) {
            _sort<DateTime>(
                (user) => DateTime.parse(user.createdAt!),
                collIndex,
                asc,
                _dataTableSource,
                widget._dataForTableSource,
                context);
          }),
      DataColumn(
          label: Text(widget._dataForColumns[4]),
          onSort: (collIndex, asc) {
            _sort<num>((user) {
              if (user.stats != null) {
                return user.stats!.costs;
              } else {
                return 0;
              }
            }, collIndex, asc, _dataTableSource, widget._dataForTableSource,
                context);
          }),
      DataColumn(
          label: Text(widget._dataForColumns[5]),
          onSort: (collIndex, asc) {
            _sort<num>((user) => user.balance!, collIndex, asc,
                _dataTableSource, widget._dataForTableSource, context);
          }),
      DataColumn(
          label: Text(widget._dataForColumns[6]),
          onSort: (collIndex, asc) {
            _sort<num>((user) {
              if (user.stats != null) {
                return user.stats!.profit;
              } else {
                return 0;
              }
            }, collIndex, asc, _dataTableSource, widget._dataForTableSource,
                context);
          }),
    ];

    return SingleChildScrollView(
      child: PaginatedDataTable(
          columns: _dataColumns,
          source: _dataTableSource,
          rowsPerPage: 15,
          sortAscending: widget._sortAscending,
          sortColumnIndex: widget._sortColumnIndex),
    );
  }
}

class UserDataTableSource extends DataTableSource {
  UserDataTableSource({
    required List<UserModel> userData,
  }) : _userData = userData;

  final List<UserModel> _userData;

  @override
  DataRow getRow(int index) {
    final _user = _userData[index];
    num purchases = 0;
    num costs = 0;
    num profit = 0;
    if (_user.stats != null) {
      purchases = _user.stats!.purchases;
      costs = _user.stats!.costs;
      profit = _user.stats!.profit;
    }
    final _spends = (purchases + costs + _user.sended!);
    Widget userTypeWidget = TextButton.icon(
      onPressed: () {},
      icon: _user.type!.icon,
      label: Text(_user.type!.title),
    );
    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(Text('${_user.name}')),
        DataCell(
          Row(children: [
            Expanded(child: userTypeWidget),
            _popupMenuButton(_user),
          ]),
        ),
        DataCell(
          Text(_user.contributor! ? 'Contributor' : ''),
        ),
        DataCell(
            Text(dateFormatWithTime.format(DateTime.parse(_user.createdAt!)))),
        DataCell(
            Text(formatCurrency.format(_spends))), //! TODO рассчитать расходы
        DataCell(Text(formatCurrency.format(_user.balance))),
        DataCell(Text(formatCurrency.format(profit))),
      ],
    );
  }

  Widget _popupMenuButton(UserModel user) => PopupMenuButton(
        onSelected: (UserType userType) async {
          await sl<UserService>()
              .adminUserUpdate(user.copyWith(type: userType));
          sl<AllUsersBloc>().add(FetchAllUsersEvent());
        },
        itemBuilder: (context) => <PopupMenuEntry<UserType>>[
          const PopupMenuItem<UserType>(
            value: UserType.admin,
            child: Text('Set role Admin'),
          ),
          const PopupMenuItem<UserType>(
            value: UserType.user,
            child: Text('Set role User'),
          ),
        ],
      );

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _userData.length;

  @override
  int get selectedRowCount => 0;
}
