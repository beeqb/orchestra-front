import 'package:flutter/material.dart';
import 'package:orchestra/models/running_app.dart';
import 'package:orchestra/util/text_util.dart';
import 'package:orchestra/models/enums/enums.dart';

class RunningAppsWidget extends StatelessWidget {
  const RunningAppsWidget({
    Key? key,
    required List<String> dataForColumns,
    required List<RunningAppModel> dataForTableSource,
  })  : _dataForColumns = dataForColumns,
        _dataForTableSource = dataForTableSource,
        super(key: key);
  final List<String> _dataForColumns;
  final List<RunningAppModel> _dataForTableSource;

  @override
  Widget build(BuildContext context) {
    final _dataColumns = _dataForColumns
        .map<DataColumn>((e) => DataColumn(label: Text(e)))
        .toList();
    final _dataTableSource = RunningAppsDataTableSource(
      context: context,
      apps: _dataForTableSource,
    );

    return SingleChildScrollView(
      child: PaginatedDataTable(
        columns: _dataColumns,
        source: _dataTableSource,
        rowsPerPage: 15,
      ),
    );
  }
}

class RunningAppsDataTableSource extends DataTableSource {
  RunningAppsDataTableSource({
    required BuildContext context,
    required List<RunningAppModel> apps,
  }) : _apps = apps;
  // _context = context;
  final List<RunningAppModel> _apps;
  // final BuildContext _context;
/* 
  get color => MaterialStateColor.resolveWith(
      (states) => Theme.of(_context).dialogBackgroundColor); */

  @override
  DataRow getRow(int index) {
    final _item = _apps[index];
    return DataRow.byIndex(
//      color: color,
      index: index,
      cells: <DataCell>[
        DataCell(Text(_item.name)),
        DataCell(Text('${_item.userId.name}')),
        DataCell(Text(
            dateFormatWithTimeSec.format(DateTime.fromMillisecondsSinceEpoch(_item.startTime!)))),
        DataCell(Text(_item.loop!.title)),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _apps.length;

  @override
  int get selectedRowCount => 0;
}
