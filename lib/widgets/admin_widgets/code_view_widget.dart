import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_highlight/flutter_highlight.dart';
import 'package:flutter_highlight/themes/github.dart';
import 'package:orchestra/blocs/bloc/product_code_bloc.dart';
import 'package:orchestra/core/const.dart';

import '../loading.dart';

class CodeViewWidget extends StatelessWidget {
  const CodeViewWidget({Key? key}) : super(key: key);

  final int tabLength = 2;

  @override
  Widget build(BuildContext context) {
    const loader = Loading();
    final _height = MediaQuery.of(context).size.height - 150;

    final tabs = [
      Tab(
        child: Text(
          'Manifest',
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ),
      Tab(
        child: Text(
          'Source',
          style: Theme.of(context).textTheme.bodyText2,
        ),
      ),
    ];

    Widget tabBarHeader = TabBar(tabs: tabs);

    return Container(
      height: _height,
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width <
                  ApplicationsPageConstants.maxWidth
              ? 0
              : (MediaQuery.of(context).size.width -
                      ApplicationsPageConstants.maxWidth) /
                  2),
      decoration: BoxDecoration(
        color: Theme.of(context).bottomAppBarColor,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
          topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
        ),
      ),
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: BlocBuilder<ProductCodeBLoC, ProductCodeState>(
        builder: (context, state) {
          Widget? view;
          state.when(
            initial: () => view = loader,
            loading: () => view = loader,
            loaded: (source, manifest) => view = DefaultTabController(
              length: tabLength,
              child: Column(
                children: [
                  tabBarHeader,
                  Expanded(
                    child: TabBarView(
                      children: [
                        SingleChildScrollView(
                          child: HighlightView(
                            manifest,
                            language: 'json',
                            theme: githubTheme,
                            padding: const EdgeInsets.all(12),
                          ),
                        ),
                        SingleChildScrollView(
                          child: HighlightView(
                            source,
                            language: 'javascript',
                            theme: githubTheme,
                            padding: const EdgeInsets.all(12),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
          return view!;
        },
      ),
    );
  }
}
