import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/product_code_bloc.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/services/admin_service.dart';
import 'package:orchestra/widgets/admin_widgets/code_view_widget.dart';

import '../../blocs/bloc/admin_products_bloc.dart';
import '../../blocs/cubits/products_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/enums/enums.dart';
import '../../util/text_util.dart';

class ProductsWidget extends StatelessWidget {
  const ProductsWidget({
    Key? key,
    required List<String> dataForColumns,
    required List<Product> dataForTableSource,
    required int? sortColumnIndex,
    required bool sortAscending,
  })  : _dataForColumns = dataForColumns,
        _dataForTableSource = dataForTableSource,
        _sortColumnIndex = sortColumnIndex,
        _sortAscending = sortAscending,
        super(key: key);

  final List<String> _dataForColumns;
  final List<Product> _dataForTableSource;
  final int? _sortColumnIndex;
  final bool _sortAscending;

  void _sort<T>(
    Comparable<T> Function(Product product) getField,
    int colIndex,
    bool asc,
    ProductDataTableSource _src,
    List<Product> _provider,
    BuildContext context,
  ) {
    BlocProvider.of<AdminProductsBloc>(context).add(SortAllProductsEvent(
      getField: getField,
      ascending: asc,
      sortColumnIndex: colIndex,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final _dataTableSource = ProductDataTableSource(
      prodData: _dataForTableSource,
      context: context,
    );

    final _dataColumns = [
      DataColumn(
          label: Text(_dataForColumns[0]),
          onSort: (collIndex, asc) {
            _sort<String>((product) => product.status.title, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[1]),
          onSort: (collIndex, asc) {
            _sort<String>((product) => product.productName, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[2]),
          onSort: (collIndex, asc) {
            _sort<String>((product) => product.productContributor, collIndex,
                asc, _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[3]),
          onSort: (collIndex, asc) {
            _sort<String>((product) => product.productType, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[4]),
          onSort: (collIndex, asc) {
            _sort<String>((product) => product.productCategory.title, collIndex,
                asc, _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[5]),
          onSort: (collIndex, asc) {
            _sort<num>((product) => product.pricingAndBusiness.price!,
                collIndex, asc, _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[6]),
          onSort: (collIndex, asc) {
            _sort<num>((product) => product.stats!.users, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[7]),
          onSort: (collIndex, asc) {
            _sort<num>((product) => product.stats!.sales, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[8]),
          onSort: (collIndex, asc) {
            _sort<num>((product) => product.stats!.launches, collIndex, asc,
                _dataTableSource, _dataForTableSource, context);
          }),
      DataColumn(
          label: Text(_dataForColumns[9]),
          onSort: (collIndex, asc) {
            _sort<DateTime>((product) => DateTime.parse(product.createdAt!),
                collIndex, asc, _dataTableSource, _dataForTableSource, context);
          }),
    ];

    return SingleChildScrollView(
      child: PaginatedDataTable(
        columns: _dataColumns,
        source: _dataTableSource,
        rowsPerPage: 15,
        sortAscending: _sortAscending,
        sortColumnIndex: _sortColumnIndex,
      ),
    );
  }
}

class ProductDataTableSource extends DataTableSource {
  ProductDataTableSource({
    required BuildContext context,
    required List<Product> prodData,
  })  : _prodData = prodData,
        _context = context;
  final List<Product> _prodData;
  final BuildContext _context;

  @override
  DataRow getRow(int index) {
    final _prod = _prodData[index];
    Widget statusButton = TextButton.icon(
        icon: _prod.status.icon,
        style: TextButton.styleFrom(
            textStyle: Theme.of(_context).textTheme.bodyText2),
        label: Text(_prod.status.title),
        onPressed: () => _handleOnSelected(_prod, 0));

    Widget detailsButton = Expanded(
        child: TextButton(
      style: TextButton.styleFrom(
          textStyle: Theme.of(_context).textTheme.bodyText2),
      onPressed: () => _handleOnSelected(_prod, 1),
      child: Text(_prod.productName),
    ));

    Widget codeButton = IconButton(
      icon: const Icon(MdiIcons.codeJson),
      onPressed: () => _showCodeViewBottom(_prod),
    );

    Widget debugLaunch = IconButton(
      icon: const Icon(MdiIcons.clipboardPlayOutline),
      onPressed: () => sl<NavigationBLoC>().add(ToTestLauncherEvent(_prod)),
    );

    Widget detailsWidget = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        detailsButton,
        codeButton,
        debugLaunch,
      ],
    );

    return DataRow.byIndex(
      index: index,
      cells: <DataCell>[
        DataCell(statusButton),
        DataCell(detailsWidget),
        DataCell(Text(_prod.productContributor)),
        DataCell(Text(_prod.productType)),
        DataCell(Text(_prod.productCategory.title)),
        DataCell(Text(formatCurrency.format(_prod.pricingAndBusiness.price))),
        DataCell(Text('${_prod.stats!.users}')),
        DataCell(Text('${_prod.stats!.sales}')),
        DataCell(Text('${_prod.stats!.launches}')),
        DataCell(
            Text(dateFormatWithTime.format(DateTime.parse(_prod.createdAt!)))),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _prodData.length;

  @override
  int get selectedRowCount => 0;

  void _handleOnSelected(Product product, int value) {
    debugPrint('${product.productName} on Selected $value');
    switch (value) {
      case 0:
        _showChangeStatusBottom(product);
        break;
      case 1:
        sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(product.id));
        break;
      case 2:
        sl<NavigationBLoC>().add(ToEditProductNavigationEvent(product));
        break;
      default:
    }
  }

  void _showChangeStatusBottom(Product product) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        //       isScrollControlled: true,
        context: _context,
        builder: (context) {
          return BottomChangeStatusSheet(
            product: product,
          );
        });
  }

  void _showCodeViewBottom(Product product) {
    sl<ProductCodeBLoC>().add(FetchProductCodeEvent(productId: product.id));

    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        //       isScrollControlled: true,
        context: _context,
        builder: (context) {
          return BlocProvider.value(
            value: sl<ProductCodeBLoC>(),
            child: const CodeViewWidget(),
          );
        });
  }
}

class BottomChangeStatusSheet extends StatefulWidget {
  final Product product;

  const BottomChangeStatusSheet({Key? key, required this.product})
      : super(key: key);

  @override
  _BottomChangeStatusSheetState createState() =>
      _BottomChangeStatusSheetState();
}

class _BottomChangeStatusSheetState extends State<BottomChangeStatusSheet> {
  ModerateType? _moderateType;

  @override
  void initState() {
    super.initState();
    _moderateType = ModerateType.deny;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width <
                  ApplicationsPageConstants.maxWidth
              ? 0
              : (MediaQuery.of(context).size.width -
                      ApplicationsPageConstants.maxWidth) /
                  2),
      decoration: BoxDecoration(
        color: Theme.of(context).bottomAppBarColor,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
          topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
        ),
      ),
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: _buildRadioForm(),
    );
  }

  Widget _buildRadioForm() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 20),
        Wrap(
          children: List.generate(
              ModerateType.values.length,
              (index) => RadioListTile<ModerateType>(
                    title: Text(ModerateType.values[index].decision),
                    value: ModerateType.values[index],
                    groupValue: _moderateType,
                    onChanged: (newValue) {
                      debugPrint('выбран статус $newValue');
                      setState(() {
                        _moderateType = newValue;
                      });
                    },
                  )),
        ),
        _buildButtonsPanel(),
      ],
    );
  }

  Widget _buildButtonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CupertinoButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'CANCEL',
            style: TextStyle(color: Colors.red),
          ),
        ),
        CupertinoButton(
          onPressed: () async {
            await sl<AdminService>()
                .moderate(widget.product.id, _moderateType!);
            await sl<ProductsCubit>().getProducts();
            sl<AdminProductsBloc>().add(LoadAllProductsEvent());
            Navigator.pop(context);
          },
          child: const Text('SAVE'),
        ),
      ],
    );
  }
}
