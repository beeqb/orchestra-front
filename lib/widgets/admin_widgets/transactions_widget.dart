import 'package:flutter/material.dart';
import '../../models/billing_models/admin_transact_model.dart';
import '../../util/text_util.dart';

class TransactionsWidget extends StatelessWidget {
  const TransactionsWidget({
    Key? key,
    required List<String> dataForColumns,
    required List<AdminTransactModel> dataForTableSource,
  })  : _dataForColumns = dataForColumns,
        _dataForTableSource = dataForTableSource,
        super(key: key);

  final List<String> _dataForColumns;
  final List<AdminTransactModel> _dataForTableSource;

  @override
  Widget build(BuildContext context) {
    final _dataColumns = _dataForColumns
        .map<DataColumn>((e) => DataColumn(label: Text(e)))
        .toList();
    final _dataTableSource = TransactionsDataTableSource(
      context: context,
      hystory: _dataForTableSource,
    );

    return SingleChildScrollView(
      child: PaginatedDataTable(
        columns: _dataColumns,
        source: _dataTableSource,
        rowsPerPage: 15,
      ),
    );
  }
}

class TransactionsDataTableSource extends DataTableSource {
  TransactionsDataTableSource({
    required BuildContext context,
    required List<AdminTransactModel> hystory,
  })  : _hystory = hystory,
        _context = context;
  final List<AdminTransactModel> _hystory;
  final BuildContext _context;
/* 
  get color => MaterialStateColor.resolveWith(
      (states) => Theme.of(_context).dialogBackgroundColor); */

  @override
  DataRow getRow(int index) {
    final _item = _hystory[index];
    return DataRow.byIndex(
//      color: color,
      index: index,
      cells: <DataCell>[
        DataCell(Text('${_item.userModel.name}')),
        DataCell(Text(formatCurrency.format(_item.userModel.balance))),
        DataCell(!_item.amount.isNegative
            ? Text(
                formatCurrency5Digits.format(_item.amount),
                style: Theme.of(_context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.green),
              )
            : const SizedBox(height: 1)),
        DataCell(_item.amount.isNegative
            ? Text(
                formatCurrency5Digits.format(_item.amount),
                style: Theme.of(_context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.red),
              )
            : const SizedBox(height: 1)), //! TODO рассчитать расходы
        DataCell(Text(_item.meta.type)),
        DataCell(Text(_item.status)),
        DataCell(Text(
            dateFormatWithTime.format(DateTime.parse(_item.createdAt)))),
      ],
    );
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => _hystory.length;

  @override
  int get selectedRowCount => 0;
}
