import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';
import 'package:orchestra/blocs/bloc/test_launcer_bloc.dart';
import 'package:orchestra/blocs/bloc/test_product_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/settings_model.dart';
import 'package:orchestra/services/admin_service.dart';
import 'package:orchestra/settings_app/presentation/widgets/form_settings_widget.dart';
import 'package:orchestra/widgets/admin_widgets/log_screen_widget.dart';
import 'package:orchestra/widgets/custom_app_bar.dart';

import '../loading.dart';

class TestLauncherPage extends StatelessWidget {
  final Product _product;
  const TestLauncherPage({required Product product, Key? key})
      : _product = product,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: BlocProvider<TestLauncherBLoC>(
        create: (context) => TestLauncherBLoC(sl<AdminService>())
          ..add(TestLauncherEvent.create(
            product: _product,
          )),
        child: BlocProvider(
          create: (context) => TestProductBLoC(
            adminService: sl<AdminService>(),
            productId: _product.id,
          ),
          child: BlocProvider(
            /// блок для показа лога на странице запуска
            create: (context) => sl<LogScreenBloc>(),
            child: const TestLauncherWidget(),
          ),
        ),
      )),
    );
  }
}

class TestLauncherWidget extends StatefulWidget {
  const TestLauncherWidget({Key? key}) : super(key: key);

  @override
  _TestLauncherWidgetState createState() => _TestLauncherWidgetState();
}

class _TestLauncherWidgetState extends State<TestLauncherWidget> {
  Map<String, dynamic>? _settings;
  LogScreenBloc? _logScreenBloc;
  @override
  void initState() {
    super.initState();
    _logScreenBloc = sl<LogScreenBloc>();
  }

  @override
  Widget build(BuildContext context) {
    Widget _mainPage(
      Product product,
      List<SettingsModel> manifest,
    ) {
      return ListView(
        children: [
          CustomAppBar(title: 'Test launcher "${product.productName}"'),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FormSettings(
              fromAppView: false,
              product: product,
              settings: manifest,
              oldSettings: '',
              sendSettings: (settings) {
                setState(() {
                  _settings = settings;
                });
                try {
                  _logScreenBloc!.add(AddStringLogScreenEvent(
                      newString: 'settings: $_settings'));
                } on Exception catch (e) {
                  // TODO
                  debugPrint('**** $e');
                }
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 150,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.blueGrey,
                  ),
                  onPressed: () {
                    debugPrint(_settings.toString());
                    if (_settings != null) {
                      BlocProvider.of<TestProductBLoC>(context)
                          .add(StartTestProductEvent(_settings!));
                      _logScreenBloc!.add(AddStringLogScreenEvent(
                          newString: 'Lore ${product.productName} started'));
                    } else {
                      _logScreenBloc!.add(const AddStringLogScreenEvent(
                          newString: 'Add settings before launching Lore'));
                    }
                  },
                  child: const Text('Test launch'),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const LogScreenWidget(),
        ],
      );
    }

    return BlocBuilder<TestLauncherBLoC, TestLauncherState>(
      builder: (context, state) {
        Widget page = const Loading();
        state.when(
          initial: () => page = const Loading(),
          loading: () => page = const Loading(),
          loaded: (prod, manifest) => page = _mainPage(prod, manifest),
        );
        return page;
      },
    );
  }
}
