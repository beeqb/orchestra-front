import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';

class LogScreenWidget extends StatelessWidget {
  const LogScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 10.0, right: 10.0, top: 10.0, bottom: 60.0),
      child: Container(
        color: Colors.grey[300],
        height: 400.0,
        child: BlocBuilder<LogScreenBloc, LogScreenState>(
          builder: (context, state) {
            if (state is LoadedLogScreenState) {
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: LogsListWidget(
                  logs: state.logs,
                ),
              );
            }
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                'waiting for the data...',
                style: GoogleFonts.getFont('Cutive Mono'),
              ),
            );
          },
        ),
      ),
    );
  }
}

class LogsListWidget extends StatefulWidget {
  final List<String> _logs;
  const LogsListWidget({
    required List<String> logs,
    Key? key,
  })  : _logs = logs,
        super(key: key);

  @override
  _LogsListWidgetState createState() => _LogsListWidgetState();
}

class _LogsListWidgetState extends State<LogsListWidget> {
  ScrollController? _scrollController;
  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  void _scrollToBottom() {
    _scrollController!.jumpTo(_scrollController!.position.maxScrollExtent);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => _scrollToBottom());
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Scrollbar(
            child: ListView.separated(
                controller: _scrollController,
                itemCount: widget._logs.length + 1,
                separatorBuilder: (_, __) => const Divider(
                      thickness: 0.5,
                    ),
                itemBuilder: (context, index) {
                  return index < widget._logs.length
                      ? Text(
                          widget._logs[index],
                          style: GoogleFonts.getFont('Cutive Mono'),
                        )
                      : Text(
                          '...',
                          style: GoogleFonts.getFont('Cutive Mono'),
                        );
                }),
          ),
        ),
      ],
    );
  }
}
