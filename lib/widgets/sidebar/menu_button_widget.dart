import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/sliding_page_bloc.dart';
import 'package:orchestra/core/const.dart';

import '../../injection_container.dart';

class MenuButtonWidget extends StatefulWidget {
  const MenuButtonWidget({Key? key}) : super(key: key);

  @override
  _MenuButtonWidgetState createState() => _MenuButtonWidgetState();
}

class _MenuButtonWidgetState extends State<MenuButtonWidget>
    with TickerProviderStateMixin {
  AnimationController? _buttonController;
  bool isPlaying = false;
  bool? showMenu;
  SlidingPageBLoC? _slidingPageBLoC;

  @override
  void initState() {
    super.initState();
    _buttonController = AnimationController(
      vsync: this,
      duration: kSlidingPageAnimDuration,
    );
    _slidingPageBLoC = BlocProvider.of<SlidingPageBLoC>(context);
    showMenu = showMenu ?? false;
    super.initState();
  }

  @override
  void dispose() {
    _buttonController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SlidingPageBLoC, SlidingPageState>(
      listener: (context, state) {
        state.when(initial: () {
          setState(() {
            showMenu = false;
          });
        }, closed: () {
          setState(() {
            showMenu = false;
            _buttonController!.reverse();
          });
        }, opened: () {
          setState(() {
            showMenu = true;
            _buttonController!.forward();
          });
        });
      },
      child: BlocBuilder<NavigationBLoC, NavigationState>(
          builder: (context, state) {
        return AnimatedPositioned(
          duration: kSlidingPageAnimDuration,
          top: showMenu! ? 55 : 5,
          left: kIsWeb
              ? showMenu!
                  ? 255
                  : 5
              : showMenu!
                  ? MediaQuery.of(context).size.width * 0.55
                  : 5,
          child: (state is ProductDetailsNavigationState)
              ? IconButton(
                  onPressed: () {
                    sl<NavigationBLoC>()
                        .add(const ToMarketplaceNavigationEvent());
                  },
                  icon: const Icon(Icons.arrow_back_ios_new_sharp))
              : IconButton(
                  icon: AnimatedIcon(
                    icon: AnimatedIcons.menu_close,
                    progress: _buttonController!,
                  ),
                  onPressed: buildSetState,
                ),
        );
      }

          // (sl<NavigationBLoC>().state is DetailsPage)
          // ? Icon(Icons.arrow_back_ios_new_sharp)
          // :
          ),
    );
  }

  void buildSetState() {
    if (showMenu!) {
      _slidingPageBLoC!.add(const CloseSlidingPageEvent());
    } else {
      _slidingPageBLoC!.add(const OpenSlidingPageEvent());
    }
  }
}
