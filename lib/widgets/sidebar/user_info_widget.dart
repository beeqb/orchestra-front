import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/sliding_page_bloc.dart';
import '../../injection_container.dart';

import '../loading.dart';

class UserInfoWidget extends StatelessWidget {
  const UserInfoWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is LoggedAuthState) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: kIsWeb ? 80 : 50,
            ),
            child: GestureDetector(
              onTap: () {
                sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
                sl<SlidingPageBLoC>().add(const CloseSlidingPageEvent());
              },
              child: SizedBox(
                height: 80,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(80),
                  child: CachedNetworkImage(
                    imageUrl: state.currentUser.photoUrl ??
                        'https://picsum.photos/200',
                    // placeholder: ,
                    errorWidget: (context, url, error) {
                      debugPrint(
                          'image error ${state.currentUser.photoUrl} $error');
                      return const SizedBox(
                        height: 80,
                        child: Center(
                          child: Icon(
                            Icons.error,
                            color: Colors.red,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          );
        } else if (state is ProcessingAuthState) {
          return const Loading();
        } else {
          return const SizedBox(
            height: 1,
          );
        }
      },
    );
  }
}
