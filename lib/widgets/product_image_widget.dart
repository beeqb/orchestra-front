import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../config.dart';
import 'colors.dart';
import 'loading.dart';

class ProductImageWidget extends StatelessWidget {
  const ProductImageWidget(
      {Key? key,
      required this.imageUrl,
      required this.width,
      required this.height,
      this.borderRadius = 12})
      : super(key: key);

  final double borderRadius;
  final String? imageUrl;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(borderRadius),
        child: CachedNetworkImage(
          height: height,
          width: width,
          fit: BoxFit.cover,
          imageUrl: '$kWidgetAvatarUrl$imageUrl',
          placeholder: (context, url) => const Loading(),
          errorWidget: (context, _, __) => Container(
            color: getRandomColor(),
          ),
        ),
      ),
    );
  }
}
