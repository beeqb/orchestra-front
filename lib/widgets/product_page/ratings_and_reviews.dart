import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/view_models/product_view_model.dart';

import '../../blocs/bloc/ratings_bloc.dart';
import '../../models/product/product.dart';
import '../loading.dart';
import '../marketplace_widgets/star_rating.dart';
import 'add_rating_widget.dart';

class DesktopRatingsAndReviews extends StatelessWidget {
  const DesktopRatingsAndReviews({Key? key, required this.product})
      : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ratings and reviews',
            style: Theme.of(context).textTheme.headlineLarge),
        const SizedBox(
          height: 40,
        ),
        BlocBuilder<RatingsBloc, RatingsState>(builder: (context, state) {
          if (state is RatingsLoaded) {
            return Flexible(
              child: SizedBox(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      width: 87,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(ProductViewModel.rankStringDecimal(product.rank),
                              style: product.rank != null
                                  ? Theme.of(context)
                                      .textTheme
                                      .displayLarge
                                      ?.copyWith(
                                          height: 1,
                                          fontSize: 48,
                                          fontWeight: FontWeight.w300)
                                  : Theme.of(context)
                                      .textTheme
                                      .displayLarge
                                      ?.copyWith(
                                          height: 1,
                                          fontSize: 48,
                                          fontWeight: FontWeight.w300,
                                          color:
                                              Theme.of(context).disabledColor)),
                          const SizedBox(height: 8),
                          StarRating(
                            rating: ProductViewModel.rankAsDouble(product.rank),
                            iconSize: 12,
                          ),
                          const SizedBox(height: 12),
                          Text(
                            '${state.items.length} reviews',
                            style: Theme.of(context).textTheme.labelMedium,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 40,
                    ),
                    Flexible(
                      child: Column(
                        children: [
                          _RatingItem(
                              rating: 5, progress: state.rankWeights[4]),
                          const SizedBox(height: 4),
                          _RatingItem(
                              rating: 4, progress: state.rankWeights[3]),
                          const SizedBox(height: 4),
                          _RatingItem(
                              rating: 3, progress: state.rankWeights[2]),
                          const SizedBox(height: 4),
                          _RatingItem(
                              rating: 2, progress: state.rankWeights[1]),
                          const SizedBox(height: 4),
                          _RatingItem(
                              rating: 1, progress: state.rankWeights[0]),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return const Loading();
        }),
        const SizedBox(height: 43),
        Divider(thickness: 1, color: Theme.of(context).dividerColor),
        AddRatingWidget(
          product: product,
        ),
      ],
    );
  }
}

class _RatingItem extends StatelessWidget {
  const _RatingItem({Key? key, required this.rating, required this.progress})
      : super(key: key);

  final double rating;
  final double progress;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 16,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            rating.toString(),
            style: Theme.of(context).textTheme.bodyMedium,
          ),
          const SizedBox(
            width: 8,
          ),
          StarRating(
            padding: 4,
            rating: rating,
            iconSize: 12,
          ),
          const SizedBox(
            width: 16,
          ),
          Flexible(
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              child: LinearProgressIndicator(
                minHeight: 4,
                valueColor:
                    const AlwaysStoppedAnimation<Color>(Color(0xFFFFAF02)),
                value: progress,
                backgroundColor: Theme.of(context).disabledColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MobileRatingsAndReviews extends StatelessWidget {
  final Product product;

  const MobileRatingsAndReviews({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Ratings and reviews',
            style: Theme.of(context).textTheme.headlineMedium),
        const SizedBox(
          height: 20,
        ),
        BlocBuilder<RatingsBloc, RatingsState>(builder: (context, state) {
          if (state is RatingsLoaded) {
            return Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 80,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(ProductViewModel.rankStringDecimal(product.rank),
                            style: product.rank != null
                                ? Theme.of(context)
                                    .textTheme
                                    .displayLarge
                                    ?.copyWith(
                                        height: 1,
                                        fontSize: 40,
                                        fontWeight: FontWeight.w300)
                                : Theme.of(context)
                                    .textTheme
                                    .displayLarge
                                    ?.copyWith(
                                        height: 1,
                                        fontSize: 40,
                                        fontWeight: FontWeight.w300,
                                        color:
                                            Theme.of(context).disabledColor)),
                        const SizedBox(height: 4),
                        Text(
                          '${state.items.length} reviews',
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium
                              ?.copyWith(fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Flexible(
                    child: Column(
                      children: [
                        _RatingItem(rating: 5, progress: state.rankWeights[4]),
                        const SizedBox(height: 4),
                        _RatingItem(rating: 4, progress: state.rankWeights[3]),
                        const SizedBox(height: 4),
                        _RatingItem(rating: 3, progress: state.rankWeights[2]),
                        const SizedBox(height: 4),
                        _RatingItem(rating: 2, progress: state.rankWeights[1]),
                        const SizedBox(height: 4),
                        _RatingItem(rating: 1, progress: state.rankWeights[0]),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
          return const Loading();
        }),
        const SizedBox(height: 17),
        Divider(thickness: 1, color: Theme.of(context).dividerColor),
        AddRatingWidget(
          product: product,
        ),
      ],
    );
  }
}
