import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/cubits/products_cubit.dart';
import 'package:orchestra/widgets/product_page/recommended_product_tile.dart';

import '../../core/scroll_behavior.dart';
import '../loading.dart';
import '../marketplace_widgets/top_products/top_product_card.dart';

class RecommendedProducts extends StatelessWidget {
  const RecommendedProducts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsCubit, ProductsState>(
      builder: (context, state) {
        if (state is ProductsLoaded) {
          return ScrollConfiguration(
            behavior: MyCustomScrollBehavior(),
            child: ListView.separated(
                separatorBuilder: (_, __) {
                  return const SizedBox(
                    width: 20,
                  );
                },
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: min(state.products.length, 3),
                itemBuilder: (context, index) {
                  return DesktopRecommendedProductTile(
                      product: state.products[index]!);
                }),
          );
        } else {
          return const SizedBox(
            height: 114,
            child: Loading(),
          );
        }
      },
    );
  }
}

class MobileRecommendedProducts extends StatelessWidget {
  const MobileRecommendedProducts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsCubit, ProductsState>(
      builder: (context, state) {
        if (state is ProductsLoaded) {
          return ListView.separated(
              separatorBuilder: (_, __) {
                return const SizedBox(
                  width: 16,
                );
              },
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: min(state.products.length, 3),
              itemBuilder: (context, index) {
                return MobileProductTile(product: state.products[index]!);
              });
        } else {
          return const SizedBox(
            height: 68,
            child: Loading(),
          );
        }
      },
    );
  }
}
