import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../config.dart';
import '../../core/const.dart';
import '../../core/scroll_behavior.dart';
import '../../models/product/product.dart';
import '../colors.dart';
import '../loading.dart';

class ProductImages extends StatelessWidget {
  const ProductImages({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return product.photoDescriptionUrls != null &&
            product.photoDescriptionUrls?.length != 0
        ? SizedBox(
            height: isMobile ? 171 : 471,
            child: ScrollConfiguration(
              behavior: MyCustomScrollBehavior(),
              child: ListView.separated(
                  separatorBuilder: (_, __) => const SizedBox(width: 15),
                  scrollDirection: Axis.horizontal,
                  itemCount: product.photoDescriptionUrls!.length,
                  itemBuilder: (context, index) {
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: AspectRatio(
                        aspectRatio: 4 / 3,
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          imageUrl:
                              '$kWidgetPhotoDescriptionUrl${product.photoDescriptionUrls?[index]}',
                          placeholder: (context, url) => const Loading(),
                          errorWidget: (context, url, error) => Container(
                            color: getRandomColor(),
                          ),
                        ),
                      ),
                    );
                  }),
            ))
        : const SizedBox.shrink();
  }
}
