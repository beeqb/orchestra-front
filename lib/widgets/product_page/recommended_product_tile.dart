import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../blocs/bloc/navigation_bloc.dart';
import '../../injection_container.dart';
import '../../models/product/product.dart';
import '../../view_models/product_view_model.dart';
import '../product_image_widget.dart';

class DesktopRecommendedProductTile extends StatelessWidget {
  const DesktopRecommendedProductTile({Key? key, required this.product})
      : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 412,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).backgroundColor),
      child: TextButton(
        onPressed: () => sl<NavigationBLoC>()
            .add(ToProductDetailsNavigationEvent(product.id)),
        child: Row(children: [
          const SizedBox(
            width: 16,
          ),
          ProductImageWidget(
            imageUrl: product.productLogoURL,
            width: 90,
            height: 90,
          ),
          const SizedBox(
            width: 16,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                product.productName,
                style: Theme.of(context).textTheme.labelMedium,
              ),
              const SizedBox(height: 2),
              Text(
                product.productCategory.title,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
              const SizedBox(height: 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.star_rounded,
                    color: Theme.of(context).disabledColor,
                    size: 12,
                  ),
                  const SizedBox(width: 4),
                  Text(ProductViewModel.rankStringDecimal(product.rank),
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: Theme.of(context).disabledColor)),
                ],
              ),
              const SizedBox(height: 4),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).cardColor),
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 4,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      ProductViewModel.priceToStr(product),
                      style: Theme.of(context).textTheme.labelSmall,
                    )
                  ],
                ),
              ),
            ],
          )
        ]),
      ),
    );
  }
}
