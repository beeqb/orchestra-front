import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/ratings_bloc.dart';
import '../../core/app_colors.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/answer_item.dart';
import '../../models/product/product.dart';
import '../../models/rating_item_model.dart';
import '../../util/text_util.dart';
import '../marketplace_widgets/star_rating.dart';

class RatingItemCard extends StatelessWidget {
  const RatingItemCard({
    Key? key,
    required this.item,
    required this.authState,
    required this.product,
  }) : super(key: key);

  final RatingItem item;
  final AuthState authState;
  final Product product;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Divider(thickness: 1, color: Theme.of(context).dividerColor),
        _CommentHeader(item: item),
        _CommentBody(item: item),
        _AnswerButton(
          product: product,
          item: item,
        ),
      ],
    );
  }
}

class _CommentHeader extends StatelessWidget {
  const _CommentHeader({Key? key, required this.item}) : super(key: key);

  final RatingItem item;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(
          height: 24,
        ),
        Row(
          children: [
            SizedBox(
                width: 40,
                height: 40,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(45.0),
                  child: BlocBuilder<AuthBloc, AuthState>(
                      builder: (context, state) {
                    final avatarUrl = item.user?.avatar;
                    if (avatarUrl != null) {
                      return CachedNetworkImage(imageUrl: avatarUrl);
                    } else {
                      return Image.asset(defaultAvatar);
                    }
                  }),
                )),
            const SizedBox(
              width: 20,
            ),
            Text(
              item.user?.name ?? '',
              style: isMobile
                  ? Theme.of(context).textTheme.labelSmall
                  : Theme.of(context).textTheme.headlineMedium,
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            StarRating(
              rating: item.rank.toDouble(),
              iconSize: 12,
            ),
            const SizedBox(
              width: 12,
            ),
            Text(
              dateFormat.format(DateTime.parse(item.createdAt!)),
              style: isMobile
                  ? Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).disabledColor)
                  : Theme.of(context)
                      .textTheme
                      .labelMedium
                      ?.copyWith(color: Theme.of(context).disabledColor),
            ),
          ],
        ),
      ],
    );
  }
}

class _CommentBody extends StatelessWidget {
  const _CommentBody({Key? key, required this.item}) : super(key: key);

  final RatingItem item;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Column(
      children: [
        const SizedBox(
          height: 12,
        ),
        Text(item.comment!,
            style: isMobile
                ? Theme.of(context).textTheme.bodyMedium
                : Theme.of(context).textTheme.displaySmall),
        item.answer!.isNotEmpty
            ? SizedBox(
                width: double.infinity,
                child: Text('Developer response',
                    style: isMobile
                        ? Theme.of(context).textTheme.titleSmall
                        : Theme.of(context).textTheme.titleMedium),
              )
            : const SizedBox.shrink(),
        item.answer!.isNotEmpty
            ? Text(
                item.answer!,
                style: isMobile
                    ? Theme.of(context).textTheme.bodyMedium
                    : Theme.of(context).textTheme.displaySmall,
              )
            : const SizedBox.shrink(),
        const SizedBox(
          height: 24,
        )
      ],
    );
  }
}

class _AnswerButton extends StatelessWidget {
  const _AnswerButton({Key? key, required this.product, required this.item})
      : super(key: key);

  final Product product;
  final RatingItem item;

  @override
  Widget build(BuildContext context) {
    final authState = sl<AuthBloc>().state;
    final isWidgetAuthor = authState is LoggedAuthState &&
        product.userId == authState.currentUser.id;
    return isWidgetAuthor
        ? Align(
            alignment: Alignment.bottomRight,
            child: GestureDetector(
              onTap: () {
                _showReplyDropdown(
                  context: context,
                  ratingsBloc: BlocProvider.of<RatingsBloc>(context),
                );
              },
              child: const Text(
                'Reply',
                style: TextStyle(color: AppColors.blueColor),
              ),
            ),
          )
        : const SizedBox.shrink();
  }

  void _showReplyDropdown({
    required BuildContext context,
    required RatingsBloc ratingsBloc,
  }) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return BlocProvider.value(
            value: ratingsBloc,
            child: _BottomReplyForm(ratingItem: item),
          );
        });
  }
}

class _BottomReplyForm extends StatelessWidget {
  final RatingItem _ratingItem;

  final TextEditingController _controller = TextEditingController();

  _BottomReplyForm({Key? key, required RatingItem ratingItem})
      : _ratingItem = ratingItem,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width <
                ApplicationsPageConstants.maxWidth
            ? 0
            : (MediaQuery.of(context).size.width -
                    ApplicationsPageConstants.maxWidth) /
                2,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).dialogBackgroundColor,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
          topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
        ),
      ),
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: ConstrainedBox(
        constraints:
            BoxConstraints(maxHeight: MediaQuery.of(context).size.height / 2),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Scrollbar(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    Text(
                      'Respond comment',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text(_ratingItem.comment!),
                    ),
                    Form(
                      child: TextFormField(
                        controller: _controller,
                        decoration:
                            const InputDecoration(labelText: 'Respond comment'),
                        maxLines: 6,
                      ),
                    ),
                    _buttonsPanel(context),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buttonsPanel(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CupertinoButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'CANCEL',
            style: TextStyle(color: Colors.red),
          ),
        ),
        CupertinoButton(
          onPressed: () {
            _saveAnswer(
              ratingItem: _ratingItem,
              ratingsBloc: BlocProvider.of<RatingsBloc>(context),
            );
            Navigator.pop(context);
          },
          child: const Text('SEND'),
        ),
      ],
    );
  }

  void _saveAnswer({
    required RatingsBloc ratingsBloc,
    required RatingItem ratingItem,
  }) {
    final answer = AnswerItem(
      ratingId: ratingItem.id!,
      answer: _controller.text,
    );
    ratingsBloc.add(PostRatingAnswerEvent(
      answerItem: answer,
      productId: ratingItem.productId,
    ));
  }
}
