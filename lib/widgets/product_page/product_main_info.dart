import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../blocs/bloc/auth_bloc.dart';
import '../../blocs/bloc/navigation_bloc.dart';
import '../../blocs/bloc/purchased_bloc.dart';
import '../../blocs/bloc/ratings_bloc.dart';
import '../../blocs/bloc/root_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/app.dart';
import '../../models/product/product.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';
import '../../util/text_util.dart';
import '../../view_models/product_view_model.dart';
import '../applications_widgets/app_creation_settings_form.dart';
import '../marketplace_widgets/star_rating.dart';
import '../product_image_widget.dart';

class DesktopProductMainInfo extends StatelessWidget {
  const DesktopProductMainInfo({Key? key, required this.product})
      : super(key: key);
  final Product product;

  @override
  Widget build(BuildContext context) {
    final testFlyInfo = getLabelText(
        freeForTest: product.pricingAndBusiness.freeForTest!,
        testEndsAfter: product.pricingAndBusiness.testEndsAfter!);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ProductImageWidget(
          imageUrl: product.productLogoURL,
          width: 215,
          height: 216,
        ),
        const SizedBox(
          width: 40,
        ),
        Flexible(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                product.productName,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 12),
              Text(
                product.productDescription,
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              const SizedBox(
                height: 24,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  StarRating(
                    rating: ProductViewModel.rankAsDouble(product.rank),
                    iconSize: 12,
                  ),
                  const SizedBox(width: 4),
                  Text(
                    ProductViewModel.rankStringDecimal(product.rank),
                    style: product.rank == null
                        ? Theme.of(context)
                            .textTheme
                            .labelMedium
                            ?.copyWith(color: Theme.of(context).disabledColor)
                        : Theme.of(context).textTheme.labelMedium,
                  ),
                  BlocBuilder<RatingsBloc, RatingsState>(
                      builder: (context, state) {
                    if (state is RatingsLoaded) {
                      final reviews = state.items;
                      if (reviews.isNotEmpty) {
                        return Text(
                          ' / ${reviews.length}',
                          style: Theme.of(context).textTheme.labelMedium,
                        );
                      } else {
                        return const SizedBox.shrink();
                      }
                    }
                    return const CircularProgressIndicator();
                  }),
                  SizedBox(
                    height: 24,
                    child: VerticalDivider(
                      width: 40,
                      thickness: 1,
                      color: Theme.of(context).dividerTheme.color,
                    ),
                  ),
                  Text(product.productCategory.title),
                  SizedBox(
                    height: 24,
                    child: VerticalDivider(
                      width: 40,
                      thickness: 1,
                      color: Theme.of(context).dividerTheme.color,
                    ),
                  ),
                  Text(
                    'TestFly: $testFlyInfo',
                    overflow: TextOverflow.fade,
                    style: Theme.of(context).textTheme.labelMedium,
                  )
                ],
              ),
              const SizedBox(
                height: 23,
              ),
              BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
                if (state is LoggedAuthState) {
                  return BlocBuilder<PurchasedBloc, PurchasedState>(
                    builder: (context, state) {
                      if (state is PurchasedLoaded) {
                        final purchasedProduct = state.purchasedProducts
                            .firstWhereOrNull(
                                (element) => element.product.id == product.id);
                        if (purchasedProduct != null) {
                          if (!purchasedProduct.isActive) {
                            return buyNowButton(context,
                                purchasedProduct.subscriptionTimeStatus);
                          } else {
                            return createNewApp(context);
                          }
                        }
                        return testFreeButton(context);
                      }
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  );
                } else {
                  return buyButtonUnauthorized(context);
                }
              })
            ],
          ),
        ),
      ],
    );
  }

  Widget testFreeButton(BuildContext context) => Wrap(
        runSpacing: 10,
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        children: [
          Container(
            width: 256,
            height: 48,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () async => await _createApp(product, context),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 12,
                ),
                child: Center(
                  child: Text(
                    "Try for free",
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Theme.of(context).backgroundColor),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 40,
          ),
          Text(
            '${ProductViewModel.priceToStr(product)} after TestFly',
            style: Theme.of(context).textTheme.headlineMedium,
          )
        ],
      );

  Widget buyButtonUnauthorized(BuildContext context) => Container(
        width: 256,
        height: 48,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: ElevatedButton(
          style: Theme.of(context).elevatedButtonTheme.style,
          onPressed: () =>
              sl<NavigationBLoC>().add(const ToLoginNavigationEvent()),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 12,
              ),
              child: Center(
                child: Text(
                  "Buy now",
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ),
              ),
            ),
          ),
        ),
      );

  Widget buyNowButton(BuildContext context, String expired) => Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        runSpacing: 10,
        children: [
          Container(
            width: 256,
            height: 48,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () => showTestFlyGoneDialogMyLore(product: product),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                  ),
                  child: Center(
                    child: Text(
                      "Buy now",
                      style: Theme.of(context)
                          .textTheme
                          .headlineMedium
                          ?.copyWith(color: Theme.of(context).backgroundColor),
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 40,
          ),
          Text(
            expired,
            style: Theme.of(context)
                .textTheme
                .headlineMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          )
        ],
      );

  Widget createNewApp(BuildContext context) => Wrap(
        runSpacing: 10,
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        children: [
          Container(
            width: 256,
            height: 48,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () async => await _createApp(product, context),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 12,
                ),
                child: Center(
                  child: Text(
                    "+ Create new application",
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Theme.of(context).backgroundColor),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 40,
          ),
          Text(
            '${ProductViewModel.priceToStr(product)} after TestFly',
            style: Theme.of(context).textTheme.headlineMedium,
          )
        ],
      );

  Future<void> _createApp(Product product, BuildContext context) async {
    final authState = sl<AuthBloc>().state;
    if (authState is LoggedAuthState) {
      sl<RootBloc>().add(
        StartCreatingAppFromWidgetEvent(
          AppModel(
            userId: authState.currentUser.id!,
            name: product.productName,
            failure: product.failure,
            loop: 'none',
            widgets: [],
            notification: ['modal'],
            status: 'Created',
            icon: product.productLogoURL,
          ),
          product,
        ),
      );
      showDialog(
          context: context,
          builder: (context) {
            return DesktopAppCreationSettingsForm(
              product: product,
            );
          });
    } else {
      sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
    }
  }
}

class MobileProductMainInfo extends StatelessWidget {
  final Product product;

  const MobileProductMainInfo({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final testFlyInfo = getLabelText(
        freeForTest: product.pricingAndBusiness.freeForTest!,
        testEndsAfter: product.pricingAndBusiness.testEndsAfter!);
    return Column(children: [
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ProductImageWidget(
            imageUrl: product.productLogoURL,
            width: 84,
            height: 84,
          ),
          const SizedBox(
            width: 16,
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.productName,
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(height: 8),
                Text(
                  product.productDescription,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            ),
          ),
        ],
      ),
      const SizedBox(
        height: 21,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            children: [
              Row(
                children: [
                  Text(
                    ProductViewModel.rankStringDecimal(product.rank),
                    style: product.rank == null
                        ? Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(color: Theme.of(context).disabledColor)
                        : Theme.of(context).textTheme.bodyMedium,
                  ),
                  BlocBuilder<RatingsBloc, RatingsState>(
                      builder: (context, state) {
                    if (state is RatingsLoaded) {
                      final reviews = state.items;
                      if (reviews.isNotEmpty) {
                        return Text(
                          '/ ${reviews.length}',
                          style: Theme.of(context).textTheme.bodyMedium,
                        );
                      } else {
                        return const SizedBox.shrink();
                      }
                    }
                    return const Center(child: CircularProgressIndicator());
                  }),
                ],
              ),
              const SizedBox(width: 4),
              StarRating(
                rating: ProductViewModel.rankAsDouble(product.rank),
                iconSize: 12,
              ),
            ],
          ),
          SizedBox(
            height: 32,
            child: VerticalDivider(
              width: 8,
              thickness: 1,
              color: Theme.of(context).dividerTheme.color,
            ),
          ),
          Column(
            children: [
              Text(
                product.productCategory.title,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              const SizedBox(width: 4),
              Text(
                'Category',
                style: Theme.of(context)
                    .textTheme
                    .bodySmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              )
            ],
          ),
          SizedBox(
            height: 32,
            child: VerticalDivider(
              width: 8,
              thickness: 1,
              color: Theme.of(context).dividerTheme.color,
            ),
          ),
          Column(
            children: [
              Text(
                testFlyInfo,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              const SizedBox(width: 4),
              Text(
                'TestFly',
                style: Theme.of(context)
                    .textTheme
                    .bodySmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              )
            ],
          ),
          const SizedBox(
            height: 21,
          ),
        ],
      ),
      const SizedBox(
        height: 24,
      ),
      BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
        if (state is LoggedAuthState) {
          return BlocBuilder<PurchasedBloc, PurchasedState>(
            builder: (context, state) {
              if (state is PurchasedLoaded) {
                final purchasedProduct = state.purchasedProducts
                    .firstWhereOrNull(
                        (element) => element.product.id == product.id);
                if (purchasedProduct != null) {
                  if (!purchasedProduct.isActive) {
                    return buyNowButton(
                        context, purchasedProduct.subscriptionTimeStatus);
                  } else {
                    return createNewApp(context);
                  }
                }
                return testFreeButton(context);
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          );
        } else {
          return buyButtonUnauthorized(context);
        }
      }),
    ]);
  }

  Widget testFreeButton(BuildContext context) => Column(
        children: [
          Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () async => await _createApp(product, context),
              child: Center(
                child: Text(
                  "Try for free",
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          Text(
            '${ProductViewModel.priceToStr(product)} after TestFly',
            style: Theme.of(context).textTheme.labelMedium,
          )
        ],
      );

  Widget buyButtonUnauthorized(BuildContext context) => Container(
        width: double.infinity,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
        ),
        child: ElevatedButton(
          style: Theme.of(context).elevatedButtonTheme.style,
          onPressed: () =>
              sl<NavigationBLoC>().add(const ToLoginNavigationEvent()),
          child: Center(
            child: Text(
              "Buy now",
              style: Theme.of(context)
                  .textTheme
                  .labelMedium
                  ?.copyWith(color: Theme.of(context).backgroundColor),
            ),
          ),
        ),
      );

  Widget buyNowButton(BuildContext context, String expired) => Column(
        children: [
          Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () => showTestFlyGoneDialogMyLore(product: product),
              child: Center(
                child: Text(
                  "Buy now",
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          Text(
            expired,
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).disabledColor),
          )
        ],
      );
  Widget createNewApp(BuildContext context) => Column(
        children: [
          Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
            ),
            child: ElevatedButton(
              style: Theme.of(context).elevatedButtonTheme.style,
              onPressed: () async => await _createApp(product, context),
              child: Center(
                child: Text(
                  "+ Create new application",
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          Text(
            '${ProductViewModel.priceToStr(product)} after TestFly',
            style: Theme.of(context).textTheme.labelMedium,
          )
        ],
      );
}

Future<void> _createApp(Product product, BuildContext context) async {
  final authState = sl<AuthBloc>().state;
  if (authState is LoggedAuthState) {
    sl<RootBloc>().add(
      StartCreatingAppFromWidgetEvent(
        AppModel(
          userId: authState.currentUser.id!,
          name: product.productName,
          failure: product.failure,
          loop: 'none',
          widgets: [],
          notification: ['modal'],
          status: 'Created',
          icon: product.productLogoURL,
        ),
        product,
      ),
    );
    showModalBottomSheet(
      useRootNavigator: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: ModalDialogConstants.mobileBorder,
      context: context,
      builder: (context) => MobileAppCreationSettingsForm(product: product),
    );
  } else {
    sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
  }
}

void showTestFlyGoneDialogMyLore({
  required Product product,
}) async {
  final dialogResult = await sl<DialogsService>().showDialog(
    dialogType: DialogType.testFlyGoneDialog,
    title: '',
    product: product,
  );
  if (dialogResult.confirmed!) {
// обработка ответа сервера при покупке виджета
    final result = await sl<BillingService>().chargeForProduct(product.id);
    if (result['status'] == 'error') {
      await sl<DialogsService>().showDialog(
        dialogType: DialogType.warningDialog,
        title: 'Error',
        description: result['message'] ?? '',
      );
    }
  }
}
