import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../core/const.dart';
import '../../models/product/product.dart';
import '../../util/text_util.dart';

class DesktopProductSummary extends StatelessWidget {
  final Product product;

  const DesktopProductSummary({Key? key, required this.product})
      : super(key: key);

  String getProductTypeTitle(String name) =>
      ProductTypeAliases.kProductType[name] ?? '';

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 196,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _DesktopProductInfoTile(
                  title: 'Lore type:',
                  value: getProductTypeTitle(product.productType)),
              _DesktopProductInfoTile(
                  title: 'Version:', value: product.productVersion),
              _DesktopProductInfoTile(
                  title: 'Created at:',
                  value: dateFormat.format(DateTime.parse(product.createdAt!))),
              _DesktopProductInfoTile(
                  title: 'Updated at:',
                  value: dateFormat.format(DateTime.parse(product.updatedAt!))),
              _DesktopProductInfoTile(
                  title: 'Data collection:',
                  value: product.productLauncher.dataCollecting?.title ?? ''),
              _DesktopProductInfoTile(
                  title: 'Incoming data info:',
                  value: product.productLauncher.incomingDataInfo?.title ?? ''),
              _DesktopProductInfoTile(
                  title: 'Outgoing data info:',
                  value: product.productLauncher.outgoingDataInfo?.title ?? ''),
              _DesktopProductInfoTile(
                  title: 'Data processing type:',
                  value:
                      product.productLauncher.dataProcessingType?.title ?? ''),
              _DesktopProductInfoTile(
                  title: 'Payments:',
                  value: product.pricingAndBusiness.model?.title ?? ''),
              _DesktopProductInfoTile(
                  title: 'TestFly:',
                  value: getLabelText(
                      freeForTest: product.pricingAndBusiness.freeForTest!,
                      testEndsAfter:
                          product.pricingAndBusiness.testEndsAfter!)),
              _DesktopProductInfoTile(
                title: 'Contributor:',
                value: product.productContributor,
              )
            ]));
  }
}

class MobileProductSummary extends StatelessWidget {
  final Product product;

  const MobileProductSummary({Key? key, required this.product})
      : super(key: key);

  String getProductTypeTitle(String name) =>
      ProductTypeAliases.kProductType[name]!;

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _MobileProductInfoTile(
              title: 'Lore type:',
              value: getProductTypeTitle(product.productType)),
          _MobileProductInfoTile(
              title: 'Version:', value: product.productVersion),
          _MobileProductInfoTile(
              title: 'Created at:',
              value: dateFormat.format(DateTime.parse(product.createdAt!))),
          _MobileProductInfoTile(
              title: 'Updated at:',
              value: dateFormat.format(DateTime.parse(product.updatedAt!))),
          _MobileProductInfoTile(
              title: 'Data collection:',
              value: product.productLauncher.dataCollecting?.title ?? ''),
          _MobileProductInfoTile(
              title: 'Incoming data info:',
              value: product.productLauncher.incomingDataInfo?.title ?? ''),
          _MobileProductInfoTile(
              title: 'Outgoing data info:',
              value: product.productLauncher.outgoingDataInfo?.title ?? ''),
          _MobileProductInfoTile(
              title: 'Data processing type:',
              value: product.productLauncher.dataProcessingType?.title ?? ''),
          _MobileProductInfoTile(
              title: 'Payments:',
              value: product.pricingAndBusiness.model?.title ?? ''),
          _MobileProductInfoTile(
              title: 'TestFly:',
              value: getLabelText(
                  freeForTest: product.pricingAndBusiness.freeForTest!,
                  testEndsAfter: product.pricingAndBusiness.testEndsAfter!)),
          _MobileProductInfoTile(
            title: 'Contributor:',
            value: product.productContributor,
          )
        ]);
  }
}

class _DesktopProductInfoTile extends StatelessWidget {
  final String title;
  final String value;

  const _DesktopProductInfoTile(
      {Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(title,
            style: Theme.of(context)
                .textTheme
                .labelMedium
                ?.copyWith(color: Theme.of(context).disabledColor)),
        const SizedBox(height: 4),
        Text(
          value,
          style: Theme.of(context).textTheme.titleSmall,
        ),
        const SizedBox(height: 16),
      ],
    );
  }
}

class _MobileProductInfoTile extends StatelessWidget {
  const _MobileProductInfoTile(
      {Key? key, required this.title, required this.value})
      : super(key: key);

  final String title;
  final String value;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(title,
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(color: Theme.of(context).disabledColor)),
            Text(
              value,
              style: Theme.of(context).textTheme.labelSmall,
            ),
          ],
        ),
        const SizedBox(
          height: 12,
        )
      ],
    );
  }
}
