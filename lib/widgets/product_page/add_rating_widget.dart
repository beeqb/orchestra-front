import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/widgets/product_page/raitring_item_card.dart';

import '../../blocs/bloc/ratings_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../loading.dart';
import '../marketplace_widgets/star_rating.dart';

class AddRatingWidget extends StatefulWidget {
  final Product product;
  const AddRatingWidget({
    required this.product,
    Key? key,
  }) : super(key: key);

  @override
  _AddRatingWidgetState createState() => _AddRatingWidgetState();
}

class _AddRatingWidgetState extends State<AddRatingWidget> {
  int rating = 0;
  bool _visible = false;
  bool _showMoreComments = false;
  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return BlocListener<RatingsBloc, RatingsState>(
      listener: (context, state) {
        if (state is RatingsLoaded) {
          showSnackBar(state.message);
        }
      },
      child: BlocBuilder<RatingsBloc, RatingsState>(
        builder: (context, state) {
          if (state is RatingsLoaded) {
            final _authState = sl<AuthBloc>().state;

            final _comments = state.items.map((e) {
              if (e.comment != null) {
                return e.comment;
              }
            }).toList();
            debugPrint('comments: $_comments');
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: isMobile ? 12 : 24),
                Wrap(
                  runSpacing: 10,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Text(
                      'Click to rate and comment',
                      style: isMobile
                          ? Theme.of(context).textTheme.headlineSmall
                          : Theme.of(context).textTheme.headlineMedium,
                    ),
                    SizedBox(
                      width: isMobile ? 12 : 20,
                    ),
                    StarRating(
                      iconSize: isMobile ? 18 : 16,
                      padding: isMobile ? 2 : 4,
                      rating: rating.toDouble(),
                      onRatingChanged: (index) {
                        if (_authState is LoggedAuthState) {
                          setState(() {
                            rating = index as int;
                            _visible = true;
                          });
                          debugPrint('Rating is $rating');
                        } else {
                          const snackBar = SnackBar(
                            content: Text(
                              'You must be logged in!',
                              style: TextStyle(color: Colors.white),
                            ),
                            backgroundColor: Colors.red,
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                    )
                  ],
                ),
                SizedBox(height: isMobile ? 12 : 24),
                _visible
                    ? Column(
                        children: [
                          TextFormField(
                            style: Theme.of(context).textTheme.headlineLarge,
                            controller: controller,
                            decoration: InputDecoration(
                              hintText: 'Your comment',
                              hintStyle: Theme.of(context)
                                  .inputDecorationTheme
                                  .hintStyle,
                              border:
                                  Theme.of(context).inputDecorationTheme.border,
                              focusedBorder: Theme.of(context)
                                  .inputDecorationTheme
                                  .focusedBorder,
                            ),
                            maxLines: 5,
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          _buttonsPanel(),
                          const SizedBox(
                            height: 24,
                          ),
                        ],
                      )
                    : const SizedBox.shrink(),
                _setVisibleComments(_comments)
                    ? ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: min(state.items.length, 3),
                        itemBuilder: (context, index) {
                          return state.items[index].comment != null
                              ? RatingItemCard(
                                  item: state.items[index],
                                  authState: _authState,
                                  product: widget.product)
                              : const SizedBox.shrink();
                        })
                    : const SizedBox.shrink(),
                Divider(thickness: 1, color: Theme.of(context).dividerColor),
                _showMoreComments && state.items.length > 3
                    ? TextButton(
                        style: Theme.of(context)
                            .textButtonTheme
                            .style
                            ?.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).cardColor),
                                fixedSize: isMobile
                                    ? MaterialStateProperty.all<Size>(
                                        const Size(double.infinity, 40))
                                    : MaterialStateProperty.all<Size>(
                                        const Size(138, 44),
                                      )),
                        onPressed: () {
                          setState(() {
                            _showMoreComments = true;
                          });
                        },
                        child: Center(
                            child: Text(
                          'All reviews',
                          style: isMobile
                              ? Theme.of(context).textTheme.labelMedium
                              : Theme.of(context).textTheme.labelLarge,
                        )),
                      )
                    : const SizedBox.shrink(),
                _showMoreComments
                    ? ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: state.items.length - 3,
                        itemBuilder: (context, index) {
                          return state.items[index + 3].comment != null
                              ? RatingItemCard(
                                  item: state.items[index + 3],
                                  authState: _authState,
                                  product: widget.product)
                              : const SizedBox.shrink();
                        })
                    : const SizedBox.shrink(),
              ],
            );
          }
          return const Loading();
        },
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            setState(() {
              _visible = false;
              rating = 0;
            });
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () {
            final authState = sl<AuthBloc>().state;
            if (authState is LoggedAuthState) {
              _saveRank(authState.currentUser.id!);
            }
            controller.clear();
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
          style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).disabledColor),
              ),
        ),
      ],
    );
  }

  void _saveRank(String userId) {
    var _comment = controller.text;
    if (_comment.trim().isEmpty) {
      // _comment = null; // убираем пустые комментарии с пробелами
    }
    BlocProvider.of<RatingsBloc>(context).add(PostSingleRatingEvent(
      productId: widget.product.id,
      userId: userId,
      rank: rating,
      comment: _comment,
    ));
    setState(() {
      _visible = false;
      rating = 0;
    });
  }

  void showSnackBar(Map<String, dynamic> message) {
    if (message.isNotEmpty) {
      final snackBar = SnackBar(
        content: Text(
          message.entries.first.value,
          style: const TextStyle(color: Colors.white),
        ),
        backgroundColor:
            message.entries.first.key == 'error' ? Colors.red : Colors.green,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  bool _setVisibleComments(List<String?> comments) {
    for (var e in comments) {
      if (e is String) return true;
    }
    return false;
  }
}
