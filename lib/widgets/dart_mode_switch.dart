import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class DarkModeSwitch extends StatefulWidget {
  const DarkModeSwitch({
    Key? key,
  }) : super(key: key);

  @override
  _DarkModeSwitchState createState() => _DarkModeSwitchState();
}

class _DarkModeSwitchState extends State<DarkModeSwitch>
    with SingleTickerProviderStateMixin {
  Animation? _circleAnimation;
  AnimationController? _animationController;
  late bool isDarkMode;

  void onChanged() {
    if (AdaptiveTheme.of(context).mode.isDark) {
      AdaptiveTheme.of(context).setLight();
      isDarkMode = false;
    } else {
      AdaptiveTheme.of(context).setDark();
      isDarkMode = true;
    }
    setState(() {});
  }

  bool isCurrentModeDark() {
    var brightness = SchedulerBinding.instance.window.platformBrightness;
    return brightness == Brightness.dark;
  }

  @override
  void initState() {
    isDarkMode = isCurrentModeDark();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100));
    _circleAnimation = AlignmentTween(
            begin: isDarkMode ? Alignment.centerRight : Alignment.centerLeft,
            end: isDarkMode ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController!, curve: Curves.linear));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController!,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController!.isCompleted) {
              _animationController!.reverse();
            } else {
              _animationController!.forward();
            }
            onChanged();
          },
          child: Container(
            width: 40.0,
            height: 24.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              color: isDarkMode ? const Color(0xFF24DF9B) : Colors.grey,
            ),
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Container(
                alignment:
                    isDarkMode ? Alignment.centerRight : Alignment.centerLeft,
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
