import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

import '../../models/results_model.dart';
import '../../util/text_util.dart';
import '../product_image_widget.dart';

class AppResultItem extends StatelessWidget {
  /// Шаблон для результата работы приложения, используется в нижней модалке
  /// на странице Dashboard
  const AppResultItem({
    Key? key,
    required this.data,
    required this.context,
    this.isMobile = true,
  }) : super(key: key);

  final ResultsModel data;
  final BuildContext context;
  final bool isMobile;

  @override
  Widget build(BuildContext context) {
    final double iconSize = isMobile ? 60 : 72;
    final parse = DateTime.parse(data.createdAt);
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding:
            EdgeInsets.symmetric(vertical: 5, horizontal: isMobile ? 14 : 24),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ProductImageWidget(
                imageUrl: data.icon, width: iconSize, height: iconSize),
            const SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                        '${data.name}: '
                        '${dateFormatWithTimeSec.format(parse)}',
                        style: isMobile
                            ? Theme.of(context).textTheme.titleSmall
                            : Theme.of(context).textTheme.headlineMedium),
                  ),
                  const SizedBox(height: 5),
                  HtmlWidget(
                    corsRefactoring(data.result),
                    textStyle: isMobile
                        ? Theme.of(context).textTheme.bodyMedium
                        : Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(fontSize: 16),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
