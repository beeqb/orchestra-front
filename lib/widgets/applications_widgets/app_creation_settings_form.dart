import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/root_bloc.dart';
import '../../blocs/cubits/settings_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/product/product.dart';
import '../../services/app_service.dart';
import '../loading.dart';
import 'application_tile/action_panel/applications_settings_sheet_layout.dart';

class DesktopAppCreationSettingsForm extends StatelessWidget {
  final Product product;

  const DesktopAppCreationSettingsForm({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).backgroundColor,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '${product.productName} settings',
            style: Theme.of(context).textTheme.titleMedium,
          ),
          IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(Icons.close_sharp))
        ],
      ),
      content: SizedBox(
        height: 468,
        width: ModalDialogConstants.desktopDialogWidth,
        child: BlocBuilder<RootBloc, RootState>(
          builder: (context, state) {
            if (state is ReadyRootState) {
              final _apps = state.apps;
              if (_apps.isNotEmpty) {
                _apps.sort((a, b) => DateTime.parse(b.createdAt!)
                    .compareTo(DateTime.parse(a.createdAt!)));
              }
              return BlocProvider<SettingsCubit>(
                create: (context) => SettingsCubit(appService: sl<AppService>())
                  ..readySettings(product.id),
                child: SettingsSheetLayout(
                  _apps.first,
                  product,
                  fromAppView: false,
                ),
              );
            }
            return const Loading();
          },
        ),
      ),
    );
  }
}

class MobileAppCreationSettingsForm extends StatelessWidget {
  final Product product;

  const MobileAppCreationSettingsForm({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: ModalDialogConstants.mobileHorizontalPadding,
          right: ModalDialogConstants.mobileHorizontalPadding,
          bottom: ModalDialogConstants.mobileVerticalPadding),
      child: Column(
        children: [
          SizedBox(
            height: ModalDialogConstants.mobileHeaderHeight,
            child: Column(
              children: [
                const SizedBox(
                  height: 8,
                ),
                Container(
                  width: 32,
                  height: 2,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      '${product.productName} settings',
                      style: Theme.of(context).textTheme.labelSmall,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: BlocBuilder<RootBloc, RootState>(
                builder: (context, state) {
                  if (state is ReadyRootState) {
                    final _apps = state.apps;
                    if (_apps.isNotEmpty) {
                      _apps.sort((a, b) => DateTime.parse(b.createdAt!)
                          .compareTo(DateTime.parse(a.createdAt!)));
                    }
                    return BlocProvider<SettingsCubit>(
                      create: (context) =>
                          SettingsCubit(appService: sl<AppService>())
                            ..readySettings(product.id),
                      child: SettingsSheetLayout(
                        _apps.first,
                        product,
                        fromAppView: false,
                      ),
                    );
                  }
                  return const Loading();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
