import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:orchestra/core/const.dart';

import '../../../../blocs/bloc/app_bloc.dart';
import '../../../../blocs/bloc/app_button_bloc.dart';
import '../../../../blocs/bloc/purchased_bloc.dart';
import '../../../../blocs/bloc/results_bloc.dart';
import '../../../../injection_container.dart';
import '../../../../models/app.dart';
import '../../../../models/enums/dialog_type.dart';
import '../../../../models/product/product.dart';
import '../../../../models/purchase_model.dart';
import '../../../../services/dialogs_service.dart';

class DesktopStartButton extends StatelessWidget {
  final AppModel app;
  final Product product;

  const DesktopStartButton(this.app, this.product, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<ResultsBloc, ResultsState>(
      listener: (context, state) {
        if (state is ResultsLoaded) {
          BlocProvider.of<AppBloc>(context).add(StartUpdatingAppInfo());
        }
      },
      child: Padding(
          padding: const EdgeInsets.only(right: 20), child: appButton(context)),
    );
  }

  PurchaseModel getPurchaseModelByProduct(
      List<PurchaseModel> purchaseModels, Product product) {
    final purchaseModel = purchaseModels
        .firstWhere((element) => element.product.id == product.id);
    return purchaseModel;
  }

  Widget appButton(BuildContext context) {
    final _appBloc = BlocProvider.of<AppBloc>(context);
    return BlocBuilder<PurchasedBloc, PurchasedState>(
      builder: (context, purchasedState) {
        return BlocBuilder<AppBloc, AppState>(
          builder: (context, appState) {
            return BlocBuilder<AppButtonBloc, AppButtonState>(
                builder: (context, appButtonState) {
              if (purchasedState is PurchasedLoaded) {
                final purchaseModel = getPurchaseModelByProduct(
                    purchasedState.purchasedProducts, product);
                if (appButtonState is AppButtonStarted) {
                  return SizedBox(
                    width: 136.0,
                    height: 48.0,
                    child: ElevatedButton(
                        style: Theme.of(context)
                            .elevatedButtonTheme
                            .style
                            ?.copyWith(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Theme.of(context).errorColor),
                            ),
                        onPressed: () {
                          _appBloc.add(StartSwitchingAppEvent(app.id!));
                          sl<DialogsService>().showDialog(
                              dialogType: DialogType.successSnackbar,
                              title: ApplicationsPageConstants.appStarted);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SvgPicture.asset(
                              'assets/icons/stop.svg',
                              color: const Color(0xFFFFFFFF),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Text(
                              'Stop',
                              style: Theme.of(context).textTheme.labelMedium,
                            )
                          ],
                        )),
                  );
                }
                if (appButtonState is AppButtonPaused) {
                  if (appState is AppLoading) {
                    return SizedBox(
                      width: 136.0,
                      height: 48.0,
                      child: ElevatedButton(
                          style: Theme.of(context)
                              .elevatedButtonTheme
                              .style
                              ?.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).iconTheme.color!),
                              ),
                          onPressed: () {
                            _appBloc.add(StartSwitchingAppEvent(app.id!));
                            sl<DialogsService>().showDialog(
                                dialogType: DialogType.successSnackbar,
                                title: ApplicationsPageConstants.appStopped);
                          },
                          child: Center(
                              child: SizedBox(
                            height: 24,
                            width: 24,
                            child: CircularProgressIndicator(
                              strokeWidth: 1.5,
                              color: Theme.of(context).backgroundColor,
                            ),
                          ))),
                    );
                  }
                  if (appState is AppLoaded) {
                    if (purchaseModel.isActive) {
                      return SizedBox(
                          width: 136.0,
                          height: 48.0,
                          child: ElevatedButton(
                              style:
                                  Theme.of(context).elevatedButtonTheme.style,
                              onPressed: () {
                                _appBloc.add(StartSwitchingAppEvent(app.id!));
                                sl<DialogsService>().showDialog(
                                    dialogType: DialogType.successSnackbar,
                                    title:
                                        ApplicationsPageConstants.appStarted);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SvgPicture.asset(
                                    'assets/icons/launch.svg',
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Flexible(
                                    child: Text(
                                      'Launch',
                                      overflow: TextOverflow.fade,
                                      softWrap: false,
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelMedium
                                          ?.copyWith(
                                              color: Theme.of(context)
                                                  .backgroundColor),
                                    ),
                                  ),
                                ],
                              )));
                    } else {
                      return SizedBox(
                        width: 136.0,
                        height: 48.0,
                        child: ElevatedButton(
                            style: Theme.of(context)
                                .elevatedButtonTheme
                                .style
                                ?.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Theme.of(context).cardColor),
                                ),
                            onPressed: () {
                              _appBloc.showTestFlyGoneDialog(
                                  product: product, appModel: appState.app);
                            },
                            child: Text('Buy now',
                                style:
                                    Theme.of(context).textTheme.labelMedium)),
                      );
                    }
                  }
                }
              }
              return const Center(child: CircularProgressIndicator());
            });
          },
        );
      },
    );
  }
}

class MobileStartButton extends StatelessWidget {
  final AppModel app;
  final Product product;

  const MobileStartButton(this.app, this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<ResultsBloc, ResultsState>(
      listener: (context, state) {
        if (state is ResultsLoaded) {
          BlocProvider.of<AppBloc>(context).add(StartUpdatingAppInfo());
        }
      },
      child: appButton(context),
    );
  }

  PurchaseModel getPurchaseModelByProduct(
      List<PurchaseModel> purchaseModels, Product product) {
    final purchaseModel = purchaseModels
        .firstWhere((element) => element.product.id == product.id);
    return purchaseModel;
  }

  Widget appButton(BuildContext context) {
    final _appBloc = BlocProvider.of<AppBloc>(context);
    return BlocBuilder<PurchasedBloc, PurchasedState>(
      builder: (context, purchasedState) {
        return BlocBuilder<AppBloc, AppState>(
          builder: (context, appState) {
            return BlocBuilder<AppButtonBloc, AppButtonState>(
                builder: (context, appButtonState) {
              if (purchasedState is PurchasedLoaded) {
                final purchaseModel = getPurchaseModelByProduct(
                    purchasedState.purchasedProducts, product);
                if (appButtonState is AppButtonStarted) {
                  return SizedBox(
                    width: 199.0,
                    height: 32.0,
                    child: ElevatedButton(
                        style: Theme.of(context)
                            .elevatedButtonTheme
                            .style
                            ?.copyWith(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Theme.of(context).errorColor),
                            ),
                        onPressed: () {
                          _appBloc.add(StartSwitchingAppEvent(app.id!));
                          sl<DialogsService>().showDialog(
                              dialogType: DialogType.successSnackbar,
                              title: ApplicationsPageConstants.appStopped);
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SvgPicture.asset(
                              'assets/icons/stop.svg',
                              color: const Color(0xFFFFFFFF),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Text(
                              'Stop',
                              style: Theme.of(context).textTheme.labelSmall,
                            )
                          ],
                        )),
                  );
                }
                if (appButtonState is AppButtonPaused) {
                  if (appState is AppLoading) {
                    return SizedBox(
                      width: 199.0,
                      height: 32.0,
                      child: ElevatedButton(
                          style: Theme.of(context)
                              .elevatedButtonTheme
                              .style
                              ?.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.all<Color>(
                                        Theme.of(context).iconTheme.color!),
                              ),
                          onPressed: () {
                            _appBloc.add(StartSwitchingAppEvent(app.id!));
                            sl<DialogsService>().showDialog(
                                dialogType: DialogType.successSnackbar,
                                title: ApplicationsPageConstants.appStopped);
                          },
                          child: Center(
                              child: SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(
                              strokeWidth: 1.5,
                              color: Theme.of(context).backgroundColor,
                            ),
                          ))),
                    );
                  }
                  if (appState is AppLoaded) {
                    if (purchaseModel.isActive) {
                      return SizedBox(
                          width: 199.0,
                          height: 32.0,
                          child: ElevatedButton(
                              style:
                                  Theme.of(context).elevatedButtonTheme.style,
                              onPressed: () {
                                _appBloc.add(StartSwitchingAppEvent(app.id!));
                                sl<DialogsService>().showDialog(
                                    dialogType: DialogType.successSnackbar,
                                    title:
                                        ApplicationsPageConstants.appStarted);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SvgPicture.asset(
                                    'assets/icons/launch.svg',
                                    color: Theme.of(context).backgroundColor,
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Text(
                                    'Launch',
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelSmall
                                        ?.copyWith(
                                            color: Theme.of(context)
                                                .backgroundColor),
                                  ),
                                ],
                              )));
                    } else {
                      return SizedBox(
                        width: 199.0,
                        height: 32.0,
                        child: ElevatedButton(
                            style: Theme.of(context)
                                .elevatedButtonTheme
                                .style
                                ?.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.all<Color>(
                                          Theme.of(context).cardColor),
                                ),
                            onPressed: () {
                              _appBloc.showTestFlyGoneDialog(
                                  product: product, appModel: appState.app);
                            },
                            child: Text('Buy now',
                                style: Theme.of(context).textTheme.labelSmall)),
                      );
                    }
                  }
                }
              }
              return const Center(child: CircularProgressIndicator());
            });
          },
        );
      },
    );
  }
}
