import 'package:flutter/material.dart';
import 'package:orchestra/widgets/applications_widgets/application_tile/action_panel/application_settings_dropdown/application_settings_dropdown.dart';
import 'package:orchestra/widgets/applications_widgets/application_tile/action_panel/start_button.dart';

import '../../../../models/app.dart';
import '../../../../models/product/product.dart';
import 'application_loop_settings/application_loop_settings.dart';

class DesktopActionPanel extends StatelessWidget {
  final AppModel app;
  final Product product;

  const DesktopActionPanel({Key? key, required this.app, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          DesktopApplicationSettingsDropdown(
            app: app,
          ),
          DesktopApplicationLoopSettings(
            app: app,
          ),
          const SizedBox(
            width: 12,
          ),
          Flexible(child: DesktopStartButton(app, product)),
        ],
      ),
    );
  }
}

class MobileActionPanel extends StatelessWidget {
  final AppModel app;
  final Product product;

  const MobileActionPanel({Key? key, required this.app, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        MobileStartButton(app, product),
        const SizedBox(
          width: 8,
        ),
        MobileApplicationSettingsDropdown(
          app: app,
        ),
        const SizedBox(
          width: 8,
        ),
        MobileApplicationLoopSettings(
          app: app,
        ),
      ],
    );
  }
}
