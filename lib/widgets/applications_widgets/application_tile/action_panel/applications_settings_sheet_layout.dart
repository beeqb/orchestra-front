import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/settings_app/presentation/widgets/form_settings_widget.dart';

import '../../../../blocs/cubits/settings_cubit.dart';
import '../../../../models/app.dart';
import '../../../loading.dart';

class SettingsSheetLayout extends StatelessWidget {
  final Product product;
  final AppModel app;
  final bool? fromAppView;

  const SettingsSheetLayout(this.app, this.product,
      {Key? key, this.fromAppView})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsCubit, SettingsState>(builder: (context, state) {
      if (state is SettingsLoaded) {
        return FormSettings(
          app: app,
          product: product,
          settings: state.settings,
          oldSettings: state.oldSettings,
          fromAppView: fromAppView,
        );
      } else if (state is SettingsReady) {
        return FormSettings(
          app: app,
          product: product,
          settings: state.settings,
          oldSettings: '',
          fromAppView: fromAppView,
        );
      }
      return const Loading();
    });
  }
}
