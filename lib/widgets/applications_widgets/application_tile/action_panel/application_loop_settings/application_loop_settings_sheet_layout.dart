import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../blocs/bloc/app_bloc.dart';
import '../../../../../core/const.dart';
import '../../../../../models/app.dart';

class DesktopLoopSettingsSheetLayout extends StatefulWidget {
  final AppModel app;
  const DesktopLoopSettingsSheetLayout({required this.app, Key? key})
      : super(key: key);

  @override
  _DesktopLoopSettingsSheetLayoutState createState() =>
      _DesktopLoopSettingsSheetLayoutState();
}

class _DesktopLoopSettingsSheetLayoutState
    extends State<DesktopLoopSettingsSheetLayout> {
  final loops = LoopPeriod.kLoopPeriod;
  String? radioGroupValue;
  String? _loop;

  @override
  void initState() {
    radioGroupValue = widget.app.loop;
    _loop = radioGroupValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Loop each:',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(
              height: 12,
            ),
            Expanded(
              child: Scrollbar(
                child: ListView(
                  shrinkWrap: true,
                  children: List.generate(
                      loops.length,
                      (index) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: RadioListTile(
                                  title: Text(
                                    loops.keys.elementAt(index),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineMedium,
                                  ),
                                  value: loops.values.elementAt(index),
                                  groupValue: radioGroupValue,
                                  onChanged: (String? val) {
                                    setState(() {
                                      radioGroupValue = val;
                                      _loop = val;
                                    });
                                  }),
                            ),
                          )),
                ),
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            buildButtonsPanel(),
          ],
        ),
      ),
    );
  }

  Widget buildButtonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        ElevatedButton(
          onPressed: () {
            final _updApp = widget.app.copyWith(loop: _loop);
            BlocProvider.of<AppBloc>(context)
                .add(StartUpdatingAppSettings(appModel: _updApp));
            Navigator.pop(context);
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }
}
