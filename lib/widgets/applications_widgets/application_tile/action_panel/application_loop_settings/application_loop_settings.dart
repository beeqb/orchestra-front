import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../blocs/bloc/app_bloc.dart';
import '../../../../../core/const.dart';
import '../../../../../models/app.dart';
import 'application_loop_settings_dialog.dart';

class DesktopApplicationLoopSettings extends StatelessWidget {
  final AppModel app;

  const DesktopApplicationLoopSettings({Key? key, required this.app})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: const CircleBorder(),
      onTap: () => _bottomAppLoop(app, context),
      child: BlocBuilder<AppBloc, AppState>(
        builder: (context, state) {
          if (state is AppLoaded) {
            return Container(
              width: 48,
              height: 48,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
              ),
              padding: const EdgeInsets.all(10),
              child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: SvgPicture.asset(
                    'assets/icons/refresh.svg',
                    color: Theme.of(context).iconTheme.color,
                  )),
            );
          }
          return SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                strokeWidth: 1,
                color: Theme.of(context).primaryIconTheme.color,
              ));
        },
      ),
    );
  }

  void _bottomAppLoop(AppModel app, BuildContext context) {
    final appBloc = BlocProvider.of<AppBloc>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) => DesktopApplicationLoopSettingDialog(
              appName: app.name,
              appBloc: appBloc,
            ));
  }
}

class MobileApplicationLoopSettings extends StatelessWidget {
  final AppModel app;

  const MobileApplicationLoopSettings({Key? key, required this.app})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: const CircleBorder(),
      onTap: () => _bottomAppLoop(app, context),
      child: BlocBuilder<AppBloc, AppState>(
        builder: (context, state) {
          if (state is AppLoaded) {
            return Container(
              width: 32,
              height: 32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: SvgPicture.asset(
                  'assets/icons/refresh.svg',
                  color: Theme.of(context).iconTheme.color,
                ),
              ),
            );
          }
          return SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                strokeWidth: 1,
                color: Theme.of(context).primaryIconTheme.color,
              ));
        },
      ),
    );
  }

  void _bottomAppLoop(AppModel app, BuildContext context) {
    final appBloc = BlocProvider.of<AppBloc>(context);
    showModalBottomSheet(
        useRootNavigator: true,
        isScrollControlled: true,
        backgroundColor: Theme.of(context).backgroundColor,
        shape: ModalDialogConstants.mobileBorder,
        context: context,
        builder: (context) => MobileApplicationLoopSettingDialog(
              appName: app.name,
              appBloc: appBloc,
            ));
  }
}

class MobileLoopSettingsWidget extends StatefulWidget {
  final AppModel app;
  const MobileLoopSettingsWidget({required this.app, Key? key})
      : super(key: key);

  @override
  _MobileLoopSettingsWidgetState createState() =>
      _MobileLoopSettingsWidgetState();
}

class _MobileLoopSettingsWidgetState extends State<MobileLoopSettingsWidget> {
  final loops = LoopPeriod.kLoopPeriod;
  String? radioGroupValue;
  String? _loop;

  @override
  void initState() {
    radioGroupValue = widget.app.loop;
    _loop = radioGroupValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Loop each:',
          style: Theme.of(context).textTheme.labelMedium,
        ),
        const SizedBox(
          height: 12,
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ...List.generate(
                    loops.length,
                    (index) => Center(
                          child: RadioListTile(
                              title: Text(
                                loops.keys.elementAt(index),
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              value: loops.values.elementAt(index),
                              groupValue: radioGroupValue,
                              onChanged: (String? val) {
                                setState(() {
                                  radioGroupValue = val;
                                  _loop = val;
                                });
                              }),
                        )),
              ],
            ),
          ),
        ),
        Align(alignment: Alignment.bottomCenter, child: buildButtonsPanel()),
      ],
    );
  }

  Widget buildButtonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: OutlinedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            style: OutlinedButton.styleFrom(
              side: BorderSide(
                width: 1.0,
                color: Theme.of(context).errorColor,
              ),
            ),
            child: SizedBox(
              // width: 140,
              height: 40,
              child: Center(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).errorColor),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        Flexible(
          child: ElevatedButton(
            onPressed: () {
              final _updApp = widget.app.copyWith(loop: _loop);
              BlocProvider.of<AppBloc>(context)
                  .add(StartUpdatingAppSettings(appModel: _updApp));
              Navigator.pop(context);
            },
            child: SizedBox(
                //  width: 140,
                height: 40,
                child: Center(
                    child: Text(
                  'Save',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ))),
          ),
        ),
      ],
    );
  }
}
