import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../blocs/bloc/app_bloc.dart';
import '../../../../../core/const.dart';
import 'application_loop_settings.dart';
import 'application_loop_settings_sheet_layout.dart';

class DesktopApplicationLoopSettingDialog extends StatelessWidget {
  final String appName;
  final AppBloc appBloc;

  const DesktopApplicationLoopSettingDialog(
      {Key? key, required this.appName, required this.appBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        backgroundColor: Theme.of(context).backgroundColor,
        titlePadding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              '$appName loop settings',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: SizedBox(
          width: ModalDialogConstants.desktopDialogWidth,
          height: 468,
          child: BlocProvider.value(
            value: appBloc,
            child: BlocBuilder<AppBloc, AppState>(
              builder: (context, state) {
                if (state is AppLoaded) {
                  return DesktopLoopSettingsSheetLayout(
                    app: state.app,
                  );
                }
                return const SizedBox(height: 1);
              },
            ),
          ),
        ));
  }
}

class MobileApplicationLoopSettingDialog extends StatelessWidget {
  final String appName;
  final AppBloc appBloc;

  const MobileApplicationLoopSettingDialog(
      {Key? key, required this.appName, required this.appBloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: ModalDialogConstants.mobileHorizontalPadding,
          right: ModalDialogConstants.mobileHorizontalPadding,
          bottom: ModalDialogConstants.mobileVerticalPadding),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: ModalDialogConstants.mobileHeaderHeight,
            child: Column(
              children: [
                const SizedBox(
                  height: 8,
                ),
                Container(
                  width: 32,
                  height: 2,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: Theme.of(context).secondaryHeaderColor,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      '$appName loop settings',
                      style: Theme.of(context).textTheme.labelSmall,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: BlocProvider.value(
              value: appBloc,
              child: BlocBuilder<AppBloc, AppState>(
                builder: (context, state) {
                  if (state is AppLoaded) {
                    return MobileLoopSettingsWidget(
                      app: state.app,
                    );
                  }
                  return const SizedBox.shrink();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
