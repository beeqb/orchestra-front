import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../blocs/bloc/app_bloc.dart';
import '../../../../core/const.dart';
import '../../../../models/app.dart';
import '../../../../util/constants.dart';

class DesktopApplicationNotificationsSettings extends StatefulWidget {
  final AppModel app;
  const DesktopApplicationNotificationsSettings({
    required this.app,
    Key? key,
  }) : super(key: key);

  @override
  _DesktopApplicationNotificationsSettingsState createState() =>
      _DesktopApplicationNotificationsSettingsState();
}

class _DesktopApplicationNotificationsSettingsState
    extends State<DesktopApplicationNotificationsSettings> {
  List<bool>? _checkValues;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _telegramController = TextEditingController();
  List<bool> isSaves = [true, true];

  @override
  void initState() {
    // settings is empty
    if (widget.app.notification.isEmpty) {
      _checkValues = <bool>[
        ...NotificationType.kNotifications
            .map((key, value) => MapEntry(key, false))
            .values
            .toList()
      ];
      _checkValues![0] = true;
    } else {
      if (widget.app.email != null) _emailController.text = widget.app.email!;
      if (widget.app.telegram != null) {
        _telegramController.text = widget.app.telegram!;
      }

      final _keys = NotificationType.kNotifications
          .map((key, value) => MapEntry(key, value))
          .keys
          .toList();
//      debugPrint(_keys);
      _checkValues = [];

      for (var e in _keys) {
        if (widget.app.notification.contains(e)) {
          _checkValues!.add(true);
        } else {
          _checkValues!.add(false);
        }
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _telegramController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        margin: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        height: MediaQuery.of(context).size.height / 1.5,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Send App result to:',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            Flexible(
                child: SingleChildScrollView(child: _buildCheckboxesList())),
            const SizedBox(
              height: 24,
            ),
            _buttonsPanel(),
          ],
        ),
      ),
    );
  }

  Widget _buildCheckboxesList() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      ...List.generate(
        _checkValues!.length,
        _buildCheckboxListItem,
      ),
    ]);
  }

  Widget _buildCheckboxListItem(int index) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: Theme.of(context).iconTheme.color!)),
        child: Column(
          children: [
            CheckboxListTile(
              value: _checkValues![index],
              title: Text(
                NotificationType.kNotifications.entries.elementAt(index).value,
                style: Theme.of(context).textTheme.bodyText2,
              ),
              onChanged: (val) {
                setState(() {
                  _checkValues![index] = val!;
                  //  debugPrint(checkValues);
                });
              },
            ),
            NotificationType.kNotifications.entries.elementAt(index).key ==
                        'email' &&
                    _checkValues![index]
                ? Padding(
                    padding: const EdgeInsets.only(
                        bottom: 20.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextField(
                          controller: _emailController,
                          decoration: InputDecoration(
                            labelText: 'Enter email',
                            enabledBorder: OutlineInputBorder(
                              borderSide: !isSaves[0]
                                  ? const BorderSide(color: Colors.red)
                                  : const BorderSide(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(height: 1),
            NotificationType.kNotifications.entries.elementAt(index).key ==
                        'telegram' &&
                    _checkValues![index]
                ? Padding(
                    padding: const EdgeInsets.only(
                        bottom: 20.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextButton(
                          onPressed: () =>
                              launch(kTelegramBotLink, forceSafariVC: false),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Join @OrchestrasBot first, '
                              'to get app results in Telegram!',
                              style: Theme.of(context).textTheme.labelMedium,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: _telegramController,
                          decoration: InputDecoration(
                            labelText: 'Enter your user ID with @',
                            enabledBorder: OutlineInputBorder(
                              borderSide: !isSaves[1]
                                  ? const BorderSide(color: Colors.red)
                                  : const BorderSide(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(height: 1),
          ],
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        ElevatedButton(
          onPressed: () {
            _updateAppSettings(widget.app);
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }

  void _updateAppSettings(AppModel app) {
    setState(() {
      isSaves[0] = true;
      isSaves[1] = true;
    });
    if (_checkValues![2] && _emailController.text.isEmpty) {
      setState(() {
        isSaves[0] = false;
      });
    }
    if (_checkValues![3] && _telegramController.text.isEmpty) {
      setState(() {
        isSaves[1] = false;
      });
    }
    if (isSaves[0] && isSaves[1]) {
      var _notif = <String>[];
      for (var i = 0; i < _checkValues!.length; i++) {
        if (_checkValues![i]) {
          _notif.add(NotificationType.kNotifications.keys.elementAt(i));
        }
      }

      final _updatedApp = app.copyWith(
        notification: _notif,
        email: _emailController.text,
        telegram: _telegramController.text,
      );

      BlocProvider.of<AppBloc>(context)
          .add(StartUpdatingAppSettings(appModel: _updatedApp));
      Navigator.pop(context);
      _emailController.clear();
      _telegramController.clear();
    }
  }
}

class MobileApplicationNotificationsSettings extends StatefulWidget {
  final AppModel app;
  const MobileApplicationNotificationsSettings({
    required this.app,
    Key? key,
  }) : super(key: key);

  @override
  _MobileApplicationNotificationsSettingsState createState() =>
      _MobileApplicationNotificationsSettingsState();
}

class _MobileApplicationNotificationsSettingsState
    extends State<MobileApplicationNotificationsSettings> {
  List<bool>? _checkValues;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _telegramController = TextEditingController();
  List<bool> isSaves = [true, true];

  @override
  void initState() {
    // settings is empty
    if (widget.app.notification.isEmpty) {
      _checkValues = <bool>[
        ...NotificationType.kNotifications
            .map((key, value) => MapEntry(key, false))
            .values
            .toList()
      ];
      _checkValues![0] = true;
    } else {
      if (widget.app.email != null) _emailController.text = widget.app.email!;
      if (widget.app.telegram != null) {
        _telegramController.text = widget.app.telegram!;
      }

      final _keys = NotificationType.kNotifications
          .map((key, value) => MapEntry(key, value))
          .keys
          .toList();
//      debugPrint(_keys);
      _checkValues = [];

      for (var e in _keys) {
        if (widget.app.notification.contains(e)) {
          _checkValues!.add(true);
        } else {
          _checkValues!.add(false);
        }
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _telegramController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Column(
        //mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Send App result to:',
            style: Theme.of(context).textTheme.labelSmall?.copyWith(
                color: Theme.of(context).popupMenuTheme.textStyle?.color),
          ),
          _buildCheckboxesList(),
          const SizedBox(
            height: 12,
          ),
          _buttonsPanel(),
        ],
      ),
    );
  }

  Widget _buildCheckboxesList() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      ...List.generate(
        _checkValues!.length,
        _buildCheckboxListItem,
      ),
    ]);
  }

  Widget _buildCheckboxListItem(int index) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: Theme.of(context).iconTheme.color!)),
        child: Column(
          children: [
            CheckboxListTile(
              value: _checkValues![index],
              title: Text(
                NotificationType.kNotifications.entries.elementAt(index).value,
                style: Theme.of(context).textTheme.labelSmall,
              ),
              onChanged: (val) {
                setState(() {
                  _checkValues![index] = val!;
                  //  debugPrint(checkValues);
                });
              },
            ),
            NotificationType.kNotifications.entries.elementAt(index).key ==
                        'email' &&
                    _checkValues![index]
                ? Padding(
                    padding: const EdgeInsets.only(
                        bottom: 20.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextField(
                          controller: _emailController,
                          decoration: InputDecoration(
                            labelText: 'Enter email',
                            enabledBorder: OutlineInputBorder(
                              borderSide: !isSaves[0]
                                  ? const BorderSide(color: Colors.red)
                                  : const BorderSide(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(height: 1),
            NotificationType.kNotifications.entries.elementAt(index).key ==
                        'telegram' &&
                    _checkValues![index]
                ? Padding(
                    padding: const EdgeInsets.only(
                        bottom: 20.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        TextButton(
                          onPressed: () =>
                              launch(kTelegramBotLink, forceSafariVC: false),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Join @OrchestrasBot first, '
                              'to get app results in Telegram!',
                              style: Theme.of(context).textTheme.labelMedium,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        TextField(
                          controller: _telegramController,
                          decoration: InputDecoration(
                            labelText: 'Enter your user ID with @',
                            enabledBorder: OutlineInputBorder(
                              borderSide: !isSaves[1]
                                  ? const BorderSide(color: Colors.red)
                                  : const BorderSide(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(height: 1),
          ],
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              side: BorderSide(
                width: 1.0,
                color: Theme.of(context).errorColor,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: SizedBox(
              //  width: 140,
              height: 40,
              child: Center(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Theme.of(context).errorColor),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              _updateAppSettings(widget.app);
            },
            child: SizedBox(
                //width: 140,
                height: 40,
                child: Center(
                    child: Text(
                  'Save',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ))),
          ),
        ),
      ],
    );
  }

  void _updateAppSettings(AppModel app) {
    setState(() {
      isSaves[0] = true;
      isSaves[1] = true;
    });
    if (_checkValues![2] && _emailController.text.isEmpty) {
      setState(() {
        isSaves[0] = false;
      });
    }
    if (_checkValues![3] && _telegramController.text.isEmpty) {
      setState(() {
        isSaves[1] = false;
      });
    }
    if (isSaves[0] && isSaves[1]) {
      var _notif = <String>[];
      for (var i = 0; i < _checkValues!.length; i++) {
        if (_checkValues![i]) {
          _notif.add(NotificationType.kNotifications.keys.elementAt(i));
        }
      }

      final _updatedApp = app.copyWith(
        notification: _notif,
        email: _emailController.text,
        telegram: _telegramController.text,
      );

      BlocProvider.of<AppBloc>(context)
          .add(StartUpdatingAppSettings(appModel: _updatedApp));
      Navigator.pop(context);
      _emailController.clear();
      _telegramController.clear();
    }
  }
}
