import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../blocs/cubits/app_widgets_cubit.dart';
import '../../../../../blocs/cubits/settings_cubit.dart';
import '../../../../../core/const.dart';
import '../../../../../injection_container.dart';
import '../../../../../models/app.dart';
import '../../../../../services/app_service.dart';
import '../../../../loading.dart';
import '../applications_settings_sheet_layout.dart';

class DesktopApplicationSettingsModal extends StatelessWidget {
  final AppModel app;
  final AppWidgetsCubit appWidgetsCubit;

  const DesktopApplicationSettingsModal(
      {Key? key, required this.app, required this.appWidgetsCubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        backgroundColor: Theme.of(context).backgroundColor,
        titlePadding: const EdgeInsets.symmetric(
            vertical: ModalDialogConstants.mobileVerticalPadding,
            horizontal: ModalDialogConstants.mobileHorizontalPadding),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              '${app.name} settings',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: BlocProvider.value(
            value: appWidgetsCubit,
            child: BlocBuilder<AppWidgetsCubit, AppWidgetsState>(
              builder: (context, state) {
                if (state is AppWidgetsLoaded) {
                  return BlocProvider(
                    create: (context) =>
                        SettingsCubit(appService: sl<AppService>())
                          ..getWidgetSettings(
                            app.id!,
                            state.appProducts[0].userId,
                            state.appProducts[0].id,
                            app.userId,
                          ),
                    child: SettingsSheetLayout(
                      app,
                      state.appProducts[0],
                      fromAppView: true,
                    ),
                  );
                }
                return const Loading();
              },
            )));
  }
}

class MobileApplicationSettingsModal extends StatelessWidget {
  final AppModel app;
  final AppWidgetsCubit appWidgetsCubit;

  const MobileApplicationSettingsModal(
      {Key? key, required this.app, required this.appWidgetsCubit})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.only(
                left: ModalDialogConstants.mobileHorizontalPadding,
                right: ModalDialogConstants.mobileHorizontalPadding,
                bottom: ModalDialogConstants.mobileVerticalPadding),
            child: Wrap(children: [
              SizedBox(
                height: ModalDialogConstants.mobileHeaderHeight,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: 32,
                      height: 2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        color: Theme.of(context).secondaryHeaderColor,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          '${app.name} settings',
                          style: Theme.of(context).textTheme.labelSmall,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              BlocProvider.value(
                  value: appWidgetsCubit,
                  child: BlocBuilder<AppWidgetsCubit, AppWidgetsState>(
                    builder: (context, state) {
                      if (state is AppWidgetsLoaded) {
                        return BlocProvider(
                          create: (context) =>
                              SettingsCubit(appService: sl<AppService>())
                                ..getWidgetSettings(
                                  app.id!,
                                  state.appProducts[0].userId,
                                  state.appProducts[0].id,
                                  app.userId,
                                ),
                          child: SettingsSheetLayout(
                            app,
                            state.appProducts[0],
                            fromAppView: true,
                          ),
                        );
                      }
                      return const Loading();
                    },
                  )),
            ])),
      ),
    );
  }
}
