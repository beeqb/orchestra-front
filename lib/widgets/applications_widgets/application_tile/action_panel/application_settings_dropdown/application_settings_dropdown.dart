import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../blocs/bloc/app_bloc.dart';
import '../../../../../blocs/bloc/root_bloc.dart';
import '../../../../../blocs/cubits/app_widgets_cubit.dart';
import '../../../../../core/const.dart';
import '../../../../../injection_container.dart';
import '../../../../../models/app.dart';
import '../../../../../models/enums/confirm_action_type.dart';
import '../../../../dialogs/custom_alert_dialog.dart';
import 'application_settings_modal.dart';
import 'notification_settings_modal.dart';

class DesktopApplicationSettingsDropdown extends StatelessWidget {
  final AppModel app;

  const DesktopApplicationSettingsDropdown({Key? key, required this.app})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
        icon: Icon(
          Icons.more_horiz_outlined,
          color: Theme.of(context).iconTheme.color,
        ),
        splashRadius: 3,
        tooltip: 'Settings',
        padding: EdgeInsets.zero,
        color: Theme.of(context).backgroundColor,
        constraints: const BoxConstraints(
          minWidth: 236,
          maxWidth: 236,
        ),
        position: PopupMenuPosition.under,
        itemBuilder: (context) {
          return [
            PopupMenuItem(
                onTap: () => _showApplicationSettings(app, context),
                child: const Text('Settings')),
            PopupMenuItem(
              onTap: () => _showNotificationSettings(app, context),
              child: const Text('Notifications'),
            ),
            PopupMenuItem(
                onTap: () async {
                  final confirmAction = await _showDeleteAppDialog(context);
                  if (confirmAction == ConfirmAction.accept) {
                    sl<RootBloc>().add(StartDeletingAppEvent(app));
                  }
                },
                child: const Text('Delete app'))
          ];
        });
  }

  void _showApplicationSettings(AppModel app, BuildContext context) {
    final appWidgetsCubit = BlocProvider.of<AppWidgetsCubit>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) => DesktopApplicationSettingsModal(
            app: app, appWidgetsCubit: appWidgetsCubit));
  }

  void _showNotificationSettings(AppModel app, BuildContext context) {
    final appBloc = BlocProvider.of<AppBloc>(context);
    showDialog(
        context: context,
        builder: (BuildContext context) => DesktopNotificationSettingsModal(
            appName: app.name, appBloc: appBloc));
  }

  Future<dynamic> _showDeleteAppDialog(BuildContext context) async =>
      showDialog(
        context: context,
        builder: (ctx) =>
            const CustomAlertDialog(message: 'The app will be removed!'),
      );
}

class MobileApplicationSettingsDropdown extends StatelessWidget {
  final AppModel app;

  const MobileApplicationSettingsDropdown({Key? key, required this.app})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      width: 32,
      child: PopupMenuButton(
          splashRadius: 16,
          icon: Icon(
            Icons.more_horiz_outlined,
            color: Theme.of(context).iconTheme.color,
          ),
          tooltip: 'Settings',
          padding: EdgeInsets.zero,
          color: Theme.of(context).backgroundColor,
          constraints: const BoxConstraints(
            minWidth: 236,
            maxWidth: 236,
          ),
          position: PopupMenuPosition.under,
          itemBuilder: (context) {
            return [
              PopupMenuItem(
                  onTap: () => _showApplicationSettings(app, context),
                  child: const Text('Settings')),
              PopupMenuItem(
                onTap: () => _showNotificationSettings(app, context),
                child: const Text('Notifications'),
              ),
              PopupMenuItem(
                  onTap: () async {
                    final confirmAction = await _showDeleteAppDialog(context);
                    if (confirmAction == ConfirmAction.accept) {
                      sl<RootBloc>().add(StartDeletingAppEvent(app));
                    }
                  },
                  child: const Text('Delete app'))
            ];
          }),
    );
  }

  void _showApplicationSettings(AppModel app, BuildContext context) {
    final appWidgetsCubit = BlocProvider.of<AppWidgetsCubit>(context);
    showModalBottomSheet(
        useRootNavigator: true,
        isScrollControlled: true,
        backgroundColor: Theme.of(context).backgroundColor,
        shape: ModalDialogConstants.mobileBorder,
        context: context,
        builder: (_) => MobileApplicationSettingsModal(
            app: app, appWidgetsCubit: appWidgetsCubit));
  }

  void _showNotificationSettings(AppModel app, BuildContext context) {
    final appBloc = BlocProvider.of<AppBloc>(context);
    showModalBottomSheet(
        isScrollControlled: true,
        useRootNavigator: true,
        backgroundColor: Theme.of(context).backgroundColor,
        shape: ModalDialogConstants.mobileBorder,
        context: context,
        builder: (context) => MobileNotificationSettingsModal(
            appName: app.name, appBloc: appBloc));
  }

  Future<dynamic> _showDeleteAppDialog(BuildContext context) async =>
      showDialog(
        context: context,
        builder: (ctx) =>
            const CustomAlertDialog(message: 'The app will be removed!'),
      );
}
