import 'dart:async';

import 'package:flutter/material.dart';

import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../../models/app.dart';
import '../../../services/app_service.dart';

class AppTitle extends StatefulWidget {
  const AppTitle({Key? key, required this.app}) : super(key: key);
  final AppModel app;

  @override
  _AppTitleState createState() => _AppTitleState();
}

class _AppTitleState extends State<AppTitle> {
  String name = '';
  TextEditingController controller = TextEditingController();
  bool enabledText = false;
  FocusNode focus = FocusNode();

  @override
  void initState() {
    name = widget.app.name;
    controller.text = name;
    super.initState();
  }

  void saveApp() {
    if (controller.text.isNotEmpty) {
      var newApp = widget.app.copyWith(name: controller.text);
      sl<AppService>().updateApp(newApp);
    }
  }

  @override
  void dispose() {
    focus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var ts = TextSpan(
        style: Theme.of(context).textTheme.labelMedium, text: controller.text);
    var tp = TextPainter(text: ts, textDirection: TextDirection.ltr);
    tp.layout();
    var textWidth = tp.width + 2.0;
    // ignore: unused_local_variable
    final fc = FocusNode();
    var isFailure = widget.app.failure ?? false;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          color:
              enabledText ? Colors.grey.withOpacity(0.2) : Colors.transparent,
          width: textWidth,
          child: GestureDetector(
            onDoubleTap: () async {
              setState(() {
                enabledText = true;
              });
              if (enabledText) {
                await Future.delayed(const Duration(milliseconds: 500));
                focus.requestFocus();
              }
            },
            child: TextField(
              focusNode: focus,
              textAlign: TextAlign.center,
              decoration: null,
              onChanged: (text) {
                setState(() {
                  debugPrint('updateText');
                });
              },
              onEditingComplete: () {
                debugPrint('complete');
                setState(() {
                  enabledText = false;
                });
                saveApp();
              },
              enabled: enabledText,
              controller: controller,
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
        ),
        enabledText
            ? IconButton(
                onPressed: () {
                  setState(() {
                    enabledText = false;
                  });
                  saveApp();
                },
                icon: const Icon(Icons.keyboard_return),
              )
            : isFailure
                ? IconButton(
                    onPressed: () {
                      // _showDialog();
                    },
                    tooltip: failureWidgetMessage,
                    icon: const Icon(
                      Icons.circle,
                      color: Colors.red,
                      size: 15,
                    ),
                  )
                : const SizedBox.shrink(),
      ],
    );
  }
}
