import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../blocs/bloc/app_bloc.dart';

class DesktopLaunchInfo extends StatelessWidget {
  const DesktopLaunchInfo({
    Key? key,
  }) : super(key: key);

  lastLaunch(int? date) {
    if (date != null) {
      DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(date);
      String dateString = DateFormat('dd.MM.yy / HH:mm').format(dateTime);
      return dateString;
    } else {
      return 'No launches';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: BlocBuilder<AppBloc, AppState>(builder: (context, state) {
          if (state is AppLoaded) {
            if (state.app.status != 'Active') {
              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      lastLaunch(state.app.startTime as int?),
                      style: Theme.of(context).textTheme.labelMedium,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Last launched',
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                          fontWeight: FontWeight.normal),
                    ),
                  ]);
            } else {
              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                            width: 20,
                            height: 20,
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                              color: Theme.of(context).primaryIconTheme.color,
                            )),
                        const SizedBox(
                          width: 8,
                        ),
                        Flexible(
                          child: Text(
                            'In progress',
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.labelMedium,
                          ),
                        )
                      ],
                    ),
                    Text(
                      'Last launched',
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.labelMedium?.copyWith(
                          color: Theme.of(context).iconTheme.color,
                          fontWeight: FontWeight.normal),
                    )
                  ]);
            }
          } else {
            return Center(
              child: SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                    strokeWidth: 1,
                    color: Theme.of(context).primaryIconTheme.color,
                  )),
            );
          }
        }),
      ),
    );
  }
}

class MobileLaunchInfo extends StatelessWidget {
  const MobileLaunchInfo({
    Key? key,
  }) : super(key: key);

  lastLaunch(int? date) {
    if (date != null) {
      DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(date);
      String dateString = DateFormat('dd.MM.yy / HH:mm').format(dateTime);
      return dateString;
    } else {
      return 'No launches';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: BlocBuilder<AppBloc, AppState>(builder: (context, state) {
        if (state is AppLoaded) {
          if (state.app.status != 'Active') {
            return Text(
              lastLaunch(state.app.startTime as int?),
              style: Theme.of(context).textTheme.labelSmall?.copyWith(
                  fontSize: 12, color: Theme.of(context).disabledColor),
              overflow: TextOverflow.ellipsis,
            );
          } else {
            return Row(
              children: [
                SizedBox(
                    width: 14,
                    height: 14,
                    child: CircularProgressIndicator(
                      strokeWidth: 1,
                      color: Theme.of(context).primaryIconTheme.color,
                    )),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  'In progress',
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.labelSmall?.copyWith(
                      fontSize: 12, color: Theme.of(context).disabledColor),
                )
              ],
            );
          }
        } else {
          return Center(
            child: Container(
              height: 20,
              width: 20,
              padding: const EdgeInsets.all(5),
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
          );
        }
      }),
    );
  }
}
