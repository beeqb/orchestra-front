import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/widgets/applications_widgets/application_tile/settings_info.dart';
import 'package:orchestra/widgets/product_image_widget.dart';

import '../../../blocs/cubits/app_widgets_cubit.dart';
import '../../../blocs/cubits/settings_cubit.dart';
import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../../models/app.dart';
import '../../../services/app_service.dart';
import 'action_panel/action_panel.dart';
import 'application_tile_shimmer.dart';
import 'launch_info.dart';

class DesktopApplicationTile extends StatefulWidget {
  final AppModel app;
  const DesktopApplicationTile({
    required this.app,
    Key? key,
  }) : super(key: key);

  /// Для поиска _AppViewState в контексте
  static _DesktopApplicationTileState of(BuildContext context) =>
      context.findAncestorStateOfType<_DesktopApplicationTileState>()!;

  @override
  _DesktopApplicationTileState createState() => _DesktopApplicationTileState();
}

class _DesktopApplicationTileState extends State<DesktopApplicationTile> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppWidgetsCubit, AppWidgetsState>(
        builder: (context, state) {
      if (state is AppWidgetsLoaded) {
        if (state.appProducts.isEmpty) return const SizedBox.shrink();
        return BlocProvider(
            create: (context) => SettingsCubit(appService: sl<AppService>())
              ..getWidgetSettings(
                widget.app.id!,
                state.appProducts[0].userId,
                state.appProducts[0].id,
                widget.app.userId,
              ),
            child: Column(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Container(
                    height: 96,
                    // width: MediaQuery.of(context).size.width,
                    color: Theme.of(context).backgroundColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: ProductImageWidget(
                              imageUrl: widget.app.icon, width: 72, height: 72),
                        ),
                        Expanded(
                            flex: 2,
                            child: Row(
                              children: [
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      //AppTitle(widget.app),
                                      Text(
                                        widget.app.name,
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelMedium,
                                      ),
                                      const SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        state.appProducts.first.productCategory
                                            .title,
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelMedium
                                            ?.copyWith(
                                                color: Theme.of(context)
                                                    .iconTheme
                                                    .color,
                                                fontWeight: FontWeight.normal),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                        Expanded(
                          flex: 2,
                          child: Row(
                            children: const [
                              DesktopSettingsInfo(),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 2,
                            child: Row(
                              //  mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: const [
                                DesktopLaunchInfo(),
                              ],
                            )),
                        Flexible(
                            flex: 5,
                            child: DesktopActionPanel(
                                app: widget.app,
                                product: state.appProducts.first)),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16,
                )
              ],
            ));
      }
      return const DesktopApplicationTileShimmer();
    });
  }
}

class MobileApplicationTile extends StatefulWidget {
  final AppModel app;
  const MobileApplicationTile({
    required this.app,
    Key? key,
  }) : super(key: key);

  /// Для поиска _AppViewState в контексте
  static _MobileApplicationTileState of(BuildContext context) =>
      context.findAncestorStateOfType<_MobileApplicationTileState>()!;

  @override
  _MobileApplicationTileState createState() => _MobileApplicationTileState();
}

class _MobileApplicationTileState extends State<MobileApplicationTile> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppWidgetsCubit, AppWidgetsState>(
        builder: (context, state) {
      if (state is AppWidgetsLoaded) {
        if (state.appProducts.isEmpty) return const SizedBox.shrink();
        return BlocProvider(
            create: (context) => SettingsCubit(appService: sl<AppService>())
              ..getWidgetSettings(
                widget.app.id!,
                state.appProducts[0].userId,
                state.appProducts[0].id,
                widget.app.userId,
              ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 102,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ProductImageWidget(
                              imageUrl: widget.app.icon,
                              width: ProductCardSizeConstants.mobileCardHeight,
                              height: ProductCardSizeConstants.mobileCardHeight,
                            ),
                            const SizedBox(
                              width: 12,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                //AppTitle(widget.app),
                                Text(
                                  widget.app.name,
                                  style: Theme.of(context).textTheme.labelSmall,
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                const MobileSettingsInfo(),
                                const SizedBox(
                                  height: 8,
                                ),
                                const MobileLaunchInfo(),
                              ],
                            ),
                          ],
                        ),
                      ),
                      MobileActionPanel(
                          app: widget.app, product: state.appProducts.first)
                    ],
                  ),
                ),
                const SizedBox(
                  height: 16,
                )
              ],
            ));
      }
      return const MobileApplicationTileShimmer();
    });
  }
}
