import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class DesktopApplicationTileShimmer extends StatelessWidget {
  const DesktopApplicationTileShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Shimmer.fromColors(
          baseColor: Theme.of(context).disabledColor,
          highlightColor: Theme.of(context).backgroundColor,
          child: Container(
            height: 96,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.white),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(2.0),
                      border: Border.all(),
                    ),
                    width: 72,
                    height: 72,
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              //AppTitle(widget.app),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              //AppTitle(widget.app),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                Expanded(
                    flex: 2,
                    child: Row(
                      children: [
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              //AppTitle(widget.app),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Container(
                                width: 100,
                                height: 5.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
                Expanded(
                    child: Container(
                  width: 15.0,
                  height: 15.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(45),
                    border: Border.all(color: Colors.white),
                  ),
                )),
                Expanded(
                    child: Container(
                  width: 15.0,
                  height: 15.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(45),
                    border: Border.all(color: Colors.white),
                  ),
                )),
                Flexible(
                    flex: 5,
                    child: Container(
                      width: 136.0,
                      height: 48.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(color: Colors.white),
                      ),
                    )),
              ],
            ),
          )),
    );
  }
}

class MobileApplicationTileShimmer extends StatelessWidget {
  const MobileApplicationTileShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Shimmer.fromColors(
          baseColor: Theme.of(context).disabledColor,
          highlightColor: Theme.of(context).backgroundColor,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(2.0),
                        border: Border.all(),
                      ),
                      width: 72,
                      height: 72,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //AppTitle(widget.app),
                      Container(
                        width: 100,
                        height: 5.0,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: 100,
                        height: 5.0,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Container(
                        width: 100,
                        height: 5.0,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    width: 199.0,
                    height: 32.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      border: Border.all(color: Colors.white),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Container(
                    width: 15.0,
                    height: 15.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(color: Colors.white),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Container(
                    width: 15.0,
                    height: 15.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(45),
                      border: Border.all(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
