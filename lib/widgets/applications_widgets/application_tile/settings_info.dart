import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/cubits/settings_cubit.dart';

class DesktopSettingsInfo extends StatelessWidget {
  const DesktopSettingsInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<SettingsCubit, SettingsState>(
                builder: (context, state) {
              print(state);
              if (state is SettingsLoaded) {
                final settings = _getSettingsFromJson(state.oldSettings);
                return Text(
                  settings,
                  style: Theme.of(context).textTheme.labelMedium,
                  overflow: TextOverflow.ellipsis,
                );
              }
              if (state is SettingsUpdated) {
                final settings = _getSettingsFromMap(state.settings);
                return Text(
                  settings,
                  style: Theme.of(context).textTheme.labelMedium,
                  overflow: TextOverflow.ellipsis,
                );
              }

              return Center(
                child: SizedBox(
                    width: 20,
                    height: 20,
                    child: CircularProgressIndicator(
                      strokeWidth: 1,
                      color: Theme.of(context).primaryIconTheme.color,
                    )),
              );
            }),
            const SizedBox(
              height: 8,
            ),
            Text(
              'Settings',
              style: Theme.of(context).textTheme.labelMedium?.copyWith(
                  color: Theme.of(context).iconTheme.color,
                  fontWeight: FontWeight.normal),
            )
          ],
        ),
      ),
    );
  }
}

class MobileSettingsInfo extends StatelessWidget {
  const MobileSettingsInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsCubit, SettingsState>(builder: (context, state) {
      if (state is SettingsLoaded) {
        final settings = _getSettingsFromJson(state.oldSettings);
        return Text(
          settings,
          style: Theme.of(context)
              .textTheme
              .labelSmall
              ?.copyWith(fontSize: 12, color: Theme.of(context).disabledColor),
          overflow: TextOverflow.ellipsis,
        );
      }
      if (state is SettingsUpdated) {
        final settings = _getSettingsFromMap(state.settings);
        return Text(
          settings,
          style: Theme.of(context)
              .textTheme
              .labelSmall
              ?.copyWith(fontSize: 12, color: Theme.of(context).disabledColor),
          overflow: TextOverflow.ellipsis,
        );
      }

      return Center(
        child: SizedBox(
            width: 20,
            height: 20,
            child: CircularProgressIndicator(
              strokeWidth: 1,
              color: Theme.of(context).primaryIconTheme.color,
            )),
      );
    });
  }
}

String _getSettingsFromJson(dynamic settingsJson) {
  if (settingsJson.isNotEmpty) {
    List settingsDecoded = json.decode(settingsJson);
    Map<dynamic, dynamic>? settingsField = settingsDecoded.first['settings'];
    if (settingsField != null && settingsField.values.isNotEmpty) {
      List<dynamic> val = settingsField.values.toList();
      val.removeWhere((element) => element == null);
      val.removeWhere((element) => element?.trim == '');
      if (val.isEmpty) {
        return '';
      }
      final settings = val.join(' / ');
      return settings;
    } else {
      return '';
    }
  } else {
    return '';
  }
}

String _getSettingsFromMap(Map<String, dynamic> settings) {
  if (settings != null && settings.values.isNotEmpty) {
    List<dynamic> val = settings.values.toList();
    val.removeWhere((element) => element == null);
    val.removeWhere((element) => element?.trim == '');
    if (val.isEmpty) {
      return '';
    }
    final newSettings = val.join(' / ');
    return newSettings;
  } else {
    return '';
  }
}
