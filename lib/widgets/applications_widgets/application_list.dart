import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/bloc/app_bloc.dart';
import '../../blocs/bloc/app_button_bloc.dart';
import '../../blocs/bloc/app_timer_bloc.dart';
import '../../blocs/bloc/root_bloc.dart';
import '../../blocs/cubits/app_widgets_cubit.dart';
import '../../core/const.dart';
import '../../core/scroll_behavior.dart';
import '../../injection_container.dart';
import '../../models/app.dart';
import '../../repository/user_repository.dart';
import '../../services/app_service.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';
import '../../services/product_service.dart';
import '../../services/timer_service.dart';
import '../appbar/sub_bar.dart';
import '../loading.dart';
import 'application_tile/application_tile.dart';
import 'new_application/create_new_application.dart';

class ApplicationList extends StatelessWidget {
  const ApplicationList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return BlocBuilder<RootBloc, RootState>(
      builder: (context, state) {
        if (state is ReadyRootState) {
          return ScrollConfiguration(
            behavior: MyCustomScrollBehavior(),
            child: ListView.builder(
                itemCount: state.apps.length + 1,
                padding: EdgeInsets.zero,
                scrollDirection: Axis.vertical,
                physics: const PageScrollPhysics(),
                itemBuilder: (context, index) {
                  if (index == 0) {
                    return Container(
                      color: Colors.red,
                      child: SubBar(context,
                          title: 'Applications',
                          isMobile: isMobile,
                          actions: const [
                            CreateNewApplication(),
                          ]),
                    );
                  }
                  return AppView(app: state.apps[index - 1]);
                }),
          );
        }
        return const Loading();
      },
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({Key? key, required this.app}) : super(key: key);

  final AppModel app;

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView>
    with AutomaticKeepAliveClientMixin<AppView> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return MultiBlocProvider(
      providers: [
        BlocProvider<AppWidgetsCubit>(
          create: (context) => AppWidgetsCubit(
              appService: sl<AppService>(),
              dialogsService: sl<DialogsService>(),
              productService: sl<ProductService>())
            ..getAppWidgets(widget.app.id!),
        ),
        BlocProvider<AppTimerBloc>(
          key: Key('appTimerBloc-${widget.app.id}'),
          create: (context) => AppTimerBloc(
            appService: sl<AppService>(),
            productService: sl<ProductService>(),
            timerService: sl.get<TimerService>(instanceName: widget.app.id),
            appModel: widget.app,
          ),
        ),
        BlocProvider<AppButtonBloc>(
          create: (context) => AppButtonBloc(
            appModel: widget.app,
            appTimerBloc: BlocProvider.of<AppTimerBloc>(context),
          ),
        ),
        BlocProvider<AppBloc>(
          create: (context) => AppBloc(
            billingService: sl<BillingService>(),
            userRepository: sl<UserRepository>(),
            appService: sl<AppService>(),
            dialogsService: sl<DialogsService>(),
            appButtonBloc: BlocProvider.of<AppButtonBloc>(context),
            app: widget.app,
          )..add(
              StartUpdatingAppInfo(),
            ),
        ),
      ],
      child: isMobile
          ? MobileApplicationTile(
              key: Key(widget.app.id!),
              app: widget.app,
            )
          : DesktopApplicationTile(
              key: Key(widget.app.id!),
              app: widget.app,
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
