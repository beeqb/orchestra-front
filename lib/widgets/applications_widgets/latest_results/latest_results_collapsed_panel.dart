import 'package:flutter/material.dart';
import 'package:orchestra/widgets/applications_widgets/latest_results/sliding_up_panel.dart';

import '../../../core/const.dart';

class LatestResultsHeader extends StatefulWidget {
  final PanelController panelController;

  const LatestResultsHeader({Key? key, required this.panelController})
      : super(key: key);

  @override
  State<LatestResultsHeader> createState() => _LatestResultsHeaderState();
}

class _LatestResultsHeaderState extends State<LatestResultsHeader>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return SizedBox(
      height: isMobile
          ? ApplicationsPageConstants.mobileBottomPanelHeight
          : ApplicationsPageConstants.desktopBottomPanelHeight,
      child: isMobile
          ? Center(
              child: Text(
                ApplicationsPageConstants.resultsSheetTitle,
                style: Theme.of(context).textTheme.labelSmall,
              ),
            )
          : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ApplicationsPageConstants.resultsSheetTitle,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  TextButton(
                    onPressed: () {
                      if (widget.panelController.isPanelClosed) {
                        widget.panelController.animatePanelToPosition(
                          1,
                        );
                        _controller.animateTo(1);
                      } else {
                        widget.panelController.animatePanelToPosition(
                          0,
                        );
                        _controller.animateTo(0);
                      }
                      setState(() {});
                    },
                    child: RotationTransition(
                      turns: Tween(begin: 0.0, end: 0.5).animate(_controller),
                      child: Icon(
                        Icons.keyboard_arrow_up_rounded,
                        size: 24,
                        color: Theme.of(context).primaryIconTheme.color,
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
