import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/widgets/applications_widgets/latest_results/sliding_up_panel.dart';

import '../../../blocs/bloc/results_bloc.dart';
import '../../../injection_container.dart';
import '../../../models/results_model.dart';
import '../app_result_item.dart';
import 'latest_results_collapsed_panel.dart';

class LatestResults extends StatelessWidget {
  final PanelController panelController;

  const LatestResults({
    required this.panelController,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResultsBloc, ResultsState>(
      builder: (context, state) {
        if (state.results.isEmpty) {
          // return const Center(child: CircularProgressIndicator());
          return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                LatestResultsHeader(
                  panelController: panelController,
                ),
                Text(
                  'No lore worked yet :(',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ]);
        } else {
          return Column(
            children: [
              LatestResultsHeader(
                panelController: panelController,
              ),
              Expanded(
                child: ResultItemList(
                  results: state.results,
                ),
              ),
            ],
          );
        }
      },
    );
  }
}

class ResultItemList extends StatelessWidget {
  final List<ResultsModel> results;

  const ResultItemList({Key? key, required this.results}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: ListView.separated(
        physics: const BouncingScrollPhysics(),
        separatorBuilder: (_, __) => const Divider(
          thickness: 1,
          indent: 20,
          endIndent: 20,
        ),
        shrinkWrap: true,
        itemCount: calculateListItemCount(),
        itemBuilder: (context, index) {
          return index >= results.length
              ? SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                    strokeWidth: 1,
                    color: Theme.of(context).primaryIconTheme.color,
                  ))
              : AppResultItem(
                  data: results[index],
                  context: context,
                  isMobile: false,
                );
        },
      ),
    );
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        notification.metrics.extentAfter == 0) {
      if (sl<ResultsBloc>().state is ResultsLoaded) {
        sl<ResultsBloc>().add(FetchResultsEvent());
      }
    }

    return false;
  }

  int calculateListItemCount() {
    if (sl<ResultsBloc>().state is ResultsLoaded) {
      return sl<ResultsBloc>().state.results.length;
    } else {
      // + 1 for the loading indicator
      return sl<ResultsBloc>().state.results.length + 1;
    }
  }
}
