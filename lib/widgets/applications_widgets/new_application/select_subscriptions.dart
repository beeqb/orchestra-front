import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/widgets/applications_widgets/new_application/subscription_selection_product_tile.dart';
import 'package:orchestra/widgets/loading.dart';

import '../../../blocs/bloc/purchased_bloc.dart';
import '../../../core/const.dart';
import '../../../models/purchase_model.dart';

class DesktopSelectSubscriptionDialog extends StatelessWidget {
  const DesktopSelectSubscriptionDialog({Key? key}) : super(key: key);

  List<PurchaseModel>? getActiveSubscriptions(
      List<PurchaseModel> purchaseModels) {
    final activeSubscriptions =
        purchaseModels.where((element) => element.isActive).toList();
    return activeSubscriptions;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        backgroundColor: Theme.of(context).backgroundColor,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Select subscriptions',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: SizedBox(
          width: ModalDialogConstants.desktopDialogWidth,
          height: 348,
          child: BlocBuilder<PurchasedBloc, PurchasedState>(
              builder: (context, state) {
            if (state is PurchasedLoaded) {
              final activeProducts =
                  getActiveSubscriptions(state.purchasedProducts);
              return activeProducts != null && activeProducts.isNotEmpty
                  ? SingleChildScrollView(
                      child: Column(children: [
                        ...activeProducts
                            .map((e) =>
                                DesktopSubscriptionSelectionProductTile(e))
                            .toList()
                      ]),
                    )
                  : Center(
                      child: Text(
                        'You have no active subscriptions',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    );
            }

            return const Loading();
          }),
        ));
  }
}

class MobileSelectSubscriptionDialog extends StatelessWidget {
  const MobileSelectSubscriptionDialog({Key? key}) : super(key: key);

  List<PurchaseModel>? getActiveSubscriptions(
      List<PurchaseModel> purchaseModels) {
    final activeSubscriptions =
        purchaseModels.where((element) => element.isActive).toList();
    return activeSubscriptions;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(14, 0, 14, 15),
      child:
          BlocBuilder<PurchasedBloc, PurchasedState>(builder: (context, state) {
        if (state is PurchasedLoaded) {
          final activeProducts =
              getActiveSubscriptions(state.purchasedProducts);
          return activeProducts != null && activeProducts.isNotEmpty
              ? Column(children: [
                  SizedBox(
                    height: 50,
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 8,
                        ),
                        Container(
                          width: 32,
                          height: 2,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            color: Theme.of(context).secondaryHeaderColor,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              'Select subscriptions',
                              style: Theme.of(context).textTheme.labelSmall,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          ...activeProducts
                              .map((e) =>
                                  MobileSubscriptionSelectionProductTile(e))
                              .toList()
                        ],
                      ),
                    ),
                  )
                ])
              : Column(
                  children: [
                    SizedBox(
                      height: 50,
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 8,
                          ),
                          Container(
                            width: 32,
                            height: 2,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              color: Theme.of(context).secondaryHeaderColor,
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                ApplicationsPageConstants.resultsSheetTitle,
                                style: Theme.of(context).textTheme.labelSmall,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: Text(
                        'You have no active subscriptions',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ),
                  ],
                );
        }

        return const Loading();
      }),
    );
  }
}
