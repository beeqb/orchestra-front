import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/blocs/bloc/root_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../../models/app.dart';
import '../../../util/constants.dart';

class CreateAppForm extends StatefulWidget {
  const CreateAppForm({Key? key}) : super(key: key);

  @override
  _CreateAppFormState createState() => _CreateAppFormState();
}

class _CreateAppFormState extends State<CreateAppForm> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> formData = {'name': null};
  List<bool>? checkValues;

  TextEditingController? _emailController;
  TextEditingController? _telegramController;

  @override
  void initState() {
    _emailController = TextEditingController();
    _telegramController = TextEditingController();
    checkValues = <bool>[
      ...NotificationType.kNotifications
          .map((key, value) => MapEntry(key, false))
          .values
          .toList()
    ];
    checkValues!.first = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const _paddingHorizontal = 24.0;
    return Padding(
      padding: const EdgeInsets.only(
          left: _paddingHorizontal, right: _paddingHorizontal),
      child: SizedBox(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Enter new application name',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                buildTextFormField(),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  'Send App result to:',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(
                  height: 10,
                ),
                buildCheckboxesList(),
                const SizedBox(
                  height: 24,
                ),
                buttonsPanel(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextFormField buildTextFormField() {
    return TextFormField(
      style: Theme.of(context).textTheme.bodyText2,
      keyboardType: TextInputType.text,
      onSaved: (value) {
        formData['name'] = value;
      },
      validator: (value) {
        if (value == null) {
          return 'Enter new apllication name';
        } else {
          if (value.isEmpty) {
            return 'Enter new apllication name';
          } else {
            return null;
          }
        }
      },
    );
  }

  Wrap buildCheckboxesList() {
    return Wrap(
      spacing: 15.0,
      runSpacing: 15.0,
      children: List.generate(
        checkValues!.length,
        _buildCheckboxListItem,
      ),
    );
  }

  Container _buildCheckboxListItem(int index) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            color: Theme.of(context).iconTheme.color!,
          )),
      child: Column(
        children: [
          CheckboxListTile(
            value: checkValues![index],
            title: Text(
              NotificationType.kNotifications.entries.elementAt(index).value,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            onChanged: (bool? val) {
              setState(() {
                checkValues![index] = val!;
              });
            },
          ),
          NotificationType.kNotifications.entries.elementAt(index).key ==
                      'email' &&
                  checkValues![index]
              ? Padding(
                  padding: const EdgeInsets.only(
                      bottom: 20.0, left: 20.0, right: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      TextField(
                        style: TextStyle(
                            color:
                                Theme.of(context).textTheme.bodyText2!.color),
                        controller: _emailController,
                        decoration: const InputDecoration(
                          labelText: 'Enter email',
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox(height: 1),
          NotificationType.kNotifications.entries.elementAt(index).key ==
                      'telegram' &&
                  checkValues![index]
              ? Padding(
                  padding: const EdgeInsets.only(
                      bottom: 20.0, left: 20.0, right: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      TextButton(
                        onPressed: () =>
                            launch(kTelegramBotLink, forceSafariVC: false),
                        child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Join @OrchestrasBot first, to get app '
                              'results in Telegram!',
                              style: Theme.of(context).textTheme.labelMedium,
                            )),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      TextField(
                        style: Theme.of(context).textTheme.bodyText2,
                        controller: _telegramController,
                        decoration: const InputDecoration(
                            labelText: 'Enter your user ID with @'),
                      ),
                    ],
                  ),
                )
              : const SizedBox(height: 1),
        ],
      ),
    );
  }

  Widget buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        ElevatedButton(
          onPressed: () async {
            await _submitForm();
            //await BlocProvider.of<AppWidgetsCubit>(context).getAppWidgets(appId)
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }

  Future<void> _submitForm() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      var _notif = <String>[];
      for (var i = 0; i < checkValues!.length; i++) {
        if (checkValues![i]) {
          _notif.add(NotificationType.kNotifications.keys.elementAt(i));
        }
      }
      final authState = sl<AuthBloc>().state;
      if (authState is LoggedAuthState) {
        sl<RootBloc>().add(
          StartCreatingAppRootEvent(
            AppModel(
              userId: authState.currentUser.id!,
              name: formData['name'],
              loop: 'none',
              widgets: [],
              notification: _notif,
              status: 'Created',
              icon: '/api/statics/logo.jpg',
              email: _emailController!.text,
              telegram: _telegramController!.text,
            ),
          ),
        );
        _emailController!.clear();
        _telegramController!.clear();
        Navigator.pop(context);
      }
    }
  }
}
