import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/enums.dart';

import '../../../blocs/bloc/auth_bloc.dart';
import '../../../blocs/bloc/navigation_bloc.dart';
import '../../../blocs/bloc/root_bloc.dart';
import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../../models/app.dart';
import '../../../models/product/product.dart';
import '../../../models/purchase_model.dart';
import '../../../view_models/product_view_model.dart';
import '../../marketplace_widgets/star_rating.dart';
import '../../product_image_widget.dart';
import '../app_creation_settings_form.dart';

class DesktopSubscriptionSelectionProductTile extends StatelessWidget {
  final PurchaseModel purchaseModel;

  const DesktopSubscriptionSelectionProductTile(this.purchaseModel, {Key? key})
      : super(key: key);

  String subscriptionTime(PurchaseModel purchaseModel) {
    if (purchaseModel.product.pricingAndBusiness.price == 0) {
      return 'Subscribed for free';
    }
    return purchaseModel.subscriptionTimeStatus;
  }

  Future<void> _createApp(Product product, BuildContext context) async {
    final authState = sl<AuthBloc>().state;
    if (authState is LoggedAuthState) {
      sl<RootBloc>().add(
        StartCreatingAppFromWidgetEvent(
          AppModel(
            userId: authState.currentUser.id!,
            name: product.productName,
            failure: product.failure,
            loop: 'none',
            widgets: [],
            notification: ['modal'],
            status: 'Created',
            icon: product.productLogoURL,
          ),
          product,
        ),
      );
      showDialog(
          context: context,
          builder: (context) {
            return DesktopAppCreationSettingsForm(
              product: product,
            );
          });
    } else {
      sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            color: Theme.of(context).secondaryHeaderColor),
        child: TextButton(
          onPressed: () {
            Navigator.pop(context);
            _createApp(purchaseModel.product, context);
          },
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Row(children: [
              ProductImageWidget(
                imageUrl: purchaseModel.product.productLogoURL,
                width: 90,
                height: 90,
              ),
              const SizedBox(
                width: 15,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    purchaseModel.product.productName,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    purchaseModel.product.productCategory.title,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: Theme.of(context).disabledColor),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    subscriptionTime(purchaseModel),
                    style: Theme.of(context).textTheme.labelMedium,
                  )
                ],
              ),
            ]),
          ),
        ),
      ),
    );
  }
}

class MobileSubscriptionSelectionProductTile extends StatelessWidget {
  final PurchaseModel purchaseModel;

  const MobileSubscriptionSelectionProductTile(this.purchaseModel, {Key? key})
      : super(key: key);

  void _showMobileCreateForm(BuildContext context) {
    final authState = sl<AuthBloc>().state;
    if (authState is LoggedAuthState) {
      sl<RootBloc>().add(
        StartCreatingAppFromWidgetEvent(
          AppModel(
            userId: authState.currentUser.id!,
            name: purchaseModel.product.productName,
            failure: purchaseModel.product.failure,
            loop: 'none',
            widgets: [],
            notification: ['modal'],
            status: 'Created',
            icon: purchaseModel.product.productLogoURL,
          ),
          purchaseModel.product,
        ),
      );
      showModalBottomSheet(
        useRootNavigator: true,
        backgroundColor: Theme.of(context).backgroundColor,
        shape: ModalDialogConstants.mobileBorder,
        context: context,
        builder: (context) =>
            MobileAppCreationSettingsForm(product: purchaseModel.product),
      );
    } else {
      sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TextButton(
        onPressed: () => _showMobileCreateForm(context),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
            ProductImageWidget(
              imageUrl: purchaseModel.product.productLogoURL,
              width: 60,
              height: 60,
            ),
            const SizedBox(
              width: 12,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  purchaseModel.product.productName,
                  style: Theme.of(context).textTheme.labelSmall,
                ),
                const SizedBox(height: 4),
                Text(
                  purchaseModel.product.productCategory.title,
                  style: Theme.of(context)
                      .textTheme
                      .bodySmall
                      ?.copyWith(color: Theme.of(context).disabledColor),
                ),
                const SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    StarRating(
                      rating: ProductViewModel.rankAsDouble(
                          purchaseModel.product.rank),
                      iconSize: 12,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      purchaseModel.product.rank == null
                          ? '0.0'
                          : purchaseModel.product.rank.toString(),
                      style: purchaseModel.product.rank == null
                          ? Theme.of(context)
                              .textTheme
                              .bodySmall
                              ?.copyWith(color: Theme.of(context).disabledColor)
                          : Theme.of(context).textTheme.bodySmall,
                    ),
                  ],
                ),
              ],
            ),
            Expanded(
              child: Align(
                alignment: Alignment.topRight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Theme.of(context).cardColor),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 6,
                        vertical: 4,
                      ),
                      child: Text(
                        ProductViewModel.priceToStr(purchaseModel.product),
                        style: Theme.of(context).textTheme.bodySmall,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
      const SizedBox(
        height: 16,
      ),
    ]);
  }
}
