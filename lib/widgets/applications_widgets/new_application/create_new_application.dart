import 'package:flutter/material.dart';
import 'package:orchestra/widgets/applications_widgets/new_application/select_subscriptions.dart';

import '../../../core/const.dart';

class CreateNewApplication extends StatelessWidget {
  const CreateNewApplication({Key? key}) : super(key: key);

  final textDialog =
      'To create an application, you need to select a Lore. Now you will be redirected to the marketplace to select a Lore.';

  void _desktopSelectSubscriptionPopup(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) =>
            const DesktopSelectSubscriptionDialog());
  }

  void _mobileSelectSubscriptionPopup(BuildContext context) {
    showModalBottomSheet(
      useRootNavigator: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: ModalDialogConstants.mobileBorder,
      context: context,
      builder: (context) => const MobileSelectSubscriptionDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Padding(
      padding: isMobile
          ? const EdgeInsets.symmetric(
              vertical: 2,
            )
          : const EdgeInsets.symmetric(
              vertical: 32,
            ),
      child: TextButton(
        onPressed: () {
          isMobile
              ? _mobileSelectSubscriptionPopup(context)
              : _desktopSelectSubscriptionPopup(context);
        },
        child: isMobile
            ? Icon(
                Icons.add,
                size: 24,
                color: Theme.of(context).primaryIconTheme.color,
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.add,
                    size: 24,
                    color: Theme.of(context).primaryIconTheme.color,
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    'Create new application',
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                ],
              ),
        style: isMobile
            ? TextButton.styleFrom(
                fixedSize: const Size(20, 20),
                backgroundColor: Theme.of(context).cardColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12), // <-- Radius
                ),
              )
            : Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).cardColor),
                fixedSize: MaterialStateProperty.all<Size>(
                  isMobile ? const Size(20, 20) : const Size(256, 48),
                )),
      ),
    );
  }
}
