import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/widgets/marketplace_widgets/top_products/top_product_card.dart';

import '../../../blocs/bloc/top_ten_products_bloc.dart';
import '../../../core/const.dart';
import '../../../core/scroll_behavior.dart';
import '../../../injection_container.dart';
import '../../loading.dart';

class TopProducts extends StatelessWidget {
  const TopProducts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TopTenProductsBLoC, TopTenProductsState>(
      builder: (context, state) {
        return state.when(
          initial: () => const Center(child: Loading()),
          loading: () => const Center(child: Loading()),
          loaded: (state) => TopProductsWidget(
            products: state,
          ),
          failure: () => const Center(child: Text('Something went wrong...')),
        );
      },
    );
  }
}

class TopProductsWidget extends StatelessWidget {
  final List<Product?>? products;

  const TopProductsWidget({
    Key? key,
    this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return SizedBox(
      height: isMobile
          ? TopProductCardConstants.mobileCardHeight
          : TopProductCardConstants.desktopCardHeight,
      child: ScrollConfiguration(
        behavior: MyCustomScrollBehavior(),
        child: ListView.separated(
          separatorBuilder: (_, __) => const SizedBox(width: 15),
          itemCount: products!.length,
          scrollDirection: Axis.horizontal,
          physics: const BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () => sl<NavigationBLoC>()
                  .add(ToProductDetailsNavigationEvent(products![index]!.id)),
              child: isMobile
                  ? MobileTopProductCard(
                      product: products![index]!,
                    )
                  : DesktopTopProductCard(
                      product: products![index]!,
                    ),
            );
          },
        ),
      ),
    );
  }
}
