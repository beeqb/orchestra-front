import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/view_models/product_view_model.dart';
import 'package:orchestra/widgets/marketplace_widgets/star_rating.dart';

import '../../../core/const.dart';
import '../../../injection_container.dart';
import '../../product_image_widget.dart';

class DesktopTopProductCard extends StatelessWidget {
  final Product product;

  const DesktopTopProductCard({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(product.id)),
      child: Container(
        width: TopProductCardConstants.desktopCardWidth,
        height: TopProductCardConstants.desktopCardHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          children: [
            ProductImageWidget(
              imageUrl: product.productPromoPictureURL,
              // fit: BoxFit.fill,
              width: TopProductCardConstants.desktopCardWidth,
              height: TopProductCardConstants.desktopPromoHeight,
            ),
            const SizedBox(
              height: 12,
            ),
            DesktopProductTile(product: product)
          ],
        ),
      ),
    );
  }
}

class DesktopProductTile extends StatelessWidget {
  const DesktopProductTile({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Theme.of(context).backgroundColor),
      child: TextButton(
        onPressed: () => sl<NavigationBLoC>()
            .add(ToProductDetailsNavigationEvent(product.id)),
        child: Row(children: [
          const SizedBox(
            width: 16,
          ),
          ProductImageWidget(
            imageUrl: product.productLogoURL,
            width: 90,
            height: 90,
          ),
          const SizedBox(
            width: 16,
          ),
          Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  product.productName,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.labelMedium,
                ),
                const SizedBox(height: 2),
                Text(
                  product.productCategory.title,
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Theme.of(context).disabledColor),
                ),
                const SizedBox(height: 4),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    StarRating(
                      rating: ProductViewModel.rankAsDouble(product.rank),
                      iconSize: 12,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      ProductViewModel.rankStringDecimal(product.rank),
                      style: product.rank == null
                          ? Theme.of(context)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(color: Theme.of(context).disabledColor)
                          : Theme.of(context).textTheme.labelSmall,
                    ),
                  ],
                ),
                const SizedBox(height: 4),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Theme.of(context).cardColor),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 4,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        ProductViewModel.priceToStr(product),
                        style: Theme.of(context).textTheme.labelSmall,
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}

class MobileProductTile extends StatelessWidget {
  const MobileProductTile({Key? key, required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TextButton(
        onPressed: () => sl<NavigationBLoC>()
            .add(ToProductDetailsNavigationEvent(product.id)),
        child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: ProductImageWidget(
              imageUrl: product.productLogoURL,
              width: ProductCardSizeConstants.mobileCardHeight,
              height: ProductCardSizeConstants.mobileCardHeight,
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                product.productName,
                style: Theme.of(context).textTheme.labelSmall,
              ),
              const SizedBox(height: 4),
              Text(
                product.productCategory.title,
                style: Theme.of(context)
                    .textTheme
                    .bodySmall
                    ?.copyWith(color: Theme.of(context).disabledColor),
              ),
              const SizedBox(height: 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  StarRating(
                    rating: ProductViewModel.rankAsDouble(product.rank),
                    iconSize: 12,
                  ),
                  const SizedBox(width: 4),
                  Text(
                    product.rank == null ? '0.0' : product.rank.toString(),
                    style: product.rank == null
                        ? Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: Theme.of(context).disabledColor)
                        : Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
            ],
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).cardColor),
                padding: const EdgeInsets.symmetric(
                  horizontal: 6,
                  vertical: 4,
                ),
                child: Text(
                  ProductViewModel.priceToStr(product),
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ),
            ),
          ),
        ]),
      ),
      const SizedBox(
        height: 16,
      ),
    ]);
  }
}

class MobileTopProductCard extends StatelessWidget {
  final Product product;

  const MobileTopProductCard({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(product.id)),
      child: SizedBox(
        width: TopProductCardConstants.mobileCardWidth,
        height: TopProductCardConstants.mobileCardHeight,
        child: Column(
          children: [
            ProductImageWidget(
              imageUrl: product.productPromoPictureURL,
              width: TopProductCardConstants.mobileCardWidth,
              height: TopProductCardConstants.mobilePromoHeight,
            ),
            const SizedBox(
              height: 7,
            ),
            _MobileTopProductTile(product: product)
          ],
        ),
      ),
    );
  }
}

class _MobileTopProductTile extends StatelessWidget {
  final Product product;

  const _MobileTopProductTile({Key? key, required this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () =>
          sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(product.id)),
      child: Row(children: [
        ProductImageWidget(
          imageUrl: product.productLogoURL,
          width: ProductCardSizeConstants.mobileCardHeight,
          height: ProductCardSizeConstants.mobileCardHeight,
        ),
        const SizedBox(
          width: 12,
        ),
        SizedBox(
          height: ProductCardSizeConstants.mobileCardHeight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                product.productName,
                style: Theme.of(context).textTheme.labelSmall,
              ),
              //const SizedBox(height: 4),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  StarRating(
                    rating: ProductViewModel.rankAsDouble(product.rank),
                    iconSize: 12,
                  ),
                  const SizedBox(width: 4),
                  Text(
                    ProductViewModel.priceToStr(product),
                    style: product.rank == null
                        ? Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: Theme.of(context).disabledColor)
                        : Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
              // const SizedBox(
              //   height: 4,
              // ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).cardColor),
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 4,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      ProductViewModel.priceToStr(product),
                      style: Theme.of(context).textTheme.bodySmall,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
