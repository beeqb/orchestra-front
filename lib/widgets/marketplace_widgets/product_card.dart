import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/core/const.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/view_models/product_view_model.dart';

import '../../core/app_colors.dart';
import '../../injection_container.dart';
import '../../models/enums/enums.dart';
import '../../util/text_util.dart';
import '../product_image_widget.dart';
import 'star_rating.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key? key,
    required this.product,
    required this.maxScreenWidth,
  }) : super(key: key);

  final Product product;
  final double maxScreenWidth;

  @override
  Widget build(BuildContext context) {
    final _price = formatCurrency.format(product.pricingAndBusiness.price);
    final _priceToStr = product.pricingAndBusiness.model ==
            BusinessModelType.monthlySubscription
        ? '$_price/mo'
        : product.pricingAndBusiness.model == BusinessModelType.perRequest
            ? '$_price/req'
            : _price;

    Widget containerLogo = ProductImageWidget(
      imageUrl: product.productLogoURL,
      width: 50,
      height: 59,
    );

    Widget priceButton = SizedBox(
      height: 25,
      child: Theme(
        data: Theme.of(context).copyWith(
          buttonTheme: ButtonTheme.of(context).copyWith(minWidth: 50),
        ),
        child: OutlinedButton(
          style: OutlinedButton.styleFrom(
            primary: Colors.grey[300],
            backgroundColor: Colors.grey[300],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
          ),
          onPressed: buildPushNamed,
          child: Text(
            product.pricingAndBusiness.price != 0 ? _priceToStr : 'Free',
            style: Theme.of(context)
                .textTheme
                .bodyText1!
                .copyWith(fontSize: 12, color: AppColors.blueColor),
          ),
        ),
      ),
    );
    var isFailure = product.failure ?? false;

    Widget expandedBody = Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            product.productName,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style:
                Theme.of(context).textTheme.headline6!.copyWith(fontSize: 16),
          ),
          StarRating(
            rating: ProductViewModel.rankAsDouble(product.rank),
            iconSize: 12,
          ),
        ],
      ),
    );
    Widget columnDescription = Column(
      mainAxisSize: maxScreenWidth < 600 ? MainAxisSize.max : MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            expandedBody,
            if (isFailure)
              IconButton(
                onPressed: () {
                  //_showDialog(context);
                },
                tooltip: failureWidgetMessage,
                icon: const Icon(
                  Icons.circle,
                  color: Colors.red,
                  size: 15,
                ),
              ),
            priceButton,
          ],
        ),
        const SizedBox(height: 3),
      ],
    );
    Widget expandedDescriptoon = Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 12.0),
        child: Container(
          child: columnDescription,
        ),
      ),
    );
    Widget mainCardRow = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        containerLogo,
        expandedDescriptoon,
      ],
    );
    return GestureDetector(
      onTap: buildPushNamed,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Container(child: mainCardRow),
      ),
    );
  }

  void buildPushNamed() =>
      sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(product.id));
}

// описание сокращенно, убрали из карточки продукта
/* Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                  product.productDescription,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                  .textTheme
                  .subtitle2
                  .copyWith(
                  color: Colors.grey,
                  fontSize: 12,
                   ),
                 ),
               ), */
