import 'package:flutter/material.dart';

typedef RatingChangeCallback = void Function(double rating);

class StarRating extends StatelessWidget {
  final int starCount;
  final double? rating;
  final RatingChangeCallback? onRatingChanged;
  final Color? color;
  final double? iconSize;
  final double padding;

  const StarRating(
      {Key? key,
      this.starCount = 5,
      this.rating = .0,
      this.onRatingChanged,
      this.color,
      this.iconSize,
      this.padding = 0})
      : super(key: key);

  Widget buildStar(BuildContext context, int index) {
    final Icon icon;
    if (rating != null) {
      if (index >= rating!) {
        icon = Icon(
          Icons.star_rounded,
          color: Theme.of(context).disabledColor,
          size: iconSize,
        );
      } else if (index > rating!.toInt() - 1 && index < rating!) {
        icon = Icon(
          Icons.star_half_rounded,
          color: color ?? const Color(0xFFFFAF02),
          size: iconSize,
        );
      } else {
        icon = Icon(
          Icons.star_rounded,
          color: color ?? const Color(0xFFFFAF02),
          size: iconSize,
        );
      }
    } else {
      icon = icon = Icon(
        Icons.star_rounded,
        color: Theme.of(context).disabledColor,
        size: iconSize,
      );
    }
    return Padding(
      padding: EdgeInsets.only(right: padding),
      child: InkResponse(
          customBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(iconSize ?? 10),
          ),
          onTap: onRatingChanged == null
              ? null
              : () => onRatingChanged!(index + 1.0),
          child: icon),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children:
            List.generate(starCount, (index) => buildStar(context, index)));
  }
}
