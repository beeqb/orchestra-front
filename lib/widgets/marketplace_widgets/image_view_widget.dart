import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../config.dart';

class ImageView extends StatelessWidget {
  final String imageUrl;
  final String tag;
  const ImageView({Key? key, required this.imageUrl, required this.tag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Hero(
          tag: tag,
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
            child: CachedNetworkImage(
              width: MediaQuery.of(context).size.width * 0.9,
              imageUrl: '$kWidgetAvatarUrl$imageUrl',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}
