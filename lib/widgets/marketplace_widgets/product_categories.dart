import 'package:flutter/material.dart';
import 'package:orchestra/widgets/marketplace_widgets/top_products/top_product_card.dart';

import '../../core/const.dart';
import '../../core/scroll_behavior.dart';
import '../../models/app_category.dart';
import '../../models/enums/enums.dart';
import '../../models/product/product.dart';
import '../category_label.dart';

class ProductCategories extends StatefulWidget {
  const ProductCategories({
    Key? key,
    required this.categories,
  }) : super(key: key);

  final List<AppCategory> categories;

  @override
  State<ProductCategories> createState() => _ProductCategoriesState();
}

class _ProductCategoriesState extends State<ProductCategories> {
  ProductCategoryType? selectedCategory;
  List<Product?> products = [];
  late List<AppCategory> nonEmptyCategories;

  void onCategoryPressed(ProductCategoryType? appCategory) {
    selectedCategory = appCategory;
    if (appCategory != null) {
      products = nonEmptyCategories
          .where((appCategory) =>
              appCategory.productCategoryType == selectedCategory)
          .first
          .prods;
    } else {
      products = _allProducts;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    nonEmptyCategories =
        widget.categories.where((element) => element.prods.isNotEmpty).toList();
    products = _allProducts;
  }

  List<Product?> get _allProducts => nonEmptyCategories
      .map((e) => e.prods)
      .reduce((value, element) => [...value, ...element]);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(
        height: isMobile ? 32 : 36,
        child: ScrollConfiguration(
          behavior: MyCustomScrollBehavior(),
          child: ListView.separated(
            separatorBuilder: (_, __) => const SizedBox(width: 15),
            itemCount: nonEmptyCategories.length + 1,
            scrollDirection: Axis.horizontal,
            physics: const BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              if (index == 0) {
                return CategoryLabel(
                    onCategoryPressed: onCategoryPressed,
                    isSelected: selectedCategory == null);
              }
              return nonEmptyCategories[index - 1].prods.isNotEmpty
                  ? CategoryLabel(
                      appCategory:
                          nonEmptyCategories[index - 1].productCategoryType,
                      onCategoryPressed: onCategoryPressed,
                      isSelected: selectedCategory ==
                          nonEmptyCategories[index - 1].productCategoryType)
                  : const SizedBox.shrink();
            },
          ),
        ),
      ),
      SizedBox(
        height: isMobile ? 16 : 24,
      ),
      _ProductList(products: products),
    ]);
  }
}

class _ProductList extends StatelessWidget {
  const _ProductList({Key? key, required this.products}) : super(key: key);

  final List<Product?> products;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return isMobile
        ? ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: products.length,
            separatorBuilder: (_, __) => const SizedBox(width: 45),
            itemBuilder: (context, index) =>
                MobileProductTile(product: products[index]!))
        : LayoutBuilder(builder: (context, constraints) {
            return SizedBox(
              child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: products.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    mainAxisExtent: isMobile
                        ? ProductCardSizeConstants.mobileCardHeight
                        : ProductCardSizeConstants.desktopCardHeight),
                itemBuilder: (BuildContext context, int index) {
                  return DesktopProductTile(
                    product: products[index]!,
                  );
                },
              ),
            );
          });
  }
}
