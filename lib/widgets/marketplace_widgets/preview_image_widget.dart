import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../config.dart';
import '../colors.dart';
import 'image_view_widget.dart';

class PreviewImageWidget extends StatelessWidget {
  final String imageUrl;
  final int index;
  const PreviewImageWidget(
      {Key? key, required this.imageUrl, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: GestureDetector(
          onTap: () => _navigate(context),
          child: Hero(
            tag: '100$index',
            child: CachedNetworkImage(
              imageUrl: '$kWidgetAvatarUrl$imageUrl',
              fit: BoxFit.cover,
              errorWidget: (context, url, error) => Container(
                color: getRandomColor(),
                child: Text(error),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _navigate(BuildContext context) {
    Navigator.of(context).push(
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 1000),
        pageBuilder: (context, animation, secondaryAnimation) {
          return ImageView(
            imageUrl: imageUrl,
            tag: '100$index',
          );
        },
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return Align(
            child: FadeTransition(
              opacity: animation,
              child: child,
            ),
          );
        },
      ),
    );
  }
}
