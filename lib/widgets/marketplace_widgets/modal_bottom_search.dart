import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../blocs/bloc/search_product_bloc.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../loading.dart';
import 'product_card.dart';

class ModalBottomSearch extends StatelessWidget {
  const ModalBottomSearch({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width <
                ApplicationsPageConstants.maxWidth
            ? 0
            : (MediaQuery.of(context).size.width -
                    ApplicationsPageConstants.maxWidth) /
                2,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).dialogBackgroundColor,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
          topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
        ),
      ),
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: ConstrainedBox(
        constraints:
            BoxConstraints(maxHeight: MediaQuery.of(context).size.height / 1.7),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  children: [
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsetsDirectional.only(
                          start: 20.0, end: 20.0),
                      child: TextFormField(
                        onChanged: (value) => sl<SearchProductBloc>()
                            .add(FetchSearchedProductsEvent(query: value)),
                        decoration: const InputDecoration(
                            prefixIcon: Padding(
                          padding: EdgeInsetsDirectional.only(start: 10.0),
                          child: Icon(
                            MdiIcons.magnify,
                            color: Colors.grey,
                          ),
                        )),
                      ),
                    )),
                  ],
                ),
              ),
            ),
            Expanded(
              child: BlocBuilder<SearchProductBloc, SearchProductState>(
                builder: (context, state) {
                  if (state is SearchProductLoaded) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: ListView(
                        shrinkWrap: true,
                        children: List.generate(
                          state.foundProducts.length,
                          (index) => ProductCard(
                              product: state.foundProducts[index],
                              maxScreenWidth:
                                  MediaQuery.of(context).size.width),
                        ),
                      ),
                    );
                  }
                  return const Loading();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
