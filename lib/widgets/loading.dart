import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../core/const.dart';

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Center(
        child: Lottie.asset(
      'assets/images/Loader.json',
      height: isMobile ? 100 : 200,
      width: isMobile ? 100 : 200,
    ));
  }
}

class LoadingInitial extends StatelessWidget {
  const LoadingInitial({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Lottie.asset(
      'assets/images/Loader.json',
      height: 150,
      width: 150,
    ));
  }
}
