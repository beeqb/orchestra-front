import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/photo_type.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/util/text_util.dart';
import 'package:orchestra/widgets/loading.dart';

class ShowImageMobile extends StatelessWidget {
  //final String imageUrl;
  final PhotoUrlType fieldName;
  final Product? editProduct;
  const ShowImageMobile({
    Key? key,
    required this.fieldName,
    required this.editProduct,
  }) : super(key: key);

  Widget _imageWidget(ImageProvider imageProvider) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: imageProvider,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _imageUrl = getImageUrl(
      editProduct: editProduct,
      fieldName: fieldName,
    );
    if (fieldName == PhotoUrlType.launcherFileUrl) {
      return Center(
        child: Text(_imageUrl),
      );
    } else {
      return CachedNetworkImage(
        imageUrl: _imageUrl,
        imageBuilder: (context, imageProvider) {
          return _imageWidget(imageProvider);
        },
        placeholder: (context, url) {
          return const Loading();
        },
        errorWidget: (context, url, error) {
          return _imageWidget(
            const AssetImage('assets/images/imagepl.png'),
          );
        },
      );
    }
  }
}
