import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dropzone/flutter_dropzone.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../blocs/bloc/upload_image_bloc.dart';
import '../../core/app_colors.dart';
import '../../models/enums/photo_type.dart';
import '../colors.dart';
import '../loading.dart';

class AddImageWidget extends StatefulWidget {
  final PhotoUrlType fieldName;
  final List<String> fileTypes;

  const AddImageWidget({
    Key? key,
    required this.fieldName,
    required this.fileTypes,
  }) : super(key: key);

  @override
  State<AddImageWidget> createState() => _AddImageWidgetState();
}

class _AddImageWidgetState extends State<AddImageWidget> {
  late DropzoneViewController controller1;

  String message1 = 'Drop something here';
  String message2 = 'Drop something here';
  bool highlighted1 = false;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UploadImageBloc, UploadImageState>(
      builder: (context, state) {
        if (state is ProcessingState) {
          return Stack(
            children: const [
              Loading(),
            ],
          );
        }
        if (state is EmptyImageState) {
          return _emptyImageContainer(context);
        }
        if (state is EmptyLauncherFileState) {
          return _emptyLauncherFileContainer(context);
        }
        if (state is ImageDeletedState) {
          return _emptyImageContainer(context);
        }
        if (state is UploadedImageState) {
          return _showCurrentImageOrFile(
            context,
            state.imageUrl,
            true,
          );
        }
        if (state is UploadedLauncherFileState) {
          return _showCurrentImageOrFile(
            context,
            state.launcherFileName,
            false,
          );
        }
        if (state is AddedImageState) {
          return _showAddedImageOrFile(
            context,
            state.imageFile.bytes!,
            true,
          );
        }
        if (state is UploadImageFailure) {
          return const Center(
            child: Text('UploadImageFailure'),
          );
        }
        return const Center(child: Text('unknown state'));
      },
    );
  }

  Widget dropZone(BuildContext context) => Builder(
        builder: (context) => DropzoneView(
          operation: DragOperation.copy,
          cursor: CursorType.grab,
          onCreated: (ctrl) => controller1 = ctrl,
          onLoaded: () => print('Zone 1 loaded'),
          onError: (ev) => print('Zone 1 error: $ev'),
          onHover: () {
            setState(() => highlighted1 = true);
            print('Zone 1 hovered');
          },
          onLeave: () {
            setState(() => highlighted1 = false);
            print('Zone 1 left');
          },
          onDrop: (ev) {
            BlocProvider.of<UploadImageBloc>(context).add(UploadImage());
          },
          onDropMultiple: (ev) async {
            print('Zone 1 drop multiple: $ev');
          },
        ),
      );

  Widget _emptyImageContainer(BuildContext context) {
    return GestureDetector(
        onTap: () {
          BlocProvider.of<UploadImageBloc>(context).add(UploadImage());
        },
        child: Stack(
          children: [
            //   kIsWeb ? dropZone(context) : const SizedBox.shrink(),
            Center(
              child: Column(
                //mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/icons/add_image.svg',
                    width: 48,
                    height: 48,
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    'Drop PNG or JPEG files here to upload',
                    style: Theme.of(context).textTheme.headlineMedium,
                  )
                ],
              ),
            ),
          ],
        ));
  }

  Widget _emptyLauncherFileContainer(BuildContext context) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<UploadImageBloc>(context).add(UploadImage());
      },
      child: Stack(
        children: [
          // kIsWeb ? dropZone(context) : const SizedBox.shrink(),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/icons/zip.svg',
                  width: 48,
                  height: 48,
                ),
                const SizedBox(
                  height: 25,
                ),
                Text(
                  'Drop a ZIP here to upload',
                  style: Theme.of(context).textTheme.headlineMedium,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _showCurrentImageOrFile(
    BuildContext context,
    String imageUrl,
    bool isImage,
  ) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isImage
                ? CachedNetworkImage(
                    imageUrl: imageUrl,
                    placeholder: (context, url) => const Loading(),
                    errorWidget: (context, url, error) => Container(
                      color: getRandomColor(),
                      child: SizedBox(
                        width: 170,
                        child: Text(
                          '$error',
                        ),
                      ),
                      // TODO добавить ограничения по размеру загружаемого файла
                    ),
                  )
                : Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        MdiIcons.zipBox,
                        color: AppColors.blueColor,
                      ),
                      Text(imageUrl),
                    ],
                  ),
            deleteButton(context),
          ],
        ),
      ),
    );
  }

  Widget _showAddedImageOrFile(
    BuildContext context,
    Uint8List imageFile,
    bool isImage,
  ) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isImage
                ? Image.memory(imageFile)
                : Image.asset('assets/icons/icon-152x152.png'),
            deleteButton(context)
          ],
        ),
      ),
    );
  }

  IconButton deleteButton(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset(
        'assets/icons/delete.svg',
        color: Theme.of(context).iconTheme.color,
        width: 17,
        height: 18,
      ),
      onPressed: () {
        BlocProvider.of<UploadImageBloc>(context)
            .add(DeleteImage(fieldName: widget.fieldName));
      },
    );
  }
}
