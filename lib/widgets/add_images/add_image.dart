import 'package:flutter/material.dart';

import '../../models/enums/photo_type.dart';
import 'add_image_stub.dart'
    if (dart.library.io) 'package:orchestra/widgets/add_images/add_image_mobile.dart'
    if (dart.library.html) 'package:orchestra/widgets/add_images/add_image_web.dart'
    as impl;

class AddImage extends StatelessWidget {
  final PhotoUrlType fieldName;
  final List<String> fileTypes;
  // final Product editProduct;
  final Function(
          PhotoUrlType urlType, String uploadedFileName, BuildContext context)
      setUploadedName;

  const AddImage({
    Key? key,
    required this.fieldName,
    required this.fileTypes,
    required this.setUploadedName,
    // required this.editProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return impl.AddImageWidget(
      fieldName: fieldName,
      fileTypes: fileTypes,
      setUploadedName: setUploadedName,
      // editProduct: editProduct,
    );
  }
}
