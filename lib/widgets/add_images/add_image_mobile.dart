import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../models/enums/photo_type.dart';

///! неиспользуемый функционал (решено добавление виджетов только в веб)

class AddImageWidget extends StatefulWidget {
  final PhotoUrlType fieldName;
  final List<String> fileTypes;
  //final Product editProduct;

  final Function(
    PhotoUrlType urlType,
    String uploadedFileName,
    BuildContext context,
  ) setUploadedName;

  const AddImageWidget({
    Key? key,
    required this.fieldName,
    required this.fileTypes,
    required this.setUploadedName,
    //required this.editProduct,
  }) : super(key: key);

  @override
  _AddImageWidgetState createState() => _AddImageWidgetState();
}

class _AddImageWidgetState extends State<AddImageWidget> {
  File? _image;

  Future getImage() async {}

  @override
  Widget build(BuildContext context) {
    return addImageButton();
  }

  Widget addImageButton() {
    return GestureDetector(
      onTap: () async {
        await getImage();
      },
      child: Stack(
        children: [
          Center(
            child: _image != null
                ? SizedBox(
                    height: 200,
                    width: 200,
                    child: Image.file(_image!),
                  )
                : SvgPicture.asset(
                    'assets/icons/add_image.svg',
                    width: 35,
                    height: 35,
                  ),
          ),
          _image != null
              ? Positioned(bottom: 10, right: 10, child: deleteButton(context))
              : const SizedBox.shrink()
        ],
      ),
    );
  }

  IconButton deleteButton(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset(
        'assets/icons/delete.svg',
        width: 17,
        height: 18,
      ),
      onPressed: () {
        setState(() {
          _image = null;
        });
      },
    );
  }
}
