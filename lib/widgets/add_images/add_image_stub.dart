import 'package:flutter/material.dart';

import '../../models/enums/photo_type.dart';

class AddImageWidget extends StatelessWidget {
  final PhotoUrlType fieldName;
  final List<String> fileTypes;
  // final Product editProduct;
  final Function(
          PhotoUrlType urlType, String uploadedFileName, BuildContext context)
      setUploadedName;

  const AddImageWidget({
    Key? key,
    required this.fieldName,
    required this.fileTypes,
    required this.setUploadedName,
    //required this.editProduct,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: const Center(
        child: Text('Its neither mobile nor web'),
      ),
    );
  }
}
