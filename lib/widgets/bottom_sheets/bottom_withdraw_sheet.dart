import 'package:flutter/material.dart';

import '../../core/const.dart';
import 'bottom_deposit_sheet.dart';

class WithdrawPopup extends StatefulWidget {
  const WithdrawPopup({Key? key}) : super(key: key);

  @override
  State<WithdrawPopup> createState() => _WithdrawPopupState();
}

class _WithdrawPopupState extends State<WithdrawPopup> {
  final GlobalKey<FormState> _mobileFormKey = GlobalKey<FormState>();
  double _transferAmount = 0.0;

  bool cryptoCategorySelected = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 24),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        backgroundColor: Theme.of(context).backgroundColor,
        titlePadding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Transfer funds',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: SizedBox(
          width: ModalDialogConstants.desktopDialogWidth,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MoneyCategoryLabel(
                    moneyCategory: 'Crypto',
                    onPressed: () {
                      setState(() {
                        cryptoCategorySelected = true;
                      });
                    },
                    isSelected: cryptoCategorySelected,
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  MoneyCategoryLabel(
                      moneyCategory: 'Card',
                      isSelected: !cryptoCategorySelected,
                      onPressed: () {
                        setState(() {
                          cryptoCategorySelected = false;
                        });
                      }),
                ],
              ),
              const SizedBox(
                height: 42,
              ),
              Text(
                "Amount",
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ),
              const SizedBox(
                height: 10,
              ),
              Form(
                key: _mobileFormKey,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: '0',
                      hintStyle: Theme.of(context)
                          .textTheme
                          .headlineMedium
                          ?.copyWith(color: Theme.of(context).iconTheme.color),
                      contentPadding: const EdgeInsets.only(left: 25),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          'USD',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      )),
                  onSaved: (newValue) {
                    _transferAmount = double.parse(newValue ?? '0.0');
                  },
                  validator: (val) {
                    if (val!.isEmpty) {
                      return 'Amount is required';
                    }
                  },
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              _buttonsPanel(),
              const SizedBox(
                height: 32,
              ),
            ],
          ),
        ));
  }

  Widget _buttonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Withdraw',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
          style: Theme.of(context).textButtonTheme.style?.copyWith(
                backgroundColor: MaterialStateProperty.all<Color?>(
                    Theme.of(context).iconTheme.color),
              ),
        ),
      ],
    );
  }
}

class BottomWithdrawSheet extends StatefulWidget {
  const BottomWithdrawSheet({Key? key}) : super(key: key);
  @override
  _BottomWithdrawSheetState createState() => _BottomWithdrawSheetState();
}

class _BottomWithdrawSheetState extends State<BottomWithdrawSheet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final Widget _padding = const SizedBox(
    height: 18,
    width: double.infinity,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width <
                  ApplicationsPageConstants.maxWidth
              ? 0
              : (MediaQuery.of(context).size.width -
                      ApplicationsPageConstants.maxWidth) /
                  2),
      decoration: BoxDecoration(
        color: Theme.of(context).bottomAppBarColor,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
          topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
        ),
      ),
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).viewInsets.bottom,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    //  textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      labelText: 'Enter amount in USD',
                    ),
                    onSaved: (newValue) {},
                    validator: (val) =>
                        val!.isEmpty ? 'Amount is required' : null,
                  ),
                  _padding,
                ],
              ),
            ),
          ),
          _padding,
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.green),
              onPressed: () {},
              child: const Text('Withdraw'),
            ),
          ),
          _padding,
        ],
      ),
    );
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(backgroundColor: color, content: Text(message)));
  }
}
