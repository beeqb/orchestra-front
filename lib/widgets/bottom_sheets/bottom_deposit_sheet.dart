import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../blocs/bloc/navigation_bloc.dart';
import '../../blocs/cubits/checkout_cubit.dart';
import '../../core/app_colors.dart';
import '../../core/const.dart';
import '../../injection_container.dart';
import '../../models/enums/enums.dart';
import '../../services/coinbase/coin_checkout.dart';
import '../../services/coinbase/coin_checkout_web.dart';
import '../../services/dialogs_service.dart';

class DepositPopup extends StatefulWidget {
  const DepositPopup({Key? key}) : super(key: key);

  @override
  State<DepositPopup> createState() => _DepositPopupState();
}

class _DepositPopupState extends State<DepositPopup> {
  final GlobalKey<FormState> _mobileFormKey = GlobalKey<FormState>();
  double _depositAmount = 0.0;

  bool isCryptoCategorySelected = true;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        contentPadding: const EdgeInsets.symmetric(horizontal: 24),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        backgroundColor: Theme.of(context).backgroundColor,
        titlePadding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Deposit funds',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: SizedBox(
          width: ModalDialogConstants.desktopDialogWidth,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  MoneyCategoryLabel(
                    moneyCategory: 'Crypto',
                    onPressed: () {
                      setState(() {
                        isCryptoCategorySelected = true;
                      });
                    },
                    isSelected: isCryptoCategorySelected,
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  MoneyCategoryLabel(
                      moneyCategory: 'Card',
                      isSelected: !isCryptoCategorySelected,
                      onPressed: () {
                        setState(() {
                          isCryptoCategorySelected = false;
                        });
                      }),
                ],
              ),
              const SizedBox(
                height: 42,
              ),
              Text(
                "Amount",
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).iconTheme.color),
              ),
              const SizedBox(
                height: 10,
              ),
              Form(
                key: _mobileFormKey,
                child: TextFormField(
                  decoration: InputDecoration(
                      hintText: '0',
                      hintStyle: Theme.of(context)
                          .textTheme
                          .headlineMedium
                          ?.copyWith(color: Theme.of(context).iconTheme.color),
                      contentPadding: EdgeInsets.only(left: 25),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          'USD',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      )),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[0-9.,]+')),
                  ],
                  onChanged: (value) {
                    _depositAmount = double.tryParse(value) ?? 0.0;
                    setState(() {});
                  },
                  validator: (val) {
                    if (val!.isEmpty) {
                      return 'Amount is required';
                    }
                  },
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              _buttonsPanel(_depositAmount),
              const SizedBox(
                height: 32,
              ),
            ],
          ),
        ));
  }

  Widget _buttonsPanel(double depositAmount) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () async {
            isCryptoCategorySelected
                ? await _redirectToCheckoutCoinbase()
                : await _redirectToCheckoutStripe();
          },
          style: depositAmount != 0.0
              ? Theme.of(context).elevatedButtonTheme.style
              : Theme.of(context).textButtonTheme.style?.copyWith(
                    backgroundColor: MaterialStateProperty.all<Color?>(
                        Theme.of(context).iconTheme.color),
                  ),
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Deposit',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }

  Future<void> _redirectToCheckoutStripe() async {
    final form = _mobileFormKey.currentState;
    if (!form!.validate()) {
    } else {
      form.save();
      Navigator.pop(context);
      await sl<CheckoutCubit>().createCheckout('stripe', _depositAmount);
      if (kIsWeb) {
        redirectToCheckout(context);
      } else {
        await Navigator.pushNamed(context, '/stripeDeposit');
      }
    }
  }

  Future<void> _redirectToCheckoutCoinbase() async {
    final form = _mobileFormKey.currentState;
    if (!form!.validate()) {
    } else {
      form.save();
      if (kIsWeb) {
        await redirectToCoinCheckout(context);
      } else {
        Navigator.pop(context);
        sl<NavigationBLoC>().add(const ToCoinDepositNavigationEvent());
      }
      await sl<CheckoutCubit>().createCheckout(
        'coinbase',
        _depositAmount,
      );
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(backgroundColor: color, content: Text(message)));
  }
}

class BottomDepositSheet extends StatefulWidget {
  const BottomDepositSheet({
    Key? key,
    required CheckoutCubit cubit,
  })  : _cubit = cubit,
        super(key: key);

  final CheckoutCubit _cubit;

  @override
  _BottomDepositSheetState createState() => _BottomDepositSheetState();
}

class _BottomDepositSheetState extends State<BottomDepositSheet> {
  TextEditingController? _depositController;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? radioGroupValue;
  final List<String> _radioValues = ['stripe', 'coinbase'];
  bool _visibleCrypto = true;
  bool _visibleCard = true;
  bool _visibleCardForm = false;
  bool _stripeDeposit = true;

  @override
  void initState() {
    super.initState();
    _depositController = TextEditingController();
    radioGroupValue = _radioValues[0];
  }

  @override
  void dispose() {
    _depositController!.dispose();
    super.dispose();
  }

  final Widget _padding = const SizedBox(height: 18);

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: widget._cubit,
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width <
                      ApplicationsPageConstants.maxWidth
                  ? 0
                  : (MediaQuery.of(context).size.width -
                          ApplicationsPageConstants.maxWidth) /
                      2),
          decoration: BoxDecoration(
            color: Theme.of(context).bottomAppBarColor,
            borderRadius: const BorderRadius.only(
              topRight:
                  Radius.circular(ApplicationsPageConstants.radiusCircular),
              topLeft:
                  Radius.circular(ApplicationsPageConstants.radiusCircular),
            ),
          ),
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                Text(
                  'Deposit',
                  style: Theme.of(context).textTheme.headline6,
                ),
                _padding,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _visibleCard
                        ? Center(
                            child: ElevatedButton.icon(
                              icon: const Icon(Icons.credit_card),
                              label: const Text('With Card'),
                              style: ElevatedButton.styleFrom(
                                primary: AppColors.orangeColor,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                                sl<DialogsService>().showDialog(
                                    dialogType: DialogType.errorSnackbar,
                                    description: 'Temporary unavailable');
                                /* setState(() {
                                  _visibleCrypto = false;
                                  _visibleCard = false;
                                  _visibleCardForm = true;
                                  _stripeDeposit = true;
                                }); */
                              },
                            ),
                          )
                        : const SizedBox(height: 1),
                    const Padding(padding: EdgeInsets.all(5)),
                    _visibleCrypto
                        ? Center(
                            child: ElevatedButton.icon(
                              icon: const Icon(FontAwesomeIcons.bitcoin),
                              label: const Text('With Crypto'),
                              style: ElevatedButton.styleFrom(
                                primary: AppColors.blueColor,
                              ),
                              onPressed: () {
                                setState(() {
                                  _visibleCardForm = true;
                                  _visibleCard = false;
                                  _visibleCrypto = false;
                                  _stripeDeposit = false;
                                });
                              },
                            ),
                          )
                        : const SizedBox(height: 1),
                  ],
                ),
                _padding,
                _visibleCardForm
                    ? Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: ConstrainedBox(
                          constraints: const BoxConstraints(maxWidth: 250),
                          child: TextFormField(
                            controller: _depositController,
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            decoration: const InputDecoration(
                              labelText: 'Enter amount in USD',
                            ),
                            validator: (value) =>
                                value!.isEmpty ? 'Value Can\'t Be Empty' : null,
                          ),
                        ),
                      )
                    : const SizedBox(height: 1),
                _visibleCardForm ? _padding : const SizedBox(height: 1),
                _visibleCardForm
                    ? Center(
                        child: Column(
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.green,
                              ),
                              onPressed: _stripeDeposit
                                  ? _redirectToCheckoutStripe
                                  : _redirectToCheckoutCoinbase,
                              child: const Text('Transfer funds'),
                            ),
                            const SizedBox(height: 10)
                          ],
                        ),
                      )
                    : const SizedBox(height: 1),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _redirectToCheckoutStripe() async {
    final form = _formKey.currentState;
    if (!form!.validate()) {
    } else {
      form.save();
      Navigator.pop(context);
      await sl<CheckoutCubit>()
          .createCheckout('stripe', double.parse(_depositController!.text));
      if (kIsWeb) {
        redirectToCheckout(context);
      } else {
        await Navigator.pushNamed(context, '/stripeDeposit');
      }
    }
  }

  void _redirectToCheckoutCoinbase() async {
    final form = _formKey.currentState;
    if (!form!.validate()) {
    } else {
      form.save();
      await sl<CheckoutCubit>().createCheckout(
        'coinbase',
        double.parse(_depositController!.text),
      );
      if (kIsWeb) {
        redirectToCoinCheckout(context);
      } else {
        Navigator.pop(context);
        sl<NavigationBLoC>().add(const ToCoinDepositNavigationEvent());
      }
    }
  }
}

class MoneyCategoryLabel extends StatelessWidget {
  const MoneyCategoryLabel(
      {Key? key,
      required this.moneyCategory,
      required this.onPressed,
      required this.isSelected})
      : super(key: key);

  final Function() onPressed;
  final String moneyCategory;
  final bool isSelected;

  TextStyle? labelStyle(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    if (isSelected) {
      if (isMobile) {
        return Theme.of(context).textTheme.labelSmall;
      } else {
        return Theme.of(context).textTheme.labelMedium;
      }
    } else {
      if (isMobile) {
        return Theme.of(context)
            .textTheme
            .labelSmall
            ?.copyWith(color: Theme.of(context).disabledColor);
      } else {
        return Theme.of(context)
            .textTheme
            .labelMedium
            ?.copyWith(color: Theme.of(context).disabledColor);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: isSelected
            ? Theme.of(context).cardColor
            : Theme.of(context).backgroundColor,
      ),
      child: TextButton(
          onPressed: onPressed,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 8,
            ),
            child: Text(moneyCategory, style: labelStyle(context)),
          )),
    );
  }
}
