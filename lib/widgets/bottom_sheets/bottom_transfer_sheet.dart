import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/cubits/transfer_user_cubit.dart';
import '../../core/const.dart';
import '../../injection_container.dart';

class MoneyTransferPopup extends StatefulWidget {
  const MoneyTransferPopup({Key? key}) : super(key: key);

  @override
  State<MoneyTransferPopup> createState() => _MoneyTransferPopupState();
}

class _MoneyTransferPopupState extends State<MoneyTransferPopup> {
  final GlobalKey<FormState> _mobileFormKey = GlobalKey<FormState>();
  double _transferAmount = 0.0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        backgroundColor: Theme.of(context).backgroundColor,
        titlePadding: const EdgeInsets.symmetric(vertical: 32, horizontal: 24),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Transfer funds',
              style: Theme.of(context).textTheme.titleMedium,
            ),
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: const Icon(Icons.close_sharp))
          ],
        ),
        content: SizedBox(
          width: ModalDialogConstants.desktopDialogWidth,
          child: Form(
            key: _mobileFormKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(child: TransferAvatar()),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "Exact  user ID, name or email",
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  onChanged: (value) async {
                    if (value.length > 3) {
                      await sl<TransferUserCubit>().getUserByQuery(value);
                    }
                  },
                  validator: (val) => val!.isEmpty
                      ? 'Name, user ID or email is required'
                      : null,
                ),
                const SizedBox(
                  height: 32,
                ),
                Text(
                  "Amount",
                  style: Theme.of(context)
                      .textTheme
                      .headlineMedium
                      ?.copyWith(color: Theme.of(context).iconTheme.color),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: '0',
                      hintStyle: Theme.of(context)
                          .textTheme
                          .headlineMedium
                          ?.copyWith(color: Theme.of(context).iconTheme.color),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.all(14.0),
                        child: Text(
                          'USD',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      )),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[0-9.,]+')),
                  ],
                  onChanged: (value) {
                    _transferAmount = double.tryParse(value) ?? 0.0;
                    setState(() {});
                  },
                  validator: (val) =>
                      val!.isEmpty ? 'Amount is required' : null,
                ),
                const SizedBox(
                  height: 24,
                ),
                _buttonsPanel(_transferAmount)
              ],
            ),
          ),
        ));
  }

  Widget buildUserInfo({required BuildContext context}) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<TransferUserCubit, TransferUserState>(
          builder: (context, state) {
            if (state is TransferUserUpdated) {
              return Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(
                                state.transferUser.photoUrl),
                          ),
                          border:
                              Border.all(color: Colors.grey[400]!, width: 3),
                        ),
                      ),
                    ),
                    Text(
                      state.transferUser.name,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
              );
            }
            if (state is TransferSent) {
              return _emptyUser;
            }
            return _emptyUser;
          },
        ),
      ),
    );
  }

  final Widget _emptyUser = Center(
    child: Container(
      height: 100,
      width: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey[200],
      ),
      child: const Icon(
        Icons.person,
        size: 80,
      ),
    ),
  );

  Widget _buttonsPanel(double transferAmount) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        TextButton(
          onPressed: () {
            _transferFunds();
          },
          style: transferAmount != 0.0
              ? Theme.of(context).elevatedButtonTheme.style
              : Theme.of(context).textButtonTheme.style?.copyWith(
                    backgroundColor: MaterialStateProperty.all<Color?>(
                        Theme.of(context).iconTheme.color),
                  ),
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Transfer',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }

  void _transferFunds() {
    final form = _mobileFormKey.currentState;
    if (!form!.validate()) {
      showMessage('Form is not valid!  Please review and correct.');
    } else {
      form.save();
      sl<TransferUserCubit>().sendTransfer(_transferAmount);
      form.reset();
      showMessage('The transfer was successful', Colors.green);
      Navigator.pop(context);
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(backgroundColor: color, content: Text(message)));
  }
}

class BottomTransferSheet extends StatefulWidget {
  const BottomTransferSheet({Key? key}) : super(key: key);
  @override
  _BottomTransferSheetState createState() => _BottomTransferSheetState();
}

class _BottomTransferSheetState extends State<BottomTransferSheet> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  double _transferAmount = 0.0;

  final Widget _padding = const SizedBox(
    height: 18,
    width: double.infinity,
  );

  @override
  Widget build(BuildContext context) {
    var sizeBotoom = MediaQuery.of(context).viewInsets.bottom;
    log(sizeBotoom.toString());
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        //height: MediaQuery.of(context).size.height / 2,
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width <
                    ApplicationsPageConstants.maxWidth
                ? 0
                : (MediaQuery.of(context).size.width -
                        ApplicationsPageConstants.maxWidth) /
                    2),
        decoration: BoxDecoration(
          color: Theme.of(context).bottomAppBarColor,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
            topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    buildUserInfo(context: context),
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: 'Enter exact user ID, name or email',
                      ),
                      onChanged: (value) async {
                        if (value.length > 2) {
                          await sl<TransferUserCubit>().getUserByQuery(value);
                        }
                      },
                      validator: (val) => val!.isEmpty
                          ? 'Name, user ID or email is required'
                          : null,
                    ),
                    _padding,
                    TextFormField(
                      //  textAlign: TextAlign.center,
                      keyboardType:
                          const TextInputType.numberWithOptions(decimal: true),
                      decoration: const InputDecoration(
                        labelText: 'Enter amount in USD',
                      ),
                      onSaved: (newValue) {
                        _transferAmount = double.parse(newValue ?? '0.0');
                      },
                      validator: (val) =>
                          val!.isEmpty ? 'Amount is required' : null,
                    ),
                  ],
                ),
              ),
            ),
            _padding,
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green,
                ),
                onPressed: () {
                  Navigator.pop(context);
                  _transferFunds();
                },
                child: const Text('Transfer funds'),
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(
            //       bottom: MediaQuery.of(context).viewInsets.bottom),
            // ),
            _padding,
          ],
        ),
      ),
    );
  }

  final Widget _emptyUser = Center(
    child: Container(
      height: 100,
      width: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey[200],
      ),
      child: const Icon(
        Icons.person,
        size: 80,
      ),
    ),
  );

  Widget buildUserInfo({required BuildContext context}) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<TransferUserCubit, TransferUserState>(
          builder: (context, state) {
            if (state is TransferUserUpdated) {
              return Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(
                                state.transferUser.photoUrl),
                          ),
                          border:
                              Border.all(color: Colors.grey[400]!, width: 3),
                        ),
                      ),
                    ),
                    Text(
                      state.transferUser.name,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
              );
            }
            if (state is TransferSent) {
              return _emptyUser;
            }
            return _emptyUser;
          },
        ),
      ),
    );
  }

  void _transferFunds() {
    final form = _formKey.currentState;
    if (!form!.validate()) {
      showMessage('Form is not valid!  Please review and correct.');
    } else {
      form.save();
      sl<TransferUserCubit>().sendTransfer(_transferAmount);
      form.reset();
      showMessage('The transfer was successful', Colors.green);
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(backgroundColor: color, content: Text(message)));
  }
}

class TransferAvatar extends StatelessWidget {
  const TransferAvatar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            width: 100,
            height: 100,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(45.0),
                child: BlocBuilder<TransferUserCubit, TransferUserState>(
                    builder: (context, state) {
                  if (state is TransferUserUpdated) {
                    final photoUrl = state.transferUser.photoUrl;
                    return CachedNetworkImage(imageUrl: photoUrl);
                  } else if (state is TransferUserInitial) {
                    return Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Theme.of(context).iconTheme.color ??
                                const Color(0xFF000000),
                            width: 2),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(45.0)),
                      ),
                      child: const Icon(
                        Icons.person,
                        size: 80,
                      ),
                    );
                  } else if (state is TransferUserUpdating) {
                    return const CircularProgressIndicator();
                  } else if (state is TransferUserError) {
                    return Container(
                        decoration: BoxDecoration(
                          // color: Theme.of(context).iconTheme.color,
                          border: Border.all(
                              color: Theme.of(context).iconTheme.color ??
                                  const Color(0xFF000000),
                              width: 2),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(45.0)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Image.asset(
                            'assets/images/user_not_found.png',
                            color: Theme.of(context).iconTheme.color,
                          ),
                        ));
                  }
                  return const SizedBox.shrink();
                }))),
        BlocBuilder<TransferUserCubit, TransferUserState>(
            builder: (context, state) {
          if (state is TransferUserError) {
            return SizedBox(
              height: 20,
              child: Text('User not found',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: Theme.of(context).disabledColor)),
            );
          }
          if (state is TransferUserUpdated) {
            return Text(state.transferUser.name,
                style: Theme.of(context).textTheme.labelSmall);
          }
          return const SizedBox(height: 20);
        })
      ],
    );
  }
}
