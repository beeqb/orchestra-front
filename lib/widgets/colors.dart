import 'dart:math';

import 'package:flutter/material.dart';

const Color black = Colors.black;
const Color google = Color(0xFFEA4335);
const Color facebook = Color(0xFF324B81);

const List<Color> listOfColors = [
  Colors.red,
  Colors.blue,
  Colors.blueGrey,
  Colors.amber,
  Colors.indigoAccent,
  Colors.cyan,
  Colors.green,
  Colors.lime,
  Colors.teal,
  Colors.deepOrangeAccent,
  Colors.deepPurple,
];

Color getRandomColor() => listOfColors[Random().nextInt(listOfColors.length)];
