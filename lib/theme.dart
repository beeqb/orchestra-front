import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'core/app_colors.dart';
import 'core/theme_extensions.dart';

final kLightTheme = ThemeData.light().copyWith(
  backgroundColor: const Color(0xFFFFFFFF),
  scaffoldBackgroundColor: const Color(0xFFF5F5F7),
  errorColor: const Color(0xFFFA4F59),
  cardColor: const Color(0xFFF1F1F1),
  secondaryHeaderColor: const Color(0xFFEAEAEA),
  disabledColor: const Color(0xFF6C6C6E),
  dialogBackgroundColor: const Color(0xFFFFFFFF),
  bottomAppBarColor: const Color(0xFFF5F5F7),
  dividerColor: const Color(0xFFEAEAEA),
  primaryColor: const Color(0xFFFFFFFF),
  primaryColorLight: const Color(0xFFF5F5F7),
  primaryColorDark: const Color(0xFF24DF9B),
  iconTheme: const IconThemeData(color: Color(0xFF8B8B8B)),
  drawerTheme:
      const DrawerThemeData(width: 240, backgroundColor: Color(0xFFF1F1F1)),
  primaryIconTheme: const IconThemeData(color: Color(0xFF22222C)),
  dividerTheme:
      const DividerThemeData(thickness: 1.0, color: Color(0xFFEAEAEA)),
  textSelectionTheme:
      const TextSelectionThemeData(cursorColor: Color(0xFF22222C)),
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: const TextStyle(
        color: Color(0xFF6C6C6E), fontSize: 18, fontWeight: FontWeight.w500),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF30303E)),
    ),
    disabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF30303E)),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF30303E)),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF30303E)),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFFFA4F59)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFFFA4F59)),
    ),
    //  labelStyle: TextStyle(color: Colors.grey),
  ),
  popupMenuTheme: const PopupMenuThemeData(
    color: Color(0xFFF5F5F7),
    textStyle: TextStyle(
      color: Color(0xFF8B8B8B),
      fontSize: 16,
      fontFamily: "Gordita",
      fontWeight: FontWeight.w500,
    ),
  ),
  textTheme: GoogleFonts.ptSansTextTheme().copyWith(
    displayLarge: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFF22222C),
      fontSize: 48,
    ),
    displayMedium: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFF22222C),
      fontSize: 24,
    ),
    displaySmall: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 18,
    ),
    headlineLarge: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFF22222C),
      fontSize: 24,
      fontWeight: FontWeight.w500,
    ),
    headlineMedium: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 18,
      fontWeight: FontWeight.w500,
    ),
    headlineSmall: const TextStyle(
        color: Color(0xFF22222C), fontSize: 14, fontWeight: FontWeight.w500),
    titleLarge: const TextStyle(
        color: Color(0xFF22222C), fontSize: 32, fontWeight: FontWeight.w600),
    titleMedium: const TextStyle(
        color: Color(0xFF22222C), fontSize: 20, fontWeight: FontWeight.w500),
    titleSmall: const TextStyle(
        color: Color(0xFF22222C), fontSize: 16, fontWeight: FontWeight.w500),
    labelLarge: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 24,
      letterSpacing: 0,
    ),
    labelMedium: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 16,
      letterSpacing: 0,
    ),
    labelSmall: const TextStyle(
      color: Color(0xFF22222C),
      letterSpacing: 0,
      fontSize: 14,
    ),
    bodyLarge: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 16,
    ),
    bodyMedium: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 14,
    ),
    bodySmall: const TextStyle(
      color: Color(0xFF22222C),
      fontSize: 12,
    ),
  ),
  buttonTheme: ButtonThemeData(
    height: 50,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
  ),
  floatingActionButtonTheme:
      const FloatingActionButtonThemeData(backgroundColor: AppColors.blueColor),
  appBarTheme: const AppBarTheme(
    backgroundColor: Color(0xFFF5F5F7),
    iconTheme: IconThemeData(
      color: Color(0xFF8B8B8B),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12),
    ),
  )),
  textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
    padding: EdgeInsets.zero,
    textStyle: const TextStyle(
      color: Color(0xFF22AFFF),
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12),
    ),
  )),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
    elevation: 0,
    backgroundColor: const Color(0xFF24DF9B),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12.0),
    ),
  )),
  extensions: <ThemeExtension<dynamic>>[
    const SidebarColors(
      selectedIconColor: Color(0xFF22222C),
      unselectedIconColor: Color(0xFF6C6C6E),
      selectedTextColor: Color(0xFF22222C),
      unselectedTextColor: Color(0xFF6C6C6E),
      selectedBoxColor: Color(0xFFEAEAEA),
      backgroundColor: Color(0xFFFFFFFF),
    ),
  ],
);

final kDarkTheme = ThemeData.dark().copyWith(
  backgroundColor: const Color(0xFF22222C),
  scaffoldBackgroundColor: const Color(0xFF171721),
  errorColor: const Color(0xFFFA4F59),
  cardColor: const Color(0xFF33333F),
  secondaryHeaderColor: const Color(0xff32323e),
  disabledColor: const Color(0xFF6C6C6E),
  dialogBackgroundColor: const Color(0xFF22222C),
  bottomAppBarColor: const Color(0xFF171721),
  dividerColor: const Color(0xFF232332),
  primaryColor: const Color(0xFF33333F),
  primaryColorLight: const Color(0xFF22222C),
  primaryColorDark: const Color(0xFF24DF9B),
  iconTheme: const IconThemeData(color: Color(0xFF8B8B8B)),
  drawerTheme:
      const DrawerThemeData(width: 240, backgroundColor: Color(0xFF33333F)),
  primaryIconTheme: const IconThemeData(color: Color(0xFFFFFFFF)),
  dividerTheme:
      const DividerThemeData(thickness: 1.0, color: Color(0xFF424252)),
  appBarTheme: const AppBarTheme(
    backgroundColor: Color(0xFF171721),
    iconTheme: IconThemeData(
      color: Color(0xFF8B8B8B),
    ),
  ),
  textSelectionTheme:
      const TextSelectionThemeData(cursorColor: Color(0xFFFFFFFF)),
  textTheme: GoogleFonts.ptSansTextTheme().copyWith(
    displayLarge: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFFFFFFFF),
      fontSize: 48,
    ),
    displayMedium: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFFFFFFFF),
      fontSize: 24,
    ),
    displaySmall: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 18,
    ),
    headlineLarge: GoogleFonts.robotoCondensed().copyWith(
      color: const Color(0xFFFFFFFF),
      fontSize: 24,
      fontWeight: FontWeight.w500,
    ),
    headlineMedium: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 18,
      fontWeight: FontWeight.w500,
    ),
    headlineSmall: const TextStyle(
        color: Color(0xFFFFFFFF), fontSize: 14, fontWeight: FontWeight.w500),
    titleLarge: const TextStyle(
        color: Color(0xFFFFFFFF), fontSize: 32, fontWeight: FontWeight.w600),
    titleMedium: const TextStyle(
        color: Color(0xFFFFFFFF), fontSize: 20, fontWeight: FontWeight.w500),
    titleSmall: const TextStyle(
        color: Color(0xFFFFFFFF), fontSize: 16, fontWeight: FontWeight.w500),
    labelLarge: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 24,
      letterSpacing: 0,
    ),
    labelMedium: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 16,
      letterSpacing: 0,
    ),
    labelSmall: const TextStyle(
      color: Color(0xFFFFFFFF),
      letterSpacing: 0,
      fontSize: 14,
    ),
    bodyLarge: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 16,
    ),
    bodyMedium: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 14,
    ),
    bodySmall: const TextStyle(
      color: Color(0xFFFFFFFF),
      fontSize: 12,
    ),
  ),
  inputDecorationTheme: InputDecorationTheme(
    hintStyle: const TextStyle(
        color: Color(0xFF6C6C6E), fontSize: 18, fontWeight: FontWeight.w500),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF6C6C6E)),
    ),
    disabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF6C6C6E)),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF6C6C6E)),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFF6C6C6E)),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFFFA4F59)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(color: Color(0xFFFA4F59)),
    ),
    //  labelStyle: TextStyle(color: Colors.grey),
  ),
  popupMenuTheme: const PopupMenuThemeData(
    color: Color(0xFF171721),
    textStyle: TextStyle(
      color: Color(0xFF8B8B8B),
      fontSize: 16,
      fontFamily: "Gordita",
      fontWeight: FontWeight.w500,
    ),
  ),
  buttonTheme: ButtonThemeData(
    height: 50,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
  ),
  floatingActionButtonTheme:
      const FloatingActionButtonThemeData(backgroundColor: AppColors.blueColor),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12),
    ),
  )),
  textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
    padding: EdgeInsets.zero,
    textStyle: const TextStyle(
      color: Color(0xFF22AFFF),
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12),
    ),
  )),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
    elevation: 0,
    backgroundColor: const Color(0xFF24DF9B),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(12.0),
    ),
  )),
  extensions: <ThemeExtension<dynamic>>[
    const SidebarColors(
      selectedIconColor: Color(0xFFFFFFFF),
      unselectedIconColor: Color(0xFF8B8B8B),
      selectedTextColor: Color(0xFFFFFFFF),
      unselectedTextColor: Color(0xFF8B8B8B),
      selectedBoxColor: Color(0xFF34343E),
      backgroundColor: Color(0xFF171721),
    ),
  ],
);
