import 'package:flutter/material.dart';

import '../config.dart';

// Длительность анимации при сдвиге страницы (показ меню)
const kSlidingPageAnimDuration = Duration(milliseconds: 300);

const defaultUserId = '123456';

const failureWidgetMessage =
    'During the last launch of the widget, something went wrong. We notified the developer about it. We are confident that he is working on correcting malfunctions.';

const String defaultAvatar = 'assets/images/not_image.png';

class ApplicationsPageConstants {
  static const double maxWidth = 1000; //for modal bottom sheet
  static const double radiusCircular = 12; //for modal bottom sheet top radius
  static const double kChartHeight = 343;
  static const double distanceFromTopToPanel = 192;
  static const double desktopTopPanelHeight = 16;
  static const double mobileTopPanelHeight = 4;
  static const double desktopBottomPanelHeight = 68;
  static const double mobileBottomPanelHeight = 44;
  static const double desktopPageHorizontalPadding = 36.0;
  static const double mobilePageHorizontalPadding = 13.0;
  static const String resultsSheetTitle = 'Latest results';
  static const String appStopped = 'App stopped';
  static const String appStarted = 'App started';
}

class ModalDialogConstants {
  static const double desktopDialogWidth = 468;
  static const double mobileHorizontalPadding = 15;
  static const double mobileVerticalPadding = 15;
  static const double desktopHorizontalPadding = 24;
  static const double desktopVerticalPadding = 24;
  static const double mobileHeaderHeight = 66;
  static const mobileBorder = RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
      topLeft: Radius.circular(8),
      topRight: Radius.circular(8),
    ),
  );
}

class AppBarSizeCostants {
  static const double desktopAppbarHeight = 72;
  static const double desktopSubBarHeight = 108;
  static const double mobileAppbarHeight = 58;
  static const double mobileSubBarHeight = 36;
}

class ProfileItemConstants {
  static const String darkModeTitle = 'Dark mode';
}

class TopProductCardConstants {
  static const double desktopCardHeight = 314;
  static const double mobileCardHeight = 176;

  static const double desktopCardWidth = 412;
  static const double mobileCardWidth = 314;

  static const double desktopPromoHeight = 200;
  static const double mobilePromoHeight = 108;
}

class ProductCardSizeConstants {
  static const double desktopCardHeight = 114;
  static const double mobileCardHeight = 60;

  static const double desktopCardWidth = 412;
  static const double mobileCardWidth = 314;
}

class SidebarConstants {
  static const double collapseBreakpoint = 1000;
  static const double hideBreakpoint = 800;
  static const double minWidth = 72;
  static const double maxWidth = 200;
  static const double logoSize = 40;
  static const logoTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontFamily: "Gilroy",
    fontWeight: FontWeight.w700,
  );
  static const itemTextStyle = TextStyle(
    fontSize: 18,
    fontFamily: "SF Pro Display",
    fontWeight: FontWeight.w500,
  );
}

class StatisticsConstants {
  static const double firstRowHeight = 221;
  static const double secondRowHeight = 480;
  static const double rowWidth = 348;
}

class BottomNavbarConstants {
  static const double hideBreakpoint = 800;
  static const double iconSize = 20;
  static const selectedLabelStyle = TextStyle(
      height: 2,
      fontSize: 11,
      fontFamily: "SF Pro Display",
      fontWeight: FontWeight.w500);
  static const unselectedLabelStyle = TextStyle(
      height: 2,
      fontSize: 11,
      fontFamily: "SF Pro Display",
      fontWeight: FontWeight.w500);
}

class UrlConstants {
  static const kGetAppByUserIdUrl = '$kHostUrl/constructor/getAppsByUserId';
  static const kSaveAppUrl = '$kHostUrl/constructor/saveApp/';
  static const kUpdateAppUrl = '$kHostUrl/constructor/updateApp/';
  static const kDeleteAppUrl = '$kHostUrl/constructor/deleteApp/';
  static const kStartAppUrl = '$kHostUrl/constructor/startApp/';
  static const kPauseAppUrl = '$kHostUrl/constructor/pauseApp/';
  static const kGetProductsNotCrByUser =
      '$kHostUrl/product/getProductsNotCreatedByUser';
  static const kProductUrl = '$kHostUrl/product';
  static const kReferralUrl = 'https://app.beeqb.com/#/onBoarding?ref=';
}

class ProductTypeAliases {
  static const kProductType = {
    'PAPP': 'Private APP',
    'ST': 'Starter',
    'PA': 'Parser',
    'DP': 'Data processor',
    'RS': 'Result supplier',
    'DE': 'Debugger',
    'OT': 'Other',
  };
}

class AddLorePageConstants {
  static const int photoDescriptionUrlMaxNumber = 5;
  static const String photoDescriptionUrlDeletedLabel = 'deleted';
}

class LoopPeriod {
  // https://www.epochconverter.com/
  // milliseconds
  static const kLoopPeriod = {
    'No Loop': 'none',
    '5 minutes': '300000',
    '15 minutes': '900000',
    '30 minutes': '1800000',
    '1 hour': '3600000',
    '3 hours': '10800000',
    '6 hours': '21600000',
    '12 hours': '43200000',
    '1 day': '86400000',
    '1 week': '604800000',
    '1 month': '2629743000',
    '3 months': '7889229000',
    '1 year': '31556926000'
  };
}

class NotificationType {
  static const kNotifications = {
    'modal': 'In App notification',
    'push': 'Push notification',
    'email': ' Email notification',
    'telegram': 'Into Telegram',
  };
}

const kTestEndsAfter = {
  '5l': '5 launches',
  '15l': '15 launches',
  '3d': '3 days',
  '7d': '7 days',
};

const kFileImageName = {
  'productLauncherFileUrl': ['zip'],
  'productLogoURL': ['jpg', 'png', 'gif'],
  'productMiniPromoPictureURL': ['jpg', 'png', 'gif'],
  'productPromoPictureURL': ['jpg', 'png', 'gif'],
};

const kOnboardingImages = [
  'assets/onboarding/onboarding_1.png',
  'assets/onboarding/onboarding_2.png',
  'assets/onboarding/onboarding_3.png',
  'assets/onboarding/onboarding_4.png',
];

const kUserDataColumns = [
  'User',
  'Status',
  'Role',
  'Added',
  'Spends',
  'Balance',
  'Profit',
];

const kProdDataColumns = [
  'Status',
  'Lore',
  'Contributor',
  'Type',
  'Category',
  'Price',
  'Users',
  'Sales',
  'Launches',
  'Added',
];
const kTransactionDataColumns = [
  'User',
  'Balance',
  'In',
  'Out',
  'Type',
  'Status',
  'Filled',
];

const kRunningAppsDataColumn = [
  'App name',
  'User',
  'Start time',
  'Loop',
];
