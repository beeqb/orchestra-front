import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppColors {
  static const Color blueColor = Color(0xff0c99c3);
  static const Color greenColor = Color(0xff3dc39d);
  static const Color yellowColor = Color(0xffdac007);
  static const Color redColor = Color(0xffbe0b2b);
  static const Color orangeColor = Color(0xffbe740b);
}
