import 'package:flutter/material.dart';

class SidebarColors extends ThemeExtension<SidebarColors> {
  const SidebarColors({
    required this.selectedIconColor,
    required this.unselectedIconColor,
    required this.selectedTextColor,
    required this.unselectedTextColor,
    required this.selectedBoxColor,
    required this.backgroundColor,
  });

  final Color? selectedIconColor;
  final Color? unselectedIconColor;
  final Color? selectedTextColor;
  final Color? unselectedTextColor;
  final Color? selectedBoxColor;
  final Color? backgroundColor;

  @override
  SidebarColors copyWith(
      {Color? selectedIconColor,
      Color? unselectedIconColor,
      Color? selectedTextColor,
      Color? unselectedTextColor,
      Color? selectedBoxColor,
      Color? backgroundColor}) {
    return SidebarColors(
      selectedIconColor: selectedIconColor ?? selectedIconColor,
      unselectedIconColor: unselectedIconColor ?? unselectedIconColor,
      selectedTextColor: selectedTextColor ?? selectedTextColor,
      unselectedTextColor: unselectedTextColor ?? unselectedTextColor,
      selectedBoxColor: selectedBoxColor ?? selectedBoxColor,
      backgroundColor: backgroundColor ?? backgroundColor,
    );
  }

  @override
  SidebarColors lerp(ThemeExtension<SidebarColors>? other, double t) {
    if (other is! SidebarColors) {
      return this;
    }
    return SidebarColors(
        selectedIconColor:
            Color.lerp(selectedIconColor, other.selectedIconColor, t),
        unselectedIconColor:
            Color.lerp(unselectedIconColor, other.unselectedIconColor, t),
        selectedTextColor:
            Color.lerp(selectedTextColor, other.selectedTextColor, t),
        unselectedTextColor:
            Color.lerp(unselectedTextColor, other.unselectedTextColor, t),
        selectedBoxColor:
            Color.lerp(selectedBoxColor, other.selectedBoxColor, t),
        backgroundColor: Color.lerp(backgroundColor, other.backgroundColor, t));
  }
}
