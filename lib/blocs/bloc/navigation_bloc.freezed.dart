// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'navigation_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$NavigationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NavigationEventCopyWith<$Res> {
  factory $NavigationEventCopyWith(
          NavigationEvent value, $Res Function(NavigationEvent) then) =
      _$NavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$NavigationEventCopyWithImpl<$Res>
    implements $NavigationEventCopyWith<$Res> {
  _$NavigationEventCopyWithImpl(this._value, this._then);

  final NavigationEvent _value;
  // ignore: unused_field
  final $Res Function(NavigationEvent) _then;
}

/// @nodoc
abstract class _$$ToMarketplaceNavigationEventCopyWith<$Res> {
  factory _$$ToMarketplaceNavigationEventCopyWith(
          _$ToMarketplaceNavigationEvent value,
          $Res Function(_$ToMarketplaceNavigationEvent) then) =
      __$$ToMarketplaceNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToMarketplaceNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToMarketplaceNavigationEventCopyWith<$Res> {
  __$$ToMarketplaceNavigationEventCopyWithImpl(
      _$ToMarketplaceNavigationEvent _value,
      $Res Function(_$ToMarketplaceNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToMarketplaceNavigationEvent));

  @override
  _$ToMarketplaceNavigationEvent get _value =>
      super._value as _$ToMarketplaceNavigationEvent;
}

/// @nodoc

class _$ToMarketplaceNavigationEvent extends ToMarketplaceNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToMarketplaceNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toMarketplace()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toMarketplace'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToMarketplaceNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toMarketplace();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toMarketplace?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toMarketplace != null) {
      return toMarketplace();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toMarketplace(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toMarketplace?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toMarketplace != null) {
      return toMarketplace(this);
    }
    return orElse();
  }
}

abstract class ToMarketplaceNavigationEvent extends NavigationEvent {
  const factory ToMarketplaceNavigationEvent() = _$ToMarketplaceNavigationEvent;
  const ToMarketplaceNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToProductDetailsNavigationEventCopyWith<$Res> {
  factory _$$ToProductDetailsNavigationEventCopyWith(
          _$ToProductDetailsNavigationEvent value,
          $Res Function(_$ToProductDetailsNavigationEvent) then) =
      __$$ToProductDetailsNavigationEventCopyWithImpl<$Res>;
  $Res call({String productId});
}

/// @nodoc
class __$$ToProductDetailsNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToProductDetailsNavigationEventCopyWith<$Res> {
  __$$ToProductDetailsNavigationEventCopyWithImpl(
      _$ToProductDetailsNavigationEvent _value,
      $Res Function(_$ToProductDetailsNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToProductDetailsNavigationEvent));

  @override
  _$ToProductDetailsNavigationEvent get _value =>
      super._value as _$ToProductDetailsNavigationEvent;

  @override
  $Res call({
    Object? productId = freezed,
  }) {
    return _then(_$ToProductDetailsNavigationEvent(
      productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ToProductDetailsNavigationEvent extends ToProductDetailsNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToProductDetailsNavigationEvent(this.productId) : super._();

  @override
  final String productId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toProductDetails(productId: $productId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationEvent.toProductDetails'))
      ..add(DiagnosticsProperty('productId', productId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToProductDetailsNavigationEvent &&
            const DeepCollectionEquality().equals(other.productId, productId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(productId));

  @JsonKey(ignore: true)
  @override
  _$$ToProductDetailsNavigationEventCopyWith<_$ToProductDetailsNavigationEvent>
      get copyWith => __$$ToProductDetailsNavigationEventCopyWithImpl<
          _$ToProductDetailsNavigationEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toProductDetails(productId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toProductDetails?.call(productId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProductDetails != null) {
      return toProductDetails(productId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toProductDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toProductDetails?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProductDetails != null) {
      return toProductDetails(this);
    }
    return orElse();
  }
}

abstract class ToProductDetailsNavigationEvent extends NavigationEvent {
  const factory ToProductDetailsNavigationEvent(final String productId) =
      _$ToProductDetailsNavigationEvent;
  const ToProductDetailsNavigationEvent._() : super._();

  String get productId;
  @JsonKey(ignore: true)
  _$$ToProductDetailsNavigationEventCopyWith<_$ToProductDetailsNavigationEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToProductDetailsUseNavigationEventCopyWith<$Res> {
  factory _$$ToProductDetailsUseNavigationEventCopyWith(
          _$ToProductDetailsUseNavigationEvent value,
          $Res Function(_$ToProductDetailsUseNavigationEvent) then) =
      __$$ToProductDetailsUseNavigationEventCopyWithImpl<$Res>;
  $Res call({String productId, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ToProductDetailsUseNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToProductDetailsUseNavigationEventCopyWith<$Res> {
  __$$ToProductDetailsUseNavigationEventCopyWithImpl(
      _$ToProductDetailsUseNavigationEvent _value,
      $Res Function(_$ToProductDetailsUseNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToProductDetailsUseNavigationEvent));

  @override
  _$ToProductDetailsUseNavigationEvent get _value =>
      super._value as _$ToProductDetailsUseNavigationEvent;

  @override
  $Res call({
    Object? productId = freezed,
    Object? product = freezed,
  }) {
    return _then(_$ToProductDetailsUseNavigationEvent(
      productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ToProductDetailsUseNavigationEvent
    extends ToProductDetailsUseNavigationEvent with DiagnosticableTreeMixin {
  const _$ToProductDetailsUseNavigationEvent(this.productId, this.product)
      : super._();

  @override
  final String productId;
  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toProductDetailsUse(productId: $productId, product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationEvent.toProductDetailsUse'))
      ..add(DiagnosticsProperty('productId', productId))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToProductDetailsUseNavigationEvent &&
            const DeepCollectionEquality().equals(other.productId, productId) &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(productId),
      const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ToProductDetailsUseNavigationEventCopyWith<
          _$ToProductDetailsUseNavigationEvent>
      get copyWith => __$$ToProductDetailsUseNavigationEventCopyWithImpl<
          _$ToProductDetailsUseNavigationEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toProductDetailsUse(productId, product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toProductDetailsUse?.call(productId, product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProductDetailsUse != null) {
      return toProductDetailsUse(productId, product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toProductDetailsUse(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toProductDetailsUse?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProductDetailsUse != null) {
      return toProductDetailsUse(this);
    }
    return orElse();
  }
}

abstract class ToProductDetailsUseNavigationEvent extends NavigationEvent {
  const factory ToProductDetailsUseNavigationEvent(
          final String productId, final Product product) =
      _$ToProductDetailsUseNavigationEvent;
  const ToProductDetailsUseNavigationEvent._() : super._();

  String get productId;
  Product get product;
  @JsonKey(ignore: true)
  _$$ToProductDetailsUseNavigationEventCopyWith<
          _$ToProductDetailsUseNavigationEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToCategoryNavigationEventCopyWith<$Res> {
  factory _$$ToCategoryNavigationEventCopyWith(
          _$ToCategoryNavigationEvent value,
          $Res Function(_$ToCategoryNavigationEvent) then) =
      __$$ToCategoryNavigationEventCopyWithImpl<$Res>;
  $Res call({ProductCategoryType categoryType});
}

/// @nodoc
class __$$ToCategoryNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToCategoryNavigationEventCopyWith<$Res> {
  __$$ToCategoryNavigationEventCopyWithImpl(_$ToCategoryNavigationEvent _value,
      $Res Function(_$ToCategoryNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToCategoryNavigationEvent));

  @override
  _$ToCategoryNavigationEvent get _value =>
      super._value as _$ToCategoryNavigationEvent;

  @override
  $Res call({
    Object? categoryType = freezed,
  }) {
    return _then(_$ToCategoryNavigationEvent(
      categoryType == freezed
          ? _value.categoryType
          : categoryType // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
    ));
  }
}

/// @nodoc

class _$ToCategoryNavigationEvent extends ToCategoryNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToCategoryNavigationEvent(this.categoryType) : super._();

  @override
  final ProductCategoryType categoryType;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toCategory(categoryType: $categoryType)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationEvent.toCategory'))
      ..add(DiagnosticsProperty('categoryType', categoryType));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToCategoryNavigationEvent &&
            const DeepCollectionEquality()
                .equals(other.categoryType, categoryType));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(categoryType));

  @JsonKey(ignore: true)
  @override
  _$$ToCategoryNavigationEventCopyWith<_$ToCategoryNavigationEvent>
      get copyWith => __$$ToCategoryNavigationEventCopyWithImpl<
          _$ToCategoryNavigationEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toCategory(categoryType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toCategory?.call(categoryType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toCategory != null) {
      return toCategory(categoryType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toCategory(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toCategory?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toCategory != null) {
      return toCategory(this);
    }
    return orElse();
  }
}

abstract class ToCategoryNavigationEvent extends NavigationEvent {
  const factory ToCategoryNavigationEvent(
      final ProductCategoryType categoryType) = _$ToCategoryNavigationEvent;
  const ToCategoryNavigationEvent._() : super._();

  ProductCategoryType get categoryType;
  @JsonKey(ignore: true)
  _$$ToCategoryNavigationEventCopyWith<_$ToCategoryNavigationEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToDashboardNavigationEventCopyWith<$Res> {
  factory _$$ToDashboardNavigationEventCopyWith(
          _$ToDashboardNavigationEvent value,
          $Res Function(_$ToDashboardNavigationEvent) then) =
      __$$ToDashboardNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToDashboardNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToDashboardNavigationEventCopyWith<$Res> {
  __$$ToDashboardNavigationEventCopyWithImpl(
      _$ToDashboardNavigationEvent _value,
      $Res Function(_$ToDashboardNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToDashboardNavigationEvent));

  @override
  _$ToDashboardNavigationEvent get _value =>
      super._value as _$ToDashboardNavigationEvent;
}

/// @nodoc

class _$ToDashboardNavigationEvent extends ToDashboardNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToDashboardNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toDashboard()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toDashboard'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToDashboardNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toDashboard();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toDashboard?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDashboard != null) {
      return toDashboard();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toDashboard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toDashboard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDashboard != null) {
      return toDashboard(this);
    }
    return orElse();
  }
}

abstract class ToDashboardNavigationEvent extends NavigationEvent {
  const factory ToDashboardNavigationEvent() = _$ToDashboardNavigationEvent;
  const ToDashboardNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToBillingsNavigationEventCopyWith<$Res> {
  factory _$$ToBillingsNavigationEventCopyWith(
          _$ToBillingsNavigationEvent value,
          $Res Function(_$ToBillingsNavigationEvent) then) =
      __$$ToBillingsNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToBillingsNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToBillingsNavigationEventCopyWith<$Res> {
  __$$ToBillingsNavigationEventCopyWithImpl(_$ToBillingsNavigationEvent _value,
      $Res Function(_$ToBillingsNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToBillingsNavigationEvent));

  @override
  _$ToBillingsNavigationEvent get _value =>
      super._value as _$ToBillingsNavigationEvent;
}

/// @nodoc

class _$ToBillingsNavigationEvent extends ToBillingsNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToBillingsNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toBillings()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toBillings'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToBillingsNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toBillings();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toBillings?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toBillings != null) {
      return toBillings();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toBillings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toBillings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toBillings != null) {
      return toBillings(this);
    }
    return orElse();
  }
}

abstract class ToBillingsNavigationEvent extends NavigationEvent {
  const factory ToBillingsNavigationEvent() = _$ToBillingsNavigationEvent;
  const ToBillingsNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToMyWidgetsNavigationEventCopyWith<$Res> {
  factory _$$ToMyWidgetsNavigationEventCopyWith(
          _$ToMyWidgetsNavigationEvent value,
          $Res Function(_$ToMyWidgetsNavigationEvent) then) =
      __$$ToMyWidgetsNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToMyWidgetsNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToMyWidgetsNavigationEventCopyWith<$Res> {
  __$$ToMyWidgetsNavigationEventCopyWithImpl(
      _$ToMyWidgetsNavigationEvent _value,
      $Res Function(_$ToMyWidgetsNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToMyWidgetsNavigationEvent));

  @override
  _$ToMyWidgetsNavigationEvent get _value =>
      super._value as _$ToMyWidgetsNavigationEvent;
}

/// @nodoc

class _$ToMyWidgetsNavigationEvent extends ToMyWidgetsNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToMyWidgetsNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toMyWidgets()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toMyWidgets'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToMyWidgetsNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toMyWidgets();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toMyWidgets?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toMyWidgets != null) {
      return toMyWidgets();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toMyWidgets(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toMyWidgets?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toMyWidgets != null) {
      return toMyWidgets(this);
    }
    return orElse();
  }
}

abstract class ToMyWidgetsNavigationEvent extends NavigationEvent {
  const factory ToMyWidgetsNavigationEvent() = _$ToMyWidgetsNavigationEvent;
  const ToMyWidgetsNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToTestLauncherEventCopyWith<$Res> {
  factory _$$ToTestLauncherEventCopyWith(_$ToTestLauncherEvent value,
          $Res Function(_$ToTestLauncherEvent) then) =
      __$$ToTestLauncherEventCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ToTestLauncherEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToTestLauncherEventCopyWith<$Res> {
  __$$ToTestLauncherEventCopyWithImpl(
      _$ToTestLauncherEvent _value, $Res Function(_$ToTestLauncherEvent) _then)
      : super(_value, (v) => _then(v as _$ToTestLauncherEvent));

  @override
  _$ToTestLauncherEvent get _value => super._value as _$ToTestLauncherEvent;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$ToTestLauncherEvent(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ToTestLauncherEvent extends ToTestLauncherEvent
    with DiagnosticableTreeMixin {
  const _$ToTestLauncherEvent(this.product) : super._();

  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toTestLauncher(product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationEvent.toTestLauncher'))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToTestLauncherEvent &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ToTestLauncherEventCopyWith<_$ToTestLauncherEvent> get copyWith =>
      __$$ToTestLauncherEventCopyWithImpl<_$ToTestLauncherEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toTestLauncher(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toTestLauncher?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toTestLauncher != null) {
      return toTestLauncher(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toTestLauncher(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toTestLauncher?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toTestLauncher != null) {
      return toTestLauncher(this);
    }
    return orElse();
  }
}

abstract class ToTestLauncherEvent extends NavigationEvent {
  const factory ToTestLauncherEvent(final Product product) =
      _$ToTestLauncherEvent;
  const ToTestLauncherEvent._() : super._();

  Product get product;
  @JsonKey(ignore: true)
  _$$ToTestLauncherEventCopyWith<_$ToTestLauncherEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToAddedProductsEventCopyWith<$Res> {
  factory _$$ToAddedProductsEventCopyWith(_$ToAddedProductsEvent value,
          $Res Function(_$ToAddedProductsEvent) then) =
      __$$ToAddedProductsEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToAddedProductsEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToAddedProductsEventCopyWith<$Res> {
  __$$ToAddedProductsEventCopyWithImpl(_$ToAddedProductsEvent _value,
      $Res Function(_$ToAddedProductsEvent) _then)
      : super(_value, (v) => _then(v as _$ToAddedProductsEvent));

  @override
  _$ToAddedProductsEvent get _value => super._value as _$ToAddedProductsEvent;
}

/// @nodoc

class _$ToAddedProductsEvent extends ToAddedProductsEvent
    with DiagnosticableTreeMixin {
  const _$ToAddedProductsEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toAddedProducts()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toAddedProducts'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ToAddedProductsEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toAddedProducts();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toAddedProducts?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAddedProducts != null) {
      return toAddedProducts();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toAddedProducts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toAddedProducts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAddedProducts != null) {
      return toAddedProducts(this);
    }
    return orElse();
  }
}

abstract class ToAddedProductsEvent extends NavigationEvent {
  const factory ToAddedProductsEvent() = _$ToAddedProductsEvent;
  const ToAddedProductsEvent._() : super._();
}

/// @nodoc
abstract class _$$ToEditProductNavigationEventCopyWith<$Res> {
  factory _$$ToEditProductNavigationEventCopyWith(
          _$ToEditProductNavigationEvent value,
          $Res Function(_$ToEditProductNavigationEvent) then) =
      __$$ToEditProductNavigationEventCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ToEditProductNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToEditProductNavigationEventCopyWith<$Res> {
  __$$ToEditProductNavigationEventCopyWithImpl(
      _$ToEditProductNavigationEvent _value,
      $Res Function(_$ToEditProductNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToEditProductNavigationEvent));

  @override
  _$ToEditProductNavigationEvent get _value =>
      super._value as _$ToEditProductNavigationEvent;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$ToEditProductNavigationEvent(
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ToEditProductNavigationEvent extends ToEditProductNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToEditProductNavigationEvent(this.product) : super._();

  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toEditProduct(product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationEvent.toEditProduct'))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToEditProductNavigationEvent &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ToEditProductNavigationEventCopyWith<_$ToEditProductNavigationEvent>
      get copyWith => __$$ToEditProductNavigationEventCopyWithImpl<
          _$ToEditProductNavigationEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toEditProduct(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toEditProduct?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toEditProduct != null) {
      return toEditProduct(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toEditProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toEditProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toEditProduct != null) {
      return toEditProduct(this);
    }
    return orElse();
  }
}

abstract class ToEditProductNavigationEvent extends NavigationEvent {
  const factory ToEditProductNavigationEvent(final Product product) =
      _$ToEditProductNavigationEvent;
  const ToEditProductNavigationEvent._() : super._();

  Product get product;
  @JsonKey(ignore: true)
  _$$ToEditProductNavigationEventCopyWith<_$ToEditProductNavigationEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ToAddProductNavigationEventCopyWith<$Res> {
  factory _$$ToAddProductNavigationEventCopyWith(
          _$ToAddProductNavigationEvent value,
          $Res Function(_$ToAddProductNavigationEvent) then) =
      __$$ToAddProductNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToAddProductNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToAddProductNavigationEventCopyWith<$Res> {
  __$$ToAddProductNavigationEventCopyWithImpl(
      _$ToAddProductNavigationEvent _value,
      $Res Function(_$ToAddProductNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToAddProductNavigationEvent));

  @override
  _$ToAddProductNavigationEvent get _value =>
      super._value as _$ToAddProductNavigationEvent;
}

/// @nodoc

class _$ToAddProductNavigationEvent extends ToAddProductNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToAddProductNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toAddProduct()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toAddProduct'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToAddProductNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toAddProduct();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toAddProduct?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAddProduct != null) {
      return toAddProduct();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toAddProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toAddProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAddProduct != null) {
      return toAddProduct(this);
    }
    return orElse();
  }
}

abstract class ToAddProductNavigationEvent extends NavigationEvent {
  const factory ToAddProductNavigationEvent() = _$ToAddProductNavigationEvent;
  const ToAddProductNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToAdminNavigationEventCopyWith<$Res> {
  factory _$$ToAdminNavigationEventCopyWith(_$ToAdminNavigationEvent value,
          $Res Function(_$ToAdminNavigationEvent) then) =
      __$$ToAdminNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToAdminNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToAdminNavigationEventCopyWith<$Res> {
  __$$ToAdminNavigationEventCopyWithImpl(_$ToAdminNavigationEvent _value,
      $Res Function(_$ToAdminNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToAdminNavigationEvent));

  @override
  _$ToAdminNavigationEvent get _value =>
      super._value as _$ToAdminNavigationEvent;
}

/// @nodoc

class _$ToAdminNavigationEvent extends ToAdminNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToAdminNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toAdmin()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toAdmin'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ToAdminNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toAdmin();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toAdmin?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAdmin != null) {
      return toAdmin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toAdmin(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toAdmin?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toAdmin != null) {
      return toAdmin(this);
    }
    return orElse();
  }
}

abstract class ToAdminNavigationEvent extends NavigationEvent {
  const factory ToAdminNavigationEvent() = _$ToAdminNavigationEvent;
  const ToAdminNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToProfileNavigationEventCopyWith<$Res> {
  factory _$$ToProfileNavigationEventCopyWith(_$ToProfileNavigationEvent value,
          $Res Function(_$ToProfileNavigationEvent) then) =
      __$$ToProfileNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToProfileNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToProfileNavigationEventCopyWith<$Res> {
  __$$ToProfileNavigationEventCopyWithImpl(_$ToProfileNavigationEvent _value,
      $Res Function(_$ToProfileNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToProfileNavigationEvent));

  @override
  _$ToProfileNavigationEvent get _value =>
      super._value as _$ToProfileNavigationEvent;
}

/// @nodoc

class _$ToProfileNavigationEvent extends ToProfileNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToProfileNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toProfile()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toProfile'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToProfileNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toProfile();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toProfile?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProfile != null) {
      return toProfile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toProfile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toProfile != null) {
      return toProfile(this);
    }
    return orElse();
  }
}

abstract class ToProfileNavigationEvent extends NavigationEvent {
  const factory ToProfileNavigationEvent() = _$ToProfileNavigationEvent;
  const ToProfileNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToLoginNavigationEventCopyWith<$Res> {
  factory _$$ToLoginNavigationEventCopyWith(_$ToLoginNavigationEvent value,
          $Res Function(_$ToLoginNavigationEvent) then) =
      __$$ToLoginNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToLoginNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToLoginNavigationEventCopyWith<$Res> {
  __$$ToLoginNavigationEventCopyWithImpl(_$ToLoginNavigationEvent _value,
      $Res Function(_$ToLoginNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToLoginNavigationEvent));

  @override
  _$ToLoginNavigationEvent get _value =>
      super._value as _$ToLoginNavigationEvent;
}

/// @nodoc

class _$ToLoginNavigationEvent extends ToLoginNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToLoginNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toLogin()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'NavigationEvent.toLogin'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ToLoginNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toLogin();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toLogin?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toLogin != null) {
      return toLogin();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toLogin(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toLogin?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toLogin != null) {
      return toLogin(this);
    }
    return orElse();
  }
}

abstract class ToLoginNavigationEvent extends NavigationEvent {
  const factory ToLoginNavigationEvent() = _$ToLoginNavigationEvent;
  const ToLoginNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToCoinDepositNavigationEventCopyWith<$Res> {
  factory _$$ToCoinDepositNavigationEventCopyWith(
          _$ToCoinDepositNavigationEvent value,
          $Res Function(_$ToCoinDepositNavigationEvent) then) =
      __$$ToCoinDepositNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToCoinDepositNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToCoinDepositNavigationEventCopyWith<$Res> {
  __$$ToCoinDepositNavigationEventCopyWithImpl(
      _$ToCoinDepositNavigationEvent _value,
      $Res Function(_$ToCoinDepositNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToCoinDepositNavigationEvent));

  @override
  _$ToCoinDepositNavigationEvent get _value =>
      super._value as _$ToCoinDepositNavigationEvent;
}

/// @nodoc

class _$ToCoinDepositNavigationEvent extends ToCoinDepositNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToCoinDepositNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toCoinDeposit()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toCoinDeposit'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToCoinDepositNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toCoinDeposit();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toCoinDeposit?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toCoinDeposit != null) {
      return toCoinDeposit();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toCoinDeposit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toCoinDeposit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toCoinDeposit != null) {
      return toCoinDeposit(this);
    }
    return orElse();
  }
}

abstract class ToCoinDepositNavigationEvent extends NavigationEvent {
  const factory ToCoinDepositNavigationEvent() = _$ToCoinDepositNavigationEvent;
  const ToCoinDepositNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToStripeDepositNavigationEventCopyWith<$Res> {
  factory _$$ToStripeDepositNavigationEventCopyWith(
          _$ToStripeDepositNavigationEvent value,
          $Res Function(_$ToStripeDepositNavigationEvent) then) =
      __$$ToStripeDepositNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToStripeDepositNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToStripeDepositNavigationEventCopyWith<$Res> {
  __$$ToStripeDepositNavigationEventCopyWithImpl(
      _$ToStripeDepositNavigationEvent _value,
      $Res Function(_$ToStripeDepositNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToStripeDepositNavigationEvent));

  @override
  _$ToStripeDepositNavigationEvent get _value =>
      super._value as _$ToStripeDepositNavigationEvent;
}

/// @nodoc

class _$ToStripeDepositNavigationEvent extends ToStripeDepositNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToStripeDepositNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toStripeDeposit()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toStripeDeposit'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToStripeDepositNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toStripeDeposit();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toStripeDeposit?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toStripeDeposit != null) {
      return toStripeDeposit();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toStripeDeposit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toStripeDeposit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toStripeDeposit != null) {
      return toStripeDeposit(this);
    }
    return orElse();
  }
}

abstract class ToStripeDepositNavigationEvent extends NavigationEvent {
  const factory ToStripeDepositNavigationEvent() =
      _$ToStripeDepositNavigationEvent;
  const ToStripeDepositNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToDepositSuccessNavigationEventCopyWith<$Res> {
  factory _$$ToDepositSuccessNavigationEventCopyWith(
          _$ToDepositSuccessNavigationEvent value,
          $Res Function(_$ToDepositSuccessNavigationEvent) then) =
      __$$ToDepositSuccessNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToDepositSuccessNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToDepositSuccessNavigationEventCopyWith<$Res> {
  __$$ToDepositSuccessNavigationEventCopyWithImpl(
      _$ToDepositSuccessNavigationEvent _value,
      $Res Function(_$ToDepositSuccessNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToDepositSuccessNavigationEvent));

  @override
  _$ToDepositSuccessNavigationEvent get _value =>
      super._value as _$ToDepositSuccessNavigationEvent;
}

/// @nodoc

class _$ToDepositSuccessNavigationEvent extends ToDepositSuccessNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToDepositSuccessNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toDepositSuccess()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toDepositSuccess'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToDepositSuccessNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toDepositSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toDepositSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDepositSuccess != null) {
      return toDepositSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toDepositSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toDepositSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDepositSuccess != null) {
      return toDepositSuccess(this);
    }
    return orElse();
  }
}

abstract class ToDepositSuccessNavigationEvent extends NavigationEvent {
  const factory ToDepositSuccessNavigationEvent() =
      _$ToDepositSuccessNavigationEvent;
  const ToDepositSuccessNavigationEvent._() : super._();
}

/// @nodoc
abstract class _$$ToDepositFailedNavigationEventCopyWith<$Res> {
  factory _$$ToDepositFailedNavigationEventCopyWith(
          _$ToDepositFailedNavigationEvent value,
          $Res Function(_$ToDepositFailedNavigationEvent) then) =
      __$$ToDepositFailedNavigationEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ToDepositFailedNavigationEventCopyWithImpl<$Res>
    extends _$NavigationEventCopyWithImpl<$Res>
    implements _$$ToDepositFailedNavigationEventCopyWith<$Res> {
  __$$ToDepositFailedNavigationEventCopyWithImpl(
      _$ToDepositFailedNavigationEvent _value,
      $Res Function(_$ToDepositFailedNavigationEvent) _then)
      : super(_value, (v) => _then(v as _$ToDepositFailedNavigationEvent));

  @override
  _$ToDepositFailedNavigationEvent get _value =>
      super._value as _$ToDepositFailedNavigationEvent;
}

/// @nodoc

class _$ToDepositFailedNavigationEvent extends ToDepositFailedNavigationEvent
    with DiagnosticableTreeMixin {
  const _$ToDepositFailedNavigationEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationEvent.toDepositFailed()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
        .add(DiagnosticsProperty('type', 'NavigationEvent.toDepositFailed'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ToDepositFailedNavigationEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() toMarketplace,
    required TResult Function(String productId) toProductDetails,
    required TResult Function(String productId, Product product)
        toProductDetailsUse,
    required TResult Function(ProductCategoryType categoryType) toCategory,
    required TResult Function() toDashboard,
    required TResult Function() toBillings,
    required TResult Function() toMyWidgets,
    required TResult Function(Product product) toTestLauncher,
    required TResult Function() toAddedProducts,
    required TResult Function(Product product) toEditProduct,
    required TResult Function() toAddProduct,
    required TResult Function() toAdmin,
    required TResult Function() toProfile,
    required TResult Function() toLogin,
    required TResult Function() toCoinDeposit,
    required TResult Function() toStripeDeposit,
    required TResult Function() toDepositSuccess,
    required TResult Function() toDepositFailed,
  }) {
    return toDepositFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
  }) {
    return toDepositFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? toMarketplace,
    TResult Function(String productId)? toProductDetails,
    TResult Function(String productId, Product product)? toProductDetailsUse,
    TResult Function(ProductCategoryType categoryType)? toCategory,
    TResult Function()? toDashboard,
    TResult Function()? toBillings,
    TResult Function()? toMyWidgets,
    TResult Function(Product product)? toTestLauncher,
    TResult Function()? toAddedProducts,
    TResult Function(Product product)? toEditProduct,
    TResult Function()? toAddProduct,
    TResult Function()? toAdmin,
    TResult Function()? toProfile,
    TResult Function()? toLogin,
    TResult Function()? toCoinDeposit,
    TResult Function()? toStripeDeposit,
    TResult Function()? toDepositSuccess,
    TResult Function()? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDepositFailed != null) {
      return toDepositFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ToMarketplaceNavigationEvent value) toMarketplace,
    required TResult Function(ToProductDetailsNavigationEvent value)
        toProductDetails,
    required TResult Function(ToProductDetailsUseNavigationEvent value)
        toProductDetailsUse,
    required TResult Function(ToCategoryNavigationEvent value) toCategory,
    required TResult Function(ToDashboardNavigationEvent value) toDashboard,
    required TResult Function(ToBillingsNavigationEvent value) toBillings,
    required TResult Function(ToMyWidgetsNavigationEvent value) toMyWidgets,
    required TResult Function(ToTestLauncherEvent value) toTestLauncher,
    required TResult Function(ToAddedProductsEvent value) toAddedProducts,
    required TResult Function(ToEditProductNavigationEvent value) toEditProduct,
    required TResult Function(ToAddProductNavigationEvent value) toAddProduct,
    required TResult Function(ToAdminNavigationEvent value) toAdmin,
    required TResult Function(ToProfileNavigationEvent value) toProfile,
    required TResult Function(ToLoginNavigationEvent value) toLogin,
    required TResult Function(ToCoinDepositNavigationEvent value) toCoinDeposit,
    required TResult Function(ToStripeDepositNavigationEvent value)
        toStripeDeposit,
    required TResult Function(ToDepositSuccessNavigationEvent value)
        toDepositSuccess,
    required TResult Function(ToDepositFailedNavigationEvent value)
        toDepositFailed,
  }) {
    return toDepositFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
  }) {
    return toDepositFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ToMarketplaceNavigationEvent value)? toMarketplace,
    TResult Function(ToProductDetailsNavigationEvent value)? toProductDetails,
    TResult Function(ToProductDetailsUseNavigationEvent value)?
        toProductDetailsUse,
    TResult Function(ToCategoryNavigationEvent value)? toCategory,
    TResult Function(ToDashboardNavigationEvent value)? toDashboard,
    TResult Function(ToBillingsNavigationEvent value)? toBillings,
    TResult Function(ToMyWidgetsNavigationEvent value)? toMyWidgets,
    TResult Function(ToTestLauncherEvent value)? toTestLauncher,
    TResult Function(ToAddedProductsEvent value)? toAddedProducts,
    TResult Function(ToEditProductNavigationEvent value)? toEditProduct,
    TResult Function(ToAddProductNavigationEvent value)? toAddProduct,
    TResult Function(ToAdminNavigationEvent value)? toAdmin,
    TResult Function(ToProfileNavigationEvent value)? toProfile,
    TResult Function(ToLoginNavigationEvent value)? toLogin,
    TResult Function(ToCoinDepositNavigationEvent value)? toCoinDeposit,
    TResult Function(ToStripeDepositNavigationEvent value)? toStripeDeposit,
    TResult Function(ToDepositSuccessNavigationEvent value)? toDepositSuccess,
    TResult Function(ToDepositFailedNavigationEvent value)? toDepositFailed,
    required TResult orElse(),
  }) {
    if (toDepositFailed != null) {
      return toDepositFailed(this);
    }
    return orElse();
  }
}

abstract class ToDepositFailedNavigationEvent extends NavigationEvent {
  const factory ToDepositFailedNavigationEvent() =
      _$ToDepositFailedNavigationEvent;
  const ToDepositFailedNavigationEvent._() : super._();
}

/// @nodoc
mixin _$NavigationState {
  OrchestraRoutePath get routePath => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $NavigationStateCopyWith<NavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NavigationStateCopyWith<$Res> {
  factory $NavigationStateCopyWith(
          NavigationState value, $Res Function(NavigationState) then) =
      _$NavigationStateCopyWithImpl<$Res>;
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class _$NavigationStateCopyWithImpl<$Res>
    implements $NavigationStateCopyWith<$Res> {
  _$NavigationStateCopyWithImpl(this._value, this._then);

  final NavigationState _value;
  // ignore: unused_field
  final $Res Function(NavigationState) _then;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_value.copyWith(
      routePath: routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc
abstract class _$$InitialNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$InitialNavigationStateCopyWith(_$InitialNavigationState value,
          $Res Function(_$InitialNavigationState) then) =
      __$$InitialNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$InitialNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$InitialNavigationStateCopyWith<$Res> {
  __$$InitialNavigationStateCopyWithImpl(_$InitialNavigationState _value,
      $Res Function(_$InitialNavigationState) _then)
      : super(_value, (v) => _then(v as _$InitialNavigationState));

  @override
  _$InitialNavigationState get _value =>
      super._value as _$InitialNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$InitialNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$InitialNavigationState extends InitialNavigationState
    with DiagnosticableTreeMixin {
  const _$InitialNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.initial(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.initial'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$InitialNavigationStateCopyWith<_$InitialNavigationState> get copyWith =>
      __$$InitialNavigationStateCopyWithImpl<_$InitialNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return initial(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return initial?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialNavigationState extends NavigationState {
  const factory InitialNavigationState(final OrchestraRoutePath routePath) =
      _$InitialNavigationState;
  const InitialNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$InitialNavigationStateCopyWith<_$InitialNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MarketplaceNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$MarketplaceNavigationStateCopyWith(
          _$MarketplaceNavigationState value,
          $Res Function(_$MarketplaceNavigationState) then) =
      __$$MarketplaceNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$MarketplaceNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$MarketplaceNavigationStateCopyWith<$Res> {
  __$$MarketplaceNavigationStateCopyWithImpl(
      _$MarketplaceNavigationState _value,
      $Res Function(_$MarketplaceNavigationState) _then)
      : super(_value, (v) => _then(v as _$MarketplaceNavigationState));

  @override
  _$MarketplaceNavigationState get _value =>
      super._value as _$MarketplaceNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$MarketplaceNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$MarketplaceNavigationState extends MarketplaceNavigationState
    with DiagnosticableTreeMixin {
  const _$MarketplaceNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.marketplace(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.marketplace'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MarketplaceNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$MarketplaceNavigationStateCopyWith<_$MarketplaceNavigationState>
      get copyWith => __$$MarketplaceNavigationStateCopyWithImpl<
          _$MarketplaceNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return marketplace(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return marketplace?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (marketplace != null) {
      return marketplace(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return marketplace(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return marketplace?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (marketplace != null) {
      return marketplace(this);
    }
    return orElse();
  }
}

abstract class MarketplaceNavigationState extends NavigationState {
  const factory MarketplaceNavigationState(final OrchestraRoutePath routePath) =
      _$MarketplaceNavigationState;
  const MarketplaceNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$MarketplaceNavigationStateCopyWith<_$MarketplaceNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductDetailsNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$ProductDetailsNavigationStateCopyWith(
          _$ProductDetailsNavigationState value,
          $Res Function(_$ProductDetailsNavigationState) then) =
      __$$ProductDetailsNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath, String productId});
}

/// @nodoc
class __$$ProductDetailsNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$ProductDetailsNavigationStateCopyWith<$Res> {
  __$$ProductDetailsNavigationStateCopyWithImpl(
      _$ProductDetailsNavigationState _value,
      $Res Function(_$ProductDetailsNavigationState) _then)
      : super(_value, (v) => _then(v as _$ProductDetailsNavigationState));

  @override
  _$ProductDetailsNavigationState get _value =>
      super._value as _$ProductDetailsNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
    Object? productId = freezed,
  }) {
    return _then(_$ProductDetailsNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
      productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ProductDetailsNavigationState extends ProductDetailsNavigationState
    with DiagnosticableTreeMixin {
  const _$ProductDetailsNavigationState(this.routePath, this.productId)
      : super._();

  @override
  final OrchestraRoutePath routePath;
  @override
  final String productId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.productDetails(routePath: $routePath, productId: $productId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.productDetails'))
      ..add(DiagnosticsProperty('routePath', routePath))
      ..add(DiagnosticsProperty('productId', productId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductDetailsNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath) &&
            const DeepCollectionEquality().equals(other.productId, productId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(routePath),
      const DeepCollectionEquality().hash(productId));

  @JsonKey(ignore: true)
  @override
  _$$ProductDetailsNavigationStateCopyWith<_$ProductDetailsNavigationState>
      get copyWith => __$$ProductDetailsNavigationStateCopyWithImpl<
          _$ProductDetailsNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return productDetails(routePath, productId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return productDetails?.call(routePath, productId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (productDetails != null) {
      return productDetails(routePath, productId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return productDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return productDetails?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (productDetails != null) {
      return productDetails(this);
    }
    return orElse();
  }
}

abstract class ProductDetailsNavigationState extends NavigationState {
  const factory ProductDetailsNavigationState(
          final OrchestraRoutePath routePath, final String productId) =
      _$ProductDetailsNavigationState;
  const ProductDetailsNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  String get productId;
  @override
  @JsonKey(ignore: true)
  _$$ProductDetailsNavigationStateCopyWith<_$ProductDetailsNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProductDetailsUseNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$ProductDetailsUseNavigationStateCopyWith(
          _$ProductDetailsUseNavigationState value,
          $Res Function(_$ProductDetailsUseNavigationState) then) =
      __$$ProductDetailsUseNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath, String productId, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$ProductDetailsUseNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$ProductDetailsUseNavigationStateCopyWith<$Res> {
  __$$ProductDetailsUseNavigationStateCopyWithImpl(
      _$ProductDetailsUseNavigationState _value,
      $Res Function(_$ProductDetailsUseNavigationState) _then)
      : super(_value, (v) => _then(v as _$ProductDetailsUseNavigationState));

  @override
  _$ProductDetailsUseNavigationState get _value =>
      super._value as _$ProductDetailsUseNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
    Object? productId = freezed,
    Object? product = freezed,
  }) {
    return _then(_$ProductDetailsUseNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
      productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$ProductDetailsUseNavigationState
    extends ProductDetailsUseNavigationState with DiagnosticableTreeMixin {
  const _$ProductDetailsUseNavigationState(
      this.routePath, this.productId, this.product)
      : super._();

  @override
  final OrchestraRoutePath routePath;
  @override
  final String productId;
  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.productDetailsUse(routePath: $routePath, productId: $productId, product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.productDetailsUse'))
      ..add(DiagnosticsProperty('routePath', routePath))
      ..add(DiagnosticsProperty('productId', productId))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProductDetailsUseNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath) &&
            const DeepCollectionEquality().equals(other.productId, productId) &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(routePath),
      const DeepCollectionEquality().hash(productId),
      const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$ProductDetailsUseNavigationStateCopyWith<
          _$ProductDetailsUseNavigationState>
      get copyWith => __$$ProductDetailsUseNavigationStateCopyWithImpl<
          _$ProductDetailsUseNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return productDetailsUse(routePath, productId, product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return productDetailsUse?.call(routePath, productId, product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (productDetailsUse != null) {
      return productDetailsUse(routePath, productId, product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return productDetailsUse(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return productDetailsUse?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (productDetailsUse != null) {
      return productDetailsUse(this);
    }
    return orElse();
  }
}

abstract class ProductDetailsUseNavigationState extends NavigationState {
  const factory ProductDetailsUseNavigationState(
      final OrchestraRoutePath routePath,
      final String productId,
      final Product product) = _$ProductDetailsUseNavigationState;
  const ProductDetailsUseNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  String get productId;
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$ProductDetailsUseNavigationStateCopyWith<
          _$ProductDetailsUseNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CategoryNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$CategoryNavigationStateCopyWith(_$CategoryNavigationState value,
          $Res Function(_$CategoryNavigationState) then) =
      __$$CategoryNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath, ProductCategoryType categoryType});
}

/// @nodoc
class __$$CategoryNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$CategoryNavigationStateCopyWith<$Res> {
  __$$CategoryNavigationStateCopyWithImpl(_$CategoryNavigationState _value,
      $Res Function(_$CategoryNavigationState) _then)
      : super(_value, (v) => _then(v as _$CategoryNavigationState));

  @override
  _$CategoryNavigationState get _value =>
      super._value as _$CategoryNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
    Object? categoryType = freezed,
  }) {
    return _then(_$CategoryNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
      categoryType == freezed
          ? _value.categoryType
          : categoryType // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
    ));
  }
}

/// @nodoc

class _$CategoryNavigationState extends CategoryNavigationState
    with DiagnosticableTreeMixin {
  const _$CategoryNavigationState(this.routePath, this.categoryType)
      : super._();

  @override
  final OrchestraRoutePath routePath;
  @override
  final ProductCategoryType categoryType;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.category(routePath: $routePath, categoryType: $categoryType)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.category'))
      ..add(DiagnosticsProperty('routePath', routePath))
      ..add(DiagnosticsProperty('categoryType', categoryType));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CategoryNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath) &&
            const DeepCollectionEquality()
                .equals(other.categoryType, categoryType));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(routePath),
      const DeepCollectionEquality().hash(categoryType));

  @JsonKey(ignore: true)
  @override
  _$$CategoryNavigationStateCopyWith<_$CategoryNavigationState> get copyWith =>
      __$$CategoryNavigationStateCopyWithImpl<_$CategoryNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return category(routePath, categoryType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return category?.call(routePath, categoryType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (category != null) {
      return category(routePath, categoryType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return category(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return category?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (category != null) {
      return category(this);
    }
    return orElse();
  }
}

abstract class CategoryNavigationState extends NavigationState {
  const factory CategoryNavigationState(final OrchestraRoutePath routePath,
      final ProductCategoryType categoryType) = _$CategoryNavigationState;
  const CategoryNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  ProductCategoryType get categoryType;
  @override
  @JsonKey(ignore: true)
  _$$CategoryNavigationStateCopyWith<_$CategoryNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DashboardNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$DashboardNavigationStateCopyWith(_$DashboardNavigationState value,
          $Res Function(_$DashboardNavigationState) then) =
      __$$DashboardNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$DashboardNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$DashboardNavigationStateCopyWith<$Res> {
  __$$DashboardNavigationStateCopyWithImpl(_$DashboardNavigationState _value,
      $Res Function(_$DashboardNavigationState) _then)
      : super(_value, (v) => _then(v as _$DashboardNavigationState));

  @override
  _$DashboardNavigationState get _value =>
      super._value as _$DashboardNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$DashboardNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$DashboardNavigationState extends DashboardNavigationState
    with DiagnosticableTreeMixin {
  const _$DashboardNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.dashboard(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.dashboard'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashboardNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$DashboardNavigationStateCopyWith<_$DashboardNavigationState>
      get copyWith =>
          __$$DashboardNavigationStateCopyWithImpl<_$DashboardNavigationState>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return dashboard(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return dashboard?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (dashboard != null) {
      return dashboard(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return dashboard(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return dashboard?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (dashboard != null) {
      return dashboard(this);
    }
    return orElse();
  }
}

abstract class DashboardNavigationState extends NavigationState {
  const factory DashboardNavigationState(final OrchestraRoutePath routePath) =
      _$DashboardNavigationState;
  const DashboardNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$DashboardNavigationStateCopyWith<_$DashboardNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$BillingsNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$BillingsNavigationStateCopyWith(_$BillingsNavigationState value,
          $Res Function(_$BillingsNavigationState) then) =
      __$$BillingsNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$BillingsNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$BillingsNavigationStateCopyWith<$Res> {
  __$$BillingsNavigationStateCopyWithImpl(_$BillingsNavigationState _value,
      $Res Function(_$BillingsNavigationState) _then)
      : super(_value, (v) => _then(v as _$BillingsNavigationState));

  @override
  _$BillingsNavigationState get _value =>
      super._value as _$BillingsNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$BillingsNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$BillingsNavigationState extends BillingsNavigationState
    with DiagnosticableTreeMixin {
  const _$BillingsNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.billings(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.billings'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BillingsNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$BillingsNavigationStateCopyWith<_$BillingsNavigationState> get copyWith =>
      __$$BillingsNavigationStateCopyWithImpl<_$BillingsNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return billings(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return billings?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (billings != null) {
      return billings(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return billings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return billings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (billings != null) {
      return billings(this);
    }
    return orElse();
  }
}

abstract class BillingsNavigationState extends NavigationState {
  const factory BillingsNavigationState(final OrchestraRoutePath routePath) =
      _$BillingsNavigationState;
  const BillingsNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$BillingsNavigationStateCopyWith<_$BillingsNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoginNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$LoginNavigationStateCopyWith(_$LoginNavigationState value,
          $Res Function(_$LoginNavigationState) then) =
      __$$LoginNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$LoginNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$LoginNavigationStateCopyWith<$Res> {
  __$$LoginNavigationStateCopyWithImpl(_$LoginNavigationState _value,
      $Res Function(_$LoginNavigationState) _then)
      : super(_value, (v) => _then(v as _$LoginNavigationState));

  @override
  _$LoginNavigationState get _value => super._value as _$LoginNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$LoginNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$LoginNavigationState extends LoginNavigationState
    with DiagnosticableTreeMixin {
  const _$LoginNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.login(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.login'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$LoginNavigationStateCopyWith<_$LoginNavigationState> get copyWith =>
      __$$LoginNavigationStateCopyWithImpl<_$LoginNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return login(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return login?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (login != null) {
      return login(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return login(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return login?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (login != null) {
      return login(this);
    }
    return orElse();
  }
}

abstract class LoginNavigationState extends NavigationState {
  const factory LoginNavigationState(final OrchestraRoutePath routePath) =
      _$LoginNavigationState;
  const LoginNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$LoginNavigationStateCopyWith<_$LoginNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ProfileNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$ProfileNavigationStateCopyWith(_$ProfileNavigationState value,
          $Res Function(_$ProfileNavigationState) then) =
      __$$ProfileNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$ProfileNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$ProfileNavigationStateCopyWith<$Res> {
  __$$ProfileNavigationStateCopyWithImpl(_$ProfileNavigationState _value,
      $Res Function(_$ProfileNavigationState) _then)
      : super(_value, (v) => _then(v as _$ProfileNavigationState));

  @override
  _$ProfileNavigationState get _value =>
      super._value as _$ProfileNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$ProfileNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$ProfileNavigationState extends ProfileNavigationState
    with DiagnosticableTreeMixin {
  const _$ProfileNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.profile(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.profile'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProfileNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$ProfileNavigationStateCopyWith<_$ProfileNavigationState> get copyWith =>
      __$$ProfileNavigationStateCopyWithImpl<_$ProfileNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return profile(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return profile?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return profile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return profile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile(this);
    }
    return orElse();
  }
}

abstract class ProfileNavigationState extends NavigationState {
  const factory ProfileNavigationState(final OrchestraRoutePath routePath) =
      _$ProfileNavigationState;
  const ProfileNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$ProfileNavigationStateCopyWith<_$ProfileNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MyWidgetsNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$MyWidgetsNavigationStateCopyWith(_$MyWidgetsNavigationState value,
          $Res Function(_$MyWidgetsNavigationState) then) =
      __$$MyWidgetsNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$MyWidgetsNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$MyWidgetsNavigationStateCopyWith<$Res> {
  __$$MyWidgetsNavigationStateCopyWithImpl(_$MyWidgetsNavigationState _value,
      $Res Function(_$MyWidgetsNavigationState) _then)
      : super(_value, (v) => _then(v as _$MyWidgetsNavigationState));

  @override
  _$MyWidgetsNavigationState get _value =>
      super._value as _$MyWidgetsNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$MyWidgetsNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$MyWidgetsNavigationState extends MyWidgetsNavigationState
    with DiagnosticableTreeMixin {
  const _$MyWidgetsNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.myWidgets(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.myWidgets'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MyWidgetsNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$MyWidgetsNavigationStateCopyWith<_$MyWidgetsNavigationState>
      get copyWith =>
          __$$MyWidgetsNavigationStateCopyWithImpl<_$MyWidgetsNavigationState>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return myWidgets(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return myWidgets?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (myWidgets != null) {
      return myWidgets(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return myWidgets(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return myWidgets?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (myWidgets != null) {
      return myWidgets(this);
    }
    return orElse();
  }
}

abstract class MyWidgetsNavigationState extends NavigationState {
  const factory MyWidgetsNavigationState(final OrchestraRoutePath routePath) =
      _$MyWidgetsNavigationState;
  const MyWidgetsNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$MyWidgetsNavigationStateCopyWith<_$MyWidgetsNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TestLauncherNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$TestLauncherNavigationStateCopyWith(
          _$TestLauncherNavigationState value,
          $Res Function(_$TestLauncherNavigationState) then) =
      __$$TestLauncherNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$TestLauncherNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$TestLauncherNavigationStateCopyWith<$Res> {
  __$$TestLauncherNavigationStateCopyWithImpl(
      _$TestLauncherNavigationState _value,
      $Res Function(_$TestLauncherNavigationState) _then)
      : super(_value, (v) => _then(v as _$TestLauncherNavigationState));

  @override
  _$TestLauncherNavigationState get _value =>
      super._value as _$TestLauncherNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
    Object? product = freezed,
  }) {
    return _then(_$TestLauncherNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$TestLauncherNavigationState extends TestLauncherNavigationState
    with DiagnosticableTreeMixin {
  const _$TestLauncherNavigationState(this.routePath, this.product) : super._();

  @override
  final OrchestraRoutePath routePath;
  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.testLauncher(routePath: $routePath, product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.testLauncher'))
      ..add(DiagnosticsProperty('routePath', routePath))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TestLauncherNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath) &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(routePath),
      const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$TestLauncherNavigationStateCopyWith<_$TestLauncherNavigationState>
      get copyWith => __$$TestLauncherNavigationStateCopyWithImpl<
          _$TestLauncherNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return testLauncher(routePath, product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return testLauncher?.call(routePath, product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (testLauncher != null) {
      return testLauncher(routePath, product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return testLauncher(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return testLauncher?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (testLauncher != null) {
      return testLauncher(this);
    }
    return orElse();
  }
}

abstract class TestLauncherNavigationState extends NavigationState {
  const factory TestLauncherNavigationState(
          final OrchestraRoutePath routePath, final Product product) =
      _$TestLauncherNavigationState;
  const TestLauncherNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$TestLauncherNavigationStateCopyWith<_$TestLauncherNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddedProductsNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$AddedProductsNavigationStateCopyWith(
          _$AddedProductsNavigationState value,
          $Res Function(_$AddedProductsNavigationState) then) =
      __$$AddedProductsNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$AddedProductsNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$AddedProductsNavigationStateCopyWith<$Res> {
  __$$AddedProductsNavigationStateCopyWithImpl(
      _$AddedProductsNavigationState _value,
      $Res Function(_$AddedProductsNavigationState) _then)
      : super(_value, (v) => _then(v as _$AddedProductsNavigationState));

  @override
  _$AddedProductsNavigationState get _value =>
      super._value as _$AddedProductsNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$AddedProductsNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$AddedProductsNavigationState extends AddedProductsNavigationState
    with DiagnosticableTreeMixin {
  const _$AddedProductsNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.addedProducts(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.addedProducts'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddedProductsNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$AddedProductsNavigationStateCopyWith<_$AddedProductsNavigationState>
      get copyWith => __$$AddedProductsNavigationStateCopyWithImpl<
          _$AddedProductsNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return addedProducts(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return addedProducts?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (addedProducts != null) {
      return addedProducts(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return addedProducts(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return addedProducts?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (addedProducts != null) {
      return addedProducts(this);
    }
    return orElse();
  }
}

abstract class AddedProductsNavigationState extends NavigationState {
  const factory AddedProductsNavigationState(
      final OrchestraRoutePath routePath) = _$AddedProductsNavigationState;
  const AddedProductsNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$AddedProductsNavigationStateCopyWith<_$AddedProductsNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EditProductNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$EditProductNavigationStateCopyWith(
          _$EditProductNavigationState value,
          $Res Function(_$EditProductNavigationState) then) =
      __$$EditProductNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$EditProductNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$EditProductNavigationStateCopyWith<$Res> {
  __$$EditProductNavigationStateCopyWithImpl(
      _$EditProductNavigationState _value,
      $Res Function(_$EditProductNavigationState) _then)
      : super(_value, (v) => _then(v as _$EditProductNavigationState));

  @override
  _$EditProductNavigationState get _value =>
      super._value as _$EditProductNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
    Object? product = freezed,
  }) {
    return _then(_$EditProductNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
      product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$EditProductNavigationState extends EditProductNavigationState
    with DiagnosticableTreeMixin {
  const _$EditProductNavigationState(this.routePath, this.product) : super._();

  @override
  final OrchestraRoutePath routePath;
  @override
  final Product product;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.editProduct(routePath: $routePath, product: $product)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.editProduct'))
      ..add(DiagnosticsProperty('routePath', routePath))
      ..add(DiagnosticsProperty('product', product));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EditProductNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath) &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(routePath),
      const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$EditProductNavigationStateCopyWith<_$EditProductNavigationState>
      get copyWith => __$$EditProductNavigationStateCopyWithImpl<
          _$EditProductNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return editProduct(routePath, product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return editProduct?.call(routePath, product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (editProduct != null) {
      return editProduct(routePath, product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return editProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return editProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (editProduct != null) {
      return editProduct(this);
    }
    return orElse();
  }
}

abstract class EditProductNavigationState extends NavigationState {
  const factory EditProductNavigationState(
          final OrchestraRoutePath routePath, final Product product) =
      _$EditProductNavigationState;
  const EditProductNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$EditProductNavigationStateCopyWith<_$EditProductNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddProductNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$AddProductNavigationStateCopyWith(
          _$AddProductNavigationState value,
          $Res Function(_$AddProductNavigationState) then) =
      __$$AddProductNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$AddProductNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$AddProductNavigationStateCopyWith<$Res> {
  __$$AddProductNavigationStateCopyWithImpl(_$AddProductNavigationState _value,
      $Res Function(_$AddProductNavigationState) _then)
      : super(_value, (v) => _then(v as _$AddProductNavigationState));

  @override
  _$AddProductNavigationState get _value =>
      super._value as _$AddProductNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$AddProductNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$AddProductNavigationState extends AddProductNavigationState
    with DiagnosticableTreeMixin {
  const _$AddProductNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.addProduct(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.addProduct'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddProductNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$AddProductNavigationStateCopyWith<_$AddProductNavigationState>
      get copyWith => __$$AddProductNavigationStateCopyWithImpl<
          _$AddProductNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return addProduct(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return addProduct?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (addProduct != null) {
      return addProduct(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return addProduct(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return addProduct?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (addProduct != null) {
      return addProduct(this);
    }
    return orElse();
  }
}

abstract class AddProductNavigationState extends NavigationState {
  const factory AddProductNavigationState(final OrchestraRoutePath routePath) =
      _$AddProductNavigationState;
  const AddProductNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$AddProductNavigationStateCopyWith<_$AddProductNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AdminNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$AdminNavigationStateCopyWith(_$AdminNavigationState value,
          $Res Function(_$AdminNavigationState) then) =
      __$$AdminNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$AdminNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$AdminNavigationStateCopyWith<$Res> {
  __$$AdminNavigationStateCopyWithImpl(_$AdminNavigationState _value,
      $Res Function(_$AdminNavigationState) _then)
      : super(_value, (v) => _then(v as _$AdminNavigationState));

  @override
  _$AdminNavigationState get _value => super._value as _$AdminNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$AdminNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$AdminNavigationState extends AdminNavigationState
    with DiagnosticableTreeMixin {
  const _$AdminNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.admin(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.admin'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AdminNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$AdminNavigationStateCopyWith<_$AdminNavigationState> get copyWith =>
      __$$AdminNavigationStateCopyWithImpl<_$AdminNavigationState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return admin(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return admin?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (admin != null) {
      return admin(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return admin(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return admin?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (admin != null) {
      return admin(this);
    }
    return orElse();
  }
}

abstract class AdminNavigationState extends NavigationState {
  const factory AdminNavigationState(final OrchestraRoutePath routePath) =
      _$AdminNavigationState;
  const AdminNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$AdminNavigationStateCopyWith<_$AdminNavigationState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CoinDepositNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$CoinDepositNavigationStateCopyWith(
          _$CoinDepositNavigationState value,
          $Res Function(_$CoinDepositNavigationState) then) =
      __$$CoinDepositNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$CoinDepositNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$CoinDepositNavigationStateCopyWith<$Res> {
  __$$CoinDepositNavigationStateCopyWithImpl(
      _$CoinDepositNavigationState _value,
      $Res Function(_$CoinDepositNavigationState) _then)
      : super(_value, (v) => _then(v as _$CoinDepositNavigationState));

  @override
  _$CoinDepositNavigationState get _value =>
      super._value as _$CoinDepositNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$CoinDepositNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$CoinDepositNavigationState extends CoinDepositNavigationState
    with DiagnosticableTreeMixin {
  const _$CoinDepositNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.coinDeposit(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.coinDeposit'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CoinDepositNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$CoinDepositNavigationStateCopyWith<_$CoinDepositNavigationState>
      get copyWith => __$$CoinDepositNavigationStateCopyWithImpl<
          _$CoinDepositNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return coinDeposit(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return coinDeposit?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (coinDeposit != null) {
      return coinDeposit(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return coinDeposit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return coinDeposit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (coinDeposit != null) {
      return coinDeposit(this);
    }
    return orElse();
  }
}

abstract class CoinDepositNavigationState extends NavigationState {
  const factory CoinDepositNavigationState(final OrchestraRoutePath routePath) =
      _$CoinDepositNavigationState;
  const CoinDepositNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$CoinDepositNavigationStateCopyWith<_$CoinDepositNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StripeDepositNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$StripeDepositNavigationStateCopyWith(
          _$StripeDepositNavigationState value,
          $Res Function(_$StripeDepositNavigationState) then) =
      __$$StripeDepositNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$StripeDepositNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$StripeDepositNavigationStateCopyWith<$Res> {
  __$$StripeDepositNavigationStateCopyWithImpl(
      _$StripeDepositNavigationState _value,
      $Res Function(_$StripeDepositNavigationState) _then)
      : super(_value, (v) => _then(v as _$StripeDepositNavigationState));

  @override
  _$StripeDepositNavigationState get _value =>
      super._value as _$StripeDepositNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$StripeDepositNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$StripeDepositNavigationState extends StripeDepositNavigationState
    with DiagnosticableTreeMixin {
  const _$StripeDepositNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.stripeDeposit(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.stripeDeposit'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StripeDepositNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$StripeDepositNavigationStateCopyWith<_$StripeDepositNavigationState>
      get copyWith => __$$StripeDepositNavigationStateCopyWithImpl<
          _$StripeDepositNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return stripeDeposit(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return stripeDeposit?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (stripeDeposit != null) {
      return stripeDeposit(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return stripeDeposit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return stripeDeposit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (stripeDeposit != null) {
      return stripeDeposit(this);
    }
    return orElse();
  }
}

abstract class StripeDepositNavigationState extends NavigationState {
  const factory StripeDepositNavigationState(
      final OrchestraRoutePath routePath) = _$StripeDepositNavigationState;
  const StripeDepositNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$StripeDepositNavigationStateCopyWith<_$StripeDepositNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DepositSuccessNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$DepositSuccessNavigationStateCopyWith(
          _$DepositSuccessNavigationState value,
          $Res Function(_$DepositSuccessNavigationState) then) =
      __$$DepositSuccessNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$DepositSuccessNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$DepositSuccessNavigationStateCopyWith<$Res> {
  __$$DepositSuccessNavigationStateCopyWithImpl(
      _$DepositSuccessNavigationState _value,
      $Res Function(_$DepositSuccessNavigationState) _then)
      : super(_value, (v) => _then(v as _$DepositSuccessNavigationState));

  @override
  _$DepositSuccessNavigationState get _value =>
      super._value as _$DepositSuccessNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$DepositSuccessNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$DepositSuccessNavigationState extends DepositSuccessNavigationState
    with DiagnosticableTreeMixin {
  const _$DepositSuccessNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.depositSuccess(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.depositSuccess'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DepositSuccessNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$DepositSuccessNavigationStateCopyWith<_$DepositSuccessNavigationState>
      get copyWith => __$$DepositSuccessNavigationStateCopyWithImpl<
          _$DepositSuccessNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return depositSuccess(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return depositSuccess?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (depositSuccess != null) {
      return depositSuccess(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return depositSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return depositSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (depositSuccess != null) {
      return depositSuccess(this);
    }
    return orElse();
  }
}

abstract class DepositSuccessNavigationState extends NavigationState {
  const factory DepositSuccessNavigationState(
      final OrchestraRoutePath routePath) = _$DepositSuccessNavigationState;
  const DepositSuccessNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$DepositSuccessNavigationStateCopyWith<_$DepositSuccessNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DepositFailedNavigationStateCopyWith<$Res>
    implements $NavigationStateCopyWith<$Res> {
  factory _$$DepositFailedNavigationStateCopyWith(
          _$DepositFailedNavigationState value,
          $Res Function(_$DepositFailedNavigationState) then) =
      __$$DepositFailedNavigationStateCopyWithImpl<$Res>;
  @override
  $Res call({OrchestraRoutePath routePath});
}

/// @nodoc
class __$$DepositFailedNavigationStateCopyWithImpl<$Res>
    extends _$NavigationStateCopyWithImpl<$Res>
    implements _$$DepositFailedNavigationStateCopyWith<$Res> {
  __$$DepositFailedNavigationStateCopyWithImpl(
      _$DepositFailedNavigationState _value,
      $Res Function(_$DepositFailedNavigationState) _then)
      : super(_value, (v) => _then(v as _$DepositFailedNavigationState));

  @override
  _$DepositFailedNavigationState get _value =>
      super._value as _$DepositFailedNavigationState;

  @override
  $Res call({
    Object? routePath = freezed,
  }) {
    return _then(_$DepositFailedNavigationState(
      routePath == freezed
          ? _value.routePath
          : routePath // ignore: cast_nullable_to_non_nullable
              as OrchestraRoutePath,
    ));
  }
}

/// @nodoc

class _$DepositFailedNavigationState extends DepositFailedNavigationState
    with DiagnosticableTreeMixin {
  const _$DepositFailedNavigationState(this.routePath) : super._();

  @override
  final OrchestraRoutePath routePath;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'NavigationState.depositFailed(routePath: $routePath)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'NavigationState.depositFailed'))
      ..add(DiagnosticsProperty('routePath', routePath));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DepositFailedNavigationState &&
            const DeepCollectionEquality().equals(other.routePath, routePath));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(routePath));

  @JsonKey(ignore: true)
  @override
  _$$DepositFailedNavigationStateCopyWith<_$DepositFailedNavigationState>
      get copyWith => __$$DepositFailedNavigationStateCopyWithImpl<
          _$DepositFailedNavigationState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(OrchestraRoutePath routePath) initial,
    required TResult Function(OrchestraRoutePath routePath) marketplace,
    required TResult Function(OrchestraRoutePath routePath, String productId)
        productDetails,
    required TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)
        productDetailsUse,
    required TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)
        category,
    required TResult Function(OrchestraRoutePath routePath) dashboard,
    required TResult Function(OrchestraRoutePath routePath) billings,
    required TResult Function(OrchestraRoutePath routePath) login,
    required TResult Function(OrchestraRoutePath routePath) profile,
    required TResult Function(OrchestraRoutePath routePath) myWidgets,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        testLauncher,
    required TResult Function(OrchestraRoutePath routePath) addedProducts,
    required TResult Function(OrchestraRoutePath routePath, Product product)
        editProduct,
    required TResult Function(OrchestraRoutePath routePath) addProduct,
    required TResult Function(OrchestraRoutePath routePath) admin,
    required TResult Function(OrchestraRoutePath routePath) coinDeposit,
    required TResult Function(OrchestraRoutePath routePath) stripeDeposit,
    required TResult Function(OrchestraRoutePath routePath) depositSuccess,
    required TResult Function(OrchestraRoutePath routePath) depositFailed,
  }) {
    return depositFailed(routePath);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
  }) {
    return depositFailed?.call(routePath);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(OrchestraRoutePath routePath)? initial,
    TResult Function(OrchestraRoutePath routePath)? marketplace,
    TResult Function(OrchestraRoutePath routePath, String productId)?
        productDetails,
    TResult Function(
            OrchestraRoutePath routePath, String productId, Product product)?
        productDetailsUse,
    TResult Function(
            OrchestraRoutePath routePath, ProductCategoryType categoryType)?
        category,
    TResult Function(OrchestraRoutePath routePath)? dashboard,
    TResult Function(OrchestraRoutePath routePath)? billings,
    TResult Function(OrchestraRoutePath routePath)? login,
    TResult Function(OrchestraRoutePath routePath)? profile,
    TResult Function(OrchestraRoutePath routePath)? myWidgets,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        testLauncher,
    TResult Function(OrchestraRoutePath routePath)? addedProducts,
    TResult Function(OrchestraRoutePath routePath, Product product)?
        editProduct,
    TResult Function(OrchestraRoutePath routePath)? addProduct,
    TResult Function(OrchestraRoutePath routePath)? admin,
    TResult Function(OrchestraRoutePath routePath)? coinDeposit,
    TResult Function(OrchestraRoutePath routePath)? stripeDeposit,
    TResult Function(OrchestraRoutePath routePath)? depositSuccess,
    TResult Function(OrchestraRoutePath routePath)? depositFailed,
    required TResult orElse(),
  }) {
    if (depositFailed != null) {
      return depositFailed(routePath);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialNavigationState value) initial,
    required TResult Function(MarketplaceNavigationState value) marketplace,
    required TResult Function(ProductDetailsNavigationState value)
        productDetails,
    required TResult Function(ProductDetailsUseNavigationState value)
        productDetailsUse,
    required TResult Function(CategoryNavigationState value) category,
    required TResult Function(DashboardNavigationState value) dashboard,
    required TResult Function(BillingsNavigationState value) billings,
    required TResult Function(LoginNavigationState value) login,
    required TResult Function(ProfileNavigationState value) profile,
    required TResult Function(MyWidgetsNavigationState value) myWidgets,
    required TResult Function(TestLauncherNavigationState value) testLauncher,
    required TResult Function(AddedProductsNavigationState value) addedProducts,
    required TResult Function(EditProductNavigationState value) editProduct,
    required TResult Function(AddProductNavigationState value) addProduct,
    required TResult Function(AdminNavigationState value) admin,
    required TResult Function(CoinDepositNavigationState value) coinDeposit,
    required TResult Function(StripeDepositNavigationState value) stripeDeposit,
    required TResult Function(DepositSuccessNavigationState value)
        depositSuccess,
    required TResult Function(DepositFailedNavigationState value) depositFailed,
  }) {
    return depositFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
  }) {
    return depositFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialNavigationState value)? initial,
    TResult Function(MarketplaceNavigationState value)? marketplace,
    TResult Function(ProductDetailsNavigationState value)? productDetails,
    TResult Function(ProductDetailsUseNavigationState value)? productDetailsUse,
    TResult Function(CategoryNavigationState value)? category,
    TResult Function(DashboardNavigationState value)? dashboard,
    TResult Function(BillingsNavigationState value)? billings,
    TResult Function(LoginNavigationState value)? login,
    TResult Function(ProfileNavigationState value)? profile,
    TResult Function(MyWidgetsNavigationState value)? myWidgets,
    TResult Function(TestLauncherNavigationState value)? testLauncher,
    TResult Function(AddedProductsNavigationState value)? addedProducts,
    TResult Function(EditProductNavigationState value)? editProduct,
    TResult Function(AddProductNavigationState value)? addProduct,
    TResult Function(AdminNavigationState value)? admin,
    TResult Function(CoinDepositNavigationState value)? coinDeposit,
    TResult Function(StripeDepositNavigationState value)? stripeDeposit,
    TResult Function(DepositSuccessNavigationState value)? depositSuccess,
    TResult Function(DepositFailedNavigationState value)? depositFailed,
    required TResult orElse(),
  }) {
    if (depositFailed != null) {
      return depositFailed(this);
    }
    return orElse();
  }
}

abstract class DepositFailedNavigationState extends NavigationState {
  const factory DepositFailedNavigationState(
      final OrchestraRoutePath routePath) = _$DepositFailedNavigationState;
  const DepositFailedNavigationState._() : super._();

  @override
  OrchestraRoutePath get routePath;
  @override
  @JsonKey(ignore: true)
  _$$DepositFailedNavigationStateCopyWith<_$DepositFailedNavigationState>
      get copyWith => throw _privateConstructorUsedError;
}
