import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:rxdart/rxdart.dart';

import 'package:orchestra/services/admin_service.dart';

part 'admin_products_event.dart';
part 'admin_products_state.dart';

class AdminProductsBloc extends Bloc<AdminProductsEvent, AdminProductsState> {
  AdminProductsBloc(
    AdminService adminService,
  )   : _adminService = adminService,
        super(AdminProductsInitial());
  final AdminService _adminService;

  // переписал поведение ивентов, чтобы последний из них сразу на исполнение
  @override
  Stream<Transition<AdminProductsEvent, AdminProductsState>> transformEvents(
      Stream<AdminProductsEvent> events,
      Stream<Transition<AdminProductsEvent, AdminProductsState>> Function(
              AdminProductsEvent)
          transitionFn) {
    if (events is SearchProductsEvent) {
      return events.switchMap(transitionFn);
    }
    return events.asyncExpand(transitionFn);
  }

  @override
  Stream<AdminProductsState> mapEventToState(
    AdminProductsEvent event,
  ) async* {
    switch (event.runtimeType) {
      case LoadAllProductsEvent:
        yield* _mapLoadAppProducts();
        break;

      case SortAllProductsEvent:
        yield* _mapSortAllProducts(
          (event as SortAllProductsEvent).getField,
          (event).ascending,
          (event).sortColumnIndex,
        );
        break;
      case SearchProductsEvent:
        yield* _mapSearchProducts((event as SearchProductsEvent).query);
        break;
      default:
    }
  }

  Stream<AdminProductsState> _mapSearchProducts(String query) async* {
    yield AdminProductsLoading();
    try {
      final result = await _adminService
          .searchProduct(query)
          .timeout(const Duration(seconds: 20));
      yield AdminProductsLoaded(products: result);
    } on TimeoutException catch (e) {
      debugPrint('AdminProductsBloc timeout error $e');
      yield AdminProductsFailure(message: e.toString());
    } catch (e) {
      debugPrint('AdminProductsBloc error $e');
      yield AdminProductsFailure(message: e.toString());
      rethrow;
    }
  }

  Stream<AdminProductsState> _mapLoadAppProducts() async* {
    yield AdminProductsLoading();
    try {
      final result = await _adminService
          .getAllProducts()
          .timeout(const Duration(seconds: 20));
      yield AdminProductsLoaded(products: result);
    } on TimeoutException catch (e) {
      debugPrint('AdminProductsBloc timeout error $e');
      yield AdminProductsFailure(message: e.toString());
    } catch (e) {
      debugPrint('AdminProductsBloc error $e');
      yield AdminProductsFailure(message: e.toString());
      rethrow;
    }
  }

  Stream<AdminProductsState> _mapSortAllProducts<T>(
    Comparable<T> Function(Product d) getField,
    bool ascending,
    int colIndex,
  ) async* {
    if (state is AdminProductsLoaded) {
      yield AdminProductsLoading();
      final _sortedProducts = (state as AdminProductsLoaded).products;
      _sortedProducts.sort((a, b) {
        final aValue = getField(a);
        final bValue = getField(b);
        return ascending
            ? Comparable.compare(aValue, bValue)
            : Comparable.compare(bValue, aValue);
      });
      yield AdminProductsLoaded(
        products: _sortedProducts,
        sortColumnIndex: colIndex,
        sortAscending: ascending,
      );
    }
  }
}
