// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_launcer_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TestLauncherEvent {
  Product get product => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Product product) create,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Product product)? create,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Product product)? create,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTestLauncherEvent value) create,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateTestLauncherEvent value)? create,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTestLauncherEvent value)? create,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $TestLauncherEventCopyWith<TestLauncherEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestLauncherEventCopyWith<$Res> {
  factory $TestLauncherEventCopyWith(
          TestLauncherEvent value, $Res Function(TestLauncherEvent) then) =
      _$TestLauncherEventCopyWithImpl<$Res>;
  $Res call({Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class _$TestLauncherEventCopyWithImpl<$Res>
    implements $TestLauncherEventCopyWith<$Res> {
  _$TestLauncherEventCopyWithImpl(this._value, this._then);

  final TestLauncherEvent _value;
  // ignore: unused_field
  final $Res Function(TestLauncherEvent) _then;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_value.copyWith(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc
abstract class _$$CreateTestLauncherEventCopyWith<$Res>
    implements $TestLauncherEventCopyWith<$Res> {
  factory _$$CreateTestLauncherEventCopyWith(_$CreateTestLauncherEvent value,
          $Res Function(_$CreateTestLauncherEvent) then) =
      __$$CreateTestLauncherEventCopyWithImpl<$Res>;
  @override
  $Res call({Product product});

  @override
  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$CreateTestLauncherEventCopyWithImpl<$Res>
    extends _$TestLauncherEventCopyWithImpl<$Res>
    implements _$$CreateTestLauncherEventCopyWith<$Res> {
  __$$CreateTestLauncherEventCopyWithImpl(_$CreateTestLauncherEvent _value,
      $Res Function(_$CreateTestLauncherEvent) _then)
      : super(_value, (v) => _then(v as _$CreateTestLauncherEvent));

  @override
  _$CreateTestLauncherEvent get _value =>
      super._value as _$CreateTestLauncherEvent;

  @override
  $Res call({
    Object? product = freezed,
  }) {
    return _then(_$CreateTestLauncherEvent(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }
}

/// @nodoc

class _$CreateTestLauncherEvent extends CreateTestLauncherEvent {
  const _$CreateTestLauncherEvent({required this.product}) : super._();

  @override
  final Product product;

  @override
  String toString() {
    return 'TestLauncherEvent.create(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTestLauncherEvent &&
            const DeepCollectionEquality().equals(other.product, product));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(product));

  @JsonKey(ignore: true)
  @override
  _$$CreateTestLauncherEventCopyWith<_$CreateTestLauncherEvent> get copyWith =>
      __$$CreateTestLauncherEventCopyWithImpl<_$CreateTestLauncherEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Product product) create,
  }) {
    return create(product);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Product product)? create,
  }) {
    return create?.call(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Product product)? create,
    required TResult orElse(),
  }) {
    if (create != null) {
      return create(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTestLauncherEvent value) create,
  }) {
    return create(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(CreateTestLauncherEvent value)? create,
  }) {
    return create?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTestLauncherEvent value)? create,
    required TResult orElse(),
  }) {
    if (create != null) {
      return create(this);
    }
    return orElse();
  }
}

abstract class CreateTestLauncherEvent extends TestLauncherEvent {
  const factory CreateTestLauncherEvent({required final Product product}) =
      _$CreateTestLauncherEvent;
  const CreateTestLauncherEvent._() : super._();

  @override
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$CreateTestLauncherEventCopyWith<_$CreateTestLauncherEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$TestLauncherState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Product product, List<SettingsModel> manifest)
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestLauncherState value) initial,
    required TResult Function(LoadingTestLauncherState value) loading,
    required TResult Function(LoadedTestLauncherState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestLauncherStateCopyWith<$Res> {
  factory $TestLauncherStateCopyWith(
          TestLauncherState value, $Res Function(TestLauncherState) then) =
      _$TestLauncherStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$TestLauncherStateCopyWithImpl<$Res>
    implements $TestLauncherStateCopyWith<$Res> {
  _$TestLauncherStateCopyWithImpl(this._value, this._then);

  final TestLauncherState _value;
  // ignore: unused_field
  final $Res Function(TestLauncherState) _then;
}

/// @nodoc
abstract class _$$InitialTestLauncherStateCopyWith<$Res> {
  factory _$$InitialTestLauncherStateCopyWith(_$InitialTestLauncherState value,
          $Res Function(_$InitialTestLauncherState) then) =
      __$$InitialTestLauncherStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialTestLauncherStateCopyWithImpl<$Res>
    extends _$TestLauncherStateCopyWithImpl<$Res>
    implements _$$InitialTestLauncherStateCopyWith<$Res> {
  __$$InitialTestLauncherStateCopyWithImpl(_$InitialTestLauncherState _value,
      $Res Function(_$InitialTestLauncherState) _then)
      : super(_value, (v) => _then(v as _$InitialTestLauncherState));

  @override
  _$InitialTestLauncherState get _value =>
      super._value as _$InitialTestLauncherState;
}

/// @nodoc

class _$InitialTestLauncherState extends InitialTestLauncherState {
  const _$InitialTestLauncherState() : super._();

  @override
  String toString() {
    return 'TestLauncherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialTestLauncherState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Product product, List<SettingsModel> manifest)
        loaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestLauncherState value) initial,
    required TResult Function(LoadingTestLauncherState value) loading,
    required TResult Function(LoadedTestLauncherState value) loaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialTestLauncherState extends TestLauncherState {
  const factory InitialTestLauncherState() = _$InitialTestLauncherState;
  const InitialTestLauncherState._() : super._();
}

/// @nodoc
abstract class _$$LoadingTestLauncherStateCopyWith<$Res> {
  factory _$$LoadingTestLauncherStateCopyWith(_$LoadingTestLauncherState value,
          $Res Function(_$LoadingTestLauncherState) then) =
      __$$LoadingTestLauncherStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingTestLauncherStateCopyWithImpl<$Res>
    extends _$TestLauncherStateCopyWithImpl<$Res>
    implements _$$LoadingTestLauncherStateCopyWith<$Res> {
  __$$LoadingTestLauncherStateCopyWithImpl(_$LoadingTestLauncherState _value,
      $Res Function(_$LoadingTestLauncherState) _then)
      : super(_value, (v) => _then(v as _$LoadingTestLauncherState));

  @override
  _$LoadingTestLauncherState get _value =>
      super._value as _$LoadingTestLauncherState;
}

/// @nodoc

class _$LoadingTestLauncherState extends LoadingTestLauncherState {
  const _$LoadingTestLauncherState() : super._();

  @override
  String toString() {
    return 'TestLauncherState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingTestLauncherState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Product product, List<SettingsModel> manifest)
        loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestLauncherState value) initial,
    required TResult Function(LoadingTestLauncherState value) loading,
    required TResult Function(LoadedTestLauncherState value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingTestLauncherState extends TestLauncherState {
  const factory LoadingTestLauncherState() = _$LoadingTestLauncherState;
  const LoadingTestLauncherState._() : super._();
}

/// @nodoc
abstract class _$$LoadedTestLauncherStateCopyWith<$Res> {
  factory _$$LoadedTestLauncherStateCopyWith(_$LoadedTestLauncherState value,
          $Res Function(_$LoadedTestLauncherState) then) =
      __$$LoadedTestLauncherStateCopyWithImpl<$Res>;
  $Res call({Product product, List<SettingsModel> manifest});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$LoadedTestLauncherStateCopyWithImpl<$Res>
    extends _$TestLauncherStateCopyWithImpl<$Res>
    implements _$$LoadedTestLauncherStateCopyWith<$Res> {
  __$$LoadedTestLauncherStateCopyWithImpl(_$LoadedTestLauncherState _value,
      $Res Function(_$LoadedTestLauncherState) _then)
      : super(_value, (v) => _then(v as _$LoadedTestLauncherState));

  @override
  _$LoadedTestLauncherState get _value =>
      super._value as _$LoadedTestLauncherState;

  @override
  $Res call({
    Object? product = freezed,
    Object? manifest = freezed,
  }) {
    return _then(_$LoadedTestLauncherState(
      product: product == freezed
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
      manifest: manifest == freezed
          ? _value._manifest
          : manifest // ignore: cast_nullable_to_non_nullable
              as List<SettingsModel>,
    ));
  }

  @override
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc

class _$LoadedTestLauncherState extends LoadedTestLauncherState {
  const _$LoadedTestLauncherState(
      {required this.product, required final List<SettingsModel> manifest})
      : _manifest = manifest,
        super._();

  @override
  final Product product;
  final List<SettingsModel> _manifest;
  @override
  List<SettingsModel> get manifest {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_manifest);
  }

  @override
  String toString() {
    return 'TestLauncherState.loaded(product: $product, manifest: $manifest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedTestLauncherState &&
            const DeepCollectionEquality().equals(other.product, product) &&
            const DeepCollectionEquality().equals(other._manifest, _manifest));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(product),
      const DeepCollectionEquality().hash(_manifest));

  @JsonKey(ignore: true)
  @override
  _$$LoadedTestLauncherStateCopyWith<_$LoadedTestLauncherState> get copyWith =>
      __$$LoadedTestLauncherStateCopyWithImpl<_$LoadedTestLauncherState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(Product product, List<SettingsModel> manifest)
        loaded,
  }) {
    return loaded(product, manifest);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
  }) {
    return loaded?.call(product, manifest);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(Product product, List<SettingsModel> manifest)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(product, manifest);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestLauncherState value) initial,
    required TResult Function(LoadingTestLauncherState value) loading,
    required TResult Function(LoadedTestLauncherState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestLauncherState value)? initial,
    TResult Function(LoadingTestLauncherState value)? loading,
    TResult Function(LoadedTestLauncherState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedTestLauncherState extends TestLauncherState {
  const factory LoadedTestLauncherState(
      {required final Product product,
      required final List<SettingsModel> manifest}) = _$LoadedTestLauncherState;
  const LoadedTestLauncherState._() : super._();

  Product get product;
  List<SettingsModel> get manifest;
  @JsonKey(ignore: true)
  _$$LoadedTestLauncherStateCopyWith<_$LoadedTestLauncherState> get copyWith =>
      throw _privateConstructorUsedError;
}
