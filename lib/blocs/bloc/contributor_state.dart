part of 'contributor_bloc.dart';

abstract class ContributorState extends Equatable {
  const ContributorState();

  @override
  List<Object> get props => [];
}

class ContributorInitial extends ContributorState {}

class ContributorUpdating extends ContributorState {}

class ContributorUpdated extends ContributorState {
  final bool isContributor;

  const ContributorUpdated({required this.isContributor});
}

class ContributorFailure extends ContributorState {
  final String message;

  const ContributorFailure({required this.message});

  @override
  bool get stringify => true;
}
