part of 'upload_image_bloc.dart';

abstract class UploadImageEvent extends Equatable {
  const UploadImageEvent();

  @override
  List<Object> get props => [];
}

class UploadImage extends UploadImageEvent {}

class UploadImageCompleted extends UploadImageEvent {
  final PlatformFile imageFile;
  final String fileName;

  const UploadImageCompleted({
    required this.imageFile,
    required this.fileName,
  });

  @override
  List<Object> get props => [imageFile, fileName];
}

class UploadLauncherFileCompleted extends UploadImageEvent {
  final String fileName;

  const UploadLauncherFileCompleted({required this.fileName});

  @override
  List<Object> get props => [fileName];
}

class ShowCurrentImage extends UploadImageEvent {}

class DeleteImage extends UploadImageEvent {
  final PhotoUrlType fieldName;

  const DeleteImage({required this.fieldName});

  @override
  List<Object> get props => [fieldName];
}

class FileUploadCanceled extends UploadImageEvent {}

class ImageUploadCanceled extends UploadImageEvent {}
