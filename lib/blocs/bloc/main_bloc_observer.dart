import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainBlocObserver extends BlocObserver {
  /// Observer для всех экземпляров BLoC & Cubit в приложении
  MainBlocObserver() : super();

/*   @override
  void onEvent(Bloc bloc, Object event) {
    if (!kReleaseMode) {
      debugPrint('event observer ${bloc.runtimeType} $event');
    }
    super.onEvent(bloc, event);
  } */

  @override
  void onTransition(Bloc bloc, Transition transition) {
    if (!kReleaseMode) {
      //  debugPrint('${bloc.runtimeType} $transition');
    }
    super.onTransition(bloc, transition);
  }
}
