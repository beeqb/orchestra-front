part of 'test_ends_after_bloc.dart';

abstract class TestEndsAfterEvent extends Equatable {
  const TestEndsAfterEvent();

  @override
  List<Object> get props => [];
}

class TestEndsAfterInit extends TestEndsAfterEvent {}

class SetTestEndsAfter extends TestEndsAfterEvent {
  final bool value;
  final int index;

  const SetTestEndsAfter({
    required this.value,
    required this.index,
  });

  @override
  List<Object> get props => [value, index];

  @override
  String toString() => 'SetTestEndsAfter(value: $value)';
}
