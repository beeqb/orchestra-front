import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/core/const.dart';
import 'package:orchestra/repository/auth_repository.dart';
import 'package:universal_html/html.dart' as html;

import '../../models/user.dart';
import '../../services/socket_service.dart';
import '../../services/user_service.dart';

abstract class AuthEvent {}

class StartGoogleAuthEvent extends AuthEvent {}

class StartFacebookAuthEvent extends AuthEvent {}

class StartLogoutAuthEvent extends AuthEvent {}

class LoggedOutAuthEvent extends AuthEvent {}

class PresentAuthEvent extends AuthEvent {
  final UserModel user;
  PresentAuthEvent(this.user);
}

class ErrorAuthEvent extends AuthEvent {}

class StartLocalAuthEvent extends AuthEvent {}

//===================================================================

abstract class AuthState {
  final UserModel currentUser;

  AuthState(this.currentUser);
}

class InitialAuthState extends AuthState {
  InitialAuthState(UserModel currentUser) : super(currentUser);
}

class NonLoggedAuthState extends AuthState {
  NonLoggedAuthState(UserModel currentUser) : super(currentUser);
}

class ProcessingAuthState extends AuthState {
  ProcessingAuthState(UserModel currentUser) : super(currentUser);
}

class LoggedAuthState extends AuthState {
  LoggedAuthState(UserModel currentUser) : super(currentUser);
  @override
  String toString() => 'LoggedAuthState(currentUser: $currentUser)';
}

class AuthStateFailure extends AuthState {
  final String message;
  AuthStateFailure(UserModel currentUser, {required this.message})
      : super(currentUser);

  @override
  String toString() => 'AuthStateFailure(message: $message)';
}

//=====================================================================
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final SocketService _socketService;
  final UserService _userService;

  AuthBloc({
    required SocketService socketService,
    required UserService userService,
  })  : _socketService = socketService,
        _userService = userService,
        super(InitialAuthState(UserModel.empty));

  LoginType? currentLoginType;

  //TODO проверить работу сокет сервиса, где-то не создаётся соединение
  void _loginUser(UserModel user) async {
    try {
      if (user.id == defaultUserId) {
        add(LoggedOutAuthEvent());
      } else {
        if (_socketService.socket == null) {
          _socketService.createSocketConnection();
        }

        if (!kIsWeb) {
          final token = await FirebaseMessaging.instance.getToken();
          // print('-----');
          // print(token);
          // Clipboard.setData(new ClipboardData(text: token));
          // print('-----');
          await saveTokenToDatabase(token!);
          FirebaseMessaging.instance.onTokenRefresh.listen(saveTokenToDatabase);
        }

        if (user.provider == 'google') {
          currentLoginType = LoginType.google;
        } else {
          currentLoginType = LoginType.facebook;
        }
        add(PresentAuthEvent(user));
        await _userService.addReferral();
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    switch (event.runtimeType) {
      case StartGoogleAuthEvent:
        await _userService.signInWithGoogle().then((userOrFailure) =>
            userOrFailure.fold((l) => _loginUser(UserModel.empty), (r) {
              currentLoginType = LoginType.google;
              return _loginUser(r);
            }));
        yield ProcessingAuthState(UserModel.empty);
        break;
      case StartFacebookAuthEvent:
        print('StartFacebookAuthEvent --- 1');
        await _userService.signInWithFacebook().then((userOrFailure) =>
            userOrFailure.fold((l) => _loginUser(UserModel.empty), (r) {
              currentLoginType = LoginType.facebook;
              print('StartFacebookAuthEvent --- 2');
              return _loginUser(r);
            }));
        yield ProcessingAuthState(UserModel.empty);
        break;
      case PresentAuthEvent:
        {
          if ((event as PresentAuthEvent).user.provider == 'google') {
            currentLoginType = LoginType.google;
          } else {
            currentLoginType = LoginType.facebook;
          }
          yield LoggedAuthState((event).user);
          break;
        }

      case StartLocalAuthEvent:
        yield* _mapStartLocalAuth();
        break;
      case StartLogoutAuthEvent:
        {
          if (currentLoginType == null) {
            add((LoggedOutAuthEvent()));
          } else {
            await _userService.signOut(currentLoginType!).then((value) {
              currentLoginType = null;
              add((LoggedOutAuthEvent()));
            });
          }
          yield ProcessingAuthState(UserModel.empty);
          break;
        }

      case LoggedOutAuthEvent:
        yield NonLoggedAuthState(UserModel.empty);
        break;
      default:
        throw 'Unknown event';
    }
  }

  Stream<AuthState> _mapStartLocalAuth() async* {
    try {
      await _userService
          .checkLocalStorage()
          .then(_loginUser)
          .timeout(const Duration(seconds: 20));
      yield ProcessingAuthState(UserModel.empty);
    } on TimeoutException catch (e) {
      yield AuthStateFailure(
        UserModel.empty,
        message: 'AuthBloc timeout error $e',
      );
    } catch (e) {
      yield AuthStateFailure(
        UserModel.empty,
        message: 'AuthBloc error $e',
      );
      //rethrow;
    }
  }

  void dispatchTokenEvent() {
    final event = html.CustomEvent('postTokenJs');
    html.document.dispatchEvent(event);
  }

  Future<void> saveTokenToDatabase(String event) async {
    final response = await _userService.addPushToken(event);
    debugPrint('Device token saved to server: $response');
  }
}
