import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import '../../models/app_category.dart';
import '../../services/product_service.dart';

part 'top_products_event.dart';
part 'top_products_state.dart';

class TopProductsBlocByCategories
    extends Bloc<TopProductsByCategoriesEvent, TopProductsByCategoriesState> {
  final ProductService _productService;
  TopProductsBlocByCategories({required ProductService productService})
      : _productService = productService,
        super(TopProductsInitial());

  @override
  Stream<TopProductsByCategoriesState> mapEventToState(
    TopProductsByCategoriesEvent event,
  ) async* {
    switch (event.runtimeType) {
      case LoadTopProductsEvent:
        yield* _mapLoadTopProductsByCategories();
        break;
      default:
    }
  }

  Stream<TopProductsByCategoriesState>
      _mapLoadTopProductsByCategories() async* {
    yield TopProductsLoading();
    try {
      final _topProductsByCategories = await _productService
          .getTopProductsByCategories()
          .timeout(const Duration(seconds: 20));

      var _topAllProducts = <Product?>[];

      yield _topProductsByCategories.fold(
        (failure) => TopProductsFailure(),
        (result) {
          for (var p in result) {
            _topAllProducts.addAll(p.prods);
          }

          _topAllProducts
              .sort((a, b) => b!.stats!.sales.compareTo(a!.stats!.sales));

          for (var e in _topAllProducts) {
            debugPrint(e!.stats!.sales.toString());
          }

          final _productsWithRatings = _createRatingsMap(
            topProducts: result,
            allProducts: _topAllProducts,
          );

          return TopProductsLoaded(
            topProductsByCategories: result,
            topAllProducts: _topAllProducts.take(10).toList(),
            productsWithRatings: _productsWithRatings,
          );
        },
      );
    } on TimeoutException catch (e) {
      debugPrint('TopProductsBloc timeout error ${e.toString()}');
      yield TopProductsFailure();
    } catch (e) {
      debugPrint('TopProductsBloc error ${e.toString()}');
      rethrow;
    }
  }

  Map<String, String> _createRatingsMap({
    required List<AppCategory> topProducts,
    required List<Product?> allProducts,
  }) {
    var _ratingsMap = <String, String>{};
    for (var product in allProducts) {
      final _productsInCategory = topProducts
          .firstWhere((e) => e.productCategoryType == product!.productCategory);
      final _prod =
          _productsInCategory.prods.firstWhere((p) => p!.id == product!.id);
      final _prodRating = _productsInCategory.prods.indexOf(_prod) + 1;
      _ratingsMap[product!.id] = _prodRating.toString();
    }
    return _ratingsMap;
  }
}
