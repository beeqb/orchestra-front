part of 'added_products_bloc.dart';

abstract class AddedProductsEvent extends Equatable {
  const AddedProductsEvent();

  @override
  List<Object> get props => [];
}

class GetProductsByIdEvent extends AddedProductsEvent {}

class ChangeProductStatusToDeletedEvent extends AddedProductsEvent {
  final String productId;

  const ChangeProductStatusToDeletedEvent({required this.productId});
}
