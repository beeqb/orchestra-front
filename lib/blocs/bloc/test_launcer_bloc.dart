import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/settings_model.dart';
import 'package:orchestra/services/admin_service.dart';

part 'test_launcer_bloc.freezed.dart';

@freezed
class TestLauncherEvent with _$TestLauncherEvent {
  const TestLauncherEvent._();

  const factory TestLauncherEvent.create({
    required Product product,
  }) = CreateTestLauncherEvent;
}

@freezed
class TestLauncherState with _$TestLauncherState {
  const TestLauncherState._();

  const factory TestLauncherState.initial() = InitialTestLauncherState;
  const factory TestLauncherState.loading() = LoadingTestLauncherState;
  const factory TestLauncherState.loaded({
    required Product product,
    required List<SettingsModel> manifest,
  }) = LoadedTestLauncherState;
}

class TestLauncherBLoC extends Bloc<TestLauncherEvent, TestLauncherState> {
  /// для загрузки инфо о продукте на страницу тестового лаунчера
  final AdminService _adminService;
  TestLauncherBLoC(AdminService adminService)
      : _adminService = adminService,
        super(const InitialTestLauncherState());

  @override
  Stream<TestLauncherState> mapEventToState(TestLauncherEvent event) =>
      event.when<Stream<TestLauncherState>>(
        create: _create,
      );

  Stream<TestLauncherState> _create(
    Product product,
  ) async* {
    yield const LoadingTestLauncherState();
    var result = <SettingsModel>[];
    final manifestString = await _adminService.getProductManifest(product.id);

    final Map<String, dynamic> res = json.decode(manifestString);
    final forms = res['widget_settings'].first;
    final _set = Map.from(forms['form_data']);

    _set.forEach((key, value) {
      final _setting = SettingsModel.fromJson(value);
      final _settingUpd = _setting.copyWith(key: key);
      result.add(_settingUpd);
    });

    yield LoadedTestLauncherState(
      product: product,
      manifest: result,
    );
  }
}
