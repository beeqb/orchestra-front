import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/util/text_util.dart';

part 'log_screen_bloc.freezed.dart';

@freezed
class LogScreenEvent with _$LogScreenEvent {
  const LogScreenEvent._();

  const factory LogScreenEvent.addString({required String newString}) =
      AddStringLogScreenEvent;

  const factory LogScreenEvent.clear() = ClearLogScreenEvent;
}

@freezed
class LogScreenState with _$LogScreenState {
  const LogScreenState._();

  const factory LogScreenState.initial() = InitialLogScreenState;
  const factory LogScreenState.loading() = LoadingLogScreenState;
  const factory LogScreenState.loaded({required List<String> logs}) =
      LoadedLogScreenState;
}

class LogScreenBloc extends Bloc<LogScreenEvent, LogScreenState> {
  /// блок для показа лога на странице запуска
  LogScreenBloc() : super(const InitialLogScreenState());
  final List<String> _logs = [];

  @override
  Stream<LogScreenState> mapEventToState(LogScreenEvent event) =>
      event.when<Stream<LogScreenState>>(
        addString: _addString,
        clear: _clear,
      );

  Stream<LogScreenState> _addString(String newString) async* {
    yield const LoadingLogScreenState();
    final time = dateFormatWithTimeSec.format(DateTime.now());

    _logs.add('$time:\n$newString');
    yield LoadedLogScreenState(logs: _logs);
  }

  Stream<LogScreenState> _clear() async* {
    yield const LoadingLogScreenState();
    _logs.clear();
    yield const LoadedLogScreenState(logs: []);
  }
}
