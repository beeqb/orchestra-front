import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../models/app.dart';
import '../../models/purchase_model.dart';
import '../../services/app_service.dart';
import '../../services/product_service.dart';
import '../../services/timer_service.dart';

part 'app_timer_event.dart';
part 'app_timer_state.dart';

class AppTimerBloc extends Bloc<AppTimerEvent, AppTimerState> {
  final TimerService _timerService;
  final ProductService _productService;
  final AppService _appService;
  final AppModel _appModel;
  double? _runningCost;

  AppTimerBloc({
    required TimerService timerService,
    required ProductService productService,
    required AppService appService,
    required AppModel appModel,
  })  : _timerService = timerService,
        _appModel = appModel,
        _productService = productService,
        _appService = appService,
        super(AppTimerInitial());

  StreamSubscription<String>? _tickerSubscription;

  @override
  Stream<AppTimerState> mapEventToState(
    AppTimerEvent event,
  ) async* {
    switch (event.runtimeType) {
      case AppTimerInitial:
        yield* _mapTimerInitial();
        break;
      case AppTimerStarted:
        yield* _mapTimerStarted();
        break;
      case AppTimerTicked:
        yield* _mapTimerTicked(duration: (event as AppTimerTicked).duration);
        break;
      case AppTimerResumed:
        yield* _mapTimerResumed();
        break;
      default:
        throw 'AppTimerBloc: Unknown event';
    }
  }

  @override
  Future<void> close() {
    _tickerSubscription?.cancel();
    return super.close();
  }

  Stream<AppTimerState> _mapTimerInitial() async* {
    try {
      final _app = await _appService
          .getAppById(_appModel.id!)
          .timeout(const Duration(seconds: 20));
      if (_app.status == 'Active') {
        add(const AppTimerStarted());
      } else {
        add(AppTimerResumed());
      }
    } on TimeoutException catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
    } catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
      rethrow;
    }
  }

  Stream<AppTimerState> _mapTimerStarted() async* {
    late int _runTimeInSec;
    try {
      _runTimeInSec =
          await _getAppRunningTime().timeout(const Duration(seconds: 1));
    } on TimeoutException catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
    } catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
      rethrow;
    }

    debugPrint('runTime in sec $_runTimeInSec');
    _timerService.startTimer(_runTimeInSec);

    await _tickerSubscription?.cancel();
    _tickerSubscription = _timerService.stream
        .listen((event) => add(AppTimerTicked(duration: event)));
  }

  Stream<AppTimerState> _mapTimerTicked({required String duration}) async* {
    //debugPrint('ticked $duration');
    yield AppTimerInProgress(runningTime: duration);
  }

  Stream<AppTimerState> _mapTimerResumed() async* {
    var _duration = _timerService.currentTime;
    //debugPrint('duration $_duration');
    try {
      if (_duration == null) {
        var _inSec =
            await _getAppRunningTime().timeout(const Duration(seconds: 1));
        _duration = _appService.printDuration(Duration(seconds: _inSec));
      }
      _timerService.resetTimer();
      yield AppTimerComplete(
          runningTime: _duration, runningCost: _runningCost!);
    } on TimeoutException catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
    } catch (e) {
      debugPrint(e.toString());
      yield AppTimerFailure();
      rethrow;
    }
  }

  Future<int> _getAppRunningTime() async {
    final res = await _productService.getPurchasedProducts();
    final purchasedModels = res;

    late List<PurchaseModel> _appPurchases;

    if (purchasedModels.isNotEmpty) {
      _appPurchases = _appModel.widgets!
          .map((e) => purchasedModels.firstWhere(
                (w) => w.product.id == e,
              ))
          .cast<PurchaseModel>()
          .toList();
    }

    var runningTimesSumm = 0;
    for (var purchaseModel in _appPurchases) {
      runningTimesSumm += purchaseModel.runTime;
    }

    _runningCost = ((runningTimesSumm / 60).floor() * 0.01);

    return runningTimesSumm;
  }
}
