part of 'ratings_bloc.dart';

abstract class RatingsState extends Equatable {
  const RatingsState();

  @override
  List<Object> get props => [];
}

class RatingsInitial extends RatingsState {}

class RatingsLoading extends RatingsState {}

class RatingsLoaded extends RatingsState {
  final List<RatingItem> items;
  final List<double> rankWeights;
  final Map<String, dynamic> message;

  const RatingsLoaded({
    required this.items,
    required this.rankWeights,
    required this.message,
  });

  @override
  List<Object> get props => [items];
}

class RatingsFailure extends RatingsState {
  final String message;

  const RatingsFailure({required this.message});

  @override
  bool get stringify => true;
}
