import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/services/admin_service.dart';

part 'test_product_bloc.freezed.dart';

@freezed
class TestProductEvent with _$TestProductEvent {
  const TestProductEvent._();

  const factory TestProductEvent.start(Map<String, dynamic> settings) =
      StartTestProductEvent;
  const factory TestProductEvent.stop() = StopTestProductEvent;
}

@freezed
class TestProductState with _$TestProductState {
  const TestProductState._();

  const factory TestProductState.initial() = InitialTestProductState;
  const factory TestProductState.started() = StartedTestProductState;
  const factory TestProductState.active() = ActiveTestProductState;
  const factory TestProductState.stopped() = StoppedTestProductState;
}

class TestProductBLoC extends Bloc<TestProductEvent, TestProductState> {
  /// для запуска и остановки тестов виджета из админ панели
  final String _productId;
  final AdminService _adminService;
  TestProductBLoC({
    required String productId,
    required AdminService adminService,
  })   : _productId = productId,
        _adminService = adminService,
        super(const InitialTestProductState());

  @override
  Stream<TestProductState> mapEventToState(TestProductEvent event) =>
      event.when<Stream<TestProductState>>(
        start: _start,
        stop: _stop,
      );

  Stream<TestProductState> _start(Map<String, dynamic> settings) async* {
    yield const StartedTestProductState();
    final result = await _adminService.startTestProduct(
      _productId,
      settings,
    );
    //добавление ответа сервера в лог для админа
    sl<LogScreenBloc>()
        .add(AddStringLogScreenEvent(newString: 'Server response:\n$result'));
    yield const ActiveTestProductState();
  }

  Stream<TestProductState> _stop() async* {
    yield const StoppedTestProductState();
  }
}
