import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import '../../models/answer_item.dart';
import '../../models/rating_item_model.dart';
import '../../services/product_service.dart';

part 'ratings_event.dart';
part 'ratings_state.dart';

class RatingsBloc extends Bloc<RatingsEvent, RatingsState> {
  final ProductService _productService;
  final Product _product;

  RatingsBloc({
    required ProductService productService,
    required Product product,
  })  : _productService = productService,
        _product = product,
        super(RatingsInitial());

  @override
  Stream<RatingsState> mapEventToState(
    RatingsEvent event,
  ) async* {
    switch (event.runtimeType) {
      case StartRatingsLoadingEvent:
        yield* _mapLoadRatings();
        break;
      case PostSingleRatingEvent:
        yield* _mapPostRating(
          productId: (event as PostSingleRatingEvent).productId,
          userId: (event).userId,
          rank: (event).rank,
          comment: (event).comment,
        );
        break;
      case PostRatingAnswerEvent:
        yield* _mapRatingAnswer(
          answerItem: (event as PostRatingAnswerEvent).answerItem,
          productId: (event).productId,
        );
        break;
      default:
        throw 'RatingsBloc unknown event';
    }
  }

  Stream<RatingsState> _mapLoadRatings() async* {
    yield RatingsLoading();
    try {
      final _items = await _productService
          .getProductRatings(_product.id)
          .timeout(const Duration(seconds: 20));
      final _weights = getRankWeights(_items);
      yield RatingsLoaded(
          items: _items, rankWeights: _weights, message: const {});
    } on TimeoutException catch (e) {
      yield RatingsFailure(message: 'RatingsBloc timeout error $e');
    } catch (e) {
      yield RatingsFailure(message: 'RatingsBloc  error $e');
      rethrow;
    }
  }

  Stream<RatingsState> _mapPostRating({
    required String productId,
    required String userId,
    required int rank,
    required String comment,
  }) async* {
    yield RatingsLoading();

    try {
      var _result = await _productService
          .addRatingAndComment(productId, userId, rank, comment)
          .timeout(const Duration(seconds: 20));
      final _items = await _productService
          .getProductRatings(_product.id)
          .timeout(const Duration(seconds: 20));
      final Map<String, dynamic> _message = json.decode(_result);
      final _weights = getRankWeights(_items);
      yield RatingsLoaded(
          items: _items, rankWeights: _weights, message: _message);
    } on TimeoutException catch (e) {
      yield RatingsFailure(message: 'RatingsBloc timeout error $e');
    } catch (e) {
      yield RatingsFailure(message: 'RatingsBloc error $e');
      rethrow;
    }
  }

  Stream<RatingsState> _mapRatingAnswer({
    required AnswerItem answerItem,
    required String productId,
  }) async* {
    yield RatingsLoading();
    try {
      final res = await _productService
          .addRatingAnswer(answerItem: answerItem, productId: productId)
          .timeout(const Duration(seconds: 20));
      debugPrint('RatingsBloc res $res');
      add(StartRatingsLoadingEvent());
    } on TimeoutException catch (e) {
      yield RatingsFailure(message: 'RatingsBloc timeout error $e');
      add(StartRatingsLoadingEvent());
    } catch (e) {
      yield RatingsFailure(message: 'RatingsBloc error $e');
      rethrow;
    }
  }

  List<double> getRankWeights(List<RatingItem> items) {
    var _list = <double>[0, 0, 0, 0, 0];
    for (var e in items) {
      e.rank > 0 ? _list[e.rank - 1]++ : null;
    }

    for (var i = 0; i < _list.length; i++) {
      if (_list[i] > 0) {
        var w = _list[i] / items.length;
        _list[i] = w;
      }
    }
    return _list;
  }
}
