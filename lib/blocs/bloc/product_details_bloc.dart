import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import '../../services/product_service.dart';

part 'product_details_event.dart';
part 'product_details_state.dart';

class ProductDetailsBloc
    extends Bloc<ProductDetailsEvent, ProductDetailsState> {
  final ProductService _productService;
  ProductDetailsBloc({required ProductService productService})
      : _productService = productService,
        super(ProductDetailsInitial());

  @override
  Stream<ProductDetailsState> mapEventToState(
    ProductDetailsEvent event,
  ) async* {
    switch (event.runtimeType) {
      case LoadProductDetailsEvent:
        yield* _mapLoadProductDetails(
            (event as LoadProductDetailsEvent).productId);
        break;
      default:
    }
  }

  Stream<ProductDetailsState> _mapLoadProductDetails(String productId) async* {
    yield ProductDetailsLoading();
    try {
      final result = await _productService
          .getProductById(productId)
          .timeout(const Duration(seconds: 20));
      yield ProductDetailsLoaded(product: result);
    } on TimeoutException catch (e) {
      debugPrint('ProductDetailsBloc timeout error ${e.toString()}');
      yield ProductDetailsFailure(message: e.toString());
    } catch (e) {
      debugPrint('ProductDetailsBloc error ${e.toString()}');
      yield ProductDetailsFailure(message: e.toString());
      rethrow;
    }
  }
}
