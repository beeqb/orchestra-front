import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../core/const.dart';
import '../../util/text_util.dart';

part 'test_ends_after_event.dart';
part 'test_ends_after_state.dart';

class TestEndsAfterBloc extends Bloc<TestEndsAfterEvent, TestEndsAfterState> {
  final List<String> _testEndsAfter;

  TestEndsAfterBloc({required List<String> testEndsAfter})
      : _testEndsAfter = testEndsAfter,
        super(TestEndsAfterInitial());

  @override
  Stream<TestEndsAfterState> mapEventToState(
    TestEndsAfterEvent event,
  ) async* {
    switch (event.runtimeType) {
      case SetTestEndsAfter:
        yield* _mapSetTestEndsAfter(
          (event as SetTestEndsAfter).value,
          (event).index,
        );
        break;
      case TestEndsAfterInit:
        yield* _mapTestEndAfterInit();
        break;
      default:
        throw 'TEST_ENDS_AFTER_BLOC: Unknown event';
    }
  }

  Stream<TestEndsAfterState> _mapTestEndAfterInit() async* {
    yield TestEndsAfterPricessing();
    // ignore: prefer_interpolation_to_compose_strings
    final _description = 'Test fly: ' +
        getLabelText(
          freeForTest: true,
          testEndsAfter: _testEndsAfter,
        );
    yield TestEndsAfterSet(
      testEndsAfter: _testEndsAfter,
      description: _description,
    );
  }

  Stream<TestEndsAfterState> _mapSetTestEndsAfter(
    bool value,
    int index,
  ) async* {
    yield TestEndsAfterPricessing();
    if (value) {
      _testEndsAfter.add(kTestEndsAfter.entries.elementAt(index).key);
    } else {
      _testEndsAfter.remove(kTestEndsAfter.entries.elementAt(index).key);
    }

    // ignore: prefer_interpolation_to_compose_strings
    final _description = 'Test fly: ' +
        getLabelText(
          freeForTest: true,
          testEndsAfter: _testEndsAfter,
        );

    yield TestEndsAfterSet(
      testEndsAfter: _testEndsAfter,
      description: _description,
    );
  }
}
