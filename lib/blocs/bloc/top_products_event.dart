part of 'top_products_bloc.dart';

abstract class TopProductsByCategoriesEvent extends Equatable {
  const TopProductsByCategoriesEvent();

  @override
  List<Object> get props => [];
}

class LoadTopProductsEvent extends TopProductsByCategoriesEvent {}
