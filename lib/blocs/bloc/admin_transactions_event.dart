part of 'admin_transactions_bloc.dart';

abstract class AdminTransactionsEvent extends Equatable {
  const AdminTransactionsEvent();

  @override
  List<Object> get props => [];
}

class LoadAdminTransactionsEvent extends AdminTransactionsEvent {}
