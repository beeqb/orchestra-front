import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sliding_page_bloc.freezed.dart';

@freezed
class SlidingPageEvent with _$SlidingPageEvent {
  const SlidingPageEvent._();

  const factory SlidingPageEvent.open() = OpenSlidingPageEvent;

  const factory SlidingPageEvent.close() = CloseSlidingPageEvent;
}

@freezed
class SlidingPageState with _$SlidingPageState {
  const SlidingPageState._();

  const factory SlidingPageState.initial() = InitialSlidingPageState;
  const factory SlidingPageState.closed() = ClosedSlidingPageState;
  const factory SlidingPageState.opened() = OpenedSlidingPageState;
}

class SlidingPageBLoC extends Bloc<SlidingPageEvent, SlidingPageState> {
  SlidingPageBLoC() : super(const InitialSlidingPageState());

  @override
  Stream<SlidingPageState> mapEventToState(SlidingPageEvent event) =>
      event.when<Stream<SlidingPageState>>(
        open: _open,
        close: _close,
      );

  Stream<SlidingPageState> _open() async* {
    yield const SlidingPageState.opened();
  }

  Stream<SlidingPageState> _close() async* {
    yield const SlidingPageState.closed();
  }
}
