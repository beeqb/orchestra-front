import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/billing_info.dart';

import 'package:orchestra/services/billing_service.dart';

abstract class BillingInfoEvent {}

class LoadBillingInfoEvent extends BillingInfoEvent {}

//===================================================================

abstract class BillingInfoState {
  BillingInfoState();
}

class InitialBillingInfoState extends BillingInfoState {
  InitialBillingInfoState() : super();
}

class LoadingBillingInfoState extends BillingInfoState {
  LoadingBillingInfoState() : super();
}

class LoadedBillingInfoState extends BillingInfoState {
  final BillingInfoModel billingInfo;
  LoadedBillingInfoState(this.billingInfo);
}

class BillingInfoStateFailure extends BillingInfoState {
  final String message;
  BillingInfoStateFailure({required this.message}) : super();

  @override
  String toString() => 'BillingInfoStateFailure(message: $message)';
}

//=====================================================================
class BillingInfoBloc extends Bloc<BillingInfoEvent, BillingInfoState> {
  final BillingService _billingService;
  BillingInfoBloc({
    required BillingService billingService,
  })   : _billingService = billingService,
        super(InitialBillingInfoState());

  @override
  Stream<BillingInfoState> mapEventToState(BillingInfoEvent event) async* {
    switch (event.runtimeType) {
      case LoadBillingInfoEvent:
        yield* _mapLoadBillingInfo();
        break;
      default:
        throw 'Unknown event';
    }
  }

  Stream<BillingInfoState> _mapLoadBillingInfo() async* {
    yield LoadingBillingInfoState();
    final result = await _billingService.getBillingInfo();
    yield LoadedBillingInfoState(result);
  }
}
