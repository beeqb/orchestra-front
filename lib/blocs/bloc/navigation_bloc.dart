import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/routing/nav2/routes.dart';
import 'package:orchestra/safe_area_color.dart';

part 'navigation_bloc.freezed.dart';

@freezed
class NavigationEvent with _$NavigationEvent {
  const NavigationEvent._();

  const factory NavigationEvent.toMarketplace() = ToMarketplaceNavigationEvent;

  const factory NavigationEvent.toProductDetails(String productId) =
      ToProductDetailsNavigationEvent;

  const factory NavigationEvent.toProductDetailsUse(
    String productId,
    Product product,
  ) = ToProductDetailsUseNavigationEvent;

  const factory NavigationEvent.toCategory(ProductCategoryType categoryType) =
      ToCategoryNavigationEvent;

  const factory NavigationEvent.toDashboard() = ToDashboardNavigationEvent;

  const factory NavigationEvent.toBillings() = ToBillingsNavigationEvent;

  const factory NavigationEvent.toMyWidgets() = ToMyWidgetsNavigationEvent;

  const factory NavigationEvent.toTestLauncher(Product product) =
      ToTestLauncherEvent;

  const factory NavigationEvent.toAddedProducts() = ToAddedProductsEvent;

  const factory NavigationEvent.toEditProduct(Product product) =
      ToEditProductNavigationEvent;

  const factory NavigationEvent.toAddProduct() = ToAddProductNavigationEvent;

  const factory NavigationEvent.toAdmin() = ToAdminNavigationEvent;

  const factory NavigationEvent.toProfile() = ToProfileNavigationEvent;

  const factory NavigationEvent.toLogin() = ToLoginNavigationEvent;

  const factory NavigationEvent.toCoinDeposit() = ToCoinDepositNavigationEvent;

  const factory NavigationEvent.toStripeDeposit() =
      ToStripeDepositNavigationEvent;

  const factory NavigationEvent.toDepositSuccess() =
      ToDepositSuccessNavigationEvent;

  const factory NavigationEvent.toDepositFailed() =
      ToDepositFailedNavigationEvent;
}

@freezed
class NavigationState with _$NavigationState {
  const NavigationState._();

  const factory NavigationState.initial(OrchestraRoutePath routePath) =
      InitialNavigationState;

  const factory NavigationState.marketplace(OrchestraRoutePath routePath) =
      MarketplaceNavigationState;

  const factory NavigationState.productDetails(
    OrchestraRoutePath routePath,
    String productId,
  ) = ProductDetailsNavigationState;

  const factory NavigationState.productDetailsUse(
    OrchestraRoutePath routePath,
    String productId,
    Product product,
  ) = ProductDetailsUseNavigationState;

  const factory NavigationState.category(
    OrchestraRoutePath routePath,
    ProductCategoryType categoryType,
  ) = CategoryNavigationState;

  const factory NavigationState.dashboard(OrchestraRoutePath routePath) =
      DashboardNavigationState;

  const factory NavigationState.billings(OrchestraRoutePath routePath) =
      BillingsNavigationState;
  const factory NavigationState.login(OrchestraRoutePath routePath) =
      LoginNavigationState;

  const factory NavigationState.profile(OrchestraRoutePath routePath) =
      ProfileNavigationState;

  const factory NavigationState.myWidgets(OrchestraRoutePath routePath) =
      MyWidgetsNavigationState;

  const factory NavigationState.testLauncher(
    OrchestraRoutePath routePath,
    Product product,
  ) = TestLauncherNavigationState;

  const factory NavigationState.addedProducts(OrchestraRoutePath routePath) =
      AddedProductsNavigationState;

  const factory NavigationState.editProduct(
    OrchestraRoutePath routePath,
    Product product,
  ) = EditProductNavigationState;

  const factory NavigationState.addProduct(
    OrchestraRoutePath routePath,
  ) = AddProductNavigationState;

  const factory NavigationState.admin(OrchestraRoutePath routePath) =
      AdminNavigationState;

  const factory NavigationState.coinDeposit(OrchestraRoutePath routePath) =
      CoinDepositNavigationState;

  const factory NavigationState.stripeDeposit(OrchestraRoutePath routePath) =
      StripeDepositNavigationState;

  const factory NavigationState.depositSuccess(OrchestraRoutePath routePath) =
      DepositSuccessNavigationState;

  const factory NavigationState.depositFailed(OrchestraRoutePath routePath) =
      DepositFailedNavigationState;
}

class NavigationBLoC extends Bloc<NavigationEvent, NavigationState> {
  NavigationBLoC() : super(InitialNavigationState(MarketplaceRoutePath()));

  @override
  Stream<NavigationState> mapEventToState(NavigationEvent event) =>
      event.maybeWhen<Stream<NavigationState>>(
        toMarketplace: _toMarketplace,
        toDashboard: _toDashboard,
        toBillings: _toBillings,
        toLogin: _toLogin,
        toProfile: _toProfile,
        toAddedProducts: _toAddedProducts,
        toAddProduct: _toAddProduct,
        toAdmin: _toAdmin,
        toMyWidgets: _toMyWidgets,
        toCoinDeposit: _toCoinDeposit,
        toDepositFailed: _toDepositFailed,
        toDepositSuccess: _toDepositSuccess,
        toStripeDeposit: _toStripeDeposit,
        toProductDetails: _toProductDetails,
        toCategory: _toCategory,
        toEditProduct: _toEditProduct,
        orElse: _toMarketplace,
        toTestLauncher: _toTestLauncher,
        toProductDetailsUse: _toProductDetailsUse,
      );

  Stream<NavigationState> _toProductDetailsUse(
      String productId, Product product) async* {
    final id = productId.split('/').last;
    yield ProductDetailsUseNavigationState(
      ProductDetailsUseRoutePath(productId: id),
      id,
      product,
    );
  }

  Stream<NavigationState> _toMarketplace() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield MarketplaceNavigationState(MarketplaceRoutePath());
  }

  Stream<NavigationState> _toProductDetails(String productId) async* {
    final id = productId.split('/').last;
    yield ProductDetailsNavigationState(
      ProductDetailsRoutePath(productId: id),
      id,
    );
  }

  Stream<NavigationState> _toCategory(ProductCategoryType categoryType) async* {
    yield CategoryNavigationState(
      CategoryRoutePath(categoryType.title),
      categoryType,
    );
  }

  Stream<NavigationState> _toDashboard() async* {
    sl<SafeAreaColor>().setStyle(false);
    yield DashboardNavigationState(DashboardRoutePath());
  }

  Stream<NavigationState> _toBillings() async* {
    sl<SafeAreaColor>().setStyle(false);
    yield BillingsNavigationState(BillingsRoutePath());
  }

  Stream<NavigationState> _toLogin() async* {
    sl<SafeAreaColor>().setColorLoginPage();
    yield LoginNavigationState(LoginRoutePath());
  }

  Stream<NavigationState> _toProfile() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield ProfileNavigationState(ProfileRoutePath());
  }

  Stream<NavigationState> _toAddedProducts() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield AddedProductsNavigationState(AddedProductsRoutePath());
  }

  Stream<NavigationState> _toEditProduct(Product product) async* {
    sl<SafeAreaColor>().setStyle(true);
    yield EditProductNavigationState(EditProductRoutePath(), product);
  }

  Stream<NavigationState> _toAddProduct() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield AddProductNavigationState(AddProductRoutePath());
  }

  Stream<NavigationState> _toAdmin() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield AdminNavigationState(AdminRoutePath());
  }

  Stream<NavigationState> _toMyWidgets() async* {
    sl<SafeAreaColor>().setStyle(true);
    yield MyWidgetsNavigationState(MyWidgetsRoutePath());
  }

  Stream<NavigationState> _toCoinDeposit() async* {
    yield CoinDepositNavigationState(CoinDepositRoutePath());
  }

  Stream<NavigationState> _toStripeDeposit() async* {
    yield StripeDepositNavigationState(StripeDepositRoutePath());
  }

  Stream<NavigationState> _toDepositSuccess() async* {
    yield DepositSuccessNavigationState(DepositSuccessRoutePath());
  }

  Stream<NavigationState> _toDepositFailed() async* {
    yield DepositFailedNavigationState(DepositFailedRoutePath());
  }

  Stream<NavigationState> _toTestLauncher(
    Product product,
  ) async* {
    yield TestLauncherNavigationState(TestLauncherRoutePath(), product);
  }

  bool get isRootDirectory {
    if (state is MarketplaceNavigationState) {
      return true;
    }
    if (state is DashboardNavigationState) {
      return true;
    }
    if (state is MyWidgetsNavigationState) {
      return true;
    }
    if (state is BillingsNavigationState) {
      return true;
    }
    return false;
  }
}
