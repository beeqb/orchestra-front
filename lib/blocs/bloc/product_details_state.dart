part of 'product_details_bloc.dart';

abstract class ProductDetailsState extends Equatable {
  const ProductDetailsState();

  @override
  List<Object> get props => [];
}

class ProductDetailsInitial extends ProductDetailsState {}

class ProductDetailsLoading extends ProductDetailsState {}

class ProductDetailsLoaded extends ProductDetailsState {
  final Product product;

  const ProductDetailsLoaded({required this.product});
  @override
  List<Object> get props => [product];
}

class ProductDetailsFailure extends ProductDetailsState {
  final String message;

  const ProductDetailsFailure({required this.message});
}
