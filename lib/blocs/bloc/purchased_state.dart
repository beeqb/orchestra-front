part of 'purchased_bloc.dart';

abstract class PurchasedState extends Equatable {
  const PurchasedState();

  @override
  List<Object> get props => [];
}

class PurchasedInitial extends PurchasedState {}

class PurchasedLoading extends PurchasedState {
  const PurchasedLoading();
}

class PurchasedLoaded extends PurchasedState {
  final List<PurchaseModel> purchasedProducts;

  const PurchasedLoaded({required this.purchasedProducts});

  @override
  String toString() => 'PurchasedLoaded(purchasedProducts: $purchasedProducts)';
}

class PurchasedError extends PurchasedState {
  final String message;

  const PurchasedError(this.message);
}
