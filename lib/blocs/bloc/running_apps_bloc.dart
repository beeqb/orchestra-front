import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/running_app.dart';
import 'package:orchestra/services/admin_service.dart';

part 'running_apps_bloc.freezed.dart';

@freezed
class RunningAppsEvent with _$RunningAppsEvent {
  const RunningAppsEvent._();

  const factory RunningAppsEvent.fetch() = FetchRunningAppsEvent;
}

@freezed
class RunningAppsState with _$RunningAppsState {
  const RunningAppsState._();

  const factory RunningAppsState.initial() = InitialRunningAppsState;
  const factory RunningAppsState.loading() = LoadingRunningAppsState;
  const factory RunningAppsState.loaded(
      {required List<RunningAppModel> runningApps}) = LoadedRunningAppsState;
  const factory RunningAppsState.failure() = FailureRunningAppsState;
}

class RunningAppsBloc extends Bloc<RunningAppsEvent, RunningAppsState> {
  RunningAppsBloc({required AdminService adminService})
      : _adminService = adminService,
        super(const InitialRunningAppsState());
  final AdminService _adminService;

  @override
  Stream<RunningAppsState> mapEventToState(RunningAppsEvent event) =>
      event.when<Stream<RunningAppsState>>(
        fetch: _fetch,
      );

  Stream<RunningAppsState> _fetch() async* {
    yield const LoadingRunningAppsState();
    try {
      final result = await _adminService
          .getRunningsProduct()
          .timeout(const Duration(seconds: 20));
      debugPrint('RunningAppsBloc $result');
      yield LoadedRunningAppsState(runningApps: result);
    } on TimeoutException catch (e) {
      debugPrint('RunningAppsBloc error $e');
      yield const FailureRunningAppsState();
    } catch (e) {
      debugPrint('RunningAppsBloc error $e');
      yield const FailureRunningAppsState();
      rethrow;
    }
  }
}
