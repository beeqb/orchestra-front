// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'test_product_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TestProductEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> settings) start,
    required TResult Function() stop,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartTestProductEvent value) start,
    required TResult Function(StopTestProductEvent value) stop,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestProductEventCopyWith<$Res> {
  factory $TestProductEventCopyWith(
          TestProductEvent value, $Res Function(TestProductEvent) then) =
      _$TestProductEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$TestProductEventCopyWithImpl<$Res>
    implements $TestProductEventCopyWith<$Res> {
  _$TestProductEventCopyWithImpl(this._value, this._then);

  final TestProductEvent _value;
  // ignore: unused_field
  final $Res Function(TestProductEvent) _then;
}

/// @nodoc
abstract class _$$StartTestProductEventCopyWith<$Res> {
  factory _$$StartTestProductEventCopyWith(_$StartTestProductEvent value,
          $Res Function(_$StartTestProductEvent) then) =
      __$$StartTestProductEventCopyWithImpl<$Res>;
  $Res call({Map<String, dynamic> settings});
}

/// @nodoc
class __$$StartTestProductEventCopyWithImpl<$Res>
    extends _$TestProductEventCopyWithImpl<$Res>
    implements _$$StartTestProductEventCopyWith<$Res> {
  __$$StartTestProductEventCopyWithImpl(_$StartTestProductEvent _value,
      $Res Function(_$StartTestProductEvent) _then)
      : super(_value, (v) => _then(v as _$StartTestProductEvent));

  @override
  _$StartTestProductEvent get _value => super._value as _$StartTestProductEvent;

  @override
  $Res call({
    Object? settings = freezed,
  }) {
    return _then(_$StartTestProductEvent(
      settings == freezed
          ? _value._settings
          : settings // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
    ));
  }
}

/// @nodoc

class _$StartTestProductEvent extends StartTestProductEvent {
  const _$StartTestProductEvent(final Map<String, dynamic> settings)
      : _settings = settings,
        super._();

  final Map<String, dynamic> _settings;
  @override
  Map<String, dynamic> get settings {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_settings);
  }

  @override
  String toString() {
    return 'TestProductEvent.start(settings: $settings)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StartTestProductEvent &&
            const DeepCollectionEquality().equals(other._settings, _settings));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_settings));

  @JsonKey(ignore: true)
  @override
  _$$StartTestProductEventCopyWith<_$StartTestProductEvent> get copyWith =>
      __$$StartTestProductEventCopyWithImpl<_$StartTestProductEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> settings) start,
    required TResult Function() stop,
  }) {
    return start(settings);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
  }) {
    return start?.call(settings);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
    required TResult orElse(),
  }) {
    if (start != null) {
      return start(settings);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartTestProductEvent value) start,
    required TResult Function(StopTestProductEvent value) stop,
  }) {
    return start(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
  }) {
    return start?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
    required TResult orElse(),
  }) {
    if (start != null) {
      return start(this);
    }
    return orElse();
  }
}

abstract class StartTestProductEvent extends TestProductEvent {
  const factory StartTestProductEvent(final Map<String, dynamic> settings) =
      _$StartTestProductEvent;
  const StartTestProductEvent._() : super._();

  Map<String, dynamic> get settings;
  @JsonKey(ignore: true)
  _$$StartTestProductEventCopyWith<_$StartTestProductEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StopTestProductEventCopyWith<$Res> {
  factory _$$StopTestProductEventCopyWith(_$StopTestProductEvent value,
          $Res Function(_$StopTestProductEvent) then) =
      __$$StopTestProductEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StopTestProductEventCopyWithImpl<$Res>
    extends _$TestProductEventCopyWithImpl<$Res>
    implements _$$StopTestProductEventCopyWith<$Res> {
  __$$StopTestProductEventCopyWithImpl(_$StopTestProductEvent _value,
      $Res Function(_$StopTestProductEvent) _then)
      : super(_value, (v) => _then(v as _$StopTestProductEvent));

  @override
  _$StopTestProductEvent get _value => super._value as _$StopTestProductEvent;
}

/// @nodoc

class _$StopTestProductEvent extends StopTestProductEvent {
  const _$StopTestProductEvent() : super._();

  @override
  String toString() {
    return 'TestProductEvent.stop()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StopTestProductEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Map<String, dynamic> settings) start,
    required TResult Function() stop,
  }) {
    return stop();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
  }) {
    return stop?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Map<String, dynamic> settings)? start,
    TResult Function()? stop,
    required TResult orElse(),
  }) {
    if (stop != null) {
      return stop();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartTestProductEvent value) start,
    required TResult Function(StopTestProductEvent value) stop,
  }) {
    return stop(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
  }) {
    return stop?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartTestProductEvent value)? start,
    TResult Function(StopTestProductEvent value)? stop,
    required TResult orElse(),
  }) {
    if (stop != null) {
      return stop(this);
    }
    return orElse();
  }
}

abstract class StopTestProductEvent extends TestProductEvent {
  const factory StopTestProductEvent() = _$StopTestProductEvent;
  const StopTestProductEvent._() : super._();
}

/// @nodoc
mixin _$TestProductState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() started,
    required TResult Function() active,
    required TResult Function() stopped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestProductState value) initial,
    required TResult Function(StartedTestProductState value) started,
    required TResult Function(ActiveTestProductState value) active,
    required TResult Function(StoppedTestProductState value) stopped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestProductStateCopyWith<$Res> {
  factory $TestProductStateCopyWith(
          TestProductState value, $Res Function(TestProductState) then) =
      _$TestProductStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$TestProductStateCopyWithImpl<$Res>
    implements $TestProductStateCopyWith<$Res> {
  _$TestProductStateCopyWithImpl(this._value, this._then);

  final TestProductState _value;
  // ignore: unused_field
  final $Res Function(TestProductState) _then;
}

/// @nodoc
abstract class _$$InitialTestProductStateCopyWith<$Res> {
  factory _$$InitialTestProductStateCopyWith(_$InitialTestProductState value,
          $Res Function(_$InitialTestProductState) then) =
      __$$InitialTestProductStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialTestProductStateCopyWithImpl<$Res>
    extends _$TestProductStateCopyWithImpl<$Res>
    implements _$$InitialTestProductStateCopyWith<$Res> {
  __$$InitialTestProductStateCopyWithImpl(_$InitialTestProductState _value,
      $Res Function(_$InitialTestProductState) _then)
      : super(_value, (v) => _then(v as _$InitialTestProductState));

  @override
  _$InitialTestProductState get _value =>
      super._value as _$InitialTestProductState;
}

/// @nodoc

class _$InitialTestProductState extends InitialTestProductState {
  const _$InitialTestProductState() : super._();

  @override
  String toString() {
    return 'TestProductState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialTestProductState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() started,
    required TResult Function() active,
    required TResult Function() stopped,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestProductState value) initial,
    required TResult Function(StartedTestProductState value) started,
    required TResult Function(ActiveTestProductState value) active,
    required TResult Function(StoppedTestProductState value) stopped,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialTestProductState extends TestProductState {
  const factory InitialTestProductState() = _$InitialTestProductState;
  const InitialTestProductState._() : super._();
}

/// @nodoc
abstract class _$$StartedTestProductStateCopyWith<$Res> {
  factory _$$StartedTestProductStateCopyWith(_$StartedTestProductState value,
          $Res Function(_$StartedTestProductState) then) =
      __$$StartedTestProductStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StartedTestProductStateCopyWithImpl<$Res>
    extends _$TestProductStateCopyWithImpl<$Res>
    implements _$$StartedTestProductStateCopyWith<$Res> {
  __$$StartedTestProductStateCopyWithImpl(_$StartedTestProductState _value,
      $Res Function(_$StartedTestProductState) _then)
      : super(_value, (v) => _then(v as _$StartedTestProductState));

  @override
  _$StartedTestProductState get _value =>
      super._value as _$StartedTestProductState;
}

/// @nodoc

class _$StartedTestProductState extends StartedTestProductState {
  const _$StartedTestProductState() : super._();

  @override
  String toString() {
    return 'TestProductState.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StartedTestProductState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() started,
    required TResult Function() active,
    required TResult Function() stopped,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestProductState value) initial,
    required TResult Function(StartedTestProductState value) started,
    required TResult Function(ActiveTestProductState value) active,
    required TResult Function(StoppedTestProductState value) stopped,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class StartedTestProductState extends TestProductState {
  const factory StartedTestProductState() = _$StartedTestProductState;
  const StartedTestProductState._() : super._();
}

/// @nodoc
abstract class _$$ActiveTestProductStateCopyWith<$Res> {
  factory _$$ActiveTestProductStateCopyWith(_$ActiveTestProductState value,
          $Res Function(_$ActiveTestProductState) then) =
      __$$ActiveTestProductStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ActiveTestProductStateCopyWithImpl<$Res>
    extends _$TestProductStateCopyWithImpl<$Res>
    implements _$$ActiveTestProductStateCopyWith<$Res> {
  __$$ActiveTestProductStateCopyWithImpl(_$ActiveTestProductState _value,
      $Res Function(_$ActiveTestProductState) _then)
      : super(_value, (v) => _then(v as _$ActiveTestProductState));

  @override
  _$ActiveTestProductState get _value =>
      super._value as _$ActiveTestProductState;
}

/// @nodoc

class _$ActiveTestProductState extends ActiveTestProductState {
  const _$ActiveTestProductState() : super._();

  @override
  String toString() {
    return 'TestProductState.active()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ActiveTestProductState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() started,
    required TResult Function() active,
    required TResult Function() stopped,
  }) {
    return active();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
  }) {
    return active?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
    required TResult orElse(),
  }) {
    if (active != null) {
      return active();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestProductState value) initial,
    required TResult Function(StartedTestProductState value) started,
    required TResult Function(ActiveTestProductState value) active,
    required TResult Function(StoppedTestProductState value) stopped,
  }) {
    return active(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
  }) {
    return active?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
    required TResult orElse(),
  }) {
    if (active != null) {
      return active(this);
    }
    return orElse();
  }
}

abstract class ActiveTestProductState extends TestProductState {
  const factory ActiveTestProductState() = _$ActiveTestProductState;
  const ActiveTestProductState._() : super._();
}

/// @nodoc
abstract class _$$StoppedTestProductStateCopyWith<$Res> {
  factory _$$StoppedTestProductStateCopyWith(_$StoppedTestProductState value,
          $Res Function(_$StoppedTestProductState) then) =
      __$$StoppedTestProductStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StoppedTestProductStateCopyWithImpl<$Res>
    extends _$TestProductStateCopyWithImpl<$Res>
    implements _$$StoppedTestProductStateCopyWith<$Res> {
  __$$StoppedTestProductStateCopyWithImpl(_$StoppedTestProductState _value,
      $Res Function(_$StoppedTestProductState) _then)
      : super(_value, (v) => _then(v as _$StoppedTestProductState));

  @override
  _$StoppedTestProductState get _value =>
      super._value as _$StoppedTestProductState;
}

/// @nodoc

class _$StoppedTestProductState extends StoppedTestProductState {
  const _$StoppedTestProductState() : super._();

  @override
  String toString() {
    return 'TestProductState.stopped()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StoppedTestProductState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() started,
    required TResult Function() active,
    required TResult Function() stopped,
  }) {
    return stopped();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
  }) {
    return stopped?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? started,
    TResult Function()? active,
    TResult Function()? stopped,
    required TResult orElse(),
  }) {
    if (stopped != null) {
      return stopped();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTestProductState value) initial,
    required TResult Function(StartedTestProductState value) started,
    required TResult Function(ActiveTestProductState value) active,
    required TResult Function(StoppedTestProductState value) stopped,
  }) {
    return stopped(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
  }) {
    return stopped?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTestProductState value)? initial,
    TResult Function(StartedTestProductState value)? started,
    TResult Function(ActiveTestProductState value)? active,
    TResult Function(StoppedTestProductState value)? stopped,
    required TResult orElse(),
  }) {
    if (stopped != null) {
      return stopped(this);
    }
    return orElse();
  }
}

abstract class StoppedTestProductState extends TestProductState {
  const factory StoppedTestProductState() = _$StoppedTestProductState;
  const StoppedTestProductState._() : super._();
}
