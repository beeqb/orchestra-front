part of 'added_products_bloc.dart';

abstract class AddedProductsState extends Equatable {
  const AddedProductsState();

  @override
  List<Object> get props => [];
}

class AddedProductsInitial extends AddedProductsState {}

class AddedProductsLoading extends AddedProductsState {}

class AddedProductsLoaded extends AddedProductsState {
  final List<Product> addedProducts;

  const AddedProductsLoaded({required this.addedProducts});

  @override
  String toString() => 'AddedProductsLoaded(addedProducts: $addedProducts)';
}

class AddedProductsFailure extends AddedProductsState {
  final String message;

  const AddedProductsFailure({required this.message});

  @override
  String toString() => 'AddedProductsFailure(message: $message)';
}
