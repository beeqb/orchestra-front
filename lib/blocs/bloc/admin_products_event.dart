part of 'admin_products_bloc.dart';

abstract class AdminProductsEvent extends Equatable {
  const AdminProductsEvent();

  @override
  List<Object> get props => [];
}

class LoadAllProductsEvent extends AdminProductsEvent {}

class SearchProductsEvent extends AdminProductsEvent {
  final String query;

  const SearchProductsEvent(this.query);
}

class SortAllProductsEvent extends AdminProductsEvent {
  final Comparable Function(Product d) getField;
  final bool ascending;
  final int sortColumnIndex;

  const SortAllProductsEvent({
    required this.getField,
    required this.ascending,
    required this.sortColumnIndex,
  });
}
