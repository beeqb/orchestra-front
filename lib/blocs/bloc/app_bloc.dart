import 'dart:async';
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:universal_html/html.dart' as html;

import '../../models/app.dart';
import '../../models/enums/enums.dart';
import '../../repository/user_repository.dart';
import '../../services/app_service.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';
import 'app_button_bloc.dart';

abstract class AppState extends Equatable {
  const AppState();
  @override
  List<Object> get props => [];
}

class AppLoading extends AppState {}

class AppLoaded extends AppState {
  final AppModel app;

  const AppLoaded({required this.app});
  @override
  List<Object> get props => [app];
}

class AppBlocFailure extends AppState {}

//==============================================

abstract class AppEvent {}

class StartSwitchingAppEvent extends AppEvent {
  final String appId;
  StartSwitchingAppEvent(this.appId);

  @override
  String toString() => 'StartSwitchingAppEvent(appId: $appId)';
}

class StartUpdatingAppInfo extends AppEvent {}

class StartUpdatingAppSettings extends AppEvent {
  final AppModel appModel;

  StartUpdatingAppSettings({required this.appModel});
}

//=================================================

class AppBloc extends Bloc<AppEvent, AppState> {
  final AppService _appService;
  final DialogsService _dialogsService;
  final AppButtonBloc _appButtonBloc;
  final AppModel _app;
  //final UserRepository _userRepository;
  final BillingService _billingService;

  //FirebaseMessaging _fcm;

  AppBloc({
    required AppService appService,
    required DialogsService dialogsService,
    required UserRepository userRepository,
    required AppModel app,
    required AppButtonBloc appButtonBloc,
    required BillingService billingService,
  })  : _appService = appService,
        _billingService = billingService,
        //_userRepository = userRepository,
        _dialogsService = dialogsService,
        _appButtonBloc = appButtonBloc,
        _app = app,
        super(AppLoaded(app: app));

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    if (event is StartSwitchingAppEvent) {
      yield* _mapSwitchingAppToState(event);
    } else if (event is StartUpdatingAppInfo) {
      yield* _mapStartUpdatingAppInfo(event);
    } else if (event is StartUpdatingAppSettings) {
      yield* _mapStartUpdatingAppSettings(appModel: event.appModel);
    }
  }

  Stream<AppState> _mapSwitchingAppToState(event) async* {
    late AppModel _appModel;

    yield AppLoading();
    if (_app.status != 'Active') {
      try {
        final response = await _appService
            .startApp(_app.id!)
            .timeout(const Duration(seconds: 20));
        _appModel = await _appService
            .getAppById(_app.id!)
            .timeout(const Duration(seconds: 20));
        Map _res = json.decode(response);
        if (_res.keys.contains('charges')) {
          final _product = Product.fromJson(_res.entries.first.value.first);
          // dialog for purchase product
          showTestFlyGoneDialog(
            product: _product,
            appModel: _appModel,
          );
        } else if (_res.keys.contains('message')) {
          showServerMessageDialog(
            message: _res.entries.first.value,
            appModel: _appModel,
          );
        }
      } on TimeoutException catch (e) {
        debugPrint('app bloc error $e');
        yield AppBlocFailure();
      } catch (e) {
        debugPrint('app bloc error $e');
        yield AppBlocFailure();
        rethrow;
      }
      _appButtonBloc.add(AppButtonStartEvent(appModel: _appModel));
    } else {
      try {
        await _appService
            .pauseApp(_app.id!)
            .timeout(const Duration(seconds: 20));
        _appModel = await _appService
            .getAppById(_app.id!)
            .timeout(const Duration(seconds: 20));
      } on TimeoutException catch (e) {
        debugPrint('app bloc error $e');
        yield AppBlocFailure();
      } catch (e) {
        debugPrint('app bloc error $e');
        yield AppBlocFailure();
        rethrow;
      }
      _appButtonBloc.add(AppButtonPauseEvent(appModel: _appModel));
    }
    yield AppLoaded(app: _appModel);
  }

  Stream<AppState> _mapStartUpdatingAppInfo(event) async* {
    late AppModel _appModel;
    yield AppLoading();
    try {
      _appModel = await _appService
          .getAppById(_app.id!)
          .timeout(const Duration(seconds: 20));
      //debugPrint('status APP ${_appModel.status}');
      _appModel.status == 'Active'
          ? _appButtonBloc.add(AppButtonStartEvent(appModel: _appModel))
          : _appButtonBloc.add(AppButtonPauseEvent(appModel: _appModel));
      yield AppLoaded(app: _appModel);
    } on TimeoutException catch (e) {
      debugPrint('app bloc error $e');
      yield AppBlocFailure();
    } catch (e) {
      debugPrint('app bloc error $e');
      yield AppBlocFailure();
      rethrow;
    }
  }

  Stream<AppState> _mapStartUpdatingAppSettings(
      {required AppModel appModel}) async* {
    if (appModel.notification.contains('push')) {
      // запрос разрешений на отправку пушей при сохранении приложения
      if (!kIsWeb) {
        /*  _fcm = FirebaseMessaging();
        final String _token = await _fcm.getToken();
        if (_token != null) {
          try {
            _userRepository
                .addPushToken(_token)
                .then(
                  (value) => debugPrint('token FCM added to server: $_token'),
                )
                .timeout(const Duration(seconds: 2));
          } on TimeoutException catch (e) {
            debugPrint('AppBloc timeout error $e');
            yield AppBlocFailure();
          } on dynamic catch (e) {
            debugPrint('AppBloc error $e');
            yield AppBlocFailure();
            rethrow;
          }
        } */
      } else {
        try {
          dispatchTokenEvent();
          //  debugPrint('fcmToken sent');
        } on Exception catch (e) {
          debugPrint('fcmToken error: $e');
        }
      }
    }
    try {
      await _appService.saveAppSettings(appModel).then((app) {
        return add(StartUpdatingAppInfo());
      }).timeout(const Duration(seconds: 2));
    } on TimeoutException catch (e) {
      debugPrint('AppBloc timeout error $e');
      yield AppBlocFailure();
    } catch (e) {
      debugPrint('AppBloc error $e');
      yield AppBlocFailure();
      rethrow;
    }
    yield AppLoading();
  }

  void dispatchTokenEvent() {
    try {
      var event = html.CustomEvent('postTokenJs');
      html.document.dispatchEvent(event);
    } on Exception catch (e) {
      debugPrint('AppBloc error $e');
      rethrow;
    }
  }

  void showTestFlyGoneDialog({
    required Product product,
    required AppModel appModel,
  }) async {
    final dialogResult = await _dialogsService.showDialog(
      dialogType: DialogType.testFlyGoneDialog,
      title: '',
      product: product,
    );
    if (dialogResult.confirmed!) {
      _appButtonBloc.add(AppButtonPauseEvent(appModel: appModel));

// обработка ответа сервера при покупке виджета
      final result = await _billingService.chargeForProduct(product.id);
      if (result['status'] == 'error') {
        await _dialogsService.showDialog(
          dialogType: DialogType.warningDialog,
          title: 'Error',
          description: result['message'] ?? '',
        );
      }
    } else {
      _appButtonBloc.add(AppButtonPauseEvent(appModel: appModel));
    }
  }

  void showServerMessageDialog({
    required String message,
    required AppModel appModel,
  }) async {
    var dialogResult = await _dialogsService.showDialog(
      dialogType: DialogType.warningDialog,
      title: 'Warning',
      description: message,
    );
    if (dialogResult.confirmed!) {
      _appButtonBloc.add(AppButtonPauseEvent(appModel: appModel));
    }
  }
}
