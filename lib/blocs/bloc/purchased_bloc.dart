import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../models/purchase_model.dart';
import '../../services/product_service.dart';
import 'auth_bloc.dart';

part 'purchased_event.dart';
part 'purchased_state.dart';

class PurchasedBloc extends Bloc<PurchasedEvent, PurchasedState> {
  final AuthBloc _authBloc;
  final ProductService _productService;

  StreamSubscription? _authSubscription;
  PurchasedBloc({
    required AuthBloc authBloc,
    required ProductService productService,
  })  : _authBloc = authBloc,
        _productService = productService,
        super(PurchasedInitial()) {
    _authSubscription = _authBloc.stream.listen((state) async {
      if (state is LoggedAuthState) {
        _mapLoadPurchasedProducts();
      }
    });
  }

  @override
  Future<void> close() {
    _authSubscription!.cancel();
    return super.close();
  }

  @override
  Stream<PurchasedState> mapEventToState(
    PurchasedEvent event,
  ) async* {
    switch (event.runtimeType) {
      case LoadPurchasedProductsEvent:
        yield* _mapLoadPurchasedProducts();
        break;
      default:
    }
  }

  Stream<PurchasedState> _mapLoadPurchasedProducts() async* {
    yield const PurchasedLoading();
    if (_authBloc.state is LoggedAuthState) {
      try {
        final _result = await _productService
            .getPurchasedProducts()
            .timeout(const Duration(seconds: 20));
        yield PurchasedLoaded(purchasedProducts: _result);
      } on TimeoutException catch (e) {
        debugPrint('PurchasedBloc timeout error ${e.toString()}');
        yield PurchasedError(e.toString());
      } catch (e) {
        yield PurchasedError(e.toString());
        rethrow;
      }
    }
  }
}
