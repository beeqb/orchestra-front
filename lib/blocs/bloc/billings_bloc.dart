import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/billing_models/transaction_model.dart';
import 'package:orchestra/services/billing_service.dart';

abstract class BillingState extends Equatable {
  final List<TransactionModel> transactions;

  const BillingState(
    this.transactions,
  );

  @override
  List<Object> get props => [];
}

class BillingsInitial extends BillingState {
  @override
  final List<TransactionModel> transactions;
  BillingsInitial({
    required this.transactions,
  }) : super([]);
}

class BillingsLoading extends BillingState {
  @override
  final List<TransactionModel> transactions;

  const BillingsLoading({required this.transactions}) : super(transactions);
}

class BillingsLoaded extends BillingState {
  @override
  final List<TransactionModel> transactions;
  const BillingsLoaded({
    required this.transactions,
  }) : super(transactions);
}

class BillingsError extends BillingState {
  final String message;

  BillingsError({required this.message}) : super([]);
}

//==================================================
abstract class BillingsEvent extends Equatable {
  const BillingsEvent();
  @override
  List<Object> get props => [];
}

class FetchBillingsInfo extends BillingsEvent {}

class ResetBillingsEvent extends BillingsEvent {}

//==================================================

class BillingsBloc extends Bloc<BillingsEvent, BillingState> {
  final BillingService _billingService;

  BillingsBloc({
    required BillingService billingService,
  })  : _billingService = billingService,
        super(BillingsInitial(transactions: const []));

  List<TransactionModel> _list = [];
  int _page = 1;

  @override
  Stream<BillingState> mapEventToState(BillingsEvent event) async* {
    if (event is FetchBillingsInfo) {
      yield* _mapFetchBillingsInfo();
    } else if (event is ResetBillingsEvent) {
      yield* _mapResetResults();
    }
  }

  Stream<BillingState> _mapResetResults() async* {
    _page = 1;
    _list = [];
  }

  Stream<BillingState> _mapFetchBillingsInfo() async* {
    yield BillingsLoading(transactions: _list);
    try {
      final transactions = await _billingService
          .getTransactionHistory(_page)
          .timeout(const Duration(seconds: 20));
      _list.addAll(transactions);
      yield BillingsLoaded(transactions: _list);
      _page++;
    } on TimeoutException catch (e) {
      debugPrint('BillingsCubit timeout error $e');
      yield BillingsError(message: e.toString());
    } catch (e) {
      debugPrint('BillingsCubit error $e');
      yield BillingsError(message: e.toString());
      rethrow;
    }
  }
}
