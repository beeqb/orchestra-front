part of 'search_product_bloc.dart';

abstract class SearchProductState extends Equatable {
  const SearchProductState();

  @override
  List<Object> get props => [];
}

class SearchProductInitial extends SearchProductState {}

class SearchProductLoading extends SearchProductState {}

class SearchProductLoaded extends SearchProductState {
  final List<Product> foundProducts;

  const SearchProductLoaded({required this.foundProducts});

  @override
  List<Object> get props => [foundProducts];

  @override
  String toString() => 'SearchProductLoaded(foundProducts: $foundProducts)';
}

class SearchProductFailure extends SearchProductState {
  final String message;

  const SearchProductFailure({required this.message});
  @override
  List<Object> get props => [message];

  @override
  String toString() => 'SearchProductFailure(message: $message)';
}
