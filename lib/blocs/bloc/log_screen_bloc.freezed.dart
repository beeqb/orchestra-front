// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'log_screen_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LogScreenEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String newString) addString,
    required TResult Function() clear,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddStringLogScreenEvent value) addString,
    required TResult Function(ClearLogScreenEvent value) clear,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LogScreenEventCopyWith<$Res> {
  factory $LogScreenEventCopyWith(
          LogScreenEvent value, $Res Function(LogScreenEvent) then) =
      _$LogScreenEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$LogScreenEventCopyWithImpl<$Res>
    implements $LogScreenEventCopyWith<$Res> {
  _$LogScreenEventCopyWithImpl(this._value, this._then);

  final LogScreenEvent _value;
  // ignore: unused_field
  final $Res Function(LogScreenEvent) _then;
}

/// @nodoc
abstract class _$$AddStringLogScreenEventCopyWith<$Res> {
  factory _$$AddStringLogScreenEventCopyWith(_$AddStringLogScreenEvent value,
          $Res Function(_$AddStringLogScreenEvent) then) =
      __$$AddStringLogScreenEventCopyWithImpl<$Res>;
  $Res call({String newString});
}

/// @nodoc
class __$$AddStringLogScreenEventCopyWithImpl<$Res>
    extends _$LogScreenEventCopyWithImpl<$Res>
    implements _$$AddStringLogScreenEventCopyWith<$Res> {
  __$$AddStringLogScreenEventCopyWithImpl(_$AddStringLogScreenEvent _value,
      $Res Function(_$AddStringLogScreenEvent) _then)
      : super(_value, (v) => _then(v as _$AddStringLogScreenEvent));

  @override
  _$AddStringLogScreenEvent get _value =>
      super._value as _$AddStringLogScreenEvent;

  @override
  $Res call({
    Object? newString = freezed,
  }) {
    return _then(_$AddStringLogScreenEvent(
      newString: newString == freezed
          ? _value.newString
          : newString // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AddStringLogScreenEvent extends AddStringLogScreenEvent {
  const _$AddStringLogScreenEvent({required this.newString}) : super._();

  @override
  final String newString;

  @override
  String toString() {
    return 'LogScreenEvent.addString(newString: $newString)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddStringLogScreenEvent &&
            const DeepCollectionEquality().equals(other.newString, newString));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(newString));

  @JsonKey(ignore: true)
  @override
  _$$AddStringLogScreenEventCopyWith<_$AddStringLogScreenEvent> get copyWith =>
      __$$AddStringLogScreenEventCopyWithImpl<_$AddStringLogScreenEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String newString) addString,
    required TResult Function() clear,
  }) {
    return addString(newString);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
  }) {
    return addString?.call(newString);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
    required TResult orElse(),
  }) {
    if (addString != null) {
      return addString(newString);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddStringLogScreenEvent value) addString,
    required TResult Function(ClearLogScreenEvent value) clear,
  }) {
    return addString(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
  }) {
    return addString?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
    required TResult orElse(),
  }) {
    if (addString != null) {
      return addString(this);
    }
    return orElse();
  }
}

abstract class AddStringLogScreenEvent extends LogScreenEvent {
  const factory AddStringLogScreenEvent({required final String newString}) =
      _$AddStringLogScreenEvent;
  const AddStringLogScreenEvent._() : super._();

  String get newString;
  @JsonKey(ignore: true)
  _$$AddStringLogScreenEventCopyWith<_$AddStringLogScreenEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ClearLogScreenEventCopyWith<$Res> {
  factory _$$ClearLogScreenEventCopyWith(_$ClearLogScreenEvent value,
          $Res Function(_$ClearLogScreenEvent) then) =
      __$$ClearLogScreenEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ClearLogScreenEventCopyWithImpl<$Res>
    extends _$LogScreenEventCopyWithImpl<$Res>
    implements _$$ClearLogScreenEventCopyWith<$Res> {
  __$$ClearLogScreenEventCopyWithImpl(
      _$ClearLogScreenEvent _value, $Res Function(_$ClearLogScreenEvent) _then)
      : super(_value, (v) => _then(v as _$ClearLogScreenEvent));

  @override
  _$ClearLogScreenEvent get _value => super._value as _$ClearLogScreenEvent;
}

/// @nodoc

class _$ClearLogScreenEvent extends ClearLogScreenEvent {
  const _$ClearLogScreenEvent() : super._();

  @override
  String toString() {
    return 'LogScreenEvent.clear()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ClearLogScreenEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String newString) addString,
    required TResult Function() clear,
  }) {
    return clear();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
  }) {
    return clear?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String newString)? addString,
    TResult Function()? clear,
    required TResult orElse(),
  }) {
    if (clear != null) {
      return clear();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(AddStringLogScreenEvent value) addString,
    required TResult Function(ClearLogScreenEvent value) clear,
  }) {
    return clear(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
  }) {
    return clear?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(AddStringLogScreenEvent value)? addString,
    TResult Function(ClearLogScreenEvent value)? clear,
    required TResult orElse(),
  }) {
    if (clear != null) {
      return clear(this);
    }
    return orElse();
  }
}

abstract class ClearLogScreenEvent extends LogScreenEvent {
  const factory ClearLogScreenEvent() = _$ClearLogScreenEvent;
  const ClearLogScreenEvent._() : super._();
}

/// @nodoc
mixin _$LogScreenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<String> logs) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialLogScreenState value) initial,
    required TResult Function(LoadingLogScreenState value) loading,
    required TResult Function(LoadedLogScreenState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LogScreenStateCopyWith<$Res> {
  factory $LogScreenStateCopyWith(
          LogScreenState value, $Res Function(LogScreenState) then) =
      _$LogScreenStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$LogScreenStateCopyWithImpl<$Res>
    implements $LogScreenStateCopyWith<$Res> {
  _$LogScreenStateCopyWithImpl(this._value, this._then);

  final LogScreenState _value;
  // ignore: unused_field
  final $Res Function(LogScreenState) _then;
}

/// @nodoc
abstract class _$$InitialLogScreenStateCopyWith<$Res> {
  factory _$$InitialLogScreenStateCopyWith(_$InitialLogScreenState value,
          $Res Function(_$InitialLogScreenState) then) =
      __$$InitialLogScreenStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialLogScreenStateCopyWithImpl<$Res>
    extends _$LogScreenStateCopyWithImpl<$Res>
    implements _$$InitialLogScreenStateCopyWith<$Res> {
  __$$InitialLogScreenStateCopyWithImpl(_$InitialLogScreenState _value,
      $Res Function(_$InitialLogScreenState) _then)
      : super(_value, (v) => _then(v as _$InitialLogScreenState));

  @override
  _$InitialLogScreenState get _value => super._value as _$InitialLogScreenState;
}

/// @nodoc

class _$InitialLogScreenState extends InitialLogScreenState {
  const _$InitialLogScreenState() : super._();

  @override
  String toString() {
    return 'LogScreenState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialLogScreenState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<String> logs) loaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialLogScreenState value) initial,
    required TResult Function(LoadingLogScreenState value) loading,
    required TResult Function(LoadedLogScreenState value) loaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialLogScreenState extends LogScreenState {
  const factory InitialLogScreenState() = _$InitialLogScreenState;
  const InitialLogScreenState._() : super._();
}

/// @nodoc
abstract class _$$LoadingLogScreenStateCopyWith<$Res> {
  factory _$$LoadingLogScreenStateCopyWith(_$LoadingLogScreenState value,
          $Res Function(_$LoadingLogScreenState) then) =
      __$$LoadingLogScreenStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingLogScreenStateCopyWithImpl<$Res>
    extends _$LogScreenStateCopyWithImpl<$Res>
    implements _$$LoadingLogScreenStateCopyWith<$Res> {
  __$$LoadingLogScreenStateCopyWithImpl(_$LoadingLogScreenState _value,
      $Res Function(_$LoadingLogScreenState) _then)
      : super(_value, (v) => _then(v as _$LoadingLogScreenState));

  @override
  _$LoadingLogScreenState get _value => super._value as _$LoadingLogScreenState;
}

/// @nodoc

class _$LoadingLogScreenState extends LoadingLogScreenState {
  const _$LoadingLogScreenState() : super._();

  @override
  String toString() {
    return 'LogScreenState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingLogScreenState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<String> logs) loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialLogScreenState value) initial,
    required TResult Function(LoadingLogScreenState value) loading,
    required TResult Function(LoadedLogScreenState value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingLogScreenState extends LogScreenState {
  const factory LoadingLogScreenState() = _$LoadingLogScreenState;
  const LoadingLogScreenState._() : super._();
}

/// @nodoc
abstract class _$$LoadedLogScreenStateCopyWith<$Res> {
  factory _$$LoadedLogScreenStateCopyWith(_$LoadedLogScreenState value,
          $Res Function(_$LoadedLogScreenState) then) =
      __$$LoadedLogScreenStateCopyWithImpl<$Res>;
  $Res call({List<String> logs});
}

/// @nodoc
class __$$LoadedLogScreenStateCopyWithImpl<$Res>
    extends _$LogScreenStateCopyWithImpl<$Res>
    implements _$$LoadedLogScreenStateCopyWith<$Res> {
  __$$LoadedLogScreenStateCopyWithImpl(_$LoadedLogScreenState _value,
      $Res Function(_$LoadedLogScreenState) _then)
      : super(_value, (v) => _then(v as _$LoadedLogScreenState));

  @override
  _$LoadedLogScreenState get _value => super._value as _$LoadedLogScreenState;

  @override
  $Res call({
    Object? logs = freezed,
  }) {
    return _then(_$LoadedLogScreenState(
      logs: logs == freezed
          ? _value._logs
          : logs // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$LoadedLogScreenState extends LoadedLogScreenState {
  const _$LoadedLogScreenState({required final List<String> logs})
      : _logs = logs,
        super._();

  final List<String> _logs;
  @override
  List<String> get logs {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_logs);
  }

  @override
  String toString() {
    return 'LogScreenState.loaded(logs: $logs)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedLogScreenState &&
            const DeepCollectionEquality().equals(other._logs, _logs));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_logs));

  @JsonKey(ignore: true)
  @override
  _$$LoadedLogScreenStateCopyWith<_$LoadedLogScreenState> get copyWith =>
      __$$LoadedLogScreenStateCopyWithImpl<_$LoadedLogScreenState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<String> logs) loaded,
  }) {
    return loaded(logs);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
  }) {
    return loaded?.call(logs);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<String> logs)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(logs);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialLogScreenState value) initial,
    required TResult Function(LoadingLogScreenState value) loading,
    required TResult Function(LoadedLogScreenState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialLogScreenState value)? initial,
    TResult Function(LoadingLogScreenState value)? loading,
    TResult Function(LoadedLogScreenState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedLogScreenState extends LogScreenState {
  const factory LoadedLogScreenState({required final List<String> logs}) =
      _$LoadedLogScreenState;
  const LoadedLogScreenState._() : super._();

  List<String> get logs;
  @JsonKey(ignore: true)
  _$$LoadedLogScreenStateCopyWith<_$LoadedLogScreenState> get copyWith =>
      throw _privateConstructorUsedError;
}
