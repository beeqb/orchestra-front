import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/services/admin_service.dart';
import '../../models/billing_models/admin_transact_model.dart';

part 'admin_transactions_event.dart';
part 'admin_transactions_state.dart';

class AdminTransactionsBloc
    extends Bloc<AdminTransactionsEvent, AdminTransactionsState> {
  AdminTransactionsBloc({required AdminService adminService})
      : _adminService = adminService,
        super(AdminTransactionsInitial());

  final AdminService _adminService;

  @override
  Stream<AdminTransactionsState> mapEventToState(
    AdminTransactionsEvent event,
  ) async* {
    switch (event.runtimeType) {
      case LoadAdminTransactionsEvent:
        yield* _mapLoadAll();
        break;
      default:
    }
  }

  Stream<AdminTransactionsState> _mapLoadAll() async* {
    yield AdminTransactionsLoading();
    try {
      final result = await _adminService
          .getAllTransactions()
          .timeout(const Duration(seconds: 20));

      yield AdminTransactionsLoaded(transactions: result);
    } on TimeoutException catch (e) {
      debugPrint('AdminTransactionsBloc timeout error $e');
      yield AdminTransactionsFailure(message: e.toString());
    } catch (e) {
      debugPrint('AdminTransactionsBloc error $e');
      yield AdminTransactionsFailure(message: e.toString());
      rethrow;
    }
  }
}
