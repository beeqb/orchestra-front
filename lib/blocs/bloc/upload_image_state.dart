part of 'upload_image_bloc.dart';

abstract class UploadImageState extends Equatable {
  const UploadImageState();

  @override
  List<Object> get props => [];
}

class UploadImageInitial extends UploadImageState {}

class EmptyImageState extends UploadImageState {}

class ImageDeletedState extends UploadImageState {
  final PhotoUrlType fieldName;

  const ImageDeletedState({required this.fieldName});

  @override
  List<Object> get props => [fieldName];

  @override
  String toString() => 'ImageDeletedState(launcherFileName: $fieldName)';
}

class EmptyLauncherFileState extends UploadImageState {}

class UploadedImageState extends UploadImageState {
  final String imageUrl;

  const UploadedImageState({required this.imageUrl});

  @override
  List<Object> get props => [imageUrl];

  @override
  String toString() => 'UploadedImageState(imageUrl: $imageUrl)';
}

class UploadedLauncherFileState extends UploadImageState {
  final String launcherFileName;

  const UploadedLauncherFileState({required this.launcherFileName});

  @override
  List<Object> get props => [launcherFileName];

  @override
  String toString() =>
      'UploadedLauncherFileState(launcherFileName: $launcherFileName)';
}

class AddedImageState extends UploadImageState {
  final PlatformFile imageFile;
  final String fileName;

  const AddedImageState({
    required this.imageFile,
    required this.fileName,
  });

  @override
  List<Object> get props => [imageFile];
}

class ProcessingState extends UploadImageState {}

class UploadImageFailure extends UploadImageState {
  final String message;

  const UploadImageFailure(this.message);

  @override
  List<Object> get props => [message];
}
