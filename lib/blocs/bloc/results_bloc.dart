import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/models/results_model.dart';
import 'package:orchestra/services/app_service.dart';

abstract class ResultsState extends Equatable {
  final List<ResultsModel> results;
  const ResultsState(this.results);

  @override
  List<Object> get props => [];
}

class ResultsInitial extends ResultsState {
  @override
  final List<ResultsModel> results;

  ResultsInitial(this.results) : super([]);
  @override
  List<Object> get props => [results];
}

class ResultsLoading extends ResultsState {
  @override
  final List<ResultsModel> results;

  const ResultsLoading(this.results) : super(results);
  @override
  List<Object> get props => [results];
}

class ResultsLoaded extends ResultsState {
  @override
  final List<ResultsModel> results;

  const ResultsLoaded({
    required this.results,
  }) : super(results);

  @override
  List<Object> get props => [results.first.id];

  @override
  String toString() => 'ResultsLoaded(results: ${results.first.id})';
}

class ResultsCubitFailure extends ResultsState {
  const ResultsCubitFailure(List<ResultsModel> results) : super(results);
}

//==================================================
abstract class ResultsEvent extends Equatable {
  const ResultsEvent();
  @override
  List<Object> get props => [];
}

class FetchResultsEvent extends ResultsEvent {}

class ResetResultsEvent extends ResultsEvent {}

//==================================================

class ResultsBloc extends Bloc<ResultsEvent, ResultsState> {
  final AppService _appService;
  ResultsBloc({
    required AppService appService,
  })  : _appService = appService,
        super(ResultsInitial(const []));

  List<ResultsModel> _list = [];
  int _page = 1;

  @override
  Stream<ResultsState> mapEventToState(ResultsEvent event) async* {
    if (event is FetchResultsEvent) {
      yield* _mapLoadResults();
    } else if (event is ResetResultsEvent) {
      yield* _mapResetResults();
    }
  }

  Stream<ResultsState> _mapResetResults() async* {
    _page = 1;
    _list = [];
  }

  Stream<ResultsState> _mapLoadResults() async* {
    yield ResultsLoading(_list);
    try {
      final _results = await _appService
          .getAppResults(_page)
          .timeout(const Duration(seconds: 20));
      _list.addAll(_results);
      yield ResultsLoaded(results: _list);
      _page++;
    } on TimeoutException catch (e) {
      debugPrint('ResultsCubit timeout error $e');
      yield const ResultsCubitFailure([]);
    } catch (e) {
      debugPrint('ResultsCubit error $e');
      yield const ResultsCubitFailure([]);
      rethrow;
    }
  }
}
