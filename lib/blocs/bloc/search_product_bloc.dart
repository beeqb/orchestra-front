import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/product_service.dart';

part 'search_product_event.dart';
part 'search_product_state.dart';

class SearchProductBloc extends Bloc<SearchProductEvent, SearchProductState> {
  final ProductService _productService;

  SearchProductBloc({
    required ProductService productService,
  })  : _productService = productService,
        super(SearchProductInitial());

// переписал поведение ивентов, чтобы последний из них сразу на исполнение
  @override
  Stream<Transition<SearchProductEvent, SearchProductState>> transformEvents(
      // ignore: type_annotate_public_apis
      events,
      // ignore: type_annotate_public_apis
      transitionFn) {
    return events.switchMap(transitionFn);
  }

  @override
  Stream<SearchProductState> mapEventToState(
    SearchProductEvent event,
  ) async* {
    switch (event.runtimeType) {
      case FetchSearchedProductsEvent:
        yield* _mapFetchSearchedProducts(
            (event as FetchSearchedProductsEvent).query);
        break;
      default:
    }
  }

  Stream<SearchProductState> _mapFetchSearchedProducts(String query) async* {
    try {
      final result = await _productService
          .getProductsByQuery(query)
          .timeout(const Duration(seconds: 20));
      yield SearchProductLoaded(foundProducts: result);
    } on TimeoutException catch (e) {
      debugPrint('TopProductsBloc timeout error ${e.toString()}');
      yield SearchProductFailure(message: e.toString());
    } catch (e) {
      yield SearchProductFailure(message: e.toString());
      rethrow;
    }
  }
}
