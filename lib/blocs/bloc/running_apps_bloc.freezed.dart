// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'running_apps_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RunningAppsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchRunningAppsEvent value) fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchRunningAppsEvent value)? fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchRunningAppsEvent value)? fetch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RunningAppsEventCopyWith<$Res> {
  factory $RunningAppsEventCopyWith(
          RunningAppsEvent value, $Res Function(RunningAppsEvent) then) =
      _$RunningAppsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$RunningAppsEventCopyWithImpl<$Res>
    implements $RunningAppsEventCopyWith<$Res> {
  _$RunningAppsEventCopyWithImpl(this._value, this._then);

  final RunningAppsEvent _value;
  // ignore: unused_field
  final $Res Function(RunningAppsEvent) _then;
}

/// @nodoc
abstract class _$$FetchRunningAppsEventCopyWith<$Res> {
  factory _$$FetchRunningAppsEventCopyWith(_$FetchRunningAppsEvent value,
          $Res Function(_$FetchRunningAppsEvent) then) =
      __$$FetchRunningAppsEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FetchRunningAppsEventCopyWithImpl<$Res>
    extends _$RunningAppsEventCopyWithImpl<$Res>
    implements _$$FetchRunningAppsEventCopyWith<$Res> {
  __$$FetchRunningAppsEventCopyWithImpl(_$FetchRunningAppsEvent _value,
      $Res Function(_$FetchRunningAppsEvent) _then)
      : super(_value, (v) => _then(v as _$FetchRunningAppsEvent));

  @override
  _$FetchRunningAppsEvent get _value => super._value as _$FetchRunningAppsEvent;
}

/// @nodoc

class _$FetchRunningAppsEvent extends FetchRunningAppsEvent
    with DiagnosticableTreeMixin {
  const _$FetchRunningAppsEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RunningAppsEvent.fetch()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'RunningAppsEvent.fetch'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FetchRunningAppsEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() fetch,
  }) {
    return fetch();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? fetch,
  }) {
    return fetch?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? fetch,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchRunningAppsEvent value) fetch,
  }) {
    return fetch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchRunningAppsEvent value)? fetch,
  }) {
    return fetch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchRunningAppsEvent value)? fetch,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch(this);
    }
    return orElse();
  }
}

abstract class FetchRunningAppsEvent extends RunningAppsEvent {
  const factory FetchRunningAppsEvent() = _$FetchRunningAppsEvent;
  const FetchRunningAppsEvent._() : super._();
}

/// @nodoc
mixin _$RunningAppsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<RunningAppModel> runningApps) loaded,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialRunningAppsState value) initial,
    required TResult Function(LoadingRunningAppsState value) loading,
    required TResult Function(LoadedRunningAppsState value) loaded,
    required TResult Function(FailureRunningAppsState value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RunningAppsStateCopyWith<$Res> {
  factory $RunningAppsStateCopyWith(
          RunningAppsState value, $Res Function(RunningAppsState) then) =
      _$RunningAppsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$RunningAppsStateCopyWithImpl<$Res>
    implements $RunningAppsStateCopyWith<$Res> {
  _$RunningAppsStateCopyWithImpl(this._value, this._then);

  final RunningAppsState _value;
  // ignore: unused_field
  final $Res Function(RunningAppsState) _then;
}

/// @nodoc
abstract class _$$InitialRunningAppsStateCopyWith<$Res> {
  factory _$$InitialRunningAppsStateCopyWith(_$InitialRunningAppsState value,
          $Res Function(_$InitialRunningAppsState) then) =
      __$$InitialRunningAppsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialRunningAppsStateCopyWithImpl<$Res>
    extends _$RunningAppsStateCopyWithImpl<$Res>
    implements _$$InitialRunningAppsStateCopyWith<$Res> {
  __$$InitialRunningAppsStateCopyWithImpl(_$InitialRunningAppsState _value,
      $Res Function(_$InitialRunningAppsState) _then)
      : super(_value, (v) => _then(v as _$InitialRunningAppsState));

  @override
  _$InitialRunningAppsState get _value =>
      super._value as _$InitialRunningAppsState;
}

/// @nodoc

class _$InitialRunningAppsState extends InitialRunningAppsState
    with DiagnosticableTreeMixin {
  const _$InitialRunningAppsState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RunningAppsState.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'RunningAppsState.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialRunningAppsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<RunningAppModel> runningApps) loaded,
    required TResult Function() failure,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialRunningAppsState value) initial,
    required TResult Function(LoadingRunningAppsState value) loading,
    required TResult Function(LoadedRunningAppsState value) loaded,
    required TResult Function(FailureRunningAppsState value) failure,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialRunningAppsState extends RunningAppsState {
  const factory InitialRunningAppsState() = _$InitialRunningAppsState;
  const InitialRunningAppsState._() : super._();
}

/// @nodoc
abstract class _$$LoadingRunningAppsStateCopyWith<$Res> {
  factory _$$LoadingRunningAppsStateCopyWith(_$LoadingRunningAppsState value,
          $Res Function(_$LoadingRunningAppsState) then) =
      __$$LoadingRunningAppsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingRunningAppsStateCopyWithImpl<$Res>
    extends _$RunningAppsStateCopyWithImpl<$Res>
    implements _$$LoadingRunningAppsStateCopyWith<$Res> {
  __$$LoadingRunningAppsStateCopyWithImpl(_$LoadingRunningAppsState _value,
      $Res Function(_$LoadingRunningAppsState) _then)
      : super(_value, (v) => _then(v as _$LoadingRunningAppsState));

  @override
  _$LoadingRunningAppsState get _value =>
      super._value as _$LoadingRunningAppsState;
}

/// @nodoc

class _$LoadingRunningAppsState extends LoadingRunningAppsState
    with DiagnosticableTreeMixin {
  const _$LoadingRunningAppsState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RunningAppsState.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'RunningAppsState.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingRunningAppsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<RunningAppModel> runningApps) loaded,
    required TResult Function() failure,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialRunningAppsState value) initial,
    required TResult Function(LoadingRunningAppsState value) loading,
    required TResult Function(LoadedRunningAppsState value) loaded,
    required TResult Function(FailureRunningAppsState value) failure,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingRunningAppsState extends RunningAppsState {
  const factory LoadingRunningAppsState() = _$LoadingRunningAppsState;
  const LoadingRunningAppsState._() : super._();
}

/// @nodoc
abstract class _$$LoadedRunningAppsStateCopyWith<$Res> {
  factory _$$LoadedRunningAppsStateCopyWith(_$LoadedRunningAppsState value,
          $Res Function(_$LoadedRunningAppsState) then) =
      __$$LoadedRunningAppsStateCopyWithImpl<$Res>;
  $Res call({List<RunningAppModel> runningApps});
}

/// @nodoc
class __$$LoadedRunningAppsStateCopyWithImpl<$Res>
    extends _$RunningAppsStateCopyWithImpl<$Res>
    implements _$$LoadedRunningAppsStateCopyWith<$Res> {
  __$$LoadedRunningAppsStateCopyWithImpl(_$LoadedRunningAppsState _value,
      $Res Function(_$LoadedRunningAppsState) _then)
      : super(_value, (v) => _then(v as _$LoadedRunningAppsState));

  @override
  _$LoadedRunningAppsState get _value =>
      super._value as _$LoadedRunningAppsState;

  @override
  $Res call({
    Object? runningApps = freezed,
  }) {
    return _then(_$LoadedRunningAppsState(
      runningApps: runningApps == freezed
          ? _value._runningApps
          : runningApps // ignore: cast_nullable_to_non_nullable
              as List<RunningAppModel>,
    ));
  }
}

/// @nodoc

class _$LoadedRunningAppsState extends LoadedRunningAppsState
    with DiagnosticableTreeMixin {
  const _$LoadedRunningAppsState(
      {required final List<RunningAppModel> runningApps})
      : _runningApps = runningApps,
        super._();

  final List<RunningAppModel> _runningApps;
  @override
  List<RunningAppModel> get runningApps {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_runningApps);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RunningAppsState.loaded(runningApps: $runningApps)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'RunningAppsState.loaded'))
      ..add(DiagnosticsProperty('runningApps', runningApps));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedRunningAppsState &&
            const DeepCollectionEquality()
                .equals(other._runningApps, _runningApps));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_runningApps));

  @JsonKey(ignore: true)
  @override
  _$$LoadedRunningAppsStateCopyWith<_$LoadedRunningAppsState> get copyWith =>
      __$$LoadedRunningAppsStateCopyWithImpl<_$LoadedRunningAppsState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<RunningAppModel> runningApps) loaded,
    required TResult Function() failure,
  }) {
    return loaded(runningApps);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
  }) {
    return loaded?.call(runningApps);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(runningApps);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialRunningAppsState value) initial,
    required TResult Function(LoadingRunningAppsState value) loading,
    required TResult Function(LoadedRunningAppsState value) loaded,
    required TResult Function(FailureRunningAppsState value) failure,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedRunningAppsState extends RunningAppsState {
  const factory LoadedRunningAppsState(
          {required final List<RunningAppModel> runningApps}) =
      _$LoadedRunningAppsState;
  const LoadedRunningAppsState._() : super._();

  List<RunningAppModel> get runningApps;
  @JsonKey(ignore: true)
  _$$LoadedRunningAppsStateCopyWith<_$LoadedRunningAppsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailureRunningAppsStateCopyWith<$Res> {
  factory _$$FailureRunningAppsStateCopyWith(_$FailureRunningAppsState value,
          $Res Function(_$FailureRunningAppsState) then) =
      __$$FailureRunningAppsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailureRunningAppsStateCopyWithImpl<$Res>
    extends _$RunningAppsStateCopyWithImpl<$Res>
    implements _$$FailureRunningAppsStateCopyWith<$Res> {
  __$$FailureRunningAppsStateCopyWithImpl(_$FailureRunningAppsState _value,
      $Res Function(_$FailureRunningAppsState) _then)
      : super(_value, (v) => _then(v as _$FailureRunningAppsState));

  @override
  _$FailureRunningAppsState get _value =>
      super._value as _$FailureRunningAppsState;
}

/// @nodoc

class _$FailureRunningAppsState extends FailureRunningAppsState
    with DiagnosticableTreeMixin {
  const _$FailureRunningAppsState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'RunningAppsState.failure()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'RunningAppsState.failure'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FailureRunningAppsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<RunningAppModel> runningApps) loaded,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<RunningAppModel> runningApps)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialRunningAppsState value) initial,
    required TResult Function(LoadingRunningAppsState value) loading,
    required TResult Function(LoadedRunningAppsState value) loaded,
    required TResult Function(FailureRunningAppsState value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialRunningAppsState value)? initial,
    TResult Function(LoadingRunningAppsState value)? loading,
    TResult Function(LoadedRunningAppsState value)? loaded,
    TResult Function(FailureRunningAppsState value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class FailureRunningAppsState extends RunningAppsState {
  const factory FailureRunningAppsState() = _$FailureRunningAppsState;
  const FailureRunningAppsState._() : super._();
}
