part of 'contributor_bloc.dart';

abstract class ContributorEvent extends Equatable {
  const ContributorEvent();

  @override
  List<Object> get props => [];
}

class BecomeContributorEvent extends ContributorEvent {}

class SetContributorEvent extends ContributorEvent {
  final bool isContributor;

  const SetContributorEvent({required this.isContributor});

  @override
  bool get stringify => true;
}
