import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'onboarding_nav_bloc.freezed.dart';

@freezed
class OnboardingNavEvent with _$OnboardingNavEvent {
  const OnboardingNavEvent._();

  const factory OnboardingNavEvent.show() = ShowOnboardingNavEvent;
  const factory OnboardingNavEvent.hide() = HideOnboardingNavEvent;
}

@freezed
class OnboardingNavState with _$OnboardingNavState {
  const OnboardingNavState._();

  const factory OnboardingNavState.initial() = InitialOnboardingNavState;
  const factory OnboardingNavState.shown() = ShownOnboardingNavState;
  const factory OnboardingNavState.hidden() = HiddenOnboardingNavState;
}

class OnboardingNavBLoC extends Bloc<OnboardingNavEvent, OnboardingNavState> {
  OnboardingNavBLoC() : super(const InitialOnboardingNavState()) {
    add(const ShowOnboardingNavEvent());
  }

  @override
  Stream<OnboardingNavState> mapEventToState(OnboardingNavEvent event) =>
      event.when<Stream<OnboardingNavState>>(
        hide: _hide,
        show: _show,
      );

  Stream<OnboardingNavState> _hide() async* {
    yield const HiddenOnboardingNavState();
  }

  Stream<OnboardingNavState> _show() async* {
    yield const ShownOnboardingNavState();
  }
}
