part of 'top_products_bloc.dart';

abstract class TopProductsByCategoriesState extends Equatable {
  const TopProductsByCategoriesState();

  @override
  List<Object> get props => [];
}

class TopProductsInitial extends TopProductsByCategoriesState {}

class TopProductsLoading extends TopProductsByCategoriesState {}

class TopProductsLoaded extends TopProductsByCategoriesState {
  final List<AppCategory> topProductsByCategories;
  final List<Product?> topAllProducts;
  final Map<String, String> productsWithRatings;

  const TopProductsLoaded({
    required this.topProductsByCategories,
    required this.topAllProducts,
    required this.productsWithRatings,
  });

  @override
  List<Object> get props => [
        topProductsByCategories,
        topAllProducts,
        productsWithRatings,
      ];
}

class TopProductsFailure extends TopProductsByCategoriesState {}
