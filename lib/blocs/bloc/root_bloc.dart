import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/app.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/services/app_service.dart';
import 'package:orchestra/services/product_service.dart';
import 'package:orchestra/services/timers_injections.dart';

import 'added_products_bloc.dart';
import 'auth_bloc.dart';
import 'top_products_bloc.dart';

abstract class RootEvent {}

class PendingRootEvent extends RootEvent {}

class StartLoadingAppsRootEvent extends RootEvent {
  StartLoadingAppsRootEvent();
}

class ReadyRootEvent extends RootEvent {
  final List<AppModel> apps;

  ReadyRootEvent(this.apps);
}

class StartCreatingAppRootEvent extends RootEvent {
  final AppModel app;

  StartCreatingAppRootEvent(this.app);
}

class StartCreatingAppFromWidgetEvent extends RootEvent {
  final AppModel app;
  final Product product;

  StartCreatingAppFromWidgetEvent(this.app, this.product);
}

class StartUpdatingAppEvent extends RootEvent {
  final AppModel app;

  StartUpdatingAppEvent(this.app);
}

class StartDeletingAppEvent extends RootEvent {
  final AppModel app;

  StartDeletingAppEvent(this.app);
}

//=======================================================

abstract class RootState extends Equatable {
  const RootState();
  @override
  List<Object> get props => [];
}

class InitialRootState extends RootState {}

class PendingRootState extends RootState {}

class ProcessingRootState extends RootState {}

class ReadyRootState extends RootState {
  final List<AppModel> apps;

  const ReadyRootState(this.apps);
}

class RootBlocFailure extends RootState {}

//=================================================================

class RootBloc extends Bloc<RootEvent, RootState> {
  final AuthBloc _authBloc;
  final TopProductsBlocByCategories _topProductsBloc;
  final AddedProductsBloc _addedProductsBloc;
  final AppService _appService;
  final ProductService _productService;

  StreamSubscription? authSubscription;
  List<AppModel>? userApps;
  final TimersInjections _timersInjections;

  RootBloc({
    required AuthBloc authBloc,
    required TopProductsBlocByCategories topProductsBloc,
    required AddedProductsBloc addedProductsBloc,
    required AppService appService,
    required ProductService productService,
    required TimersInjections timersInjections,
  })  : _authBloc = authBloc,
        _topProductsBloc = topProductsBloc,
        _addedProductsBloc = addedProductsBloc,
        _appService = appService,
        _productService = productService,
        _timersInjections = timersInjections,
        super(InitialRootState()) {
    authSubscription = _authBloc.stream.listen((state) async {
      if (state is LoggedAuthState) {
        add(StartLoadingAppsRootEvent());
        _addedProductsBloc.add(GetProductsByIdEvent());
      }
    });
  }

  @override
  Future<void> close() {
    authSubscription!.cancel();
    return super.close();
  }

  @override
  Stream<RootState> mapEventToState(RootEvent event) async* {
    switch (event.runtimeType) {
      case PendingRootEvent:
        yield PendingRootState();
        break;

      case StartLoadingAppsRootEvent:
        yield* _mapStartLoadingApps();
        break;

      case ReadyRootEvent:
        final apps = (event as ReadyRootEvent).apps;
        yield ReadyRootState(apps);
        break;

      case StartCreatingAppRootEvent:
        yield* _mapStartCreatingApp((event as StartCreatingAppRootEvent).app);
        break;

      case StartCreatingAppFromWidgetEvent:
        yield* _mapStartCreatingAppFromWidget(
          appModel: (event as StartCreatingAppFromWidgetEvent).app,
          product: (event).product,
        );
        break;

      case StartDeletingAppEvent:
        yield* _mapStartDeletingApp(
            appModel: (event as StartDeletingAppEvent).app);
        break;

/*       case StartUpdatingAppEvent:
        yield* _mapStartUpdatingApp(
            appModel: (event as StartUpdatingAppEvent).app);
        break; */
      default:
        throw 'Unknown event';
    }
  }

  Stream<RootState> _mapStartLoadingApps() async* {
    try {
      await _appService.getAppsByUserId().then((apps) {
        if (apps.isNotEmpty) {
          apps.sort((a, b) => DateTime.parse(b.createdAt!)
              .compareTo(DateTime.parse(a.createdAt!)));
        }
        userApps = apps;
        //add singletones for timers
        _timersInjections.addTimersToGetIt(apps: apps);

        return add(ReadyRootEvent(apps));
      }).timeout(const Duration(seconds: 2));
    } on TimeoutException catch (e) {
      debugPrint('RootBloc timeout error $e');
      yield RootBlocFailure();
    } catch (e) {
      debugPrint('RootBloc error $e');
      yield RootBlocFailure();
      //rethrow;
    }
    yield ProcessingRootState();
  }

  Stream<RootState> _mapStartCreatingApp(AppModel appModel) async* {
    try {
      await _appService
          .saveApp(appModel)
          .then((value) => add(StartLoadingAppsRootEvent()))
          .timeout(const Duration(seconds: 20));
    } on TimeoutException catch (e) {
      debugPrint('RootBloc timeout error $e');
      yield RootBlocFailure();
    } catch (e) {
      debugPrint('RootBloc error $e');
      yield RootBlocFailure();
      rethrow;
    }
    yield ProcessingRootState();
  }

  Stream<RootState> _mapStartCreatingAppFromWidget(
      {required AppModel appModel, required Product product}) async* {
    try {
      await _appService.saveApp(appModel).then((value) {
        _productService.buyProduct(product.id);
        _productService
            .saveWidgetForApp(
              product,
              value.id!,
              value.userId,
            )
            .then((prod) => add(StartLoadingAppsRootEvent()))
            .timeout(const Duration(seconds: 20));
      });
    } on TimeoutException catch (e) {
      debugPrint('RootBloc timeout error $e');
      yield RootBlocFailure();
    } catch (e) {
      debugPrint('RootBloc error $e');
      yield RootBlocFailure();
      rethrow;
    }
    yield ProcessingRootState();
  }

  Stream<RootState> _mapStartDeletingApp({required AppModel appModel}) async* {
    try {
      await _appService.deleteApp(appModel).then((value) {
        add(StartLoadingAppsRootEvent());
        _topProductsBloc.add(LoadTopProductsEvent());
      }).timeout(const Duration(seconds: 2));
    } on TimeoutException catch (e) {
      debugPrint('RootBloc timeout error $e');
      yield RootBlocFailure();
    } catch (e) {
      debugPrint('RootBloc error $e');
      yield RootBlocFailure();
      rethrow;
    }
    yield ProcessingRootState();
  }
}
