part of 'all_users_bloc.dart';

abstract class AllUsersEvent extends Equatable {
  const AllUsersEvent();

  @override
  List<Object> get props => [];
}

class FetchAllUsersEvent extends AllUsersEvent {}

class SearchUsersEvent extends AllUsersEvent {
  final String query;

  const SearchUsersEvent({required this.query});
}

class SortAllUsersEvent extends AllUsersEvent {
  final Comparable Function(UserModel d) getField;
  final bool ascending;
  final int sortColumnIndex;

  const SortAllUsersEvent({
    required this.getField,
    required this.ascending,
    required this.sortColumnIndex,
  });

  @override
  List<Object> get props => [ ascending];
}
