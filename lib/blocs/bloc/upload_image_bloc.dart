import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/models/product/product.dart';

import '../../injection_container.dart';
import '../../models/enums/photo_type.dart';
import '../../models/user.dart';
import '../../services/product_service.dart';
import '../../util/text_util.dart';
import 'auth_bloc.dart';

part 'upload_image_event.dart';
part 'upload_image_state.dart';

class UploadImageBloc extends Bloc<UploadImageEvent, UploadImageState> {
  final PhotoUrlType fieldName;
  final Product? editProduct;
  final List<String> fileTypes;
  final UserModel contributor;
  final _completer = Completer();

  UploadImageBloc({
    required this.fieldName,
    required this.editProduct,
    required this.fileTypes,
    required this.contributor,
  }) : super(UploadImageInitial());

  @override
  Stream<UploadImageState> mapEventToState(
    UploadImageEvent event,
  ) async* {
    switch (event.runtimeType) {
      case DeleteImage:
        yield* _mapDeleteImage((event as DeleteImage).fieldName);
        break;
      case ShowCurrentImage:
        yield* _mapShowCurrentImage();
        break;
      case UploadImage:
        yield* _mapUploadImageToServer();
        break;
      case UploadImageCompleted:
        yield* _mapUploadCompleted(
          (event as UploadImageCompleted).imageFile,
          (event).fileName,
        );
        break;
      case UploadLauncherFileCompleted:
        yield* _mapLauncherUploadCompleted(
            (event as UploadLauncherFileCompleted).fileName);
        break;
      case ImageUploadCanceled:
        yield* _mapImageUploadCanceled();
        break;
      case FileUploadCanceled:
        yield* _mapFileUploadCanceled();
        break;

      default:
        throw 'UPLOAD_IMAGE_BLOC: Unknown event';
    }
  }

  Stream<UploadImageState> _mapUploadImageToServer() async* {
    yield ProcessingState();

    try {
      await uploadFile();
    } on TimeoutException catch (e) {
      debugPrint('_mapUploadImageToServer $e');
      yield EmptyImageState();
    }
  }

  Stream<UploadImageState> _mapUploadCompleted(
    PlatformFile imageFile,
    String fileName,
  ) async* {
    yield AddedImageState(imageFile: imageFile, fileName: fileName);
  }

  Stream<UploadImageState> _mapLauncherUploadCompleted(String fileName) async* {
    yield UploadedLauncherFileState(launcherFileName: fileName);
  }

  Stream<UploadImageState> _mapShowCurrentImage() async* {
    if (editProduct != null) {
      final _imageUrl = getImageUrl(
        editProduct: editProduct!,
        fieldName: fieldName,
      );
      if (_imageUrl != 'error') {
        if (fieldName == PhotoUrlType.launcherFileUrl) {
          yield UploadedLauncherFileState(launcherFileName: _imageUrl);
        } else {
          yield UploadedImageState(imageUrl: _imageUrl);
        }
      } else {
        yield EmptyImageState();
      }
    } else if (fieldName != PhotoUrlType.launcherFileUrl) {
      yield EmptyImageState();
    } else {
      yield EmptyLauncherFileState();
    }
  }

  Stream<UploadImageState> _mapDeleteImage(
    PhotoUrlType fileName,
  ) async* {
    yield ProcessingState();
    yield ImageDeletedState(fieldName: fieldName);
  }

  Stream<UploadImageState> _mapImageUploadCanceled() async* {
    yield EmptyImageState();
  }

  Stream<UploadImageState> _mapFileUploadCanceled() async* {
    yield EmptyLauncherFileState();
  }

  Future uploadFile() async {
    uploadImage();

    return _completer.future;
  }

  Future uploadImage() async {
    FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(
        type: fieldName == PhotoUrlType.launcherFileUrl
            ? FileType.custom
            : FileType.image,
        allowedExtensions:
            fieldName == PhotoUrlType.launcherFileUrl ? fileTypes : null,
        allowMultiple: false,
        withData: true);
    if (filePickerResult != null) {
      final _file = filePickerResult.files.first;
      final authState = sl<AuthBloc>().state as LoggedAuthState;
      if (fieldName != PhotoUrlType.launcherFileUrl) {
        await sl<ProductService>().uploadFile(filePickerResult.files.first,
            fieldName.name, authState.currentUser.id!);
        add(UploadImageCompleted(imageFile: _file, fileName: _file.name));
        if (!_completer.isCompleted) {
          _completer.complete();
        }
      } else if (fieldName == PhotoUrlType.launcherFileUrl) {
        await sl<ProductService>().uploadFile(filePickerResult.files.first,
            fieldName.name, authState.currentUser.id!);
        add(UploadLauncherFileCompleted(fileName: _file.name));
        if (!_completer.isCompleted) {
          _completer.complete();
        }
      }
    } else if (fieldName != PhotoUrlType.launcherFileUrl) {
      add(ImageUploadCanceled());
      if (!_completer.isCompleted) {
        _completer.complete();
      }
    } else {
      add(FileUploadCanceled());
      if (!_completer.isCompleted) {
        _completer.complete();
      }
    }
  }
}
