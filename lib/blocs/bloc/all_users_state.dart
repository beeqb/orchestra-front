part of 'all_users_bloc.dart';

abstract class AllUsersState extends Equatable {
  const AllUsersState();

  @override
  List<Object> get props => [];
}

class AllUsersInitial extends AllUsersState {}

class AllUsersLoading extends AllUsersState {}

class AllUsersLoaded extends AllUsersState {
  final List<UserModel> users;
  final int? sortColumnIndex;
  final bool sortAscending;

  const AllUsersLoaded({
    required this.users,
    this.sortColumnIndex,
    required this.sortAscending,
  });

  @override
  List<Object> get props => [
        users,
        sortAscending,
      ];
}

class AllUsersFailure extends AllUsersState {
  final String message;

  const AllUsersFailure({required this.message});
}
