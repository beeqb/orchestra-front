// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'onboarding_nav_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$OnboardingNavEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() show,
    required TResult Function() hide,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ShowOnboardingNavEvent value) show,
    required TResult Function(HideOnboardingNavEvent value) hide,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OnboardingNavEventCopyWith<$Res> {
  factory $OnboardingNavEventCopyWith(
          OnboardingNavEvent value, $Res Function(OnboardingNavEvent) then) =
      _$OnboardingNavEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$OnboardingNavEventCopyWithImpl<$Res>
    implements $OnboardingNavEventCopyWith<$Res> {
  _$OnboardingNavEventCopyWithImpl(this._value, this._then);

  final OnboardingNavEvent _value;
  // ignore: unused_field
  final $Res Function(OnboardingNavEvent) _then;
}

/// @nodoc
abstract class _$$ShowOnboardingNavEventCopyWith<$Res> {
  factory _$$ShowOnboardingNavEventCopyWith(_$ShowOnboardingNavEvent value,
          $Res Function(_$ShowOnboardingNavEvent) then) =
      __$$ShowOnboardingNavEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ShowOnboardingNavEventCopyWithImpl<$Res>
    extends _$OnboardingNavEventCopyWithImpl<$Res>
    implements _$$ShowOnboardingNavEventCopyWith<$Res> {
  __$$ShowOnboardingNavEventCopyWithImpl(_$ShowOnboardingNavEvent _value,
      $Res Function(_$ShowOnboardingNavEvent) _then)
      : super(_value, (v) => _then(v as _$ShowOnboardingNavEvent));

  @override
  _$ShowOnboardingNavEvent get _value =>
      super._value as _$ShowOnboardingNavEvent;
}

/// @nodoc

class _$ShowOnboardingNavEvent extends ShowOnboardingNavEvent {
  const _$ShowOnboardingNavEvent() : super._();

  @override
  String toString() {
    return 'OnboardingNavEvent.show()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ShowOnboardingNavEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() show,
    required TResult Function() hide,
  }) {
    return show();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
  }) {
    return show?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
    required TResult orElse(),
  }) {
    if (show != null) {
      return show();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ShowOnboardingNavEvent value) show,
    required TResult Function(HideOnboardingNavEvent value) hide,
  }) {
    return show(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
  }) {
    return show?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
    required TResult orElse(),
  }) {
    if (show != null) {
      return show(this);
    }
    return orElse();
  }
}

abstract class ShowOnboardingNavEvent extends OnboardingNavEvent {
  const factory ShowOnboardingNavEvent() = _$ShowOnboardingNavEvent;
  const ShowOnboardingNavEvent._() : super._();
}

/// @nodoc
abstract class _$$HideOnboardingNavEventCopyWith<$Res> {
  factory _$$HideOnboardingNavEventCopyWith(_$HideOnboardingNavEvent value,
          $Res Function(_$HideOnboardingNavEvent) then) =
      __$$HideOnboardingNavEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HideOnboardingNavEventCopyWithImpl<$Res>
    extends _$OnboardingNavEventCopyWithImpl<$Res>
    implements _$$HideOnboardingNavEventCopyWith<$Res> {
  __$$HideOnboardingNavEventCopyWithImpl(_$HideOnboardingNavEvent _value,
      $Res Function(_$HideOnboardingNavEvent) _then)
      : super(_value, (v) => _then(v as _$HideOnboardingNavEvent));

  @override
  _$HideOnboardingNavEvent get _value =>
      super._value as _$HideOnboardingNavEvent;
}

/// @nodoc

class _$HideOnboardingNavEvent extends HideOnboardingNavEvent {
  const _$HideOnboardingNavEvent() : super._();

  @override
  String toString() {
    return 'OnboardingNavEvent.hide()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$HideOnboardingNavEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() show,
    required TResult Function() hide,
  }) {
    return hide();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
  }) {
    return hide?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? show,
    TResult Function()? hide,
    required TResult orElse(),
  }) {
    if (hide != null) {
      return hide();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ShowOnboardingNavEvent value) show,
    required TResult Function(HideOnboardingNavEvent value) hide,
  }) {
    return hide(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
  }) {
    return hide?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ShowOnboardingNavEvent value)? show,
    TResult Function(HideOnboardingNavEvent value)? hide,
    required TResult orElse(),
  }) {
    if (hide != null) {
      return hide(this);
    }
    return orElse();
  }
}

abstract class HideOnboardingNavEvent extends OnboardingNavEvent {
  const factory HideOnboardingNavEvent() = _$HideOnboardingNavEvent;
  const HideOnboardingNavEvent._() : super._();
}

/// @nodoc
mixin _$OnboardingNavState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() shown,
    required TResult Function() hidden,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialOnboardingNavState value) initial,
    required TResult Function(ShownOnboardingNavState value) shown,
    required TResult Function(HiddenOnboardingNavState value) hidden,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OnboardingNavStateCopyWith<$Res> {
  factory $OnboardingNavStateCopyWith(
          OnboardingNavState value, $Res Function(OnboardingNavState) then) =
      _$OnboardingNavStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$OnboardingNavStateCopyWithImpl<$Res>
    implements $OnboardingNavStateCopyWith<$Res> {
  _$OnboardingNavStateCopyWithImpl(this._value, this._then);

  final OnboardingNavState _value;
  // ignore: unused_field
  final $Res Function(OnboardingNavState) _then;
}

/// @nodoc
abstract class _$$InitialOnboardingNavStateCopyWith<$Res> {
  factory _$$InitialOnboardingNavStateCopyWith(
          _$InitialOnboardingNavState value,
          $Res Function(_$InitialOnboardingNavState) then) =
      __$$InitialOnboardingNavStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialOnboardingNavStateCopyWithImpl<$Res>
    extends _$OnboardingNavStateCopyWithImpl<$Res>
    implements _$$InitialOnboardingNavStateCopyWith<$Res> {
  __$$InitialOnboardingNavStateCopyWithImpl(_$InitialOnboardingNavState _value,
      $Res Function(_$InitialOnboardingNavState) _then)
      : super(_value, (v) => _then(v as _$InitialOnboardingNavState));

  @override
  _$InitialOnboardingNavState get _value =>
      super._value as _$InitialOnboardingNavState;
}

/// @nodoc

class _$InitialOnboardingNavState extends InitialOnboardingNavState {
  const _$InitialOnboardingNavState() : super._();

  @override
  String toString() {
    return 'OnboardingNavState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialOnboardingNavState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() shown,
    required TResult Function() hidden,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialOnboardingNavState value) initial,
    required TResult Function(ShownOnboardingNavState value) shown,
    required TResult Function(HiddenOnboardingNavState value) hidden,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialOnboardingNavState extends OnboardingNavState {
  const factory InitialOnboardingNavState() = _$InitialOnboardingNavState;
  const InitialOnboardingNavState._() : super._();
}

/// @nodoc
abstract class _$$ShownOnboardingNavStateCopyWith<$Res> {
  factory _$$ShownOnboardingNavStateCopyWith(_$ShownOnboardingNavState value,
          $Res Function(_$ShownOnboardingNavState) then) =
      __$$ShownOnboardingNavStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ShownOnboardingNavStateCopyWithImpl<$Res>
    extends _$OnboardingNavStateCopyWithImpl<$Res>
    implements _$$ShownOnboardingNavStateCopyWith<$Res> {
  __$$ShownOnboardingNavStateCopyWithImpl(_$ShownOnboardingNavState _value,
      $Res Function(_$ShownOnboardingNavState) _then)
      : super(_value, (v) => _then(v as _$ShownOnboardingNavState));

  @override
  _$ShownOnboardingNavState get _value =>
      super._value as _$ShownOnboardingNavState;
}

/// @nodoc

class _$ShownOnboardingNavState extends ShownOnboardingNavState {
  const _$ShownOnboardingNavState() : super._();

  @override
  String toString() {
    return 'OnboardingNavState.shown()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShownOnboardingNavState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() shown,
    required TResult Function() hidden,
  }) {
    return shown();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
  }) {
    return shown?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
    required TResult orElse(),
  }) {
    if (shown != null) {
      return shown();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialOnboardingNavState value) initial,
    required TResult Function(ShownOnboardingNavState value) shown,
    required TResult Function(HiddenOnboardingNavState value) hidden,
  }) {
    return shown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
  }) {
    return shown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
    required TResult orElse(),
  }) {
    if (shown != null) {
      return shown(this);
    }
    return orElse();
  }
}

abstract class ShownOnboardingNavState extends OnboardingNavState {
  const factory ShownOnboardingNavState() = _$ShownOnboardingNavState;
  const ShownOnboardingNavState._() : super._();
}

/// @nodoc
abstract class _$$HiddenOnboardingNavStateCopyWith<$Res> {
  factory _$$HiddenOnboardingNavStateCopyWith(_$HiddenOnboardingNavState value,
          $Res Function(_$HiddenOnboardingNavState) then) =
      __$$HiddenOnboardingNavStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HiddenOnboardingNavStateCopyWithImpl<$Res>
    extends _$OnboardingNavStateCopyWithImpl<$Res>
    implements _$$HiddenOnboardingNavStateCopyWith<$Res> {
  __$$HiddenOnboardingNavStateCopyWithImpl(_$HiddenOnboardingNavState _value,
      $Res Function(_$HiddenOnboardingNavState) _then)
      : super(_value, (v) => _then(v as _$HiddenOnboardingNavState));

  @override
  _$HiddenOnboardingNavState get _value =>
      super._value as _$HiddenOnboardingNavState;
}

/// @nodoc

class _$HiddenOnboardingNavState extends HiddenOnboardingNavState {
  const _$HiddenOnboardingNavState() : super._();

  @override
  String toString() {
    return 'OnboardingNavState.hidden()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HiddenOnboardingNavState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() shown,
    required TResult Function() hidden,
  }) {
    return hidden();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
  }) {
    return hidden?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? shown,
    TResult Function()? hidden,
    required TResult orElse(),
  }) {
    if (hidden != null) {
      return hidden();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialOnboardingNavState value) initial,
    required TResult Function(ShownOnboardingNavState value) shown,
    required TResult Function(HiddenOnboardingNavState value) hidden,
  }) {
    return hidden(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
  }) {
    return hidden?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialOnboardingNavState value)? initial,
    TResult Function(ShownOnboardingNavState value)? shown,
    TResult Function(HiddenOnboardingNavState value)? hidden,
    required TResult orElse(),
  }) {
    if (hidden != null) {
      return hidden(this);
    }
    return orElse();
  }
}

abstract class HiddenOnboardingNavState extends OnboardingNavState {
  const factory HiddenOnboardingNavState() = _$HiddenOnboardingNavState;
  const HiddenOnboardingNavState._() : super._();
}
