// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'top_ten_products_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$TopTenProductsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() read,
    required TResult Function() update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReadTopTenProductsEvent value) read,
    required TResult Function(UpdateTopTenProductsEvent value) update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TopTenProductsEventCopyWith<$Res> {
  factory $TopTenProductsEventCopyWith(
          TopTenProductsEvent value, $Res Function(TopTenProductsEvent) then) =
      _$TopTenProductsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$TopTenProductsEventCopyWithImpl<$Res>
    implements $TopTenProductsEventCopyWith<$Res> {
  _$TopTenProductsEventCopyWithImpl(this._value, this._then);

  final TopTenProductsEvent _value;
  // ignore: unused_field
  final $Res Function(TopTenProductsEvent) _then;
}

/// @nodoc
abstract class _$$ReadTopTenProductsEventCopyWith<$Res> {
  factory _$$ReadTopTenProductsEventCopyWith(_$ReadTopTenProductsEvent value,
          $Res Function(_$ReadTopTenProductsEvent) then) =
      __$$ReadTopTenProductsEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReadTopTenProductsEventCopyWithImpl<$Res>
    extends _$TopTenProductsEventCopyWithImpl<$Res>
    implements _$$ReadTopTenProductsEventCopyWith<$Res> {
  __$$ReadTopTenProductsEventCopyWithImpl(_$ReadTopTenProductsEvent _value,
      $Res Function(_$ReadTopTenProductsEvent) _then)
      : super(_value, (v) => _then(v as _$ReadTopTenProductsEvent));

  @override
  _$ReadTopTenProductsEvent get _value =>
      super._value as _$ReadTopTenProductsEvent;
}

/// @nodoc

class _$ReadTopTenProductsEvent extends ReadTopTenProductsEvent {
  const _$ReadTopTenProductsEvent() : super._();

  @override
  String toString() {
    return 'TopTenProductsEvent.read()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadTopTenProductsEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() read,
    required TResult Function() update,
  }) {
    return read();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
  }) {
    return read?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReadTopTenProductsEvent value) read,
    required TResult Function(UpdateTopTenProductsEvent value) update,
  }) {
    return read(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
  }) {
    return read?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(this);
    }
    return orElse();
  }
}

abstract class ReadTopTenProductsEvent extends TopTenProductsEvent {
  const factory ReadTopTenProductsEvent() = _$ReadTopTenProductsEvent;
  const ReadTopTenProductsEvent._() : super._();
}

/// @nodoc
abstract class _$$UpdateTopTenProductsEventCopyWith<$Res> {
  factory _$$UpdateTopTenProductsEventCopyWith(
          _$UpdateTopTenProductsEvent value,
          $Res Function(_$UpdateTopTenProductsEvent) then) =
      __$$UpdateTopTenProductsEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UpdateTopTenProductsEventCopyWithImpl<$Res>
    extends _$TopTenProductsEventCopyWithImpl<$Res>
    implements _$$UpdateTopTenProductsEventCopyWith<$Res> {
  __$$UpdateTopTenProductsEventCopyWithImpl(_$UpdateTopTenProductsEvent _value,
      $Res Function(_$UpdateTopTenProductsEvent) _then)
      : super(_value, (v) => _then(v as _$UpdateTopTenProductsEvent));

  @override
  _$UpdateTopTenProductsEvent get _value =>
      super._value as _$UpdateTopTenProductsEvent;
}

/// @nodoc

class _$UpdateTopTenProductsEvent extends UpdateTopTenProductsEvent {
  const _$UpdateTopTenProductsEvent() : super._();

  @override
  String toString() {
    return 'TopTenProductsEvent.update()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateTopTenProductsEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() read,
    required TResult Function() update,
  }) {
    return update();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
  }) {
    return update?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? read,
    TResult Function()? update,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReadTopTenProductsEvent value) read,
    required TResult Function(UpdateTopTenProductsEvent value) update,
  }) {
    return update(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
  }) {
    return update?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReadTopTenProductsEvent value)? read,
    TResult Function(UpdateTopTenProductsEvent value)? update,
    required TResult orElse(),
  }) {
    if (update != null) {
      return update(this);
    }
    return orElse();
  }
}

abstract class UpdateTopTenProductsEvent extends TopTenProductsEvent {
  const factory UpdateTopTenProductsEvent() = _$UpdateTopTenProductsEvent;
  const UpdateTopTenProductsEvent._() : super._();
}

/// @nodoc
mixin _$TopTenProductsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Product> products) loaded,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTopTenProductsState value) initial,
    required TResult Function(LoadingTopTenProductsState value) loading,
    required TResult Function(LoadedTopTenProductsState value) loaded,
    required TResult Function(FailureTopTenProductsState value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TopTenProductsStateCopyWith<$Res> {
  factory $TopTenProductsStateCopyWith(
          TopTenProductsState value, $Res Function(TopTenProductsState) then) =
      _$TopTenProductsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$TopTenProductsStateCopyWithImpl<$Res>
    implements $TopTenProductsStateCopyWith<$Res> {
  _$TopTenProductsStateCopyWithImpl(this._value, this._then);

  final TopTenProductsState _value;
  // ignore: unused_field
  final $Res Function(TopTenProductsState) _then;
}

/// @nodoc
abstract class _$$InitialTopTenProductsStateCopyWith<$Res> {
  factory _$$InitialTopTenProductsStateCopyWith(
          _$InitialTopTenProductsState value,
          $Res Function(_$InitialTopTenProductsState) then) =
      __$$InitialTopTenProductsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialTopTenProductsStateCopyWithImpl<$Res>
    extends _$TopTenProductsStateCopyWithImpl<$Res>
    implements _$$InitialTopTenProductsStateCopyWith<$Res> {
  __$$InitialTopTenProductsStateCopyWithImpl(
      _$InitialTopTenProductsState _value,
      $Res Function(_$InitialTopTenProductsState) _then)
      : super(_value, (v) => _then(v as _$InitialTopTenProductsState));

  @override
  _$InitialTopTenProductsState get _value =>
      super._value as _$InitialTopTenProductsState;
}

/// @nodoc

class _$InitialTopTenProductsState extends InitialTopTenProductsState {
  const _$InitialTopTenProductsState() : super._();

  @override
  String toString() {
    return 'TopTenProductsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialTopTenProductsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Product> products) loaded,
    required TResult Function() failure,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTopTenProductsState value) initial,
    required TResult Function(LoadingTopTenProductsState value) loading,
    required TResult Function(LoadedTopTenProductsState value) loaded,
    required TResult Function(FailureTopTenProductsState value) failure,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialTopTenProductsState extends TopTenProductsState {
  const factory InitialTopTenProductsState() = _$InitialTopTenProductsState;
  const InitialTopTenProductsState._() : super._();
}

/// @nodoc
abstract class _$$LoadingTopTenProductsStateCopyWith<$Res> {
  factory _$$LoadingTopTenProductsStateCopyWith(
          _$LoadingTopTenProductsState value,
          $Res Function(_$LoadingTopTenProductsState) then) =
      __$$LoadingTopTenProductsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingTopTenProductsStateCopyWithImpl<$Res>
    extends _$TopTenProductsStateCopyWithImpl<$Res>
    implements _$$LoadingTopTenProductsStateCopyWith<$Res> {
  __$$LoadingTopTenProductsStateCopyWithImpl(
      _$LoadingTopTenProductsState _value,
      $Res Function(_$LoadingTopTenProductsState) _then)
      : super(_value, (v) => _then(v as _$LoadingTopTenProductsState));

  @override
  _$LoadingTopTenProductsState get _value =>
      super._value as _$LoadingTopTenProductsState;
}

/// @nodoc

class _$LoadingTopTenProductsState extends LoadingTopTenProductsState {
  const _$LoadingTopTenProductsState() : super._();

  @override
  String toString() {
    return 'TopTenProductsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingTopTenProductsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Product> products) loaded,
    required TResult Function() failure,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTopTenProductsState value) initial,
    required TResult Function(LoadingTopTenProductsState value) loading,
    required TResult Function(LoadedTopTenProductsState value) loaded,
    required TResult Function(FailureTopTenProductsState value) failure,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingTopTenProductsState extends TopTenProductsState {
  const factory LoadingTopTenProductsState() = _$LoadingTopTenProductsState;
  const LoadingTopTenProductsState._() : super._();
}

/// @nodoc
abstract class _$$LoadedTopTenProductsStateCopyWith<$Res> {
  factory _$$LoadedTopTenProductsStateCopyWith(
          _$LoadedTopTenProductsState value,
          $Res Function(_$LoadedTopTenProductsState) then) =
      __$$LoadedTopTenProductsStateCopyWithImpl<$Res>;
  $Res call({List<Product> products});
}

/// @nodoc
class __$$LoadedTopTenProductsStateCopyWithImpl<$Res>
    extends _$TopTenProductsStateCopyWithImpl<$Res>
    implements _$$LoadedTopTenProductsStateCopyWith<$Res> {
  __$$LoadedTopTenProductsStateCopyWithImpl(_$LoadedTopTenProductsState _value,
      $Res Function(_$LoadedTopTenProductsState) _then)
      : super(_value, (v) => _then(v as _$LoadedTopTenProductsState));

  @override
  _$LoadedTopTenProductsState get _value =>
      super._value as _$LoadedTopTenProductsState;

  @override
  $Res call({
    Object? products = freezed,
  }) {
    return _then(_$LoadedTopTenProductsState(
      products == freezed
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>,
    ));
  }
}

/// @nodoc

class _$LoadedTopTenProductsState extends LoadedTopTenProductsState {
  const _$LoadedTopTenProductsState(final List<Product> products)
      : _products = products,
        super._();

  final List<Product> _products;
  @override
  List<Product> get products {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_products);
  }

  @override
  String toString() {
    return 'TopTenProductsState.loaded(products: $products)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedTopTenProductsState &&
            const DeepCollectionEquality().equals(other._products, _products));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_products));

  @JsonKey(ignore: true)
  @override
  _$$LoadedTopTenProductsStateCopyWith<_$LoadedTopTenProductsState>
      get copyWith => __$$LoadedTopTenProductsStateCopyWithImpl<
          _$LoadedTopTenProductsState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Product> products) loaded,
    required TResult Function() failure,
  }) {
    return loaded(products);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
  }) {
    return loaded?.call(products);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(products);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTopTenProductsState value) initial,
    required TResult Function(LoadingTopTenProductsState value) loading,
    required TResult Function(LoadedTopTenProductsState value) loaded,
    required TResult Function(FailureTopTenProductsState value) failure,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedTopTenProductsState extends TopTenProductsState {
  const factory LoadedTopTenProductsState(final List<Product> products) =
      _$LoadedTopTenProductsState;
  const LoadedTopTenProductsState._() : super._();

  List<Product> get products;
  @JsonKey(ignore: true)
  _$$LoadedTopTenProductsStateCopyWith<_$LoadedTopTenProductsState>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailureTopTenProductsStateCopyWith<$Res> {
  factory _$$FailureTopTenProductsStateCopyWith(
          _$FailureTopTenProductsState value,
          $Res Function(_$FailureTopTenProductsState) then) =
      __$$FailureTopTenProductsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailureTopTenProductsStateCopyWithImpl<$Res>
    extends _$TopTenProductsStateCopyWithImpl<$Res>
    implements _$$FailureTopTenProductsStateCopyWith<$Res> {
  __$$FailureTopTenProductsStateCopyWithImpl(
      _$FailureTopTenProductsState _value,
      $Res Function(_$FailureTopTenProductsState) _then)
      : super(_value, (v) => _then(v as _$FailureTopTenProductsState));

  @override
  _$FailureTopTenProductsState get _value =>
      super._value as _$FailureTopTenProductsState;
}

/// @nodoc

class _$FailureTopTenProductsState extends FailureTopTenProductsState {
  const _$FailureTopTenProductsState() : super._();

  @override
  String toString() {
    return 'TopTenProductsState.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FailureTopTenProductsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Product> products) loaded,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Product> products)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialTopTenProductsState value) initial,
    required TResult Function(LoadingTopTenProductsState value) loading,
    required TResult Function(LoadedTopTenProductsState value) loaded,
    required TResult Function(FailureTopTenProductsState value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialTopTenProductsState value)? initial,
    TResult Function(LoadingTopTenProductsState value)? loading,
    TResult Function(LoadedTopTenProductsState value)? loaded,
    TResult Function(FailureTopTenProductsState value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class FailureTopTenProductsState extends TopTenProductsState {
  const factory FailureTopTenProductsState() = _$FailureTopTenProductsState;
  const FailureTopTenProductsState._() : super._();
}
