import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import '../../services/product_service.dart';

part 'added_products_event.dart';
part 'added_products_state.dart';

class AddedProductsBloc extends Bloc<AddedProductsEvent, AddedProductsState> {
  final ProductService _productService;

  AddedProductsBloc({
    required ProductService productService,
  })  : _productService = productService,
        super(AddedProductsInitial());

  @override
  Stream<AddedProductsState> mapEventToState(
    AddedProductsEvent event,
  ) async* {
    if (event is GetProductsByIdEvent) {
      yield* _mapGetAddedProducts();
    } else if (event is ChangeProductStatusToDeletedEvent) {
      yield* _mapStatusToDeleted((event).productId);
    }
  }

  Stream<AddedProductsState> _mapGetAddedProducts() async* {
    yield AddedProductsLoading();
    try {
      final addedProducts = await _productService
          .getProductsByUserId()
          .timeout(const Duration(seconds: 20));
      yield AddedProductsLoaded(addedProducts: addedProducts);
    } on TimeoutException catch (e) {
      debugPrint('AddedProductsBloc()._mapGetAddedProducts timeout error $e');
      yield AddedProductsFailure(message: e.toString());
    } catch (e) {
      debugPrint('AddedProductsBloc()._mapGetAddedProducts error $e');
      yield AddedProductsFailure(message: e.toString());
      rethrow;
    }
  }

  Stream<AddedProductsState> _mapStatusToDeleted(String productId) async* {
    yield AddedProductsLoading();
    try {
      await _productService
          .changeProductStatusToDeleted(productId: productId)
          .timeout(const Duration(seconds: 20));
      final addedProducts = await _productService
          .getProductsByUserId()
          .timeout(const Duration(seconds: 20));
      yield AddedProductsLoaded(addedProducts: addedProducts);
    } on TimeoutException catch (e) {
      debugPrint('AddedProductsBloc()._mapStatusToDeleted timeout error $e');
      yield AddedProductsFailure(message: e.toString());
    } catch (e) {
      debugPrint('AddedProductsBloc()._mapStatusToDeleted error $e');
      yield AddedProductsFailure(message: e.toString());
      rethrow;
    }
  }
}
