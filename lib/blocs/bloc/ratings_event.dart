part of 'ratings_bloc.dart';

abstract class RatingsEvent extends Equatable {
  const RatingsEvent();

  @override
  List<Object> get props => [];
}

class StartRatingsLoadingEvent extends RatingsEvent {}

class StartSingleRatingUpdateEvent extends RatingsEvent {}

class PostSingleRatingEvent extends RatingsEvent {
  final String productId;
  final String userId;
  final int rank;
  final String comment;

  const PostSingleRatingEvent({
    required this.productId,
    required this.userId,
    required this.rank,
    required this.comment,
  });

  @override
  List<Object> get props => [productId, userId, comment];

  @override
  bool get stringify => true;
}

class PostRatingAnswerEvent extends RatingsEvent {
  final String productId;
  final AnswerItem answerItem;

  const PostRatingAnswerEvent({
    required this.productId,
    required this.answerItem,
  });

  @override
  List<Object> get props => [productId, answerItem];

  @override
  bool get stringify => true;
}
