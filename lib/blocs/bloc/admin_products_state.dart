part of 'admin_products_bloc.dart';

abstract class AdminProductsState extends Equatable {
  const AdminProductsState();

  @override
  List<Object> get props => [];
}

class AdminProductsInitial extends AdminProductsState {}

class AdminProductsLoading extends AdminProductsState {}

class AdminProductsLoaded extends AdminProductsState {
  final List<Product> products;
  final int? sortColumnIndex;
  final bool sortAscending;

  const AdminProductsLoaded({
    required this.products,
    this.sortColumnIndex,
    this.sortAscending = true,
  });
  @override
  List<Object> get props => [
        products,
        sortAscending,
      ];
}

class AdminProductsFailure extends AdminProductsState {
  final String message;

  const AdminProductsFailure({required this.message});
  @override
  List<Object> get props => [message];
}
