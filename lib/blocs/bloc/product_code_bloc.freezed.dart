// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_code_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProductCodeEvent {
  String get productId => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String productId) fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String productId)? fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String productId)? fetch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchProductCodeEvent value) fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchProductCodeEvent value)? fetch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchProductCodeEvent value)? fetch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProductCodeEventCopyWith<ProductCodeEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCodeEventCopyWith<$Res> {
  factory $ProductCodeEventCopyWith(
          ProductCodeEvent value, $Res Function(ProductCodeEvent) then) =
      _$ProductCodeEventCopyWithImpl<$Res>;
  $Res call({String productId});
}

/// @nodoc
class _$ProductCodeEventCopyWithImpl<$Res>
    implements $ProductCodeEventCopyWith<$Res> {
  _$ProductCodeEventCopyWithImpl(this._value, this._then);

  final ProductCodeEvent _value;
  // ignore: unused_field
  final $Res Function(ProductCodeEvent) _then;

  @override
  $Res call({
    Object? productId = freezed,
  }) {
    return _then(_value.copyWith(
      productId: productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$FetchProductCodeEventCopyWith<$Res>
    implements $ProductCodeEventCopyWith<$Res> {
  factory _$$FetchProductCodeEventCopyWith(_$FetchProductCodeEvent value,
          $Res Function(_$FetchProductCodeEvent) then) =
      __$$FetchProductCodeEventCopyWithImpl<$Res>;
  @override
  $Res call({String productId});
}

/// @nodoc
class __$$FetchProductCodeEventCopyWithImpl<$Res>
    extends _$ProductCodeEventCopyWithImpl<$Res>
    implements _$$FetchProductCodeEventCopyWith<$Res> {
  __$$FetchProductCodeEventCopyWithImpl(_$FetchProductCodeEvent _value,
      $Res Function(_$FetchProductCodeEvent) _then)
      : super(_value, (v) => _then(v as _$FetchProductCodeEvent));

  @override
  _$FetchProductCodeEvent get _value => super._value as _$FetchProductCodeEvent;

  @override
  $Res call({
    Object? productId = freezed,
  }) {
    return _then(_$FetchProductCodeEvent(
      productId: productId == freezed
          ? _value.productId
          : productId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FetchProductCodeEvent extends FetchProductCodeEvent {
  const _$FetchProductCodeEvent({required this.productId}) : super._();

  @override
  final String productId;

  @override
  String toString() {
    return 'ProductCodeEvent.fetch(productId: $productId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FetchProductCodeEvent &&
            const DeepCollectionEquality().equals(other.productId, productId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(productId));

  @JsonKey(ignore: true)
  @override
  _$$FetchProductCodeEventCopyWith<_$FetchProductCodeEvent> get copyWith =>
      __$$FetchProductCodeEventCopyWithImpl<_$FetchProductCodeEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String productId) fetch,
  }) {
    return fetch(productId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String productId)? fetch,
  }) {
    return fetch?.call(productId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String productId)? fetch,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch(productId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(FetchProductCodeEvent value) fetch,
  }) {
    return fetch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(FetchProductCodeEvent value)? fetch,
  }) {
    return fetch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(FetchProductCodeEvent value)? fetch,
    required TResult orElse(),
  }) {
    if (fetch != null) {
      return fetch(this);
    }
    return orElse();
  }
}

abstract class FetchProductCodeEvent extends ProductCodeEvent {
  const factory FetchProductCodeEvent({required final String productId}) =
      _$FetchProductCodeEvent;
  const FetchProductCodeEvent._() : super._();

  @override
  String get productId;
  @override
  @JsonKey(ignore: true)
  _$$FetchProductCodeEventCopyWith<_$FetchProductCodeEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ProductCodeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String source, String manifest) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductCodeState value) initial,
    required TResult Function(LoadingProductCodeState value) loading,
    required TResult Function(LoadedProductCodeState value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCodeStateCopyWith<$Res> {
  factory $ProductCodeStateCopyWith(
          ProductCodeState value, $Res Function(ProductCodeState) then) =
      _$ProductCodeStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ProductCodeStateCopyWithImpl<$Res>
    implements $ProductCodeStateCopyWith<$Res> {
  _$ProductCodeStateCopyWithImpl(this._value, this._then);

  final ProductCodeState _value;
  // ignore: unused_field
  final $Res Function(ProductCodeState) _then;
}

/// @nodoc
abstract class _$$InitialProductCodeStateCopyWith<$Res> {
  factory _$$InitialProductCodeStateCopyWith(_$InitialProductCodeState value,
          $Res Function(_$InitialProductCodeState) then) =
      __$$InitialProductCodeStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialProductCodeStateCopyWithImpl<$Res>
    extends _$ProductCodeStateCopyWithImpl<$Res>
    implements _$$InitialProductCodeStateCopyWith<$Res> {
  __$$InitialProductCodeStateCopyWithImpl(_$InitialProductCodeState _value,
      $Res Function(_$InitialProductCodeState) _then)
      : super(_value, (v) => _then(v as _$InitialProductCodeState));

  @override
  _$InitialProductCodeState get _value =>
      super._value as _$InitialProductCodeState;
}

/// @nodoc

class _$InitialProductCodeState extends InitialProductCodeState {
  const _$InitialProductCodeState() : super._();

  @override
  String toString() {
    return 'ProductCodeState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialProductCodeState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String source, String manifest) loaded,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductCodeState value) initial,
    required TResult Function(LoadingProductCodeState value) loading,
    required TResult Function(LoadedProductCodeState value) loaded,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialProductCodeState extends ProductCodeState {
  const factory InitialProductCodeState() = _$InitialProductCodeState;
  const InitialProductCodeState._() : super._();
}

/// @nodoc
abstract class _$$LoadingProductCodeStateCopyWith<$Res> {
  factory _$$LoadingProductCodeStateCopyWith(_$LoadingProductCodeState value,
          $Res Function(_$LoadingProductCodeState) then) =
      __$$LoadingProductCodeStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingProductCodeStateCopyWithImpl<$Res>
    extends _$ProductCodeStateCopyWithImpl<$Res>
    implements _$$LoadingProductCodeStateCopyWith<$Res> {
  __$$LoadingProductCodeStateCopyWithImpl(_$LoadingProductCodeState _value,
      $Res Function(_$LoadingProductCodeState) _then)
      : super(_value, (v) => _then(v as _$LoadingProductCodeState));

  @override
  _$LoadingProductCodeState get _value =>
      super._value as _$LoadingProductCodeState;
}

/// @nodoc

class _$LoadingProductCodeState extends LoadingProductCodeState {
  const _$LoadingProductCodeState() : super._();

  @override
  String toString() {
    return 'ProductCodeState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadingProductCodeState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String source, String manifest) loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductCodeState value) initial,
    required TResult Function(LoadingProductCodeState value) loading,
    required TResult Function(LoadedProductCodeState value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingProductCodeState extends ProductCodeState {
  const factory LoadingProductCodeState() = _$LoadingProductCodeState;
  const LoadingProductCodeState._() : super._();
}

/// @nodoc
abstract class _$$LoadedProductCodeStateCopyWith<$Res> {
  factory _$$LoadedProductCodeStateCopyWith(_$LoadedProductCodeState value,
          $Res Function(_$LoadedProductCodeState) then) =
      __$$LoadedProductCodeStateCopyWithImpl<$Res>;
  $Res call({String source, String manifest});
}

/// @nodoc
class __$$LoadedProductCodeStateCopyWithImpl<$Res>
    extends _$ProductCodeStateCopyWithImpl<$Res>
    implements _$$LoadedProductCodeStateCopyWith<$Res> {
  __$$LoadedProductCodeStateCopyWithImpl(_$LoadedProductCodeState _value,
      $Res Function(_$LoadedProductCodeState) _then)
      : super(_value, (v) => _then(v as _$LoadedProductCodeState));

  @override
  _$LoadedProductCodeState get _value =>
      super._value as _$LoadedProductCodeState;

  @override
  $Res call({
    Object? source = freezed,
    Object? manifest = freezed,
  }) {
    return _then(_$LoadedProductCodeState(
      source == freezed
          ? _value.source
          : source // ignore: cast_nullable_to_non_nullable
              as String,
      manifest == freezed
          ? _value.manifest
          : manifest // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LoadedProductCodeState extends LoadedProductCodeState {
  const _$LoadedProductCodeState(this.source, this.manifest) : super._();

  @override
  final String source;
  @override
  final String manifest;

  @override
  String toString() {
    return 'ProductCodeState.loaded(source: $source, manifest: $manifest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedProductCodeState &&
            const DeepCollectionEquality().equals(other.source, source) &&
            const DeepCollectionEquality().equals(other.manifest, manifest));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(source),
      const DeepCollectionEquality().hash(manifest));

  @JsonKey(ignore: true)
  @override
  _$$LoadedProductCodeStateCopyWith<_$LoadedProductCodeState> get copyWith =>
      __$$LoadedProductCodeStateCopyWithImpl<_$LoadedProductCodeState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String source, String manifest) loaded,
  }) {
    return loaded(source, manifest);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
  }) {
    return loaded?.call(source, manifest);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String source, String manifest)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(source, manifest);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialProductCodeState value) initial,
    required TResult Function(LoadingProductCodeState value) loading,
    required TResult Function(LoadedProductCodeState value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialProductCodeState value)? initial,
    TResult Function(LoadingProductCodeState value)? loading,
    TResult Function(LoadedProductCodeState value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedProductCodeState extends ProductCodeState {
  const factory LoadedProductCodeState(
      final String source, final String manifest) = _$LoadedProductCodeState;
  const LoadedProductCodeState._() : super._();

  String get source;
  String get manifest;
  @JsonKey(ignore: true)
  _$$LoadedProductCodeStateCopyWith<_$LoadedProductCodeState> get copyWith =>
      throw _privateConstructorUsedError;
}
