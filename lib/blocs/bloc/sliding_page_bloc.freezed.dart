// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sliding_page_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SlidingPageEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() open,
    required TResult Function() close,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(OpenSlidingPageEvent value) open,
    required TResult Function(CloseSlidingPageEvent value) close,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SlidingPageEventCopyWith<$Res> {
  factory $SlidingPageEventCopyWith(
          SlidingPageEvent value, $Res Function(SlidingPageEvent) then) =
      _$SlidingPageEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SlidingPageEventCopyWithImpl<$Res>
    implements $SlidingPageEventCopyWith<$Res> {
  _$SlidingPageEventCopyWithImpl(this._value, this._then);

  final SlidingPageEvent _value;
  // ignore: unused_field
  final $Res Function(SlidingPageEvent) _then;
}

/// @nodoc
abstract class _$$OpenSlidingPageEventCopyWith<$Res> {
  factory _$$OpenSlidingPageEventCopyWith(_$OpenSlidingPageEvent value,
          $Res Function(_$OpenSlidingPageEvent) then) =
      __$$OpenSlidingPageEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OpenSlidingPageEventCopyWithImpl<$Res>
    extends _$SlidingPageEventCopyWithImpl<$Res>
    implements _$$OpenSlidingPageEventCopyWith<$Res> {
  __$$OpenSlidingPageEventCopyWithImpl(_$OpenSlidingPageEvent _value,
      $Res Function(_$OpenSlidingPageEvent) _then)
      : super(_value, (v) => _then(v as _$OpenSlidingPageEvent));

  @override
  _$OpenSlidingPageEvent get _value => super._value as _$OpenSlidingPageEvent;
}

/// @nodoc

class _$OpenSlidingPageEvent extends OpenSlidingPageEvent {
  const _$OpenSlidingPageEvent() : super._();

  @override
  String toString() {
    return 'SlidingPageEvent.open()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OpenSlidingPageEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() open,
    required TResult Function() close,
  }) {
    return open();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
  }) {
    return open?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
    required TResult orElse(),
  }) {
    if (open != null) {
      return open();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(OpenSlidingPageEvent value) open,
    required TResult Function(CloseSlidingPageEvent value) close,
  }) {
    return open(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
  }) {
    return open?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
    required TResult orElse(),
  }) {
    if (open != null) {
      return open(this);
    }
    return orElse();
  }
}

abstract class OpenSlidingPageEvent extends SlidingPageEvent {
  const factory OpenSlidingPageEvent() = _$OpenSlidingPageEvent;
  const OpenSlidingPageEvent._() : super._();
}

/// @nodoc
abstract class _$$CloseSlidingPageEventCopyWith<$Res> {
  factory _$$CloseSlidingPageEventCopyWith(_$CloseSlidingPageEvent value,
          $Res Function(_$CloseSlidingPageEvent) then) =
      __$$CloseSlidingPageEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CloseSlidingPageEventCopyWithImpl<$Res>
    extends _$SlidingPageEventCopyWithImpl<$Res>
    implements _$$CloseSlidingPageEventCopyWith<$Res> {
  __$$CloseSlidingPageEventCopyWithImpl(_$CloseSlidingPageEvent _value,
      $Res Function(_$CloseSlidingPageEvent) _then)
      : super(_value, (v) => _then(v as _$CloseSlidingPageEvent));

  @override
  _$CloseSlidingPageEvent get _value => super._value as _$CloseSlidingPageEvent;
}

/// @nodoc

class _$CloseSlidingPageEvent extends CloseSlidingPageEvent {
  const _$CloseSlidingPageEvent() : super._();

  @override
  String toString() {
    return 'SlidingPageEvent.close()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CloseSlidingPageEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() open,
    required TResult Function() close,
  }) {
    return close();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
  }) {
    return close?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? open,
    TResult Function()? close,
    required TResult orElse(),
  }) {
    if (close != null) {
      return close();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(OpenSlidingPageEvent value) open,
    required TResult Function(CloseSlidingPageEvent value) close,
  }) {
    return close(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
  }) {
    return close?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(OpenSlidingPageEvent value)? open,
    TResult Function(CloseSlidingPageEvent value)? close,
    required TResult orElse(),
  }) {
    if (close != null) {
      return close(this);
    }
    return orElse();
  }
}

abstract class CloseSlidingPageEvent extends SlidingPageEvent {
  const factory CloseSlidingPageEvent() = _$CloseSlidingPageEvent;
  const CloseSlidingPageEvent._() : super._();
}

/// @nodoc
mixin _$SlidingPageState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() closed,
    required TResult Function() opened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSlidingPageState value) initial,
    required TResult Function(ClosedSlidingPageState value) closed,
    required TResult Function(OpenedSlidingPageState value) opened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SlidingPageStateCopyWith<$Res> {
  factory $SlidingPageStateCopyWith(
          SlidingPageState value, $Res Function(SlidingPageState) then) =
      _$SlidingPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SlidingPageStateCopyWithImpl<$Res>
    implements $SlidingPageStateCopyWith<$Res> {
  _$SlidingPageStateCopyWithImpl(this._value, this._then);

  final SlidingPageState _value;
  // ignore: unused_field
  final $Res Function(SlidingPageState) _then;
}

/// @nodoc
abstract class _$$InitialSlidingPageStateCopyWith<$Res> {
  factory _$$InitialSlidingPageStateCopyWith(_$InitialSlidingPageState value,
          $Res Function(_$InitialSlidingPageState) then) =
      __$$InitialSlidingPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialSlidingPageStateCopyWithImpl<$Res>
    extends _$SlidingPageStateCopyWithImpl<$Res>
    implements _$$InitialSlidingPageStateCopyWith<$Res> {
  __$$InitialSlidingPageStateCopyWithImpl(_$InitialSlidingPageState _value,
      $Res Function(_$InitialSlidingPageState) _then)
      : super(_value, (v) => _then(v as _$InitialSlidingPageState));

  @override
  _$InitialSlidingPageState get _value =>
      super._value as _$InitialSlidingPageState;
}

/// @nodoc

class _$InitialSlidingPageState extends InitialSlidingPageState {
  const _$InitialSlidingPageState() : super._();

  @override
  String toString() {
    return 'SlidingPageState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitialSlidingPageState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() closed,
    required TResult Function() opened,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSlidingPageState value) initial,
    required TResult Function(ClosedSlidingPageState value) closed,
    required TResult Function(OpenedSlidingPageState value) opened,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialSlidingPageState extends SlidingPageState {
  const factory InitialSlidingPageState() = _$InitialSlidingPageState;
  const InitialSlidingPageState._() : super._();
}

/// @nodoc
abstract class _$$ClosedSlidingPageStateCopyWith<$Res> {
  factory _$$ClosedSlidingPageStateCopyWith(_$ClosedSlidingPageState value,
          $Res Function(_$ClosedSlidingPageState) then) =
      __$$ClosedSlidingPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ClosedSlidingPageStateCopyWithImpl<$Res>
    extends _$SlidingPageStateCopyWithImpl<$Res>
    implements _$$ClosedSlidingPageStateCopyWith<$Res> {
  __$$ClosedSlidingPageStateCopyWithImpl(_$ClosedSlidingPageState _value,
      $Res Function(_$ClosedSlidingPageState) _then)
      : super(_value, (v) => _then(v as _$ClosedSlidingPageState));

  @override
  _$ClosedSlidingPageState get _value =>
      super._value as _$ClosedSlidingPageState;
}

/// @nodoc

class _$ClosedSlidingPageState extends ClosedSlidingPageState {
  const _$ClosedSlidingPageState() : super._();

  @override
  String toString() {
    return 'SlidingPageState.closed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ClosedSlidingPageState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() closed,
    required TResult Function() opened,
  }) {
    return closed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
  }) {
    return closed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
    required TResult orElse(),
  }) {
    if (closed != null) {
      return closed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSlidingPageState value) initial,
    required TResult Function(ClosedSlidingPageState value) closed,
    required TResult Function(OpenedSlidingPageState value) opened,
  }) {
    return closed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
  }) {
    return closed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
    required TResult orElse(),
  }) {
    if (closed != null) {
      return closed(this);
    }
    return orElse();
  }
}

abstract class ClosedSlidingPageState extends SlidingPageState {
  const factory ClosedSlidingPageState() = _$ClosedSlidingPageState;
  const ClosedSlidingPageState._() : super._();
}

/// @nodoc
abstract class _$$OpenedSlidingPageStateCopyWith<$Res> {
  factory _$$OpenedSlidingPageStateCopyWith(_$OpenedSlidingPageState value,
          $Res Function(_$OpenedSlidingPageState) then) =
      __$$OpenedSlidingPageStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$OpenedSlidingPageStateCopyWithImpl<$Res>
    extends _$SlidingPageStateCopyWithImpl<$Res>
    implements _$$OpenedSlidingPageStateCopyWith<$Res> {
  __$$OpenedSlidingPageStateCopyWithImpl(_$OpenedSlidingPageState _value,
      $Res Function(_$OpenedSlidingPageState) _then)
      : super(_value, (v) => _then(v as _$OpenedSlidingPageState));

  @override
  _$OpenedSlidingPageState get _value =>
      super._value as _$OpenedSlidingPageState;
}

/// @nodoc

class _$OpenedSlidingPageState extends OpenedSlidingPageState {
  const _$OpenedSlidingPageState() : super._();

  @override
  String toString() {
    return 'SlidingPageState.opened()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$OpenedSlidingPageState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() closed,
    required TResult Function() opened,
  }) {
    return opened();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
  }) {
    return opened?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? closed,
    TResult Function()? opened,
    required TResult orElse(),
  }) {
    if (opened != null) {
      return opened();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSlidingPageState value) initial,
    required TResult Function(ClosedSlidingPageState value) closed,
    required TResult Function(OpenedSlidingPageState value) opened,
  }) {
    return opened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
  }) {
    return opened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSlidingPageState value)? initial,
    TResult Function(ClosedSlidingPageState value)? closed,
    TResult Function(OpenedSlidingPageState value)? opened,
    required TResult orElse(),
  }) {
    if (opened != null) {
      return opened(this);
    }
    return orElse();
  }
}

abstract class OpenedSlidingPageState extends SlidingPageState {
  const factory OpenedSlidingPageState() = _$OpenedSlidingPageState;
  const OpenedSlidingPageState._() : super._();
}
