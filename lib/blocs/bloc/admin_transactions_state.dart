part of 'admin_transactions_bloc.dart';

abstract class AdminTransactionsState extends Equatable {
  const AdminTransactionsState();

  @override
  List<Object> get props => [];
}

class AdminTransactionsInitial extends AdminTransactionsState {}

class AdminTransactionsLoading extends AdminTransactionsState {}

class AdminTransactionsLoaded extends AdminTransactionsState {
  final List<AdminTransactModel> transactions;

  const AdminTransactionsLoaded({required this.transactions});
  @override
  List<Object> get props => [transactions];
}

class AdminTransactionsFailure extends AdminTransactionsState {
  final String message;

  const AdminTransactionsFailure({required this.message});
}
