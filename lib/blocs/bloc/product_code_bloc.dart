import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/services/admin_service.dart';

part 'product_code_bloc.freezed.dart';

@freezed
class ProductCodeEvent with _$ProductCodeEvent {
  const ProductCodeEvent._();

  const factory ProductCodeEvent.fetch({required String productId}) =
      FetchProductCodeEvent;
}

@freezed
class ProductCodeState with _$ProductCodeState {
  const ProductCodeState._();

  const factory ProductCodeState.initial() = InitialProductCodeState;
  const factory ProductCodeState.loading() = LoadingProductCodeState;
  const factory ProductCodeState.loaded(String source, String manifest) =
      LoadedProductCodeState;
}

class ProductCodeBLoC extends Bloc<ProductCodeEvent, ProductCodeState> {
  ProductCodeBLoC() : super(const InitialProductCodeState());

  @override
  Stream<ProductCodeState> mapEventToState(ProductCodeEvent event) =>
      event.when<Stream<ProductCodeState>>(
        fetch: _fetchCode,
      );

  Stream<ProductCodeState> _fetchCode(String productId) async* {
    yield const LoadingProductCodeState();
    final source = await sl<AdminService>().getProductSource(productId);
    final manifest = await sl<AdminService>().getProductManifest(productId);
    yield LoadedProductCodeState(source, manifest);
  }
}
