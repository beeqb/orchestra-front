import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../models/app.dart';
import 'app_timer_bloc.dart';

part 'app_button_event.dart';
part 'app_button_state.dart';

class AppButtonBloc extends Bloc<AppButtonEvent, AppButtonState> {
  final AppTimerBloc _appTimerBloc;

  AppButtonBloc({
    required AppModel appModel,
    required AppTimerBloc appTimerBloc,
  })  : _appTimerBloc = appTimerBloc,
        super(AppButtonInitial(appModel: appModel));

  @override
  Stream<AppButtonState> mapEventToState(
    AppButtonEvent event,
  ) async* {
    switch (event.runtimeType) {
      case AppButtonStartEvent:
        yield* _mapAppButtonStarted(
          appModel: (event as AppButtonStartEvent).appModel,
        );
        break;
      case AppButtonPauseEvent:
        yield* _mapAppButtonPaused(
          appModel: (event as AppButtonPauseEvent).appModel,
        );
        break;
      default:
        throw 'APP_BUTTON_BLOC: Unknown event';
    }
  }

  Stream<AppButtonState> _mapAppButtonStarted(
      {required AppModel appModel}) async* {
    // _appTimerBloc.add(const AppTimerStarted());
    yield AppButtonStarted(appModel: appModel);
  }

  Stream<AppButtonState> _mapAppButtonPaused(
      {required AppModel appModel}) async* {
    if (appModel.status != 'Active') {
      //   _appTimerBloc.add(AppTimerResumed());
    }
    yield AppButtonPaused(appModel: appModel);
  }
}
