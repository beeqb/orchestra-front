import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../models/enums/enums.dart';
import '../../services/dialogs_service.dart';
import '../../services/user_service.dart';
import '../cubits/user_cubit.dart';

part 'contributor_event.dart';
part 'contributor_state.dart';

class ContributorBloc extends Bloc<ContributorEvent, ContributorState> {
  final UserService _userService;
  final DialogsService _dialogsService;
  final UserCubit _userCubit;

  ContributorBloc({
    required UserService userService,
    required DialogsService dialogsService,
    required UserCubit userCubit,
  })  : _userService = userService,
        _dialogsService = dialogsService,
        _userCubit = userCubit,
        super(ContributorInitial());

  @override
  Stream<ContributorState> mapEventToState(
    ContributorEvent event,
  ) async* {
    switch (event.runtimeType) {
      case BecomeContributorEvent:
        yield* _mapBecomeContributor();
        break;
      case SetContributorEvent:
        yield* _mapSetContributor(
            isContributor: (event as SetContributorEvent).isContributor);
        break;

      default:
        throw 'ContributorBloc: Unknown event';
    }
  }

  Stream<ContributorState> _mapSetContributor(
      {required bool isContributor}) async* {
    yield ContributorUpdating();
    yield ContributorUpdated(isContributor: isContributor);
  }

  Stream<ContributorState> _mapBecomeContributor() async* {
    yield ContributorUpdating();
    try {
      final result = await _userService
          .becomeContributor()
          .timeout(const Duration(seconds: 20));
      if (result.keys.first == 'success') {
        await _dialogsService.showDialog(
            dialogType: DialogType.successSnackbar,
            title: '',
            description: 'Congratulations! You have become a contributor');
        await _userCubit.getUserInfo();
        yield const ContributorUpdated(isContributor: true);
      }
      if (result.keys.first == 'error') {
        await _dialogsService.showDialog(
            dialogType: DialogType.errorSnackbar,
            title: '',
            description: result.values.first);
        yield ContributorFailure(message: result.values.first);
      }
      if (result.keys.first == 'status') {
        await _dialogsService.showDialog(
            dialogType: DialogType.errorSnackbar,
            title: '',
            description: result.values.first);
        yield ContributorFailure(message: result.values.first);
      }
    } on TimeoutException catch (e) {
      debugPrint('ContributorCubit timeout error ${e.toString()}');
      yield ContributorFailure(message: e.toString());
    } catch (e) {
      debugPrint('ContributorCubit error ${e.toString()}');
      yield ContributorFailure(message: e.toString());
      rethrow;
    }
  }
}
