part of 'app_timer_bloc.dart';

abstract class AppTimerState extends Equatable {
  const AppTimerState();

  @override
  List<Object> get props => [];
}

class AppTimerInitial extends AppTimerState {}

class AppTimerInProgress extends AppTimerState {
  final String runningTime;

  const AppTimerInProgress({required this.runningTime}) : super();

  @override
  List<Object> get props => [runningTime];

  @override
  String toString() => 'AppTimerInProgress(runningTime: $runningTime)';
}

class AppTimerPause extends AppTimerState {
  const AppTimerPause() : super();
}

class AppTimerComplete extends AppTimerState {
  final String runningTime;
  final double runningCost;

  const AppTimerComplete({
    required this.runningTime,
    required this.runningCost,
  }) : super();

  @override
  String toString() =>
      'AppTimerComplete(runningTime: $runningTime, runningCost: $runningCost)';
}

class AppTimerFailure extends AppTimerState {}
