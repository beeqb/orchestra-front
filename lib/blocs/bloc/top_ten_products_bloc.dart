import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/services/product_service.dart';

part 'top_ten_products_bloc.freezed.dart';

@freezed
class TopTenProductsEvent with _$TopTenProductsEvent {
  const TopTenProductsEvent._();

  const factory TopTenProductsEvent.read() = ReadTopTenProductsEvent;

  const factory TopTenProductsEvent.update() = UpdateTopTenProductsEvent;
}

@freezed
class TopTenProductsState with _$TopTenProductsState {
  const TopTenProductsState._();

  const factory TopTenProductsState.initial() = InitialTopTenProductsState;
  const factory TopTenProductsState.loading() = LoadingTopTenProductsState;
  const factory TopTenProductsState.loaded(List<Product> products) =
      LoadedTopTenProductsState;
  const factory TopTenProductsState.failure() = FailureTopTenProductsState;
}

class TopTenProductsBLoC
    extends Bloc<TopTenProductsEvent, TopTenProductsState> {
  final ProductService _productService;
  TopTenProductsBLoC(ProductService productService)
      : _productService = productService,
        super(const InitialTopTenProductsState());

  @override
  Stream<TopTenProductsState> mapEventToState(TopTenProductsEvent event) =>
      event.when<Stream<TopTenProductsState>>(
        read: _read,
        update: _update,
      );

  Stream<TopTenProductsState> _read() async* {
    yield const TopTenProductsState.loading();
    try {
      final result = await _productService
          .getTopProducts()
          .timeout(const Duration(seconds: 20));
      yield result.fold(
        (l) => const TopTenProductsState.failure(),
        (r) => TopTenProductsState.loaded(r.reversed.toList()),
      );
    } on TimeoutException {
      yield const TopTenProductsState.failure();
    }
  }

  Stream<TopTenProductsState> _update() async* {
    // ...
  }
}
