part of 'app_button_bloc.dart';

abstract class AppButtonState extends Equatable {
  const AppButtonState();

  @override
  List<Object> get props => [];
}

class AppButtonInitial extends AppButtonState {
  final AppModel appModel;

  const AppButtonInitial({required this.appModel});
  @override
  List<Object> get props => [appModel];

  @override
  String toString() => 'AppButtonPaused(appModel: $appModel)';
}

class AppButtonPaused extends AppButtonState {
  final AppModel appModel;

  const AppButtonPaused({required this.appModel});
  @override
  List<Object> get props => [appModel];

  @override
  bool get stringify => true;
}

class AppButtonStarted extends AppButtonState {
  final AppModel appModel;

  const AppButtonStarted({required this.appModel});
  @override
  List<Object> get props => [appModel];

  @override
  bool get stringify => true;
}
