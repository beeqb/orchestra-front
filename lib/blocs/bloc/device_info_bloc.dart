import 'package:bloc/bloc.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:package_info_plus/package_info_plus.dart';

part 'device_info_bloc.freezed.dart';

@freezed
class DeviceInfoEvent with _$DeviceInfoEvent {
  const DeviceInfoEvent._();

  const factory DeviceInfoEvent.read() = ReadDeviceInfoEvent;
}

@freezed
class DeviceInfoState with _$DeviceInfoState {
  const DeviceInfoState._();

  String? get versionNumber => when<String?>(
        initial: () => null,
        loading: () => null,
        loaded: (v, _) => v,
        failure: () => null,
      );

  String? get buildNumber => when<String?>(
        initial: () => null,
        loading: () => null,
        loaded: (_, b) => b,
        failure: () => null,
      );

  const factory DeviceInfoState.initial() = InitialDeviceInfoState;

  const factory DeviceInfoState.loading() = LoadingDeviceInfoState;

  const factory DeviceInfoState.loaded(
      {required String appVersion,
      required String appBuild}) = LoadedDeviceInfoState;

  const factory DeviceInfoState.failure() = FailureDeviceInfoState;
}

class DeviceInfoBloc extends Bloc<DeviceInfoEvent, DeviceInfoState> {
  DeviceInfoBloc() : super(const InitialDeviceInfoState()) {
    on<DeviceInfoEvent>(
      (event, emit) => event.map(read: (event) => _read(event, emit)),
    );
  }

  Future<void> _read(
      ReadDeviceInfoEvent event, Emitter<DeviceInfoState> emitter) async {
    emitter(const LoadingDeviceInfoState());
/*     DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    print('Running on: ${androidInfo}'); */ // e.g. "Moto G (4)"

/*     IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    print('Running on ${iosInfo.utsname.machine}');
 */

    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    // String appName = packageInfo.appName;
    // String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    try {
      if (!kIsWeb) {
        FirebaseAnalytics.instance.setDefaultEventParameters({
          'version': version,
          'buildNumber': buildNumber,
        });
      }
    } finally {
      emitter(LoadedDeviceInfoState(
        appVersion: version,
        appBuild: buildNumber,
      ));
    }
  }
}
