part of 'test_ends_after_bloc.dart';

abstract class TestEndsAfterState extends Equatable {
  const TestEndsAfterState();

  @override
  List<Object> get props => [];
}

class TestEndsAfterInitial extends TestEndsAfterState {}

class TestEndsAfterPricessing extends TestEndsAfterState {}

class TestEndsAfterSet extends TestEndsAfterState {
  final List<String> testEndsAfter;
  final String description;

  const TestEndsAfterSet({
    required this.testEndsAfter,
    required this.description,
  });

  @override
  List<Object> get props => [testEndsAfter, description];

  @override
  String toString() =>
      // ignore: lines_longer_than_80_chars
      'TestEndsAfterSet(testEndsAfter: $testEndsAfter, description: $description)';
}
