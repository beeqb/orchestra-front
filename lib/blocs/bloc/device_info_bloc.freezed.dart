// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'device_info_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$DeviceInfoEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? read,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReadDeviceInfoEvent value) read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReadDeviceInfoEvent value)? read,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReadDeviceInfoEvent value)? read,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeviceInfoEventCopyWith<$Res> {
  factory $DeviceInfoEventCopyWith(
          DeviceInfoEvent value, $Res Function(DeviceInfoEvent) then) =
      _$DeviceInfoEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeviceInfoEventCopyWithImpl<$Res>
    implements $DeviceInfoEventCopyWith<$Res> {
  _$DeviceInfoEventCopyWithImpl(this._value, this._then);

  final DeviceInfoEvent _value;
  // ignore: unused_field
  final $Res Function(DeviceInfoEvent) _then;
}

/// @nodoc
abstract class _$$ReadDeviceInfoEventCopyWith<$Res> {
  factory _$$ReadDeviceInfoEventCopyWith(_$ReadDeviceInfoEvent value,
          $Res Function(_$ReadDeviceInfoEvent) then) =
      __$$ReadDeviceInfoEventCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ReadDeviceInfoEventCopyWithImpl<$Res>
    extends _$DeviceInfoEventCopyWithImpl<$Res>
    implements _$$ReadDeviceInfoEventCopyWith<$Res> {
  __$$ReadDeviceInfoEventCopyWithImpl(
      _$ReadDeviceInfoEvent _value, $Res Function(_$ReadDeviceInfoEvent) _then)
      : super(_value, (v) => _then(v as _$ReadDeviceInfoEvent));

  @override
  _$ReadDeviceInfoEvent get _value => super._value as _$ReadDeviceInfoEvent;
}

/// @nodoc

class _$ReadDeviceInfoEvent extends ReadDeviceInfoEvent
    with DiagnosticableTreeMixin {
  const _$ReadDeviceInfoEvent() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceInfoEvent.read()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'DeviceInfoEvent.read'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ReadDeviceInfoEvent);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() read,
  }) {
    return read();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? read,
  }) {
    return read?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? read,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReadDeviceInfoEvent value) read,
  }) {
    return read(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReadDeviceInfoEvent value)? read,
  }) {
    return read?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReadDeviceInfoEvent value)? read,
    required TResult orElse(),
  }) {
    if (read != null) {
      return read(this);
    }
    return orElse();
  }
}

abstract class ReadDeviceInfoEvent extends DeviceInfoEvent {
  const factory ReadDeviceInfoEvent() = _$ReadDeviceInfoEvent;
  const ReadDeviceInfoEvent._() : super._();
}

/// @nodoc
mixin _$DeviceInfoState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String appVersion, String appBuild) loaded,
    required TResult Function() failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialDeviceInfoState value) initial,
    required TResult Function(LoadingDeviceInfoState value) loading,
    required TResult Function(LoadedDeviceInfoState value) loaded,
    required TResult Function(FailureDeviceInfoState value) failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeviceInfoStateCopyWith<$Res> {
  factory $DeviceInfoStateCopyWith(
          DeviceInfoState value, $Res Function(DeviceInfoState) then) =
      _$DeviceInfoStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$DeviceInfoStateCopyWithImpl<$Res>
    implements $DeviceInfoStateCopyWith<$Res> {
  _$DeviceInfoStateCopyWithImpl(this._value, this._then);

  final DeviceInfoState _value;
  // ignore: unused_field
  final $Res Function(DeviceInfoState) _then;
}

/// @nodoc
abstract class _$$InitialDeviceInfoStateCopyWith<$Res> {
  factory _$$InitialDeviceInfoStateCopyWith(_$InitialDeviceInfoState value,
          $Res Function(_$InitialDeviceInfoState) then) =
      __$$InitialDeviceInfoStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialDeviceInfoStateCopyWithImpl<$Res>
    extends _$DeviceInfoStateCopyWithImpl<$Res>
    implements _$$InitialDeviceInfoStateCopyWith<$Res> {
  __$$InitialDeviceInfoStateCopyWithImpl(_$InitialDeviceInfoState _value,
      $Res Function(_$InitialDeviceInfoState) _then)
      : super(_value, (v) => _then(v as _$InitialDeviceInfoState));

  @override
  _$InitialDeviceInfoState get _value =>
      super._value as _$InitialDeviceInfoState;
}

/// @nodoc

class _$InitialDeviceInfoState extends InitialDeviceInfoState
    with DiagnosticableTreeMixin {
  const _$InitialDeviceInfoState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceInfoState.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'DeviceInfoState.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialDeviceInfoState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String appVersion, String appBuild) loaded,
    required TResult Function() failure,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialDeviceInfoState value) initial,
    required TResult Function(LoadingDeviceInfoState value) loading,
    required TResult Function(LoadedDeviceInfoState value) loaded,
    required TResult Function(FailureDeviceInfoState value) failure,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialDeviceInfoState extends DeviceInfoState {
  const factory InitialDeviceInfoState() = _$InitialDeviceInfoState;
  const InitialDeviceInfoState._() : super._();
}

/// @nodoc
abstract class _$$LoadingDeviceInfoStateCopyWith<$Res> {
  factory _$$LoadingDeviceInfoStateCopyWith(_$LoadingDeviceInfoState value,
          $Res Function(_$LoadingDeviceInfoState) then) =
      __$$LoadingDeviceInfoStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingDeviceInfoStateCopyWithImpl<$Res>
    extends _$DeviceInfoStateCopyWithImpl<$Res>
    implements _$$LoadingDeviceInfoStateCopyWith<$Res> {
  __$$LoadingDeviceInfoStateCopyWithImpl(_$LoadingDeviceInfoState _value,
      $Res Function(_$LoadingDeviceInfoState) _then)
      : super(_value, (v) => _then(v as _$LoadingDeviceInfoState));

  @override
  _$LoadingDeviceInfoState get _value =>
      super._value as _$LoadingDeviceInfoState;
}

/// @nodoc

class _$LoadingDeviceInfoState extends LoadingDeviceInfoState
    with DiagnosticableTreeMixin {
  const _$LoadingDeviceInfoState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceInfoState.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'DeviceInfoState.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingDeviceInfoState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String appVersion, String appBuild) loaded,
    required TResult Function() failure,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialDeviceInfoState value) initial,
    required TResult Function(LoadingDeviceInfoState value) loading,
    required TResult Function(LoadedDeviceInfoState value) loaded,
    required TResult Function(FailureDeviceInfoState value) failure,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingDeviceInfoState extends DeviceInfoState {
  const factory LoadingDeviceInfoState() = _$LoadingDeviceInfoState;
  const LoadingDeviceInfoState._() : super._();
}

/// @nodoc
abstract class _$$LoadedDeviceInfoStateCopyWith<$Res> {
  factory _$$LoadedDeviceInfoStateCopyWith(_$LoadedDeviceInfoState value,
          $Res Function(_$LoadedDeviceInfoState) then) =
      __$$LoadedDeviceInfoStateCopyWithImpl<$Res>;
  $Res call({String appVersion, String appBuild});
}

/// @nodoc
class __$$LoadedDeviceInfoStateCopyWithImpl<$Res>
    extends _$DeviceInfoStateCopyWithImpl<$Res>
    implements _$$LoadedDeviceInfoStateCopyWith<$Res> {
  __$$LoadedDeviceInfoStateCopyWithImpl(_$LoadedDeviceInfoState _value,
      $Res Function(_$LoadedDeviceInfoState) _then)
      : super(_value, (v) => _then(v as _$LoadedDeviceInfoState));

  @override
  _$LoadedDeviceInfoState get _value => super._value as _$LoadedDeviceInfoState;

  @override
  $Res call({
    Object? appVersion = freezed,
    Object? appBuild = freezed,
  }) {
    return _then(_$LoadedDeviceInfoState(
      appVersion: appVersion == freezed
          ? _value.appVersion
          : appVersion // ignore: cast_nullable_to_non_nullable
              as String,
      appBuild: appBuild == freezed
          ? _value.appBuild
          : appBuild // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LoadedDeviceInfoState extends LoadedDeviceInfoState
    with DiagnosticableTreeMixin {
  const _$LoadedDeviceInfoState(
      {required this.appVersion, required this.appBuild})
      : super._();

  @override
  final String appVersion;
  @override
  final String appBuild;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceInfoState.loaded(appVersion: $appVersion, appBuild: $appBuild)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'DeviceInfoState.loaded'))
      ..add(DiagnosticsProperty('appVersion', appVersion))
      ..add(DiagnosticsProperty('appBuild', appBuild));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedDeviceInfoState &&
            const DeepCollectionEquality()
                .equals(other.appVersion, appVersion) &&
            const DeepCollectionEquality().equals(other.appBuild, appBuild));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(appVersion),
      const DeepCollectionEquality().hash(appBuild));

  @JsonKey(ignore: true)
  @override
  _$$LoadedDeviceInfoStateCopyWith<_$LoadedDeviceInfoState> get copyWith =>
      __$$LoadedDeviceInfoStateCopyWithImpl<_$LoadedDeviceInfoState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String appVersion, String appBuild) loaded,
    required TResult Function() failure,
  }) {
    return loaded(appVersion, appBuild);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
  }) {
    return loaded?.call(appVersion, appBuild);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(appVersion, appBuild);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialDeviceInfoState value) initial,
    required TResult Function(LoadingDeviceInfoState value) loading,
    required TResult Function(LoadedDeviceInfoState value) loaded,
    required TResult Function(FailureDeviceInfoState value) failure,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedDeviceInfoState extends DeviceInfoState {
  const factory LoadedDeviceInfoState(
      {required final String appVersion,
      required final String appBuild}) = _$LoadedDeviceInfoState;
  const LoadedDeviceInfoState._() : super._();

  String get appVersion;
  String get appBuild;
  @JsonKey(ignore: true)
  _$$LoadedDeviceInfoStateCopyWith<_$LoadedDeviceInfoState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailureDeviceInfoStateCopyWith<$Res> {
  factory _$$FailureDeviceInfoStateCopyWith(_$FailureDeviceInfoState value,
          $Res Function(_$FailureDeviceInfoState) then) =
      __$$FailureDeviceInfoStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailureDeviceInfoStateCopyWithImpl<$Res>
    extends _$DeviceInfoStateCopyWithImpl<$Res>
    implements _$$FailureDeviceInfoStateCopyWith<$Res> {
  __$$FailureDeviceInfoStateCopyWithImpl(_$FailureDeviceInfoState _value,
      $Res Function(_$FailureDeviceInfoState) _then)
      : super(_value, (v) => _then(v as _$FailureDeviceInfoState));

  @override
  _$FailureDeviceInfoState get _value =>
      super._value as _$FailureDeviceInfoState;
}

/// @nodoc

class _$FailureDeviceInfoState extends FailureDeviceInfoState
    with DiagnosticableTreeMixin {
  const _$FailureDeviceInfoState() : super._();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'DeviceInfoState.failure()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'DeviceInfoState.failure'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FailureDeviceInfoState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String appVersion, String appBuild) loaded,
    required TResult Function() failure,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String appVersion, String appBuild)? loaded,
    TResult Function()? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialDeviceInfoState value) initial,
    required TResult Function(LoadingDeviceInfoState value) loading,
    required TResult Function(LoadedDeviceInfoState value) loaded,
    required TResult Function(FailureDeviceInfoState value) failure,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialDeviceInfoState value)? initial,
    TResult Function(LoadingDeviceInfoState value)? loading,
    TResult Function(LoadedDeviceInfoState value)? loaded,
    TResult Function(FailureDeviceInfoState value)? failure,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class FailureDeviceInfoState extends DeviceInfoState {
  const factory FailureDeviceInfoState() = _$FailureDeviceInfoState;
  const FailureDeviceInfoState._() : super._();
}
