part of 'purchased_bloc.dart';

abstract class PurchasedEvent extends Equatable {
  const PurchasedEvent();

  @override
  List<Object> get props => [];
}

class LoadPurchasedProductsEvent extends PurchasedEvent {}
