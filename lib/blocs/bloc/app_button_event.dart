part of 'app_button_bloc.dart';

abstract class AppButtonEvent extends Equatable {
  const AppButtonEvent();

  @override
  List<Object> get props => [];
}

class AppButtonStartEvent extends AppButtonEvent {
  final AppModel appModel;

  const AppButtonStartEvent({required this.appModel});

  @override
  String toString() => 'AppButtonStartEvent(appModel: $appModel)';

  @override
  List<Object> get props => [appModel];
}

class AppButtonPauseEvent extends AppButtonEvent {
  final AppModel appModel;

  const AppButtonPauseEvent({required this.appModel});

  @override
  String toString() => 'AppButtonStartEvent(appModel: $appModel)';

  @override
  List<Object> get props => [appModel];
}
