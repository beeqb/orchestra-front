import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import '../../models/user.dart';
import '../../services/user_service.dart';

part 'all_users_event.dart';
part 'all_users_state.dart';

class AllUsersBloc extends Bloc<AllUsersEvent, AllUsersState> {
  AllUsersBloc({required UserService userService})
      : _userService = userService,
        super(AllUsersInitial());

  final UserService _userService;

  @override
  Future<void> close() async {
    await super.close();
    //debugPrint('close bloc');
  }

  @override
  Stream<AllUsersState> mapEventToState(
    AllUsersEvent event,
  ) async* {
    switch (event.runtimeType) {
      case FetchAllUsersEvent:
        yield* _mapFetchAllUsers();
        break;
      case SortAllUsersEvent:
        debugPrint('sort event');
        yield* _mapSortAllUsers(
          (event as SortAllUsersEvent).getField,
          (event).ascending,
          (event).sortColumnIndex,
        );
        break;
      case SearchUsersEvent:
        yield* _mapSearchUsers((event as SearchUsersEvent).query);
        break;
      default:
        yield const AllUsersFailure(
            message: 'AllUsersBloc error: unknown event');
    }
  }

  Stream<AllUsersState> _mapSearchUsers(String query) async* {
    yield AllUsersLoading();
    try {
      final result = await _userService
          .searchUsers(query)
          .timeout(const Duration(seconds: 20));
      yield result.fold(
          (l) => AllUsersFailure(message: 'AllUsersBloc server error $l'),
          (r) => AllUsersLoaded(users: r, sortAscending: true));
    } on TimeoutException catch (e) {
      yield AllUsersFailure(message: 'AllUsersBloc timeout error $e');
    } catch (e) {
      yield AllUsersFailure(message: 'AllUsersBloc error $e');
    }
  }

  Stream<AllUsersState> _mapFetchAllUsers() async* {
    yield AllUsersLoading();
    try {
      final result = await _userService
          .fetchAllUsers()
          .timeout(const Duration(seconds: 20));
      yield AllUsersLoaded(users: result, sortAscending: true);
    } on TimeoutException catch (e) {
      yield AllUsersFailure(message: 'AllUsersBloc timeout error $e');
    } catch (e) {
      yield AllUsersFailure(message: 'AllUsersBloc error $e');
    }
  }

  Stream<AllUsersState> _mapSortAllUsers<T>(
    Comparable<T> Function(UserModel d) getField,
    bool ascending,
    int colIndex,
  ) async* {
    debugPrint('bloc sort $ascending');
    if (state is AllUsersLoaded) {
      yield AllUsersLoading();
      final _sortedUsers = (state as AllUsersLoaded).users;
      _sortedUsers.sort((a, b) {
        final aValue = getField(a);
        final bValue = getField(b);
        return ascending
            ? Comparable.compare(aValue, bValue)
            : Comparable.compare(bValue, aValue);
      });
      yield AllUsersLoaded(
        users: _sortedUsers,
        sortColumnIndex: colIndex,
        sortAscending: ascending,
      );
    }
  }
}
