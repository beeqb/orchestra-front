part of 'search_product_bloc.dart';

abstract class SearchProductEvent extends Equatable {
  const SearchProductEvent();

  @override
  List<Object> get props => [];
}

class FetchSearchedProductsEvent extends SearchProductEvent {
  final String query;

  const FetchSearchedProductsEvent({required this.query});
  @override
  List<Object> get props => [query];
}
