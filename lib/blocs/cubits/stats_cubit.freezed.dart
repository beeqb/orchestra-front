// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'stats_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$StatsState {
  List<DailySales> get dailySales => throw _privateConstructorUsedError;
  LaunchStats? get launchStats => throw _privateConstructorUsedError;
  CustomerActivity? get customerActivity => throw _privateConstructorUsedError;
  StatisticsLoadingState get salesStatisticsLoadingState =>
      throw _privateConstructorUsedError;
  StatisticsLoadingState get launchesStatisticsLoadingState =>
      throw _privateConstructorUsedError;
  StatisticsLoadingState get customerActivityLoadingState =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StatsStateCopyWith<StatsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StatsStateCopyWith<$Res> {
  factory $StatsStateCopyWith(
          StatsState value, $Res Function(StatsState) then) =
      _$StatsStateCopyWithImpl<$Res>;
  $Res call(
      {List<DailySales> dailySales,
      LaunchStats? launchStats,
      CustomerActivity? customerActivity,
      StatisticsLoadingState salesStatisticsLoadingState,
      StatisticsLoadingState launchesStatisticsLoadingState,
      StatisticsLoadingState customerActivityLoadingState});

  $LaunchStatsCopyWith<$Res>? get launchStats;
  $CustomerActivityCopyWith<$Res>? get customerActivity;
}

/// @nodoc
class _$StatsStateCopyWithImpl<$Res> implements $StatsStateCopyWith<$Res> {
  _$StatsStateCopyWithImpl(this._value, this._then);

  final StatsState _value;
  // ignore: unused_field
  final $Res Function(StatsState) _then;

  @override
  $Res call({
    Object? dailySales = freezed,
    Object? launchStats = freezed,
    Object? customerActivity = freezed,
    Object? salesStatisticsLoadingState = freezed,
    Object? launchesStatisticsLoadingState = freezed,
    Object? customerActivityLoadingState = freezed,
  }) {
    return _then(_value.copyWith(
      dailySales: dailySales == freezed
          ? _value.dailySales
          : dailySales // ignore: cast_nullable_to_non_nullable
              as List<DailySales>,
      launchStats: launchStats == freezed
          ? _value.launchStats
          : launchStats // ignore: cast_nullable_to_non_nullable
              as LaunchStats?,
      customerActivity: customerActivity == freezed
          ? _value.customerActivity
          : customerActivity // ignore: cast_nullable_to_non_nullable
              as CustomerActivity?,
      salesStatisticsLoadingState: salesStatisticsLoadingState == freezed
          ? _value.salesStatisticsLoadingState
          : salesStatisticsLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
      launchesStatisticsLoadingState: launchesStatisticsLoadingState == freezed
          ? _value.launchesStatisticsLoadingState
          : launchesStatisticsLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
      customerActivityLoadingState: customerActivityLoadingState == freezed
          ? _value.customerActivityLoadingState
          : customerActivityLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
    ));
  }

  @override
  $LaunchStatsCopyWith<$Res>? get launchStats {
    if (_value.launchStats == null) {
      return null;
    }

    return $LaunchStatsCopyWith<$Res>(_value.launchStats!, (value) {
      return _then(_value.copyWith(launchStats: value));
    });
  }

  @override
  $CustomerActivityCopyWith<$Res>? get customerActivity {
    if (_value.customerActivity == null) {
      return null;
    }

    return $CustomerActivityCopyWith<$Res>(_value.customerActivity!, (value) {
      return _then(_value.copyWith(customerActivity: value));
    });
  }
}

/// @nodoc
abstract class _$$_StatsStateCopyWith<$Res>
    implements $StatsStateCopyWith<$Res> {
  factory _$$_StatsStateCopyWith(
          _$_StatsState value, $Res Function(_$_StatsState) then) =
      __$$_StatsStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<DailySales> dailySales,
      LaunchStats? launchStats,
      CustomerActivity? customerActivity,
      StatisticsLoadingState salesStatisticsLoadingState,
      StatisticsLoadingState launchesStatisticsLoadingState,
      StatisticsLoadingState customerActivityLoadingState});

  @override
  $LaunchStatsCopyWith<$Res>? get launchStats;
  @override
  $CustomerActivityCopyWith<$Res>? get customerActivity;
}

/// @nodoc
class __$$_StatsStateCopyWithImpl<$Res> extends _$StatsStateCopyWithImpl<$Res>
    implements _$$_StatsStateCopyWith<$Res> {
  __$$_StatsStateCopyWithImpl(
      _$_StatsState _value, $Res Function(_$_StatsState) _then)
      : super(_value, (v) => _then(v as _$_StatsState));

  @override
  _$_StatsState get _value => super._value as _$_StatsState;

  @override
  $Res call({
    Object? dailySales = freezed,
    Object? launchStats = freezed,
    Object? customerActivity = freezed,
    Object? salesStatisticsLoadingState = freezed,
    Object? launchesStatisticsLoadingState = freezed,
    Object? customerActivityLoadingState = freezed,
  }) {
    return _then(_$_StatsState(
      dailySales: dailySales == freezed
          ? _value._dailySales
          : dailySales // ignore: cast_nullable_to_non_nullable
              as List<DailySales>,
      launchStats: launchStats == freezed
          ? _value.launchStats
          : launchStats // ignore: cast_nullable_to_non_nullable
              as LaunchStats?,
      customerActivity: customerActivity == freezed
          ? _value.customerActivity
          : customerActivity // ignore: cast_nullable_to_non_nullable
              as CustomerActivity?,
      salesStatisticsLoadingState: salesStatisticsLoadingState == freezed
          ? _value.salesStatisticsLoadingState
          : salesStatisticsLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
      launchesStatisticsLoadingState: launchesStatisticsLoadingState == freezed
          ? _value.launchesStatisticsLoadingState
          : launchesStatisticsLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
      customerActivityLoadingState: customerActivityLoadingState == freezed
          ? _value.customerActivityLoadingState
          : customerActivityLoadingState // ignore: cast_nullable_to_non_nullable
              as StatisticsLoadingState,
    ));
  }
}

/// @nodoc

class _$_StatsState implements _StatsState {
  const _$_StatsState(
      {final List<DailySales> dailySales = const [],
      this.launchStats,
      this.customerActivity,
      this.salesStatisticsLoadingState = StatisticsLoadingState.initial,
      this.launchesStatisticsLoadingState = StatisticsLoadingState.initial,
      this.customerActivityLoadingState = StatisticsLoadingState.initial})
      : _dailySales = dailySales;

  final List<DailySales> _dailySales;
  @override
  @JsonKey()
  List<DailySales> get dailySales {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_dailySales);
  }

  @override
  final LaunchStats? launchStats;
  @override
  final CustomerActivity? customerActivity;
  @override
  @JsonKey()
  final StatisticsLoadingState salesStatisticsLoadingState;
  @override
  @JsonKey()
  final StatisticsLoadingState launchesStatisticsLoadingState;
  @override
  @JsonKey()
  final StatisticsLoadingState customerActivityLoadingState;

  @override
  String toString() {
    return 'StatsState(dailySales: $dailySales, launchStats: $launchStats, customerActivity: $customerActivity, salesStatisticsLoadingState: $salesStatisticsLoadingState, launchesStatisticsLoadingState: $launchesStatisticsLoadingState, customerActivityLoadingState: $customerActivityLoadingState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StatsState &&
            const DeepCollectionEquality()
                .equals(other._dailySales, _dailySales) &&
            const DeepCollectionEquality()
                .equals(other.launchStats, launchStats) &&
            const DeepCollectionEquality()
                .equals(other.customerActivity, customerActivity) &&
            const DeepCollectionEquality().equals(
                other.salesStatisticsLoadingState,
                salesStatisticsLoadingState) &&
            const DeepCollectionEquality().equals(
                other.launchesStatisticsLoadingState,
                launchesStatisticsLoadingState) &&
            const DeepCollectionEquality().equals(
                other.customerActivityLoadingState,
                customerActivityLoadingState));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_dailySales),
      const DeepCollectionEquality().hash(launchStats),
      const DeepCollectionEquality().hash(customerActivity),
      const DeepCollectionEquality().hash(salesStatisticsLoadingState),
      const DeepCollectionEquality().hash(launchesStatisticsLoadingState),
      const DeepCollectionEquality().hash(customerActivityLoadingState));

  @JsonKey(ignore: true)
  @override
  _$$_StatsStateCopyWith<_$_StatsState> get copyWith =>
      __$$_StatsStateCopyWithImpl<_$_StatsState>(this, _$identity);
}

abstract class _StatsState implements StatsState {
  const factory _StatsState(
          {final List<DailySales> dailySales,
          final LaunchStats? launchStats,
          final CustomerActivity? customerActivity,
          final StatisticsLoadingState salesStatisticsLoadingState,
          final StatisticsLoadingState launchesStatisticsLoadingState,
          final StatisticsLoadingState customerActivityLoadingState}) =
      _$_StatsState;

  @override
  List<DailySales> get dailySales;
  @override
  LaunchStats? get launchStats;
  @override
  CustomerActivity? get customerActivity;
  @override
  StatisticsLoadingState get salesStatisticsLoadingState;
  @override
  StatisticsLoadingState get launchesStatisticsLoadingState;
  @override
  StatisticsLoadingState get customerActivityLoadingState;
  @override
  @JsonKey(ignore: true)
  _$$_StatsStateCopyWith<_$_StatsState> get copyWith =>
      throw _privateConstructorUsedError;
}
