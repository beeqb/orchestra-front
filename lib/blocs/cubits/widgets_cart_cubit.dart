import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';

abstract class WidgetsCartState extends Equatable {
  const WidgetsCartState();

  @override
  List<Object> get props => [];
}

class WidgetsCartInitial extends WidgetsCartState {}

class WidgetLoading extends WidgetsCartState {}

class WidgetLoaded extends WidgetsCartState {
  final List<Product> products;
  const WidgetLoaded({required this.products});

  @override
  String toString() => 'WidgetLoaded(products: $products)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is WidgetLoaded && listEquals(o.products, products);
  }

  @override
  int get hashCode => products.hashCode;
}

class WidgetsError extends WidgetsCartState {
  final String message;
  const WidgetsError(this.message);

  @override
  String toString() => 'WidgetsError(message: $message)';
}

//=======================================================================

class WidgetsCartCubit extends Cubit<WidgetsCartState> {
  List<Product> products = [];
  WidgetsCartCubit() : super(WidgetsCartInitial());

  void addToCart(Product product) {
    try {
      emit(WidgetLoading());
      products.add(product);
      emit(WidgetLoaded(products: products));
    } on Exception catch (e) {
      emit(WidgetsError('Add product to cart ERROR: $e'));
    }
  }

  void deleteFromCart(Product product) {
    try {
      emit(WidgetLoading());
      products.removeWhere((e) => e.id == product.id);
      emit(WidgetLoaded(products: products));
    } on Exception catch (e) {
      emit(WidgetsError('Delete product to cart ERROR: $e'));
    }
  }

  void clearCart() {
    emit(WidgetLoading());
    products.clear();
    emit(WidgetLoaded(products: products));
  }
}
//===========================================================================
