import 'dart:async';

import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../bloc/navigation_bloc.dart';

class NavigationMenuCubit extends HydratedCubit<int> {
  final NavigationBLoC _navBloc;

  StreamSubscription? _navBlocSubscription;

  NavigationMenuCubit({required NavigationBLoC navBloc})
      : _navBloc = navBloc,
        super(0) {
    _navBlocSubscription = _navBloc.stream.listen((state) {
      if (state is MarketplaceNavigationState) {
        emit(0);
      }
      if (state is MyWidgetsNavigationState) {
        emit(1);
      }
      if (state is DashboardNavigationState) {
        emit(2);
      }
      if (state is BillingsNavigationState) {
        emit(3);
      }
      if (state is AdminNavigationState) {
        emit(4);
      }
    });
  }

  void chooseNavItem(int index) {
    emit(index);
  }

  @override
  int fromJson(Map<String, dynamic> json) => json['value'] as int;

  @override
  Map<String, int> toJson(int state) => {'value': state};
}
