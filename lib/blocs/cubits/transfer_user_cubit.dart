import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../models/billing_models/transfer_model.dart';
import '../../models/billing_models/transfer_user.dart';
import '../../models/enums/enums.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';
import '../../util/text_util.dart';
import 'user_cubit.dart';

abstract class TransferUserState extends Equatable {
  const TransferUserState();

  @override
  List<Object> get props => [];
}

class TransferUserInitial extends TransferUserState {}

class TransferUserUpdating extends TransferUserState {}

class TransferUserUpdated extends TransferUserState {
  final TransferUser transferUser;

  const TransferUserUpdated({required this.transferUser});

  @override
  String toString() => 'UserInfoUpdated(currentUser: $transferUser)';
}

class TransferSending extends TransferUserState {}

class TransferSent extends TransferUserState {
  final TransferModel transferModel;
  const TransferSent({
    required this.transferModel,
  });

  @override
  String toString() => 'TransferSent(transferModel: $transferModel)';
}

class TransferUserError extends TransferUserState {
  final String message;

  const TransferUserError({required this.message});

  @override
  String toString() => 'UserUpdatingError(message: $message)';
}

//=========================================

class TransferUserCubit extends Cubit<TransferUserState> {
  final UserCubit _userCubit;
  final BillingService _billingService;
  final DialogsService _dialogsService;

  TransferUserCubit({
    required UserCubit userCubit,
    required BillingService billingService,
    required DialogsService dialogsService,
  })  : _userCubit = userCubit,
        _billingService = billingService,
        _dialogsService = dialogsService,
        super(TransferUserInitial());

  TransferUser? transferUser;

  Future<void> getUserByQuery(String query) async {
    emit(TransferUserUpdating());
    try {
      transferUser = await _billingService
          .getTransferUser(query)
          .timeout(const Duration(seconds: 20));
      if (transferUser != null) {
        emit(TransferUserUpdated(transferUser: transferUser!));
      } else {
        throw Error();
      }
    } on TimeoutException catch (e) {
      emit(TransferUserError(
          message:
              'TRANSFER_USER_CUBIT getTransferUser ERROR ${e.toString()}'));
    } catch (e) {
      emit(TransferUserError(
          message: 'TRANSFER_USER_CUBIT getTransferUser ERROR $e'));
      rethrow;
    }
  }

  Future<void> sendTransfer(double amount) async {
    TransferModel _transferModel;
    emit(TransferSending());
    try {
      _transferModel = await _billingService
          .send(transferUser!.id, amount)
          .timeout(const Duration(seconds: 20));

      await _userCubit.getUserInfo().then((value) {
        emit(TransferSent(transferModel: _transferModel));
        showTransferDialog(
          transferModel: _transferModel,
          amount: amount,
        );
      }).timeout(const Duration(seconds: 2));
    } on TimeoutException catch (e) {
      emit(TransferUserError(
          message:
              'TRANSFER_USER_CUBIT getTransferUser ERROR ${e.toString()}'));
    } catch (e) {
      emit(TransferUserError(
          message: 'TRANSFER_USER_CUBIT getTransferUser ERROR $e'));
      rethrow;
    }
  }

  void showTransferDialog({
    required TransferModel transferModel,
    required double amount,
  }) async {
    await _dialogsService.showDialog(
      dialogType: DialogType.successDialog,
      description: 'You sent ${formatCurrency.format(amount)}'
          ' to ${transferModel.meta['to']['name']}',
    );
  }
}
