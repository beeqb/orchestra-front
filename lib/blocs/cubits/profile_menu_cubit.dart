import 'package:bloc/bloc.dart';

class ProfileMenuCubit extends Cubit<int> {
  ProfileMenuCubit() : super(0);

  void chooseMenuItem(int index) {
    emit(index);
  }
}
