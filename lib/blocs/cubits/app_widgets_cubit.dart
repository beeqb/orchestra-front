import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/models/product/product.dart';
import '../../models/enums/dialog_type.dart';

import '../../services/app_service.dart';
import '../../services/dialogs_service.dart';
import '../../services/product_service.dart';

abstract class AppWidgetsState {
  const AppWidgetsState();
}

class AppWidgetsInitial extends AppWidgetsState {}

class AppWidgetsLoading extends AppWidgetsState {}

class AppWidgetsLoaded extends AppWidgetsState {
  final List<Product> appProducts;

  AppWidgetsLoaded(this.appProducts);

  @override
  String toString() => 'AppWidgetsLoaded(appProducts: $appProducts)';
}

class AppWidgetsError extends AppWidgetsState {
  final String message;

  AppWidgetsError({required this.message});

  @override
  String toString() => 'AppWidgetsError(message: $message)';
}

//===============================================

class AppWidgetsCubit extends Cubit<AppWidgetsState> {
  final ProductService _productService;
  final AppService _appService;
  final DialogsService _dialogsService;

  AppWidgetsCubit({
    required ProductService productService,
    required AppService appService,
    required DialogsService dialogsService,
  })  : _appService = appService,
        _productService = productService,
        _dialogsService = dialogsService,
        super(AppWidgetsInitial());

  Future<void> getAppWidgets(String appId) async {
    emit(AppWidgetsLoading());
    try {
      final appWidgets = await _appService
          .getWidgetsByAppId(appId)
          .timeout(const Duration(seconds: 20));
      emit(AppWidgetsLoaded(appWidgets));
    } on TimeoutException catch (e) {
      debugPrint('AppWidgetsCubit timeout error $e');
      emit(AppWidgetsError(message: e.toString()));
    } catch (e) {
      debugPrint('AppWidgetsCubit error $e');
      emit(AppWidgetsError(message: e.toString()));
      rethrow;
    }
  }

  Future<void> addWidgetToApp(
      Product product, String appId, String userId) async {
    emit(AppWidgetsLoading());
    try {
      // Сохранение выбранного виджета для приложения
      // TODO добавить изменение картинки приложения при добавлении виджета
      final result = await _productService
          .saveWidgetForApp(product, appId, userId)
          .timeout(const Duration(seconds: 20));
      if (result.message == 'success') {
        final appWidgets = await _appService
            .getWidgetsByAppId(appId)
            .timeout(const Duration(seconds: 20));
        emit(AppWidgetsLoaded(appWidgets));
      } else {
        emit(AppWidgetsError(message: result.message));
      }
    } on TimeoutException catch (e) {
      debugPrint('AppWidgetsCubit timeout error $e');
      emit(AppWidgetsError(message: e.toString()));
    } catch (e) {
      debugPrint('AppWidgetsCubit error $e');
      emit(AppWidgetsError(message: e.toString()));
      rethrow;
    }
  }

  Future<void> deleteWidgetFromApp(
      Product product, String appId, String userId) async {
    emit(AppWidgetsLoading());
    try {
      await _productService
          .deleteWidgetFromApp(userId, appId, product.id)
          .timeout(const Duration(seconds: 20));
      final appWidgets = await _appService
          .getWidgetsByAppId(appId)
          .timeout(const Duration(seconds: 20));
      await _dialogsService.showDialog(
        dialogType: DialogType.warningDialog,
        title: 'Lore deleted!',
        description: 'Lore deleted from current app',
      );
      emit(AppWidgetsLoaded(appWidgets));
    } on TimeoutException catch (e) {
      debugPrint('AppWidgetsCubit timeout error $e');
      emit(AppWidgetsError(message: e.toString()));
    } catch (e) {
      debugPrint('AppWidgetsCubit error $e');
      emit(AppWidgetsError(message: e.toString()));
      rethrow;
    }
  }
}
