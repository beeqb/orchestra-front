import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/services/user_service.dart';

import '../../models/customer_activity.dart';
import '../../models/daily_sale.dart';
import '../../models/launch_stats.dart';

part 'stats_cubit.freezed.dart';

enum StatisticsLoadingState {
  initial,
  loading,
  completed,
  error;

  bool get isInitial => this == StatisticsLoadingState.initial;
  bool get isLoading => this == StatisticsLoadingState.loading;
  bool get isCompleted => this == StatisticsLoadingState.completed;
  bool get isError => this == StatisticsLoadingState.error;
}

@freezed
class StatsState with _$StatsState {
  const factory StatsState({
    @Default([]) List<DailySales> dailySales,
    LaunchStats? launchStats,
    CustomerActivity? customerActivity,
    @Default(StatisticsLoadingState.initial)
        StatisticsLoadingState salesStatisticsLoadingState,
    @Default(StatisticsLoadingState.initial)
        StatisticsLoadingState launchesStatisticsLoadingState,
    @Default(StatisticsLoadingState.initial)
        StatisticsLoadingState customerActivityLoadingState,
  }) = _StatsState;
}

class StatsCubit extends Cubit<StatsState> {
  final UserService _userService;

  StatsCubit({
    required UserService userService,
  })  : _userService = userService,
        super(const StatsState());

  Future<void> getUserDailySales() async {
    emit(const StatsState(
        salesStatisticsLoadingState: StatisticsLoadingState.loading));
    try {
      final dailySales = await _userService.getDailySales().timeout(
            const Duration(seconds: 10),
          );
      dailySales.fold(
          (l) => emit(state.copyWith(
              salesStatisticsLoadingState: StatisticsLoadingState.error)),
          (r) => emit(state.copyWith(
              dailySales: r,
              salesStatisticsLoadingState: StatisticsLoadingState.completed)));
    } on TimeoutException catch (e) {
      debugPrint('UserCubit timeout error $e');
      emit(state.copyWith(
          salesStatisticsLoadingState: StatisticsLoadingState.error));
    } catch (e) {
      debugPrint('UserCubit error $e');
      emit(state.copyWith(
          salesStatisticsLoadingState: StatisticsLoadingState.error));
      rethrow;
    }
  }

  Future<void> getLaunchStats() async {
    emit(const StatsState(
        launchesStatisticsLoadingState: StatisticsLoadingState.loading));
    try {
      final launchStats = await _userService.getLaunchStats().timeout(
            const Duration(seconds: 10),
          );
      launchStats.fold(
          (l) => emit(state.copyWith(
              launchesStatisticsLoadingState: StatisticsLoadingState.error)),
          (r) => emit(state.copyWith(
              launchStats: r,
              launchesStatisticsLoadingState:
                  StatisticsLoadingState.completed)));
    } on TimeoutException catch (e) {
      debugPrint('UserCubit timeout error $e');
      emit(state.copyWith(
          launchesStatisticsLoadingState: StatisticsLoadingState.error));
    } catch (e) {
      debugPrint('UserCubit error $e');
      emit(state.copyWith(
          launchesStatisticsLoadingState: StatisticsLoadingState.error));
      rethrow;
    }
  }

  Future<void> getCustomerActivity() async {
    emit(const StatsState(
        customerActivityLoadingState: StatisticsLoadingState.loading));
    try {
      final customerActivity = await _userService
          .getCustomerActivity()
          .timeout(const Duration(seconds: 10));
      customerActivity.fold(
          (l) => emit(state.copyWith(
              customerActivityLoadingState: StatisticsLoadingState.error)),
          (r) => emit(state.copyWith(
              customerActivity: r,
              customerActivityLoadingState: StatisticsLoadingState.completed)));
    } on TimeoutException catch (e) {
      debugPrint('UserCubit timeout error $e');
      emit(state.copyWith(
          customerActivityLoadingState: StatisticsLoadingState.error));
    } catch (e) {
      debugPrint('UserCubit error $e');
      emit(state.copyWith(
          customerActivityLoadingState: StatisticsLoadingState.error));
      rethrow;
    }
  }
}
