import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';

import '../../models/user.dart';
import '../../services/user_service.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}

class UserInitial extends UserState {}

class UserInfoUpdating extends UserState {}

class UserInfoUpdated extends UserState {
  final UserModel currentUser;

  const UserInfoUpdated({required this.currentUser});

  @override
  List<Object> get props => [currentUser];

  @override
  String toString() => 'UserInfoUpdated(currentUser: $currentUser)';
}

class UserUpdatingError extends UserState {
  final String message;

  const UserUpdatingError({required this.message});
}

//=========================================

class UserCubit extends Cubit<UserState> {
  final UserService _userService;

  UserCubit({
    required UserService userService,
    required AuthBloc authBloc,
  })  : _userService = userService,
        super(UserInitial());

  Future<void> updateUserInfo(UserModel updatedUser) async {
    emit(UserInfoUpdating());
    try {
      final currentUser = await _userService
          .updateUser(updatedUser)
          .timeout(const Duration(seconds: 20));
      currentUser.fold(
        (l) => emit(UserUpdatingError(message: l.toString())),
        (r) => emit(UserInfoUpdated(currentUser: r)),
      );
    } on TimeoutException catch (e) {
      debugPrint('UserCubit timeout error $e');
      emit(UserUpdatingError(message: e.toString()));
    } catch (e) {
      debugPrint('UserCubit error $e');
      emit(UserUpdatingError(message: e.toString()));
      rethrow;
    }
  }

  Future<void> getUserInfo() async {
    emit(UserInfoUpdating());
    try {
      final currentUser = await _userService.getUser().timeout(
            const Duration(seconds: 10),
          );
      // update currentUser for context
      //_authBloc.add(PresentAuthEvent(currentUser));
      emit(UserInfoUpdated(currentUser: currentUser));
    } on TimeoutException catch (e) {
      debugPrint('UserCubit timeout error $e');
      emit(UserUpdatingError(message: e.toString()));
    } catch (e) {
      debugPrint('UserCubit error $e');
      emit(UserUpdatingError(message: e.toString()));
      rethrow;
    }
  }
}
