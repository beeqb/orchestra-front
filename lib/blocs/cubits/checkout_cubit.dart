import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/enums/enums.dart';
import '../../services/billing_service.dart';
import '../../services/dialogs_service.dart';

abstract class CheckoutState extends Equatable {
  const CheckoutState();

  @override
  List<Object> get props => [];
}

class CheckoutInitial extends CheckoutState {}

class CheckoutLoading extends CheckoutState {}

class CheckoutLoaded extends CheckoutState {
  final String session;
  const CheckoutLoaded({
    required this.session,
  });
}

class CheckoutError extends CheckoutState {
  final String message;

  const CheckoutError({required this.message});
}

//==================================================

class CheckoutCubit extends Cubit<CheckoutState> {
  final BillingService _billingService;
  final DialogsService _dailogService;

  CheckoutCubit({
    required BillingService billingService,
    required DialogsService dialogsService,
  })  : _billingService = billingService,
        _dailogService = dialogsService,
        super(CheckoutInitial());

  Future<void> createCheckout(String gateway, double amount) async {
    emit(CheckoutLoading());
    try {
      final result = await _billingService
          .deposit(gateway: gateway, amount: amount)
          .timeout(const Duration(seconds: 20));
      debugPrint('CheckoutCubit result $result');
      if (result.isNotEmpty) {
        if (result['status'] == 'error') {
          emit(CheckoutError(
              message: 'CHECKOUT_ERROR server response ${result['message']}'));
        } else {
          if (gateway == 'stripe') {
            emit(CheckoutLoaded(session: result['id']));
          } else if (gateway == 'coinbase') {
            emit(CheckoutLoaded(session: result['url']));
          }
        }
      } else {
        emit(const CheckoutError(
            message: 'CHECKOUT_ERROR server response NULL'));
      }
    } on TimeoutException catch (e) {
      emit(CheckoutError(message: 'CHECKOUT_ERROR timeout $e'));
    } catch (e) {
      debugPrint('CheckoutCubit error $e');
      await _dailogService.showDialog(
        dialogType: DialogType.alertDialog,
        title: 'Wrong server response',
        description: e.toString(),
      );
      emit(CheckoutError(message: 'CHECKOUT_ERROR $e'));
      rethrow;
    }
  }
}
