import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/models/product/product.dart';

import '../../models/app_category.dart';
import '../../services/product_service.dart';

abstract class ProductsState extends Equatable {
  const ProductsState();

  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductsState {}

// грузим все продукты без разбивки по категориям
class ProductsLoading extends ProductsState {
  const ProductsLoading();
}

class ProductsLoaded extends ProductsState {
  final List<AppCategory> categories;
  final List<Product?> products;

  const ProductsLoaded(this.categories, this.products);
}

class ProductsError extends ProductsState {
  final String message;

  const ProductsError({required this.message});

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ProductsError && o.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}

//=======================================================================

class ProductsCubit extends Cubit<ProductsState> {
  final AuthBloc authBloc;
  final ProductService _productService;
  StreamSubscription? authSubscription;

  ProductsCubit({
    required this.authBloc,
    required ProductService productService,
  })  : _productService = productService,
        super(ProductsInitial()) {
    authSubscription = authBloc.stream.listen((state) {
      if (state is LoggedAuthState) {
        getProducts();
      }
    });
  }

  List<Product?>? productsList;

  @override
  Future<void> close() {
    authSubscription!.cancel();
    return super.close();
  }

  Future<void> getProducts() async {
    try {
      emit(const ProductsLoading());
      final _categories = await _productService
          .getProductsNotCreatedByUser()
          .timeout(const Duration(seconds: 55));

      _categories.fold(
        (l) => emit(ProductsError(message: l.toString())),
        (r) {
          productsList = [];
          for (var prod in r) {
            productsList!.addAll(prod.prods);
          }
          emit(ProductsLoaded(r, productsList!));
        },
      );
    } on TimeoutException catch (e) {
      debugPrint('ProductsCubit timeout error $e');
      emit(ProductsError(message: e.toString()));
    } catch (e) {
      debugPrint('ProductsCubit error $e');
      emit(ProductsError(message: e.toString()));
      rethrow;
    }
  }
}
