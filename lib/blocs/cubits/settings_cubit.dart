import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/settings_model.dart';
import '../../services/app_service.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

class SettingsInitial extends SettingsState {}

class SettingsLoading extends SettingsState {}

class SettingsLoaded extends SettingsState {
  final List<SettingsModel> settings;
  final String oldSettings;

  const SettingsLoaded({required this.settings, required this.oldSettings});

  @override
  List<Object> get props => [settings, oldSettings];
}

class SettingsUpdated extends SettingsState {
  final Map<String, dynamic> settings;

  const SettingsUpdated({
    required this.settings,
  });

  // @override
  // List<Object> get props => [settings];
}

class SettingsReady extends SettingsState {
  final List<SettingsModel> settings;

  const SettingsReady({required this.settings});
  @override
  List<Object> get props => [settings];
}

class SettingsError extends SettingsState {
  final String message;

  const SettingsError({required this.message});
}

//===================================================================

class SettingsCubit extends Cubit<SettingsState> {
  final AppService _appService;

  SettingsCubit({
    required AppService appService,
  })  : _appService = appService,
        super(SettingsInitial());

  Future<void> getWidgetSettings(
    String appId,
    String creatorId,
    String productId,
    String userId,
  ) async {
    emit(SettingsLoading());
    try {
      final manifest = await _appService
          .getWidgetSettings(productId)
          .timeout(const Duration(seconds: 20));
      final _oldSettings = await _appService
          .getAppWidgetSettings(appId)
          .timeout(const Duration(seconds: 20));
      emit(SettingsLoaded(settings: manifest, oldSettings: _oldSettings));
    } on TimeoutException catch (e) {
      debugPrint('SettingsCubit getWidgetSettings error $e');
      emit(SettingsError(message: e.toString()));
    } catch (e) {
      debugPrint('SettingsCubit getWidgetSettings error $e');
      emit(SettingsError(message: e.toString()));
      rethrow;
    }
  }

  Future<void> saveAppWidgetSettings({
    required String appId,
    required String productId,
    required String userId,
    required Map<String, dynamic> settings,
  }) async {
    emit(SettingsLoading());

    try {
      await _appService
          .saveAppWidgetSettings(
            appId,
            productId,
            settings,
          )
          .timeout(const Duration(seconds: 20));
      emit(SettingsUpdated(settings: settings));
    } on TimeoutException catch (e) {
      debugPrint('SettingsCubit saveAppWidgetSettings error $e');
      emit(SettingsError(message: e.toString()));
    } catch (e) {
      debugPrint('SettingsCubit saveAppWidgetSettings error $e');
      emit(SettingsError(message: e.toString()));
      rethrow;
    }
  }

  Future<void> readySettings(String productId) async {
    emit(SettingsLoading());
    try {
      final manifest = await _appService
          .getWidgetSettings(productId)
          .timeout(const Duration(seconds: 20));
      emit(SettingsReady(settings: manifest));
    } on TimeoutException catch (e) {
      debugPrint('SettingsCubit readySettings error $e');
      emit(SettingsError(message: e.toString()));
    } catch (e) {
      debugPrint('SettingsCubit readySettings error $e');
      emit(SettingsError(message: e.toString()));
      rethrow;
    }
  }
}
