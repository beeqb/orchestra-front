import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:orchestra/models/billing_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import '../core/exceptions.dart';
import '../models/billing_models/admin_transact_model.dart';
import '../models/billing_models/transaction_model.dart';
import '../models/billing_models/transfer_model.dart';
import '../models/billing_models/transfer_user.dart';

abstract class BillingRepository {
  Future<Map<String, String>> chargeForProduct(String productId);

  Future<List<TransactionModel>> getTransactionHistory(int page);

  Future<Map<String, dynamic>> deposit(String gateway, double amount);

  Future<TransferUser?> getTransferUser(String query);

  Future<TransferModel> send(String userId, double amount);

  Future<List<AdminTransactModel>> getAllTransactions();

  Future<BillingInfoModel> getBillingInfo();
}

class BillingRepositoryImpl extends BillingRepository {
  final http.Client _client;
  final SharedPreferences _sharedPreferences;

  BillingRepositoryImpl({
    required http.Client client,
    required SharedPreferences sharedPreferences,
  })  : _sharedPreferences = sharedPreferences,
        _client = client,
        super();

  String? get token => _sharedPreferences.getString('token');
  Map<String, String> get headers => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

  @override
  Future<Map<String, String>> chargeForProduct(String productId) async {
    final body = {'productId': productId};
    final uri = Uri.parse('$kHostUrl/transaction/chargeForProduct');
    final response =
        await _client.post(uri, body: json.encode(body), headers: headers);
    if (response.statusCode == 200) {
      return Map<String, String>.from(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<TransactionModel>> getTransactionHistory(int page) async {
    const limit = 20;
    final uri =
        Uri.parse('$kHostUrl/transaction/history?page=$page&limit=$limit');
    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      return await compute(parseTransactionsHistory, response.body);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Map<String, dynamic>> deposit(String gateway, double amount) async {
    final body = {'amount': amount, 'gateway': gateway};
    final uri = Uri.parse('$kHostUrl/gateway/deposit');
    final response =
        await _client.post(uri, body: json.encode(body), headers: headers);

    if (response.statusCode == 200) {
      return Map.from(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<TransferUser?> getTransferUser(String query) async {
    try {
      final uri =
          Uri.parse('$kHostUrl/transaction/getTransferUser?identifier=$query');
      final response = await _client.get(uri, headers: headers);

      if (response.statusCode == 200) {
        Map user = json.decode(response.body);

        return TransferUser.fromJson(user['user']);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Future<TransferModel> send(String userId, double amount) async {
    final body = {
      'amount': amount,
      'userId': userId,
    };
    final uri = Uri.parse('$kHostUrl/transaction/send');
    final response =
        await _client.post(uri, body: json.encode(body), headers: headers);
    if (response.statusCode == 200) {
      Map transfer = json.decode(response.body);
      return TransferModel.fromJson(transfer['transfer']);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<AdminTransactModel>> getAllTransactions() async {
    const page = 1;
    const limit = 1500;
    final uri =
        Uri.parse('$kHostUrl/admin/transactions?page=$page&limit=$limit');
    final response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);

      return result['items']
          .map((e) => AdminTransactModel.fromJson(e))
          .cast<AdminTransactModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<BillingInfoModel> getBillingInfo() async {
    final uri = Uri.parse('$kHostUrl/users/balances');
    final response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return BillingInfoModel.fromJson(result);
    } else {
      throw ServerException();
    }
  }
}

List<TransactionModel> parseTransactionsHistory(String body) {
  final Iterable parsed = json.decode(body);
  final res = List.from(parsed);

  final parsedRes = res
      .map((item) {
        //  debugPrint(item);
        final tr = TransactionModel.fromJson(item);
        return tr;
      })
      .cast<TransactionModel>()
      .toList();

  return parsedRes;
}
