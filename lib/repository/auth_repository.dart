import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';
import '../util/constants.dart';

enum LoginType {
  google,
  facebook,
}

abstract class AuthRepository {
  Future<UserModel> signInWithGoogle();
  Future<UserModel> signInWithFacebook();
  Future<UserModel> getLocalUser();
  Future<void> saveLocalUser(UserModel user);
  Future<void> signOut(LoginType type);
}

//================================================

class AuthRepositoryImpl implements AuthRepository {
  final _facebookLogin = FacebookAuth.instance;
  final _googleSignIn = GoogleSignIn(scopes: ['email']);
  final SharedPreferences _sharedPreferences;

  AuthRepositoryImpl({required SharedPreferences sharedPreferences})
      : _sharedPreferences = sharedPreferences,
        super();

  @override
  Future<UserModel> signInWithGoogle() async {
    GoogleSignInAccount profile;

    profile = (await _googleSignIn.signIn())!;

    log(profile.id);
    log(profile.email);
    log('${profile.displayName}');
    log('${profile.photoUrl}');

    return UserModel(
      id: profile.id,
      name: profile.displayName,
      email: profile.email,
      photoUrl: profile.photoUrl,
    );
  }

  @override
  Future<UserModel> signInWithFacebook() async {
    Map<String, dynamic>? _userData;
    try {
      // show a circular progress indicator

      print('await facebook login');
      // final perm = await FacebookAuth.instance.permissions;
      // print(perm?.declined.toString());
      // print(perm?.granted.toString());
      print('await facebook login2');
      final _accessToken = await _facebookLogin.login();

      print('await facebook login accestoken  $_accessToken');

      //
      // by the fault we request the email and the public profile
      // loginBehavior is only supported for Android devices, for ios it will
      //be ignored
      // _accessToken = await FacebookAuth.instance.login(
      //   permissions: ['email', 'public_profile', 'user_birthday',
      //'user_friends',
      //'user_gender', 'user_link'],
      //   loginBehavior:
      //       LoginBehavior.DIALOG_ONLY, // (only android) show an
      //authentication dialog instead of redirecting to facebook app
      // );
      // get the user data
      // by default we get the userId, email,name and picture
      final userData = await _facebookLogin.getUserData();

      print('facebbok user data $userData');

      if (!kReleaseMode) {
        debugPrint('FB token ${_accessToken.accessToken}');
      }
      // final userData = await FacebookAuth.instance.getUserData(fields:
      //"email,birthday,friends,gender,link");
      _userData = userData;
    } /* on FacebookAuthErrorCode catch (e) {
      // if the facebook login fails
      debugPrint(e.toString()); // print the error message in console
      // check the error type

    } */
    on Exception catch (e, s) {
      // print in the logs the unknown errors
      debugPrint(e.toString());
      debugPrint(s.toString());
      print(e.toString());
    }

    log(_userData.toString());

    return UserModel(
      id: _userData!['id'],
      name: _userData['name'],
      email: _userData['email'],
      photoUrl: _userData['picture']['data']['url'],
    );
  }

  @override
  Future<UserModel> getLocalUser() async {
    if (_sharedPreferences.containsKey(kUsersAuthKey)) {
      final userString = _sharedPreferences.getString(kUsersAuthKey);
      final userMap = json.decode(userString!);
      debugPrint('userLocal->$userMap');
      return UserModel.fromJson(userMap);
    } else {
      return UserModel.empty;
    }
  }

  @override
  Future<void> saveLocalUser(UserModel user) async {
    await _sharedPreferences.setString(
        kUsersAuthKey, json.encode(user.toJson()));
  }

  @override
  Future<void> signOut(LoginType type) async {
    await _sharedPreferences.clear();
    try {
      if (type == LoginType.google) {
        await _googleSignIn.signOut();
      } else {
        await _facebookLogin.logOut();
      }
    } catch (e) {
      log(e.toString());
    }
    return Future.value();
  }
}
