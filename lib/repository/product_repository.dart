import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/product_models/post_product.dart';
import 'package:orchestra/models/running_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import '../core/const.dart';
import '../core/exceptions.dart';
import '../models/answer_item.dart';
import '../models/app.dart';
import '../models/app_category.dart';
import '../models/enums/enums.dart';
import '../models/purchase_model.dart';
import '../models/rating_item_model.dart';
import '../models/server_message.dart';
import '../models/user.dart';
import '../util/constants.dart';

abstract class ProductRepository {
  Future<List<AppCategory>> getProductsNotCreatedByUser();

  Future<Product> buyProduct(
    String productId,
  );

  Future<List<PurchaseModel>> getPurchasedProducts();

  Future<ServerMessage> saveWidgetForApp(
    Product product,
    String appId,
    String userId,
  );

  Future<AppModel> deleteWidgetFromApp(
    String userId,
    String appId,
    String productId,
  );

  Future<String> addRatingAndComment(
    String productId,
    String userId,
    int rating,
    String comment,
  );

  Future<List<RatingItem>> getProductRatings(
    String productId,
  );

  Future<List<Product>> getProductsByUserId();

  Future<Product> addProduct(PostProduct product);

  Future<void> changeProductStatusToDeleted(String productId);

  Future<Product> editProductById(Product newProduct);

  Future<List<AppCategory>> getTopProductsByCategories();

  Future<List<Product>> getProductsByQuery(String query);

  Future<dynamic> addRatingAnswer(
    AnswerItem answerItem,
    String productId,
  );

  Future<List<Product>> getAllProducts();

  Future<void> moderate(String productId, ModerateType moderateType);

  Future<Product> getProductById(String productId);

  Future<List<Product>> searchProduct(String query);

  Future<List<Product>> getTopProducts();

  Future<String> getProductSource(String productId);

  Future<String> getProductManifest(String productId);

  Future<String> startTestProduct(
    String productId,
    Map<String, dynamic> settings,
  );

  Future<List<RunningAppModel>> getRunningProducts();

  Future<void> uploadFile(PlatformFile file, String field, String userId);
}

class ProductRepositoryImpl implements ProductRepository {
  final http.Client _client;
  final SharedPreferences _sharedPreferences;

  ProductRepositoryImpl({
    required http.Client client,
    required SharedPreferences sharedPreferences,
  })  : _sharedPreferences = sharedPreferences,
        _client = client,
        super();
  String? get token => _sharedPreferences.getString('token');
  Map<String, String> get headers => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

  @override
  Future<List<AppCategory>> getProductsNotCreatedByUser() async {
    final uri = Uri.parse(UrlConstants.kGetProductsNotCrByUser);
    final response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      final result = await compute(parseAppCategories, response.body);
      return result;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Product> buyProduct(String productId) async {
    final uri = Uri.parse('${UrlConstants.kProductUrl}/$productId/purchase');

    final response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      return Product.fromJson(jsonDecode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<PurchaseModel>> getPurchasedProducts() async {
    final uri = Uri.parse('$kHostUrl/product/getPurchasedProducts');

    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((prod) => PurchaseModel.fromJson(prod))
          .cast<PurchaseModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<ServerMessage> saveWidgetForApp(
      Product product, String appId, String userId) async {
    final data = {
      'appId': appId,
      'productId': product.id,
      'userId': userId,
      'creatorId': product.userId
    };
    final uri = Uri.parse('$kHostUrl/constructor/saveWidgetForApp');

    final response =
        await _client.post(uri, body: json.encode(data), headers: headers);
    if (response.statusCode == 200) {
      return ServerMessage.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
    //  debugPrint('PRODUCT_REPOSITORY saveWidgetForApp responce ${responce.body}');
  }

  @override
  Future<AppModel> deleteWidgetFromApp(
      String userId, String appId, String productId) async {
    final uri = Uri.parse('$kHostUrl/constructor/deleteWidget');

    final data = {
      'userId': userId,
      'appId': appId,
      'productId': productId,
    };

    final response =
        await _client.post(uri, body: json.encode(data), headers: headers);

    if (response.statusCode == 200) {
      return AppModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> addRatingAndComment(
      String productId, String userId, int rank, String comment) async {
    final data = {
      'userId': userId,
      'rank': rank,
      'comment': comment,
    };
    final uri = Uri.parse('$kHostUrl/product/rateProduct/$productId');

    final response =
        await _client.post(uri, body: json.encode(data), headers: headers);

    //  debugPrint('PRODUCT_REPOSITORY addRatingAndComment responce'
    //' ${response.body}');
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<RatingItem>> getProductRatings(String productId) async {
    final uri = Uri.parse('$kHostUrl/product/getProductRatings/$productId');

    var response = await _client.get(uri);

    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((rating) => RatingItem.fromJson(rating))
          .cast<RatingItem>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<Product>> getProductsByUserId() async {
    final uri = Uri.parse('$kHostUrl/product/getProductsByUserId');

    var user = await getLocalUser();
    var response = await _client.post(uri,
        body: json.encode({'userId': user.id}), headers: headers);
    final products = json
        .decode(response.body)
        .map((e) => Product.fromJson(e))
        .cast<Product>()
        .toList();
    if (response.statusCode == 200) {
      return products;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<AppCategory>> getTopProductsByCategories() async {
    final uri = Uri.parse('$kHostUrl/product/getTopProducts');

    final response = await _client.get(
      uri,
      headers: headers,
    );

    if (response.statusCode == 200) {
      var list = <AppCategory>[];
      Map<String, dynamic> categories = jsonDecode(response.body);
      categories.forEach((key, value) {
        var apps = value
            .map((product) => Product.fromJson(product))
            .cast<Product>()
            .toList();
        const productCategoryType = ProductCategoryType.finance;
        final appCategory = AppCategory(
            productCategoryType.setType(key), apps); //! Проверить как работает
        list.add(appCategory);
      });
      return await compute(parseAppCategories, response.body);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<Product>> getTopProducts() async {
    final uri = Uri.parse('$kHostUrl/product/getTopTenProducts');

    final response = await _client.get(
      uri,
      headers: headers,
    );
    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((e) => Product.fromJson(e))
          .cast<Product>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Product> addProduct(PostProduct product) async {
    http.Response response;

    final uri = Uri.parse('$kHostUrl/product/addProduct');

    response = await _client.post(uri,
        body: json.encode(product.toJson()), headers: headers);

    if (response.statusCode == 200) {
      final product = Product.fromJson(json.decode(response.body));
      return product;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<void> changeProductStatusToDeleted(String productId) async {
    final body = json.encode({'productId': productId});
    try {
      final uri = Uri.parse('$kHostUrl/product/changeProductStatusToDeleted');

      await _client.put(uri, body: body, headers: headers);
    } on ServerException catch (e) {
      debugPrint('changeProductStatusToDeleted ERROR: $e');
    }
  }

  @override
  Future<List<Product>> getProductsByQuery(String query) async {
    final uri = Uri.parse('$kHostUrl/product/find?name=$query');

    var response = await _client.get(
      uri,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        //'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((e) => Product.fromJson(e))
          .cast<Product>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<Product>> getAllProducts() async {
    const page = 1;
    const limit = 500;
    final uri = Uri.parse('$kHostUrl/admin/widgets?page=$page&limit=$limit');

    var response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return result['items']
          .map((e) => Product.fromJson(e))
          .cast<Product>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<dynamic> addRatingAnswer(
      AnswerItem answerItem, String productId) async {
    final body = {
      'id': answerItem.ratingId,
      'answer': answerItem.answer,
    };
    final uri = Uri.parse('$kHostUrl/product/feedbackProduct/$productId');

    var response =
        await _client.post(uri, body: json.encode(body), headers: headers);
    if (response.statusCode == 200) {
      debugPrint('server response ${response.body}');
      return response.body;
    } else {
      throw ServerException();
    }
  }

  Future<UserModel> getLocalUser() async {
    final prefs = _sharedPreferences;
    if (prefs.containsKey(kUsersAuthKey)) {
      final userString = prefs.getString(kUsersAuthKey);
      final userMap = json.decode(userString!);
      return UserModel(
          id: userMap['_id'],
          name: userMap['name'],
          email: userMap['email'],
          photoUrl: userMap['photoUrl']);
    } else {
      return UserModel.empty;
    }
  }

  @override
  Future<dynamic> moderate(String productId, ModerateType moderateType) async {
    final body = {
      'id': productId,
      'decision': moderateType.name,
    };
    final uri = Uri.parse('$kHostUrl/admin/widgets/moderate');

    http.Response? response =
        await _client.post(uri, body: json.encode(body), headers: headers);
    debugPrint('server response ${response.body}');
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Product> getProductById(String productId) async {
    final uri = Uri.parse('$kHostUrl/product/getProductById/$productId');

    var response = await _client.get(
      uri,
      headers: headers,
    );
    if (response.statusCode == 200) {
      final product = Product.fromJson(json.decode(response.body));
      print(response.body);
      return product;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Product> editProductById(Product newProduct) async {
    http.Response response;

    final body = {
      'data': newProduct,
      'productId': newProduct.id,
    };
    final uri = Uri.parse('$kHostUrl/product/editProductById');

    response =
        await _client.put(uri, body: json.encode(body), headers: headers);

    if (response.statusCode == 200) {
      final product = Product.fromJson(json.decode(response.body));
      return product;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<Product>> searchProduct(String query) async {
    final uri = Uri.parse('$kHostUrl/admin/widgets/search?query=$query');

    final response = await _client.get(
      uri,
      headers: headers,
    );

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return result.map((e) => Product.fromJson(e)).cast<Product>().toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> getProductSource(String productId) async {
    final uri = Uri.parse('$kHostUrl/admin/widgets/$productId/source');

    final response = await _client.get(
      uri,
      headers: headers,
    );
    if (response.statusCode == 200) {
      final result = response.body;
      return result;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> getProductManifest(String productId) async {
    final uri = Uri.parse('$kHostUrl/admin/widgets/$productId/manifest');

    final response = await _client.get(
      uri,
      headers: headers,
    );
    if (response.statusCode == 200) {
      final result = response.body;
      return result;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> startTestProduct(
    String productId,
    Map<String, dynamic> settings,
  ) async {
    final body = json.encode(settings);
    final uri = Uri.parse('$kHostUrl/admin/widgets/$productId/launch');

    final response = await _client.post(
      uri,
      body: body,
      headers: headers,
    );
    if (response.statusCode == 200) {
      final result = response.body;
      return result;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<RunningAppModel>> getRunningProducts() async {
    const page = 1;
    const limit = 500;
    final uri = Uri.parse('$kHostUrl/admin/live?page=$page&limit=$limit');

    final response = await _client.get(
      uri,
      headers: headers,
    );
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return result['apps']
          .map((e) => RunningAppModel.fromJson(e))
          .cast<RunningAppModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<void> uploadFile(
      PlatformFile file, String field, String userId) async {
    try {
      final url = '$kHostUrl/product/upload/$userId';
      if (!kIsWeb) {
        final dio = Dio();
        final formData = FormData.fromMap({
          'file': await MultipartFile.fromFile(file.path!),
          'fileName': file.name
        });
        await dio.post(
          url,
          data: formData,
          onSendProgress: (int sent, int total) {
            debugPrint('${file.size}');
            debugPrint('$sent $total');
          },
        );
      } else {
        var request = http.MultipartRequest("POST", Uri.parse(url));
        final fil = http.MultipartFile.fromBytes('file', file.bytes!,
            filename: file.name);
        request.files.add(fil);
        request.send().then((response) {
          print(response.statusCode);
          if (response.statusCode == 200) print("Uploaded!");
        });
      }
    } catch (e) {
      print(e);
    }
  }
}

Future<List<AppCategory>> parseAppCategories(String body) async {
  var list = <AppCategory>[];
/*   Map<String, dynamic> categories = jsonDecode(body);

  categories.forEach((key, value) {
    final json = {key: value};
    debugPrint('key: $key');
    var appCat = AppC  ategory.fromJson(json);
    _list.add(appCat);
  }); */

  Map<String, dynamic> categories = jsonDecode(body);
  categories.forEach((key, value) {
    var apps = value
        .map((product) => Product.fromJson(product))
        .cast<Product>()
        .toList();
    var productCategoryType = ProductCategoryType.other;
    final appCategory = AppCategory(productCategoryType.setType(key), apps);
    list.add(appCategory);
  });

  return list;
}
