import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:orchestra/models/product/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import '../core/const.dart';
import '../core/exceptions.dart';
import '../models/app.dart';
import '../models/app_action_request.dart';
import '../models/enums/app_action_type.dart';
import '../models/results_model.dart';
import '../models/settings_model.dart';

abstract class AppRepository {
  Future<List<AppModel>> getAppsByUserId();
  Future<AppModel> saveApp(AppModel app);
  Future<AppModel> updateApp(AppModel app);
  Future<dynamic> startApp(String appId);
  Future<dynamic> pauseApp(String appId);
  Future<AppModel> deleteApp(AppModel app);
  Future<List<Product>> getWidgetsByAppId(String appId);
  Future<List<SettingsModel>> getWidgetSettings(String productId);
  Future<String> getAppWidgetSettings(String appId);
  Future<String> saveAppWidgetSettings(
    String appId,
    String productId,
    Map<String, dynamic> settings,
  );
  Future<AppModel> saveAppSettings(AppModel app);

  Future<List<ResultsModel>> getAppResults(int page);

  Future<AppModel> getAppById(String appId);

  Future<List<ResultsModel>> getUpdates(String timestamp);
}

//============================================================

class AppRepositoryImpl implements AppRepository {
  final http.Client _client;
  final SharedPreferences _sharedPreferences;

  AppRepositoryImpl({
    required http.Client client,
    required SharedPreferences sharedPreferences,
  })  : _client = client,
        _sharedPreferences = sharedPreferences,
        super();
  String? get token => _sharedPreferences.getString('token');

  Map<String, String> get headers => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

  @override
  Future<List<AppModel>> getAppsByUserId() async {
    final _uri = Uri.parse('${UrlConstants.kGetAppByUserIdUrl}/user');
    final response = await _client.get(_uri, headers: headers);
//    debugPrint('APP_REPOSITORY: get request userId $userId');
//    debugPrint('APP_REPOSITORY: get request response ${response.body}');
    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((app) => AppModel.fromJson(app))
          .cast<AppModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AppModel> saveApp(AppModel app) async {
    final body = json.encode(app.toJson());
    final _uri = Uri.parse(UrlConstants.kSaveAppUrl);

    final response = await _client.post(_uri, body: body, headers: headers);
    // debugPrint('APP_REPOSITORY: responce ${response.body}');

    if (response.statusCode == 200) {
      return AppModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AppModel> saveAppSettings(AppModel app) async {
    final body = json.encode({
      'appId': app.id,
      'loop': app.loop,
      'name': app.name,
      'notification': app.notification,
      'telegram': app.telegram,
      'email': app.email,
    });
    final _uri = Uri.parse('$kHostUrl/constructor/saveAppSettings/');
    final response = await _client.post(_uri, body: body, headers: headers);

    if (response.statusCode == 200) {
      return AppModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<dynamic> startApp(String appId) async {
    final body = json.encode(
        AppActionRequest(type: AppActionType.startApp.name, appId: appId)
            .toMap());
    final _uri = Uri.parse(UrlConstants.kStartAppUrl);

    final response = await _client.post(_uri, body: body, headers: headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<dynamic> pauseApp(String appId) async {
    final body = json.encode(
        AppActionRequest(type: AppActionType.stopApp.name, appId: appId)
            .toMap());
    final _uri = Uri.parse(UrlConstants.kPauseAppUrl);

    final response = await _client.post(_uri, body: body, headers: headers);
    if (response.statusCode == 200) {
      return response;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AppModel> deleteApp(AppModel app) async {
    final _uri = Uri.parse('${UrlConstants.kDeleteAppUrl}${app.id}');
    await _client.delete(_uri, headers: headers);

    return app;
  }

  @override
  Future<List<Product>> getWidgetsByAppId(String appId) async {
    final _uri = Uri.parse('$kHostUrl/constructor/getWidgetByAppId/$appId');
    final response = await _client.get(_uri, headers: headers);

    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((prod) => Product.fromJson(prod['productId']))
          .cast<Product>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<SettingsModel>> getWidgetSettings(
    String productId,
  ) async {
    var result = <SettingsModel>[];
    final _uri = Uri.parse('$kHostUrl/product/getProductManifest/$productId');

    final response = await _client.get(_uri, headers: headers);

    if (response.statusCode == 200) {
      final Map<String, dynamic> res = json.decode(response.body);

      List forms = res.entries.first.value;

      for (var e in forms) {
        final _set = Map.from(e['form_data']);
        _set.forEach((key, value) {
          final _setting = SettingsModel.fromJson(value);

          final _settingUpd = _setting.copyWith(key: key);

          result.add(_settingUpd);
        });
      }
      return result;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> saveAppWidgetSettings(
    String appId,
    String productId,
    Map<String, dynamic> settings,
  ) async {
    final data = {
      'appId': appId,
      'productId': productId,
      'settings': settings,
    };
    debugPrint('settings->');
    debugPrint(settings.toString());
    final _uri = Uri.parse('$kHostUrl/constructor/saveWidgetSettings');
    final response =
        await _client.post(_uri, body: json.encode(data), headers: headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> getAppWidgetSettings(
    String appId,
  ) async {
    final _uri = Uri.parse('$kHostUrl/constructor/getWidgetSettings/$appId');
    final response = await _client.get(_uri, headers: headers);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<ResultsModel>> getAppResults(int page) async {
    const limit = 15;
    final _uri = Uri.parse(
        '$kHostUrl/constructor/getAppResults?page=$page&limit=$limit');
    final response = await _client.get(_uri, headers: headers);
    if (response.statusCode == 200) {
      return compute(parseAppResults, response.body);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AppModel> getAppById(String appId) async {
    final _uri = Uri.parse('$kHostUrl/constructor/getAppById/$appId');
    final response = await _client.get(_uri, headers: headers);

    if (response.statusCode == 200) {
      return AppModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<ResultsModel>> getUpdates(String timestamp) async {
    final _uri = Uri.parse('$kHostUrl/users/getUpdates/$timestamp');
    final response = await _client.get(_uri, headers: headers);
    if (response.statusCode == 200) {
      final parsed = json.decode(response.body);
      final lastResult = parsed['appResults'];
      return compute(parseAppResults, json.encode(lastResult));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<AppModel> updateApp(AppModel app) async {
    final body = json.encode(app.toJson());
    final _uri = Uri.parse(UrlConstants.kUpdateAppUrl);

    final response = await _client.put(_uri, body: body, headers: headers);
    if (response.statusCode == 200) {
      return AppModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}

Future<List<ResultsModel>> parseAppResults(String responseBody) async {
  final parsed = json.decode(responseBody);
  return await parsed
      .map((result) => ResultsModel.fromJson(result))
      .cast<ResultsModel>()
      .toList();
}
