import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:orchestra/models/launch_stats.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';
import '../core/exceptions.dart';
import '../models/customer_activity.dart';
import '../models/daily_sale.dart';
import '../models/user.dart';
import 'auth_repository.dart';

abstract class UserRepository {
  Future<UserModel> saveUserAsFacebookUser(UserModel user);
  Future<UserModel> saveUserAsGoogleUser(UserModel user);
  Future<UserModel> updateUser(UserModel user);
  Future<UserModel> getUser();
  Future<String> addPushToken(String token);
  Future<Map<String, String>> becomeContributor();
  Future<List<UserModel>> fetchAllUsers();
  Future<UserModel> adminUserUpdate(UserModel user);
  Future<Map<String, String>> addReferral();
  Future<List<UserModel>> searchUsers({String query});
  Future<String> generateApiKey();
  Future<List<DailySales>> getDailySales();
  Future<LaunchStats> getLaunchStats();
  Future<CustomerActivity> getCustomerActivity();
}

//============================================================================

class UserRepositoryImpl implements UserRepository {
  static const kSaveFacebookUserUrl = '$kHostUrl/users/auth/facebook';
  static const kSaveGoogleUserUrl = '$kHostUrl/users/auth/google';
  final http.Client _client;
  final SharedPreferences _sharedPreferences;
  final AuthRepository _authRepository;

  UserRepositoryImpl({
    required http.Client client,
    required SharedPreferences sharedPreferences,
    required AuthRepository authRepository,
  })  : _client = client,
        _sharedPreferences = sharedPreferences,
        _authRepository = authRepository,
        super();

  String? get token => _sharedPreferences.getString('token');
  Map<String, String> get headers => {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      };

  @override
  Future<UserModel> saveUserAsFacebookUser(UserModel user) async {
    final body = {
      'id': user.id,
      'name': user.name,
      'email': user.email,
      'photoUrl': user.photoUrl,
    };
    final uri = Uri.parse(kSaveFacebookUserUrl);

    final response = await _client.post(uri, body: body);
    if (response.statusCode == 200) {
      //bgFirebaseAnalytics.instance.logSignUp(signUpMethod: 'facebook');
      return _decodeUser(response);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserModel> saveUserAsGoogleUser(UserModel user) async {
    final body = {
      'id': user.id,
      'name': user.name,
      'email': user.email,
      'photoUrl': user.photoUrl,
    };
    final uri = Uri.parse(kSaveGoogleUserUrl);

    final response = await _client.post(uri, body: body);
    if (response.statusCode == 200) {
      // FirebaseAnalytics.instance.logSignUp(signUpMethod: 'google');
      return _decodeUser(response);
    } else {
      throw ServerException();
    }
  }

  UserModel _decodeUser(http.Response response) {
    var body = json.decode(response.body);
    var token = body['token'];
    //debugPrint('USER_REPOSITORY: token $token');
    _sharedPreferences.setString('token', token);
    final user = UserModel.fromJson(body['user']);
    return user;
  }

  @override
  Future<UserModel> updateUser(UserModel user) async {
    final uri = Uri.parse('$kHostUrl/users/editUserData');

    final response =
        await _client.put(uri, body: json.encode(user), headers: headers);
    //debugPrint('USER_REPO update user response ${response.body}');

    if (response.statusCode == 200) {
      final user = json.decode(response.body);
      return UserModel.fromJson(user['user']);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserModel> getUser() async {
    final user = await _authRepository.getLocalUser();
    final userId = user.id;
    final uri = Uri.parse('$kHostUrl/users/me/$userId');

    final response = await _client.get(uri, headers: headers);
    //debugPrint('USER_REPO get user info response ${response.body}');
    if (response.statusCode == 200) {
      final body = json.decode(response.body);
      return UserModel.fromJson(body['user']);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> addPushToken(String pushToken) async {
    final body = json.encode({'token': pushToken});
    final uri = Uri.parse('$kHostUrl/users/addPushToken');

    final response = await _client.post(uri, body: body, headers: headers);

    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Map<String, String>> becomeContributor() async {
    final uri = Uri.parse('$kHostUrl/users/become-contributor');

    final response = await _client.get(uri, headers: headers);

    if (response.statusCode == 200) {
      return Map<String, String>.from(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<UserModel>> fetchAllUsers() async {
    const page = 1;
    const limit = 500;
    final uri = Uri.parse('$kHostUrl/admin/users?page=$page&limit=$limit');

    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      final Map<String, dynamic> result = json.decode(response.body);

      return result['items']
          .map((user) => UserModel.fromJson(user))
          .cast<UserModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<UserModel> adminUserUpdate(UserModel user) async {
    final uri = Uri.parse('$kHostUrl/admin/users/save');

    final response =
        await _client.post(uri, body: json.encode(user), headers: headers);

    if (response.statusCode == 200) {
      final user = json.decode(response.body);
      return UserModel.fromJson(user['user']);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<Map<String, String>> addReferral() async {
    final code = _sharedPreferences.getString('refCode');

    if (code == null) return {};
    final body = {'refCode': code};
    final uri = Uri.parse('$kHostUrl/users/addReferral');

    final response =
        await _client.post(uri, body: json.encode(body), headers: headers);
    if (response.statusCode == 200) {
      return Map<String, String>.from(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<UserModel>> searchUsers({String? query}) async {
    final uri = Uri.parse('$kHostUrl/admin/users/search?query=$query');

    final response = await _client.get(uri);

    if (response.statusCode == 200) {
      return json
          .decode(response.body)
          .map((user) => UserModel.fromJson(user))
          .cast<UserModel>()
          .toList();
    } else {
      throw ServerException();
    }
  }

  @override
  Future<String> generateApiKey() async {
    final uri = Uri.parse('$kHostUrl/users/generateApiKey');

    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      return result['apiKey'];
    } else {
      throw ServerException();
    }
  }

  @override
  Future<List<DailySales>> getDailySales() async {
    final uri = Uri.parse('$kHostUrl/users/dailySales');

    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      final result = json.decode(response.body) as List<dynamic>;
      final dailySales = result.map((e) => DailySales.fromJson(e)).toList();
      return dailySales;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<LaunchStats> getLaunchStats() async {
    final uri = Uri.parse('$kHostUrl/users/launchStats');

    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      final launchStats = LaunchStats.fromJson(result);
      return launchStats;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<CustomerActivity> getCustomerActivity() async {
    final uri = Uri.parse('$kHostUrl/users/customerActivity');
    final response = await _client.get(uri, headers: headers);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      final customerActivity = CustomerActivity.fromJson(result);
      return customerActivity;
    } else {
      throw ServerException();
    }
  }
}
