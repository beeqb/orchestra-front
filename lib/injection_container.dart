import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';
import 'package:orchestra/blocs/bloc/product_code_bloc.dart';
import 'package:orchestra/blocs/bloc/running_apps_bloc.dart';
import 'package:orchestra/blocs/bloc/top_ten_products_bloc.dart';
import 'package:orchestra/blocs/cubits/profile_menu_cubit.dart';
import 'package:orchestra/blocs/cubits/stats_cubit.dart';
import 'package:orchestra/safe_area_color.dart';
import 'package:orchestra/services/admin_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'blocs/bloc/added_products_bloc.dart';
import 'blocs/bloc/admin_products_bloc.dart';
import 'blocs/bloc/admin_transactions_bloc.dart';
import 'blocs/bloc/all_users_bloc.dart';
import 'blocs/bloc/auth_bloc.dart';
import 'blocs/bloc/device_info_bloc.dart';
import 'blocs/bloc/navigation_bloc.dart';
import 'blocs/bloc/onboarding_nav_bloc.dart';
import 'blocs/bloc/purchased_bloc.dart';
import 'blocs/bloc/results_bloc.dart';
import 'blocs/bloc/root_bloc.dart';
import 'blocs/bloc/search_product_bloc.dart';
import 'blocs/bloc/sliding_page_bloc.dart';
import 'blocs/bloc/top_products_bloc.dart';
import 'blocs/cubits/checkout_cubit.dart';
import 'blocs/cubits/navigation_menu_cubit.dart';
import 'blocs/cubits/products_cubit.dart';
import 'blocs/cubits/transfer_user_cubit.dart';
import 'blocs/cubits/user_cubit.dart';
import 'repository/app_repository.dart';
import 'repository/auth_repository.dart';
import 'repository/billing_repository.dart';
import 'repository/product_repository.dart';
import 'repository/user_repository.dart';
import 'services/app_service.dart';
import 'services/billing_service.dart';
import 'services/connection_service.dart';
import 'services/dialogs_service.dart';
import 'services/fcm_service.dart';
import 'services/product_service.dart';
import 'services/socket_service.dart';
import 'services/timers_injections.dart';
import 'services/user_service.dart';

final sl = GetIt.instance;

Future<void> init() async {
  sl.registerLazySingleton(() => SafeAreaColor());

  // Blocs

  sl.registerLazySingleton<StatsCubit>(() => StatsCubit(userService: sl()));

  sl.registerLazySingleton(() => ProfileMenuCubit());

  sl.registerLazySingleton(() => NavigationMenuCubit(navBloc: sl()));

  sl.registerLazySingleton(() => AuthBloc(
        socketService: sl(),
        userService: sl(),
      ));
  sl.registerLazySingleton(() => RootBloc(
        authBloc: sl(),
        appService: sl(),
        productService: sl(),
        timersInjections: sl(),
        topProductsBloc: sl(),
        addedProductsBloc: sl(),
      ));
  sl.registerLazySingleton(() => ResultsBloc(appService: sl()));
  sl.registerLazySingleton(() => AddedProductsBloc(productService: sl()));
  sl.registerLazySingleton(
      () => ProductsCubit(authBloc: sl(), productService: sl()));
  sl.registerLazySingleton<PurchasedBloc>(() => PurchasedBloc(
        authBloc: sl(),
        productService: sl(),
      ));
  sl.registerLazySingleton<UserCubit>(() => UserCubit(
        userService: sl(),
        authBloc: sl(),
      ));
  sl.registerLazySingleton<TopProductsBlocByCategories>(
      () => TopProductsBlocByCategories(productService: sl()));
  sl.registerLazySingleton<SearchProductBloc>(
      () => SearchProductBloc(productService: sl()));
  sl.registerLazySingleton<CheckoutCubit>(() => CheckoutCubit(
        billingService: sl(),
        dialogsService: sl(),
      ));
  sl.registerLazySingleton<SlidingPageBLoC>(() => SlidingPageBLoC());
  sl.registerLazySingleton<TransferUserCubit>(() => TransferUserCubit(
        userCubit: sl(),
        billingService: sl(),
        dialogsService: sl(),
      ));
  sl.registerLazySingleton<AllUsersBloc>(() => AllUsersBloc(userService: sl()));
  sl.registerLazySingleton<AdminProductsBloc>(() => AdminProductsBloc(sl()));
  sl.registerLazySingleton<AdminTransactionsBloc>(
      () => AdminTransactionsBloc(adminService: sl()));
  sl.registerLazySingleton<TopTenProductsBLoC>(() => TopTenProductsBLoC(sl()));
  sl.registerLazySingleton<NavigationBLoC>(() => NavigationBLoC());
  sl.registerSingleton<OnboardingNavBLoC>(OnboardingNavBLoC());
  sl.registerLazySingleton<ProductCodeBLoC>(() => ProductCodeBLoC());
  sl.registerLazySingleton<RunningAppsBloc>(
      () => RunningAppsBloc(adminService: sl()));
  sl.registerLazySingleton<DeviceInfoBloc>(() => DeviceInfoBloc());

  // Reposytories
  sl.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(sharedPreferences: sl()));
  sl.registerLazySingleton<AppRepository>(() => AppRepositoryImpl(
        client: sl(),
        sharedPreferences: sl(),
      ));
  sl.registerLazySingleton<UserRepository>(() => UserRepositoryImpl(
        client: sl(),
        authRepository: sl(),
        sharedPreferences: sl(),
      ));
  sl.registerLazySingleton<BillingRepository>(() => BillingRepositoryImpl(
        client: sl(),
        sharedPreferences: sl(),
      ));
  sl.registerLazySingleton<ProductRepository>(() => ProductRepositoryImpl(
        client: sl(),
        sharedPreferences: sl(),
      ));
  sl.registerLazySingleton<LogScreenBloc>(() => LogScreenBloc());

  //Services
  sl.registerLazySingleton<UserService>(() => UserService(
        authRepository: sl(),
        userRepository: sl(),
      ));
  sl.registerLazySingleton<ProductService>(
      () => ProductService(productRepository: sl()));
  sl.registerLazySingleton<AppService>(() => AppService(
        appRepository: sl(),
      ));
  sl.registerLazySingleton<BillingService>(
      () => BillingServiceImpl(billingRepository: sl()));
  sl.registerLazySingleton<TimersInjections>(() => TimersInjections());
  sl.registerLazySingleton<DialogsService>(() => DialogsService());
  sl.registerLazySingleton<FcmService>(() => FcmService(
        resultsCubit: sl(),
        dialogsService: sl(),
        userService: sl(),
      ));
  sl.registerLazySingleton<AdminService>(() => AdminService(
        billingRepository: sl(),
        productRepository: sl(),
      ));

  // Outside
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton<SocketService>(() => SocketService(
        resultsCubit: sl(),
        userService: sl(),
        dialogsService: sl(),
      ));
  sl.registerLazySingleton<ConnectionService>(
      () => ConnectionService(dialogsService: sl()));
}
