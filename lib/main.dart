import 'dart:io';

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:orchestra/blocs/bloc/device_info_bloc.dart';
import 'package:orchestra/blocs/bloc/sliding_page_bloc.dart';
import 'package:orchestra/blocs/cubits/profile_menu_cubit.dart';
import 'package:orchestra/blocs/cubits/stats_cubit.dart';
import 'package:orchestra/models/user.dart';
import 'package:orchestra/safe_area_color.dart';
import 'package:orchestra/widgets/loading.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wiredash/wiredash.dart';

import 'blocs/bloc/added_products_bloc.dart';
import 'blocs/bloc/auth_bloc.dart';
import 'blocs/bloc/main_bloc_observer.dart';
import 'blocs/bloc/navigation_bloc.dart';
import 'blocs/bloc/onboarding_nav_bloc.dart';
import 'blocs/bloc/purchased_bloc.dart';
import 'blocs/bloc/results_bloc.dart';
import 'blocs/bloc/root_bloc.dart';
import 'blocs/bloc/search_product_bloc.dart';
import 'blocs/bloc/top_products_bloc.dart';
import 'blocs/cubits/checkout_cubit.dart';
import 'blocs/cubits/navigation_menu_cubit.dart';
import 'blocs/cubits/products_cubit.dart';
import 'blocs/cubits/transfer_user_cubit.dart';
import 'blocs/cubits/user_cubit.dart';
import 'blocs/cubits/widgets_cart_cubit.dart';
import 'config.dart';
import 'injection_container.dart' as di;
import 'injection_container.dart';
import 'routing/nav2/router_delegate.dart';
import 'routing/nav2/router_information_parser.dart';
import 'services/connection_service.dart';
import 'services/fcm_service.dart';
import 'theme.dart';
import 'util/constants.dart';
import 'util/http_override.dart';

final wiredashKey = GlobalKey<NavigatorState>();

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: kIsWeb
        ? HydratedStorage.webStorageDirectory
        : await getTemporaryDirectory(),
  );
  await di.init();
  if (!kIsWeb) {
    await Firebase.initializeApp();

    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  } else {
    if (!kDebugMode) {
      await FacebookAuth.i.webAndDesktopInitialize(
        appId: '1032592780716935', //<-- YOUR APP_ID
        cookie: true,
        xfbml: true,
        version: 'v15.0',
      );
      print(
          'FacebookAuth.i.isWebSdkInitialized ${FacebookAuth.i.isWebSdkInitialized}');
    }
  }

  print('fb init');

  sl<ConnectionService>().init();

  sl<FcmService>().init();

  Bloc.observer = MainBlocObserver();

  if (!kIsWeb) {
    FlutterAppBadger.removeBadge();
  }
  sl<SharedPreferences>().setInt('countBages', 0);

  runApp(ChangeNotifierProvider(
    create: (context) => sl<SafeAreaColor>(),
    child: MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => di.sl<AuthBloc>()..add(StartLocalAuthEvent()),
        ),
        BlocProvider<RootBloc>(
          create: (context) => di.sl<RootBloc>(),
        ),
        BlocProvider<ProductsCubit>(
          create: (context) => di.sl<ProductsCubit>()..getProducts(),
        ),
        BlocProvider<WidgetsCartCubit>(
          create: (context) => WidgetsCartCubit(),
        ),
        BlocProvider<UserCubit>(
          create: (context) => di.sl<UserCubit>(),
        ),
        BlocProvider<PurchasedBloc>(
          create: (context) =>
              di.sl<PurchasedBloc>()..add(LoadPurchasedProductsEvent()),
        ),
        BlocProvider<TransferUserCubit>(
          create: (context) => sl<TransferUserCubit>(),
        ),
        BlocProvider<CheckoutCubit>(
          create: (context) => sl<CheckoutCubit>(),
        ),
        BlocProvider<ResultsBloc>(
          create: (context) => di.sl<ResultsBloc>(),
        ),
        BlocProvider<AddedProductsBloc>(
          create: (context) => di.sl<AddedProductsBloc>(),
        ),
        BlocProvider<TopProductsBlocByCategories>(
          create: (context) => di.sl<TopProductsBlocByCategories>(),
        ),
        BlocProvider<SearchProductBloc>(
          create: (context) => di.sl<SearchProductBloc>(),
        ),
        BlocProvider<OnboardingNavBLoC>(
          create: (context) => di.sl<OnboardingNavBLoC>(),
        ),
        BlocProvider<SlidingPageBLoC>(
          create: (context) => di.sl<SlidingPageBLoC>(),
        ),
        BlocProvider<DeviceInfoBloc>(
          create: (context) =>
              di.sl<DeviceInfoBloc>()..add(const DeviceInfoEvent.read()),
        ),
        BlocProvider<ProfileMenuCubit>(
            create: (context) => di.sl<ProfileMenuCubit>()),
        BlocProvider<StatsCubit>(create: (context) => di.sl<StatsCubit>()),
      ],
      child: const MyApp(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  static Brightness get brightness =>
      SchedulerBinding.instance.window.platformBrightness;

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('theme mode $brightness');
    return Wiredash(
      projectId: kWiredashId,
      secret: kWiredashSecret,
      child: AdaptiveTheme(
        initial: AdaptiveThemeMode.system,
        light: kLightTheme,
        dark: kDarkTheme,
        builder: (light, dark) {
          sl<SafeAreaColor>()
              .setThemeData(brightness == Brightness.light ? light : dark);
          return BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state) {
              if ((state is LoggedAuthState) ||
                  (state is NonLoggedAuthState) ||
                  state is AuthStateFailure) {
                return NestedRouter(
                  themeData: brightness == Brightness.light ? light : dark,
                  currentUser: state.currentUser,
                );
              }
              return const LoadingInitial();
            },
          );
        },
      ),
    );
  }
}

class NestedRouter extends StatefulWidget {
  const NestedRouter({
    Key? key,
    required this.themeData,
    required this.currentUser,
  }) : super(key: key);
  final ThemeData themeData;
  final UserModel currentUser;

  @override
  _NestedRouterState createState() => _NestedRouterState();
}

class _NestedRouterState extends State<NestedRouter> {
  final _routerDelegate = OrchestraRouterDelegate();
  late OrchestraRouteInformationParser _routeInformationParser;

  @override
  void initState() {
    super.initState();
    _routeInformationParser =
        OrchestraRouteInformationParser(widget.currentUser);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<NavigationBLoC>(
          create: (context) => di.sl<NavigationBLoC>(),
        ),
        BlocProvider<NavigationMenuCubit>(
          create: (context) => di.sl<NavigationMenuCubit>(),
        ),
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: kAppTitle,
        theme: widget.themeData,
        routerDelegate: _routerDelegate,
        routeInformationParser: _routeInformationParser,
      ),
    );
  }
}
