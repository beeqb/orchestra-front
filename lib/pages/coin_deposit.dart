import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../blocs/cubits/checkout_cubit.dart';
import '../util/constants.dart';
import '../widgets/custom_app_bar.dart';
import '../widgets/loading.dart';

void redirectToCoinCheckout(BuildContext context) async {
  sl<NavigationBLoC>().add(const ToCoinDepositNavigationEvent());
}

class CoinDepositPage extends StatefulWidget {
  const CoinDepositPage({Key? key}) : super(key: key);

  @override
  _CoinDepositPageState createState() => _CoinDepositPageState();
}

class _CoinDepositPageState extends State<CoinDepositPage> {
  // ignore: unused_field
  late WebViewController _controller;
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(title: 'Deposit'),
      body: SafeArea(
        child: BlocBuilder<CheckoutCubit, CheckoutState>(
          builder: (context, state) {
            if (state is CheckoutLoaded) {
              return Stack(
                children: [
                  WebView(
                    initialUrl: state.session,
                    javascriptMode: JavascriptMode.unrestricted,
                    onWebViewCreated: (controller) => _controller = controller,
                    navigationDelegate: (request) {
                      if (request.url.startsWith(kDepositSuccessPageUrl)) {
                        sl<NavigationBLoC>()
                            .add(const ToDepositSuccessNavigationEvent());
// <-- Handle success case
                      } else if (request.url
                          .startsWith(kDepositFailedPageUrl)) {
                        sl<NavigationBLoC>()
                            .add(const ToDepositFailedNavigationEvent());
                        // <-- Handle cancel case
                      }
                      return NavigationDecision.navigate;
                    },
                    onPageFinished: (_) {
                      setState(() {
                        _isLoading = false;
                      });
                    },
                  ),
                  _isLoading
                      ? const Center(
                          child: Loading(),
                        )
                      : Stack(),
                ],
              );
            }
            return const Center(
              child: Loading(),
            );
          },
        ),
      ),
    );
  }
}
