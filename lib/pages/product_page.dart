import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/widgets/product_page/product_images.dart';
import 'package:orchestra/widgets/product_page/product_summary.dart';
import 'package:orchestra/widgets/product_page/ratings_and_reviews.dart';
import 'package:orchestra/widgets/product_page/recommended_products.dart';

import '../blocs/bloc/product_details_bloc.dart';
import '../blocs/bloc/ratings_bloc.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../models/product/product.dart';
import '../services/product_service.dart';
import '../widgets/loading.dart';
import '../widgets/product_page/product_main_info.dart';

class ProductPage extends StatelessWidget {
  final String productId;

  const ProductPage({
    Key? key,
    required this.productId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return BlocProvider(
      create: (context) =>
          ProductDetailsBloc(productService: sl<ProductService>())
            ..add(LoadProductDetailsEvent(productId)),
      child: BlocBuilder<ProductDetailsBloc, ProductDetailsState>(
        builder: (context, state) {
          if (state is ProductDetailsLoaded) {
            return Scaffold(
              body: BlocProvider<RatingsBloc>(
                create: (context) => RatingsBloc(
                    productService: sl<ProductService>(),
                    product: state.product)
                  ..add(StartRatingsLoadingEvent()),
                child: isMobile
                    ? MobileProductPage(
                        product: state.product,
                      )
                    : DesktopProductPage(
                        product: state
                            .product), //_buildScrollable(context, product),
              ),
            );
          }
          return const Loading();
        },
      ),
    );
  }
}

class DesktopProductPage extends StatelessWidget {
  final Product product;

  const DesktopProductPage({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 55),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              !isMobile
                  ? SizedBox(
                      height: 96,
                      child: IconButton(
                          onPressed: () => Navigator.of(context).pop(context),
                          splashRadius: 24,
                          padding: EdgeInsets.zero,
                          icon: const Icon(
                            Icons.arrow_back_ios_sharp,
                            size: 24,
                          )),
                    )
                  : SizedBox.shrink(),
              DesktopProductMainInfo(product: product),
              const SizedBox(
                height: 80,
              ),
              ProductImages(product: product),
              const SizedBox(
                height: 80,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    flex: 6,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'How it works',
                              style: Theme.of(context).textTheme.headlineLarge,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              product.howItWork,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            const SizedBox(
                              height: 60,
                            ),
                            DesktopRatingsAndReviews(product: product),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 50,
                  ),
                  Flexible(
                      flex: 2, child: DesktopProductSummary(product: product)),
                ],
              ),
              const SizedBox(
                height: 77,
              ),
              Text(
                'Recommended for you',
                style: Theme.of(context).textTheme.headlineLarge,
              ),
              const SizedBox(
                height: 26,
              ),
              const SizedBox(height: 120, child: RecommendedProducts())
            ],
          )),
    );
  }
}

class MobileProductPage extends StatelessWidget {
  final Product product;

  const MobileProductPage({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MobileProductMainInfo(product: product),
              const SizedBox(
                height: 32,
              ),
              ProductImages(product: product),
              // MobileAboutProduct(
              //     productDescription: product.productDescription),
              Text(
                'How it works',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 16,
              ),
              Text(product.howItWork,
                  style: Theme.of(context).textTheme.bodyMedium),
              const SizedBox(
                height: 40,
              ),
              MobileRatingsAndReviews(product: product),
              const SizedBox(
                height: 40,
              ),
              Text(
                'Information',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 12,
              ),
              MobileProductSummary(product: product),
              const SizedBox(
                height: 40,
              ),
              Text(
                'Recommended for you',
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 20,
              ),
              const MobileRecommendedProducts()
            ],
          )),
    );
  }
}

class MobileAboutProduct extends StatefulWidget {
  final String productDescription;

  const MobileAboutProduct({Key? key, required this.productDescription})
      : super(key: key);

  @override
  State<MobileAboutProduct> createState() => _MobileAboutProductState();
}

class _MobileAboutProductState extends State<MobileAboutProduct> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'About',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            TextButton(
                onPressed: () {
                  setState(() {
                    isExpanded = true;
                  });
                },
                child: Text(
                  'See all',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall
                      ?.copyWith(color: const Color(0xFF22AFFF)),
                ))
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        isExpanded
            ? Text(
                widget.productDescription,
                style: Theme.of(context).textTheme.bodyMedium,
              )
            : Text(
                widget.productDescription,
                style: Theme.of(context).textTheme.bodyMedium,
                overflow: TextOverflow.ellipsis,
                maxLines: 5,
              ),
        const SizedBox(
          height: 40,
        ),
      ],
    );
  }
}
