import 'dart:async';

import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/injection_container.dart';

class DepositFailedPage extends StatefulWidget {
  const DepositFailedPage({Key? key}) : super(key: key);

  @override
  _DepositFailedPageState createState() => _DepositFailedPageState();
}

class _DepositFailedPageState extends State<DepositFailedPage> {
  @override
  void initState() {
    startTime();
    super.initState();
  }

  Future<Timer> startTime() async {
    var _duration = const Duration(seconds: 4);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() =>
      sl<NavigationBLoC>().add(const ToBillingsNavigationEvent());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: navigationPage,
                child: const SizedBox(
                  height: 70,
                  width: 70,
                  child: Icon(
                    Icons.error,
                    color: Colors.red,
                    size: 50,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Text(
                  'Deposit failed!',
                  style: Theme.of(context).textTheme.headline6,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
