import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/cubits/navigation_menu_cubit.dart';
import 'package:orchestra/routing/dialogs_manager.dart';
import 'package:orchestra/safe_area_color.dart';
import 'package:orchestra/services/dialogs_service.dart';
import 'package:orchestra/widgets/appbar/mobile_appbar.dart';

import '../core/const.dart';
import '../injection_container.dart';
import '../routing/nav2/nested_route_delegate.dart';
import '../widgets/appbar/desktop_appbar.dart';
import '../widgets/navigation/bottom_navbar/bottom_navbar.dart';
import '../widgets/navigation/collapsible_sidebar/collapsible_sidebar.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  get _isSidebarCollapsed =>
      MediaQuery.of(context).size.width <= SidebarConstants.collapseBreakpoint;
  get _isMobile =>
      MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var bgColor = context.watch<SafeAreaColor>();
    return Scaffold(
        body: SafeArea(
      child: BlocListener<AuthBloc, AuthState>(listener: (context, state) {
        if (state is LoggedAuthState) {
          sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
        } else if (state is NonLoggedAuthState) {
          if (sl<NavigationBLoC>().state is! MarketplaceNavigationState) {
            sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
          }
        }
      }, child: BlocBuilder<NavigationMenuCubit, int>(
        builder: (context, state) {
          return CollapsibleSidebar(
            isCollapsed: _isSidebarCollapsed,
            isHidden: _isMobile,
            body: SelectionArea(
              child: Scaffold(
                drawer: _isMobile ? const MobileDrawer() : null,
                appBar: PreferredSize(
                    preferredSize: Size.fromHeight(_isMobile
                        ? AppBarSizeCostants.mobileAppbarHeight
                        : AppBarSizeCostants.desktopAppbarHeight),
                    child: _isMobile
                        ? MobileAppbar(navigatorKey: navigatorKey)
                        : const DesktopAppbar()),
                backgroundColor: bgColor.backgroundColor,
                primary: true,
                body: SubRouter(
                  navigatorKey: navigatorKey,
                ),
                bottomNavigationBar: _isMobile ? const BottomNavbar() : null,
              ),
            ),
          );
        },
      )),
    ));
  }
}

class SubRouter extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  const SubRouter({Key? key, required this.navigatorKey}) : super(key: key);
  @override
  _SubRouterState createState() => _SubRouterState();
}

class _SubRouterState extends State<SubRouter> {
  NestedRouterDelegate? _nestedRouterDelegate;
  ChildBackButtonDispatcher? _backButtonDispatcher;

  @override
  void initState() {
    super.initState();
    _nestedRouterDelegate =
        NestedRouterDelegate(navigatorKey: widget.navigatorKey);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Defer back button dispatching to the child router
    _backButtonDispatcher = Router.of(context)
        .backButtonDispatcher!
        .createChildBackButtonDispatcher();
  }

  @override
  Widget build(BuildContext context) {
    // Claim priority, If there are parallel sub router, you will need
    // to pick which one should take priority;
    _backButtonDispatcher!.takePriority();

    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: DialogManager(
          dialogsService: sl<DialogsService>(),
          child: Router(
            routerDelegate: _nestedRouterDelegate!,
            //backButtonDispatcher: _backButtonDispatcher,
          ),
        ));
  }
}
