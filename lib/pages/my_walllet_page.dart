import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/billings_bloc.dart';

import '../blocs/cubits/user_cubit.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../services/billing_service.dart';
import '../widgets/appbar/sub_bar.dart';
import '../widgets/loading.dart';
import '../widgets/my_wallet_widgets/billings_history_widget.dart';
import '../widgets/my_wallet_widgets/wallet_main_panel.dart';

class BillingsPage extends StatefulWidget {
  const BillingsPage({Key? key}) : super(key: key);

  @override
  _BillingsPageState createState() => _BillingsPageState();
}

class _BillingsPageState extends State<BillingsPage> {
  BorderRadiusGeometry radius = const BorderRadius.only(
    topLeft: Radius.circular(ApplicationsPageConstants.radiusCircular),
    topRight: Radius.circular(ApplicationsPageConstants.radiusCircular),
  );

  @override
  void initState() {
    sl<UserCubit>().getUserInfo(); // TODO проверить обновление инфо
    super.initState();
  }

  final ScrollController _controller = ScrollController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserInfoUpdated) {
            return BlocProvider<BillingsBloc>(
                create: (context) => BillingsBloc(
                      billingService: sl<BillingService>(),
                    )..add(FetchBillingsInfo()),
                child: Builder(builder: (context) {
                  _handleScrollNotification(ScrollNotification notification) {
                    if (notification is ScrollEndNotification &&
                        _controller.position.extentAfter == 0) {
                      debugPrint('end of list');
                      if (BlocProvider.of<BillingsBloc>(context).state
                          is BillingsLoaded) {
                        BlocProvider.of<BillingsBloc>(context)
                            .add(FetchBillingsInfo());
                      }
                    }
                    return false;
                  }

                  return NotificationListener(
                      onNotification: _handleScrollNotification,
                      child: SingleChildScrollView(
                          controller: _controller,
                          child: Padding(
                            padding: isMobile
                                ? const EdgeInsets.symmetric(horizontal: 14)
                                : const EdgeInsets.symmetric(horizontal: 36),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SubBar(context,
                                    title: 'My wallet', isMobile: isMobile),
                                isMobile
                                    ? const SizedBox(
                                        height: 14,
                                      )
                                    : const SizedBox(
                                        height: 4,
                                      ),
                                isMobile
                                    ? MobilWalletMainPanel(
                                        currentUser: state.currentUser)
                                    : DesktopWalletMainPanel(
                                        currentUser: state.currentUser),
                                isMobile
                                    ? const SizedBox(
                                        height: 32,
                                      )
                                    : const SizedBox(
                                        height: 44,
                                      ),
                                Text(
                                  'Transactions',
                                  style: isMobile
                                      ? Theme.of(context)
                                          .textTheme
                                          .headlineMedium
                                      : Theme.of(context)
                                          .textTheme
                                          .headlineLarge,
                                ),
                                isMobile
                                    ? const SizedBox(
                                        height: 16,
                                      )
                                    : const SizedBox(
                                        height: 24,
                                      ),
                                TransactionsHistory(
                                  controller: _controller,
                                ),
                                isMobile
                                    ? const SizedBox(
                                        height: 16,
                                      )
                                    : const SizedBox(
                                        height: 24,
                                      ),
                              ],
                            ),
                          )));
                }));
          }
          if (state is UserUpdatingError) {
            return Center(
              child: Text(
                'User info loaded with error ${state.message}',
              ),
            );
          }
          return const Center(
            child: Center(
              child: Loading(),
            ),
          );
        },
      ),
    );
  }
}
