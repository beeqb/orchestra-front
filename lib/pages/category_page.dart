import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/top_products_bloc.dart';
import 'package:orchestra/widgets/loading.dart';
import 'package:orchestra/widgets/marketplace_widgets/top_products/top_products.dart';

import '../blocs/cubits/products_cubit.dart';
import '../models/app_category.dart';
import '../models/enums/enums.dart';
import '../widgets/custom_app_bar.dart';
import '../widgets/marketplace_widgets/product_card.dart';

class CategoryPage extends StatelessWidget {
  final ProductCategoryType categoryType;
  const CategoryPage({Key? key, required this.categoryType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsCubit, ProductsState>(
      builder: (context, state) {
        final appCategory = (state as ProductsLoaded).categories.firstWhere(
              (e) => e.productCategoryType == categoryType,
            );
        return Scaffold(
          appBar: CustomAppBar(
            title: appCategory.productCategoryType.title,
          ),
          body: buildLayoutBuilder(appCategory),
        );
      },
    );
  }

  Widget buildLayoutBuilder(AppCategory appCategory) {
    final topProducts =
        BlocBuilder<TopProductsBlocByCategories, TopProductsByCategoriesState>(
      builder: (context, state) {
        if (state is TopProductsLoaded) {
          final prods = state.topProductsByCategories
              .firstWhere(
                  (element) => element.productCategoryType == categoryType)
              .prods;
          return TopProductsWidget(products: prods);
        } else {
          return const SizedBox(
            height: 300,
            child: Loading(),
          );
        }
      },
    );

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxWidth < 600) {
              return Column(
                children: [
                  topProducts,
                  const Divider(
                    thickness: 0.8,
                  ),
                  Wrap(
                    children: List.generate(
                      appCategory.prods.length,
                      (index) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12),
                        child: ProductCard(
                          product: appCategory.prods[index]!,
                          maxScreenWidth: constraints.maxWidth,
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
            return Column(
              children: [
                topProducts,
                const Divider(
                  thickness: 0.8,
                ),
                GridView.count(
                  physics: const NeverScrollableScrollPhysics(),
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 30,
                  childAspectRatio: 3,
                  shrinkWrap: true,
                  crossAxisCount: constraints.maxWidth < 1200 ? 3 : 5,
                  children: List.generate(
                    appCategory.prods.length,
                    (indS) => ProductCard(
                      product: appCategory.prods[indS]!,
                      maxScreenWidth: constraints.maxWidth,
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
