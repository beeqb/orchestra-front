import 'package:flutter/material.dart';

import '../blocs/bloc/results_bloc.dart';
import '../blocs/bloc/root_bloc.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../widgets/applications_widgets/application_list.dart';
import '../widgets/applications_widgets/latest_results/lastest_results.dart';
import '../widgets/applications_widgets/latest_results/sliding_up_panel.dart';

class ApplicationPage extends StatefulWidget {
  const ApplicationPage({Key? key}) : super(key: key);

  @override
  _ApplicationPageState createState() => _ApplicationPageState();
}

class _ApplicationPageState extends State<ApplicationPage> {
  final PanelController panelController = PanelController();

  @override
  void initState() {
    super.initState();
    sl<RootBloc>().add(StartLoadingAppsRootEvent());
    sl<ResultsBloc>().add(ResetResultsEvent());
    sl<ResultsBloc>().add(FetchResultsEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    final bodyHeightMobile = MediaQuery.of(context).size.height -
        Scaffold.of(context).appBarMaxHeight! -
        AppBarSizeCostants.mobileSubBarHeight -
        ApplicationsPageConstants.mobileTopPanelHeight;
    final bodyHeightDesktop = MediaQuery.of(context).size.height -
        Scaffold.of(context).appBarMaxHeight! -
        AppBarSizeCostants.desktopSubBarHeight -
        ApplicationsPageConstants.desktopTopPanelHeight;
    return Scaffold(
      body: Stack(
        children: [
          Padding(
              padding: isMobile
                  ? const EdgeInsets.only(
                      top: ApplicationsPageConstants.mobileTopPanelHeight,
                      left:
                          ApplicationsPageConstants.mobilePageHorizontalPadding,
                      right:
                          ApplicationsPageConstants.mobilePageHorizontalPadding,
                      bottom: ApplicationsPageConstants.mobileBottomPanelHeight)
                  : const EdgeInsets.only(
                      top: ApplicationsPageConstants.desktopTopPanelHeight,
                      left: ApplicationsPageConstants
                          .desktopPageHorizontalPadding,
                      right: ApplicationsPageConstants
                          .desktopPageHorizontalPadding,
                      bottom:
                          ApplicationsPageConstants.desktopBottomPanelHeight,
                    ),
              child: const ApplicationList()),
          Padding(
              padding: isMobile
                  ? EdgeInsets.zero
                  : const EdgeInsets.symmetric(
                      horizontal: 36,
                    ),
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return SlidingUpPanel(
                    minHeight: isMobile
                        ? ApplicationsPageConstants.mobileBottomPanelHeight
                        : ApplicationsPageConstants.desktopBottomPanelHeight,
                    maxHeight: isMobile ? bodyHeightMobile : bodyHeightDesktop,
                    width: constraints.maxWidth,
                    controller: panelController,
                    color: Theme.of(context).backgroundColor,
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(
                          ApplicationsPageConstants.radiusCircular),
                      topRight: Radius.circular(
                          ApplicationsPageConstants.radiusCircular),
                    ),
                    panel: LatestResults(
                      panelController: panelController,
                    ),
                  );
                },
              ))
        ],
      ),
    );
  }
}
