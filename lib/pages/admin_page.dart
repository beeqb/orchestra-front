import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:orchestra/blocs/bloc/product_code_bloc.dart';
import 'package:orchestra/blocs/bloc/running_apps_bloc.dart';
import 'package:orchestra/widgets/admin_widgets/running_apps_widget.dart';
import '../blocs/bloc/admin_products_bloc.dart';
import '../blocs/bloc/admin_transactions_bloc.dart';
import '../blocs/bloc/all_users_bloc.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../widgets/admin_widgets/products_widget.dart';
import '../widgets/admin_widgets/transactions_widget.dart';
import '../widgets/admin_widgets/users_widget.dart';
import '../widgets/custom_app_bar.dart';
import '../widgets/loading.dart';

class AdminPage extends StatelessWidget {
  const AdminPage({Key? key}) : super(key: key);

  final String title = 'Administration';
  final int tabLength = 4;
  final tabs = const [
    Tab(
        icon: Icon(
      MdiIcons.accountMultiple,
      color: Color(0xff0c99c3),
    )),
    Tab(
        icon: Icon(
      MdiIcons.puzzle,
      color: Color(0xff0c99c3),
    )),
    Tab(
        icon: Icon(
      MdiIcons.currencyUsd,
      color: Color(0xff0c99c3),
    )),
    Tab(
        icon: Icon(
      MdiIcons.animationPlayOutline,
      color: Color(0xff0c99c3),
    )),
  ];

  @override
  Widget build(BuildContext context) {
    const Widget _loader = Center(child: Loading());

    Widget prodTable = Expanded(
      child: SingleChildScrollView(
        child: BlocBuilder<AdminProductsBloc, AdminProductsState>(
          builder: (context, state) {
            if (state is AdminProductsLoaded) {
              return BlocProvider(
                create: (context) => sl<ProductCodeBLoC>(),
                child: ProductsWidget(
                  dataForColumns: kProdDataColumns,
                  dataForTableSource: state.products,
                  sortColumnIndex: state.sortColumnIndex,
                  sortAscending: state.sortAscending,
                ),
              );
            }
            return _loader;
          },
        ),
      ),
    );

    Widget usersTable = Expanded(
      child: SingleChildScrollView(
        child: BlocBuilder<AllUsersBloc, AllUsersState>(
          builder: (context, state) {
            if (state is AllUsersLoaded) {
              return AdminUsersWidget(
                dataForColumns: kUserDataColumns,
                dataForTableSource: state.users,
                sortColumnIndex: state.sortColumnIndex,
                sortAscending: state.sortAscending,
              );
            }
            return _loader;
          },
        ),
      ),
    );

    Widget transactionsTable = Expanded(
      child: SingleChildScrollView(
        child: BlocBuilder<AdminTransactionsBloc, AdminTransactionsState>(
          builder: (context, state) {
            if (state is AdminTransactionsLoaded) {
              return TransactionsWidget(
                dataForColumns: kTransactionDataColumns,
                dataForTableSource: state.transactions,
              );
            }
            return _loader;
          },
        ),
      ),
    );

    Widget runningAppsTable = Expanded(child: SingleChildScrollView(
      child: BlocBuilder<RunningAppsBloc, RunningAppsState>(
        builder: (context, state) {
          if (state is LoadedRunningAppsState) {
            return RunningAppsWidget(
              dataForColumns: kRunningAppsDataColumn,
              dataForTableSource: state.runningApps,
            );
          }
          return _loader;
        },
      ),
    ));

    var usersTableHeader = _tableHeader(
        onSearch: (value) =>
            sl<AllUsersBloc>().add(SearchUsersEvent(query: value)));

    var prodTableHeader = _tableHeader(
        onSearch: (value) =>
            sl<AdminProductsBloc>().add(SearchProductsEvent(value)));

    Widget pageBodyWidget = TabBarView(
      children: [
        Column(
          children: [
            usersTableHeader,
            usersTable,
          ],
        ),
        Column(
          children: [
            prodTableHeader,
            prodTable,
          ],
        ),
        Column(
          children: [
            transactionsTable,
          ],
        ),
        Column(
          children: [
            runningAppsTable,
          ],
        ),
      ],
    );

    Widget buildLayoutBuilder = LayoutBuilder(builder: (context, constraints) {
      return MultiBlocProvider(
        providers: [
          BlocProvider<AllUsersBloc>(
            create: (context) => sl<AllUsersBloc>()..add(FetchAllUsersEvent()),
          ),
          BlocProvider(
            create: (context) =>
                sl<AdminProductsBloc>()..add(LoadAllProductsEvent()),
          ),
          BlocProvider(
            create: (context) =>
                sl<AdminTransactionsBloc>()..add(LoadAdminTransactionsEvent()),
          ),
          BlocProvider(
            create: (context) =>
                sl<RunningAppsBloc>()..add(const FetchRunningAppsEvent()),
            child: Container(),
          )
        ],
        child: pageBodyWidget,
      );
    });

    var tabBarHeader = PreferredSize(
      preferredSize: const Size.fromHeight(70),
      child: TabBar(tabs: tabs),
    );

    Widget scaffoldView = Scaffold(
      appBar: CustomAppBar(
        title: title,
        bottom: tabBarHeader,
      ),
      body: buildLayoutBuilder,
    );

    return DefaultTabController(
      length: tabLength,
      child: scaffoldView,
    );
  }

  Widget _tableHeader({required Function(String query) onSearch}) {
    Widget textFormField = TextFormField(
      onChanged: onSearch,
      decoration: const InputDecoration(
          prefixIcon: Padding(
        padding: EdgeInsetsDirectional.only(start: 10.0),
        child: Icon(
          MdiIcons.magnify,
          color: Colors.grey,
        ),
      )),
    );

    return SizedBox(
      height: 100,
      child: Row(
        children: [
          Expanded(
              child: Padding(
            padding: const EdgeInsetsDirectional.only(start: 20.0, end: 20.0),
            child: textFormField,
          )),
        ],
      ),
    );
  }
}
