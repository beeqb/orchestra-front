import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/onboarding_nav_bloc.dart';
import 'package:orchestra/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_html/html.dart' as html;

import '../core/const.dart';
import '../injection_container.dart';
import '../services/user_service.dart';

class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: sl<UserService>().checkLocalStorage(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData && snapshot.data != UserModel.empty) {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
              });
            }
          }
          return const OnBoardingPageView();
        },
      ),
    );
  }
}

class OnBoardingPageView extends StatefulWidget {
  const OnBoardingPageView({Key? key}) : super(key: key);

  @override
  _OnBoardingPageViewState createState() => _OnBoardingPageViewState();
}

class _OnBoardingPageViewState extends State<OnBoardingPageView> {
  final PageController _pageController = PageController(initialPage: 0);

  int _currentPage = 0;
  final _countPage = 4;

  static const colors = [
    Color(0xff000000),
    Color(0xffff8f63),
    Color(0xffb22f42),
    Color(0xff4d3d8a),
  ];

  List<Widget> viewPages = [];

  void onChangeValuePageController(int value) {
    setState(() {
      _currentPage = value;
    });
  }

  Widget _buildImage(String assetName) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Image.asset(
        assetName,
        fit: BoxFit.contain,
        // width: 350,
        // height: 400,
        height: MediaQuery.of(context).size.height / 2,
      ),
    );
  }

  final bodyStyle = const TextStyle(
    fontSize: 19.0,
    color: Colors.white,
  );
  final bodyTextWidth = 350.0;
  final textTitleStyle = const TextStyle(
    fontSize: 34.0,
    fontWeight: FontWeight.w700,
    color: Colors.white,
  );
  final textSubtitleStyle = GoogleFonts.openSansCondensed().copyWith(
    fontSize: 34,
    color: Colors.white,
    fontWeight: FontWeight.w100,
  );

  void _onIntroEnd(context) {
    sl<OnboardingNavBLoC>().add(const HideOnboardingNavEvent());
    // Future.delayed(const Duration(seconds: 5)).then(
    //   (value) =>
    //       sl<DialogsService>().showDialog(dialogType: DialogType.welcomeDialog),
    // );
  }

  Widget _toggleBtn(Widget btn, bool isShow) {
    return isShow
        ? btn
        : Opacity(opacity: 0.0, child: IgnorePointer(child: btn));
  }

  void animateToPage(int page) {
    _pageController.animateToPage(page,
        duration: const Duration(milliseconds: 350), curve: Curves.easeIn);
  }

  void onTapNextBtn() {
    setState(() {
      _currentPage++;
      animateToPage(_currentPage);
    });
  }

  void onTapPreviousBtn() {
    if (_currentPage > 0) {
      setState(() {
        _currentPage--;
        animateToPage(_currentPage);
      });
    }
  }

  Widget buildBtn({
    required String title,
    Function()? onTap,
    bool isShow = true,
  }) {
    return _toggleBtn(
      TextButton(
          onPressed: onTap,
          child: Text(
            title,
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          )),
      isShow,
    );
  }

  Widget _buildBottomPanel() {
    return Container(
      padding: const EdgeInsets.all(10),
      child: Row(children: [
        Expanded(
          flex: 1,
          child: buildBtn(
            title: 'Previous',
            onTap: onTapPreviousBtn,
            isShow: (_currentPage > 0),
          ),
        ),
        Expanded(
          flex: 1,
          child: Center(
              child: DotsIndicator(
                  dotsCount: _countPage,
                  position: _currentPage.toDouble(),
                  onTap: (position) {
                    _pageController.animateToPage(position.toInt(),
                        duration: const Duration(milliseconds: 350),
                        curve: Curves.easeIn);
                    setState(() {
                      _currentPage = position.toInt();
                    });
                  })),
        ),
        Expanded(
            flex: 1,
            child: !(_currentPage == (_countPage - 1))
                ? buildBtn(title: 'Next', onTap: onTapNextBtn, isShow: true)
                : buildBtn(title: 'Done', onTap: () => _onIntroEnd(context))),
        Expanded(
            flex: 1,
            child: buildBtn(
                title: 'Skip',
                onTap: () => _onIntroEnd(context),
                isShow: true)),
      ]),
    );
  }

  Widget _buildPageView(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: PageView(
            controller: _pageController,
            onPageChanged: (value) => onChangeValuePageController(value),
            children: [
              _buildPageViewContainer(
                0,
                title: 'Psst!',
                subtitle: 'Want some boost?',
                bodyText: 'Orchestra is the world’s first marketplace that'
                    " provides the most featured AI's and RPA's solutions with the"
                    ' lowest price in the fastest way.',
                image: _buildImage(
                  kOnboardingImages[0],
                ),
              ),
              _buildPageViewContainer(
                1,
                title: 'Нero from Zero',
                bodyText:
                    // ignore: lines_longer_than_80_chars
                    'No coding, no time-wasting, no hassles, no trash pay. Go to the'
                    ' marketplace find the needed booster and start using it. Get '
                    'super boost result in 5 clicks.',
                image: _buildImage(
                  kOnboardingImages[1],
                ),
              ),
              _buildPageViewContainer(
                2,
                title: 'No money needed',
                subtitle: 'Pay nothing till you love it!',
                bodyText: 'Pay nothing till you love it!\n'
                    'Try all of solutions for free and pay as you go. '
                    'No hidden fees. One second tariffication.',
                image: _buildImage(
                  kOnboardingImages[2],
                ),
              ),
              _buildPageViewContainer(
                3,
                title: 'Need some Money',
                subtitle: 'We want to pay you...',
                bodyText:
                    // ignore: lines_longer_than_80_chars
                    "Welcome & Referral bonus, Scout's & Affiliate campaign give you"
                    ' access to no money consuming.',
                image: _buildImage(
                  kOnboardingImages[3],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: _buildBottomPanel(),
        ),
      ],
    );
  }

  Widget _buildPageViewContainer(
    int i, {
    required String title,
    String? subtitle,
    required String bodyText,
    required Widget image,
  }) {
    return Container(
      color: colors[i],
      child: Align(
        alignment: Alignment.bottomCenter,
        child: SizedBox(
          width: 350,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              image,
              AutoSizeText(
                title,
                style: const TextStyle(
                    fontSize: 34.0,
                    fontWeight: FontWeight.w700,
                    color: Colors.white),
              ),
              SizedBox(
                width: bodyTextWidth,
                child: subtitle != null
                    ? AutoSizeText(
                        subtitle,
                        style: textSubtitleStyle,
                      )
                    : null,
              ),
              const SizedBox(
                height: 10,
              ),
              AutoSizeText(
                bodyText,
                style: bodyStyle,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    var uri = Uri.dataFromString(html.window.location.href);

    addReferralInfoLocalStorage(uri);
  }

  void addReferralInfoLocalStorage(Uri uri) async {
    /// Записываем токен реферала в локальное хранилище, чтобы при авторизации
    /// проверить, есть ли ссылка на реферала
    var parameters = uri.queryParameters;
    if (parameters.isNotEmpty) {
      await sl<SharedPreferences>().setString('refCode', parameters['ref']!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return _buildPageView(context);
  }
}

const Size kDefaultSize = Size.square(9.0);
const EdgeInsets kDefaultSpacing = EdgeInsets.all(6.0);
const ShapeBorder kDefaultShape = CircleBorder();

class DotsDecorator {
  /// Inactive dot color
  ///
  /// @Default `Colors.grey`
  final Color color;

  /// Active dot color
  ///
  /// @Default `Colors.lightBlue`
  final Color activeColor;

  /// Inactive dot size
  ///
  /// @Default `Size.square(9.0)`
  final Size size;

  /// Active dot size
  ///
  /// @Default `Size.square(9.0)`
  final Size activeSize;

  /// Inactive dot shape
  ///
  /// @Default `CircleBorder()`
  final ShapeBorder shape;

  /// Active dot shape
  ///
  /// @Default `CircleBorder()`
  final ShapeBorder activeShape;

  /// Spacing between dots
  ///
  /// @Default `EdgeInsets.all(6.0)`
  final EdgeInsets spacing;

  const DotsDecorator({
    this.color = Colors.grey,
    this.activeColor = Colors.lightBlue,
    this.size = kDefaultSize,
    this.activeSize = kDefaultSize,
    this.shape = kDefaultShape,
    this.activeShape = kDefaultShape,
    this.spacing = kDefaultSpacing,
  });
}

typedef OnTap = void Function(double position);

class DotsIndicator extends StatelessWidget {
  final int dotsCount;
  final double position;
  final DotsDecorator decorator;
  final Axis axis;
  final bool reversed;
  final OnTap? onTap;
  final MainAxisSize mainAxisSize;
  final MainAxisAlignment mainAxisAlignment;

  const DotsIndicator({
    Key? key,
    required this.dotsCount,
    this.position = 0.0,
    this.decorator = const DotsDecorator(),
    this.axis = Axis.horizontal,
    this.reversed = false,
    this.mainAxisSize = MainAxisSize.min,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.onTap,
  })  : assert(dotsCount > 0),
        assert(position >= 0),
        assert(
          position < dotsCount,
          'Position must be inferior than dotsCount',
        ),
        super(key: key);

  Widget _buildDot(int index) {
    final state = min(1.0, (position - index).abs());

    final size = Size.lerp(decorator.activeSize, decorator.size, state)!;
    final color = Color.lerp(decorator.activeColor, decorator.color, state);
    final shape = ShapeBorder.lerp(
      decorator.activeShape,
      decorator.shape,
      state,
    )!;

    final dot = Container(
      width: size.width,
      height: size.height,
      margin: decorator.spacing,
      decoration: ShapeDecoration(
        color: color,
        shape: shape,
      ),
    );
    return onTap == null
        ? dot
        : InkWell(
            customBorder: const CircleBorder(),
            onTap: () => onTap!(index.toDouble()),
            child: dot,
          );
  }

  @override
  Widget build(BuildContext context) {
    final dotsList = List<Widget>.generate(dotsCount, _buildDot);
    final dots = reversed == true ? dotsList.reversed.toList() : dotsList;

    return axis == Axis.vertical
        ? Column(
            mainAxisAlignment: mainAxisAlignment,
            mainAxisSize: mainAxisSize,
            children: dots,
          )
        : Row(
            mainAxisAlignment: mainAxisAlignment,
            mainAxisSize: mainAxisSize,
            children: dots,
          );
  }
}
