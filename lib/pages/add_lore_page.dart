import 'package:flutter/material.dart';
import 'package:orchestra/widgets/add_lore/lore_submit_edit.dart';
import 'package:provider/provider.dart';

import '../models/product/product.dart';
import '../widgets/add_lore/add_lore_form_model.dart';
import '../widgets/add_lore/lore_details/lore_details_section.dart';
import '../widgets/add_lore/lore_launcher/lore_launcher_section.dart';
import '../widgets/add_lore/lore_pricing_business_model/lore_pricing_business_model_section.dart';
import '../widgets/add_lore/lore_screenshots/lore_screenshots_section.dart';
import '../widgets/add_lore/lore_submit.dart';
import '../widgets/appbar/sub_bar.dart';

class AddLorePage extends StatelessWidget {
  final Product? product;
  const AddLorePage(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(
            left: 36,
            right: 36,
          ),
          child: ChangeNotifierProvider(
              create: (_) => AddLoreFormModel(product: product),
              builder: (context, child) {
                return Form(
                  key: context.read<AddLoreFormModel>().formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SubBar(context,
                          title: 'Add Lore to Marketplace', isMobile: false),
                      const LoreDetailsSection(),
                      const LoreScreenshotsSection(),
                      const LoreLauncherSection(),
                      const LorePricingBusinessModelSection(),
                      product == null
                          ? const LoreSubmit()
                          : const LoreSubmitEdit(),
                      const SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                );
              }),
        ),
      ),
    );
  }
}
