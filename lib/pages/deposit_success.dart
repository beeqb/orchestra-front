import 'dart:async';

import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';

import '../injection_container.dart';

class DepositSuccessPage extends StatefulWidget {
  const DepositSuccessPage({Key? key}) : super(key: key);

  @override
  _DepositSuccessPageState createState() => _DepositSuccessPageState();
}

class _DepositSuccessPageState extends State<DepositSuccessPage> {
  @override
  void initState() {
    startTime();
    super.initState();
  }

  Future<Timer> startTime() async {
    var _duration = const Duration(seconds: 4);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() =>
      sl<NavigationBLoC>().add(const ToBillingsNavigationEvent());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: navigationPage,
                child: const SizedBox(
                  height: 70,
                  width: 70,
                  child: Icon(
                    Icons.check_circle,
                    color: Colors.green,
                    size: 50,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Text(
                  'Successfully deposited',
                  style: Theme.of(context).textTheme.headline6,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
