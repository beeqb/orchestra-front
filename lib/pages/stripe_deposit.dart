// ignore_for_file: unnecessary_string_escapes

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../blocs/cubits/checkout_cubit.dart';
import '../injection_container.dart';
import '../util/constants.dart';
import '../widgets/custom_app_bar.dart';
import '../widgets/loading.dart';

void redirectToCheckout(BuildContext context) async {
  sl<NavigationBLoC>().add(const ToStripeDepositNavigationEvent());
}

class StripeDepositPage extends StatefulWidget {
  const StripeDepositPage({Key? key}) : super(key: key);

  @override
  _StripeDepositPageState createState() => _StripeDepositPageState();
}

class _StripeDepositPageState extends State<StripeDepositPage> {
  WebViewController? _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(title: 'Deposit'),
      body: SafeArea(
        child: BlocBuilder<CheckoutCubit, CheckoutState>(
          builder: (context, state) {
            if (state is CheckoutLoaded) {
              return WebView(
                initialUrl: initialUrl,
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (controller) => _controller = controller,
                onPageFinished: (url) {
                  if (url == initialUrl) {
                    _redirectToStripe(
                      sessionId: state.session,
                    );
                  }
                },
                navigationDelegate: (request) {
                  if (request.url.startsWith(kDepositSuccessPageUrl)) {
                    sl<NavigationBLoC>().add(
                        const ToDepositSuccessNavigationEvent()); // <-- Handle success case
                  } else if (request.url.startsWith(kDepositFailedPageUrl)) {
                    sl<NavigationBLoC>().add(
                        const ToDepositFailedNavigationEvent()); // <-- Handle cancel case
                  }
                  return NavigationDecision.navigate;
                },
              );
            }
            return const Center(
              child: Loading(),
            );
          },
        ),
      ),
    );
  }

  String get initialUrl =>
      'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(kStripeHtmlPage))}';

  void _redirectToStripe({required String sessionId}) {
    //<--- prepare the JS in a normal string
    final redirectToCheckoutJs = '''
var stripe = Stripe(\'$kPublishableKeyStripe\');
    
stripe.redirectToCheckout({
  sessionId: '$sessionId'
}).then(function (result) {
  result.error.message = 'Error'
});
''';
    _controller!.evaluateJavascript(
        redirectToCheckoutJs); //<--- call the JS function on controller
  }
}

const kStripeHtmlPage = '''
<!DOCTYPE html>
<html>
<script src="https://js.stripe.com/v3/"></script>
<head><title>Stripe checkout</title></head>
<body>
Please, wait ...
</body>
</html>
''';
