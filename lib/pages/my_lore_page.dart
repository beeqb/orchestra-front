import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/bloc/purchased_bloc.dart';
import '../core/const.dart';
import '../widgets/appbar/sub_bar.dart';
import '../widgets/loading.dart';
import '../widgets/my_lore_widgets/my_lore_products.dart';

class MyLorePage extends StatelessWidget {
  const MyLorePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Scaffold(
      body: SafeArea(child:
          BlocBuilder<PurchasedBloc, PurchasedState>(builder: (context, state) {
        if (state is PurchasedLoaded) {
          return SingleChildScrollView(
            child: Padding(
              padding: isMobile
                  ? const EdgeInsets.symmetric(horizontal: 14, vertical: 6)
                  : const EdgeInsets.fromLTRB(55, 0, 36, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SubBar(context, title: 'My lore', isMobile: isMobile),
                  SizedBox(
                    height: isMobile ? 16 : 0,
                  ),
                  MyLoreProducts(purchaseModels: state.purchasedProducts),
                  isMobile
                      ? const SizedBox(
                          height: 10,
                        )
                      : const SizedBox(
                          height: 40,
                        ),
                ],
              ),
            ),
          );
        }
        if (state is PurchasedLoading) {
          return const Loading();
        }
        if (state is PurchasedError) {
          return Center(
            child: Text('My lore loading error ${state.message}'),
          );
        }
        return const SizedBox(
          height: 1,
        );
      })),
    );
  }
}
