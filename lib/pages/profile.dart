import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/cubits/profile_menu_cubit.dart';
import 'package:orchestra/widgets/profile_widgets/side_panel.dart';

import '../blocs/cubits/user_cubit.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../widgets/appbar/sub_bar.dart';
import '../widgets/loading.dart';
import '../widgets/profile_widgets/address_tab.dart';
import '../widgets/profile_widgets/contributions_tab.dart';
import '../widgets/profile_widgets/my_profile_tab.dart';
import '../widgets/profile_widgets/referral_program_tab.dart';
import '../widgets/profile_widgets/x_api_key_tab.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sl<UserCubit>().getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return isMobile
        ? const MobileSelectedTab()
        : Scaffold(
            body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36),
              child: Column(
                children: [
                  SubBar(context, title: 'Profile', isMobile: false),
                  SizedBox(
                    height: 900,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        ProfileTabPanel(),
                        SizedBox(
                          width: 32,
                        ),
                        Expanded(child: DesktopSelectedTab()),
                      ],
                    ),
                  ),
                  isMobile
                      ? const SizedBox(
                          height: 10,
                        )
                      : const SizedBox(
                          height: 40,
                        ),
                ],
              ),
            ),
          ));
  }
}

void showMessage(BuildContext context, String message,
    [MaterialColor color = Colors.red,
    Duration duration = const Duration(seconds: 2)]) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    backgroundColor: color,
    content: Text(message),
    duration: duration,
  ));
}

class DesktopSelectedTab extends StatelessWidget {
  const DesktopSelectedTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(builder: (context, state) {
      if (state is UserInfoUpdated) {
        return BlocBuilder<ProfileMenuCubit, int>(
            builder: (context, menuState) {
          final int selectedTab = menuState;
          final currentUser = state.currentUser;
          switch (selectedTab) {
            case 0:
              return DesktopMyProfileTab(currentUser);
            case 1:
              return DesktopAddressTab(currentUser);
            case 2:
              return DesktopReferralProgramTab(currentUser);
            case 3:
              return DesktopXApiKeyTab(currentUser);
            case 4:
              return DesktopContributionsTab(currentUser);
            default:
              return DesktopMyProfileTab(currentUser);
          }
        });
      } else if (state is UserInfoUpdating) {
        return const Loading();
      } else if (state is UserUpdatingError) {
        return Center(
          child: Text(state.message),
        );
      }
      return const SizedBox.shrink();
    });
  }
}

class MobileSelectedTab extends StatelessWidget {
  const MobileSelectedTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(builder: (context, state) {
      if (state is UserInfoUpdated) {
        return BlocBuilder<ProfileMenuCubit, int>(
            builder: (context, menuState) {
          final int selectedTab = menuState;
          final currentUser = state.currentUser;
          switch (selectedTab) {
            case 0:
              return MobileMyProfileTab(currentUser);
            case 1:
              return MobileAddressTab(currentUser);
            case 2:
              return MobileReferralProgramTab(currentUser);
            case 3:
              return MobileXApiKeyTab(currentUser);
            case 4:
              return MobileContributionsTab(currentUser);
            default:
              return MobileMyProfileTab(currentUser);
          }
        });
      } else if (state is UserInfoUpdating) {
        return const Loading();
      } else if (state is UserUpdatingError) {
        return Center(
          child: Text(state.message),
        );
      }
      return const SizedBox.shrink();
    });
  }
}
