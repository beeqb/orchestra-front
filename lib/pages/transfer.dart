import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../blocs/cubits/transfer_user_cubit.dart';
import '../injection_container.dart';
import '../widgets/custom_app_bar.dart';

class TransferPage extends StatefulWidget {
  const TransferPage({Key? key}) : super(key: key);

  @override
  _TransferPageState createState() => _TransferPageState();
}

class _TransferPageState extends State<TransferPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  double _transferAmount = 0.0;

  @override
  void initState() {
    super.initState();
  }

  final Widget _textUser = const Text(
    'User:',
    style: TextStyle(
        color: Colors.black54, fontWeight: FontWeight.w400, fontSize: 18),
  );

  final Widget _padding = const SizedBox(height: 18);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(
        title: 'Transfer',
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Amount:',
                        style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.w400,
                            fontSize: 18),
                      ),
                      TextFormField(
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          labelText: 'Enter amount in USD',
                        ),
                        onSaved: (newValue) {
                          _transferAmount = double.parse(newValue ?? '0.0');
                        },
                        validator: (val) =>
                            val!.isEmpty ? 'Amount is required' : null,
                      ),
                      _padding,
                      _textUser,
                      TextFormField(
                        decoration: const InputDecoration(
                          labelText: 'Enter exact user ID, name or email',
                        ),
                        onChanged: (value) async {
                          if (value.length > 1) {
                            await sl<TransferUserCubit>().getUserByQuery(value);
                          }
                        },
                        validator: (val) => val!.isEmpty
                            ? 'Name, user ID or email is required'
                            : null,
                      ),
                      _padding,
                      buildUserInfo(
                        context: context,
                      ),
                      _padding,
                      Center(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.red,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('Cancel'),
                        ),
                      ),
                      _padding,
                      Center(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            _transferFunds();
                          },
                          child: const Text('Transfer funds'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final Widget _emptyUser = Center(
    child: Container(
      height: 100,
      width: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.grey[50],
      ),
      child: const Icon(
        Icons.person,
        size: 70,
      ),
    ),
  );

  Widget buildUserInfo({required BuildContext context}) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<TransferUserCubit, TransferUserState>(
          builder: (context, state) {
            if (state is TransferUserUpdated) {
              return Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 100,
                        width: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(
                                state.transferUser.photoUrl),
                          ),
                          border:
                              Border.all(color: Colors.grey[400]!, width: 3),
                        ),
                      ),
                    ),
                    Text(
                      state.transferUser.name,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ],
                ),
              );
            }
            if (state is TransferSent) {
              return _emptyUser;
            }
            return _emptyUser;
          },
        ),
      ),
    );
  }

  void _transferFunds() {
    final form = _formKey.currentState;
    if (!form!.validate()) {
      showMessage('Form is not valid! Please review and correct.');
    } else {
      form.save();
      sl<TransferUserCubit>().sendTransfer(_transferAmount);
      form.reset();
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(backgroundColor: color, content: Text(message)));
  }
}
