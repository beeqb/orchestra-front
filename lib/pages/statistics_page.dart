import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/cubits/products_cubit.dart';
import 'package:orchestra/widgets/appbar/sub_bar.dart';
import 'package:orchestra/widgets/statistic/customer_activity.dart';
import 'package:orchestra/widgets/statistic/daily_sales.dart';
import 'package:orchestra/widgets/statistic/launches_statistic.dart';

import '../blocs/bloc/added_products_bloc.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../widgets/loading.dart';
import '../widgets/statistic/added_lore.dart';
import '../widgets/statistic/statistic_chart.dart';

class StatisticsPage extends StatefulWidget {
  const StatisticsPage({Key? key}) : super(key: key);

  @override
  StatisticsPageState createState() => StatisticsPageState();
}

class StatisticsPageState extends State<StatisticsPage> {
  @override
  void initState() {
    super.initState();
    sl<AddedProductsBloc>().add(GetProductsByIdEvent());
  }

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Scaffold(
      body: BlocBuilder<AddedProductsBloc, AddedProductsState>(
        builder: (context, state) {
          if (state is AddedProductsLoaded) {
            final myProducts = state.addedProducts;
            return Padding(
              padding: isMobile
                  ? const EdgeInsets.symmetric(horizontal: 14, vertical: 6)
                  : const EdgeInsets.fromLTRB(55, 0, 36, 0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SubBar(context, title: 'Statistics', isMobile: isMobile),
                    isMobile
                        ? const SizedBox(
                            height: 14,
                          )
                        : const SizedBox(
                            height: 4,
                          ),
                    isMobile
                        ? Wrap(
                            spacing: 10,
                            runSpacing: 10,
                            children: const [
                              DailySalesWidget(),
                              LaunchesStatistic(),
                              CustomerActivityWidget()
                            ],
                          )
                        : Row(
                            children: const [
                              Expanded(child: DailySalesWidget()),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(child: LaunchesStatistic()),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(child: CustomerActivityWidget())
                            ],
                          ),
                    const SizedBox(
                      height: 22,
                    ),
                    BlocBuilder<ProductsCubit, ProductsState>(
                      builder: (context, productsState) {
                        if (productsState is ProductsLoaded) {
                          final myLores = state.addedProducts;
                          return isMobile
                              ? Wrap(
                                  spacing: 10,
                                  runSpacing: 10,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: List.generate(
                                    myLores.length + 1,
                                    (index) {
                                      return index == 0
                                          ? StatisticChart(products: myLores
                                              // products: myProducts
                                              )
                                          : AddedLoreCard(
                                              product: myLores[index - 1]);
                                    },
                                  ),
                                )
                              : GridView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: myLores.length + 1,
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 3,
                                          crossAxisSpacing: 20,
                                          mainAxisSpacing: 20,
                                          mainAxisExtent: StatisticsConstants
                                              .secondRowHeight),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return index == 0
                                        ? StatisticChart(products: myLores
                                            // products: myProducts
                                            )
                                        : AddedLoreCard(
                                            product: myLores[index - 1]);
                                  },
                                );
                        }
                        return const Loading();
                      },
                    ),
                  ],
                ),
              ),
            );
          }
          return const Loading();
        },
      ),
    );
  }
}

// class SalesData {
//   SalesData(this.year, this.sales);
//   final String year;
//   final double sales;
//
//   @override
//   String toString() => 'SalesData(year: $year, sales: $sales)';
// }
