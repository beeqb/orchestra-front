import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/widgets/appbar/sub_bar.dart';

import '../blocs/bloc/top_ten_products_bloc.dart';
import '../blocs/cubits/products_cubit.dart';
import '../core/const.dart';
import '../injection_container.dart';
import '../widgets/loading.dart';
import '../widgets/marketplace_widgets/product_categories.dart';
import '../widgets/marketplace_widgets/top_products/top_products.dart';

class MarketplacePage extends StatelessWidget {
  const MarketplacePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return Scaffold(
      body: BlocBuilder<ProductsCubit, ProductsState>(
        builder: (context, state) {
          if (state is ProductsLoaded) {
            final topProducts = BlocProvider(
              create: (context) => sl<TopTenProductsBLoC>()
                ..add(const ReadTopTenProductsEvent()),
              child: const TopProducts(),
            );
            return SingleChildScrollView(
              child: Padding(
                padding: isMobile
                    ? const EdgeInsets.fromLTRB(14, 6, 14, 6)
                    : const EdgeInsets.fromLTRB(56, 0, 36, 82),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SubBar(
                      context,
                      title: 'Marketplace',
                      isMobile: isMobile,
                    ),
                    SizedBox(
                      height: isMobile ? 16 : 0,
                    ),
                    topProducts,
                    SizedBox(height: isMobile ? 32 : 44),
                    ProductCategories(categories: state.categories),
                  ],
                ),
              ),
            );
          } else {
            return const Loading();
          }
        },
      ),
    );
  }
}
