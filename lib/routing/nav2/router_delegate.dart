import 'dart:async';

import 'package:flutter/material.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/onboarding_nav_bloc.dart';
import 'package:orchestra/main.dart';

import '../../injection_container.dart';
import '../../models/user.dart';
import '../../pages/main_page.dart';
import '../../pages/on_boarding_page.dart';
import 'routes.dart';

class OrchestraRouterDelegate extends RouterDelegate<OrchestraRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<OrchestraRoutePath> {
  final UserModel _currentUser;
  final NavigationBLoC _navigationBLoC;
  final OnboardingNavBLoC _onboardingNavBLoC;
  StreamSubscription? _subscription;
  StreamSubscription? _subscriptionOnboard;
  OrchestraRoutePath? _currentConfiguration;
  bool _onboard;

  OrchestraRouterDelegate()
      : _currentUser = sl<AuthBloc>().state.currentUser,
        _onboard = true,
        _onboardingNavBLoC = sl<OnboardingNavBLoC>(),
        _navigationBLoC = sl<NavigationBLoC>() {
    _subscription = _navigationBLoC.stream.listen((state) {
      _currentConfiguration = state.routePath;
      notifyListeners();
    });
    _subscriptionOnboard = _onboardingNavBLoC.stream.listen((state) {
      if (state is ShownOnboardingNavState) {
        _onboard = true;
      } else {
        _onboard = false;
      }
      notifyListeners();
    });
  }

  @override
  void dispose() {
    _subscription!.cancel();
    _subscriptionOnboard!.cancel();
    super.dispose();
  }

  @override
  Future<bool> popRoute() async {
    debugPrint(currentConfiguration.toString());
    debugPrint('popRoute');
    if (currentConfiguration is CategoryRoutePath ||
        currentConfiguration is ProductDetailsRoutePath) {
      sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
    }
    return true;
  }

  @override
  OrchestraRoutePath get currentConfiguration {
    debugPrint('currentUser $_currentUser');
    debugPrint('currentConfiguration $_currentConfiguration');
    if (_currentUser != UserModel.empty || !_onboard) {
      return _currentConfiguration ?? MarketplaceRoutePath();
    } else {
      return _currentConfiguration ?? OnboardingRoutePath();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: [
        _currentUser == UserModel.empty && _onboard
            ? const MaterialPage(
                key: ValueKey('OnboardingPage'),
                child: OnBoardingPage(),
              )
            : const MaterialPage(
                key: ValueKey('MainPage'),
                child: MainPage(),
              ),
      ],
      onPopPage: (route, result) {
        debugPrint('route $result');
        debugPrint('did pop $result');
        if (!route.didPop(result)) {
          return false;
        }
        notifyListeners();
        return true;
      },
    );
  }

  @override
  Future<void> setNewRoutePath(configuration) async {
    debugPrint('setNewRoutePath $configuration');
    if (configuration is MarketplaceRoutePath) {
      sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
    } else if (configuration is DashboardRoutePath) {
      sl<NavigationBLoC>().add(const ToDashboardNavigationEvent());
    } else if (configuration is BillingsRoutePath) {
      sl<NavigationBLoC>().add(const ToBillingsNavigationEvent());
    } else if (configuration is ProfileRoutePath) {
      sl<NavigationBLoC>().add(const ToProfileNavigationEvent());
    } else if (configuration is AddProductRoutePath) {
      sl<NavigationBLoC>().add(const ToAddProductNavigationEvent());
    } else if (configuration is AddedProductsRoutePath) {
      sl<NavigationBLoC>().add(const ToAddedProductsEvent());
    } else if (configuration is AdminRoutePath) {
      sl<NavigationBLoC>().add(const ToAdminNavigationEvent());
    } else if (configuration is LoginRoutePath) {
      sl<NavigationBLoC>().add(const ToLoginNavigationEvent());
    } else if (configuration is MyWidgetsRoutePath) {
      sl<NavigationBLoC>().add(const ToMyWidgetsNavigationEvent());
    } else if (configuration is DepositFailedRoutePath) {
      sl<NavigationBLoC>().add(const ToDepositFailedNavigationEvent());
    } else if (configuration is DepositSuccessRoutePath) {
      sl<NavigationBLoC>().add(const ToDepositSuccessNavigationEvent());
    } else if (configuration is CoinDepositRoutePath) {
      sl<NavigationBLoC>().add(const ToCoinDepositNavigationEvent());
    } else if (configuration is StripeDepositRoutePath) {
      sl<NavigationBLoC>().add(const ToStripeDepositNavigationEvent());
    } else if (configuration is CategoryRoutePath) {
      sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
    } else if (configuration is ProductDetailsRoutePath) {
      debugPrint('go to details ${configuration.urlPath}');
      sl<NavigationBLoC>()
          .add(ToProductDetailsNavigationEvent(configuration.urlPath));
    }
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => wiredashKey;
}
