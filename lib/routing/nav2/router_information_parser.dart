import 'package:flutter/widgets.dart';
import 'package:orchestra/blocs/bloc/auth_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/models/user.dart';
import 'package:orchestra/routing/nav2/routes.dart';
import 'package:orchestra/routing/nav2/ui_pages.dart';

class OrchestraRouteInformationParser
    extends RouteInformationParser<OrchestraRoutePath> {
  OrchestraRouteInformationParser(this.currentUser);
  final UserModel currentUser;

  @override
  Future<OrchestraRoutePath> parseRouteInformation(
      RouteInformation routeInfo) async {
    final uri = Uri.parse(routeInfo.location!);
    final locationString = routeInfo.location;
    debugPrint('parseRouteInformation uri $uri');

    // Handle '/'
    if (uri.pathSegments.isEmpty) {
      return MarketplaceRoutePath();
    } else {
      if (locationString == Pages.dashboard.urlPath &&
          currentUser != UserModel.empty) {
        return DashboardRoutePath();
      } else if (locationString == Pages.marketplace.urlPath) {
        return MarketplaceRoutePath();
      } else if (locationString == Pages.profile.urlPath) {
      } else if (locationString == Pages.billings.urlPath &&
          currentUser != UserModel.empty) {
        return BillingsRoutePath();
      } else if (locationString == Pages.profile.urlPath &&
          currentUser != UserModel.empty) {
        return ProfileRoutePath();
      } else if (locationString == Pages.addProduct.urlPath &&
          currentUser != UserModel.empty) {
        return AddProductRoutePath();
      } else if (locationString == Pages.addedLore.urlPath &&
          currentUser != UserModel.empty) {
        return AddedProductsRoutePath();
      } else if (locationString == Pages.admin.urlPath &&
          currentUser != UserModel.empty) {
        return AdminRoutePath();
      } else if (locationString == Pages.login.urlPath) {
        if (sl<AuthBloc>().state is LoggedAuthState) {
          return MarketplaceRoutePath();
        }
        return LoginRoutePath();
      } else if (locationString == Pages.myLore.urlPath &&
          currentUser != UserModel.empty) {
        return MyWidgetsRoutePath();
      } else if (locationString == Pages.depositFailed.urlPath) {
        return DepositFailedRoutePath();
      } else if (locationString == Pages.depositSuccess.urlPath) {
        return DepositSuccessRoutePath();
      } else if (locationString == Pages.coinDeposit.urlPath) {
        return CoinDepositRoutePath();
      } else if (locationString == Pages.testLauncher.urlPath &&
          currentUser != UserModel.empty) {
        return TestLauncherRoutePath();
      } else if (locationString == Pages.stripeDeposit.urlPath) {
        return StripeDepositRoutePath();
      }

      /// Разбор пути на страницу категории ()
      else if (uri.pathSegments.length >= 3) {
/*         debugPrint('***1 ${uri.pathSegments[0]}');
        debugPrint('***1 ${uri.pathSegments[1]}');
        debugPrint('***1 ${uri.pathSegments[2]}');
 */
        if (uri.pathSegments[1] == 'categories') {
          return CategoryRoutePath(uri.pathSegments[2]);
        }
      }

      /// Разбор пути на страницу детализации продукта
      else if (uri.pathSegments.length >= 2) {
        if (uri.pathSegments[0] ==
            Pages.marketplace.urlPath.replaceAll(RegExp(r'/'), '')) {
          debugPrint('0000001 ${uri.pathSegments[1]}');
          return ProductDetailsRoutePath(productId: uri.pathSegments[1]);
        }
      }
    }
    return MarketplaceRoutePath();
  }

  @override
  RouteInformation restoreRouteInformation(OrchestraRoutePath configuration) {
    debugPrint('restoreRouteInformation ${configuration.urlPath}');
    return RouteInformation(location: configuration.urlPath);
  }
}
