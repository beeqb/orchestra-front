import 'package:orchestra/routing/nav2/ui_pages.dart';

abstract class OrchestraRoutePath {
  final String urlPath;
  const OrchestraRoutePath() : urlPath = '';
}

class MarketplaceRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  MarketplaceRoutePath() : urlPath = Pages.marketplace.urlPath;
}

class ProductDetailsRoutePath extends OrchestraRoutePath {
  final String productId;
  @override
  final String urlPath;
  ProductDetailsRoutePath({required this.productId})
      : urlPath = '${Pages.productDetails.urlPath}$productId';
}

class ProductDetailsUseRoutePath extends OrchestraRoutePath {
  final String productId;
  @override
  final String urlPath;
  ProductDetailsUseRoutePath({required this.productId})
      : urlPath = '${Pages.productDetails.urlPath}$productId+?use';
}

class CategoryRoutePath extends OrchestraRoutePath {
  String categoryType;
  @override
  final String urlPath;
  CategoryRoutePath(this.categoryType)
      : urlPath =
            '${Pages.category.urlPath}${categoryType.replaceAll(RegExp(r' '), '_')}';
}

class DashboardRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  DashboardRoutePath() : urlPath = Pages.dashboard.urlPath;
}

class BillingsRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  BillingsRoutePath() : urlPath = Pages.billings.urlPath;
}

class MyWidgetsRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  MyWidgetsRoutePath() : urlPath = Pages.myLore.urlPath;
}

/// EditProductRoutePath & AddProductRoutePath use one widget AddProductPage
class EditProductRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  EditProductRoutePath() : urlPath = Pages.addProduct.urlPath;
}

class AddProductRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  AddProductRoutePath() : urlPath = Pages.addProduct.urlPath;
}

class AddedProductsRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  AddedProductsRoutePath() : urlPath = Pages.addedLore.urlPath;
}

class AdminRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  AdminRoutePath() : urlPath = Pages.admin.urlPath;
}

class LoginRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  LoginRoutePath() : urlPath = Pages.login.urlPath;
}

class ProfileRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  ProfileRoutePath() : urlPath = Pages.profile.urlPath;
}

class OnboardingRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  OnboardingRoutePath() : urlPath = Pages.onboarding.urlPath;
}

class TestLauncherRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  TestLauncherRoutePath() : urlPath = Pages.testLauncher.urlPath;
}

class StripeDepositRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  StripeDepositRoutePath() : urlPath = Pages.stripeDeposit.urlPath;
}

class CoinDepositRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  CoinDepositRoutePath() : urlPath = Pages.coinDeposit.urlPath;
}

class DepositSuccessRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  DepositSuccessRoutePath() : urlPath = Pages.depositSuccess.urlPath;
}

class DepositFailedRoutePath extends OrchestraRoutePath {
  @override
  final String urlPath;
  DepositFailedRoutePath() : urlPath = Pages.depositFailed.urlPath;
}
