enum Pages {
  onboarding,
  marketplace,
  dashboard,
  billings,
  profile,
  addProduct,
  addedLore,
  admin,
  login,
  myLore,
  depositSuccess,
  depositFailed,
  stripeDeposit,
  coinDeposit,
  productDetails,
  category,
  testLauncher,
}

extension PagesExtension on Pages {
  String get urlPath {
    switch (this) {
      case Pages.onboarding:
        return '/onboarding';
      case Pages.marketplace:
        return '/marketplace';
      case Pages.productDetails:
        return '/marketplace/';
      case Pages.category:
        return '/marketplace/categories/';
      case Pages.dashboard:
        return '/dashboard';
      case Pages.billings:
        return '/billings';
      case Pages.profile:
        return '/profile';
      case Pages.addProduct:
        return '/addProduct';
      case Pages.addedLore:
        return '/addedLore';
      case Pages.admin:
        return '/admin';
      case Pages.login:
        return '/login';
      case Pages.myLore:
        return '/myLore';
      case Pages.depositSuccess:
        return '/depositSuccess';
      case Pages.depositFailed:
        return '/depositFailed';
      case Pages.stripeDeposit:
        return '/stripeDeposit';
      case Pages.coinDeposit:
        return '/coinDeposit';
      case Pages.testLauncher:
        return '/testLauncher';
      default:
        return '/unknown';
    }
  }
}
