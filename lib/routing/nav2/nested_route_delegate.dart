import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/pages/add_lore_page.dart';
import 'package:orchestra/pages/admin_page.dart';
import 'package:orchestra/pages/applications_page.dart';
import 'package:orchestra/pages/category_page.dart';
import 'package:orchestra/pages/coin_deposit.dart';
import 'package:orchestra/pages/deposit_failed.dart';
import 'package:orchestra/pages/deposit_success.dart';
import 'package:orchestra/pages/login.dart';
import 'package:orchestra/pages/marketplace_page.dart';
import 'package:orchestra/pages/my_lore_page.dart';
import 'package:orchestra/pages/my_walllet_page.dart';
import 'package:orchestra/pages/product_page.dart';
import 'package:orchestra/pages/profile.dart';
import 'package:orchestra/pages/statistics_page.dart';
import 'package:orchestra/pages/stripe_deposit.dart';
import 'package:orchestra/widgets/admin_widgets/launcher_widget.dart';
import 'package:orchestra/widgets/loading.dart';

import '../../blocs/bloc/root_bloc.dart';
import '../../blocs/cubits/settings_cubit.dart';
import '../../services/app_service.dart';
import '../../widgets/applications_widgets/application_tile/action_panel/applications_settings_sheet_layout.dart';
import 'routes.dart';

class NestedRouterDelegate extends RouterDelegate<OrchestraRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<OrchestraRoutePath> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;
  List<Page> pages = [];
  final NavigationBLoC _navigationBLoC;
  StreamSubscription? _subscription;

  NestedRouterDelegate({required this.navigatorKey})
      : _navigationBLoC = sl<NavigationBLoC>() {
    _subscription = _navigationBLoC.stream.listen((state) {
      if (state != sl<NavigationBLoC>().state) {
        sl<NavigationBLoC>().add(const ToMyWidgetsNavigationEvent());
        notifyListeners();
      }
    });
  }

  @override
  void dispose() {
    _subscription!.cancel();
    super.dispose();
  }

  void addPage(MaterialPage page) {
    pages.removeWhere((element) => element.key == page.key);
    pages.add(page);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationBLoC, NavigationState>(
      builder: (context, state) {
        return Navigator(
          key: navigatorKey,
          pages: [
            state.when(
              initial: (path) {
                const page = MaterialPage(
                  key: ValueKey('MarketplacePage'),
                  child: MarketplacePage(),
                );

                addPage(page);
                return page;
              },
              marketplace: (path) {
                const page = MaterialPage(
                  key: ValueKey('MarketplacePage'),
                  child: MarketplacePage(),
                );

                addPage(page);
                return page;
              },
              productDetails: (path, prodId) {
                final page = MaterialPage(
                  key: ValueKey('ProductDetailsPage-$prodId'),
                  child: ProductPage(
                    productId: prodId,
                  ),
                );

                addPage(page);
                return page;
              },
              dashboard: (path) {
                const page = MaterialPage(
                  key: ValueKey('DashboardPage'),
                  child: ApplicationPage(),
                );

                addPage(page);
                return page;
              },
              billings: (path) {
                const page = MaterialPage(
                  key: ValueKey('BillingsPage'),
                  child: BillingsPage(),
                );

                addPage(page);
                return page;
              },
              login: (path) {
                const page = MaterialPage(
                  key: ValueKey('LoginPage'),
                  child: LoginPage(),
                );

                addPage(page);
                return page;
              },
              profile: (path) {
                const page = MaterialPage(
                  key: ValueKey('ProfilePage'),
                  child: ProfilePage(),
                );

                addPage(page);
                return page;
              },
              myWidgets: (path) {
                const page = MaterialPage(
                  key: ValueKey('MyLorePage'),
                  child: MyLorePage(),
                );

                addPage(page);
                return page;
              },
              admin: (path) {
                const page = MaterialPage(
                  key: ValueKey('AdminPage'),
                  child: AdminPage(),
                );

                addPage(page);
                return page;
              },
              addProduct: (path) {
                const page = MaterialPage(
                  key: ValueKey('AddProductPage'),
                  child: AddLorePage(null),
                );

                return page;
              },
              editProduct: (path, product) {
                final page = MaterialPage(
                  key: const ValueKey('EditProductPage'),
                  child: AddLorePage(
                    product,
                  ),
                );

                addPage(page);
                return page;
              },
              addedProducts: (path) {
                const page = MaterialPage(
                  key: ValueKey('AddedProductsPage'),
                  child: StatisticsPage(),
                );

                addPage(page);
                return page;
              },
              depositFailed: (path) {
                const page = MaterialPage(
                  key: ValueKey('DepositFailedPage'),
                  child: DepositFailedPage(),
                );

                addPage(page);
                return page;
              },
              depositSuccess: (path) {
                const page = MaterialPage(
                  key: ValueKey('DepositSuccessPage'),
                  child: DepositSuccessPage(),
                );

                addPage(page);
                return page;
              },
              coinDeposit: (path) {
                const page = MaterialPage(
                  key: ValueKey('CoinDepositPage'),
                  child: CoinDepositPage(),
                );

                addPage(page);
                return page;
              },
              stripeDeposit: (path) {
                const page = MaterialPage(
                  key: ValueKey('StripeDepositPage'),
                  child: StripeDepositPage(),
                );

                addPage(page);
                return page;
              },
              testLauncher: (path, product) {
                final page = MaterialPage(
                  key: const ValueKey('LauncherWidget'),
                  child: TestLauncherPage(product: product),
                );

                addPage(page);
                return page;
              },
              category: (routePath, categoryType) {
                final page = MaterialPage(
                  child: CategoryPage(
                    categoryType: categoryType,
                    key: ValueKey('CategoryType$categoryType'),
                  ),
                );

                addPage(page);
                return page;
              },
              productDetailsUse:
                  (OrchestraRoutePath routePath, String productId, product) {
                final page = MaterialPage(
                    key: const ValueKey('ProductUse'),
                    child: BlocBuilder<RootBloc, RootState>(
                      builder: (context, state) {
                        if (state is ReadyRootState) {
                          final _apps = state.apps;
                          if (_apps.isNotEmpty) {
                            _apps.sort((a, b) => DateTime.parse(b.createdAt!)
                                .compareTo(DateTime.parse(a.createdAt!)));
                          }
                          return BlocProvider<SettingsCubit>(
                            create: (context) =>
                                SettingsCubit(appService: sl<AppService>())
                                  ..readySettings(product.id),
                            child: SettingsSheetLayout(
                              _apps.first,
                              product,
                              fromAppView: false,
                            ),
                          );
                        }
                        return const Scaffold(body: Loading());
                      },
                    ));

                addPage(page);
                return page;
              },
            )
          ],
          onPopPage: (route, result) {
            // check if route removed
            if (route.didPop(result)) {
              // remove the last page
              pages.remove(route.settings);
              if (pages.last.key == const ValueKey('MyLorePage')) {
                sl<NavigationBLoC>().add(const ToMyWidgetsNavigationEvent());
              }
              if (pages.last.key == const ValueKey('MarketplacePage')) {
                sl<NavigationBLoC>().add(const ToMarketplaceNavigationEvent());
              }
              if (pages.last.key == const ValueKey('DashboardPage')) {
                sl<NavigationBLoC>().add(const ToDashboardNavigationEvent());
              }
              if (pages.last.key == const ValueKey('BillingsPage')) {
                sl<NavigationBLoC>().add(const ToBillingsNavigationEvent());
              }
              if (pages.last.key == const ValueKey('AddedProductsPage')) {
                sl<NavigationBLoC>().add(const ToAddedProductsEvent());
              }
              if (pages.last.key.toString().contains('ProductDetailsPage')) {
                sl<NavigationBLoC>().add(ToProductDetailsNavigationEvent(
                    pages.last.key.toString().split('-')[1].split("'")[0]));
              }
              return true;
            }
            return false;
          },
        );
      },
    );
  }

  @override
  Future<void> setNewRoutePath(OrchestraRoutePath configuration) async {
    assert(false);
  }
}

// extension UniquePages<Page, LocalKey> on List<Page> {
//   List<Page> unique(
//       [LocalKey Function(Page element)? id, bool inplace = true]) {
//     final ids = <LocalKey>{};
//     var list = List<Page>.from(this);
//     list.retainWhere((x) => ids.add());
//     return list;
//   }
// }
