import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/widgets/dialogs/edit_widget_dialog.dart';
import 'package:orchestra/widgets/dialogs/welcome_bonus_dialog.dart';

import '../main.dart';
import '../models/dialog_model.dart';
import '../models/enums/enums.dart';
import '../services/dialogs_service.dart';
import '../widgets/dialogs/app_result_dialog.dart';
import '../widgets/dialogs/comment_info_dialog.dart';
import '../widgets/dialogs/custom_dialog.dart';
import '../widgets/dialogs/test_fly_gone_dialog.dart';

class DialogManager extends StatefulWidget {
  /// для управления показом диалогов и снекбаров
  const DialogManager({
    Key? key,
    required this.child,
    required this.dialogsService,
  }) : super(key: key);

  /// Для поиска _$NAME$State в контексте
  static _DialogManagerState? of(BuildContext context) =>
      context.findAncestorStateOfType<_DialogManagerState>();

  final Widget child;
  final DialogsService dialogsService;

  @override
  _DialogManagerState createState() => _DialogManagerState();
}

class _DialogManagerState extends State<DialogManager> {
  @override
  void initState() {
    super.initState();
    widget.dialogsService.registerDialogListener(_showDialog);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  void _showDialog(DialogRequest request) {
    switch (request.dialogType) {
      //=============================================================
      case DialogType.commentDetailsDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CommentInfoDialog(
                title: request.title,
                ratingItem: request.ratingItem!,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.appResultDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: SingleChildScrollView(
                child: AppResultDialog(
                  title: request.title,
                  result: request.description,
                ),
              ),
            );
          },
        );
        break;
      //=============================================================
      case DialogType.alertDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CustomDialog(
                content: request.description,
                title: request.title,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.warningDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CustomDialog(
                content: request.description,
                title: request.title,
                type: DialogType.warningDialog,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.editWidgetDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: EditWidgetDialog(
                content: request.description,
                title: request.title,
                type: DialogType.warningDialog,
              ),
            );
          },
        );
        break;
      //=============================================================
      case DialogType.noInternetDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CustomDialog(
                content: request.description,
                title: request.title,
                type: DialogType.noInternetDialog,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.errorDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CustomDialog(
                title: request.title,
                content: request.description,
                type: DialogType.errorDialog,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.successDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CustomDialog(
                content: request.description,
                type: DialogType.successDialog,
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: true)));
        break;
      //=============================================================
      case DialogType.testFlyGoneDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: TestFlyGoneDialog(
                product: request.product!,
                onCancel: () {
                  widget.dialogsService
                      .dialogComplete(DialogResponse(confirmed: false));
                  sl<NavigationBLoC>().add(
                      ToProductDetailsNavigationEvent(request.product!.id));
                },
                onPressed: () {
                  widget.dialogsService
                      .dialogComplete(DialogResponse(confirmed: true));
                  Navigator.of(context).pop();
                },
              ),
            );
          },
        ).then((value) => widget.dialogsService
            .dialogComplete(DialogResponse(confirmed: false)));
        break;
      //=============================================================
      //=============================================================
      case DialogType.welcomeDialog:
        showDialog(
          context: context,
          builder: (context) {
            return Center(
              child: WelcomeBonusDialog(
                type: DialogType.infoDialog,
                title: 'Welcome bonus',
                icon: Icon(
                  MdiIcons.giftOutline,
                  color: Colors.yellow[800],
                ),
                content: 'Register now and get \$2 as gift',
              ),
            );
          },
        );
        break;
      //=============================================================
      case DialogType.successSnackbar:
        final snackBar = SnackBar(
            duration: const Duration(milliseconds: 3000),
            backgroundColor: DialogType.successSnackbar.color,
            content: Text(
              request.title,
              style: Theme.of(context).textTheme.bodyMedium,
            ));
        ScaffoldMessenger.of(wiredashKey.currentContext!)
            .showSnackBar(snackBar);
        break;
      //=============================================================
      case DialogType.errorSnackbar:
        final snackBar = SnackBar(
            duration: const Duration(milliseconds: 3000),
            backgroundColor: DialogType.errorSnackbar.color,
            content: Text(
              request.title,
              style: Theme.of(context).textTheme.bodyMedium,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        break;
      default:
        const snackBar = SnackBar(
            duration: Duration(milliseconds: 3000),
            backgroundColor: Colors.red,
            content: Text('DialogType not found'));
        ScaffoldMessenger.of(wiredashKey.currentContext!)
            .showSnackBar(snackBar);
    }
  }
}
