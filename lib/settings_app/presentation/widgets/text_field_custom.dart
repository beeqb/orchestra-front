// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/settings_app/domain/bloc/settings_bloc.dart';

class TextFieldCustom extends StatelessWidget {
  const TextFieldCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: const InputDecoration(
        hintText: 'Search',
        prefixIcon: const Icon(Icons.search),
      ),
      onChanged: (query) {
        context.read<SettingsBloc>().add(SerchSettingsEvent(query));
      },
    );
  }
}
