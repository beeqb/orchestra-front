import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/blocs/bloc/log_screen_bloc.dart';
import 'package:orchestra/blocs/bloc/navigation_bloc.dart';
import 'package:orchestra/blocs/bloc/root_bloc.dart';
import 'package:orchestra/blocs/cubits/settings_cubit.dart';
import 'package:orchestra/injection_container.dart';
import 'package:orchestra/models/app.dart';
import 'package:orchestra/models/enums/manifest_input_type.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/settings_model.dart';
import 'package:orchestra/settings_app/domain/bloc/settings_bloc.dart';

// ignore: must_be_immutable
class DesktopDynamicFormSettings extends StatefulWidget {
  DesktopDynamicFormSettings({
    Key? key,
    this.sendSettings,
    required this.settings,
    this.app,
    this.fromAppView,
    required this.oldSettings,
    required this.product,
    required this.indexList,
  }) : super(key: key);
  List<int> indexList;
  final List<SettingsModel> settings;
  final String oldSettings;
  final AppModel? app;
  final Product product;
  final bool? fromAppView;
  final Function(Map<String, dynamic>)? sendSettings;

  @override
  _DesktopDynamicFormSettingsState createState() =>
      _DesktopDynamicFormSettingsState();
}

class _DesktopDynamicFormSettingsState
    extends State<DesktopDynamicFormSettings> {
  List<Widget> _generateWidgets() {
    return List.generate(
      widget.settings.length,
      (index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.settings.elementAt(index).name,
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                  color: Theme.of(context).popupMenuTheme.textStyle?.color)),
          const SizedBox(
            height: 10,
          ),
          _buildSettingsForm(
            widget.settings[index],
            index,
          ),
          const SizedBox(
            height: 22,
          )
        ],
      ),
    );
  }

  List<Widget> _filterList(List<Widget> widgetList) {
    var filterList = <Widget>[];
    for (var i = 0; i < widgetList.length; i++) {
      for (var j = 0; j < widget.indexList.length; j++) {
        if (i == widget.indexList[j]) {
          filterList.add(widgetList[i]);
        }
      }
    }
    return filterList;
  }

  @override
  Widget build(BuildContext context) {
    var listWidgets = _generateWidgets();
    return Column(
      children: [
        Column(children: _filterList(listWidgets)),
        const SizedBox(
          height: 24,
        ),
        buildButtonsPanel(),
      ],
    );
  }

  Widget _buildSettingsForm(SettingsModel setting, int position) {
    switch (setting.dataType) {
      case ManifestInputType.checkbox:
        return _buildCheckboxForm(setting, position);
      case ManifestInputType.radio:
        return _buildRadioForm(setting, position);
      case ManifestInputType.select:
        return _buildSelectButton(position, setting);
      case ManifestInputType.text:
        return _buildTextField(position);
      case ManifestInputType.textarea:
        return _buildTextAreaField(position);
      default:
        return const Text('Unknown data_type in Manifest');
    }
  }

  Widget _buildTextAreaField(int position) {
    return TextField(
      maxLines: 10,
      controller: context.read<SettingsBloc>().presets[position],
      style: Theme.of(context).textTheme.headlineMedium,
    );
  }

  Widget _buildTextField(int position) {
    return TextField(
      controller: context.read<SettingsBloc>().presets[position],
      style: Theme.of(context).textTheme.headlineMedium,
    );
  }

  Widget _buildSelectButton(int position, SettingsModel setting) {
    var labels = setting.labels ?? {};
    var itemsDrop = labels.entries.map<String>((e) => e.value).toList();
    return DropdownSearch<String>(
        mode: Mode.MENU,
        showSearchBox: (itemsDrop.length > 10),
        items: itemsDrop,
        popupBackgroundColor: Theme.of(context).backgroundColor,
        onChanged: (newValue) async {
          setFocus();
          setState(() {
            context.read<SettingsBloc>().presets[position] = newValue;
            var key = labels.keys.firstWhere((k) => labels[k] == newValue);
            context.read<SettingsBloc>().keyPresets[position] = key;
          });
        },
        selectedItem: //stringToSettingTimeValue(
            context.read<SettingsBloc>().presets[position]
        //)
        //  .name,
        );
  }

  void setFocus() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  Widget _buildRadioForm(SettingsModel setting, int position) {
    var labels = setting.labels ?? {};
    return Wrap(
      children: List.generate(
          labels.length,
          (index) => RadioListTile<String>(
                title: Text(
                  labels.entries.elementAt(index).value,
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                value: labels.entries.elementAt(index).key,
                // groupValue: context.read<SettingsBloc>().presets[position],
                groupValue: context.read<SettingsBloc>().keyPresets[position],
                onChanged: (newValue) {
                  setFocus();
                  setState(() {
                    context.read<SettingsBloc>().keyPresets[position] =
                        newValue;
                    context.read<SettingsBloc>().presets[position] =
                        labels.values.firstWhere((v) => labels[newValue] == v);

                    // context.read<SettingsBloc>().presets[position] = newValue;
                    // context.read<SettingsBloc>().keyPresets[position] =
                    //     labels.keys.firstWhere((k) => labels[k] == newValue);
                  });
                },
              )),
    );
  }

  Widget _buildCheckboxForm(SettingsModel setting, int position) {
    var labels = setting.labels ?? {};
    return Wrap(
      children: List.generate(
        labels.length,
        (index) => CheckboxListTile(
          title: Text(
            labels.entries.elementAt(index).value,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          value: context
              .read<SettingsBloc>()
              .presets[position]
              .entries
              .elementAt(index)
              .value,
          onChanged: (value) {
            setFocus();
            context.read<SettingsBloc>().presets[position].update(
                context
                    .read<SettingsBloc>()
                    .presets[position]
                    .entries
                    .elementAt(index)
                    .key,
                (v) => value);
            setState(() {
              debugPrint(context
                  .read<SettingsBloc>()
                  .presets[position]
                  .entries
                  .elementAt(index)
                  .value);
              debugPrint(
                  'change _presets ${context.read<SettingsBloc>().presets[position]}');
            });
          },
        ),
      ),
    );
  }

  Widget buildButtonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: BorderSide(
              width: 1.0,
              color: Theme.of(context).errorColor,
            ),
          ),
          onPressed: () {
            ///проверка, аппка нулевая если виджет построен из админ панели
            ///тогда удалять не нужно
            if (widget.oldSettings.isEmpty && widget.app != null) {
              sl<RootBloc>().add(StartDeletingAppEvent(widget.app!));
            }
            // очистка лога в админке (страница настроек при запуске теста)
            sl<LogScreenBloc>().add(const ClearLogScreenEvent());
            sl.resetLazySingleton<LogScreenBloc>(instance: sl<LogScreenBloc>());
            // переход в админку
            //sl<NavigationBLoC>().add(ToAdminNavigationEvent());
            if (sl<NavigationBLoC>().state is TestLauncherNavigationState) {
              sl<NavigationBLoC>().add(const ToAdminNavigationEvent());
            }
            Navigator.pop(context);
          },
          child: SizedBox(
            width: 140,
            height: 48,
            child: Center(
              child: Text(
                'Cancel',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        ElevatedButton(
          onPressed: () {
            saveWidgetSettings();
            if (widget.app != null) {
              Navigator.pop(context);
              sl<NavigationBLoC>().add(const ToDashboardNavigationEvent());
            }
          },
          child: SizedBox(
              width: 140,
              height: 48,
              child: Center(
                  child: Text(
                'Save',
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Theme.of(context).backgroundColor),
              ))),
        ),
      ],
    );
  }

  void saveWidgetSettings() {
    debugPrint(context.read<SettingsBloc>().presets.length.toString());
    var newSettings = <String, dynamic>{};
    for (var i = 0; i < context.read<SettingsBloc>().presets.length; i++) {
      var _key = widget.settings[i].key;
      var dataType2 = widget.settings[i].dataType;
      if (dataType2 == ManifestInputType.checkbox) {
        final _map = Map.from(context.read<SettingsBloc>().presets[i]);
        final _list = _map.keys.where((e) => _map[e] == true).toList();
        newSettings.putIfAbsent(_key!, () => _list);
      } else if (dataType2 == ManifestInputType.text ||
          dataType2 == ManifestInputType.textarea) {
        newSettings.putIfAbsent(
            _key!,
            () => (context.read<SettingsBloc>().presets[i]
                    as TextEditingController)
                .text);
      } else {
        newSettings.putIfAbsent(
            _key!, () => context.read<SettingsBloc>().keyPresets[i]);
        // if (_key! == 'period' || _key == 'interval') {
        //   newSettings.putIfAbsent(_key,
        //       () => parseValueToKey(context.read<SettingsBloc>().presets[i]));
        // } else {
        //   newSettings.putIfAbsent(
        //       _key, () => context.read<SettingsBloc>().presets[i]);
        // }
      }
    }

    if (widget.app != null) {
      BlocProvider.of<SettingsCubit>(context).saveAppWidgetSettings(
        userId: widget.app!.userId,
        productId: widget.product.id,
        appId: widget.app!.id!,
        settings: newSettings,
      );
    } else {
      widget.sendSettings!(newSettings);
    }
  }
}

class MobileDynamicFormSettings extends StatefulWidget {
  List<int> indexList;
  final List<SettingsModel> settings;
  final String oldSettings;
  final AppModel? app;
  final Product product;
  final bool? fromAppView;
  final Function(Map<String, dynamic>)? sendSettings;

  MobileDynamicFormSettings({
    Key? key,
    this.sendSettings,
    required this.settings,
    this.app,
    this.fromAppView,
    required this.oldSettings,
    required this.product,
    required this.indexList,
  }) : super(key: key);

  @override
  _MobileDynamicFormSettingsState createState() =>
      _MobileDynamicFormSettingsState();
}

class _MobileDynamicFormSettingsState extends State<MobileDynamicFormSettings> {
  List<Widget> _generateWidgets() {
    return List.generate(
      widget.settings.length,
      (index) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(widget.settings.elementAt(index).name,
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                  color: Theme.of(context).popupMenuTheme.textStyle?.color)),
          const SizedBox(
            height: 10,
          ),
          _buildSettingsForm(
            widget.settings[index],
            index,
          ),
          const SizedBox(
            height: 22,
          )
        ],
      ),
    );
  }

  List<Widget> _filterList(List<Widget> widgetList) {
    var filterList = <Widget>[];
    for (var i = 0; i < widgetList.length; i++) {
      for (var j = 0; j < widget.indexList.length; j++) {
        if (i == widget.indexList[j]) {
          filterList.add(widgetList[i]);
        }
      }
    }
    return filterList;
  }

  @override
  Widget build(BuildContext context) {
    var listWidgets = _generateWidgets();
    return Column(
      children: [
        Column(children: _filterList(listWidgets)),
        buildButtonsPanel(),
      ],
    );
  }

  Widget _buildSettingsForm(SettingsModel setting, int position) {
    switch (setting.dataType) {
      case ManifestInputType.checkbox:
        return _buildCheckboxForm(setting, position);
      case ManifestInputType.radio:
        return _buildRadioForm(setting, position);
      case ManifestInputType.select:
        return _buildSelectButton(position, setting);
      case ManifestInputType.text:
        return _buildTextField(position);
      case ManifestInputType.textarea:
        return _buildTextAreaField(position);
      default:
        return const Text('Unknown data_type in Manifest');
    }
  }

  Widget _buildTextAreaField(int position) {
    return TextField(
      maxLines: 10,
      controller: context.read<SettingsBloc>().presets[position],
      style: Theme.of(context).textTheme.bodyMedium,
    );
  }

  Widget _buildTextField(int position) {
    return TextField(
      controller: context.read<SettingsBloc>().presets[position],
      style: Theme.of(context).textTheme.bodyMedium,
    );
  }

  Widget _buildSelectButton(int position, SettingsModel setting) {
    var labels = setting.labels ?? {};
    var itemsDrop = labels.entries.map<String>((e) => e.value).toList();
    return DropdownSearch<String>(
        mode: Mode.MENU,
        showSearchBox: (itemsDrop.length > 10),
        items: itemsDrop,
        popupBackgroundColor: Theme.of(context).backgroundColor,
        onChanged: (newValue) async {
          setFocus();
          setState(() {
            context.read<SettingsBloc>().presets[position] = newValue;
            var key = labels.keys.firstWhere((k) => labels[k] == newValue);
            context.read<SettingsBloc>().keyPresets[position] = key;
          });
        },
        selectedItem: //stringToSettingTimeValue(
            context.read<SettingsBloc>().presets[position]
        //)
        //  .name,
        );
  }

  void setFocus() {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  Widget _buildRadioForm(SettingsModel setting, int position) {
    var labels = setting.labels ?? {};
    return Wrap(
      children: List.generate(
          labels.length,
          (index) => RadioListTile<String>(
                title: Text(labels.entries.elementAt(index).value,
                    style: Theme.of(context).textTheme.bodyMedium),
                value: labels.entries.elementAt(index).key,
                // groupValue: context.read<SettingsBloc>().presets[position],
                groupValue: context.read<SettingsBloc>().keyPresets[position],
                onChanged: (newValue) {
                  setFocus();
                  setState(() {
                    context.read<SettingsBloc>().keyPresets[position] =
                        newValue;
                    context.read<SettingsBloc>().presets[position] =
                        labels.values.firstWhere((v) => labels[newValue] == v);

                    // context.read<SettingsBloc>().presets[position] = newValue;
                    // context.read<SettingsBloc>().keyPresets[position] =
                    //     labels.keys.firstWhere((k) => labels[k] == newValue);
                  });
                },
              )),
    );
  }

  Widget _buildCheckboxForm(SettingsModel setting, int position) {
    var labels = setting.labels ?? {};
    return Wrap(
      children: List.generate(
        labels.length,
        (index) => CheckboxListTile(
          title: Text(
            labels.entries.elementAt(index).value,
            style: Theme.of(context).textTheme.bodyMedium,
          ),
          value: context
              .read<SettingsBloc>()
              .presets[position]
              .entries
              .elementAt(index)
              .value,
          onChanged: (value) {
            setFocus();
            context.read<SettingsBloc>().presets[position].update(
                context
                    .read<SettingsBloc>()
                    .presets[position]
                    .entries
                    .elementAt(index)
                    .key,
                (v) => value);
            setState(() {
              debugPrint(context
                  .read<SettingsBloc>()
                  .presets[position]
                  .entries
                  .elementAt(index)
                  .value);
              debugPrint(
                  'change _presets ${context.read<SettingsBloc>().presets[position]}');
            });
          },
        ),
      ),
    );
  }

  Widget buildButtonsPanel() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              side: BorderSide(
                width: 1.0,
                color: Theme.of(context).errorColor,
              ),
            ),
            onPressed: () {
              ///проверка, аппка нулевая если виджет построен из админ панели
              ///тогда удалять не нужно
              if (widget.oldSettings.isEmpty && widget.app != null) {
                sl<RootBloc>().add(StartDeletingAppEvent(widget.app!));
              }
              // очистка лога в админке (страница настроек при запуске теста)
              sl<LogScreenBloc>().add(const ClearLogScreenEvent());
              sl.resetLazySingleton<LogScreenBloc>(
                  instance: sl<LogScreenBloc>());
              // переход в админку
              //sl<NavigationBLoC>().add(ToAdminNavigationEvent());
              if (sl<NavigationBLoC>().state is TestLauncherNavigationState) {
                sl<NavigationBLoC>().add(const ToAdminNavigationEvent());
              }
              Navigator.pop(context);
            },
            child: SizedBox(
              height: 40,
              child: Center(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Theme.of(context).errorColor),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(
          width: 12,
        ),
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              saveWidgetSettings();
              if (widget.app != null) {
                Navigator.pop(context);
                Navigator.pop(context);
                sl<NavigationBLoC>().add(const ToDashboardNavigationEvent());
              }
            },
            child: SizedBox(
                height: 40,
                child: Center(
                    child: Text(
                  'Save',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: Theme.of(context).backgroundColor),
                ))),
          ),
        ),
      ],
    );
  }

  void saveWidgetSettings() {
    debugPrint(context.read<SettingsBloc>().presets.length.toString());
    var newSettings = <String, dynamic>{};
    for (var i = 0; i < context.read<SettingsBloc>().presets.length; i++) {
      var _key = widget.settings[i].key;
      var dataType2 = widget.settings[i].dataType;
      if (dataType2 == ManifestInputType.checkbox) {
        final _map = Map.from(context.read<SettingsBloc>().presets[i]);
        final _list = _map.keys.where((e) => _map[e] == true).toList();
        newSettings.putIfAbsent(_key!, () => _list);
      } else if (dataType2 == ManifestInputType.text ||
          dataType2 == ManifestInputType.textarea) {
        newSettings.putIfAbsent(
            _key!,
            () => (context.read<SettingsBloc>().presets[i]
                    as TextEditingController)
                .text);
      } else {
        newSettings.putIfAbsent(
            _key!, () => context.read<SettingsBloc>().keyPresets[i]);
        // if (_key! == 'period' || _key == 'interval') {
        //   newSettings.putIfAbsent(_key,
        //       () => parseValueToKey(context.read<SettingsBloc>().presets[i]));
        // } else {
        //   newSettings.putIfAbsent(
        //       _key, () => context.read<SettingsBloc>().presets[i]);
        // }
      }
    }

    if (widget.app != null) {
      BlocProvider.of<SettingsCubit>(context).saveAppWidgetSettings(
        userId: widget.app!.userId,
        productId: widget.product.id,
        appId: widget.app!.id!,
        settings: newSettings,
      );
    } else {
      widget.sendSettings!(newSettings);
    }
  }
}
