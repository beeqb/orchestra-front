import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:orchestra/models/app.dart';
import 'package:orchestra/models/product/product.dart';
import 'package:orchestra/models/settings_model.dart';
import 'package:orchestra/settings_app/domain/bloc/settings_bloc.dart';
import 'package:orchestra/settings_app/domain/usecases/init_presets.dart';
import 'package:orchestra/settings_app/domain/usecases/serch_settings.dart';
import 'package:orchestra/settings_app/presentation/widgets/dynamic_form_settings.dart';
import 'package:orchestra/settings_app/presentation/widgets/text_field_custom.dart';
import 'package:orchestra/widgets/loading.dart';

import '../../../core/const.dart';

class FormSettings extends StatelessWidget {
  const FormSettings({
    Key? key,
    required this.settings,
    required this.oldSettings,
    this.app,
    required this.product,
    this.fromAppView,
    this.sendSettings,
  }) : super(key: key);

  final List<SettingsModel> settings;
  final String oldSettings;
  final AppModel? app;
  final Product product;
  final bool? fromAppView;
  final Function(Map<String, dynamic>)? sendSettings;

  @override
  Widget build(BuildContext context) {
    final isMobile =
        MediaQuery.of(context).size.width <= SidebarConstants.hideBreakpoint;
    return BlocProvider(
      create: (context) => SettingsBloc(
        serchSettings: SerchSettings(settings: settings),
        initPresets: InitPresets(
          oldSettings: oldSettings,
          productId: product.id,
        ),
      )..add(const SerchSettingsEvent('')),
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (settings.length > 10) const TextFieldCustom(),
            BlocBuilder<SettingsBloc, SettingsState>(
              builder: (context, state) {
                return state.when(initial: () {
                  return const Loading();
                }, loading: () {
                  return const Loading();
                }, failure: () {
                  return const Loading();
                }, empty: () {
                  return const Center(
                    child: Text('Settings not found'),
                  );
                }, loaded: (List<int> listIndex) {
                  return isMobile
                      ? MobileDynamicFormSettings(
                          app: app,
                          sendSettings: sendSettings,
                          fromAppView: fromAppView,
                          settings: settings,
                          oldSettings: oldSettings,
                          product: product,
                          indexList: listIndex,
                        )
                      : DesktopDynamicFormSettings(
                          app: app,
                          sendSettings: sendSettings,
                          fromAppView: fromAppView,
                          settings: settings,
                          oldSettings: oldSettings,
                          product: product,
                          indexList: listIndex,
                        );
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
