import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:orchestra/models/enums/manifest_input_type.dart';
import 'package:orchestra/models/settings_model.dart';

class InitPresets {
  final String oldSettings;
  final String productId;
  InitPresets({
    required this.oldSettings,
    required this.productId,
  });

  List<dynamic> call(
      {required List<SettingsModel> settings, bool isKey = false}) {
    var presets = <dynamic>[];
    for (var element in settings) {
      switch (element.dataType) {
        case ManifestInputType.select:
          presets.add(_selectType(element, isKey));
          break;
        case ManifestInputType.radio:
          presets.add(_radioValueType(element));
          break;
        case ManifestInputType.checkbox:
          presets.add(_checkBoxType(element));
          break;
        case ManifestInputType.text:
          presets.add(_textType(element));
          break;
        case ManifestInputType.textarea:
          presets.add(_textAreaType(element));
          break;
        default:
          presets.add('_');
          break;
      }
    }
    return presets;
  }

  dynamic _getOldSettings() {
    if (oldSettings.isNotEmpty) {
      List _old = json.decode(oldSettings);
      final Map _concreteOld =
          _old.firstWhere((e) => e['productId'] == productId);
      return _concreteOld['settings'];
    } else {
      return null;
    }
  }

  TextEditingController _textAreaType(SettingsModel model) {
    var _oldSettings = _getOldSettings();
    var _textArea = ' ';
    var controller = TextEditingController();
    // check old settings
    if (_oldSettings != null) {
      final _old = Map.from(_oldSettings);
      final _filteredOld = _old.entries.firstWhereOrNull(
        (element) => element.key == model.key,
      );
      if (_filteredOld != null) {
        _textArea = _filteredOld.value;
        controller.text = _textArea;
      }
    }
    return controller;
  }

  TextEditingController _textType(SettingsModel model) {
    var _oldSettings = _getOldSettings();
    var _text = ' ';
    var controller = TextEditingController();
    // check old settings
    if (_oldSettings != null) {
      final _old = Map.from(_oldSettings);
      final _filteredOld = _old.entries.firstWhereOrNull(
        (element) => element.key == model.key,
      );
      if (_filteredOld != null) {
        _text = _filteredOld.value;
        controller.text = _text;
      }
    }
    return controller;
  }

  String? _selectType(SettingsModel model, bool isKey) {
    var typeKey = model.key;
    var labels = model.labels ?? {};
    var _oldSettings = _getOldSettings();
    var _key = labels.entries.first.key;
    if (_oldSettings != null) {
      final _old = Map.from(_oldSettings);
      final _filteredOld = _old.entries.firstWhereOrNull(
        (element) => element.key == model.key,
      );
      if (_filteredOld != null && _filteredOld.value != null) {
        _key = _filteredOld.value;
        if (isKey) {
          return _key;
        } else {
          var value = labels[_key];
          return value;
        }
        // if (typeKey == 'period' || typeKey == 'interval') {
        //   return parseKeyToValue(_key);
        // } else {
        //   return _key;
        // }
      }
    } else {
      return null;
    }
  }

  String _radioValueType(SettingsModel model) {
    var labels = model.labels ?? {};
    var _oldSettings = _getOldSettings();
    if (_oldSettings != null) {
      final _old = Map.from(_oldSettings);
      final _filteredOld = _old.entries.firstWhereOrNull(
        (element) => element.key == model.key,
      );
      if (_filteredOld != null) {
        return _filteredOld.value;
      } else {
        return labels.entries.elementAt(0).key;
      }
    } else {
      return labels.entries.elementAt(0).key;
    }
  }

  Map<String, bool> _checkBoxType(SettingsModel model) {
    var _oldSettings = _getOldSettings();
    var labels = model.labels ?? {};

    final _labels = <String, bool>{};
    for (var f = 0; f < labels.length; f++) {
      _labels.putIfAbsent(
          labels.entries.elementAt(f).key, () => f == 0 ? true : false);
    }
    if (_oldSettings != null) {
      _labels.update(labels.keys.first, (value) => false);
      final _old = Map.from(_oldSettings);
      final _filteredOld = _old.entries.firstWhereOrNull(
        (element) => element.key == model.key,
      );
      if (_filteredOld != null) {
        List<String> _entryValues = _filteredOld.value.cast<String>();

        for (var e in _labels.entries) {
          for (var v in _entryValues) {
            if (v == e.key) {
              _labels.update(e.key, (value) => true);
            }
          }
        }
      }
    }
    return _labels;
  }
}
