import 'package:orchestra/models/settings_model.dart';

class SerchSettings {
  final List<SettingsModel> settings;

  SerchSettings({required this.settings});

  List<SettingsModel> call(String query) {
    if (query.isEmpty) {
      return settings;
    } else {
      var _searchList = <SettingsModel>[];
      for (var i = 0; i < settings.length; i++) {
        var name = settings.elementAt(i).name;
        if (name.toLowerCase().contains(query.toLowerCase())) {
          _searchList.add(settings.elementAt(i));
        }
      }
      return _searchList;
    }
  }

  List<int> callIndex(String query) {
    if (query.isEmpty) {
      return List.generate(settings.length, (index) => index);
    } else {
      var _searchIndex = <int>[];
      for (var i = 0; i < settings.length; i++) {
        var name = settings.elementAt(i).name;
        if (name.toLowerCase().contains(query.toLowerCase())) {
          _searchIndex.add(i);
        }
      }
      return _searchIndex;
    }
  }
}
