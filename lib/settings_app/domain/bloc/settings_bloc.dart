import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/settings_app/domain/usecases/init_presets.dart';
import 'package:orchestra/settings_app/domain/usecases/serch_settings.dart';

part 'settings_bloc.freezed.dart';

@freezed
class SettingsEvent with _$SettingsEvent {
  const SettingsEvent._();

  const factory SettingsEvent.serch(String query) = SerchSettingsEvent;
}

@freezed
class SettingsState with _$SettingsState {
  const SettingsState._();

  const factory SettingsState.initial() = InitialSettingsState;

  const factory SettingsState.loading() = LoadingSettingsState;

  const factory SettingsState.loaded(List<int> listIndex) = LoadedSettingsState;

  const factory SettingsState.failure() = FailureSettingsState;

  const factory SettingsState.empty() = EmptySettingsState;
}

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc({
    required this.serchSettings,
    required this.initPresets,
  }) : super(const InitialSettingsState()) {
    presets = initPresets.call(settings: serchSettings.call(''));
    keyPresets =
        initPresets.call(settings: serchSettings.call(''), isKey: true);
  }

  final SerchSettings serchSettings;
  final InitPresets initPresets;
  List<dynamic> presets = [];
  List<dynamic> keyPresets = [];

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) =>
      event.when<Stream<SettingsState>>(
        serch: _search,
      );

  Stream<SettingsState> _search(String query) async* {
    yield const LoadingSettingsState();

    try {
      var indexSettings = serchSettings.callIndex(query);
      if (indexSettings.isNotEmpty) {
        yield LoadedSettingsState(indexSettings);
      } else {
        yield const EmptySettingsState();
      }
    } catch (e) {
      yield const FailureSettingsState();
    }
  }
}
