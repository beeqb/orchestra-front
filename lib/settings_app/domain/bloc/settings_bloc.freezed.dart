// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'settings_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SettingsEvent {
  String get query => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) serch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String query)? serch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? serch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SerchSettingsEvent value) serch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SerchSettingsEvent value)? serch,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SerchSettingsEvent value)? serch,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SettingsEventCopyWith<SettingsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsEventCopyWith<$Res> {
  factory $SettingsEventCopyWith(
          SettingsEvent value, $Res Function(SettingsEvent) then) =
      _$SettingsEventCopyWithImpl<$Res>;
  $Res call({String query});
}

/// @nodoc
class _$SettingsEventCopyWithImpl<$Res>
    implements $SettingsEventCopyWith<$Res> {
  _$SettingsEventCopyWithImpl(this._value, this._then);

  final SettingsEvent _value;
  // ignore: unused_field
  final $Res Function(SettingsEvent) _then;

  @override
  $Res call({
    Object? query = freezed,
  }) {
    return _then(_value.copyWith(
      query: query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$SerchSettingsEventCopyWith<$Res>
    implements $SettingsEventCopyWith<$Res> {
  factory _$$SerchSettingsEventCopyWith(_$SerchSettingsEvent value,
          $Res Function(_$SerchSettingsEvent) then) =
      __$$SerchSettingsEventCopyWithImpl<$Res>;
  @override
  $Res call({String query});
}

/// @nodoc
class __$$SerchSettingsEventCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$SerchSettingsEventCopyWith<$Res> {
  __$$SerchSettingsEventCopyWithImpl(
      _$SerchSettingsEvent _value, $Res Function(_$SerchSettingsEvent) _then)
      : super(_value, (v) => _then(v as _$SerchSettingsEvent));

  @override
  _$SerchSettingsEvent get _value => super._value as _$SerchSettingsEvent;

  @override
  $Res call({
    Object? query = freezed,
  }) {
    return _then(_$SerchSettingsEvent(
      query == freezed
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SerchSettingsEvent extends SerchSettingsEvent {
  const _$SerchSettingsEvent(this.query) : super._();

  @override
  final String query;

  @override
  String toString() {
    return 'SettingsEvent.serch(query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SerchSettingsEvent &&
            const DeepCollectionEquality().equals(other.query, query));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(query));

  @JsonKey(ignore: true)
  @override
  _$$SerchSettingsEventCopyWith<_$SerchSettingsEvent> get copyWith =>
      __$$SerchSettingsEventCopyWithImpl<_$SerchSettingsEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) serch,
  }) {
    return serch(query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String query)? serch,
  }) {
    return serch?.call(query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? serch,
    required TResult orElse(),
  }) {
    if (serch != null) {
      return serch(query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(SerchSettingsEvent value) serch,
  }) {
    return serch(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(SerchSettingsEvent value)? serch,
  }) {
    return serch?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(SerchSettingsEvent value)? serch,
    required TResult orElse(),
  }) {
    if (serch != null) {
      return serch(this);
    }
    return orElse();
  }
}

abstract class SerchSettingsEvent extends SettingsEvent {
  const factory SerchSettingsEvent(final String query) = _$SerchSettingsEvent;
  const SerchSettingsEvent._() : super._();

  @override
  String get query;
  @override
  @JsonKey(ignore: true)
  _$$SerchSettingsEventCopyWith<_$SerchSettingsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SettingsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsStateCopyWith<$Res> {
  factory $SettingsStateCopyWith(
          SettingsState value, $Res Function(SettingsState) then) =
      _$SettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SettingsStateCopyWithImpl<$Res>
    implements $SettingsStateCopyWith<$Res> {
  _$SettingsStateCopyWithImpl(this._value, this._then);

  final SettingsState _value;
  // ignore: unused_field
  final $Res Function(SettingsState) _then;
}

/// @nodoc
abstract class _$$InitialSettingsStateCopyWith<$Res> {
  factory _$$InitialSettingsStateCopyWith(_$InitialSettingsState value,
          $Res Function(_$InitialSettingsState) then) =
      __$$InitialSettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialSettingsStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$InitialSettingsStateCopyWith<$Res> {
  __$$InitialSettingsStateCopyWithImpl(_$InitialSettingsState _value,
      $Res Function(_$InitialSettingsState) _then)
      : super(_value, (v) => _then(v as _$InitialSettingsState));

  @override
  _$InitialSettingsState get _value => super._value as _$InitialSettingsState;
}

/// @nodoc

class _$InitialSettingsState extends InitialSettingsState {
  const _$InitialSettingsState() : super._();

  @override
  String toString() {
    return 'SettingsState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialSettingsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class InitialSettingsState extends SettingsState {
  const factory InitialSettingsState() = _$InitialSettingsState;
  const InitialSettingsState._() : super._();
}

/// @nodoc
abstract class _$$LoadingSettingsStateCopyWith<$Res> {
  factory _$$LoadingSettingsStateCopyWith(_$LoadingSettingsState value,
          $Res Function(_$LoadingSettingsState) then) =
      __$$LoadingSettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingSettingsStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$LoadingSettingsStateCopyWith<$Res> {
  __$$LoadingSettingsStateCopyWithImpl(_$LoadingSettingsState _value,
      $Res Function(_$LoadingSettingsState) _then)
      : super(_value, (v) => _then(v as _$LoadingSettingsState));

  @override
  _$LoadingSettingsState get _value => super._value as _$LoadingSettingsState;
}

/// @nodoc

class _$LoadingSettingsState extends LoadingSettingsState {
  const _$LoadingSettingsState() : super._();

  @override
  String toString() {
    return 'SettingsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingSettingsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingSettingsState extends SettingsState {
  const factory LoadingSettingsState() = _$LoadingSettingsState;
  const LoadingSettingsState._() : super._();
}

/// @nodoc
abstract class _$$LoadedSettingsStateCopyWith<$Res> {
  factory _$$LoadedSettingsStateCopyWith(_$LoadedSettingsState value,
          $Res Function(_$LoadedSettingsState) then) =
      __$$LoadedSettingsStateCopyWithImpl<$Res>;
  $Res call({List<int> listIndex});
}

/// @nodoc
class __$$LoadedSettingsStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$LoadedSettingsStateCopyWith<$Res> {
  __$$LoadedSettingsStateCopyWithImpl(
      _$LoadedSettingsState _value, $Res Function(_$LoadedSettingsState) _then)
      : super(_value, (v) => _then(v as _$LoadedSettingsState));

  @override
  _$LoadedSettingsState get _value => super._value as _$LoadedSettingsState;

  @override
  $Res call({
    Object? listIndex = freezed,
  }) {
    return _then(_$LoadedSettingsState(
      listIndex == freezed
          ? _value._listIndex
          : listIndex // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ));
  }
}

/// @nodoc

class _$LoadedSettingsState extends LoadedSettingsState {
  const _$LoadedSettingsState(final List<int> listIndex)
      : _listIndex = listIndex,
        super._();

  final List<int> _listIndex;
  @override
  List<int> get listIndex {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_listIndex);
  }

  @override
  String toString() {
    return 'SettingsState.loaded(listIndex: $listIndex)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadedSettingsState &&
            const DeepCollectionEquality()
                .equals(other._listIndex, _listIndex));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_listIndex));

  @JsonKey(ignore: true)
  @override
  _$$LoadedSettingsStateCopyWith<_$LoadedSettingsState> get copyWith =>
      __$$LoadedSettingsStateCopyWithImpl<_$LoadedSettingsState>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) {
    return loaded(listIndex);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) {
    return loaded?.call(listIndex);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(listIndex);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class LoadedSettingsState extends SettingsState {
  const factory LoadedSettingsState(final List<int> listIndex) =
      _$LoadedSettingsState;
  const LoadedSettingsState._() : super._();

  List<int> get listIndex;
  @JsonKey(ignore: true)
  _$$LoadedSettingsStateCopyWith<_$LoadedSettingsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FailureSettingsStateCopyWith<$Res> {
  factory _$$FailureSettingsStateCopyWith(_$FailureSettingsState value,
          $Res Function(_$FailureSettingsState) then) =
      __$$FailureSettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$FailureSettingsStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$FailureSettingsStateCopyWith<$Res> {
  __$$FailureSettingsStateCopyWithImpl(_$FailureSettingsState _value,
      $Res Function(_$FailureSettingsState) _then)
      : super(_value, (v) => _then(v as _$FailureSettingsState));

  @override
  _$FailureSettingsState get _value => super._value as _$FailureSettingsState;
}

/// @nodoc

class _$FailureSettingsState extends FailureSettingsState {
  const _$FailureSettingsState() : super._();

  @override
  String toString() {
    return 'SettingsState.failure()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$FailureSettingsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) {
    return failure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) {
    return failure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) {
    return failure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) {
    return failure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) {
    if (failure != null) {
      return failure(this);
    }
    return orElse();
  }
}

abstract class FailureSettingsState extends SettingsState {
  const factory FailureSettingsState() = _$FailureSettingsState;
  const FailureSettingsState._() : super._();
}

/// @nodoc
abstract class _$$EmptySettingsStateCopyWith<$Res> {
  factory _$$EmptySettingsStateCopyWith(_$EmptySettingsState value,
          $Res Function(_$EmptySettingsState) then) =
      __$$EmptySettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$EmptySettingsStateCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$EmptySettingsStateCopyWith<$Res> {
  __$$EmptySettingsStateCopyWithImpl(
      _$EmptySettingsState _value, $Res Function(_$EmptySettingsState) _then)
      : super(_value, (v) => _then(v as _$EmptySettingsState));

  @override
  _$EmptySettingsState get _value => super._value as _$EmptySettingsState;
}

/// @nodoc

class _$EmptySettingsState extends EmptySettingsState {
  const _$EmptySettingsState() : super._();

  @override
  String toString() {
    return 'SettingsState.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$EmptySettingsState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<int> listIndex) loaded,
    required TResult Function() failure,
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
  }) {
    return empty?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<int> listIndex)? loaded,
    TResult Function()? failure,
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitialSettingsState value) initial,
    required TResult Function(LoadingSettingsState value) loading,
    required TResult Function(LoadedSettingsState value) loaded,
    required TResult Function(FailureSettingsState value) failure,
    required TResult Function(EmptySettingsState value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
  }) {
    return empty?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitialSettingsState value)? initial,
    TResult Function(LoadingSettingsState value)? loading,
    TResult Function(LoadedSettingsState value)? loaded,
    TResult Function(FailureSettingsState value)? failure,
    TResult Function(EmptySettingsState value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }
}

abstract class EmptySettingsState extends SettingsState {
  const factory EmptySettingsState() = _$EmptySettingsState;
  const EmptySettingsState._() : super._();
}
