// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      id: json['_id'] as String?,
      email: json['email'] as String?,
      name: json['name'] as String?,
      photoUrl: json['photoUrl'] as String?,
      apiKey: json['apiKey'] as String?,
      balance: json['balance'] as num?,
      received: json['received'] as num?,
      sended: json['sended'] as num?,
      deposited: json['deposited'] as num?,
      contributor: json['contributor'] as bool?,
      stats: json['stats'] == null
          ? null
          : UserStats.fromJson(json['stats'] as Map<String, dynamic>),
      isBlocked: json['isBlocked'] as bool?,
      address: json['address'] == null
          ? null
          : Address.fromJson(json['address'] as Map<String, dynamic>),
      companyName: json['companyName'] as String?,
      occupation: json['occupation'] as String?,
      phoneNo: json['phoneNo'] as String?,
      referralToken: json['referralToken'] as String?,
      referralUrl: json['referralUrl'] as String?,
      referralCount: json['referralCount'] as int?,
      referrerId: json['referrerId'] as String?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      provider: json['provider'] as String?,
      type: $enumDecodeNullable(_$UserTypeEnumMap, json['type']),
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('email', instance.email);
  writeNotNull('photoUrl', instance.photoUrl);
  writeNotNull('balance', instance.balance);
  writeNotNull('received', instance.received);
  writeNotNull('sended', instance.sended);
  writeNotNull('deposited', instance.deposited);
  writeNotNull('contributor', instance.contributor);
  writeNotNull('stats', instance.stats?.toJson());
  writeNotNull('isBlocked', instance.isBlocked);
  writeNotNull('address', instance.address?.toJson());
  writeNotNull('companyName', instance.companyName);
  writeNotNull('occupation', instance.occupation);
  writeNotNull('phoneNo', instance.phoneNo);
  writeNotNull('referralToken', instance.referralToken);
  writeNotNull('referralUrl', instance.referralUrl);
  writeNotNull('referralCount', instance.referralCount);
  writeNotNull('referrerId', instance.referrerId);
  val['createdAt'] = instance.createdAt;
  val['updatedAt'] = instance.updatedAt;
  writeNotNull('provider', instance.provider);
  writeNotNull('type', _$UserTypeEnumMap[instance.type]);
  val['apiKey'] = instance.apiKey;
  return val;
}

const _$UserTypeEnumMap = {
  UserType.admin: 'Admin',
  UserType.user: 'User',
};
