import 'dart:convert';

class BillingInfoModel {
  final String updatedAt;
  final num directIn;
  final num directOut;
  final num usageFees;
  final num purchases;
  final num profit;
  final num refIncome;
  final num sales;

  BillingInfoModel({
    required this.updatedAt,
    required this.directIn,
    required this.directOut,
    required this.usageFees,
    required this.purchases,
    required this.profit,
    required this.refIncome,
    required this.sales,
  });

  Map<String, dynamic> toJson() {
    return {
      'updatedAt': updatedAt,
      'directIn': directIn,
      'directOut': directOut,
      'usageFees': usageFees,
      'purchases': purchases,
      'profit': profit,
      'refIncome': refIncome,
      'sales': sales,
    };
  }

  factory BillingInfoModel.fromMap(Map<String, dynamic> map) {
    return BillingInfoModel(
      updatedAt: map['updatedAt'],
      directIn: map['directIn'],
      directOut: map['directOut'],
      usageFees: map['usageFees'],
      purchases: map['purchases'],
      profit: map['profit'],
      refIncome: map['refIncome'],
      sales: map['sales'],
    );
  }

  factory BillingInfoModel.fromJson(String source) =>
      BillingInfoModel.fromMap(json.decode(source));
}
