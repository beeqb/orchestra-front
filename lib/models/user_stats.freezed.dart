// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_stats.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserStats _$UserStatsFromJson(Map<String, dynamic> json) {
  return _UserStats.fromJson(json);
}

/// @nodoc
mixin _$UserStats {
  String? get updatedAt => throw _privateConstructorUsedError;
  num get costs => throw _privateConstructorUsedError;
  num get purchases => throw _privateConstructorUsedError;
  num get profit => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserStatsCopyWith<UserStats> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserStatsCopyWith<$Res> {
  factory $UserStatsCopyWith(UserStats value, $Res Function(UserStats) then) =
      _$UserStatsCopyWithImpl<$Res>;
  $Res call({String? updatedAt, num costs, num purchases, num profit});
}

/// @nodoc
class _$UserStatsCopyWithImpl<$Res> implements $UserStatsCopyWith<$Res> {
  _$UserStatsCopyWithImpl(this._value, this._then);

  final UserStats _value;
  // ignore: unused_field
  final $Res Function(UserStats) _then;

  @override
  $Res call({
    Object? updatedAt = freezed,
    Object? costs = freezed,
    Object? purchases = freezed,
    Object? profit = freezed,
  }) {
    return _then(_value.copyWith(
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      costs: costs == freezed
          ? _value.costs
          : costs // ignore: cast_nullable_to_non_nullable
              as num,
      purchases: purchases == freezed
          ? _value.purchases
          : purchases // ignore: cast_nullable_to_non_nullable
              as num,
      profit: profit == freezed
          ? _value.profit
          : profit // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
abstract class _$$_UserStatsCopyWith<$Res> implements $UserStatsCopyWith<$Res> {
  factory _$$_UserStatsCopyWith(
          _$_UserStats value, $Res Function(_$_UserStats) then) =
      __$$_UserStatsCopyWithImpl<$Res>;
  @override
  $Res call({String? updatedAt, num costs, num purchases, num profit});
}

/// @nodoc
class __$$_UserStatsCopyWithImpl<$Res> extends _$UserStatsCopyWithImpl<$Res>
    implements _$$_UserStatsCopyWith<$Res> {
  __$$_UserStatsCopyWithImpl(
      _$_UserStats _value, $Res Function(_$_UserStats) _then)
      : super(_value, (v) => _then(v as _$_UserStats));

  @override
  _$_UserStats get _value => super._value as _$_UserStats;

  @override
  $Res call({
    Object? updatedAt = freezed,
    Object? costs = freezed,
    Object? purchases = freezed,
    Object? profit = freezed,
  }) {
    return _then(_$_UserStats(
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      costs: costs == freezed
          ? _value.costs
          : costs // ignore: cast_nullable_to_non_nullable
              as num,
      purchases: purchases == freezed
          ? _value.purchases
          : purchases // ignore: cast_nullable_to_non_nullable
              as num,
      profit: profit == freezed
          ? _value.profit
          : profit // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserStats implements _UserStats {
  _$_UserStats(
      {this.updatedAt, this.costs = 0, this.purchases = 0, this.profit = 0});

  factory _$_UserStats.fromJson(Map<String, dynamic> json) =>
      _$$_UserStatsFromJson(json);

  @override
  final String? updatedAt;
  @override
  @JsonKey()
  final num costs;
  @override
  @JsonKey()
  final num purchases;
  @override
  @JsonKey()
  final num profit;

  @override
  String toString() {
    return 'UserStats(updatedAt: $updatedAt, costs: $costs, purchases: $purchases, profit: $profit)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserStats &&
            const DeepCollectionEquality().equals(other.updatedAt, updatedAt) &&
            const DeepCollectionEquality().equals(other.costs, costs) &&
            const DeepCollectionEquality().equals(other.purchases, purchases) &&
            const DeepCollectionEquality().equals(other.profit, profit));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(updatedAt),
      const DeepCollectionEquality().hash(costs),
      const DeepCollectionEquality().hash(purchases),
      const DeepCollectionEquality().hash(profit));

  @JsonKey(ignore: true)
  @override
  _$$_UserStatsCopyWith<_$_UserStats> get copyWith =>
      __$$_UserStatsCopyWithImpl<_$_UserStats>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserStatsToJson(
      this,
    );
  }
}

abstract class _UserStats implements UserStats {
  factory _UserStats(
      {final String? updatedAt,
      final num costs,
      final num purchases,
      final num profit}) = _$_UserStats;

  factory _UserStats.fromJson(Map<String, dynamic> json) =
      _$_UserStats.fromJson;

  @override
  String? get updatedAt;
  @override
  num get costs;
  @override
  num get purchases;
  @override
  num get profit;
  @override
  @JsonKey(ignore: true)
  _$$_UserStatsCopyWith<_$_UserStats> get copyWith =>
      throw _privateConstructorUsedError;
}
