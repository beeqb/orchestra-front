// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_activity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CustomerActivity _$$_CustomerActivityFromJson(Map<String, dynamic> json) =>
    _$_CustomerActivity(
      testFly: json['testFly'] as int,
      buy: json['buy'] as int,
      cr: (json['cr'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_CustomerActivityToJson(_$_CustomerActivity instance) =>
    <String, dynamic>{
      'testFly': instance.testFly,
      'buy': instance.buy,
      'cr': instance.cr,
    };
