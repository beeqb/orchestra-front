import 'package:freezed_annotation/freezed_annotation.dart';

part 'daily_sale.freezed.dart';
part 'daily_sale.g.dart';

@freezed
class DailySales with _$DailySales {
  const factory DailySales({required String date, required int sales}) =
      _DailySale;

  factory DailySales.fromJson(Map<String, Object?> json) =>
      _$DailySalesFromJson(json);
}
