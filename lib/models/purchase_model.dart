import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:orchestra/models/product/product.dart';

part 'purchase_model.g.dart';

@JsonSerializable(explicitToJson: true)
class PurchaseModel extends Equatable {
  final String userId;
  final bool activeTest;
  final String createdAt;
  final String updatedAt;
  final int launches;
  final String paidTill;
  final Product product;
  final int runTime;

  const PurchaseModel({
    required this.userId,
    required this.activeTest,
    required this.createdAt,
    required this.updatedAt,
    required this.launches,
    required this.paidTill,
    required this.product,
    required this.runTime,
  });

  factory PurchaseModel.fromJson(Map<String, dynamic> json) =>
      _$PurchaseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PurchaseModelToJson(this);

  bool get isPaid {
    final expiredDate = DateTime.tryParse(paidTill);
    if (expiredDate != null) {
      final time = DateTime.now().difference(expiredDate);
      return time.isNegative;
    } else {
      return false;
    }
  }

  bool get isActive => isPaid || activeTest;

  Map<String, int>? get testTerms {
    final testEndsAfter = product.pricingAndBusiness.testEndsAfter;
    if (testEndsAfter != null) {
      for (final e in testEndsAfter) {
        final isDays = e.endsWith('d');
        if (isDays) {
          final dayNumber = int.tryParse(e.split('d').first);
          if (dayNumber != null) {
            return {'days': dayNumber};
          }
        } else {
          final launchNumber = int.tryParse(e.split('l').first);
          if (launchNumber != null) {
            return {'launches': launchNumber};
          }
        }
      }
    }
    return null;
  }

  String get subscriptionTimeStatus {
    DateTime? expiredDate;
    Duration timeLeft;

    if (activeTest) {
      final testTerms = this.testTerms;
      if (testTerms != null) {
        if ((testTerms.containsKey('launches'))) {
          final launchesLeft = testTerms['launches']! - launches;
          return 'Expires in $launchesLeft launches';
        }
      }
    }
    expiredDate = DateTime.tryParse(paidTill);
    if (expiredDate != null) {
      timeLeft = DateTime.now().difference(expiredDate);
      if (activeTest) {
        final testTerms = this.testTerms;
        if (testTerms != null) {
          if (testTerms.containsKey('days')) {
            expiredDate = expiredDate.add(Duration(days: testTerms['days']!));
            timeLeft = expiredDate.difference(DateTime.now());
          }
        }
      }
      if (expiredDate.compareTo(DateTime.now()) >= 0) {
        if (timeLeft.inDays > 0) {
          final daysRemaining = timeLeft.inDays.toString();
          if (timeLeft.inDays == 1) {
            return 'Expires in 1 day';
          }
          return 'Expires in $daysRemaining days';
        } else if (timeLeft.inHours > 0) {
          final hoursRemaining = timeLeft.inHours.toString();
          if (timeLeft.inHours == 1) {
            return 'Expires in 1 hour';
          }
          return 'Expires in $hoursRemaining hours';
        } else {
          final minutesRemaining = timeLeft.inMinutes.toString();
          return 'Expires in $minutesRemaining minutes';
        }
      } else {
        final expiredDateFormatted = DateFormat('dd.MM.yy').format(expiredDate);
        return 'Expired $expiredDateFormatted';
      }
    } else {
      return 'Expires: unknown';
    }
  }

  @override
  List<Object> get props => [
        userId,
        activeTest,
        createdAt,
        updatedAt,
        launches,
        paidTill,
        product,
        runTime,
      ];

  @override
  bool get stringify => true;
}
