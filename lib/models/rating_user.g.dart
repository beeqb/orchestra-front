// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingUser _$RatingUserFromJson(Map<String, dynamic> json) => RatingUser(
      json['name'] as String,
      json['avatar'] as String,
    );

Map<String, dynamic> _$RatingUserToJson(RatingUser instance) =>
    <String, dynamic>{
      'name': instance.name,
      'avatar': instance.avatar,
    };
