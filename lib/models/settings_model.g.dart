// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SettingsModel _$$_SettingsModelFromJson(Map<String, dynamic> json) =>
    _$_SettingsModel(
      json['key'] as String?,
      json['type'] as String,
      json['errorHandle'] as String?,
      json['name'] as String,
      $enumDecode(_$ManifestInputTypeEnumMap, json['data_type']),
      json['position_num'] as int,
      json['file_format'] as String?,
      json['max'] as int?,
      json['max_symbols'] as int?,
      (json['labels'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String),
      ),
      (json['options'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String),
      ),
    );

Map<String, dynamic> _$$_SettingsModelToJson(_$_SettingsModel instance) {
  final val = <String, dynamic>{
    'key': instance.key,
    'type': instance.type,
    'errorHandle': instance.errorHandle,
    'name': instance.name,
    'data_type': _$ManifestInputTypeEnumMap[instance.dataType]!,
    'position_num': instance.positionNum,
    'file_format': instance.fileFormat,
    'max': instance.max,
    'max_symbols': instance.maxSymbols,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('labels', instance.labels);
  writeNotNull('options', instance.options);
  return val;
}

const _$ManifestInputTypeEnumMap = {
  ManifestInputType.select: 'select',
  ManifestInputType.radio: 'radio',
  ManifestInputType.checkbox: 'checkbox',
  ManifestInputType.text: 'text',
  ManifestInputType.textarea: 'textarea',
};
