// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_item_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingItem _$RatingItemFromJson(Map<String, dynamic> json) => RatingItem(
      id: json['_id'] as String?,
      rank: json['rank'] as int,
      comment: json['comment'] as String?,
      productId: json['appId'] as String,
      createdAt: json['createdAt'] as String?,
      user: json['user'] == null
          ? null
          : RatingUser.fromJson(json['user'] as Map<String, dynamic>),
      answer: json['answer'] as String?,
    );

Map<String, dynamic> _$RatingItemToJson(RatingItem instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  val['rank'] = instance.rank;
  writeNotNull('comment', instance.comment);
  val['appId'] = instance.productId;
  val['createdAt'] = instance.createdAt;
  val['user'] = instance.user?.toJson();
  val['answer'] = instance.answer;
  return val;
}
