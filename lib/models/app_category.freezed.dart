// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'app_category.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppCategory _$AppCategoryFromJson(Map<String, dynamic> json) {
  return _AppCategory.fromJson(json);
}

/// @nodoc
mixin _$AppCategory {
  ProductCategoryType get productCategoryType =>
      throw _privateConstructorUsedError;
  List<Product?> get prods => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppCategoryCopyWith<AppCategory> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppCategoryCopyWith<$Res> {
  factory $AppCategoryCopyWith(
          AppCategory value, $Res Function(AppCategory) then) =
      _$AppCategoryCopyWithImpl<$Res>;
  $Res call({ProductCategoryType productCategoryType, List<Product?> prods});
}

/// @nodoc
class _$AppCategoryCopyWithImpl<$Res> implements $AppCategoryCopyWith<$Res> {
  _$AppCategoryCopyWithImpl(this._value, this._then);

  final AppCategory _value;
  // ignore: unused_field
  final $Res Function(AppCategory) _then;

  @override
  $Res call({
    Object? productCategoryType = freezed,
    Object? prods = freezed,
  }) {
    return _then(_value.copyWith(
      productCategoryType: productCategoryType == freezed
          ? _value.productCategoryType
          : productCategoryType // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      prods: prods == freezed
          ? _value.prods
          : prods // ignore: cast_nullable_to_non_nullable
              as List<Product?>,
    ));
  }
}

/// @nodoc
abstract class _$$_AppCategoryCopyWith<$Res>
    implements $AppCategoryCopyWith<$Res> {
  factory _$$_AppCategoryCopyWith(
          _$_AppCategory value, $Res Function(_$_AppCategory) then) =
      __$$_AppCategoryCopyWithImpl<$Res>;
  @override
  $Res call({ProductCategoryType productCategoryType, List<Product?> prods});
}

/// @nodoc
class __$$_AppCategoryCopyWithImpl<$Res> extends _$AppCategoryCopyWithImpl<$Res>
    implements _$$_AppCategoryCopyWith<$Res> {
  __$$_AppCategoryCopyWithImpl(
      _$_AppCategory _value, $Res Function(_$_AppCategory) _then)
      : super(_value, (v) => _then(v as _$_AppCategory));

  @override
  _$_AppCategory get _value => super._value as _$_AppCategory;

  @override
  $Res call({
    Object? productCategoryType = freezed,
    Object? prods = freezed,
  }) {
    return _then(_$_AppCategory(
      productCategoryType == freezed
          ? _value.productCategoryType
          : productCategoryType // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      prods == freezed
          ? _value._prods
          : prods // ignore: cast_nullable_to_non_nullable
              as List<Product?>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AppCategory implements _AppCategory {
  _$_AppCategory(this.productCategoryType, final List<Product?> prods)
      : _prods = prods;

  factory _$_AppCategory.fromJson(Map<String, dynamic> json) =>
      _$$_AppCategoryFromJson(json);

  @override
  final ProductCategoryType productCategoryType;
  final List<Product?> _prods;
  @override
  List<Product?> get prods {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_prods);
  }

  @override
  String toString() {
    return 'AppCategory(productCategoryType: $productCategoryType, prods: $prods)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppCategory &&
            const DeepCollectionEquality()
                .equals(other.productCategoryType, productCategoryType) &&
            const DeepCollectionEquality().equals(other._prods, _prods));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(productCategoryType),
      const DeepCollectionEquality().hash(_prods));

  @JsonKey(ignore: true)
  @override
  _$$_AppCategoryCopyWith<_$_AppCategory> get copyWith =>
      __$$_AppCategoryCopyWithImpl<_$_AppCategory>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppCategoryToJson(
      this,
    );
  }
}

abstract class _AppCategory implements AppCategory {
  factory _AppCategory(final ProductCategoryType productCategoryType,
      final List<Product?> prods) = _$_AppCategory;

  factory _AppCategory.fromJson(Map<String, dynamic> json) =
      _$_AppCategory.fromJson;

  @override
  ProductCategoryType get productCategoryType;
  @override
  List<Product?> get prods;
  @override
  @JsonKey(ignore: true)
  _$$_AppCategoryCopyWith<_$_AppCategory> get copyWith =>
      throw _privateConstructorUsedError;
}
