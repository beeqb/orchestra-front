import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
part 'results_model.g.dart';

@JsonSerializable()
class ResultsModel extends Equatable {
  final String id;
  final String name;
  final String createdAt;
  final String result;
  final String icon;

  const ResultsModel({
    required this.id,
    required this.name,
    required this.createdAt,
    required this.result,
    required this.icon,
  });

  factory ResultsModel.fromJson(Map<String, dynamic> json) =>
      _$ResultsModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResultsModelToJson(this);

  @override
  List<Object> get props => [
        id,
        name,
        createdAt,
        result,
        icon,
      ];

  @override
  bool get stringify => true;
}
