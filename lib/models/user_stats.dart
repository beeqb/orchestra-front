/* import 'package:json_annotation/json_annotation.dart';

part 'user_stats.g.dart';

@JsonSerializable(explicitToJson: true)
class UserStats {
  final String? updatedAt;
  final num costs;
  final num purchases;
  final num profit;

  const UserStats(
    this.updatedAt,
    this.costs,
    this.purchases,
    this.profit,
  );

  factory UserStats.fromJson(Map<String, dynamic> json) =>
      _$UserStatsFromJson(json);

  Map<String, dynamic> toJson() => _$UserStatsToJson(this);

  @override
  String toString() {
    return 'UserStats(updatedAt: $updatedAt, costs: $costs, '
        'purchases: $purchases, profit: $profit)';
  }
} */

import 'package:freezed_annotation/freezed_annotation.dart';
part 'user_stats.freezed.dart';
part 'user_stats.g.dart';

@freezed
class UserStats with _$UserStats {
  factory UserStats({
    String? updatedAt,
    @Default(0) num costs,
    @Default(0) num purchases,
    @Default(0) num profit,
  }) = _UserStats;

  factory UserStats.fromJson(Map<String, dynamic> json) =>
      _$UserStatsFromJson(json);
}
