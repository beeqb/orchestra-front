import 'package:orchestra/models/product/product.dart';

import 'enums/dialog_type.dart';
import 'rating_item_model.dart';

class DialogRequest {
  final DialogType dialogType;
  final String title;
  final String description;
  final RatingItem? ratingItem;
  final Product? product;

  DialogRequest({
    required this.dialogType,
    required this.title,
    required this.description,
    this.ratingItem,
    this.product,
  });

  @override
  String toString() => 'DialogRequest(title: $title, description: $description';
}

class DialogResponse {
  final String? fieldOne;
  final String? fieldTwo;
  final bool? confirmed;

  DialogResponse({
    this.fieldOne,
    this.fieldTwo,
    this.confirmed,
  });

  @override
  String toString() =>
      'DialogResponse(fieldOne: $fieldOne, fieldTwo: $fieldTwo,'
      ' confirmed: $confirmed)';
}
