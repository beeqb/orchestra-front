import 'package:orchestra/models/product/product.dart';

import 'enums/enums.dart';

import 'package:freezed_annotation/freezed_annotation.dart';
part 'app_category.freezed.dart';
part 'app_category.g.dart';

@freezed
class AppCategory with _$AppCategory {
  factory AppCategory(
    ProductCategoryType productCategoryType,
    List<Product?> prods,
  ) = _AppCategory;

  factory AppCategory.fromJson(Map<String, dynamic> json) =>
      _$AppCategoryFromJson(json);
}
