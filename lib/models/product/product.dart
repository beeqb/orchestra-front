import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/product_category_type.dart';
import 'package:orchestra/models/enums/product_status_type.dart';
import 'package:orchestra/models/product_models/adress.dart';
import 'package:orchestra/models/product_models/pricing_and_business.dart';
import 'package:orchestra/models/product_models/product_launcher.dart';
import 'package:orchestra/models/product_models/stats.dart';

import '../enums/business_model_type.dart';
import '../enums/data_processing_type.dart';
import '../enums/incoming_data_type.dart';
import '../enums/outgoing_data_type.dart';
part 'product.freezed.dart';
part 'product.g.dart';

@freezed
class Product with _$Product {
  factory Product({
    @JsonKey(includeIfNull: false) final Stats? stats,
    @JsonKey(includeIfNull: false) final List<String>? photoDescriptionUrls,
    @JsonKey(includeIfNull: false) final List<String>? buyByUser,
    @JsonKey(includeIfNull: false) final List<String>? apps,
    @JsonKey(includeIfNull: false, name: '_id') required final String id,
    required final ProductStatusType status,
    @JsonKey(includeIfNull: false) required final String userId,
    @JsonKey(includeIfNull: false) required final String productType,
    @JsonKey(includeIfNull: false)
        required final ProductCategoryType productCategory,
    @JsonKey(includeIfNull: false) required final String productVersion,
    @JsonKey(includeIfNull: false) required final String productEmail,
    @JsonKey(includeIfNull: false) required final String productURL,
    @JsonKey(includeIfNull: false) required final String productLogoURL,
    @JsonKey(includeIfNull: false) required final String productName,
    @JsonKey(includeIfNull: false) final String? productMiniPromoPictureURL,
    @JsonKey(includeIfNull: false) final String? productPromoPictureURL,
    @JsonKey(includeIfNull: false) required final String productDescription,
    @JsonKey(includeIfNull: false) required final String howItWork,
    @JsonKey(includeIfNull: false) required final String videoDescription,
    @JsonKey(includeIfNull: false) required final Address address,
    @JsonKey(includeIfNull: false) final BusinessModelType? businessModelType,
    @JsonKey(includeIfNull: false)
        required final PricingAndBusiness pricingAndBusiness,
    @JsonKey(includeIfNull: false)
        required final ProductLauncher productLauncher,
    @JsonKey(includeIfNull: false) final String? createdAt,
    @JsonKey(includeIfNull: false) final String? updatedAt,
    @JsonKey(includeIfNull: false) final int? iV,
    @JsonKey(includeIfNull: false) final bool? purchased,
    @JsonKey(includeIfNull: false) final String? rank,
    @JsonKey(includeIfNull: false) required final String productContributor,
    final bool? failure,
  }) = _Product;

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}
