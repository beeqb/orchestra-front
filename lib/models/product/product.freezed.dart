// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Product _$ProductFromJson(Map<String, dynamic> json) {
  return _Product.fromJson(json);
}

/// @nodoc
mixin _$Product {
  @JsonKey(includeIfNull: false)
  Stats? get stats => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  List<String>? get photoDescriptionUrls => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  List<String>? get buyByUser => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  List<String>? get apps => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false, name: '_id')
  String get id => throw _privateConstructorUsedError;
  ProductStatusType get status => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get userId => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productType => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  ProductCategoryType get productCategory => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productVersion => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productEmail => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productURL => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productLogoURL => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productName => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get productMiniPromoPictureURL => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get productPromoPictureURL => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productDescription => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get howItWork => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get videoDescription => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  Address get address => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  BusinessModelType? get businessModelType =>
      throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  PricingAndBusiness get pricingAndBusiness =>
      throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  ProductLauncher get productLauncher => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get createdAt => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get updatedAt => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  int? get iV => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  bool? get purchased => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get rank => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String get productContributor => throw _privateConstructorUsedError;
  bool? get failure => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductCopyWith<Product> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductCopyWith<$Res> {
  factory $ProductCopyWith(Product value, $Res Function(Product) then) =
      _$ProductCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(includeIfNull: false) Stats? stats,
      @JsonKey(includeIfNull: false) List<String>? photoDescriptionUrls,
      @JsonKey(includeIfNull: false) List<String>? buyByUser,
      @JsonKey(includeIfNull: false) List<String>? apps,
      @JsonKey(includeIfNull: false, name: '_id') String id,
      ProductStatusType status,
      @JsonKey(includeIfNull: false) String userId,
      @JsonKey(includeIfNull: false) String productType,
      @JsonKey(includeIfNull: false) ProductCategoryType productCategory,
      @JsonKey(includeIfNull: false) String productVersion,
      @JsonKey(includeIfNull: false) String productEmail,
      @JsonKey(includeIfNull: false) String productURL,
      @JsonKey(includeIfNull: false) String productLogoURL,
      @JsonKey(includeIfNull: false) String productName,
      @JsonKey(includeIfNull: false) String? productMiniPromoPictureURL,
      @JsonKey(includeIfNull: false) String? productPromoPictureURL,
      @JsonKey(includeIfNull: false) String productDescription,
      @JsonKey(includeIfNull: false) String howItWork,
      @JsonKey(includeIfNull: false) String videoDescription,
      @JsonKey(includeIfNull: false) Address address,
      @JsonKey(includeIfNull: false) BusinessModelType? businessModelType,
      @JsonKey(includeIfNull: false) PricingAndBusiness pricingAndBusiness,
      @JsonKey(includeIfNull: false) ProductLauncher productLauncher,
      @JsonKey(includeIfNull: false) String? createdAt,
      @JsonKey(includeIfNull: false) String? updatedAt,
      @JsonKey(includeIfNull: false) int? iV,
      @JsonKey(includeIfNull: false) bool? purchased,
      @JsonKey(includeIfNull: false) String? rank,
      @JsonKey(includeIfNull: false) String productContributor,
      bool? failure});

  $AddressCopyWith<$Res> get address;
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness;
  $ProductLauncherCopyWith<$Res> get productLauncher;
}

/// @nodoc
class _$ProductCopyWithImpl<$Res> implements $ProductCopyWith<$Res> {
  _$ProductCopyWithImpl(this._value, this._then);

  final Product _value;
  // ignore: unused_field
  final $Res Function(Product) _then;

  @override
  $Res call({
    Object? stats = freezed,
    Object? photoDescriptionUrls = freezed,
    Object? buyByUser = freezed,
    Object? apps = freezed,
    Object? id = freezed,
    Object? status = freezed,
    Object? userId = freezed,
    Object? productType = freezed,
    Object? productCategory = freezed,
    Object? productVersion = freezed,
    Object? productEmail = freezed,
    Object? productURL = freezed,
    Object? productLogoURL = freezed,
    Object? productName = freezed,
    Object? productMiniPromoPictureURL = freezed,
    Object? productPromoPictureURL = freezed,
    Object? productDescription = freezed,
    Object? howItWork = freezed,
    Object? videoDescription = freezed,
    Object? address = freezed,
    Object? businessModelType = freezed,
    Object? pricingAndBusiness = freezed,
    Object? productLauncher = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? iV = freezed,
    Object? purchased = freezed,
    Object? rank = freezed,
    Object? productContributor = freezed,
    Object? failure = freezed,
  }) {
    return _then(_value.copyWith(
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as Stats?,
      photoDescriptionUrls: photoDescriptionUrls == freezed
          ? _value.photoDescriptionUrls
          : photoDescriptionUrls // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      buyByUser: buyByUser == freezed
          ? _value.buyByUser
          : buyByUser // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      apps: apps == freezed
          ? _value.apps
          : apps // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProductStatusType,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      productType: productType == freezed
          ? _value.productType
          : productType // ignore: cast_nullable_to_non_nullable
              as String,
      productCategory: productCategory == freezed
          ? _value.productCategory
          : productCategory // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      productVersion: productVersion == freezed
          ? _value.productVersion
          : productVersion // ignore: cast_nullable_to_non_nullable
              as String,
      productEmail: productEmail == freezed
          ? _value.productEmail
          : productEmail // ignore: cast_nullable_to_non_nullable
              as String,
      productURL: productURL == freezed
          ? _value.productURL
          : productURL // ignore: cast_nullable_to_non_nullable
              as String,
      productLogoURL: productLogoURL == freezed
          ? _value.productLogoURL
          : productLogoURL // ignore: cast_nullable_to_non_nullable
              as String,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String,
      productMiniPromoPictureURL: productMiniPromoPictureURL == freezed
          ? _value.productMiniPromoPictureURL
          : productMiniPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productPromoPictureURL: productPromoPictureURL == freezed
          ? _value.productPromoPictureURL
          : productPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productDescription: productDescription == freezed
          ? _value.productDescription
          : productDescription // ignore: cast_nullable_to_non_nullable
              as String,
      howItWork: howItWork == freezed
          ? _value.howItWork
          : howItWork // ignore: cast_nullable_to_non_nullable
              as String,
      videoDescription: videoDescription == freezed
          ? _value.videoDescription
          : videoDescription // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as Address,
      businessModelType: businessModelType == freezed
          ? _value.businessModelType
          : businessModelType // ignore: cast_nullable_to_non_nullable
              as BusinessModelType?,
      pricingAndBusiness: pricingAndBusiness == freezed
          ? _value.pricingAndBusiness
          : pricingAndBusiness // ignore: cast_nullable_to_non_nullable
              as PricingAndBusiness,
      productLauncher: productLauncher == freezed
          ? _value.productLauncher
          : productLauncher // ignore: cast_nullable_to_non_nullable
              as ProductLauncher,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      iV: iV == freezed
          ? _value.iV
          : iV // ignore: cast_nullable_to_non_nullable
              as int?,
      purchased: purchased == freezed
          ? _value.purchased
          : purchased // ignore: cast_nullable_to_non_nullable
              as bool?,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as String?,
      productContributor: productContributor == freezed
          ? _value.productContributor
          : productContributor // ignore: cast_nullable_to_non_nullable
              as String,
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }

  @override
  $AddressCopyWith<$Res> get address {
    return $AddressCopyWith<$Res>(_value.address, (value) {
      return _then(_value.copyWith(address: value));
    });
  }

  @override
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness {
    return $PricingAndBusinessCopyWith<$Res>(_value.pricingAndBusiness,
        (value) {
      return _then(_value.copyWith(pricingAndBusiness: value));
    });
  }

  @override
  $ProductLauncherCopyWith<$Res> get productLauncher {
    return $ProductLauncherCopyWith<$Res>(_value.productLauncher, (value) {
      return _then(_value.copyWith(productLauncher: value));
    });
  }
}

/// @nodoc
abstract class _$$_ProductCopyWith<$Res> implements $ProductCopyWith<$Res> {
  factory _$$_ProductCopyWith(
          _$_Product value, $Res Function(_$_Product) then) =
      __$$_ProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(includeIfNull: false) Stats? stats,
      @JsonKey(includeIfNull: false) List<String>? photoDescriptionUrls,
      @JsonKey(includeIfNull: false) List<String>? buyByUser,
      @JsonKey(includeIfNull: false) List<String>? apps,
      @JsonKey(includeIfNull: false, name: '_id') String id,
      ProductStatusType status,
      @JsonKey(includeIfNull: false) String userId,
      @JsonKey(includeIfNull: false) String productType,
      @JsonKey(includeIfNull: false) ProductCategoryType productCategory,
      @JsonKey(includeIfNull: false) String productVersion,
      @JsonKey(includeIfNull: false) String productEmail,
      @JsonKey(includeIfNull: false) String productURL,
      @JsonKey(includeIfNull: false) String productLogoURL,
      @JsonKey(includeIfNull: false) String productName,
      @JsonKey(includeIfNull: false) String? productMiniPromoPictureURL,
      @JsonKey(includeIfNull: false) String? productPromoPictureURL,
      @JsonKey(includeIfNull: false) String productDescription,
      @JsonKey(includeIfNull: false) String howItWork,
      @JsonKey(includeIfNull: false) String videoDescription,
      @JsonKey(includeIfNull: false) Address address,
      @JsonKey(includeIfNull: false) BusinessModelType? businessModelType,
      @JsonKey(includeIfNull: false) PricingAndBusiness pricingAndBusiness,
      @JsonKey(includeIfNull: false) ProductLauncher productLauncher,
      @JsonKey(includeIfNull: false) String? createdAt,
      @JsonKey(includeIfNull: false) String? updatedAt,
      @JsonKey(includeIfNull: false) int? iV,
      @JsonKey(includeIfNull: false) bool? purchased,
      @JsonKey(includeIfNull: false) String? rank,
      @JsonKey(includeIfNull: false) String productContributor,
      bool? failure});

  @override
  $AddressCopyWith<$Res> get address;
  @override
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness;
  @override
  $ProductLauncherCopyWith<$Res> get productLauncher;
}

/// @nodoc
class __$$_ProductCopyWithImpl<$Res> extends _$ProductCopyWithImpl<$Res>
    implements _$$_ProductCopyWith<$Res> {
  __$$_ProductCopyWithImpl(_$_Product _value, $Res Function(_$_Product) _then)
      : super(_value, (v) => _then(v as _$_Product));

  @override
  _$_Product get _value => super._value as _$_Product;

  @override
  $Res call({
    Object? stats = freezed,
    Object? photoDescriptionUrls = freezed,
    Object? buyByUser = freezed,
    Object? apps = freezed,
    Object? id = freezed,
    Object? status = freezed,
    Object? userId = freezed,
    Object? productType = freezed,
    Object? productCategory = freezed,
    Object? productVersion = freezed,
    Object? productEmail = freezed,
    Object? productURL = freezed,
    Object? productLogoURL = freezed,
    Object? productName = freezed,
    Object? productMiniPromoPictureURL = freezed,
    Object? productPromoPictureURL = freezed,
    Object? productDescription = freezed,
    Object? howItWork = freezed,
    Object? videoDescription = freezed,
    Object? address = freezed,
    Object? businessModelType = freezed,
    Object? pricingAndBusiness = freezed,
    Object? productLauncher = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? iV = freezed,
    Object? purchased = freezed,
    Object? rank = freezed,
    Object? productContributor = freezed,
    Object? failure = freezed,
  }) {
    return _then(_$_Product(
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as Stats?,
      photoDescriptionUrls: photoDescriptionUrls == freezed
          ? _value._photoDescriptionUrls
          : photoDescriptionUrls // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      buyByUser: buyByUser == freezed
          ? _value._buyByUser
          : buyByUser // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      apps: apps == freezed
          ? _value._apps
          : apps // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProductStatusType,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      productType: productType == freezed
          ? _value.productType
          : productType // ignore: cast_nullable_to_non_nullable
              as String,
      productCategory: productCategory == freezed
          ? _value.productCategory
          : productCategory // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      productVersion: productVersion == freezed
          ? _value.productVersion
          : productVersion // ignore: cast_nullable_to_non_nullable
              as String,
      productEmail: productEmail == freezed
          ? _value.productEmail
          : productEmail // ignore: cast_nullable_to_non_nullable
              as String,
      productURL: productURL == freezed
          ? _value.productURL
          : productURL // ignore: cast_nullable_to_non_nullable
              as String,
      productLogoURL: productLogoURL == freezed
          ? _value.productLogoURL
          : productLogoURL // ignore: cast_nullable_to_non_nullable
              as String,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String,
      productMiniPromoPictureURL: productMiniPromoPictureURL == freezed
          ? _value.productMiniPromoPictureURL
          : productMiniPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productPromoPictureURL: productPromoPictureURL == freezed
          ? _value.productPromoPictureURL
          : productPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productDescription: productDescription == freezed
          ? _value.productDescription
          : productDescription // ignore: cast_nullable_to_non_nullable
              as String,
      howItWork: howItWork == freezed
          ? _value.howItWork
          : howItWork // ignore: cast_nullable_to_non_nullable
              as String,
      videoDescription: videoDescription == freezed
          ? _value.videoDescription
          : videoDescription // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as Address,
      businessModelType: businessModelType == freezed
          ? _value.businessModelType
          : businessModelType // ignore: cast_nullable_to_non_nullable
              as BusinessModelType?,
      pricingAndBusiness: pricingAndBusiness == freezed
          ? _value.pricingAndBusiness
          : pricingAndBusiness // ignore: cast_nullable_to_non_nullable
              as PricingAndBusiness,
      productLauncher: productLauncher == freezed
          ? _value.productLauncher
          : productLauncher // ignore: cast_nullable_to_non_nullable
              as ProductLauncher,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      iV: iV == freezed
          ? _value.iV
          : iV // ignore: cast_nullable_to_non_nullable
              as int?,
      purchased: purchased == freezed
          ? _value.purchased
          : purchased // ignore: cast_nullable_to_non_nullable
              as bool?,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as String?,
      productContributor: productContributor == freezed
          ? _value.productContributor
          : productContributor // ignore: cast_nullable_to_non_nullable
              as String,
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Product implements _Product {
  _$_Product(
      {@JsonKey(includeIfNull: false) this.stats,
      @JsonKey(includeIfNull: false) final List<String>? photoDescriptionUrls,
      @JsonKey(includeIfNull: false) final List<String>? buyByUser,
      @JsonKey(includeIfNull: false) final List<String>? apps,
      @JsonKey(includeIfNull: false, name: '_id') required this.id,
      required this.status,
      @JsonKey(includeIfNull: false) required this.userId,
      @JsonKey(includeIfNull: false) required this.productType,
      @JsonKey(includeIfNull: false) required this.productCategory,
      @JsonKey(includeIfNull: false) required this.productVersion,
      @JsonKey(includeIfNull: false) required this.productEmail,
      @JsonKey(includeIfNull: false) required this.productURL,
      @JsonKey(includeIfNull: false) required this.productLogoURL,
      @JsonKey(includeIfNull: false) required this.productName,
      @JsonKey(includeIfNull: false) this.productMiniPromoPictureURL,
      @JsonKey(includeIfNull: false) this.productPromoPictureURL,
      @JsonKey(includeIfNull: false) required this.productDescription,
      @JsonKey(includeIfNull: false) required this.howItWork,
      @JsonKey(includeIfNull: false) required this.videoDescription,
      @JsonKey(includeIfNull: false) required this.address,
      @JsonKey(includeIfNull: false) this.businessModelType,
      @JsonKey(includeIfNull: false) required this.pricingAndBusiness,
      @JsonKey(includeIfNull: false) required this.productLauncher,
      @JsonKey(includeIfNull: false) this.createdAt,
      @JsonKey(includeIfNull: false) this.updatedAt,
      @JsonKey(includeIfNull: false) this.iV,
      @JsonKey(includeIfNull: false) this.purchased,
      @JsonKey(includeIfNull: false) this.rank,
      @JsonKey(includeIfNull: false) required this.productContributor,
      this.failure})
      : _photoDescriptionUrls = photoDescriptionUrls,
        _buyByUser = buyByUser,
        _apps = apps;

  factory _$_Product.fromJson(Map<String, dynamic> json) =>
      _$$_ProductFromJson(json);

  @override
  @JsonKey(includeIfNull: false)
  final Stats? stats;
  final List<String>? _photoDescriptionUrls;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get photoDescriptionUrls {
    final value = _photoDescriptionUrls;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String>? _buyByUser;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get buyByUser {
    final value = _buyByUser;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String>? _apps;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get apps {
    final value = _apps;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(includeIfNull: false, name: '_id')
  final String id;
  @override
  final ProductStatusType status;
  @override
  @JsonKey(includeIfNull: false)
  final String userId;
  @override
  @JsonKey(includeIfNull: false)
  final String productType;
  @override
  @JsonKey(includeIfNull: false)
  final ProductCategoryType productCategory;
  @override
  @JsonKey(includeIfNull: false)
  final String productVersion;
  @override
  @JsonKey(includeIfNull: false)
  final String productEmail;
  @override
  @JsonKey(includeIfNull: false)
  final String productURL;
  @override
  @JsonKey(includeIfNull: false)
  final String productLogoURL;
  @override
  @JsonKey(includeIfNull: false)
  final String productName;
  @override
  @JsonKey(includeIfNull: false)
  final String? productMiniPromoPictureURL;
  @override
  @JsonKey(includeIfNull: false)
  final String? productPromoPictureURL;
  @override
  @JsonKey(includeIfNull: false)
  final String productDescription;
  @override
  @JsonKey(includeIfNull: false)
  final String howItWork;
  @override
  @JsonKey(includeIfNull: false)
  final String videoDescription;
  @override
  @JsonKey(includeIfNull: false)
  final Address address;
  @override
  @JsonKey(includeIfNull: false)
  final BusinessModelType? businessModelType;
  @override
  @JsonKey(includeIfNull: false)
  final PricingAndBusiness pricingAndBusiness;
  @override
  @JsonKey(includeIfNull: false)
  final ProductLauncher productLauncher;
  @override
  @JsonKey(includeIfNull: false)
  final String? createdAt;
  @override
  @JsonKey(includeIfNull: false)
  final String? updatedAt;
  @override
  @JsonKey(includeIfNull: false)
  final int? iV;
  @override
  @JsonKey(includeIfNull: false)
  final bool? purchased;
  @override
  @JsonKey(includeIfNull: false)
  final String? rank;
  @override
  @JsonKey(includeIfNull: false)
  final String productContributor;
  @override
  final bool? failure;

  @override
  String toString() {
    return 'Product(stats: $stats, photoDescriptionUrls: $photoDescriptionUrls, buyByUser: $buyByUser, apps: $apps, id: $id, status: $status, userId: $userId, productType: $productType, productCategory: $productCategory, productVersion: $productVersion, productEmail: $productEmail, productURL: $productURL, productLogoURL: $productLogoURL, productName: $productName, productMiniPromoPictureURL: $productMiniPromoPictureURL, productPromoPictureURL: $productPromoPictureURL, productDescription: $productDescription, howItWork: $howItWork, videoDescription: $videoDescription, address: $address, businessModelType: $businessModelType, pricingAndBusiness: $pricingAndBusiness, productLauncher: $productLauncher, createdAt: $createdAt, updatedAt: $updatedAt, iV: $iV, purchased: $purchased, rank: $rank, productContributor: $productContributor, failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Product &&
            const DeepCollectionEquality().equals(other.stats, stats) &&
            const DeepCollectionEquality()
                .equals(other._photoDescriptionUrls, _photoDescriptionUrls) &&
            const DeepCollectionEquality()
                .equals(other._buyByUser, _buyByUser) &&
            const DeepCollectionEquality().equals(other._apps, _apps) &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality()
                .equals(other.productType, productType) &&
            const DeepCollectionEquality()
                .equals(other.productCategory, productCategory) &&
            const DeepCollectionEquality()
                .equals(other.productVersion, productVersion) &&
            const DeepCollectionEquality()
                .equals(other.productEmail, productEmail) &&
            const DeepCollectionEquality()
                .equals(other.productURL, productURL) &&
            const DeepCollectionEquality()
                .equals(other.productLogoURL, productLogoURL) &&
            const DeepCollectionEquality()
                .equals(other.productName, productName) &&
            const DeepCollectionEquality().equals(
                other.productMiniPromoPictureURL, productMiniPromoPictureURL) &&
            const DeepCollectionEquality()
                .equals(other.productPromoPictureURL, productPromoPictureURL) &&
            const DeepCollectionEquality()
                .equals(other.productDescription, productDescription) &&
            const DeepCollectionEquality().equals(other.howItWork, howItWork) &&
            const DeepCollectionEquality()
                .equals(other.videoDescription, videoDescription) &&
            const DeepCollectionEquality().equals(other.address, address) &&
            const DeepCollectionEquality()
                .equals(other.businessModelType, businessModelType) &&
            const DeepCollectionEquality()
                .equals(other.pricingAndBusiness, pricingAndBusiness) &&
            const DeepCollectionEquality()
                .equals(other.productLauncher, productLauncher) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt) &&
            const DeepCollectionEquality().equals(other.updatedAt, updatedAt) &&
            const DeepCollectionEquality().equals(other.iV, iV) &&
            const DeepCollectionEquality().equals(other.purchased, purchased) &&
            const DeepCollectionEquality().equals(other.rank, rank) &&
            const DeepCollectionEquality()
                .equals(other.productContributor, productContributor) &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(stats),
        const DeepCollectionEquality().hash(_photoDescriptionUrls),
        const DeepCollectionEquality().hash(_buyByUser),
        const DeepCollectionEquality().hash(_apps),
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(userId),
        const DeepCollectionEquality().hash(productType),
        const DeepCollectionEquality().hash(productCategory),
        const DeepCollectionEquality().hash(productVersion),
        const DeepCollectionEquality().hash(productEmail),
        const DeepCollectionEquality().hash(productURL),
        const DeepCollectionEquality().hash(productLogoURL),
        const DeepCollectionEquality().hash(productName),
        const DeepCollectionEquality().hash(productMiniPromoPictureURL),
        const DeepCollectionEquality().hash(productPromoPictureURL),
        const DeepCollectionEquality().hash(productDescription),
        const DeepCollectionEquality().hash(howItWork),
        const DeepCollectionEquality().hash(videoDescription),
        const DeepCollectionEquality().hash(address),
        const DeepCollectionEquality().hash(businessModelType),
        const DeepCollectionEquality().hash(pricingAndBusiness),
        const DeepCollectionEquality().hash(productLauncher),
        const DeepCollectionEquality().hash(createdAt),
        const DeepCollectionEquality().hash(updatedAt),
        const DeepCollectionEquality().hash(iV),
        const DeepCollectionEquality().hash(purchased),
        const DeepCollectionEquality().hash(rank),
        const DeepCollectionEquality().hash(productContributor),
        const DeepCollectionEquality().hash(failure)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_ProductCopyWith<_$_Product> get copyWith =>
      __$$_ProductCopyWithImpl<_$_Product>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductToJson(
      this,
    );
  }
}

abstract class _Product implements Product {
  factory _Product(
      {@JsonKey(includeIfNull: false)
          final Stats? stats,
      @JsonKey(includeIfNull: false)
          final List<String>? photoDescriptionUrls,
      @JsonKey(includeIfNull: false)
          final List<String>? buyByUser,
      @JsonKey(includeIfNull: false)
          final List<String>? apps,
      @JsonKey(includeIfNull: false, name: '_id')
          required final String id,
      required final ProductStatusType status,
      @JsonKey(includeIfNull: false)
          required final String userId,
      @JsonKey(includeIfNull: false)
          required final String productType,
      @JsonKey(includeIfNull: false)
          required final ProductCategoryType productCategory,
      @JsonKey(includeIfNull: false)
          required final String productVersion,
      @JsonKey(includeIfNull: false)
          required final String productEmail,
      @JsonKey(includeIfNull: false)
          required final String productURL,
      @JsonKey(includeIfNull: false)
          required final String productLogoURL,
      @JsonKey(includeIfNull: false)
          required final String productName,
      @JsonKey(includeIfNull: false)
          final String? productMiniPromoPictureURL,
      @JsonKey(includeIfNull: false)
          final String? productPromoPictureURL,
      @JsonKey(includeIfNull: false)
          required final String productDescription,
      @JsonKey(includeIfNull: false)
          required final String howItWork,
      @JsonKey(includeIfNull: false)
          required final String videoDescription,
      @JsonKey(includeIfNull: false)
          required final Address address,
      @JsonKey(includeIfNull: false)
          final BusinessModelType? businessModelType,
      @JsonKey(includeIfNull: false)
          required final PricingAndBusiness pricingAndBusiness,
      @JsonKey(includeIfNull: false)
          required final ProductLauncher productLauncher,
      @JsonKey(includeIfNull: false)
          final String? createdAt,
      @JsonKey(includeIfNull: false)
          final String? updatedAt,
      @JsonKey(includeIfNull: false)
          final int? iV,
      @JsonKey(includeIfNull: false)
          final bool? purchased,
      @JsonKey(includeIfNull: false)
          final String? rank,
      @JsonKey(includeIfNull: false)
          required final String productContributor,
      final bool? failure}) = _$_Product;

  factory _Product.fromJson(Map<String, dynamic> json) = _$_Product.fromJson;

  @override
  @JsonKey(includeIfNull: false)
  Stats? get stats;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get photoDescriptionUrls;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get buyByUser;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get apps;
  @override
  @JsonKey(includeIfNull: false, name: '_id')
  String get id;
  @override
  ProductStatusType get status;
  @override
  @JsonKey(includeIfNull: false)
  String get userId;
  @override
  @JsonKey(includeIfNull: false)
  String get productType;
  @override
  @JsonKey(includeIfNull: false)
  ProductCategoryType get productCategory;
  @override
  @JsonKey(includeIfNull: false)
  String get productVersion;
  @override
  @JsonKey(includeIfNull: false)
  String get productEmail;
  @override
  @JsonKey(includeIfNull: false)
  String get productURL;
  @override
  @JsonKey(includeIfNull: false)
  String get productLogoURL;
  @override
  @JsonKey(includeIfNull: false)
  String get productName;
  @override
  @JsonKey(includeIfNull: false)
  String? get productMiniPromoPictureURL;
  @override
  @JsonKey(includeIfNull: false)
  String? get productPromoPictureURL;
  @override
  @JsonKey(includeIfNull: false)
  String get productDescription;
  @override
  @JsonKey(includeIfNull: false)
  String get howItWork;
  @override
  @JsonKey(includeIfNull: false)
  String get videoDescription;
  @override
  @JsonKey(includeIfNull: false)
  Address get address;
  @override
  @JsonKey(includeIfNull: false)
  BusinessModelType? get businessModelType;
  @override
  @JsonKey(includeIfNull: false)
  PricingAndBusiness get pricingAndBusiness;
  @override
  @JsonKey(includeIfNull: false)
  ProductLauncher get productLauncher;
  @override
  @JsonKey(includeIfNull: false)
  String? get createdAt;
  @override
  @JsonKey(includeIfNull: false)
  String? get updatedAt;
  @override
  @JsonKey(includeIfNull: false)
  int? get iV;
  @override
  @JsonKey(includeIfNull: false)
  bool? get purchased;
  @override
  @JsonKey(includeIfNull: false)
  String? get rank;
  @override
  @JsonKey(includeIfNull: false)
  String get productContributor;
  @override
  bool? get failure;
  @override
  @JsonKey(ignore: true)
  _$$_ProductCopyWith<_$_Product> get copyWith =>
      throw _privateConstructorUsedError;
}
