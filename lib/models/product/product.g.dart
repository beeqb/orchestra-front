// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Product _$$_ProductFromJson(Map<String, dynamic> json) => _$_Product(
      stats: json['stats'] == null
          ? null
          : Stats.fromJson(json['stats'] as Map<String, dynamic>),
      photoDescriptionUrls: (json['photoDescriptionUrls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      buyByUser: (json['buyByUser'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      apps: (json['apps'] as List<dynamic>?)?.map((e) => e as String).toList(),
      id: json['_id'] as String,
      status: $enumDecode(_$ProductStatusTypeEnumMap, json['status']),
      userId: json['userId'] as String,
      productType: json['productType'] as String,
      productCategory:
          $enumDecode(_$ProductCategoryTypeEnumMap, json['productCategory']),
      productVersion: json['productVersion'] as String,
      productEmail: json['productEmail'] as String,
      productURL: json['productURL'] as String,
      productLogoURL: json['productLogoURL'] as String,
      productName: json['productName'] as String,
      productMiniPromoPictureURL: json['productMiniPromoPictureURL'] as String?,
      productPromoPictureURL: json['productPromoPictureURL'] as String?,
      productDescription: json['productDescription'] as String,
      howItWork: json['howItWork'] as String,
      videoDescription: json['videoDescription'] as String,
      address: Address.fromJson(json['address'] as Map<String, dynamic>),
      businessModelType: $enumDecodeNullable(
          _$BusinessModelTypeEnumMap, json['businessModelType']),
      pricingAndBusiness: PricingAndBusiness.fromJson(
          json['pricingAndBusiness'] as Map<String, dynamic>),
      productLauncher: ProductLauncher.fromJson(
          json['productLauncher'] as Map<String, dynamic>),
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      iV: json['iV'] as int?,
      purchased: json['purchased'] as bool?,
      rank: json['rank'] as String?,
      productContributor: json['productContributor'] as String,
      failure: json['failure'] as bool?,
    );

Map<String, dynamic> _$$_ProductToJson(_$_Product instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('stats', instance.stats);
  writeNotNull('photoDescriptionUrls', instance.photoDescriptionUrls);
  writeNotNull('buyByUser', instance.buyByUser);
  writeNotNull('apps', instance.apps);
  val['_id'] = instance.id;
  val['status'] = _$ProductStatusTypeEnumMap[instance.status]!;
  val['userId'] = instance.userId;
  val['productType'] = instance.productType;
  val['productCategory'] =
      _$ProductCategoryTypeEnumMap[instance.productCategory]!;
  val['productVersion'] = instance.productVersion;
  val['productEmail'] = instance.productEmail;
  val['productURL'] = instance.productURL;
  val['productLogoURL'] = instance.productLogoURL;
  val['productName'] = instance.productName;
  writeNotNull(
      'productMiniPromoPictureURL', instance.productMiniPromoPictureURL);
  writeNotNull('productPromoPictureURL', instance.productPromoPictureURL);
  val['productDescription'] = instance.productDescription;
  val['howItWork'] = instance.howItWork;
  val['videoDescription'] = instance.videoDescription;
  val['address'] = instance.address;
  writeNotNull('businessModelType',
      _$BusinessModelTypeEnumMap[instance.businessModelType]);
  val['pricingAndBusiness'] = instance.pricingAndBusiness;
  val['productLauncher'] = instance.productLauncher;
  writeNotNull('createdAt', instance.createdAt);
  writeNotNull('updatedAt', instance.updatedAt);
  writeNotNull('iV', instance.iV);
  writeNotNull('purchased', instance.purchased);
  writeNotNull('rank', instance.rank);
  val['productContributor'] = instance.productContributor;
  val['failure'] = instance.failure;
  return val;
}

const _$ProductStatusTypeEnumMap = {
  ProductStatusType.marketplaceResident: 'Marketplace resident',
  ProductStatusType.waitingForReview: 'Waiting for review',
  ProductStatusType.deleted: 'Deleted',
  ProductStatusType.deniedToPublish: 'Denied to publish',
};

const _$ProductCategoryTypeEnumMap = {
  ProductCategoryType.privateUseOnly: 'PAPP',
  ProductCategoryType.business: 'BU',
  ProductCategoryType.productivity: 'PR',
  ProductCategoryType.mentalHealth: 'MH',
  ProductCategoryType.social: 'SO',
  ProductCategoryType.finance: 'FI',
  ProductCategoryType.sales: 'SA',
  ProductCategoryType.management: 'MA',
  ProductCategoryType.marketing: 'MK',
  ProductCategoryType.accounting: 'AC',
  ProductCategoryType.utils: 'UT',
  ProductCategoryType.trading: 'TR',
  ProductCategoryType.other: 'OT',
};

const _$BusinessModelTypeEnumMap = {
  BusinessModelType.oneTime: 0,
  BusinessModelType.perRequest: 1,
  BusinessModelType.monthlySubscription: 2,
};
