// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_fly_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TestFlyModel _$TestFlyModelFromJson(Map<String, dynamic> json) => TestFlyModel(
      launches: json['launches'] as int,
      period: json['period'] as String,
    );

Map<String, dynamic> _$TestFlyModelToJson(TestFlyModel instance) =>
    <String, dynamic>{
      'launches': instance.launches,
      'period': instance.period,
    };
