// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'running_app.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RunningAppModel _$RunningAppModelFromJson(Map<String, dynamic> json) =>
    RunningAppModel(
      id: json['_id'] as String?,
      appId: json['appId'] as String?,
      userId: UserModel.fromJson(json['userId'] as Map<String, dynamic>),
      name: json['name'] as String,
      status: json['status'] as String,
      icon: json['icon'] as String,
      lastMessage: json['lastMessage'] as String?,
      startTime: json['startTime'] as int?,
      pauseTime: json['pauseTime'] as int?,
      endTime: json['endTime'] as int?,
      pid: json['pid'] as int?,
      widgets:
          (json['widgets'] as List<dynamic>?)?.map((e) => e as String).toList(),
      loop: $enumDecodeNullable(_$LoopTypeEnumMap, json['loop']),
      notification: (json['notification'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      email: json['email'] as String?,
      telegram: json['telegram'] as String?,
    );

Map<String, dynamic> _$RunningAppModelToJson(RunningAppModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'appId': instance.appId,
      'userId': instance.userId.toJson(),
      'name': instance.name,
      'status': instance.status,
      'icon': instance.icon,
      'lastMessage': instance.lastMessage,
      'startTime': instance.startTime,
      'pauseTime': instance.pauseTime,
      'endTime': instance.endTime,
      'pid': instance.pid,
      'widgets': instance.widgets,
      'notification': instance.notification,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'email': instance.email,
      'telegram': instance.telegram,
      'loop': _$LoopTypeEnumMap[instance.loop],
    };

const _$LoopTypeEnumMap = {
  LoopType.noLoop: 'none',
  LoopType.m5: '300000',
  LoopType.m15: '900000',
  LoopType.m30: '1800000',
  LoopType.h1: '3600000',
  LoopType.h3: '10800000',
  LoopType.h6: '21600000',
  LoopType.h12: '43200000',
  LoopType.day: '86400000',
  LoopType.week: '604800000',
  LoopType.month: '2629743000',
  LoopType.month3: '7889229000',
  LoopType.year: '31556926000',
};
