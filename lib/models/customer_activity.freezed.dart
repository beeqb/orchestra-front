// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'customer_activity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CustomerActivity _$CustomerActivityFromJson(Map<String, dynamic> json) {
  return _CustomerActivity.fromJson(json);
}

/// @nodoc
mixin _$CustomerActivity {
  int get testFly => throw _privateConstructorUsedError;
  int get buy => throw _privateConstructorUsedError;
  double? get cr => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CustomerActivityCopyWith<CustomerActivity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CustomerActivityCopyWith<$Res> {
  factory $CustomerActivityCopyWith(
          CustomerActivity value, $Res Function(CustomerActivity) then) =
      _$CustomerActivityCopyWithImpl<$Res>;
  $Res call({int testFly, int buy, double? cr});
}

/// @nodoc
class _$CustomerActivityCopyWithImpl<$Res>
    implements $CustomerActivityCopyWith<$Res> {
  _$CustomerActivityCopyWithImpl(this._value, this._then);

  final CustomerActivity _value;
  // ignore: unused_field
  final $Res Function(CustomerActivity) _then;

  @override
  $Res call({
    Object? testFly = freezed,
    Object? buy = freezed,
    Object? cr = freezed,
  }) {
    return _then(_value.copyWith(
      testFly: testFly == freezed
          ? _value.testFly
          : testFly // ignore: cast_nullable_to_non_nullable
              as int,
      buy: buy == freezed
          ? _value.buy
          : buy // ignore: cast_nullable_to_non_nullable
              as int,
      cr: cr == freezed
          ? _value.cr
          : cr // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
abstract class _$$_CustomerActivityCopyWith<$Res>
    implements $CustomerActivityCopyWith<$Res> {
  factory _$$_CustomerActivityCopyWith(
          _$_CustomerActivity value, $Res Function(_$_CustomerActivity) then) =
      __$$_CustomerActivityCopyWithImpl<$Res>;
  @override
  $Res call({int testFly, int buy, double? cr});
}

/// @nodoc
class __$$_CustomerActivityCopyWithImpl<$Res>
    extends _$CustomerActivityCopyWithImpl<$Res>
    implements _$$_CustomerActivityCopyWith<$Res> {
  __$$_CustomerActivityCopyWithImpl(
      _$_CustomerActivity _value, $Res Function(_$_CustomerActivity) _then)
      : super(_value, (v) => _then(v as _$_CustomerActivity));

  @override
  _$_CustomerActivity get _value => super._value as _$_CustomerActivity;

  @override
  $Res call({
    Object? testFly = freezed,
    Object? buy = freezed,
    Object? cr = freezed,
  }) {
    return _then(_$_CustomerActivity(
      testFly: testFly == freezed
          ? _value.testFly
          : testFly // ignore: cast_nullable_to_non_nullable
              as int,
      buy: buy == freezed
          ? _value.buy
          : buy // ignore: cast_nullable_to_non_nullable
              as int,
      cr: cr == freezed
          ? _value.cr
          : cr // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CustomerActivity implements _CustomerActivity {
  const _$_CustomerActivity(
      {required this.testFly, required this.buy, required this.cr});

  factory _$_CustomerActivity.fromJson(Map<String, dynamic> json) =>
      _$$_CustomerActivityFromJson(json);

  @override
  final int testFly;
  @override
  final int buy;
  @override
  final double? cr;

  @override
  String toString() {
    return 'CustomerActivity(testFly: $testFly, buy: $buy, cr: $cr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CustomerActivity &&
            const DeepCollectionEquality().equals(other.testFly, testFly) &&
            const DeepCollectionEquality().equals(other.buy, buy) &&
            const DeepCollectionEquality().equals(other.cr, cr));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(testFly),
      const DeepCollectionEquality().hash(buy),
      const DeepCollectionEquality().hash(cr));

  @JsonKey(ignore: true)
  @override
  _$$_CustomerActivityCopyWith<_$_CustomerActivity> get copyWith =>
      __$$_CustomerActivityCopyWithImpl<_$_CustomerActivity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CustomerActivityToJson(
      this,
    );
  }
}

abstract class _CustomerActivity implements CustomerActivity {
  const factory _CustomerActivity(
      {required final int testFly,
      required final int buy,
      required final double? cr}) = _$_CustomerActivity;

  factory _CustomerActivity.fromJson(Map<String, dynamic> json) =
      _$_CustomerActivity.fromJson;

  @override
  int get testFly;
  @override
  int get buy;
  @override
  double? get cr;
  @override
  @JsonKey(ignore: true)
  _$$_CustomerActivityCopyWith<_$_CustomerActivity> get copyWith =>
      throw _privateConstructorUsedError;
}
