import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:orchestra/core/const.dart';

import 'enums/enums.dart';
import 'product_models/adress.dart';
import 'user_stats.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class UserModel extends Equatable {
  @JsonKey(name: '_id', includeIfNull: false)
  final String? id;
  @JsonKey(includeIfNull: false)
  final String? name;
  @JsonKey(includeIfNull: false)
  final String? email;
  @JsonKey(includeIfNull: false)
  final String? photoUrl;
  @JsonKey(includeIfNull: false)
  final num? balance;
  @JsonKey(includeIfNull: false)
  final num? received;
  @JsonKey(includeIfNull: false)
  final num? sended;
  @JsonKey(includeIfNull: false)
  final num? deposited;
  @JsonKey(includeIfNull: false)
  final bool? contributor;
  @JsonKey(includeIfNull: false)
  final UserStats? stats;
  @JsonKey(includeIfNull: false)
  final bool? isBlocked;
  @JsonKey(includeIfNull: false)
  final Address? address;
  @JsonKey(includeIfNull: false)
  final String? companyName;
  @JsonKey(includeIfNull: false)
  final String? occupation;
  @JsonKey(includeIfNull: false)
  final String? phoneNo;
  @JsonKey(includeIfNull: false)
  @JsonKey(includeIfNull: false)
  final String? referralToken;
  @JsonKey(includeIfNull: false)
  final String? referralUrl;
  @JsonKey(includeIfNull: false)
  final int? referralCount;
  @JsonKey(includeIfNull: false)
  final String? referrerId;
  final String? createdAt;
  final String? updatedAt;
  @JsonKey(includeIfNull: false)
  final String? provider;
  @JsonKey(includeIfNull: false)
  final UserType? type;
  final String? apiKey;

  const UserModel({
    this.id,
    this.email,
    this.name,
    this.photoUrl,
    this.apiKey,
    num? balance,
    num? received,
    num? sended,
    num? deposited,
    this.contributor,
    UserStats? stats,
    this.isBlocked,
    this.address,
    this.companyName,
    this.occupation,
    this.phoneNo,
    this.referralToken,
    this.referralUrl,
    this.referralCount,
    this.referrerId,
    this.createdAt,
    this.updatedAt,
    this.provider,
    this.type,
  })  : stats = stats,
        sended = sended ?? 0.0,
        received = received ?? 0.0,
        deposited = deposited ?? 0.0,
        balance = balance ?? 0.0;

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  @override
  List<Object> get props => [id!];

  @override
  bool get stringify => true;

  static const empty = UserModel(id: defaultUserId);

  UserModel copyWith({
    String? id,
    String? name,
    String? email,
    String? photoUrl,
    num? balance,
    num? received,
    num? sended,
    num? deposited,
    bool? contributor,
    UserStats? stats,
    bool? isBlocked,
    Address? address,
    String? companyName,
    String? occupation,
    String? phoneNo,
    String? referralToken,
    String? referralUrl,
    int? referralCount,
    String? referrerId,
    String? createdAt,
    String? updatedAt,
    String? provider,
    UserType? type,
    String? apiKey,
  }) {
    return UserModel(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        photoUrl: photoUrl ?? this.photoUrl,
        balance: balance ?? this.balance,
        received: received ?? this.received,
        sended: sended ?? this.sended,
        deposited: deposited ?? this.deposited,
        contributor: contributor ?? this.contributor,
        stats: stats ?? this.stats,
        isBlocked: isBlocked ?? this.isBlocked,
        address: address ?? this.address,
        companyName: companyName ?? this.companyName,
        occupation: occupation ?? this.occupation,
        phoneNo: phoneNo ?? this.phoneNo,
        referralToken: referralToken ?? this.referralToken,
        referralUrl: referralUrl ?? this.referralUrl,
        referralCount: referralCount ?? this.referralCount,
        referrerId: referrerId ?? this.referrerId,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        provider: provider ?? this.provider,
        type: type ?? this.type,
        apiKey: apiKey ?? this.apiKey);
  }
}

// import 'package:freezed_annotation/freezed_annotation.dart';
// import 'package:orchestra/models/enums/user_type.dart';
// import 'package:orchestra/models/product_models/adress.dart';
// import 'package:orchestra/models/user_stats.dart';
// part 'user_model.freezed.dart';
// part 'user_model.g.dart';

// @freezed
// class UserModel with _$UserModel {
//   factory UserModel({
//     @JsonKey(name: '_id', includeIfNull: false) String? id,
//     @JsonKey(includeIfNull: false) String? name,
//     @JsonKey(includeIfNull: false) String? email,
//     @JsonKey(includeIfNull: false) String? photoUrl,
//     @JsonKey(includeIfNull: false) num? balance,
//     @JsonKey(includeIfNull: false) num? received,
//     @JsonKey(includeIfNull: false) num? sended,
//     @JsonKey(includeIfNull: false) num? deposited,
//     @JsonKey(includeIfNull: false) bool? contributor,
//     @JsonKey(includeIfNull: false) UserStats stats,
//     @JsonKey(includeIfNull: false) bool? isBlocked,
//     @JsonKey(includeIfNull: false) Address? address,
//     @JsonKey(includeIfNull: false) String? companyName,
//     @JsonKey(includeIfNull: false) String? occupation,
//     @JsonKey(includeIfNull: false) String? phoneNo,
//     @JsonKey(includeIfNull: false) String? referralToken,
//     @JsonKey(includeIfNull: false) String? referralUrl,
//     @JsonKey(includeIfNull: false) int? referralCount,
//     @JsonKey(includeIfNull: false) String? referrerId,
//     String? createdAt,
//     String? updatedAt,
//     @JsonKey(includeIfNull: false) String? provider,
//     @JsonKey(includeIfNull: false) UserType? type,
//   }) = _UserModel;

//   factory UserModel.fromJson(Map<String, dynamic> json) =>
//       _$UserModelFromJson(json);

//  // static const empty = UserModel(id: '1234567');

//   Map<String, dynamic> toJson() => _$UserModelToJson(this);
// }
