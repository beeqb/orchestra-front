// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transfer_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferUser _$TransferUserFromJson(Map<String, dynamic> json) => TransferUser(
      json['name'] as String,
      json['photoUrl'] as String,
      json['_id'] as String,
      json['referralUrl'] as String,
    );

Map<String, dynamic> _$TransferUserToJson(TransferUser instance) =>
    <String, dynamic>{
      'name': instance.name,
      'photoUrl': instance.photoUrl,
      'referralUrl': instance.referralUrl,
      '_id': instance.id,
    };
