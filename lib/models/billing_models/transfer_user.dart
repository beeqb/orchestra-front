import 'package:json_annotation/json_annotation.dart';

part 'transfer_user.g.dart';

@JsonSerializable()
class TransferUser {
  final String name;
  final String photoUrl;
  final String referralUrl;
  @JsonKey(name: '_id')
  final String id;

  TransferUser(
    this.name,
    this.photoUrl,
    this.id,
    this.referralUrl,
  );

  factory TransferUser.fromJson(Map<String, dynamic> json) =>
      _$TransferUserFromJson(json);

  Map<String, dynamic> toJson() => _$TransferUserToJson(this);

  @override
  String toString() {
    return 'TransferUser(name: $name, photoUrl: $photoUrl, '
        'referralUrl: $referralUrl, id: $id)';
  }
}
