// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'admin_transact_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AdminTransactModel _$AdminTransactModelFromJson(Map<String, dynamic> json) =>
    AdminTransactModel(
      UserModel.fromJson(json['userId'] as Map<String, dynamic>),
      json['amount'] as num,
      json['status'] as String,
      json['id'] as String,
      TransactionMeta.fromJson(json['meta'] as Map<String, dynamic>),
      json['createdAt'] as String,
    );

Map<String, dynamic> _$AdminTransactModelToJson(AdminTransactModel instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'createdAt': instance.createdAt,
      'id': instance.id,
      'status': instance.status,
      'meta': instance.meta.toJson(),
      'userId': instance.userModel.toJson(),
    };
