import 'package:json_annotation/json_annotation.dart';

import '../user.dart';
import 'transaction_meta.dart';
import 'transaction_model.dart';

part 'admin_transact_model.g.dart';

@JsonSerializable(explicitToJson: true)
class AdminTransactModel extends TransactionModel {
  @JsonKey(name: 'userId')
  final UserModel userModel;

  const AdminTransactModel(
    this.userModel,
    num amount,
    String status,
    String id,
    TransactionMeta meta,
    String createdAt,
  ) : super(
          amount: amount,
          status: status,
          id: id,
          meta: meta,
          createdAt: createdAt,
        );

  factory AdminTransactModel.fromJson(Map<String, dynamic> json) =>
      _$AdminTransactModelFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$AdminTransactModelToJson(this);
}
