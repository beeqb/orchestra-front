import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'transaction_meta.g.dart';

@JsonSerializable(explicitToJson: true)
class TransactionMeta extends Equatable {
  @JsonKey(includeIfNull: false)
  final String? product;
  @JsonKey(includeIfNull: false)
  final String? productIcon;
  @JsonKey(includeIfNull: false)
  final String? productName;
  @JsonKey(includeIfNull: false)
  final String type;
  @JsonKey(includeIfNull: false)
  final num total;

  const TransactionMeta({
    this.product,
    this.productIcon,
    this.productName,
    required this.type,
    required this.total,
  });

  factory TransactionMeta.fromJson(Map<String, dynamic> json) =>
      _$TransactionMetaFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionMetaToJson(this);
  @override
  List<Object> get props => [
        type,
        total,
      ];

  @override
  bool get stringify => true;
}
