// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transfer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferModel _$TransferModelFromJson(Map<String, dynamic> json) =>
    TransferModel(
      amount: json['amount'] as num,
      createdAt: json['createdAt'] as String,
      id: json['_id'] as String,
      status: json['status'] as String,
      meta: json['meta'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$TransferModelToJson(TransferModel instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'createdAt': instance.createdAt,
      '_id': instance.id,
      'status': instance.status,
      'meta': instance.meta,
    };
