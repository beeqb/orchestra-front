import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'transaction_meta.dart';

part 'transaction_model.g.dart';

@JsonSerializable(explicitToJson: true)
class TransactionModel extends Equatable {
  final num amount;
  final String createdAt;
  final String id;
  final String status;
  final TransactionMeta meta;

  const TransactionModel({
    required this.amount,
    required this.createdAt,
    required this.id,
    required this.status,
    required this.meta,
  });

  factory TransactionModel.fromJson(Map<String, dynamic> json) =>
      _$TransactionModelFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionModelToJson(this);

  @override
  List<Object> get props => [
        amount,
        createdAt,
        id,
        status,
        meta,
      ];
  @override
  bool get stringify => true;
}
