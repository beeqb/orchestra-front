// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_meta.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionMeta _$TransactionMetaFromJson(Map<String, dynamic> json) =>
    TransactionMeta(
      product: json['product'] as String?,
      productIcon: json['productIcon'] as String?,
      productName: json['productName'] as String?,
      type: json['type'] as String,
      total: json['total'] as num,
    );

Map<String, dynamic> _$TransactionMetaToJson(TransactionMeta instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('product', instance.product);
  writeNotNull('productIcon', instance.productIcon);
  writeNotNull('productName', instance.productName);
  val['type'] = instance.type;
  val['total'] = instance.total;
  return val;
}
