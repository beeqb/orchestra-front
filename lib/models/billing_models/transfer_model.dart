import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'transfer_model.g.dart';

@JsonSerializable()
class TransferModel extends Equatable {
  final num amount;
  final String createdAt;
  @JsonKey(name: '_id')
  final String id;
  final String status;
  final Map meta;

  const TransferModel({
    required this.amount,
    required this.createdAt,
    required this.id,
    required this.status,
    required this.meta,
  });

  factory TransferModel.fromJson(Map<String, dynamic> json) =>
      _$TransferModelFromJson(json);

  Map<String, dynamic> toJson() => _$TransferModelToJson(this);

  @override
  List<Object> get props => [
        amount,
        createdAt,
        id,
        status,
        meta,
      ];

  @override
  bool get stringify => true;
}
