// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionModel _$TransactionModelFromJson(Map<String, dynamic> json) =>
    TransactionModel(
      amount: json['amount'] as num,
      createdAt: json['createdAt'] as String,
      id: json['id'] as String,
      status: json['status'] as String,
      meta: TransactionMeta.fromJson(json['meta'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TransactionModelToJson(TransactionModel instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'createdAt': instance.createdAt,
      'id': instance.id,
      'status': instance.status,
      'meta': instance.meta.toJson(),
    };
