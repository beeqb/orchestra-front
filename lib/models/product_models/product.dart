// import 'package:equatable/equatable.dart';
// import 'package:json_annotation/json_annotation.dart';

// import '../enums/enums.dart';
// import '../enums/product_category_type.dart';
// import 'adress.dart';
// import 'pricing_and_business.dart';
// import 'product_launcher.dart';
// import 'stats.dart';

// part 'product.g.dart';

// @JsonSerializable(explicitToJson: true)
// class Product extends Equatable {
//   @JsonKey(includeIfNull: false)
//   final Stats? stats;
//   @JsonKey(includeIfNull: false)
//   final List<String>? photoDescriptionUrls;
//   @JsonKey(includeIfNull: false)
//   final List<String>? buyByUser;
//   @JsonKey(includeIfNull: false)
//   final List<String>? apps;
//   @JsonKey(includeIfNull: false, name: '_id')
//   final String id;
//   @JsonKey(includeIfNull: false)
//   final ProductStatusType status;
//   @JsonKey(includeIfNull: false)
//   final String userId;
//   @JsonKey(includeIfNull: false)
//   final String productType;
//   @JsonKey(includeIfNull: false)
//   final ProductCategoryType productCategory;
//   @JsonKey(includeIfNull: false)
//   final String productVersion;
//   @JsonKey(includeIfNull: false)
//   final String productEmail;
//   @JsonKey(includeIfNull: false)
//   final String productURL;
//   @JsonKey(includeIfNull: false)
//   final String productLogoURL;
//   @JsonKey(includeIfNull: false)
//   final String productName;
//   @JsonKey(includeIfNull: false)
//   final String? productMiniPromoPictureURL;
//   @JsonKey(includeIfNull: false)
//   final String? productPromoPictureURL;
//   @JsonKey(includeIfNull: false)
//   final String productDescription;
//   @JsonKey(includeIfNull: false)
//   final String howItWork;
//   @JsonKey(includeIfNull: false)
//   final String videoDescription;
//   @JsonKey(includeIfNull: false)
//   final Address address;
//   @JsonKey(includeIfNull: false)
//   final PricingAndBusiness pricingAndBusiness;
//   @JsonKey(includeIfNull: false)
//   final ProductLauncher productLauncher;
//   @JsonKey(includeIfNull: false)
//   final String? createdAt;
//   @JsonKey(includeIfNull: false)
//   final String? updatedAt;
//   @JsonKey(includeIfNull: false)
//   final int? iV;
//   @JsonKey(includeIfNull: false)
//   final bool? purchased;
//   @JsonKey(includeIfNull: false)
//   final String? rank;
//   @JsonKey(includeIfNull: false)
//   final String productContributor;

//   Product({
//     this.stats,
//     this.photoDescriptionUrls,
//     this.buyByUser,
//     this.apps,
//     required this.id,
//     required this.status,
//     required this.userId,
//     required this.productType,
//     required this.productCategory,
//     required this.productVersion,
//     required this.productEmail,
//     required this.productURL,
//     required this.productLogoURL,
//     required this.productName,
//     this.productMiniPromoPictureURL,
//     this.productPromoPictureURL,
//     required this.productDescription,
//     required this.howItWork,
//     required this.videoDescription,
//     required this.address,
//     required this.pricingAndBusiness,
//     required this.productLauncher,
//     this.createdAt,
//     this.updatedAt,
//     this.iV,
//     this.purchased,
//     this.rank,
//     required this.productContributor,
//   });

//   factory Product.fromJson(Map<String, dynamic> json) =>
//       _$ProductFromJson(json);

//   Map<String, dynamic> toJson() => _$ProductToJson(this);

//   @override
//   List<Object> get props => [id];

//   @override
//   bool get stringify => true;

//   Product copyWith({
//     Stats? stats,
//     List<String>? photoDescriptionUrls,
//     List<String>? buyByUser,
//     List<String>? apps,
//     String? id,
//     ProductStatusType? status,
//     String? userId,
//     String? productType,
//     ProductCategoryType? productCategory,
//     String? productVersion,
//     String? productEmail,
//     String? productURL,
//     String? productLogoURL,
//     String? productName,
//     String? productMiniPromoPictureURL,
//     String? productPromoPictureURL,
//     String? productDescription,
//     String? howItWork,
//     String? videoDescription,
//     Address? address,
//     PricingAndBusiness? pricingAndBusiness,
//     ProductLauncher? productLauncher,
//     String? createdAt,
//     String? updatedAt,
//     int? iV,
//     bool? purchased,
//     String? rank,
//     String? productContributor,
//   }) {
//     return Product(
//       stats: stats ?? this.stats,
//       photoDescriptionUrls: photoDescriptionUrls ?? this.photoDescriptionUrls,
//       buyByUser: buyByUser ?? this.buyByUser,
//       apps: apps ?? this.apps,
//       id: id ?? this.id,
//       status: status ?? this.status,
//       userId: userId ?? this.userId,
//       productType: productType ?? this.productType,
//       productCategory: productCategory ?? this.productCategory,
//       productVersion: productVersion ?? this.productVersion,
//       productEmail: productEmail ?? this.productEmail,
//       productURL: productURL ?? this.productURL,
//       productLogoURL: productLogoURL ?? this.productLogoURL,
//       productName: productName ?? this.productName,
//       productMiniPromoPictureURL:
//           productMiniPromoPictureURL ?? this.productMiniPromoPictureURL,
//       productPromoPictureURL:
//           productPromoPictureURL ?? this.productPromoPictureURL,
//       productDescription: productDescription ?? this.productDescription,
//       howItWork: howItWork ?? this.howItWork,
//       videoDescription: videoDescription ?? this.videoDescription,
//       address: address ?? this.address,
//       pricingAndBusiness: pricingAndBusiness ?? this.pricingAndBusiness,
//       productLauncher: productLauncher ?? this.productLauncher,
//       createdAt: createdAt ?? this.createdAt,
//       updatedAt: updatedAt ?? this.updatedAt,
//       iV: iV ?? this.iV,
//       purchased: purchased ?? this.purchased,
//       rank: rank ?? this.rank,
//       productContributor: productContributor ?? this.productContributor,
//     );
//   }
// }
