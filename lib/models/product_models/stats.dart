import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'stats.g.dart';

@JsonSerializable()
class Stats extends Equatable {
  @JsonKey(defaultValue: 0)
  final int sales;
  @JsonKey(defaultValue: 0)
  final int users;
  @JsonKey(defaultValue: 0)
  final int launches;
  @JsonKey(defaultValue: 0)
  final num platformRevenue;
  @JsonKey(defaultValue: 0)
  final num contributorRevenue;

  const Stats(
      {required this.sales,
      required this.users,
      required this.launches,
      required this.platformRevenue,
      required this.contributorRevenue});

  factory Stats.fromJson(Map<String, dynamic> json) => _$StatsFromJson(json);

  Map<String, dynamic> toJson() => _$StatsToJson(this);

  @override
  List<Object> get props => [
        sales,
        launches,
        platformRevenue,
        contributorRevenue,
      ];

  @override
  bool get stringify => true;
}
