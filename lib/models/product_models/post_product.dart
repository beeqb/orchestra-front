import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/product_category_type.dart';
import 'package:orchestra/models/enums/product_status_type.dart';
import 'package:orchestra/models/product_models/adress.dart';
import 'package:orchestra/models/product_models/pricing_and_business.dart';
import 'package:orchestra/models/product_models/product_launcher.dart';
import 'package:orchestra/models/product_models/stats.dart';

import '../enums/business_model_type.dart';
import '../enums/data_collecting_type.dart';
import '../enums/data_processing_type.dart';
import '../enums/incoming_data_type.dart';
import '../enums/outgoing_data_type.dart';
part 'post_product.freezed.dart';
part 'post_product.g.dart';

@freezed
class PostProduct with _$PostProduct {
  factory PostProduct({
    Stats? stats,
    List<String>? photoDescriptionUrls,
    List<String>? buyByUser,
    List<String>? apps,
    required ProductStatusType status,
    required String userId,
    required String productType,
    required ProductCategoryType productCategory,
    required String productVersion,
    required String productEmail,
    required String productURL,
    required String productLogoURL,
    required String productName,
    String? productMiniPromoPictureURL,
    String? productPromoPictureURL,
    required String productDescription,
    required String howItWork,
    required String videoDescription,
    required Address address,
    required PricingAndBusiness pricingAndBusiness,
    required ProductLauncher productLauncher,
    required DataCollectingType dataCollectingType,
    required IncomingDataType incomingDataType,
    required OutgoingDataType outgoingDataType,
    required DataProcessingType dataProcessingType,
    required BusinessModelType businessModelType,
    String? createdAt,
    String? updatedAt,
    int? iV,
    bool? purchased,
    String? rank,
    required String productContributor,
  }) = _PostProduct;

  factory PostProduct.fromJson(Map<String, dynamic> json) =>
      _$PostProductFromJson(json);
}
