// import 'package:equatable/equatable.dart';
// import 'package:json_annotation/json_annotation.dart';
// import '../enums/business_model_type.dart';

// part 'pricing_and_business.g.dart';

// @JsonSerializable()
// class PricingAndBusiness extends Equatable {
//   @JsonKey(includeIfNull: false)
//   final List<String>? testEndsAfter;
//   @JsonKey(includeIfNull: false)
//   final String? sId;
//   @JsonKey(includeIfNull: false)
//   final String? createDate;
//   @JsonKey(
//     includeIfNull: false,
//     unknownEnumValue: BusinessModelType.oneTime,
//   )
//   final BusinessModelType? model;
//   @JsonKey(includeIfNull: false)
//   final num? price;
//   @JsonKey(includeIfNull: false)
//   final int? marketplaceReward;
//   @JsonKey(includeIfNull: false)
//   final bool? freeForTest;

//   PricingAndBusiness(
//       {this.testEndsAfter,
//       this.sId,
//       this.createDate,
//       this.model,
//       this.price,
//       this.marketplaceReward,
//       this.freeForTest});

//   factory PricingAndBusiness.fromJson(Map<String, dynamic> json) =>
//       _$PricingAndBusinessFromJson(json);

//   Map<String, dynamic> toJson() => _$PricingAndBusinessToJson(this);

//   @override
//   List<Object> get props => [
//         testEndsAfter!,
//         sId!,
//         createDate!,
//         model!,
//         price!,
//         marketplaceReward!,
//         freeForTest!,
//       ];

//   @override
//   bool get stringify => true;
// }

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/business_model_type.dart';
part 'pricing_and_business.freezed.dart';
part 'pricing_and_business.g.dart';

@freezed
class PricingAndBusiness with _$PricingAndBusiness {
  factory PricingAndBusiness({
    @JsonKey(includeIfNull: false)
        final List<String>? testEndsAfter,
    @JsonKey(includeIfNull: false)
        final String? sId,
    @JsonKey(includeIfNull: false)
        final String? createDate,
    @JsonKey(
      includeIfNull: false,
      unknownEnumValue: BusinessModelType.oneTime,
    )
        final BusinessModelType? model,
    @JsonKey(includeIfNull: false)
        final num? price,
    @JsonKey(includeIfNull: false)
        final int? marketplaceReward,
    @JsonKey(includeIfNull: false)
        final bool? freeForTest,
  }) = _PricingAndBusiness;

  factory PricingAndBusiness.fromJson(Map<String, dynamic> json) =>
      _$PricingAndBusinessFromJson(json);
}
