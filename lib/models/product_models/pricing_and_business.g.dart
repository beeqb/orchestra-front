// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pricing_and_business.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PricingAndBusiness _$$_PricingAndBusinessFromJson(
        Map<String, dynamic> json) =>
    _$_PricingAndBusiness(
      testEndsAfter: (json['testEndsAfter'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      sId: json['sId'] as String?,
      createDate: json['createDate'] as String?,
      model: $enumDecodeNullable(_$BusinessModelTypeEnumMap, json['model'],
          unknownValue: BusinessModelType.oneTime),
      price: json['price'] as num?,
      marketplaceReward: json['marketplaceReward'] as int?,
      freeForTest: json['freeForTest'] as bool?,
    );

Map<String, dynamic> _$$_PricingAndBusinessToJson(
    _$_PricingAndBusiness instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('testEndsAfter', instance.testEndsAfter);
  writeNotNull('sId', instance.sId);
  writeNotNull('createDate', instance.createDate);
  writeNotNull('model', _$BusinessModelTypeEnumMap[instance.model]);
  writeNotNull('price', instance.price);
  writeNotNull('marketplaceReward', instance.marketplaceReward);
  writeNotNull('freeForTest', instance.freeForTest);
  return val;
}

const _$BusinessModelTypeEnumMap = {
  BusinessModelType.oneTime: 0,
  BusinessModelType.perRequest: 1,
  BusinessModelType.monthlySubscription: 2,
};
