// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_launcher.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProductLauncher _$$_ProductLauncherFromJson(Map<String, dynamic> json) =>
    _$_ProductLauncher(
      sId: json['sId'] as String?,
      createDate: json['createDate'] as String?,
      productLauncherFileUrl: json['productLauncherFileUrl'] as String?,
      incomingDataInfo: $enumDecodeNullable(
          _$IncomingDataTypeEnumMap, json['incomingDataInfo'],
          unknownValue: IncomingDataType.fixedSource),
      outgoingDataInfo: $enumDecodeNullable(
          _$OutgoingDataTypeEnumMap, json['outgoingDataInfo'],
          unknownValue: OutgoingDataType.fixedChanel),
      dataProcessingType: $enumDecodeNullable(
          _$DataProcessingTypeEnumMap, json['dataProcessingType'],
          unknownValue: DataProcessingType.onPremise),
      dataCollecting: $enumDecodeNullable(
          _$DataCollectingTypeEnumMap, json['dataCollecting'],
          unknownValue: DataCollectingType.noDataCollection),
    );

Map<String, dynamic> _$$_ProductLauncherToJson(_$_ProductLauncher instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('sId', instance.sId);
  writeNotNull('createDate', instance.createDate);
  writeNotNull('productLauncherFileUrl', instance.productLauncherFileUrl);
  writeNotNull(
      'incomingDataInfo', _$IncomingDataTypeEnumMap[instance.incomingDataInfo]);
  writeNotNull(
      'outgoingDataInfo', _$OutgoingDataTypeEnumMap[instance.outgoingDataInfo]);
  writeNotNull('dataProcessingType',
      _$DataProcessingTypeEnumMap[instance.dataProcessingType]);
  writeNotNull(
      'dataCollecting', _$DataCollectingTypeEnumMap[instance.dataCollecting]);
  return val;
}

const _$IncomingDataTypeEnumMap = {
  IncomingDataType.fixedSource: 0,
  IncomingDataType.editableSource: 1,
  IncomingDataType.dataProvederNeeded: 2,
};

const _$OutgoingDataTypeEnumMap = {
  OutgoingDataType.fixedChanel: 0,
  OutgoingDataType.editableChanel: 1,
  OutgoingDataType.resultStreamedNeeded: 2,
};

const _$DataProcessingTypeEnumMap = {
  DataProcessingType.onPremise: 0,
  DataProcessingType.cloud: 1,
  DataProcessingType.mixed: 2,
};

const _$DataCollectingTypeEnumMap = {
  DataCollectingType.noDataCollection: 0,
  DataCollectingType.collectionProcessingData: 1,
  DataCollectingType.depersonalizedDataCollection: 2,
};
