// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PostProduct _$$_PostProductFromJson(Map<String, dynamic> json) =>
    _$_PostProduct(
      stats: json['stats'] == null
          ? null
          : Stats.fromJson(json['stats'] as Map<String, dynamic>),
      photoDescriptionUrls: (json['photoDescriptionUrls'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      buyByUser: (json['buyByUser'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      apps: (json['apps'] as List<dynamic>?)?.map((e) => e as String).toList(),
      status: $enumDecode(_$ProductStatusTypeEnumMap, json['status']),
      userId: json['userId'] as String,
      productType: json['productType'] as String,
      productCategory:
          $enumDecode(_$ProductCategoryTypeEnumMap, json['productCategory']),
      productVersion: json['productVersion'] as String,
      productEmail: json['productEmail'] as String,
      productURL: json['productURL'] as String,
      productLogoURL: json['productLogoURL'] as String,
      productName: json['productName'] as String,
      productMiniPromoPictureURL: json['productMiniPromoPictureURL'] as String?,
      productPromoPictureURL: json['productPromoPictureURL'] as String?,
      productDescription: json['productDescription'] as String,
      howItWork: json['howItWork'] as String,
      videoDescription: json['videoDescription'] as String,
      address: Address.fromJson(json['address'] as Map<String, dynamic>),
      pricingAndBusiness: PricingAndBusiness.fromJson(
          json['pricingAndBusiness'] as Map<String, dynamic>),
      productLauncher: ProductLauncher.fromJson(
          json['productLauncher'] as Map<String, dynamic>),
      dataCollectingType:
          $enumDecode(_$DataCollectingTypeEnumMap, json['dataCollectingType']),
      incomingDataType:
          $enumDecode(_$IncomingDataTypeEnumMap, json['incomingDataType']),
      outgoingDataType:
          $enumDecode(_$OutgoingDataTypeEnumMap, json['outgoingDataType']),
      dataProcessingType:
          $enumDecode(_$DataProcessingTypeEnumMap, json['dataProcessingType']),
      businessModelType:
          $enumDecode(_$BusinessModelTypeEnumMap, json['businessModelType']),
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      iV: json['iV'] as int?,
      purchased: json['purchased'] as bool?,
      rank: json['rank'] as String?,
      productContributor: json['productContributor'] as String,
    );

Map<String, dynamic> _$$_PostProductToJson(_$_PostProduct instance) =>
    <String, dynamic>{
      'stats': instance.stats,
      'photoDescriptionUrls': instance.photoDescriptionUrls,
      'buyByUser': instance.buyByUser,
      'apps': instance.apps,
      'status': _$ProductStatusTypeEnumMap[instance.status]!,
      'userId': instance.userId,
      'productType': instance.productType,
      'productCategory':
          _$ProductCategoryTypeEnumMap[instance.productCategory]!,
      'productVersion': instance.productVersion,
      'productEmail': instance.productEmail,
      'productURL': instance.productURL,
      'productLogoURL': instance.productLogoURL,
      'productName': instance.productName,
      'productMiniPromoPictureURL': instance.productMiniPromoPictureURL,
      'productPromoPictureURL': instance.productPromoPictureURL,
      'productDescription': instance.productDescription,
      'howItWork': instance.howItWork,
      'videoDescription': instance.videoDescription,
      'address': instance.address,
      'pricingAndBusiness': instance.pricingAndBusiness,
      'productLauncher': instance.productLauncher,
      'dataCollectingType':
          _$DataCollectingTypeEnumMap[instance.dataCollectingType]!,
      'incomingDataType': _$IncomingDataTypeEnumMap[instance.incomingDataType]!,
      'outgoingDataType': _$OutgoingDataTypeEnumMap[instance.outgoingDataType]!,
      'dataProcessingType':
          _$DataProcessingTypeEnumMap[instance.dataProcessingType]!,
      'businessModelType':
          _$BusinessModelTypeEnumMap[instance.businessModelType]!,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'iV': instance.iV,
      'purchased': instance.purchased,
      'rank': instance.rank,
      'productContributor': instance.productContributor,
    };

const _$ProductStatusTypeEnumMap = {
  ProductStatusType.marketplaceResident: 'Marketplace resident',
  ProductStatusType.waitingForReview: 'Waiting for review',
  ProductStatusType.deleted: 'Deleted',
  ProductStatusType.deniedToPublish: 'Denied to publish',
};

const _$ProductCategoryTypeEnumMap = {
  ProductCategoryType.privateUseOnly: 'PAPP',
  ProductCategoryType.business: 'BU',
  ProductCategoryType.productivity: 'PR',
  ProductCategoryType.mentalHealth: 'MH',
  ProductCategoryType.social: 'SO',
  ProductCategoryType.finance: 'FI',
  ProductCategoryType.sales: 'SA',
  ProductCategoryType.management: 'MA',
  ProductCategoryType.marketing: 'MK',
  ProductCategoryType.accounting: 'AC',
  ProductCategoryType.utils: 'UT',
  ProductCategoryType.trading: 'TR',
  ProductCategoryType.other: 'OT',
};

const _$DataCollectingTypeEnumMap = {
  DataCollectingType.noDataCollection: 0,
  DataCollectingType.collectionProcessingData: 1,
  DataCollectingType.depersonalizedDataCollection: 2,
};

const _$IncomingDataTypeEnumMap = {
  IncomingDataType.fixedSource: 0,
  IncomingDataType.editableSource: 1,
  IncomingDataType.dataProvederNeeded: 2,
};

const _$OutgoingDataTypeEnumMap = {
  OutgoingDataType.fixedChanel: 0,
  OutgoingDataType.editableChanel: 1,
  OutgoingDataType.resultStreamedNeeded: 2,
};

const _$DataProcessingTypeEnumMap = {
  DataProcessingType.onPremise: 0,
  DataProcessingType.cloud: 1,
  DataProcessingType.mixed: 2,
};

const _$BusinessModelTypeEnumMap = {
  BusinessModelType.oneTime: 0,
  BusinessModelType.perRequest: 1,
  BusinessModelType.monthlySubscription: 2,
};
