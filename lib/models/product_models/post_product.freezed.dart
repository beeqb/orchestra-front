// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'post_product.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PostProduct _$PostProductFromJson(Map<String, dynamic> json) {
  return _PostProduct.fromJson(json);
}

/// @nodoc
mixin _$PostProduct {
  Stats? get stats => throw _privateConstructorUsedError;
  List<String>? get photoDescriptionUrls => throw _privateConstructorUsedError;
  List<String>? get buyByUser => throw _privateConstructorUsedError;
  List<String>? get apps => throw _privateConstructorUsedError;
  ProductStatusType get status => throw _privateConstructorUsedError;
  String get userId => throw _privateConstructorUsedError;
  String get productType => throw _privateConstructorUsedError;
  ProductCategoryType get productCategory => throw _privateConstructorUsedError;
  String get productVersion => throw _privateConstructorUsedError;
  String get productEmail => throw _privateConstructorUsedError;
  String get productURL => throw _privateConstructorUsedError;
  String get productLogoURL => throw _privateConstructorUsedError;
  String get productName => throw _privateConstructorUsedError;
  String? get productMiniPromoPictureURL => throw _privateConstructorUsedError;
  String? get productPromoPictureURL => throw _privateConstructorUsedError;
  String get productDescription => throw _privateConstructorUsedError;
  String get howItWork => throw _privateConstructorUsedError;
  String get videoDescription => throw _privateConstructorUsedError;
  Address get address => throw _privateConstructorUsedError;
  PricingAndBusiness get pricingAndBusiness =>
      throw _privateConstructorUsedError;
  ProductLauncher get productLauncher => throw _privateConstructorUsedError;
  DataCollectingType get dataCollectingType =>
      throw _privateConstructorUsedError;
  IncomingDataType get incomingDataType => throw _privateConstructorUsedError;
  OutgoingDataType get outgoingDataType => throw _privateConstructorUsedError;
  DataProcessingType get dataProcessingType =>
      throw _privateConstructorUsedError;
  BusinessModelType get businessModelType => throw _privateConstructorUsedError;
  String? get createdAt => throw _privateConstructorUsedError;
  String? get updatedAt => throw _privateConstructorUsedError;
  int? get iV => throw _privateConstructorUsedError;
  bool? get purchased => throw _privateConstructorUsedError;
  String? get rank => throw _privateConstructorUsedError;
  String get productContributor => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostProductCopyWith<PostProduct> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostProductCopyWith<$Res> {
  factory $PostProductCopyWith(
          PostProduct value, $Res Function(PostProduct) then) =
      _$PostProductCopyWithImpl<$Res>;
  $Res call(
      {Stats? stats,
      List<String>? photoDescriptionUrls,
      List<String>? buyByUser,
      List<String>? apps,
      ProductStatusType status,
      String userId,
      String productType,
      ProductCategoryType productCategory,
      String productVersion,
      String productEmail,
      String productURL,
      String productLogoURL,
      String productName,
      String? productMiniPromoPictureURL,
      String? productPromoPictureURL,
      String productDescription,
      String howItWork,
      String videoDescription,
      Address address,
      PricingAndBusiness pricingAndBusiness,
      ProductLauncher productLauncher,
      DataCollectingType dataCollectingType,
      IncomingDataType incomingDataType,
      OutgoingDataType outgoingDataType,
      DataProcessingType dataProcessingType,
      BusinessModelType businessModelType,
      String? createdAt,
      String? updatedAt,
      int? iV,
      bool? purchased,
      String? rank,
      String productContributor});

  $AddressCopyWith<$Res> get address;
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness;
  $ProductLauncherCopyWith<$Res> get productLauncher;
}

/// @nodoc
class _$PostProductCopyWithImpl<$Res> implements $PostProductCopyWith<$Res> {
  _$PostProductCopyWithImpl(this._value, this._then);

  final PostProduct _value;
  // ignore: unused_field
  final $Res Function(PostProduct) _then;

  @override
  $Res call({
    Object? stats = freezed,
    Object? photoDescriptionUrls = freezed,
    Object? buyByUser = freezed,
    Object? apps = freezed,
    Object? status = freezed,
    Object? userId = freezed,
    Object? productType = freezed,
    Object? productCategory = freezed,
    Object? productVersion = freezed,
    Object? productEmail = freezed,
    Object? productURL = freezed,
    Object? productLogoURL = freezed,
    Object? productName = freezed,
    Object? productMiniPromoPictureURL = freezed,
    Object? productPromoPictureURL = freezed,
    Object? productDescription = freezed,
    Object? howItWork = freezed,
    Object? videoDescription = freezed,
    Object? address = freezed,
    Object? pricingAndBusiness = freezed,
    Object? productLauncher = freezed,
    Object? dataCollectingType = freezed,
    Object? incomingDataType = freezed,
    Object? outgoingDataType = freezed,
    Object? dataProcessingType = freezed,
    Object? businessModelType = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? iV = freezed,
    Object? purchased = freezed,
    Object? rank = freezed,
    Object? productContributor = freezed,
  }) {
    return _then(_value.copyWith(
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as Stats?,
      photoDescriptionUrls: photoDescriptionUrls == freezed
          ? _value.photoDescriptionUrls
          : photoDescriptionUrls // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      buyByUser: buyByUser == freezed
          ? _value.buyByUser
          : buyByUser // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      apps: apps == freezed
          ? _value.apps
          : apps // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProductStatusType,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      productType: productType == freezed
          ? _value.productType
          : productType // ignore: cast_nullable_to_non_nullable
              as String,
      productCategory: productCategory == freezed
          ? _value.productCategory
          : productCategory // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      productVersion: productVersion == freezed
          ? _value.productVersion
          : productVersion // ignore: cast_nullable_to_non_nullable
              as String,
      productEmail: productEmail == freezed
          ? _value.productEmail
          : productEmail // ignore: cast_nullable_to_non_nullable
              as String,
      productURL: productURL == freezed
          ? _value.productURL
          : productURL // ignore: cast_nullable_to_non_nullable
              as String,
      productLogoURL: productLogoURL == freezed
          ? _value.productLogoURL
          : productLogoURL // ignore: cast_nullable_to_non_nullable
              as String,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String,
      productMiniPromoPictureURL: productMiniPromoPictureURL == freezed
          ? _value.productMiniPromoPictureURL
          : productMiniPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productPromoPictureURL: productPromoPictureURL == freezed
          ? _value.productPromoPictureURL
          : productPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productDescription: productDescription == freezed
          ? _value.productDescription
          : productDescription // ignore: cast_nullable_to_non_nullable
              as String,
      howItWork: howItWork == freezed
          ? _value.howItWork
          : howItWork // ignore: cast_nullable_to_non_nullable
              as String,
      videoDescription: videoDescription == freezed
          ? _value.videoDescription
          : videoDescription // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as Address,
      pricingAndBusiness: pricingAndBusiness == freezed
          ? _value.pricingAndBusiness
          : pricingAndBusiness // ignore: cast_nullable_to_non_nullable
              as PricingAndBusiness,
      productLauncher: productLauncher == freezed
          ? _value.productLauncher
          : productLauncher // ignore: cast_nullable_to_non_nullable
              as ProductLauncher,
      dataCollectingType: dataCollectingType == freezed
          ? _value.dataCollectingType
          : dataCollectingType // ignore: cast_nullable_to_non_nullable
              as DataCollectingType,
      incomingDataType: incomingDataType == freezed
          ? _value.incomingDataType
          : incomingDataType // ignore: cast_nullable_to_non_nullable
              as IncomingDataType,
      outgoingDataType: outgoingDataType == freezed
          ? _value.outgoingDataType
          : outgoingDataType // ignore: cast_nullable_to_non_nullable
              as OutgoingDataType,
      dataProcessingType: dataProcessingType == freezed
          ? _value.dataProcessingType
          : dataProcessingType // ignore: cast_nullable_to_non_nullable
              as DataProcessingType,
      businessModelType: businessModelType == freezed
          ? _value.businessModelType
          : businessModelType // ignore: cast_nullable_to_non_nullable
              as BusinessModelType,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      iV: iV == freezed
          ? _value.iV
          : iV // ignore: cast_nullable_to_non_nullable
              as int?,
      purchased: purchased == freezed
          ? _value.purchased
          : purchased // ignore: cast_nullable_to_non_nullable
              as bool?,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as String?,
      productContributor: productContributor == freezed
          ? _value.productContributor
          : productContributor // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }

  @override
  $AddressCopyWith<$Res> get address {
    return $AddressCopyWith<$Res>(_value.address, (value) {
      return _then(_value.copyWith(address: value));
    });
  }

  @override
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness {
    return $PricingAndBusinessCopyWith<$Res>(_value.pricingAndBusiness,
        (value) {
      return _then(_value.copyWith(pricingAndBusiness: value));
    });
  }

  @override
  $ProductLauncherCopyWith<$Res> get productLauncher {
    return $ProductLauncherCopyWith<$Res>(_value.productLauncher, (value) {
      return _then(_value.copyWith(productLauncher: value));
    });
  }
}

/// @nodoc
abstract class _$$_PostProductCopyWith<$Res>
    implements $PostProductCopyWith<$Res> {
  factory _$$_PostProductCopyWith(
          _$_PostProduct value, $Res Function(_$_PostProduct) then) =
      __$$_PostProductCopyWithImpl<$Res>;
  @override
  $Res call(
      {Stats? stats,
      List<String>? photoDescriptionUrls,
      List<String>? buyByUser,
      List<String>? apps,
      ProductStatusType status,
      String userId,
      String productType,
      ProductCategoryType productCategory,
      String productVersion,
      String productEmail,
      String productURL,
      String productLogoURL,
      String productName,
      String? productMiniPromoPictureURL,
      String? productPromoPictureURL,
      String productDescription,
      String howItWork,
      String videoDescription,
      Address address,
      PricingAndBusiness pricingAndBusiness,
      ProductLauncher productLauncher,
      DataCollectingType dataCollectingType,
      IncomingDataType incomingDataType,
      OutgoingDataType outgoingDataType,
      DataProcessingType dataProcessingType,
      BusinessModelType businessModelType,
      String? createdAt,
      String? updatedAt,
      int? iV,
      bool? purchased,
      String? rank,
      String productContributor});

  @override
  $AddressCopyWith<$Res> get address;
  @override
  $PricingAndBusinessCopyWith<$Res> get pricingAndBusiness;
  @override
  $ProductLauncherCopyWith<$Res> get productLauncher;
}

/// @nodoc
class __$$_PostProductCopyWithImpl<$Res> extends _$PostProductCopyWithImpl<$Res>
    implements _$$_PostProductCopyWith<$Res> {
  __$$_PostProductCopyWithImpl(
      _$_PostProduct _value, $Res Function(_$_PostProduct) _then)
      : super(_value, (v) => _then(v as _$_PostProduct));

  @override
  _$_PostProduct get _value => super._value as _$_PostProduct;

  @override
  $Res call({
    Object? stats = freezed,
    Object? photoDescriptionUrls = freezed,
    Object? buyByUser = freezed,
    Object? apps = freezed,
    Object? status = freezed,
    Object? userId = freezed,
    Object? productType = freezed,
    Object? productCategory = freezed,
    Object? productVersion = freezed,
    Object? productEmail = freezed,
    Object? productURL = freezed,
    Object? productLogoURL = freezed,
    Object? productName = freezed,
    Object? productMiniPromoPictureURL = freezed,
    Object? productPromoPictureURL = freezed,
    Object? productDescription = freezed,
    Object? howItWork = freezed,
    Object? videoDescription = freezed,
    Object? address = freezed,
    Object? pricingAndBusiness = freezed,
    Object? productLauncher = freezed,
    Object? dataCollectingType = freezed,
    Object? incomingDataType = freezed,
    Object? outgoingDataType = freezed,
    Object? dataProcessingType = freezed,
    Object? businessModelType = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? iV = freezed,
    Object? purchased = freezed,
    Object? rank = freezed,
    Object? productContributor = freezed,
  }) {
    return _then(_$_PostProduct(
      stats: stats == freezed
          ? _value.stats
          : stats // ignore: cast_nullable_to_non_nullable
              as Stats?,
      photoDescriptionUrls: photoDescriptionUrls == freezed
          ? _value._photoDescriptionUrls
          : photoDescriptionUrls // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      buyByUser: buyByUser == freezed
          ? _value._buyByUser
          : buyByUser // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      apps: apps == freezed
          ? _value._apps
          : apps // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as ProductStatusType,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      productType: productType == freezed
          ? _value.productType
          : productType // ignore: cast_nullable_to_non_nullable
              as String,
      productCategory: productCategory == freezed
          ? _value.productCategory
          : productCategory // ignore: cast_nullable_to_non_nullable
              as ProductCategoryType,
      productVersion: productVersion == freezed
          ? _value.productVersion
          : productVersion // ignore: cast_nullable_to_non_nullable
              as String,
      productEmail: productEmail == freezed
          ? _value.productEmail
          : productEmail // ignore: cast_nullable_to_non_nullable
              as String,
      productURL: productURL == freezed
          ? _value.productURL
          : productURL // ignore: cast_nullable_to_non_nullable
              as String,
      productLogoURL: productLogoURL == freezed
          ? _value.productLogoURL
          : productLogoURL // ignore: cast_nullable_to_non_nullable
              as String,
      productName: productName == freezed
          ? _value.productName
          : productName // ignore: cast_nullable_to_non_nullable
              as String,
      productMiniPromoPictureURL: productMiniPromoPictureURL == freezed
          ? _value.productMiniPromoPictureURL
          : productMiniPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productPromoPictureURL: productPromoPictureURL == freezed
          ? _value.productPromoPictureURL
          : productPromoPictureURL // ignore: cast_nullable_to_non_nullable
              as String?,
      productDescription: productDescription == freezed
          ? _value.productDescription
          : productDescription // ignore: cast_nullable_to_non_nullable
              as String,
      howItWork: howItWork == freezed
          ? _value.howItWork
          : howItWork // ignore: cast_nullable_to_non_nullable
              as String,
      videoDescription: videoDescription == freezed
          ? _value.videoDescription
          : videoDescription // ignore: cast_nullable_to_non_nullable
              as String,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as Address,
      pricingAndBusiness: pricingAndBusiness == freezed
          ? _value.pricingAndBusiness
          : pricingAndBusiness // ignore: cast_nullable_to_non_nullable
              as PricingAndBusiness,
      productLauncher: productLauncher == freezed
          ? _value.productLauncher
          : productLauncher // ignore: cast_nullable_to_non_nullable
              as ProductLauncher,
      dataCollectingType: dataCollectingType == freezed
          ? _value.dataCollectingType
          : dataCollectingType // ignore: cast_nullable_to_non_nullable
              as DataCollectingType,
      incomingDataType: incomingDataType == freezed
          ? _value.incomingDataType
          : incomingDataType // ignore: cast_nullable_to_non_nullable
              as IncomingDataType,
      outgoingDataType: outgoingDataType == freezed
          ? _value.outgoingDataType
          : outgoingDataType // ignore: cast_nullable_to_non_nullable
              as OutgoingDataType,
      dataProcessingType: dataProcessingType == freezed
          ? _value.dataProcessingType
          : dataProcessingType // ignore: cast_nullable_to_non_nullable
              as DataProcessingType,
      businessModelType: businessModelType == freezed
          ? _value.businessModelType
          : businessModelType // ignore: cast_nullable_to_non_nullable
              as BusinessModelType,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      iV: iV == freezed
          ? _value.iV
          : iV // ignore: cast_nullable_to_non_nullable
              as int?,
      purchased: purchased == freezed
          ? _value.purchased
          : purchased // ignore: cast_nullable_to_non_nullable
              as bool?,
      rank: rank == freezed
          ? _value.rank
          : rank // ignore: cast_nullable_to_non_nullable
              as String?,
      productContributor: productContributor == freezed
          ? _value.productContributor
          : productContributor // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PostProduct implements _PostProduct {
  _$_PostProduct(
      {this.stats,
      final List<String>? photoDescriptionUrls,
      final List<String>? buyByUser,
      final List<String>? apps,
      required this.status,
      required this.userId,
      required this.productType,
      required this.productCategory,
      required this.productVersion,
      required this.productEmail,
      required this.productURL,
      required this.productLogoURL,
      required this.productName,
      this.productMiniPromoPictureURL,
      this.productPromoPictureURL,
      required this.productDescription,
      required this.howItWork,
      required this.videoDescription,
      required this.address,
      required this.pricingAndBusiness,
      required this.productLauncher,
      required this.dataCollectingType,
      required this.incomingDataType,
      required this.outgoingDataType,
      required this.dataProcessingType,
      required this.businessModelType,
      this.createdAt,
      this.updatedAt,
      this.iV,
      this.purchased,
      this.rank,
      required this.productContributor})
      : _photoDescriptionUrls = photoDescriptionUrls,
        _buyByUser = buyByUser,
        _apps = apps;

  factory _$_PostProduct.fromJson(Map<String, dynamic> json) =>
      _$$_PostProductFromJson(json);

  @override
  final Stats? stats;
  final List<String>? _photoDescriptionUrls;
  @override
  List<String>? get photoDescriptionUrls {
    final value = _photoDescriptionUrls;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String>? _buyByUser;
  @override
  List<String>? get buyByUser {
    final value = _buyByUser;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String>? _apps;
  @override
  List<String>? get apps {
    final value = _apps;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final ProductStatusType status;
  @override
  final String userId;
  @override
  final String productType;
  @override
  final ProductCategoryType productCategory;
  @override
  final String productVersion;
  @override
  final String productEmail;
  @override
  final String productURL;
  @override
  final String productLogoURL;
  @override
  final String productName;
  @override
  final String? productMiniPromoPictureURL;
  @override
  final String? productPromoPictureURL;
  @override
  final String productDescription;
  @override
  final String howItWork;
  @override
  final String videoDescription;
  @override
  final Address address;
  @override
  final PricingAndBusiness pricingAndBusiness;
  @override
  final ProductLauncher productLauncher;
  @override
  final DataCollectingType dataCollectingType;
  @override
  final IncomingDataType incomingDataType;
  @override
  final OutgoingDataType outgoingDataType;
  @override
  final DataProcessingType dataProcessingType;
  @override
  final BusinessModelType businessModelType;
  @override
  final String? createdAt;
  @override
  final String? updatedAt;
  @override
  final int? iV;
  @override
  final bool? purchased;
  @override
  final String? rank;
  @override
  final String productContributor;

  @override
  String toString() {
    return 'PostProduct(stats: $stats, photoDescriptionUrls: $photoDescriptionUrls, buyByUser: $buyByUser, apps: $apps, status: $status, userId: $userId, productType: $productType, productCategory: $productCategory, productVersion: $productVersion, productEmail: $productEmail, productURL: $productURL, productLogoURL: $productLogoURL, productName: $productName, productMiniPromoPictureURL: $productMiniPromoPictureURL, productPromoPictureURL: $productPromoPictureURL, productDescription: $productDescription, howItWork: $howItWork, videoDescription: $videoDescription, address: $address, pricingAndBusiness: $pricingAndBusiness, productLauncher: $productLauncher, dataCollectingType: $dataCollectingType, incomingDataType: $incomingDataType, outgoingDataType: $outgoingDataType, dataProcessingType: $dataProcessingType, businessModelType: $businessModelType, createdAt: $createdAt, updatedAt: $updatedAt, iV: $iV, purchased: $purchased, rank: $rank, productContributor: $productContributor)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PostProduct &&
            const DeepCollectionEquality().equals(other.stats, stats) &&
            const DeepCollectionEquality()
                .equals(other._photoDescriptionUrls, _photoDescriptionUrls) &&
            const DeepCollectionEquality()
                .equals(other._buyByUser, _buyByUser) &&
            const DeepCollectionEquality().equals(other._apps, _apps) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality()
                .equals(other.productType, productType) &&
            const DeepCollectionEquality()
                .equals(other.productCategory, productCategory) &&
            const DeepCollectionEquality()
                .equals(other.productVersion, productVersion) &&
            const DeepCollectionEquality()
                .equals(other.productEmail, productEmail) &&
            const DeepCollectionEquality()
                .equals(other.productURL, productURL) &&
            const DeepCollectionEquality()
                .equals(other.productLogoURL, productLogoURL) &&
            const DeepCollectionEquality()
                .equals(other.productName, productName) &&
            const DeepCollectionEquality().equals(
                other.productMiniPromoPictureURL, productMiniPromoPictureURL) &&
            const DeepCollectionEquality()
                .equals(other.productPromoPictureURL, productPromoPictureURL) &&
            const DeepCollectionEquality()
                .equals(other.productDescription, productDescription) &&
            const DeepCollectionEquality().equals(other.howItWork, howItWork) &&
            const DeepCollectionEquality()
                .equals(other.videoDescription, videoDescription) &&
            const DeepCollectionEquality().equals(other.address, address) &&
            const DeepCollectionEquality()
                .equals(other.pricingAndBusiness, pricingAndBusiness) &&
            const DeepCollectionEquality()
                .equals(other.productLauncher, productLauncher) &&
            const DeepCollectionEquality()
                .equals(other.dataCollectingType, dataCollectingType) &&
            const DeepCollectionEquality()
                .equals(other.incomingDataType, incomingDataType) &&
            const DeepCollectionEquality()
                .equals(other.outgoingDataType, outgoingDataType) &&
            const DeepCollectionEquality()
                .equals(other.dataProcessingType, dataProcessingType) &&
            const DeepCollectionEquality()
                .equals(other.businessModelType, businessModelType) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt) &&
            const DeepCollectionEquality().equals(other.updatedAt, updatedAt) &&
            const DeepCollectionEquality().equals(other.iV, iV) &&
            const DeepCollectionEquality().equals(other.purchased, purchased) &&
            const DeepCollectionEquality().equals(other.rank, rank) &&
            const DeepCollectionEquality()
                .equals(other.productContributor, productContributor));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(stats),
        const DeepCollectionEquality().hash(_photoDescriptionUrls),
        const DeepCollectionEquality().hash(_buyByUser),
        const DeepCollectionEquality().hash(_apps),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(userId),
        const DeepCollectionEquality().hash(productType),
        const DeepCollectionEquality().hash(productCategory),
        const DeepCollectionEquality().hash(productVersion),
        const DeepCollectionEquality().hash(productEmail),
        const DeepCollectionEquality().hash(productURL),
        const DeepCollectionEquality().hash(productLogoURL),
        const DeepCollectionEquality().hash(productName),
        const DeepCollectionEquality().hash(productMiniPromoPictureURL),
        const DeepCollectionEquality().hash(productPromoPictureURL),
        const DeepCollectionEquality().hash(productDescription),
        const DeepCollectionEquality().hash(howItWork),
        const DeepCollectionEquality().hash(videoDescription),
        const DeepCollectionEquality().hash(address),
        const DeepCollectionEquality().hash(pricingAndBusiness),
        const DeepCollectionEquality().hash(productLauncher),
        const DeepCollectionEquality().hash(dataCollectingType),
        const DeepCollectionEquality().hash(incomingDataType),
        const DeepCollectionEquality().hash(outgoingDataType),
        const DeepCollectionEquality().hash(dataProcessingType),
        const DeepCollectionEquality().hash(businessModelType),
        const DeepCollectionEquality().hash(createdAt),
        const DeepCollectionEquality().hash(updatedAt),
        const DeepCollectionEquality().hash(iV),
        const DeepCollectionEquality().hash(purchased),
        const DeepCollectionEquality().hash(rank),
        const DeepCollectionEquality().hash(productContributor)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_PostProductCopyWith<_$_PostProduct> get copyWith =>
      __$$_PostProductCopyWithImpl<_$_PostProduct>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PostProductToJson(
      this,
    );
  }
}

abstract class _PostProduct implements PostProduct {
  factory _PostProduct(
      {final Stats? stats,
      final List<String>? photoDescriptionUrls,
      final List<String>? buyByUser,
      final List<String>? apps,
      required final ProductStatusType status,
      required final String userId,
      required final String productType,
      required final ProductCategoryType productCategory,
      required final String productVersion,
      required final String productEmail,
      required final String productURL,
      required final String productLogoURL,
      required final String productName,
      final String? productMiniPromoPictureURL,
      final String? productPromoPictureURL,
      required final String productDescription,
      required final String howItWork,
      required final String videoDescription,
      required final Address address,
      required final PricingAndBusiness pricingAndBusiness,
      required final ProductLauncher productLauncher,
      required final DataCollectingType dataCollectingType,
      required final IncomingDataType incomingDataType,
      required final OutgoingDataType outgoingDataType,
      required final DataProcessingType dataProcessingType,
      required final BusinessModelType businessModelType,
      final String? createdAt,
      final String? updatedAt,
      final int? iV,
      final bool? purchased,
      final String? rank,
      required final String productContributor}) = _$_PostProduct;

  factory _PostProduct.fromJson(Map<String, dynamic> json) =
      _$_PostProduct.fromJson;

  @override
  Stats? get stats;
  @override
  List<String>? get photoDescriptionUrls;
  @override
  List<String>? get buyByUser;
  @override
  List<String>? get apps;
  @override
  ProductStatusType get status;
  @override
  String get userId;
  @override
  String get productType;
  @override
  ProductCategoryType get productCategory;
  @override
  String get productVersion;
  @override
  String get productEmail;
  @override
  String get productURL;
  @override
  String get productLogoURL;
  @override
  String get productName;
  @override
  String? get productMiniPromoPictureURL;
  @override
  String? get productPromoPictureURL;
  @override
  String get productDescription;
  @override
  String get howItWork;
  @override
  String get videoDescription;
  @override
  Address get address;
  @override
  PricingAndBusiness get pricingAndBusiness;
  @override
  ProductLauncher get productLauncher;
  @override
  DataCollectingType get dataCollectingType;
  @override
  IncomingDataType get incomingDataType;
  @override
  OutgoingDataType get outgoingDataType;
  @override
  DataProcessingType get dataProcessingType;
  @override
  BusinessModelType get businessModelType;
  @override
  String? get createdAt;
  @override
  String? get updatedAt;
  @override
  int? get iV;
  @override
  bool? get purchased;
  @override
  String? get rank;
  @override
  String get productContributor;
  @override
  @JsonKey(ignore: true)
  _$$_PostProductCopyWith<_$_PostProduct> get copyWith =>
      throw _privateConstructorUsedError;
}
