// import 'package:equatable/equatable.dart';
// import 'package:json_annotation/json_annotation.dart';
// import '../enums/enums.dart';

// part 'product_launcher.g.dart';

// @JsonSerializable()
// class ProductLauncher extends Equatable {
//   @JsonKey(includeIfNull: false)
//   final String? sId;
//   @JsonKey(includeIfNull: false)
//   final String? createDate;
//   @JsonKey(includeIfNull: false)
//   final String? productLauncherFileUrl;
//   @JsonKey(
//     includeIfNull: false,
//     unknownEnumValue: IncomingDataType.fixedSource,
//   )
//   final IncomingDataType? incomingDataInfo;
//   @JsonKey(
//     includeIfNull: false,
//     unknownEnumValue: OutgoingDataType.fixedChanel,
//   )
//   final OutgoingDataType? outgoingDataInfo;
//   @JsonKey(
//     includeIfNull: false,
//     unknownEnumValue: DataProcessingType.onPremise,
//   )
//   final DataProcessingType? dataProcessingType;
//   @JsonKey(
//     includeIfNull: false,
//     unknownEnumValue: DataCollectingType.noDataCollection,
//   )
//   final DataCollectingType? dataCollecting;

//   ProductLauncher(
//       {this.sId,
//       this.createDate,
//       this.productLauncherFileUrl,
//       this.incomingDataInfo,
//       this.outgoingDataInfo,
//       this.dataProcessingType,
//       this.dataCollecting});

//   factory ProductLauncher.fromJson(Map<String, dynamic> json) =>
//       _$ProductLauncherFromJson(json);

//   Map<String, dynamic> toJson() => _$ProductLauncherToJson(this);

//   @override
//   List<Object> get props => [
//         sId!,
//         createDate!,
//         productLauncherFileUrl!,
//         incomingDataInfo!,
//         outgoingDataInfo!,
//         dataProcessingType!,
//         dataCollecting!,
//       ];

//   @override
//   bool get stringify => true;
// }

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/data_collecting_type.dart';
import 'package:orchestra/models/enums/data_processing_type.dart';
import 'package:orchestra/models/enums/incoming_data_type.dart';
import 'package:orchestra/models/enums/outgoing_data_type.dart';
part 'product_launcher.freezed.dart';
part 'product_launcher.g.dart';

@freezed
class ProductLauncher with _$ProductLauncher {
  factory ProductLauncher({
    @JsonKey(includeIfNull: false)
        final String? sId,
    @JsonKey(includeIfNull: false)
        final String? createDate,
    @JsonKey(includeIfNull: false)
        final String? productLauncherFileUrl,
    @JsonKey(
      includeIfNull: false,
      unknownEnumValue: IncomingDataType.fixedSource,
    )
        final IncomingDataType? incomingDataInfo,
    @JsonKey(
      includeIfNull: false,
      unknownEnumValue: OutgoingDataType.fixedChanel,
    )
        final OutgoingDataType? outgoingDataInfo,
    @JsonKey(
      includeIfNull: false,
      unknownEnumValue: DataProcessingType.onPremise,
    )
        final DataProcessingType? dataProcessingType,
    @JsonKey(
      includeIfNull: false,
      unknownEnumValue: DataCollectingType.noDataCollection,
    )
        final DataCollectingType? dataCollecting,
  }) = _ProductLauncher;

  factory ProductLauncher.fromJson(Map<String, dynamic> json) =>
      _$ProductLauncherFromJson(json);
}
