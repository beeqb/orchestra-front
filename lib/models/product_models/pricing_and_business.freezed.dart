// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'pricing_and_business.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PricingAndBusiness _$PricingAndBusinessFromJson(Map<String, dynamic> json) {
  return _PricingAndBusiness.fromJson(json);
}

/// @nodoc
mixin _$PricingAndBusiness {
  @JsonKey(includeIfNull: false)
  List<String>? get testEndsAfter => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get sId => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get createDate => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
  BusinessModelType? get model => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  num? get price => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  int? get marketplaceReward => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  bool? get freeForTest => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PricingAndBusinessCopyWith<PricingAndBusiness> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PricingAndBusinessCopyWith<$Res> {
  factory $PricingAndBusinessCopyWith(
          PricingAndBusiness value, $Res Function(PricingAndBusiness) then) =
      _$PricingAndBusinessCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(includeIfNull: false)
          List<String>? testEndsAfter,
      @JsonKey(includeIfNull: false)
          String? sId,
      @JsonKey(includeIfNull: false)
          String? createDate,
      @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
          BusinessModelType? model,
      @JsonKey(includeIfNull: false)
          num? price,
      @JsonKey(includeIfNull: false)
          int? marketplaceReward,
      @JsonKey(includeIfNull: false)
          bool? freeForTest});
}

/// @nodoc
class _$PricingAndBusinessCopyWithImpl<$Res>
    implements $PricingAndBusinessCopyWith<$Res> {
  _$PricingAndBusinessCopyWithImpl(this._value, this._then);

  final PricingAndBusiness _value;
  // ignore: unused_field
  final $Res Function(PricingAndBusiness) _then;

  @override
  $Res call({
    Object? testEndsAfter = freezed,
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? model = freezed,
    Object? price = freezed,
    Object? marketplaceReward = freezed,
    Object? freeForTest = freezed,
  }) {
    return _then(_value.copyWith(
      testEndsAfter: testEndsAfter == freezed
          ? _value.testEndsAfter
          : testEndsAfter // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      model: model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as BusinessModelType?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as num?,
      marketplaceReward: marketplaceReward == freezed
          ? _value.marketplaceReward
          : marketplaceReward // ignore: cast_nullable_to_non_nullable
              as int?,
      freeForTest: freeForTest == freezed
          ? _value.freeForTest
          : freeForTest // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$$_PricingAndBusinessCopyWith<$Res>
    implements $PricingAndBusinessCopyWith<$Res> {
  factory _$$_PricingAndBusinessCopyWith(_$_PricingAndBusiness value,
          $Res Function(_$_PricingAndBusiness) then) =
      __$$_PricingAndBusinessCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(includeIfNull: false)
          List<String>? testEndsAfter,
      @JsonKey(includeIfNull: false)
          String? sId,
      @JsonKey(includeIfNull: false)
          String? createDate,
      @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
          BusinessModelType? model,
      @JsonKey(includeIfNull: false)
          num? price,
      @JsonKey(includeIfNull: false)
          int? marketplaceReward,
      @JsonKey(includeIfNull: false)
          bool? freeForTest});
}

/// @nodoc
class __$$_PricingAndBusinessCopyWithImpl<$Res>
    extends _$PricingAndBusinessCopyWithImpl<$Res>
    implements _$$_PricingAndBusinessCopyWith<$Res> {
  __$$_PricingAndBusinessCopyWithImpl(
      _$_PricingAndBusiness _value, $Res Function(_$_PricingAndBusiness) _then)
      : super(_value, (v) => _then(v as _$_PricingAndBusiness));

  @override
  _$_PricingAndBusiness get _value => super._value as _$_PricingAndBusiness;

  @override
  $Res call({
    Object? testEndsAfter = freezed,
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? model = freezed,
    Object? price = freezed,
    Object? marketplaceReward = freezed,
    Object? freeForTest = freezed,
  }) {
    return _then(_$_PricingAndBusiness(
      testEndsAfter: testEndsAfter == freezed
          ? _value._testEndsAfter
          : testEndsAfter // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      model: model == freezed
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as BusinessModelType?,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as num?,
      marketplaceReward: marketplaceReward == freezed
          ? _value.marketplaceReward
          : marketplaceReward // ignore: cast_nullable_to_non_nullable
              as int?,
      freeForTest: freeForTest == freezed
          ? _value.freeForTest
          : freeForTest // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PricingAndBusiness implements _PricingAndBusiness {
  _$_PricingAndBusiness(
      {@JsonKey(includeIfNull: false)
          final List<String>? testEndsAfter,
      @JsonKey(includeIfNull: false)
          this.sId,
      @JsonKey(includeIfNull: false)
          this.createDate,
      @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
          this.model,
      @JsonKey(includeIfNull: false)
          this.price,
      @JsonKey(includeIfNull: false)
          this.marketplaceReward,
      @JsonKey(includeIfNull: false)
          this.freeForTest})
      : _testEndsAfter = testEndsAfter;

  factory _$_PricingAndBusiness.fromJson(Map<String, dynamic> json) =>
      _$$_PricingAndBusinessFromJson(json);

  final List<String>? _testEndsAfter;
  @override
  @JsonKey(includeIfNull: false)
  List<String>? get testEndsAfter {
    final value = _testEndsAfter;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(includeIfNull: false)
  final String? sId;
  @override
  @JsonKey(includeIfNull: false)
  final String? createDate;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
  final BusinessModelType? model;
  @override
  @JsonKey(includeIfNull: false)
  final num? price;
  @override
  @JsonKey(includeIfNull: false)
  final int? marketplaceReward;
  @override
  @JsonKey(includeIfNull: false)
  final bool? freeForTest;

  @override
  String toString() {
    return 'PricingAndBusiness(testEndsAfter: $testEndsAfter, sId: $sId, createDate: $createDate, model: $model, price: $price, marketplaceReward: $marketplaceReward, freeForTest: $freeForTest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PricingAndBusiness &&
            const DeepCollectionEquality()
                .equals(other._testEndsAfter, _testEndsAfter) &&
            const DeepCollectionEquality().equals(other.sId, sId) &&
            const DeepCollectionEquality()
                .equals(other.createDate, createDate) &&
            const DeepCollectionEquality().equals(other.model, model) &&
            const DeepCollectionEquality().equals(other.price, price) &&
            const DeepCollectionEquality()
                .equals(other.marketplaceReward, marketplaceReward) &&
            const DeepCollectionEquality()
                .equals(other.freeForTest, freeForTest));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_testEndsAfter),
      const DeepCollectionEquality().hash(sId),
      const DeepCollectionEquality().hash(createDate),
      const DeepCollectionEquality().hash(model),
      const DeepCollectionEquality().hash(price),
      const DeepCollectionEquality().hash(marketplaceReward),
      const DeepCollectionEquality().hash(freeForTest));

  @JsonKey(ignore: true)
  @override
  _$$_PricingAndBusinessCopyWith<_$_PricingAndBusiness> get copyWith =>
      __$$_PricingAndBusinessCopyWithImpl<_$_PricingAndBusiness>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PricingAndBusinessToJson(
      this,
    );
  }
}

abstract class _PricingAndBusiness implements PricingAndBusiness {
  factory _PricingAndBusiness(
      {@JsonKey(includeIfNull: false)
          final List<String>? testEndsAfter,
      @JsonKey(includeIfNull: false)
          final String? sId,
      @JsonKey(includeIfNull: false)
          final String? createDate,
      @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
          final BusinessModelType? model,
      @JsonKey(includeIfNull: false)
          final num? price,
      @JsonKey(includeIfNull: false)
          final int? marketplaceReward,
      @JsonKey(includeIfNull: false)
          final bool? freeForTest}) = _$_PricingAndBusiness;

  factory _PricingAndBusiness.fromJson(Map<String, dynamic> json) =
      _$_PricingAndBusiness.fromJson;

  @override
  @JsonKey(includeIfNull: false)
  List<String>? get testEndsAfter;
  @override
  @JsonKey(includeIfNull: false)
  String? get sId;
  @override
  @JsonKey(includeIfNull: false)
  String? get createDate;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: BusinessModelType.oneTime)
  BusinessModelType? get model;
  @override
  @JsonKey(includeIfNull: false)
  num? get price;
  @override
  @JsonKey(includeIfNull: false)
  int? get marketplaceReward;
  @override
  @JsonKey(includeIfNull: false)
  bool? get freeForTest;
  @override
  @JsonKey(ignore: true)
  _$$_PricingAndBusinessCopyWith<_$_PricingAndBusiness> get copyWith =>
      throw _privateConstructorUsedError;
}
