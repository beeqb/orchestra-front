// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Stats _$StatsFromJson(Map<String, dynamic> json) => Stats(
      sales: json['sales'] as int? ?? 0,
      users: json['users'] as int? ?? 0,
      launches: json['launches'] as int? ?? 0,
      platformRevenue: json['platformRevenue'] as num? ?? 0,
      contributorRevenue: json['contributorRevenue'] as num? ?? 0,
    );

Map<String, dynamic> _$StatsToJson(Stats instance) => <String, dynamic>{
      'sales': instance.sales,
      'users': instance.users,
      'launches': instance.launches,
      'platformRevenue': instance.platformRevenue,
      'contributorRevenue': instance.contributorRevenue,
    };
