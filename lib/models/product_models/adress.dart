import 'package:freezed_annotation/freezed_annotation.dart';
part 'adress.freezed.dart';
part 'adress.g.dart';

@freezed
class Address with _$Address {
  factory Address({
    @JsonKey(includeIfNull: false) final String? sId,
    @JsonKey(includeIfNull: false) final String? createDate,
    @JsonKey(includeIfNull: false) final String? addressLine1,
    @JsonKey(includeIfNull: false) final String? addressLine2,
    @JsonKey(includeIfNull: false) final String? country,
    @JsonKey(includeIfNull: false) final String? city,
    @JsonKey(includeIfNull: false) final String? state,
    @JsonKey(includeIfNull: false) final String? postcode,
  }) = _Adress;

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);
}
