// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'adress.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Address _$AddressFromJson(Map<String, dynamic> json) {
  return _Adress.fromJson(json);
}

/// @nodoc
mixin _$Address {
  @JsonKey(includeIfNull: false)
  String? get sId => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get createDate => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get addressLine1 => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get addressLine2 => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get country => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get city => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get state => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get postcode => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AddressCopyWith<Address> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddressCopyWith<$Res> {
  factory $AddressCopyWith(Address value, $Res Function(Address) then) =
      _$AddressCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(includeIfNull: false) String? sId,
      @JsonKey(includeIfNull: false) String? createDate,
      @JsonKey(includeIfNull: false) String? addressLine1,
      @JsonKey(includeIfNull: false) String? addressLine2,
      @JsonKey(includeIfNull: false) String? country,
      @JsonKey(includeIfNull: false) String? city,
      @JsonKey(includeIfNull: false) String? state,
      @JsonKey(includeIfNull: false) String? postcode});
}

/// @nodoc
class _$AddressCopyWithImpl<$Res> implements $AddressCopyWith<$Res> {
  _$AddressCopyWithImpl(this._value, this._then);

  final Address _value;
  // ignore: unused_field
  final $Res Function(Address) _then;

  @override
  $Res call({
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? addressLine1 = freezed,
    Object? addressLine2 = freezed,
    Object? country = freezed,
    Object? city = freezed,
    Object? state = freezed,
    Object? postcode = freezed,
  }) {
    return _then(_value.copyWith(
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1: addressLine1 == freezed
          ? _value.addressLine1
          : addressLine1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine2: addressLine2 == freezed
          ? _value.addressLine2
          : addressLine2 // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      postcode: postcode == freezed
          ? _value.postcode
          : postcode // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_AdressCopyWith<$Res> implements $AddressCopyWith<$Res> {
  factory _$$_AdressCopyWith(_$_Adress value, $Res Function(_$_Adress) then) =
      __$$_AdressCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(includeIfNull: false) String? sId,
      @JsonKey(includeIfNull: false) String? createDate,
      @JsonKey(includeIfNull: false) String? addressLine1,
      @JsonKey(includeIfNull: false) String? addressLine2,
      @JsonKey(includeIfNull: false) String? country,
      @JsonKey(includeIfNull: false) String? city,
      @JsonKey(includeIfNull: false) String? state,
      @JsonKey(includeIfNull: false) String? postcode});
}

/// @nodoc
class __$$_AdressCopyWithImpl<$Res> extends _$AddressCopyWithImpl<$Res>
    implements _$$_AdressCopyWith<$Res> {
  __$$_AdressCopyWithImpl(_$_Adress _value, $Res Function(_$_Adress) _then)
      : super(_value, (v) => _then(v as _$_Adress));

  @override
  _$_Adress get _value => super._value as _$_Adress;

  @override
  $Res call({
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? addressLine1 = freezed,
    Object? addressLine2 = freezed,
    Object? country = freezed,
    Object? city = freezed,
    Object? state = freezed,
    Object? postcode = freezed,
  }) {
    return _then(_$_Adress(
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine1: addressLine1 == freezed
          ? _value.addressLine1
          : addressLine1 // ignore: cast_nullable_to_non_nullable
              as String?,
      addressLine2: addressLine2 == freezed
          ? _value.addressLine2
          : addressLine2 // ignore: cast_nullable_to_non_nullable
              as String?,
      country: country == freezed
          ? _value.country
          : country // ignore: cast_nullable_to_non_nullable
              as String?,
      city: city == freezed
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      state: state == freezed
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as String?,
      postcode: postcode == freezed
          ? _value.postcode
          : postcode // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Adress implements _Adress {
  _$_Adress(
      {@JsonKey(includeIfNull: false) this.sId,
      @JsonKey(includeIfNull: false) this.createDate,
      @JsonKey(includeIfNull: false) this.addressLine1,
      @JsonKey(includeIfNull: false) this.addressLine2,
      @JsonKey(includeIfNull: false) this.country,
      @JsonKey(includeIfNull: false) this.city,
      @JsonKey(includeIfNull: false) this.state,
      @JsonKey(includeIfNull: false) this.postcode});

  factory _$_Adress.fromJson(Map<String, dynamic> json) =>
      _$$_AdressFromJson(json);

  @override
  @JsonKey(includeIfNull: false)
  final String? sId;
  @override
  @JsonKey(includeIfNull: false)
  final String? createDate;
  @override
  @JsonKey(includeIfNull: false)
  final String? addressLine1;
  @override
  @JsonKey(includeIfNull: false)
  final String? addressLine2;
  @override
  @JsonKey(includeIfNull: false)
  final String? country;
  @override
  @JsonKey(includeIfNull: false)
  final String? city;
  @override
  @JsonKey(includeIfNull: false)
  final String? state;
  @override
  @JsonKey(includeIfNull: false)
  final String? postcode;

  @override
  String toString() {
    return 'Address(sId: $sId, createDate: $createDate, addressLine1: $addressLine1, addressLine2: $addressLine2, country: $country, city: $city, state: $state, postcode: $postcode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Adress &&
            const DeepCollectionEquality().equals(other.sId, sId) &&
            const DeepCollectionEquality()
                .equals(other.createDate, createDate) &&
            const DeepCollectionEquality()
                .equals(other.addressLine1, addressLine1) &&
            const DeepCollectionEquality()
                .equals(other.addressLine2, addressLine2) &&
            const DeepCollectionEquality().equals(other.country, country) &&
            const DeepCollectionEquality().equals(other.city, city) &&
            const DeepCollectionEquality().equals(other.state, state) &&
            const DeepCollectionEquality().equals(other.postcode, postcode));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(sId),
      const DeepCollectionEquality().hash(createDate),
      const DeepCollectionEquality().hash(addressLine1),
      const DeepCollectionEquality().hash(addressLine2),
      const DeepCollectionEquality().hash(country),
      const DeepCollectionEquality().hash(city),
      const DeepCollectionEquality().hash(state),
      const DeepCollectionEquality().hash(postcode));

  @JsonKey(ignore: true)
  @override
  _$$_AdressCopyWith<_$_Adress> get copyWith =>
      __$$_AdressCopyWithImpl<_$_Adress>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AdressToJson(
      this,
    );
  }
}

abstract class _Adress implements Address {
  factory _Adress(
      {@JsonKey(includeIfNull: false) final String? sId,
      @JsonKey(includeIfNull: false) final String? createDate,
      @JsonKey(includeIfNull: false) final String? addressLine1,
      @JsonKey(includeIfNull: false) final String? addressLine2,
      @JsonKey(includeIfNull: false) final String? country,
      @JsonKey(includeIfNull: false) final String? city,
      @JsonKey(includeIfNull: false) final String? state,
      @JsonKey(includeIfNull: false) final String? postcode}) = _$_Adress;

  factory _Adress.fromJson(Map<String, dynamic> json) = _$_Adress.fromJson;

  @override
  @JsonKey(includeIfNull: false)
  String? get sId;
  @override
  @JsonKey(includeIfNull: false)
  String? get createDate;
  @override
  @JsonKey(includeIfNull: false)
  String? get addressLine1;
  @override
  @JsonKey(includeIfNull: false)
  String? get addressLine2;
  @override
  @JsonKey(includeIfNull: false)
  String? get country;
  @override
  @JsonKey(includeIfNull: false)
  String? get city;
  @override
  @JsonKey(includeIfNull: false)
  String? get state;
  @override
  @JsonKey(includeIfNull: false)
  String? get postcode;
  @override
  @JsonKey(ignore: true)
  _$$_AdressCopyWith<_$_Adress> get copyWith =>
      throw _privateConstructorUsedError;
}
