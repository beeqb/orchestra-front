// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_launcher.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductLauncher _$ProductLauncherFromJson(Map<String, dynamic> json) {
  return _ProductLauncher.fromJson(json);
}

/// @nodoc
mixin _$ProductLauncher {
  @JsonKey(includeIfNull: false)
  String? get sId => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get createDate => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  String? get productLauncherFileUrl => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
  IncomingDataType? get incomingDataInfo => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
  OutgoingDataType? get outgoingDataInfo => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
  DataProcessingType? get dataProcessingType =>
      throw _privateConstructorUsedError;
  @JsonKey(
      includeIfNull: false,
      unknownEnumValue: DataCollectingType.noDataCollection)
  DataCollectingType? get dataCollecting => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductLauncherCopyWith<ProductLauncher> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductLauncherCopyWith<$Res> {
  factory $ProductLauncherCopyWith(
          ProductLauncher value, $Res Function(ProductLauncher) then) =
      _$ProductLauncherCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(includeIfNull: false)
          String? sId,
      @JsonKey(includeIfNull: false)
          String? createDate,
      @JsonKey(includeIfNull: false)
          String? productLauncherFileUrl,
      @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
          IncomingDataType? incomingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
          OutgoingDataType? outgoingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
          DataProcessingType? dataProcessingType,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataCollectingType.noDataCollection)
          DataCollectingType? dataCollecting});
}

/// @nodoc
class _$ProductLauncherCopyWithImpl<$Res>
    implements $ProductLauncherCopyWith<$Res> {
  _$ProductLauncherCopyWithImpl(this._value, this._then);

  final ProductLauncher _value;
  // ignore: unused_field
  final $Res Function(ProductLauncher) _then;

  @override
  $Res call({
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? productLauncherFileUrl = freezed,
    Object? incomingDataInfo = freezed,
    Object? outgoingDataInfo = freezed,
    Object? dataProcessingType = freezed,
    Object? dataCollecting = freezed,
  }) {
    return _then(_value.copyWith(
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      productLauncherFileUrl: productLauncherFileUrl == freezed
          ? _value.productLauncherFileUrl
          : productLauncherFileUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      incomingDataInfo: incomingDataInfo == freezed
          ? _value.incomingDataInfo
          : incomingDataInfo // ignore: cast_nullable_to_non_nullable
              as IncomingDataType?,
      outgoingDataInfo: outgoingDataInfo == freezed
          ? _value.outgoingDataInfo
          : outgoingDataInfo // ignore: cast_nullable_to_non_nullable
              as OutgoingDataType?,
      dataProcessingType: dataProcessingType == freezed
          ? _value.dataProcessingType
          : dataProcessingType // ignore: cast_nullable_to_non_nullable
              as DataProcessingType?,
      dataCollecting: dataCollecting == freezed
          ? _value.dataCollecting
          : dataCollecting // ignore: cast_nullable_to_non_nullable
              as DataCollectingType?,
    ));
  }
}

/// @nodoc
abstract class _$$_ProductLauncherCopyWith<$Res>
    implements $ProductLauncherCopyWith<$Res> {
  factory _$$_ProductLauncherCopyWith(
          _$_ProductLauncher value, $Res Function(_$_ProductLauncher) then) =
      __$$_ProductLauncherCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(includeIfNull: false)
          String? sId,
      @JsonKey(includeIfNull: false)
          String? createDate,
      @JsonKey(includeIfNull: false)
          String? productLauncherFileUrl,
      @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
          IncomingDataType? incomingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
          OutgoingDataType? outgoingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
          DataProcessingType? dataProcessingType,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataCollectingType.noDataCollection)
          DataCollectingType? dataCollecting});
}

/// @nodoc
class __$$_ProductLauncherCopyWithImpl<$Res>
    extends _$ProductLauncherCopyWithImpl<$Res>
    implements _$$_ProductLauncherCopyWith<$Res> {
  __$$_ProductLauncherCopyWithImpl(
      _$_ProductLauncher _value, $Res Function(_$_ProductLauncher) _then)
      : super(_value, (v) => _then(v as _$_ProductLauncher));

  @override
  _$_ProductLauncher get _value => super._value as _$_ProductLauncher;

  @override
  $Res call({
    Object? sId = freezed,
    Object? createDate = freezed,
    Object? productLauncherFileUrl = freezed,
    Object? incomingDataInfo = freezed,
    Object? outgoingDataInfo = freezed,
    Object? dataProcessingType = freezed,
    Object? dataCollecting = freezed,
  }) {
    return _then(_$_ProductLauncher(
      sId: sId == freezed
          ? _value.sId
          : sId // ignore: cast_nullable_to_non_nullable
              as String?,
      createDate: createDate == freezed
          ? _value.createDate
          : createDate // ignore: cast_nullable_to_non_nullable
              as String?,
      productLauncherFileUrl: productLauncherFileUrl == freezed
          ? _value.productLauncherFileUrl
          : productLauncherFileUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      incomingDataInfo: incomingDataInfo == freezed
          ? _value.incomingDataInfo
          : incomingDataInfo // ignore: cast_nullable_to_non_nullable
              as IncomingDataType?,
      outgoingDataInfo: outgoingDataInfo == freezed
          ? _value.outgoingDataInfo
          : outgoingDataInfo // ignore: cast_nullable_to_non_nullable
              as OutgoingDataType?,
      dataProcessingType: dataProcessingType == freezed
          ? _value.dataProcessingType
          : dataProcessingType // ignore: cast_nullable_to_non_nullable
              as DataProcessingType?,
      dataCollecting: dataCollecting == freezed
          ? _value.dataCollecting
          : dataCollecting // ignore: cast_nullable_to_non_nullable
              as DataCollectingType?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProductLauncher implements _ProductLauncher {
  _$_ProductLauncher(
      {@JsonKey(includeIfNull: false)
          this.sId,
      @JsonKey(includeIfNull: false)
          this.createDate,
      @JsonKey(includeIfNull: false)
          this.productLauncherFileUrl,
      @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
          this.incomingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
          this.outgoingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
          this.dataProcessingType,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataCollectingType.noDataCollection)
          this.dataCollecting});

  factory _$_ProductLauncher.fromJson(Map<String, dynamic> json) =>
      _$$_ProductLauncherFromJson(json);

  @override
  @JsonKey(includeIfNull: false)
  final String? sId;
  @override
  @JsonKey(includeIfNull: false)
  final String? createDate;
  @override
  @JsonKey(includeIfNull: false)
  final String? productLauncherFileUrl;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
  final IncomingDataType? incomingDataInfo;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
  final OutgoingDataType? outgoingDataInfo;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
  final DataProcessingType? dataProcessingType;
  @override
  @JsonKey(
      includeIfNull: false,
      unknownEnumValue: DataCollectingType.noDataCollection)
  final DataCollectingType? dataCollecting;

  @override
  String toString() {
    return 'ProductLauncher(sId: $sId, createDate: $createDate, productLauncherFileUrl: $productLauncherFileUrl, incomingDataInfo: $incomingDataInfo, outgoingDataInfo: $outgoingDataInfo, dataProcessingType: $dataProcessingType, dataCollecting: $dataCollecting)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductLauncher &&
            const DeepCollectionEquality().equals(other.sId, sId) &&
            const DeepCollectionEquality()
                .equals(other.createDate, createDate) &&
            const DeepCollectionEquality()
                .equals(other.productLauncherFileUrl, productLauncherFileUrl) &&
            const DeepCollectionEquality()
                .equals(other.incomingDataInfo, incomingDataInfo) &&
            const DeepCollectionEquality()
                .equals(other.outgoingDataInfo, outgoingDataInfo) &&
            const DeepCollectionEquality()
                .equals(other.dataProcessingType, dataProcessingType) &&
            const DeepCollectionEquality()
                .equals(other.dataCollecting, dataCollecting));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(sId),
      const DeepCollectionEquality().hash(createDate),
      const DeepCollectionEquality().hash(productLauncherFileUrl),
      const DeepCollectionEquality().hash(incomingDataInfo),
      const DeepCollectionEquality().hash(outgoingDataInfo),
      const DeepCollectionEquality().hash(dataProcessingType),
      const DeepCollectionEquality().hash(dataCollecting));

  @JsonKey(ignore: true)
  @override
  _$$_ProductLauncherCopyWith<_$_ProductLauncher> get copyWith =>
      __$$_ProductLauncherCopyWithImpl<_$_ProductLauncher>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductLauncherToJson(
      this,
    );
  }
}

abstract class _ProductLauncher implements ProductLauncher {
  factory _ProductLauncher(
      {@JsonKey(includeIfNull: false)
          final String? sId,
      @JsonKey(includeIfNull: false)
          final String? createDate,
      @JsonKey(includeIfNull: false)
          final String? productLauncherFileUrl,
      @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
          final IncomingDataType? incomingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
          final OutgoingDataType? outgoingDataInfo,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
          final DataProcessingType? dataProcessingType,
      @JsonKey(includeIfNull: false, unknownEnumValue: DataCollectingType.noDataCollection)
          final DataCollectingType? dataCollecting}) = _$_ProductLauncher;

  factory _ProductLauncher.fromJson(Map<String, dynamic> json) =
      _$_ProductLauncher.fromJson;

  @override
  @JsonKey(includeIfNull: false)
  String? get sId;
  @override
  @JsonKey(includeIfNull: false)
  String? get createDate;
  @override
  @JsonKey(includeIfNull: false)
  String? get productLauncherFileUrl;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: IncomingDataType.fixedSource)
  IncomingDataType? get incomingDataInfo;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: OutgoingDataType.fixedChanel)
  OutgoingDataType? get outgoingDataInfo;
  @override
  @JsonKey(includeIfNull: false, unknownEnumValue: DataProcessingType.onPremise)
  DataProcessingType? get dataProcessingType;
  @override
  @JsonKey(
      includeIfNull: false,
      unknownEnumValue: DataCollectingType.noDataCollection)
  DataCollectingType? get dataCollecting;
  @override
  @JsonKey(ignore: true)
  _$$_ProductLauncherCopyWith<_$_ProductLauncher> get copyWith =>
      throw _privateConstructorUsedError;
}
