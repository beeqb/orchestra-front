// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'daily_sale.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

DailySales _$DailySalesFromJson(Map<String, dynamic> json) {
  return _DailySale.fromJson(json);
}

/// @nodoc
mixin _$DailySales {
  String get date => throw _privateConstructorUsedError;
  int get sales => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DailySalesCopyWith<DailySales> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DailySalesCopyWith<$Res> {
  factory $DailySalesCopyWith(
          DailySales value, $Res Function(DailySales) then) =
      _$DailySalesCopyWithImpl<$Res>;
  $Res call({String date, int sales});
}

/// @nodoc
class _$DailySalesCopyWithImpl<$Res> implements $DailySalesCopyWith<$Res> {
  _$DailySalesCopyWithImpl(this._value, this._then);

  final DailySales _value;
  // ignore: unused_field
  final $Res Function(DailySales) _then;

  @override
  $Res call({
    Object? date = freezed,
    Object? sales = freezed,
  }) {
    return _then(_value.copyWith(
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      sales: sales == freezed
          ? _value.sales
          : sales // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_DailySaleCopyWith<$Res>
    implements $DailySalesCopyWith<$Res> {
  factory _$$_DailySaleCopyWith(
          _$_DailySale value, $Res Function(_$_DailySale) then) =
      __$$_DailySaleCopyWithImpl<$Res>;
  @override
  $Res call({String date, int sales});
}

/// @nodoc
class __$$_DailySaleCopyWithImpl<$Res> extends _$DailySalesCopyWithImpl<$Res>
    implements _$$_DailySaleCopyWith<$Res> {
  __$$_DailySaleCopyWithImpl(
      _$_DailySale _value, $Res Function(_$_DailySale) _then)
      : super(_value, (v) => _then(v as _$_DailySale));

  @override
  _$_DailySale get _value => super._value as _$_DailySale;

  @override
  $Res call({
    Object? date = freezed,
    Object? sales = freezed,
  }) {
    return _then(_$_DailySale(
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      sales: sales == freezed
          ? _value.sales
          : sales // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DailySale implements _DailySale {
  const _$_DailySale({required this.date, required this.sales});

  factory _$_DailySale.fromJson(Map<String, dynamic> json) =>
      _$$_DailySaleFromJson(json);

  @override
  final String date;
  @override
  final int sales;

  @override
  String toString() {
    return 'DailySales(date: $date, sales: $sales)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DailySale &&
            const DeepCollectionEquality().equals(other.date, date) &&
            const DeepCollectionEquality().equals(other.sales, sales));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(date),
      const DeepCollectionEquality().hash(sales));

  @JsonKey(ignore: true)
  @override
  _$$_DailySaleCopyWith<_$_DailySale> get copyWith =>
      __$$_DailySaleCopyWithImpl<_$_DailySale>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DailySaleToJson(
      this,
    );
  }
}

abstract class _DailySale implements DailySales {
  const factory _DailySale(
      {required final String date, required final int sales}) = _$_DailySale;

  factory _DailySale.fromJson(Map<String, dynamic> json) =
      _$_DailySale.fromJson;

  @override
  String get date;
  @override
  int get sales;
  @override
  @JsonKey(ignore: true)
  _$$_DailySaleCopyWith<_$_DailySale> get copyWith =>
      throw _privateConstructorUsedError;
}
