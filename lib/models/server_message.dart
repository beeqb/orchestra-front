import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_message.g.dart';

@JsonSerializable()
class ServerMessage extends Equatable {
  final String message;

  const ServerMessage({required this.message});

  factory ServerMessage.fromJson(Map<String, dynamic> json) =>
      _$ServerMessageFromJson(json);

  Map<String, dynamic> toJson() => _$ServerMessageToJson(this);

  @override
  List<Object> get props => [];
}
