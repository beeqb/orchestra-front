// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'answer_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnswerItem _$AnswerItemFromJson(Map<String, dynamic> json) => AnswerItem(
      id: json['_id'] as String?,
      ratingId: json['ratingId'] as String,
      answer: json['answer'] as String,
    );

Map<String, dynamic> _$AnswerItemToJson(AnswerItem instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('_id', instance.id);
  val['ratingId'] = instance.ratingId;
  val['answer'] = instance.answer;
  return val;
}
