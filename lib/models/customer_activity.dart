import 'package:freezed_annotation/freezed_annotation.dart';

part 'customer_activity.g.dart';
part 'customer_activity.freezed.dart';

@freezed
class CustomerActivity with _$CustomerActivity {
  const factory CustomerActivity(
      {required int testFly,
      required int buy,
      required double? cr}) = _CustomerActivity;

  factory CustomerActivity.fromJson(Map<String, Object?> json) =>
      _$CustomerActivityFromJson(json);
}
