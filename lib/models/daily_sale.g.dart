// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'daily_sale.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_DailySale _$$_DailySaleFromJson(Map<String, dynamic> json) => _$_DailySale(
      date: json['date'] as String,
      sales: json['sales'] as int,
    );

Map<String, dynamic> _$$_DailySaleToJson(_$_DailySale instance) =>
    <String, dynamic>{
      'date': instance.date,
      'sales': instance.sales,
    };
