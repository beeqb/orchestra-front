import 'package:freezed_annotation/freezed_annotation.dart';

part 'launch_stats.freezed.dart';
part 'launch_stats.g.dart';

@freezed
class LaunchStats with _$LaunchStats {
  const factory LaunchStats({
    double? buy,
    double? testfly,
    double? returnSales,
  }) = _LaunchStats;

  factory LaunchStats.fromJson(Map<String, Object?> json) =>
      _$LaunchStatsFromJson(json);
}
