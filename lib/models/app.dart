// import 'package:equatable/equatable.dart';
// import 'package:json_annotation/json_annotation.dart';

// part 'app.g.dart';

// @JsonSerializable()
// class AppModel extends Equatable {
//   @JsonKey(name: '_id')
//   final String? id;
//   final String? appId;
//   final String userId;
//   final String name;
//   final String status;
//   final String icon;
//   final String? lastMessage;
//   final num? startTime;
//   final num? pauseTime;
//   final num? endTime;
//   final num? pid;
//   final List<String>? widgets;
//   final List<String> notification;
//   final String? createdAt;
//   final String? updatedAt;
//   final String? email;
//   final String? telegram;
//   final String? loop;

//   AppModel({
//     this.id,
//     this.appId,
//     required this.userId,
//     required this.name,
//     required this.status,
//     required this.icon,
//     this.lastMessage,
//     this.startTime,
//     this.pauseTime,
//     this.endTime,
//     this.pid,
//     this.widgets,
//     this.loop,
//     required this.notification,
//     this.createdAt,
//     this.updatedAt,
//     this.email,
//     this.telegram,
//   });

//   factory AppModel.fromJson(Map<String, dynamic> json) =>
//       _$AppModelFromJson(json);

//   Map<String, dynamic> toJson() => _$AppModelToJson(this);

//   @override
//   List<Object> get props => [
//         id!,
//         userId,
//         name,
//         status,
//         icon,
//         // lastMessage!,
//         // startTime!,
//         // pauseTime!,
//         // endTime!,
//         // pid!,
//         // widgets!,
//         // loop!,
//         // notification,
//         // email!,
//         // telegram!,
//       ];

//   @override
//   bool get stringify => true;

//   AppModel copyWith({
//     String? id,
//     String? appId,
//     String? userId,
//     String? name,
//     String? status,
//     String? icon,
//     String? lastMessage,
//     num? startTime,
//     num? pauseTime,
//     num? endTime,
//     num? pid,
//     List<String>? widgets,
//     List<String>? notification,
//     String? createdAt,
//     String? updatedAt,
//     String? email,
//     String? telegram,
//     String? loop,
//   }) {
//     return AppModel(
//       id: id ?? this.id,
//       appId: appId ?? this.appId,
//       userId: userId ?? this.userId,
//       name: name ?? this.name,
//       status: status ?? this.status,
//       icon: icon ?? this.icon,
//       lastMessage: lastMessage ?? this.lastMessage,
//       startTime: startTime ?? this.startTime,
//       pauseTime: pauseTime ?? this.pauseTime,
//       endTime: endTime ?? this.endTime,
//       pid: pid ?? this.pid,
//       widgets: widgets ?? this.widgets,
//       notification: notification ?? this.notification,
//       createdAt: createdAt ?? this.createdAt,
//       updatedAt: updatedAt ?? this.updatedAt,
//       email: email ?? this.email,
//       telegram: telegram ?? this.telegram,
//       loop: loop ?? this.loop,
//     );
//   }
// }

import 'package:freezed_annotation/freezed_annotation.dart';
part 'app.freezed.dart';
part 'app.g.dart';

@freezed
class AppModel with _$AppModel {
  factory AppModel({
    @JsonKey(name: '_id') final String? id,
    final String? appId,
    required final String userId,
    required final String name,
    required final String status,
    required final String icon,
    final String? lastMessage,
    final num? startTime,
    final num? pauseTime,
    final num? endTime,
    final num? pid,
    final List<String>? widgets,
    required final List<String> notification,
    final String? createdAt,
    final String? updatedAt,
    final String? email,
    final String? telegram,
    final String? loop,
    final bool? failure,
  }) = _AppModel;

  factory AppModel.fromJson(Map<String, dynamic> json) =>
      _$AppModelFromJson(json);
}
