// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_stats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserStats _$$_UserStatsFromJson(Map<String, dynamic> json) => _$_UserStats(
      updatedAt: json['updatedAt'] as String?,
      costs: json['costs'] as num? ?? 0,
      purchases: json['purchases'] as num? ?? 0,
      profit: json['profit'] as num? ?? 0,
    );

Map<String, dynamic> _$$_UserStatsToJson(_$_UserStats instance) =>
    <String, dynamic>{
      'updatedAt': instance.updatedAt,
      'costs': instance.costs,
      'purchases': instance.purchases,
      'profit': instance.profit,
    };
