import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:orchestra/models/enums/manifest_input_type.dart';
part 'settings_model.freezed.dart';
part 'settings_model.g.dart';

@freezed
class SettingsModel with _$SettingsModel {
  factory SettingsModel(
    final String? key,
    final String type,
    final String? errorHandle,
    final String name,
    @JsonKey(name: 'data_type') final ManifestInputType dataType,
    @JsonKey(name: 'position_num') final int positionNum,
    @JsonKey(name: 'file_format') final String? fileFormat,
    final int? max,
    @JsonKey(name: 'max_symbols') final int? maxSymbols,
    @JsonKey(includeIfNull: false) final Map<String, String>? labels,
    @JsonKey(includeIfNull: false) final Map<String, String>? options,
  ) = _SettingsModel;

  factory SettingsModel.fromJson(Map<String, dynamic> json) =>
      _$SettingsModelFromJson(json);
}
