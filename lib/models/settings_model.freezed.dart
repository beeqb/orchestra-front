// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'settings_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SettingsModel _$SettingsModelFromJson(Map<String, dynamic> json) {
  return _SettingsModel.fromJson(json);
}

/// @nodoc
mixin _$SettingsModel {
  String? get key => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  String? get errorHandle => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'data_type')
  ManifestInputType get dataType => throw _privateConstructorUsedError;
  @JsonKey(name: 'position_num')
  int get positionNum => throw _privateConstructorUsedError;
  @JsonKey(name: 'file_format')
  String? get fileFormat => throw _privateConstructorUsedError;
  int? get max => throw _privateConstructorUsedError;
  @JsonKey(name: 'max_symbols')
  int? get maxSymbols => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  Map<String, String>? get labels => throw _privateConstructorUsedError;
  @JsonKey(includeIfNull: false)
  Map<String, String>? get options => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SettingsModelCopyWith<SettingsModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsModelCopyWith<$Res> {
  factory $SettingsModelCopyWith(
          SettingsModel value, $Res Function(SettingsModel) then) =
      _$SettingsModelCopyWithImpl<$Res>;
  $Res call(
      {String? key,
      String type,
      String? errorHandle,
      String name,
      @JsonKey(name: 'data_type') ManifestInputType dataType,
      @JsonKey(name: 'position_num') int positionNum,
      @JsonKey(name: 'file_format') String? fileFormat,
      int? max,
      @JsonKey(name: 'max_symbols') int? maxSymbols,
      @JsonKey(includeIfNull: false) Map<String, String>? labels,
      @JsonKey(includeIfNull: false) Map<String, String>? options});
}

/// @nodoc
class _$SettingsModelCopyWithImpl<$Res>
    implements $SettingsModelCopyWith<$Res> {
  _$SettingsModelCopyWithImpl(this._value, this._then);

  final SettingsModel _value;
  // ignore: unused_field
  final $Res Function(SettingsModel) _then;

  @override
  $Res call({
    Object? key = freezed,
    Object? type = freezed,
    Object? errorHandle = freezed,
    Object? name = freezed,
    Object? dataType = freezed,
    Object? positionNum = freezed,
    Object? fileFormat = freezed,
    Object? max = freezed,
    Object? maxSymbols = freezed,
    Object? labels = freezed,
    Object? options = freezed,
  }) {
    return _then(_value.copyWith(
      key: key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      errorHandle: errorHandle == freezed
          ? _value.errorHandle
          : errorHandle // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      dataType: dataType == freezed
          ? _value.dataType
          : dataType // ignore: cast_nullable_to_non_nullable
              as ManifestInputType,
      positionNum: positionNum == freezed
          ? _value.positionNum
          : positionNum // ignore: cast_nullable_to_non_nullable
              as int,
      fileFormat: fileFormat == freezed
          ? _value.fileFormat
          : fileFormat // ignore: cast_nullable_to_non_nullable
              as String?,
      max: max == freezed
          ? _value.max
          : max // ignore: cast_nullable_to_non_nullable
              as int?,
      maxSymbols: maxSymbols == freezed
          ? _value.maxSymbols
          : maxSymbols // ignore: cast_nullable_to_non_nullable
              as int?,
      labels: labels == freezed
          ? _value.labels
          : labels // ignore: cast_nullable_to_non_nullable
              as Map<String, String>?,
      options: options == freezed
          ? _value.options
          : options // ignore: cast_nullable_to_non_nullable
              as Map<String, String>?,
    ));
  }
}

/// @nodoc
abstract class _$$_SettingsModelCopyWith<$Res>
    implements $SettingsModelCopyWith<$Res> {
  factory _$$_SettingsModelCopyWith(
          _$_SettingsModel value, $Res Function(_$_SettingsModel) then) =
      __$$_SettingsModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? key,
      String type,
      String? errorHandle,
      String name,
      @JsonKey(name: 'data_type') ManifestInputType dataType,
      @JsonKey(name: 'position_num') int positionNum,
      @JsonKey(name: 'file_format') String? fileFormat,
      int? max,
      @JsonKey(name: 'max_symbols') int? maxSymbols,
      @JsonKey(includeIfNull: false) Map<String, String>? labels,
      @JsonKey(includeIfNull: false) Map<String, String>? options});
}

/// @nodoc
class __$$_SettingsModelCopyWithImpl<$Res>
    extends _$SettingsModelCopyWithImpl<$Res>
    implements _$$_SettingsModelCopyWith<$Res> {
  __$$_SettingsModelCopyWithImpl(
      _$_SettingsModel _value, $Res Function(_$_SettingsModel) _then)
      : super(_value, (v) => _then(v as _$_SettingsModel));

  @override
  _$_SettingsModel get _value => super._value as _$_SettingsModel;

  @override
  $Res call({
    Object? key = freezed,
    Object? type = freezed,
    Object? errorHandle = freezed,
    Object? name = freezed,
    Object? dataType = freezed,
    Object? positionNum = freezed,
    Object? fileFormat = freezed,
    Object? max = freezed,
    Object? maxSymbols = freezed,
    Object? labels = freezed,
    Object? options = freezed,
  }) {
    return _then(_$_SettingsModel(
      key == freezed
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String?,
      type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      errorHandle == freezed
          ? _value.errorHandle
          : errorHandle // ignore: cast_nullable_to_non_nullable
              as String?,
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      dataType == freezed
          ? _value.dataType
          : dataType // ignore: cast_nullable_to_non_nullable
              as ManifestInputType,
      positionNum == freezed
          ? _value.positionNum
          : positionNum // ignore: cast_nullable_to_non_nullable
              as int,
      fileFormat == freezed
          ? _value.fileFormat
          : fileFormat // ignore: cast_nullable_to_non_nullable
              as String?,
      max == freezed
          ? _value.max
          : max // ignore: cast_nullable_to_non_nullable
              as int?,
      maxSymbols == freezed
          ? _value.maxSymbols
          : maxSymbols // ignore: cast_nullable_to_non_nullable
              as int?,
      labels == freezed
          ? _value._labels
          : labels // ignore: cast_nullable_to_non_nullable
              as Map<String, String>?,
      options == freezed
          ? _value._options
          : options // ignore: cast_nullable_to_non_nullable
              as Map<String, String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SettingsModel implements _SettingsModel {
  _$_SettingsModel(
      this.key,
      this.type,
      this.errorHandle,
      this.name,
      @JsonKey(name: 'data_type') this.dataType,
      @JsonKey(name: 'position_num') this.positionNum,
      @JsonKey(name: 'file_format') this.fileFormat,
      this.max,
      @JsonKey(name: 'max_symbols') this.maxSymbols,
      @JsonKey(includeIfNull: false) final Map<String, String>? labels,
      @JsonKey(includeIfNull: false) final Map<String, String>? options)
      : _labels = labels,
        _options = options;

  factory _$_SettingsModel.fromJson(Map<String, dynamic> json) =>
      _$$_SettingsModelFromJson(json);

  @override
  final String? key;
  @override
  final String type;
  @override
  final String? errorHandle;
  @override
  final String name;
  @override
  @JsonKey(name: 'data_type')
  final ManifestInputType dataType;
  @override
  @JsonKey(name: 'position_num')
  final int positionNum;
  @override
  @JsonKey(name: 'file_format')
  final String? fileFormat;
  @override
  final int? max;
  @override
  @JsonKey(name: 'max_symbols')
  final int? maxSymbols;
  final Map<String, String>? _labels;
  @override
  @JsonKey(includeIfNull: false)
  Map<String, String>? get labels {
    final value = _labels;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  final Map<String, String>? _options;
  @override
  @JsonKey(includeIfNull: false)
  Map<String, String>? get options {
    final value = _options;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  String toString() {
    return 'SettingsModel(key: $key, type: $type, errorHandle: $errorHandle, name: $name, dataType: $dataType, positionNum: $positionNum, fileFormat: $fileFormat, max: $max, maxSymbols: $maxSymbols, labels: $labels, options: $options)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SettingsModel &&
            const DeepCollectionEquality().equals(other.key, key) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.errorHandle, errorHandle) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.dataType, dataType) &&
            const DeepCollectionEquality()
                .equals(other.positionNum, positionNum) &&
            const DeepCollectionEquality()
                .equals(other.fileFormat, fileFormat) &&
            const DeepCollectionEquality().equals(other.max, max) &&
            const DeepCollectionEquality()
                .equals(other.maxSymbols, maxSymbols) &&
            const DeepCollectionEquality().equals(other._labels, _labels) &&
            const DeepCollectionEquality().equals(other._options, _options));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(key),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(errorHandle),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(dataType),
      const DeepCollectionEquality().hash(positionNum),
      const DeepCollectionEquality().hash(fileFormat),
      const DeepCollectionEquality().hash(max),
      const DeepCollectionEquality().hash(maxSymbols),
      const DeepCollectionEquality().hash(_labels),
      const DeepCollectionEquality().hash(_options));

  @JsonKey(ignore: true)
  @override
  _$$_SettingsModelCopyWith<_$_SettingsModel> get copyWith =>
      __$$_SettingsModelCopyWithImpl<_$_SettingsModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SettingsModelToJson(
      this,
    );
  }
}

abstract class _SettingsModel implements SettingsModel {
  factory _SettingsModel(
          final String? key,
          final String type,
          final String? errorHandle,
          final String name,
          @JsonKey(name: 'data_type') final ManifestInputType dataType,
          @JsonKey(name: 'position_num') final int positionNum,
          @JsonKey(name: 'file_format') final String? fileFormat,
          final int? max,
          @JsonKey(name: 'max_symbols') final int? maxSymbols,
          @JsonKey(includeIfNull: false) final Map<String, String>? labels,
          @JsonKey(includeIfNull: false) final Map<String, String>? options) =
      _$_SettingsModel;

  factory _SettingsModel.fromJson(Map<String, dynamic> json) =
      _$_SettingsModel.fromJson;

  @override
  String? get key;
  @override
  String get type;
  @override
  String? get errorHandle;
  @override
  String get name;
  @override
  @JsonKey(name: 'data_type')
  ManifestInputType get dataType;
  @override
  @JsonKey(name: 'position_num')
  int get positionNum;
  @override
  @JsonKey(name: 'file_format')
  String? get fileFormat;
  @override
  int? get max;
  @override
  @JsonKey(name: 'max_symbols')
  int? get maxSymbols;
  @override
  @JsonKey(includeIfNull: false)
  Map<String, String>? get labels;
  @override
  @JsonKey(includeIfNull: false)
  Map<String, String>? get options;
  @override
  @JsonKey(ignore: true)
  _$$_SettingsModelCopyWith<_$_SettingsModel> get copyWith =>
      throw _privateConstructorUsedError;
}
