import 'package:json_annotation/json_annotation.dart';
part 'rating_user.g.dart';

@JsonSerializable()
class RatingUser {
  final String name;
  final String avatar;

  RatingUser(this.name, this.avatar);

  factory RatingUser.fromJson(Map<String, dynamic> json) =>
      _$RatingUserFromJson(json);

  Map<String, dynamic> toJson() => _$RatingUserToJson(this);

  @override
  String toString() => 'RatingUser(name: $name, avatar: $avatar)';
}
