// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppCategory _$$_AppCategoryFromJson(Map<String, dynamic> json) =>
    _$_AppCategory(
      $enumDecode(_$ProductCategoryTypeEnumMap, json['productCategoryType']),
      (json['prods'] as List<dynamic>)
          .map((e) =>
              e == null ? null : Product.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_AppCategoryToJson(_$_AppCategory instance) =>
    <String, dynamic>{
      'productCategoryType':
          _$ProductCategoryTypeEnumMap[instance.productCategoryType]!,
      'prods': instance.prods,
    };

const _$ProductCategoryTypeEnumMap = {
  ProductCategoryType.privateUseOnly: 'PAPP',
  ProductCategoryType.business: 'BU',
  ProductCategoryType.productivity: 'PR',
  ProductCategoryType.mentalHealth: 'MH',
  ProductCategoryType.social: 'SO',
  ProductCategoryType.finance: 'FI',
  ProductCategoryType.sales: 'SA',
  ProductCategoryType.management: 'MA',
  ProductCategoryType.marketing: 'MK',
  ProductCategoryType.accounting: 'AC',
  ProductCategoryType.utils: 'UT',
  ProductCategoryType.trading: 'TR',
  ProductCategoryType.other: 'OT',
};
