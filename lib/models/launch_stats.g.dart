// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'launch_stats.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LaunchStats _$$_LaunchStatsFromJson(Map<String, dynamic> json) =>
    _$_LaunchStats(
      buy: (json['buy'] as num?)?.toDouble(),
      testfly: (json['testfly'] as num?)?.toDouble(),
      returnSales: (json['returnSales'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$_LaunchStatsToJson(_$_LaunchStats instance) =>
    <String, dynamic>{
      'buy': instance.buy,
      'testfly': instance.testfly,
      'returnSales': instance.returnSales,
    };
