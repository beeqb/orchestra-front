// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'purchase_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PurchaseModel _$PurchaseModelFromJson(Map<String, dynamic> json) =>
    PurchaseModel(
      userId: json['userId'] as String,
      activeTest: json['activeTest'] as bool,
      createdAt: json['createdAt'] as String,
      updatedAt: json['updatedAt'] as String,
      launches: json['launches'] as int,
      paidTill: json['paidTill'] as String,
      product: Product.fromJson(json['product'] as Map<String, dynamic>),
      runTime: json['runTime'] as int,
    );

Map<String, dynamic> _$PurchaseModelToJson(PurchaseModel instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'activeTest': instance.activeTest,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'launches': instance.launches,
      'paidTill': instance.paidTill,
      'product': instance.product.toJson(),
      'runTime': instance.runTime,
    };
