import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'answer_item.g.dart';

@JsonSerializable()
class AnswerItem extends Equatable {
  @JsonKey(name: '_id', includeIfNull: false)
  final String? id;
  final String ratingId;
  @JsonKey(includeIfNull: false)
  final String answer;

  const AnswerItem({
    this.id,
    required this.ratingId,
    required this.answer,
  });

  @override
  List<Object> get props => [id!];

  factory AnswerItem.fromJson(Map<String, dynamic> json) =>
      _$AnswerItemFromJson(json);

  Map<String, dynamic> toJson() => _$AnswerItemToJson(this);

  @override
  bool get stringify => true;
}
