import 'package:json_annotation/json_annotation.dart';
part 'test_fly_model.g.dart';

@JsonSerializable()
class TestFlyModel {
  final int launches;
  final String period;

  TestFlyModel({required this.launches, required this.period});

  factory TestFlyModel.fromJson(Map<String, dynamic> json) =>
      _$TestFlyModelFromJson(json);

  Map<String, dynamic> toJson() => _$TestFlyModelToJson(this);
}
