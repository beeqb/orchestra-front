import 'package:json_annotation/json_annotation.dart';

enum BusinessModelType {
  @JsonValue(0)
  oneTime,
  @JsonValue(1)
  perRequest,
  @JsonValue(2)
  monthlySubscription,
}

extension BusinessModelTypeExtension on BusinessModelType {
  String get title {
    switch (this) {
      case BusinessModelType.oneTime:
        return 'One time payment';
      case BusinessModelType.perRequest:
        return 'Pay per request';
      case BusinessModelType.monthlySubscription:
        return 'Monthly subscription';
      default:
        return 'Unknown BusinessModelType title';
    }
  }

  String get description {
    switch (this) {
      case BusinessModelType.oneTime:
        return 'Customer buy your Lore with no additional costs';
      case BusinessModelType.perRequest:
        return 'Customer get your Lore for free but pay any time '
            'when Lore launched';
      case BusinessModelType.monthlySubscription:
        return 'Client get Lore for free but pay monthly royalty '
            'if Lore launched';
      default:
        return 'Unknown BusinessModelType description';
    }
  }
}
