import 'package:json_annotation/json_annotation.dart';

enum DataProcessingType {
  @JsonValue(0)
  onPremise,
  @JsonValue(1)
  cloud,
  @JsonValue(2)
  mixed,
}

extension DataProcessingTypeExtension on DataProcessingType {
  String get title {
    switch (this) {
      case DataProcessingType.onPremise:
        return 'On-premise';
      case DataProcessingType.cloud:
        return 'Cloud';
      case DataProcessingType.mixed:
        return 'Mixed';
      default:
        return 'Unknown DataProcessingType title';
    }
  }

  String get description {
    switch (this) {
      case DataProcessingType.onPremise:
        return 'All data is processed locally - on-premise. User data is '
            'not transferred to a third party server.';
      case DataProcessingType.cloud:
        return 'User data are processed either on the solution provider\'s '
            'server or on the project server. The user data can be used by '
            'the solution provider.';
      case DataProcessingType.mixed:
        return 'The user can choose the type of data processing, or data is '
            'partially processed locally or on remote servers.';
      default:
        return 'Unknown DataProcessingType description';
    }
  }
}
