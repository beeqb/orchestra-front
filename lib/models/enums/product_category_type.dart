import 'package:json_annotation/json_annotation.dart';

enum ProductCategoryType {
  @JsonValue('PAPP')
  privateUseOnly,
  @JsonValue('BU')
  business,
  @JsonValue('PR')
  productivity,
  @JsonValue('MH')
  mentalHealth,
  @JsonValue('SO')
  social,
  @JsonValue('FI')
  finance,
  @JsonValue('SA')
  sales,
  @JsonValue('MA')
  management,
  @JsonValue('MK')
  marketing,
  @JsonValue('AC')
  accounting,
  @JsonValue('UT')
  utils,
  @JsonValue('TR')
  trading,
  @JsonValue('OT')
  other,
}

extension ProductCategoryTypeExtension on ProductCategoryType {
  String get title {
    switch (this) {
      case ProductCategoryType.accounting:
        return 'Accounting';
      case ProductCategoryType.business:
        return 'Business';
      case ProductCategoryType.finance:
        return 'Finance';
      case ProductCategoryType.management:
        return 'Management';
      case ProductCategoryType.marketing:
        return 'Marketing';
      case ProductCategoryType.mentalHealth:
        return 'Mental Health';
      case ProductCategoryType.other:
        return 'Other';
      case ProductCategoryType.privateUseOnly:
        return 'Private use only';
      case ProductCategoryType.productivity:
        return 'Productivity';
      case ProductCategoryType.sales:
        return 'Sales';
      case ProductCategoryType.social:
        return 'Social';
      case ProductCategoryType.trading:
        return 'Trading';
      case ProductCategoryType.utils:
        return 'Utils';
      default:
        return 'Category title not found';
    }
  }

  ProductCategoryType setType(String alias) {
    switch (alias) {
      case 'PAPP':
        return ProductCategoryType.privateUseOnly;
      case 'BU':
        return ProductCategoryType.business;
      case 'PR':
        return ProductCategoryType.productivity;
      case 'MH':
        return ProductCategoryType.mentalHealth;
      case 'SO':
        return ProductCategoryType.social;
      case 'FI':
        return ProductCategoryType.finance;
      case 'SA':
        return ProductCategoryType.sales;
      case 'MA':
        return ProductCategoryType.management;
      case 'MK':
        return ProductCategoryType.marketing;
      case 'AC':
        return ProductCategoryType.accounting;
      case 'UT':
        return ProductCategoryType.utils;
      case 'TR':
        return ProductCategoryType.trading;
      case 'OT':
        return ProductCategoryType.other;
      default:
        return ProductCategoryType.other;
    }
  }

  ProductCategoryType setTypeByName(String name) {
    switch (name) {
      case 'Private use only':
        return ProductCategoryType.privateUseOnly;
      case 'Business':
        return ProductCategoryType.business;
      case 'Productivity':
        return ProductCategoryType.productivity;
      case 'Mental Health':
        return ProductCategoryType.mentalHealth;
      case 'Social':
        return ProductCategoryType.social;
      case 'Finance':
        return ProductCategoryType.finance;
      case 'Sales':
        return ProductCategoryType.sales;
      case 'Management':
        return ProductCategoryType.management;
      case 'Marketing':
        return ProductCategoryType.marketing;
      case 'Accounting':
        return ProductCategoryType.accounting;
      case 'Utils':
        return ProductCategoryType.utils;
      case 'Trading':
        return ProductCategoryType.trading;
      case 'Other':
        return ProductCategoryType.other;
      default:
        return ProductCategoryType.other;
    }
  }
}
