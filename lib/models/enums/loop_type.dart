import 'package:json_annotation/json_annotation.dart';

enum LoopType {
  @JsonValue('none')
  noLoop,
  @JsonValue('300000')
  m5,
  @JsonValue('900000')
  m15,
  @JsonValue('1800000')
  m30,
  @JsonValue('3600000')
  h1,
  @JsonValue('10800000')
  h3,
  @JsonValue('21600000')
  h6,
  @JsonValue('43200000')
  h12,
  @JsonValue('86400000')
  day,
  @JsonValue('604800000')
  week,
  @JsonValue('2629743000')
  month,
  @JsonValue('7889229000')
  month3,
  @JsonValue('31556926000')
  year,
}

extension LoopTypeExtension on LoopType {
  String get title {
    switch (this) {
      case LoopType.noLoop:
        return 'No Loop';
      case LoopType.m5:
        return '5 minutes';
      case LoopType.m15:
        return '15 minutes';
      case LoopType.m30:
        return '30 minutes';
      case LoopType.h1:
        return '1 hour';
      case LoopType.h3:
        return '3 hours';
      case LoopType.h6:
        return '6 hours';
      case LoopType.h12:
        return '12 hours';
      case LoopType.day:
        return '1 day';
      case LoopType.week:
        return '1 week';
      case LoopType.month:
        return '1 month';
      case LoopType.month3:
        return '1 months';
      case LoopType.year:
        return '1 year';
      default:
        return 'Unknown loop period';
    }
  }
}
