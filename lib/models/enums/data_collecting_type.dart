import 'package:json_annotation/json_annotation.dart';

enum DataCollectingType {
  @JsonValue(0)
  noDataCollection,
  @JsonValue(1)
  collectionProcessingData,
  @JsonValue(2)
  depersonalizedDataCollection,
}

extension DataCollectingTypeExtension on DataCollectingType {
  String get title {
    switch (this) {
      case DataCollectingType.noDataCollection:
        return 'No data collection';
      case DataCollectingType.collectionProcessingData:
        return 'Collection processing data';
      case DataCollectingType.depersonalizedDataCollection:
        return 'Depersonalized data collection';
      default:
        throw Exception('enum DataCollectingType has more cases');
    }
  }

  String get description {
    switch (this) {
      case DataCollectingType.noDataCollection:
        return 'The Lore and/or provider doesn\'t collect any incoming, processed and outgoing data.';
      case DataCollectingType.collectionProcessingData:
        return 'The Lore and/or provider collect some or all incoming, processed and outgoing data.';
      case DataCollectingType.depersonalizedDataCollection:
        return 'The Lore and/or provider collect only depersonalized incoming, processed and outgoing data.';
      default:
        throw Exception('enum DataCollectingType has more cases');
    }
  }
}
