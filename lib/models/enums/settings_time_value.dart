enum SettingTimeValue {
  m5,
  m15,
  m30,
  m60,
  m90,
  d1,
  d5,
  wk1,
  mo1,
  mo3,
  unknown,
}

// SettingTimeValue stringToSettingTimeValue(String str) {
//   switch (str) {
//     case '5 minutes':
//       return SettingTimeValue.m5;
//     case '15 minutes':
//       return SettingTimeValue.m15;
//     case '30 minutes':
//       return SettingTimeValue.m30;
//     case '60 minutes':
//       return SettingTimeValue.m60;
//     case '90 minutes':
//       return SettingTimeValue.m90;
//     case '1 day':
//       return SettingTimeValue.d1;
//     case '5 days':
//       return SettingTimeValue.d5;
//     case '1 week':
//       return SettingTimeValue.wk1;
//     case '1 month':
//       return SettingTimeValue.mo1;
//     case '3 months':
//       return SettingTimeValue.mo3;
//     default:
//       return SettingTimeValue.unknown;
//   }
// }
String? parseValueToKey(String? str) {
  switch (str) {
    case '5 minutes':
      return '5m';
    case '15 minutes':
      return '15m';
    case '30 minutes':
      return '30m';
    case '60 minutes':
      return '60m';
    case '90 minutes':
      return '90m';
    case '1 day':
      return '1d';
    case '5 days':
      return '5d';
    case '1 week':
      return '1wk';
    case '1 month':
      return '1mo';
    case '3 months':
      return '3mo';
    case '6 months':
      return '6mo';
    case '1 year':
      return 'year';

    default:
      return null;
  }
}

String? parseKeyToValue(String? str) {
  switch (str) {
    case '5m':
      return '5 minutes';
    case '15m':
      return '15 minutes';
    case '30m':
      return '30 minutes';
    case '60m':
      return '60 minutes';
    case '90m':
      return '90 minutes';
    case '1d':
      return '1 day';
    case '5d':
      return '5 days';
    case '1wk':
      return '1 week';
    case '1mo':
      return '1 month';
    case '3mo':
      return '3 months';
    case '6mo':
      return '6 months';
    case 'year':
      return '1 year';
    default:
      return null;
  }
}

extension SettingTimeValueExtention on SettingTimeValue {
  String get name {
    switch (this) {
      case SettingTimeValue.m5:
        return '5m';
      case SettingTimeValue.m15:
        return '15m';
      case SettingTimeValue.m30:
        return '30m';
      case SettingTimeValue.m60:
        return '60m';
      case SettingTimeValue.m90:
        return '90m';
      case SettingTimeValue.d1:
        return '1d';
      case SettingTimeValue.d5:
        return '5d';
      case SettingTimeValue.wk1:
        return '1wk';
      case SettingTimeValue.mo1:
        return '1mo';
      case SettingTimeValue.mo3:
        return '3mo';
      case SettingTimeValue.unknown:
        return '5m';
    }
  }
}
