import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

enum ProductStatusType {
  @JsonValue('Marketplace resident')
  marketplaceResident,
  @JsonValue('Waiting for review')
  waitingForReview,
  @JsonValue('Deleted')
  deleted,
  @JsonValue('Denied to publish')
  deniedToPublish,
}

extension ProductStatusTypeExt on ProductStatusType {
  String get title {
    switch (this) {
      case ProductStatusType.deleted:
        return 'Deleted';
      case ProductStatusType.deniedToPublish:
        return 'Denied to publish';
      case ProductStatusType.marketplaceResident:
        return 'Marketplace resident';
      case ProductStatusType.waitingForReview:
        return 'Waiting for review';
      default:
        return 'Unknown status';
    }
  }

  Icon get icon {
    switch (this) {
      case ProductStatusType.deleted:
        return Icon(MdiIcons.deleteAlert, color: Colors.blueGrey[200]);
      case ProductStatusType.deniedToPublish:
        return const Icon(
          MdiIcons.alarmLightOutline,
          color: Colors.redAccent,
        );
      case ProductStatusType.marketplaceResident:
        return const Icon(
          MdiIcons.checkOutline,
          color: Colors.green,
        );
      case ProductStatusType.waitingForReview:
        return const Icon(
          MdiIcons.timerSand,
          color: Colors.orange,
        );
      default:
        return const Icon(
          MdiIcons.timerSand,
          color: Colors.orange,
        );
    }
  }
}
