import 'package:flutter/material.dart';

import '../../core/app_colors.dart';

enum DialogType {
  commentDetailsDialog,
  testFlyGoneDialog,
  alertDialog,
  successDialog,
  errorDialog,
  warningDialog,
  infoDialog,
  noInternetDialog,
  appResultDialog,
  successSnackbar,
  errorSnackbar,
  welcomeDialog,
  editWidgetDialog,
}

extension DialogTypeExtension on DialogType {
  IconData get icon {
    switch (this) {
      case DialogType.warningDialog:
        return Icons.warning_rounded;
      case DialogType.successDialog:
        return Icons.check_circle;
      case DialogType.errorDialog:
        return Icons.error;
      case DialogType.noInternetDialog:
        return Icons.portable_wifi_off;
      case DialogType.infoDialog:
      default:
        return Icons.info_outline;
    }
  }

  Color get color {
    switch (this) {
      case DialogType.warningDialog:
        return Colors.orange;
      case DialogType.successDialog:
        return Colors.green;
      case DialogType.errorDialog:
        return Colors.red;
      case DialogType.noInternetDialog:
        return Colors.red;
      case DialogType.infoDialog:
      default:
        return AppColors.blueColor;
    }
  }
}
