enum PhotoUrlType {
  photoDescUrl_1,
  photoDescUrl_2,
  photoDescUrl_3,
  photoDescUrl_4,
  photoDescUrl_5,
  photoDescUrl_6,
  logoUrl,
  promoUrl,
  miniPromoUrl,
  launcherFileUrl
}

extension PhotoTypeExtension on PhotoUrlType {
  // String get name {
  //   switch (this) {
  //     case PhotoUrlType.photoDescUrl_1:
  //       return 'photoDescriptionUrl1R';
  //     case PhotoUrlType.photoDescUrl_2:
  //       return 'photoDescriptionUrl2R';
  //     case PhotoUrlType.photoDescUrl_3:
  //       return 'photoDescriptionUrl3R';
  //     case PhotoUrlType.photoDescUrl_4:
  //       return 'photoDescriptionUrl4R';
  //     case PhotoUrlType.photoDescUrl_5:
  //       return 'photoDescriptionUrl5R';
  //     case PhotoUrlType.photoDescUrl_6:
  //       return 'photoDescriptionUrl6R';
  //     case PhotoUrlType.logoUrl:
  //       return 'productLogoURL';
  //     case PhotoUrlType.promoUrl:
  //       return 'productPromoPictureURL';
  //     case PhotoUrlType.miniPromoUrl:
  //       return 'productMiniPromoPictureURL';
  //     case PhotoUrlType.launcherFileUrl:
  //       return 'productLauncherFileUrl';
  //     default:
  //       return 'null';
  //   }
  // }

  int get photoIndex {
    switch (this) {
      case PhotoUrlType.photoDescUrl_1:
        return 0;
      case PhotoUrlType.photoDescUrl_2:
        return 1;
      case PhotoUrlType.photoDescUrl_3:
        return 2;
      case PhotoUrlType.photoDescUrl_4:
        return 3;
      case PhotoUrlType.photoDescUrl_5:
        return 4;
      case PhotoUrlType.photoDescUrl_6:
        return 5;
      default:
        return 404;
    }
  }
}
