import 'package:json_annotation/json_annotation.dart';

enum IncomingDataType {
  @JsonValue(0)
  fixedSource,
  @JsonValue(1)
  editableSource,
  @JsonValue(2)
  dataProvederNeeded,
}

extension IncomingDataTypeExtension on IncomingDataType {
  String get title {
    switch (this) {
      case IncomingDataType.fixedSource:
        return 'Fixed source';
      case IncomingDataType.editableSource:
        return 'Editable source';
      case IncomingDataType.dataProvederNeeded:
        return 'Data provider needed';
      default:
        return 'Unknown IncomingDataType title';
    }
  }

  String get description {
    switch (this) {
      case IncomingDataType.fixedSource:
        return 'Users can\'t choose or change data sources. It means there no'
            ' need data provider for the Lore.';
      case IncomingDataType.editableSource:
        return 'Users can enter self source or can choose one of the existing.'
            ' It means there no need data provider for the Lore.';
      case IncomingDataType.dataProvederNeeded:
        return 'Lore doesn\'t have an internal data provider or data '
            'grabber. Users should use third - party data providers to get an '
            'Lore\'s benefits.';
      default:
        return 'Unknown IncomingDataType description';
    }
  }
}
