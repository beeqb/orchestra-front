import 'package:json_annotation/json_annotation.dart';

enum OutgoingDataType {
  @JsonValue(0)
  fixedChanel,
  @JsonValue(1)
  editableChanel,
  @JsonValue(2)
  resultStreamedNeeded,
}

extension OutgoingDataTypeExtension on OutgoingDataType {
  String get title {
    switch (this) {
      case OutgoingDataType.fixedChanel:
        return 'Fixed channel';
      case OutgoingDataType.editableChanel:
        return 'Editable channel';
      case OutgoingDataType.resultStreamedNeeded:
        return 'Result streamer needed';
      default:
        return 'Unknown OutgoingDataType title';
    }
  }

  String get description {
    switch (this) {
      case OutgoingDataType.fixedChanel:
        return 'Users can\'t choose or change result streaming service / '
            'type. It means there no need result streamer for the Lore.';
      case OutgoingDataType.editableChanel:
        return 'Users can enter self source or can choose one of the '
            'existing. It means there no need result streamer for the Lore.';
      case OutgoingDataType.resultStreamedNeeded:
        return 'Lore doesn\'t have an internal result streamer. Users '
            'should use third - party result streamers to get an Lore\'s'
            ' benefits.';
      default:
        return 'Unknown OutgoingDataType description';
    }
  }
}
