enum AppActionType {
  stopApp,
  startApp,
}

extension AppActionTypeExtention on AppActionType {
  String get name {
    switch (this) {
      case AppActionType.startApp:
        return 'start';
      case AppActionType.stopApp:
        return 'stop';
      default:
        return 'null';
    }
  }
}
