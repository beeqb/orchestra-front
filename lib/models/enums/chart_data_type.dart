enum ChartDataType {
  profit,
  users,
  sales,
  conversion,
}

extension ChartDataTypeExt on ChartDataType {
  String get name {
    switch (this) {
      case ChartDataType.profit:
        return 'Profit';
      case ChartDataType.users:
        return 'Users';
      case ChartDataType.sales:
        return 'Sales';
      case ChartDataType.conversion:
        return 'Conversion';
      default:
        return 'Chart data type not found';
    }
  }
}
