import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

enum UserType {
  @JsonValue('Admin')
  admin,
  @JsonValue('User')
  user,
}

extension UserTypeExtension on UserType {
  String get title => this == UserType.admin ? 'Admin' : 'User';
  Icon get icon => this == UserType.admin
      ? const Icon(
          MdiIcons.chessQueen,
          color: Colors.blue,
        )
      : const Icon(
          MdiIcons.chessPawn,
          color: Colors.green,
        );
}
