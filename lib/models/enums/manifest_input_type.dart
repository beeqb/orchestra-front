import 'package:json_annotation/json_annotation.dart';

enum ManifestInputType {
  @JsonValue('select')
  select,
  @JsonValue('radio')
  radio,
  @JsonValue('checkbox')
  checkbox,
  @JsonValue('text')
  text,
  @JsonValue('textarea')
  textarea,
}
