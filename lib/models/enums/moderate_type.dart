enum ModerateType { deny, publish }

extension ModerateTypeExt on ModerateType {
  String get name {
    switch (this) {
      case ModerateType.deny:
        return 'deny';
      case ModerateType.publish:
        return 'publish';
      default:
        return 'null';
    }
  }

  String get decision {
    switch (this) {
      case ModerateType.deny:
        return 'Denied to publish';
      case ModerateType.publish:
        return 'Marketplace resident';
      default:
        return 'null';
    }
  }
}
