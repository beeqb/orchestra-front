// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'app.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AppModel _$AppModelFromJson(Map<String, dynamic> json) {
  return _AppModel.fromJson(json);
}

/// @nodoc
mixin _$AppModel {
  @JsonKey(name: '_id')
  String? get id => throw _privateConstructorUsedError;
  String? get appId => throw _privateConstructorUsedError;
  String get userId => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  String get icon => throw _privateConstructorUsedError;
  String? get lastMessage => throw _privateConstructorUsedError;
  num? get startTime => throw _privateConstructorUsedError;
  num? get pauseTime => throw _privateConstructorUsedError;
  num? get endTime => throw _privateConstructorUsedError;
  num? get pid => throw _privateConstructorUsedError;
  List<String>? get widgets => throw _privateConstructorUsedError;
  List<String> get notification => throw _privateConstructorUsedError;
  String? get createdAt => throw _privateConstructorUsedError;
  String? get updatedAt => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get telegram => throw _privateConstructorUsedError;
  String? get loop => throw _privateConstructorUsedError;
  bool? get failure => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AppModelCopyWith<AppModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppModelCopyWith<$Res> {
  factory $AppModelCopyWith(AppModel value, $Res Function(AppModel) then) =
      _$AppModelCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: '_id') String? id,
      String? appId,
      String userId,
      String name,
      String status,
      String icon,
      String? lastMessage,
      num? startTime,
      num? pauseTime,
      num? endTime,
      num? pid,
      List<String>? widgets,
      List<String> notification,
      String? createdAt,
      String? updatedAt,
      String? email,
      String? telegram,
      String? loop,
      bool? failure});
}

/// @nodoc
class _$AppModelCopyWithImpl<$Res> implements $AppModelCopyWith<$Res> {
  _$AppModelCopyWithImpl(this._value, this._then);

  final AppModel _value;
  // ignore: unused_field
  final $Res Function(AppModel) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? appId = freezed,
    Object? userId = freezed,
    Object? name = freezed,
    Object? status = freezed,
    Object? icon = freezed,
    Object? lastMessage = freezed,
    Object? startTime = freezed,
    Object? pauseTime = freezed,
    Object? endTime = freezed,
    Object? pid = freezed,
    Object? widgets = freezed,
    Object? notification = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? email = freezed,
    Object? telegram = freezed,
    Object? loop = freezed,
    Object? failure = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      appId: appId == freezed
          ? _value.appId
          : appId // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String,
      lastMessage: lastMessage == freezed
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      startTime: startTime == freezed
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as num?,
      pauseTime: pauseTime == freezed
          ? _value.pauseTime
          : pauseTime // ignore: cast_nullable_to_non_nullable
              as num?,
      endTime: endTime == freezed
          ? _value.endTime
          : endTime // ignore: cast_nullable_to_non_nullable
              as num?,
      pid: pid == freezed
          ? _value.pid
          : pid // ignore: cast_nullable_to_non_nullable
              as num?,
      widgets: widgets == freezed
          ? _value.widgets
          : widgets // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      notification: notification == freezed
          ? _value.notification
          : notification // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      telegram: telegram == freezed
          ? _value.telegram
          : telegram // ignore: cast_nullable_to_non_nullable
              as String?,
      loop: loop == freezed
          ? _value.loop
          : loop // ignore: cast_nullable_to_non_nullable
              as String?,
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
abstract class _$$_AppModelCopyWith<$Res> implements $AppModelCopyWith<$Res> {
  factory _$$_AppModelCopyWith(
          _$_AppModel value, $Res Function(_$_AppModel) then) =
      __$$_AppModelCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: '_id') String? id,
      String? appId,
      String userId,
      String name,
      String status,
      String icon,
      String? lastMessage,
      num? startTime,
      num? pauseTime,
      num? endTime,
      num? pid,
      List<String>? widgets,
      List<String> notification,
      String? createdAt,
      String? updatedAt,
      String? email,
      String? telegram,
      String? loop,
      bool? failure});
}

/// @nodoc
class __$$_AppModelCopyWithImpl<$Res> extends _$AppModelCopyWithImpl<$Res>
    implements _$$_AppModelCopyWith<$Res> {
  __$$_AppModelCopyWithImpl(
      _$_AppModel _value, $Res Function(_$_AppModel) _then)
      : super(_value, (v) => _then(v as _$_AppModel));

  @override
  _$_AppModel get _value => super._value as _$_AppModel;

  @override
  $Res call({
    Object? id = freezed,
    Object? appId = freezed,
    Object? userId = freezed,
    Object? name = freezed,
    Object? status = freezed,
    Object? icon = freezed,
    Object? lastMessage = freezed,
    Object? startTime = freezed,
    Object? pauseTime = freezed,
    Object? endTime = freezed,
    Object? pid = freezed,
    Object? widgets = freezed,
    Object? notification = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
    Object? email = freezed,
    Object? telegram = freezed,
    Object? loop = freezed,
    Object? failure = freezed,
  }) {
    return _then(_$_AppModel(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      appId: appId == freezed
          ? _value.appId
          : appId // ignore: cast_nullable_to_non_nullable
              as String?,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
      icon: icon == freezed
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String,
      lastMessage: lastMessage == freezed
          ? _value.lastMessage
          : lastMessage // ignore: cast_nullable_to_non_nullable
              as String?,
      startTime: startTime == freezed
          ? _value.startTime
          : startTime // ignore: cast_nullable_to_non_nullable
              as num?,
      pauseTime: pauseTime == freezed
          ? _value.pauseTime
          : pauseTime // ignore: cast_nullable_to_non_nullable
              as num?,
      endTime: endTime == freezed
          ? _value.endTime
          : endTime // ignore: cast_nullable_to_non_nullable
              as num?,
      pid: pid == freezed
          ? _value.pid
          : pid // ignore: cast_nullable_to_non_nullable
              as num?,
      widgets: widgets == freezed
          ? _value._widgets
          : widgets // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      notification: notification == freezed
          ? _value._notification
          : notification // ignore: cast_nullable_to_non_nullable
              as List<String>,
      createdAt: createdAt == freezed
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as String?,
      updatedAt: updatedAt == freezed
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      telegram: telegram == freezed
          ? _value.telegram
          : telegram // ignore: cast_nullable_to_non_nullable
              as String?,
      loop: loop == freezed
          ? _value.loop
          : loop // ignore: cast_nullable_to_non_nullable
              as String?,
      failure: failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AppModel implements _AppModel {
  _$_AppModel(
      {@JsonKey(name: '_id') this.id,
      this.appId,
      required this.userId,
      required this.name,
      required this.status,
      required this.icon,
      this.lastMessage,
      this.startTime,
      this.pauseTime,
      this.endTime,
      this.pid,
      final List<String>? widgets,
      required final List<String> notification,
      this.createdAt,
      this.updatedAt,
      this.email,
      this.telegram,
      this.loop,
      this.failure})
      : _widgets = widgets,
        _notification = notification;

  factory _$_AppModel.fromJson(Map<String, dynamic> json) =>
      _$$_AppModelFromJson(json);

  @override
  @JsonKey(name: '_id')
  final String? id;
  @override
  final String? appId;
  @override
  final String userId;
  @override
  final String name;
  @override
  final String status;
  @override
  final String icon;
  @override
  final String? lastMessage;
  @override
  final num? startTime;
  @override
  final num? pauseTime;
  @override
  final num? endTime;
  @override
  final num? pid;
  final List<String>? _widgets;
  @override
  List<String>? get widgets {
    final value = _widgets;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<String> _notification;
  @override
  List<String> get notification {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_notification);
  }

  @override
  final String? createdAt;
  @override
  final String? updatedAt;
  @override
  final String? email;
  @override
  final String? telegram;
  @override
  final String? loop;
  @override
  final bool? failure;

  @override
  String toString() {
    return 'AppModel(id: $id, appId: $appId, userId: $userId, name: $name, status: $status, icon: $icon, lastMessage: $lastMessage, startTime: $startTime, pauseTime: $pauseTime, endTime: $endTime, pid: $pid, widgets: $widgets, notification: $notification, createdAt: $createdAt, updatedAt: $updatedAt, email: $email, telegram: $telegram, loop: $loop, failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppModel &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.appId, appId) &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality().equals(other.icon, icon) &&
            const DeepCollectionEquality()
                .equals(other.lastMessage, lastMessage) &&
            const DeepCollectionEquality().equals(other.startTime, startTime) &&
            const DeepCollectionEquality().equals(other.pauseTime, pauseTime) &&
            const DeepCollectionEquality().equals(other.endTime, endTime) &&
            const DeepCollectionEquality().equals(other.pid, pid) &&
            const DeepCollectionEquality().equals(other._widgets, _widgets) &&
            const DeepCollectionEquality()
                .equals(other._notification, _notification) &&
            const DeepCollectionEquality().equals(other.createdAt, createdAt) &&
            const DeepCollectionEquality().equals(other.updatedAt, updatedAt) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.telegram, telegram) &&
            const DeepCollectionEquality().equals(other.loop, loop) &&
            const DeepCollectionEquality().equals(other.failure, failure));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(id),
        const DeepCollectionEquality().hash(appId),
        const DeepCollectionEquality().hash(userId),
        const DeepCollectionEquality().hash(name),
        const DeepCollectionEquality().hash(status),
        const DeepCollectionEquality().hash(icon),
        const DeepCollectionEquality().hash(lastMessage),
        const DeepCollectionEquality().hash(startTime),
        const DeepCollectionEquality().hash(pauseTime),
        const DeepCollectionEquality().hash(endTime),
        const DeepCollectionEquality().hash(pid),
        const DeepCollectionEquality().hash(_widgets),
        const DeepCollectionEquality().hash(_notification),
        const DeepCollectionEquality().hash(createdAt),
        const DeepCollectionEquality().hash(updatedAt),
        const DeepCollectionEquality().hash(email),
        const DeepCollectionEquality().hash(telegram),
        const DeepCollectionEquality().hash(loop),
        const DeepCollectionEquality().hash(failure)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$_AppModelCopyWith<_$_AppModel> get copyWith =>
      __$$_AppModelCopyWithImpl<_$_AppModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AppModelToJson(
      this,
    );
  }
}

abstract class _AppModel implements AppModel {
  factory _AppModel(
      {@JsonKey(name: '_id') final String? id,
      final String? appId,
      required final String userId,
      required final String name,
      required final String status,
      required final String icon,
      final String? lastMessage,
      final num? startTime,
      final num? pauseTime,
      final num? endTime,
      final num? pid,
      final List<String>? widgets,
      required final List<String> notification,
      final String? createdAt,
      final String? updatedAt,
      final String? email,
      final String? telegram,
      final String? loop,
      final bool? failure}) = _$_AppModel;

  factory _AppModel.fromJson(Map<String, dynamic> json) = _$_AppModel.fromJson;

  @override
  @JsonKey(name: '_id')
  String? get id;
  @override
  String? get appId;
  @override
  String get userId;
  @override
  String get name;
  @override
  String get status;
  @override
  String get icon;
  @override
  String? get lastMessage;
  @override
  num? get startTime;
  @override
  num? get pauseTime;
  @override
  num? get endTime;
  @override
  num? get pid;
  @override
  List<String>? get widgets;
  @override
  List<String> get notification;
  @override
  String? get createdAt;
  @override
  String? get updatedAt;
  @override
  String? get email;
  @override
  String? get telegram;
  @override
  String? get loop;
  @override
  bool? get failure;
  @override
  @JsonKey(ignore: true)
  _$$_AppModelCopyWith<_$_AppModel> get copyWith =>
      throw _privateConstructorUsedError;
}
