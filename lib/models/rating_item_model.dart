import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'rating_user.dart';

part 'rating_item_model.g.dart';

@JsonSerializable(explicitToJson: true)
class RatingItem extends Equatable {
  @JsonKey(name: '_id', includeIfNull: false)
  final String? id;
  final int rank;
  @JsonKey(includeIfNull: false)
  final String? comment;
  @JsonKey(name: 'appId')
  final String productId;
  final String? createdAt;
  final RatingUser? user;
  final String? answer;

  const RatingItem({
    this.id,
    required this.rank,
    this.comment,
    required this.productId,
    this.createdAt,
    this.user,
    this.answer,
  });

  @override
  List<Object> get props => [id!, createdAt!];

  factory RatingItem.fromJson(Map<String, dynamic> json) =>
      _$RatingItemFromJson(json);

  Map<String, dynamic> toJson() => _$RatingItemToJson(this);

  @override
  bool get stringify => true;
}
