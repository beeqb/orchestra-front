import 'package:equatable/equatable.dart';

class AppActionRequest extends Equatable {
  final String type;
  final String appId;

  const AppActionRequest({required this.type, required this.appId});

  Map<String, String> toMap() {
    var map = <String, String>{};
    map['type'] = type;
    map['appId'] = appId;
    return map;
  }

  @override
  List<Object> get props => [type, appId];

  @override
  String toString() {
    return 'AppActionRequest(type: $type, appId: $appId)';
  }
}
