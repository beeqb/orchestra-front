// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'launch_stats.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LaunchStats _$LaunchStatsFromJson(Map<String, dynamic> json) {
  return _LaunchStats.fromJson(json);
}

/// @nodoc
mixin _$LaunchStats {
  double? get buy => throw _privateConstructorUsedError;
  double? get testfly => throw _privateConstructorUsedError;
  double? get returnSales => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LaunchStatsCopyWith<LaunchStats> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LaunchStatsCopyWith<$Res> {
  factory $LaunchStatsCopyWith(
          LaunchStats value, $Res Function(LaunchStats) then) =
      _$LaunchStatsCopyWithImpl<$Res>;
  $Res call({double? buy, double? testfly, double? returnSales});
}

/// @nodoc
class _$LaunchStatsCopyWithImpl<$Res> implements $LaunchStatsCopyWith<$Res> {
  _$LaunchStatsCopyWithImpl(this._value, this._then);

  final LaunchStats _value;
  // ignore: unused_field
  final $Res Function(LaunchStats) _then;

  @override
  $Res call({
    Object? buy = freezed,
    Object? testfly = freezed,
    Object? returnSales = freezed,
  }) {
    return _then(_value.copyWith(
      buy: buy == freezed
          ? _value.buy
          : buy // ignore: cast_nullable_to_non_nullable
              as double?,
      testfly: testfly == freezed
          ? _value.testfly
          : testfly // ignore: cast_nullable_to_non_nullable
              as double?,
      returnSales: returnSales == freezed
          ? _value.returnSales
          : returnSales // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
abstract class _$$_LaunchStatsCopyWith<$Res>
    implements $LaunchStatsCopyWith<$Res> {
  factory _$$_LaunchStatsCopyWith(
          _$_LaunchStats value, $Res Function(_$_LaunchStats) then) =
      __$$_LaunchStatsCopyWithImpl<$Res>;
  @override
  $Res call({double? buy, double? testfly, double? returnSales});
}

/// @nodoc
class __$$_LaunchStatsCopyWithImpl<$Res> extends _$LaunchStatsCopyWithImpl<$Res>
    implements _$$_LaunchStatsCopyWith<$Res> {
  __$$_LaunchStatsCopyWithImpl(
      _$_LaunchStats _value, $Res Function(_$_LaunchStats) _then)
      : super(_value, (v) => _then(v as _$_LaunchStats));

  @override
  _$_LaunchStats get _value => super._value as _$_LaunchStats;

  @override
  $Res call({
    Object? buy = freezed,
    Object? testfly = freezed,
    Object? returnSales = freezed,
  }) {
    return _then(_$_LaunchStats(
      buy: buy == freezed
          ? _value.buy
          : buy // ignore: cast_nullable_to_non_nullable
              as double?,
      testfly: testfly == freezed
          ? _value.testfly
          : testfly // ignore: cast_nullable_to_non_nullable
              as double?,
      returnSales: returnSales == freezed
          ? _value.returnSales
          : returnSales // ignore: cast_nullable_to_non_nullable
              as double?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LaunchStats implements _LaunchStats {
  const _$_LaunchStats({this.buy, this.testfly, this.returnSales});

  factory _$_LaunchStats.fromJson(Map<String, dynamic> json) =>
      _$$_LaunchStatsFromJson(json);

  @override
  final double? buy;
  @override
  final double? testfly;
  @override
  final double? returnSales;

  @override
  String toString() {
    return 'LaunchStats(buy: $buy, testfly: $testfly, returnSales: $returnSales)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LaunchStats &&
            const DeepCollectionEquality().equals(other.buy, buy) &&
            const DeepCollectionEquality().equals(other.testfly, testfly) &&
            const DeepCollectionEquality()
                .equals(other.returnSales, returnSales));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(buy),
      const DeepCollectionEquality().hash(testfly),
      const DeepCollectionEquality().hash(returnSales));

  @JsonKey(ignore: true)
  @override
  _$$_LaunchStatsCopyWith<_$_LaunchStats> get copyWith =>
      __$$_LaunchStatsCopyWithImpl<_$_LaunchStats>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LaunchStatsToJson(
      this,
    );
  }
}

abstract class _LaunchStats implements LaunchStats {
  const factory _LaunchStats(
      {final double? buy,
      final double? testfly,
      final double? returnSales}) = _$_LaunchStats;

  factory _LaunchStats.fromJson(Map<String, dynamic> json) =
      _$_LaunchStats.fromJson;

  @override
  double? get buy;
  @override
  double? get testfly;
  @override
  double? get returnSales;
  @override
  @JsonKey(ignore: true)
  _$$_LaunchStatsCopyWith<_$_LaunchStats> get copyWith =>
      throw _privateConstructorUsedError;
}
