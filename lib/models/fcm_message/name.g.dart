// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'name.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NameModel _$NameModelFromJson(Map<String, dynamic> json) => NameModel(
      from: json['from'] as String?,
      priority: json['priority'] as String?,
      notification: json['notification'] == null
          ? null
          : NotificationModel.fromJson(
              json['notification'] as Map<String, dynamic>),
      collapseKey: json['collapse_key'] as String?,
    );

Map<String, dynamic> _$NameModelToJson(NameModel instance) => <String, dynamic>{
      'from': instance.from,
      'priority': instance.priority,
      'notification': instance.notification?.toJson(),
      'collapse_key': instance.collapseKey,
    };
