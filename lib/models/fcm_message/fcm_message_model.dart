import 'package:json_annotation/json_annotation.dart';

import 'name.dart';
part 'fcm_message_model.g.dart';

@JsonSerializable(explicitToJson: true)
class FcmMessageModel {
  final NameModel name;
  FcmMessageModel({
    required this.name,
  });

  factory FcmMessageModel.fromJson(Map<String, dynamic> json) =>
      _$FcmMessageModelFromJson(json);

  Map<String, dynamic> toJson() => _$FcmMessageModelToJson(this);
}
