// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fcm_message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FcmMessageModel _$FcmMessageModelFromJson(Map<String, dynamic> json) =>
    FcmMessageModel(
      name: NameModel.fromJson(json['name'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FcmMessageModelToJson(FcmMessageModel instance) =>
    <String, dynamic>{
      'name': instance.name.toJson(),
    };
