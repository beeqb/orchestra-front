import 'package:json_annotation/json_annotation.dart';

import 'notification.dart';
part 'name.g.dart';

@JsonSerializable(explicitToJson: true)
class NameModel {
  final String? from;
  final String? priority;
  final NotificationModel? notification;
  @JsonKey(name: 'collapse_key')
  final String? collapseKey;
  NameModel({
    this.from,
    this.priority,
    this.notification,
    this.collapseKey,
  });

  factory NameModel.fromJson(Map<String, dynamic> json) =>
      _$NameModelFromJson(json);

  Map<String, dynamic> toJson() => _$NameModelToJson(this);
}
