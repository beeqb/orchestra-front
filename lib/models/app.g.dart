// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AppModel _$$_AppModelFromJson(Map<String, dynamic> json) => _$_AppModel(
      id: json['_id'] as String?,
      appId: json['appId'] as String?,
      userId: json['userId'] as String,
      name: json['name'] as String,
      status: json['status'] as String,
      icon: json['icon'] as String,
      lastMessage: json['lastMessage'] as String?,
      startTime: json['startTime'] as num?,
      pauseTime: json['pauseTime'] as num?,
      endTime: json['endTime'] as num?,
      pid: json['pid'] as num?,
      widgets:
          (json['widgets'] as List<dynamic>?)?.map((e) => e as String).toList(),
      notification: (json['notification'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
      email: json['email'] as String?,
      telegram: json['telegram'] as String?,
      loop: json['loop'] as String?,
      failure: json['failure'] as bool?,
    );

Map<String, dynamic> _$$_AppModelToJson(_$_AppModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'appId': instance.appId,
      'userId': instance.userId,
      'name': instance.name,
      'status': instance.status,
      'icon': instance.icon,
      'lastMessage': instance.lastMessage,
      'startTime': instance.startTime,
      'pauseTime': instance.pauseTime,
      'endTime': instance.endTime,
      'pid': instance.pid,
      'widgets': instance.widgets,
      'notification': instance.notification,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'email': instance.email,
      'telegram': instance.telegram,
      'loop': instance.loop,
      'failure': instance.failure,
    };
