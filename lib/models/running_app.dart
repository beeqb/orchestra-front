import 'package:json_annotation/json_annotation.dart';
import 'package:orchestra/models/enums/enums.dart';

import 'package:orchestra/models/user.dart';

part 'running_app.g.dart';

@JsonSerializable(explicitToJson: true)
class RunningAppModel {
  @JsonKey(name: '_id')
  final String? id;
  final String? appId;
  final UserModel userId;
  final String name;
  final String status;
  final String icon;
  final String? lastMessage;
  final int? startTime;
  final int? pauseTime;
  final int? endTime;
  final int? pid;
  final List<String>? widgets;
  final List<String> notification;
  final String? createdAt;
  final String? updatedAt;
  final String? email;
  final String? telegram;
  final LoopType? loop;

  RunningAppModel({
    this.id,
    this.appId,
    required this.userId,
    required this.name,
    required this.status,
    required this.icon,
    this.lastMessage,
    this.startTime,
    this.pauseTime,
    this.endTime,
    this.pid,
    this.widgets,
    this.loop,
    required this.notification,
    this.createdAt,
    this.updatedAt,
    this.email,
    this.telegram,
  });

  factory RunningAppModel.fromJson(Map<String, dynamic> json) =>
      _$RunningAppModelFromJson(json);

  Map<String, dynamic> toJson() => _$RunningAppModelToJson(this);

  @override
  String toString() {
    return 'RunningAppModel(id: $id, appId: $appId, userId: ${userId.name}, '
        'name: $name, status: $status, icon: $icon, lastMessage: $lastMessage,'
        ' startTime: $startTime, pauseTime: $pauseTime, endTime: $endTime, '
        'pid: $pid, widgets: $widgets, notification: $notification, '
        'createdAt: $createdAt, updatedAt: $updatedAt, email: $email, '
        'telegram: $telegram, loop: ${loop!.title})';
  }
}
