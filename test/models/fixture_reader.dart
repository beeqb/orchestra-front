import 'dart:io';

String fixture(String name) => File('test/models/$name').readAsStringSync();
