// ignore_for_file: unnecessary_const

import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:orchestra/models/enums/enums.dart';
import 'package:orchestra/models/user.dart';

import 'models/fixture_reader.dart';

void main() {
  const tUser = const UserModel(
    balance: 100.05000000000003,
    sended: 15,
    received: 0,
    deposited: 235,
    contributor: true,
    isBlocked: false,
    referralToken: 'ae51aa2ddd0c6bf65d01c17193e6ff9d',
    referralCount: 0,
    id: '6004742da9b829005f59b464',
    provider: 'google',
    referrerId: null,
    type: UserType.user,
    name: 'Вячеслав Матусевич',
    photoUrl:
        'https://lh3.googleusercontent.com/a-/AOh14Giw-5ccC8DUTOtsAQc5xaLzot8e1GvefJmXAtnqg8Y=s96-c',
    email: 'matusevichvv@gmail.com',
    createdAt: '2021-01-17T17:30:21.322Z',
    updatedAt: '2021-01-24T12:53:43.850Z',
  );
  group('from Json', () {
    test('User from Json', () async {
      final Map<String, dynamic> jsonMap = jsonDecode(fixture('user.json'));
      final result = UserModel.fromJson(jsonMap['user']);
      expect(result, tUser);
    });
  });
}
