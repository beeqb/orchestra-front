/* import 'dart:convert';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:orchestra/blocs/bloc/contributor_bloc.dart';
import 'package:orchestra/blocs/cubits/checkout_cubit.dart';
import 'package:orchestra/blocs/cubits/user_cubit.dart';
import 'package:orchestra/models/user.dart';
import 'package:orchestra/services/billing_service.dart';
import 'package:orchestra/services/dialogs_service.dart';
import 'package:orchestra/services/user_service.dart';

import 'models/fixture_reader.dart';

class MockUserCubit extends MockBloc<UserModel> implements UserCubit {}

class MockUserService extends Mock implements UserService {}

class MockDialogsService extends Mock implements DialogsService {}

class MockBillingsService extends Mock implements BillingService {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MockUserCubit mockUserCubit;
  MockDialogsService mockDialogsService;
  MockUserService mockUserService;
  MockBillingsService mockBillingsService;

  setUp(() {
    mockUserCubit = MockUserCubit();
    mockDialogsService = MockDialogsService();
    mockUserService = MockUserService();
    mockBillingsService = MockBillingsService();
  });

  group('ContributorCubit', () {
    final alreadyContributor = <String, String>{
      'status': 'You are already contributor'
    };

    final successState = <String, String>{'success': 'Status upgraded'};

    final errorResponse = <String, String>{
      'error': 'Non-sufficient funds, please make deposit'
    };

    blocTest<ContributorBloc, dynamic>(
      'Contributor state set success',
      build: () {
        when(mockUserService.becomeContributor())
            .thenAnswer((_) async => successState);
        return ContributorBloc(
          userService: mockUserService,
          dialogsService: mockDialogsService,
          userCubit: mockUserCubit,
        );
      },
      act: (bloc) async => bloc.add(BecomeContributorEvent()),
      expect: [
        ContributorUpdating(),
        ContributorUpdated(isContributor: true),
      ],
    );

    blocTest<ContributorBloc, dynamic>(
      'Contributor state insufficient funds',
      build: () {
        when(mockUserService.becomeContributor())
            .thenAnswer((_) async => errorResponse);
        return ContributorBloc(
          userService: mockUserService,
          dialogsService: mockDialogsService,
          userCubit: mockUserCubit,
        );
      },
      act: (bloc) async => bloc.add(BecomeContributorEvent()),
      expect: [
        ContributorUpdating(),
        ContributorFailure(message: errorResponse.values.first),
      ],
    );

    blocTest<ContributorBloc, dynamic>(
      'Contributor state already contributor',
      build: () {
        when(mockUserService.becomeContributor())
            .thenAnswer((_) async => alreadyContributor);
        return ContributorBloc(
          userService: mockUserService,
          dialogsService: mockDialogsService,
          userCubit: mockUserCubit,
        );
      },
      act: (bloc) async => bloc.add(BecomeContributorEvent()),
      expect: [
        ContributorUpdating(),
        ContributorFailure(message: alreadyContributor.values.first),
      ],
    );
  });

  group('CheckoutCubit', () {
    final stripeServerResponce = Map<String, dynamic>.from(
        json.decode(fixture('stripe_session_response.json')));
    blocTest<CheckoutCubit, dynamic>(
      'CheckoutCubit recieved result null for STRIPE',
      build: () {
        when(mockBillingsService.deposit(gateway: 'stripe', amount: 10.0))
            .thenAnswer((_) async => null);
        return CheckoutCubit(
          dialogsService: mockDialogsService,
          billingService: mockBillingsService,
        );
      },
      act: (cubit) async => cubit.createCheckout('stripe', 10.0),
      expect: [
        CheckoutLoading(),
        CheckoutError(message: 'CHECKOUT_ERROR server response NULL'),
      ],
    );
    blocTest<CheckoutCubit, dynamic>(
      'CheckoutCubit recieved result null for COINBASE',
      build: () {
        when(mockBillingsService.deposit(gateway: 'coinbase', amount: 10.0))
            .thenAnswer((_) async => null);
        return CheckoutCubit(
          dialogsService: mockDialogsService,
          billingService: mockBillingsService,
        );
      },
      act: (cubit) async => cubit.createCheckout('coinbase', 10.0),
      expect: [
        CheckoutLoading(),
        CheckoutError(message: 'CHECKOUT_ERROR server response NULL'),
      ],
    );
    blocTest<CheckoutCubit, dynamic>(
      'CheckoutCubit should to return state CheckoutLoaded for STRIPE',
      build: () {
        when(mockBillingsService.deposit(gateway: 'stripe', amount: 10.0))
            .thenAnswer((_) async => stripeServerResponce);
        return CheckoutCubit(
          dialogsService: mockDialogsService,
          billingService: mockBillingsService,
        );
      },
      act: (cubit) async => cubit.createCheckout('stripe', 10.0),
      expect: [
        CheckoutLoading(),
        CheckoutLoaded(session: stripeServerResponce['id']),
      ],
    );
    blocTest<CheckoutCubit, dynamic>(
      'CheckoutCubit should to return state CheckoutLoaded for COINBASE',
      build: () {
        when(mockBillingsService.deposit(gateway: 'coinbase', amount: 10.0))
            .thenAnswer((_) async => stripeServerResponce);
        return CheckoutCubit(
          dialogsService: mockDialogsService,
          billingService: mockBillingsService,
        );
      },
      act: (cubit) async => cubit.createCheckout('coinbase', 10.0),
      expect: [
        CheckoutLoading(),
        CheckoutLoaded(session: stripeServerResponce['id']),
      ],
    );
  });
}
 */