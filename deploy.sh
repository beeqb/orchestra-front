#!/bin/bash
cd /home/frontmx/www/orchestra-front/
git pull origin master
docker build -t glukhota/frontmx .
docker login docker.io
docker push glukhota/frontmx
cd /home/frontmx/www/orchestra-front/kube
kubectl delete -f app.yaml
kubectl create -f app.yaml

exit 0
